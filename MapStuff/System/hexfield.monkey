Strict

Import space_game
'Import brl.filesystem

Class Cord
	Field x:float
	Field y:float
	Method New(x:Float = 0, y:Float = 0)
		Self.x = x
		Self.y = y
	End
End

Class HexField Implements Loadable
	Const TIME_MULTIPLIER:Int = 3
	Const RADIUS:Int = 500
	Const MAP_SIZE:Int = 20
	Const UPDATES_PER_FRAME:Int = 5
	Const DRAW_ALL_TILES:Bool = False
	
	Const SPAWN_RATE:Float = 43.47
	Const POI_MIN:Int = 40
	Const POI_MAX:Int = 60
	Field system:Int
	Field showPath:Bool
	Field hex:Stack<Hex>
	Field moveCounter:Int
	Field moveTime:Int
	Field haveMoved:Bool = True
	Field centerX:Int
	Field centerY:Int
	Field cameraX:Int
	Field cameraY:Int
	Field spaceObjects:Stack<HexObject>
	Field grabObject:HexObject
	Field lList:LeveledList
	Field locked:bool
	Field level:int
	Field onEdgeTile:bool
	Field economy:Economy
	Field systemName:String
	
	Method New(economy:Economy, system:Int, level:Int, name:String)
		Self.system = system
		Self.economy = economy
		centerX = (ScreenWidth / 2) + 430
		centerY = ScreenHeight / 2
		cameraX = centerX
		cameraY = centerY
		Self.level = level
		systemName = name
		lList = New LeveledList(economy, level)
		spaceObjects = New Stack<HexObject>()
		lList.BuildPartsStore()
		lList.BuildModStore()
		BuildGrid()
	End
	
	
	
	Method FileName:String()
		Return "" + system + "_map.json"
	End
	
	Method ClearCurrent:Void()
		If grabObject = Null Then Return
		For Local temp:Int = 0 Until spaceObjects.Length()
			If spaceObjects.Get(temp).x = grabObject.x And spaceObjects.Get(temp).y = grabObject.y And spaceObjects.Get(temp).z = grabObject.z
				If ( not spaceObjects.Get(temp).isProtected())
					spaceObjects.Remove(temp)
					grabObject = Null
				End
				Return
			End
		Next
	End
	
	Method OnSave:JsonObject()
		Local data:JsonObject = New JsonObject()
		Local objects:JsonArray = New JsonArray(spaceObjects.Length())
		For Local temp:Int = 0 Until objects.Length()
			objects.Set(temp, spaceObjects.Get(temp).OnSave())
		Next
		data.Set("Objects", objects)
		data.SetInt("Level", level)
		Return data
	End
	
	Method OnLoad:Void(data:JsonObject)
		Local objArr:JsonArray = JsonArray(data.Get("Objects"))
		spaceObjects = New Stack<HexObject>()
		For Local temp:Int = 0 Until objArr.Length()
			Local obj:HexObject = New HexObject(0, 0, 0, 0, 0, 0)
			obj.OnLoad(JsonObject(objArr.Get(temp)), hex)
			spaceObjects.Push(obj)
		Next
	End
	
	Method BuildSystem:Void()

		Local g:Int
		Local found:Bool
		Local count:Int
		
		
		If spaceObjects = Null Or spaceObjects.Length() = 0
			spaceObjects = New Stack<HexObject>()
			spaceObjects.Push(New HexObject(0, 0, 0, hex.Get(0).xPos, hex.Get(0).yPos, HexObject.STAR))
			Repeat
				g = Rnd(hex.Length())
				If hex.Get(g).r = 3
					spaceObjects.Push(New HexObject(hex.Get(g).x, hex.Get(g).y, hex.Get(g).z, hex.Get(g).xPos, hex.Get(g).yPos, HexObject.PLANET, Null, getInventory(),, getCrew()))
					Exit
				End
			Forever
			Repeat
				g = Rnd(hex.Length())
				If hex.Get(g).r = 6
					spaceObjects.Push(New HexObject(hex.Get(g).x, hex.Get(g).y, hex.Get(g).z, hex.Get(g).xPos, hex.Get(g).yPos, HexObject.PLANET, Null, getInventory(),, getCrew()))
					Exit
				End
			Forever
			Repeat
				g = Rnd(hex.Length())
				If hex.Get(g).r = 9
					spaceObjects.Push(New HexObject(hex.Get(g).x, hex.Get(g).y, hex.Get(g).z, hex.Get(g).xPos, hex.Get(g).yPos, HexObject.PLANET, Null, getInventory(),, getCrew()))
					Exit
				End
			Forever
			count = Log(1.0 + economy.resources) * SPAWN_RATE
			found = True
			While (count > 0) '//Asteroids
				g = Rnd(hex.Length())
				found = True
				If Not ( (hex.Get(g).r >= 10 And hex.Get(g).r <= 12)) Then Continue
				For Local temp:Int = 0 Until spaceObjects.Length()
					If spaceObjects.Get(temp).x = hex.Get(g).x And spaceObjects.Get(temp).y = hex.Get(g).y And spaceObjects.Get(temp).z = hex.Get(g).z
						found = False
						Exit
					End
				End
				If found = True
					spaceObjects.Push(New HexObject(hex.Get(g).x, hex.Get(g).y, hex.Get(g).z, hex.Get(g).xPos, hex.Get(g).yPos, HexObject.ASTEROID,,, New Asteroid(1220, 100, False, economy.resources)))
					count -= 1
				End
			Wend
			
			count = Rnd(POI_MIN, POI_MAX)
			While (count > 0) '//Events
				g = Rnd(hex.Length())
				found = True
				If hex.Get(g).isEdge Then Continue
				For Local temp:Int = 0 Until spaceObjects.Length()
					If spaceObjects.Get(temp).x = hex.Get(g).x And spaceObjects.Get(temp).y = hex.Get(g).y And spaceObjects.Get(temp).z = hex.Get(g).z
						found = False
						Continue 'this was exit before :/
					End
				End
				If found = True
					spaceObjects.Push(New HexObject(hex.Get(g).x, hex.Get(g).y, hex.Get(g).z, hex.Get(g).xPos, hex.Get(g).yPos, HexObject.POI))
					count -= 1
				End
			Wend
			Local counter:Int = 1
			For Local obj:HexObject = EachIn spaceObjects
				If obj.type = HexObject.PLANET
					obj.name = systemName + NameGenerator.GetRoman(counter, True)
					counter += 1
				End
			Next
			
		End
		
	End
	
	Method AddQuest:Void(id:int)
		Repeat
			Local g:int = Rnd(hex.Length())
			If hex.Get(g).isEdge Then Continue
			For Local temp:Int = 0 Until spaceObjects.Length()
				If spaceObjects.Get(temp).x = hex.Get(g).x And spaceObjects.Get(temp).y = hex.Get(g).y And spaceObjects.Get(temp).z = hex.Get(g).z
					Continue 'this was exit before :/
				End
			End
			Local obj:HexObject = New HexObject(hex.Get(g).x, hex.Get(g).y, hex.Get(g).z, hex.Get(g).xPos, hex.Get(g).yPos, HexObject.QUEST)
			obj.questid = id
			spaceObjects.Push(obj)
			Return
		Forever
	End
	
	Method BuildGrid:Void()
		hex = New Stack<Hex>()
		Local o:Hex = New Hex(0, 0, 0, New Cord(centerX, centerY), 0, False)
		hex.Push(o)
		Local tail:Int = hex.Length()
		Local head:Int = 0
		Local r:Int = MAP_SIZE
		Local maxr:int = r
		head = 0
		Local edge:Bool = False
		While r > 0
			tail = hex.Length()
			While head < tail
				Local h:Hex = hex.Get(head)
				If isDupe(h.x, h.y + 1, h.z - 1) = False
					hex.Push(New Hex(h.x, h.y + 1, h.z - 1, GetCord(h, 0, 1, -1), maxr - r, edge))
				End
				If isDupe(h.x, h.y - 1, h.z + 1) = False
					hex.Push(New Hex(h.x, h.y - 1, h.z + 1, GetCord(h, 0, -1, 1), maxr - r, edge))
				End
				If isDupe(h.x + 1, h.y, h.z - 1) = False
					hex.Push(New Hex(h.x + 1, h.y, h.z - 1, GetCord(h, 1, 0, -1), maxr - r, edge))
				End
				If isDupe(h.x - 1, h.y, h.z + 1) = False
					hex.Push(New Hex(h.x - 1, h.y, h.z + 1, GetCord(h, -1, 0, 1), maxr - r, edge))
				End
				If isDupe(h.x + 1, h.y - 1, h.z) = False
					hex.Push(New Hex(h.x + 1, h.y - 1, h.z, GetCord(h, 1, -1, 0), maxr - r, edge))
				End
				If isDupe(h.x - 1, h.y + 1, h.z) = False
					hex.Push(New Hex(h.x - 1, h.y + 1, h.z, GetCord(h, -1, 1, 0), maxr - r, edge))
				End
				head += 1
			Wend
			r -= 1
			head = tail
			tail = hex.Length()
			If r = 1 Then edge = True
		Wend
	End
	
	Method isMoving:Bool()
		If moveCounter <= 0 And haveMoved = True Then Return False
		Return True
	End
	
	Method finishedMoving:Bool()
		If Player.map.x = Player.map.targetX And Player.map.y = Player.map.targetY And Player.map.z = Player.map.targetZ Then Return True
		Return False
	End
	Method Render:Void()
		Local px:Int
		Local py:Int
		Local tx:Int
		Local ty:int
		For Local temp:Int = 0 Until hex.Length()
			#If CONFIG="debug"
			
			#ELse
				If not Game.isFrozen and hex.Get(temp).PointInHex(Game.Cursor.x(), Game.Cursor.y(), cameraX, cameraY, centerX, centerY) And finishedMoving() ' And haveMoved And not isMoving()
					hex.Get(temp).Render(cameraX, cameraY, centerX, centerY)
				End
				If hex.Get(temp).isDupe(Player.map.targetX, Player.map.targetY, Player.map.targetZ) And Not finishedMoving()
					hex.Get(temp).Render(cameraX, cameraY, centerX, centerY)
				End
				If DRAW_ALL_TILES
					hex.Get(temp).Render(cameraX, cameraY, centerX, centerY)
				End
				If hex.Get(temp).isEdge
					SetColor(0, 0, 255)
					hex.Get(temp).Render(cameraX, cameraY, centerX, centerY)
				Else If hex.Get(temp).visible = True And showPath
					hex.Get(temp).Render(cameraX, cameraY, centerX, centerY)
				End
			#End
			
			If hex.Get(temp).isDupe(Player.map.x, Player.map.y, Player.map.z)
				px = hex.Get(temp).xPos
				py = hex.Get(temp).yPos
			End
			If hex.Get(temp).isDupe(Player.map.nextX, Player.map.nextY, Player.map.nextZ)
				tx = hex.Get(temp).xPos
				ty = hex.Get(temp).yPos
			End
			
		Next
		For Local t:Int = 0 Until spaceObjects.Length()
			spaceObjects.Get(t).Render(cameraX, cameraY, centerX, centerY)
		Next
		If moveCounter <= 0 And haveMoved = True
			cameraX = (centerX - px)
			cameraY = (centerY - py)
			DrawCircle(px + cameraX, py + cameraY, 5)
		Else
			px = Lerp(px, tx, (moveTime - moveCounter) / float(moveTime))
			py = Lerp(py, ty, (moveTime - moveCounter) / float(moveTime))
			cameraX = (centerX - px)
			cameraY = (centerY - py)
			DrawCircle(px + cameraX, py + cameraY, 5)
		End
	'	Print px + "/" + py
	'	Print centerX + "#" + centerY
		DrawArc(centerX, centerY, RADIUS, RADIUS, 0, 360, 50)
		

		Local range:Int = 2 + Player.ship.GetSystemEffect(Modcodes.SCAN)
		DrawArc(centerX, centerY, (range * Hex.SIZE), (range * Hex.SIZE), 0, 360, 20)
		ResetColorAlpha()

	End
	
	Method getCrew:Stack<Crew>()
		Local s:Stack<Crew> = New Stack<Crew>()
		For Local temp:Int = 0 Until HexObject.CREW_SIZE
			Local c:Crew = New Crew(-1, -1, Null, RaceLoader.GetRace("ANY", "ANY", "PLAYABLE"))
			c.skills.RandomLevel(Rnd(3, 11), False)
			c.health = c.GetMaxHp()
			c.SetBasicJob(Crew.JOB_MECHANIC)
			s.Push(c)
		Next
		Return s
	End
	
	Method getInventory:Inventory()
	
	
		Local i:Inventory = New Inventory(HexObject.STORE_SIZE)
		
		'// Goods
		Local index:Int = 0
		i.slots[index] = New Item(ItemLoader.GetItem("Fuel"))
		Local num:int = economy.industry * 200
		num = 200
		i.slots[index].count = num + Rnd(num / 2)
		index += 1
		
		i.slots[index] = New Item(ItemLoader.GetItem("Antimatter"))
		i.slots[index].count = (num / 2) + Rnd(num / 4)
		index += 1
		
		num = (2.0 - economy.GetPrice(Item.FOOD)) * 1200
		i.slots[index] = New Item(ItemLoader.GetItem("Supplies"))
		i.slots[index].count = (num) + Rnd(num / 2)
		index += 1
		
		
		num = 100
		i.slots[index] = New Item(ItemLoader.GetItem("Missile"))
		i.slots[index].count = (num) + Rnd(num / 2)
		index += 1
		
		num = 500
		i.slots[index] = New Item(ItemLoader.GetItem("Cannon Ball"))
		i.slots[index].count = (num) + Rnd(num / 2)
		index += 1
		
		num = 100
		i.slots[index] = New Item(ItemLoader.GetItem("Thermite"))
		i.slots[index].count = (num) + Rnd(num / 2)
		index += 1
		
		Local s:ItemStack = ItemLoader.GetItemList(Item.METAL)
		For Local count:Int = 0 Until economy.resources * 10
			Local name:String = s.Get(Rnd(s.Length())).id
			i.slots[index] = New Item(ItemLoader.GetItem(name))
			i.slots[index].count += Rnd(20, 20 + (economy.resources * 50))
			index += 1
		Next
		
		
		'// Parts
		For Local temp:Int = 0 Until 18 * economy.technology
			i.slots[index] = lList.GetItem(LeveledList.PART_STORE, True)
			index += 1
		End
		'// Mods
		
		For Local temp:Int = 0 Until 10 * economy.technology
			i.slots[index] = lList.GetItem(LeveledList.MOD_STORE, True)
			index += 1
		End
		
		Return i
	End
	#rem
	Method getMods:Inventory()
		Local i:Inventory = New Inventory(HexObject.STORE_SIZE)
		Local counter:Int = 10 * area.technology
		Local index:Int = 0
		While counter > 0
			Local it:Item = lList.modStore.Get(Rnd(lList.modStore.Length()))
			If it.rarity > Rnd(100)
				counter -= 1
				i.slots[index] = New Item(ItemLoader.GetItem(it.id), 0, it.qualityLevel)
				index += 1
			End
		End
		i.Sort(True)
		Return i
	End
	Method getParts:Inventory()
		Local i:Inventory = New Inventory(HexObject.STORE_SIZE)
		
		i.Sort(True)
		Return i
	End
	
	Method getGoods:Inventory()
		Local i:Inventory = New Inventory(HexObject.STORE_SIZE)
		i.slots[0] = New Item(ItemLoader.GetItem("Fuel"))
		Local num:int = (2.0 - area.GetPrice(Item.FUEL)) * 200
		i.slots[0].count = num + Rnd(num / 2)
		i.slots[1] = New Item(ItemLoader.GetItem("Antimatter"))
		i.slots[1].count = (num / 2) + Rnd(num / 4)
		num = (2.0 - area.GetPrice(Item.FOOD)) * 1200
		i.slots[2] = New Item(ItemLoader.GetItem("Supplies"))
		i.slots[2].count = (num) + Rnd(num / 2)
		Local index:Int = 3
		'parts
		
		Local s:ItemStack = ItemLoader.GetItemList(Item.METAL)
		For Local count:Int = 0 Until area.resources * 10
			Local name:String = s.Get(Rnd(s.Length())).id
			i.slots[index] = New Item(ItemLoader.GetItem(name))
			i.slots[index].count += Rnd(20, 20 + (area.resources * 50))
			index += 1
		Next
		Return i
	End
	#end
	Method Update:Void()
		If Game.Cursor.Hit()
			For Local temp:Int = 0 Until hex.Length()
				If hex.Get(temp).PointInHex(Game.Cursor.x(), Game.Cursor.y(), cameraX, cameraY, centerX, centerY)
					Select Player.ship.GetWarpCode()
						Case WarpCode.READY_TO_WARP
							Player.map.targetX = hex.Get(temp).x
							Player.map.targetY = hex.Get(temp).y
							Player.map.targetZ = hex.Get(temp).z
							Exit
						Case WarpCode.ENGINES_OFFLINE
							Player.chat.Log(LogItem.FAILURE, "ENGINES OFFLINE")
						Case WarpCode.HIGH_HEAT
							Player.chat.Log(LogItem.FAILURE, "SYSTEMS OVERHEATING")
						Case WarpCode.LOW_POWER
							Player.chat.Log(LogItem.FAILURE, "INSUFFICIENT POWER")
						Case WarpCode.NO_FUEL
							Player.chat.Log(LogItem.FAILURE, "INSUFFICIENT FUEL")
					End
					
				End
			Next
		End
		If Game.Cursor.RightHit()
			Print "click"
			For Local temp:Int = 0 Until hex.Length()
				If hex.Get(temp).PointInHex(Game.Cursor.x(), Game.Cursor.y(), cameraX, cameraY, centerX, centerY)
					For Local count:Int = 0 Until spaceObjects.Length
						If hex.Get(temp).x = spaceObjects.Get(count).x And hex.Get(temp).y = spaceObjects.Get(count).y And hex.Get(temp).z = spaceObjects.Get(count).z
							Print "found"
							'spaceObjects.Get(count).Scan(systemData)
							Exit
						End
					Next
				End
			Next
		End
		grabObject = Null
		MovePlayer(Player.map.targetX, Player.map.targetY, Player.map.targetZ)
		If isMoving()
			For Local temp:Int = 0 Until UPDATES_PER_FRAME
				Player.ship.Update(, True)
			Next
		End
		For Local x:Int = 0 Until spaceObjects.Length()
			If Player.map.x = spaceObjects.Get(x).x And Player.map.y = spaceObjects.Get(x).y And Player.map.z = spaceObjects.Get(x).z
				grabObject = spaceObjects.Get(x)
			End
		End
		onEdgeTile = False
		For Local temp:Int = hex.Length() -1 Until 0 Step - 1
			If hex.Get(temp).isDupe(Player.map.x, Player.map.y, Player.map.z) And hex.Get(temp).isEdge
				onEdgeTile = True
				Exit
			Else If hex.Get(temp).isEdge = False
				Exit
			End
		Next
	End
	
	Method Scan:Void()
		Local range:Int = 2 + Player.ship.GetSystemEffect(Modcodes.SCAN)
		Local tx:Int
		Local ty:Int
		
		For Local temp:Int = 0 Until hex.Length()
			If hex.Get(temp).isDupe(Player.map.x, Player.map.y, Player.map.z)
				tx = hex.Get(temp).xPos
				ty = hex.Get(temp).yPos
				Exit
			End
		Next
		Local ping:Bool = False
		For Local temp:Int = 0 Until spaceObjects.Length()
			If spaceObjects.Get(temp).visible = False
				If Distance(spaceObjects.Get(temp).xPos, spaceObjects.Get(temp).yPos, tx, ty) <= 1 + (range * Hex.SIZE)
					spaceObjects.Get(temp).visible = True
					ping = True
					If spaceObjects.Get(temp).type = HexObject.ASTEROID
						Player.chat.Log(LogItem.RADAR, "An Asteroid has been detected")
					Else If spaceObjects.Get(temp).type = HexObject.POI
						Player.chat.Log(LogItem.COMMS, "Undefined signal source has been detected")
					End
				End
			End
		Next
		If ping Then SoundEffect.Play(SoundEffect.RADAR)
		
	End
	
	Method CancelMovement:Void()
		Player.map.targetX = Player.map.x
		Player.map.targetY = Player.map.y
		Player.map.targetZ = Player.map.z
	End
	
	Method MovePlayer:Void(targetX:Int, targetY:Int, targetZ:int)
		Local x:bool
		Local y:Bool
		Local z:bool
		x = Player.map.x <> targetX
	  	y = Player.map.y <> targetY
		z = Player.map.z <> targetZ
		If x or y or z And Not Player.interaction.HasScript()
			If moveCounter = 0 And haveMoved = False And Not Player.interaction.HasScript()
				Player.map.x = Player.map.nextX
				Player.map.y = Player.map.nextY
				Player.map.z = Player.map.nextZ
				haveMoved = True
				Player.interaction.LoadScript("on_movement.ess")
				Scan()
			End
			If haveMoved And Not Player.interaction.HasScript()
				x = Player.map.x <> targetX
				y = Player.map.y <> targetY
				z = Player.map.z <> targetZ
				If x Or y Or z
					moveCounter = Player.ship.BurnFuel() * TIME_MULTIPLIER
					If moveCounter < 0 Then Return
					moveTime = moveCounter
					haveMoved = False
					GetNextStep(targetX, targetY, targetZ)
				End
			Else
				moveCounter -= 1
			End
		End
	End
	
	Method GetNextStep:Void(targetX:Int, targetY:Int, targetZ:int)
		Player.map.nextX = Player.map.x
		Player.map.nextY = Player.map.y
		Player.map.nextZ = Player.map.z
		Local x:bool
		Local y:Bool
		Local z:bool
		x = Player.map.x <> targetX
		y = Player.map.y <> targetY
		z = Player.map.z <> targetZ
		
		If not x And y And z
						
			If Player.map.nextY < targetY
				Player.map.nextY += 1
			Else
				Player.map.nextY -= 1
			End
						
			If Player.map.nextZ < targetZ
				Player.map.nextZ += 1
			Else
				Player.map.nextZ -= 1
			End
		Else If not y And x And z
			If Player.map.nextX < targetX
				Player.map.nextX += 1
			Else
				Player.map.nextX -= 1
			End
						
						
			If Player.map.nextZ < targetZ
				Player.map.nextZ += 1
			Else
				Player.map.nextZ -= 1
			End
		Else If Not z And x And y
					
			If Player.map.nextX < targetX
				Player.map.nextX += 1
			Else
				Player.map.nextX -= 1
			End
						
			If Player.map.nextY < targetY
				Player.map.nextY += 1
			Else
				Player.map.nextY -= 1
			End
		Else If x And Not y And Not z
			If Player.map.nextX < targetX
				Player.map.nextX += 1
			Else
				Player.map.nextX -= 1
			End
		Else If y And not x And Not z
			If Player.map.nextY < targetY
				Player.map.nextY += 1
			Else
				Player.map.nextY -= 1
			End
		Else If z And Not x And Not y
			If Player.map.nextZ < targetZ
				Player.map.nextZ += 1
			Else
				Player.map.nextZ -= 1
			End
		Else If x And y And z
			If (Player.map.nextX - targetX < 0) <> (Player.map.nextY - targetY < 0)
				If Player.map.nextX < targetX
					Player.map.nextX += 1
				Else
					Player.map.nextX -= 1
				End
				If Player.map.nextY < targetY
					Player.map.nextY += 1
				Else
					Player.map.nextY -= 1
				End
			Else If (Player.map.nextX - targetX < 0) <> (Player.map.nextZ - targetZ < 0)
				If Player.map.nextX < targetX
					Player.map.nextX += 1
				Else
					Player.map.nextX -= 1
				End
				If Player.map.nextZ < targetZ
					Player.map.nextZ += 1
				Else
					Player.map.nextZ -= 1
				End
			Else If (Player.map.nextX - targetY < 0) <> (Player.map.nextZ - targetZ < 0)
				If Player.map.nextX < targetX
					Player.map.nextX += 1
				Else
					Player.map.nextX -= 1
				End
				If Player.map.nextZ < targetZ
					Player.map.nextZ += 1
				Else
					Player.map.nextZ -= 1
				End
			Else
				Print "error"
			End
			
		End
	End
	
	Method GetCord:Cord(h:Hex, x:Int, y:int, z:int)
		Local c:Cord = New Cord()
		If x = 0 And y = 1 And z = -1
			c.x = h.xPos
			c.y = h.yPos - h.height
		Else If x = 0 And y = -1 And z = 1
			c.x = h.xPos
			c.y = h.yPos + h.height
		Else If x = 1 And y = 0 And z = -1
			c.x = h.xPos + (h.width * 0.75)
			c.y = h.yPos - (h.height / 2)
		Else If x = -1 And y = 0 And z = 1
			c.x = h.xPos - (h.width * 0.75)
			c.y = h.yPos + (h.height / 2)
		Else If x = 1 And y = -1 And z = 0
			c.x = h.xPos + (h.width * 0.75)
			c.y = h.yPos + (h.height / 2)
		Else If x = -1 And y = 1 And z = 0
			c.x = h.xPos - (h.width * 0.75)
			c.y = h.yPos - (h.height / 2)
		Else
			Print "ERROR"
		End
		Return c
	End	
	Method isDupe:Bool(x:Int, y:Int, z:int)
		For Local temp:Int = 0 Until hex.Length()
			If hex.Get(temp).isDupe(x, y, z) Return True
		Next
		Return False
	End
	
End

Class StoreType
	Const GOODS:String = "GOODS"
	Const PARTS:String = "PARTS"
	Const MODS:String = "MODS"
	Const BLACK_MARKET:String = "BLACK_MARKET"
End