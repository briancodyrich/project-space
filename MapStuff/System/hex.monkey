Strict

Import space_game

Class Hex
	Const SIZE:Float = 40
	Field x:int
	Field y:int
	Field z:int
	Field r:int
	Field width:Float = SIZE * 2
	Field height:Float = 0.86602 * SIZE * 2
	Field visible:bool
	Field xPos:Float
	Field yPos:Float
	Field img:Image
	Field verts:Float[14]
	Field isEdge:Bool
	
	Method New(x:Int, y:Int, z:int, c:Cord, r:int, isEdge:bool)
		Self.xPos = c.x
		Self.yPos = c.y
		Self.x = x
		Self.y = y
		Self.z = z
		Self.r = r
		Self.isEdge = isEdge
		For Local temp:Int = 0 Until 6
			verts[temp * 2] = xPos + (Cos(temp * 60) * SIZE)
			verts[temp * 2 + 1] = yPos - (Sin(temp * 60) * SIZE)
		Next
		verts[12] = verts[0]
		verts[13] = verts[1]
	End
	
	Method GetSlope:Float(x1:Float, y1:Float, x2:Float, y2:Float)
		Return ( (-y2 + y1) / (x2 - x1))
	End
	
	Method PointInHex:Bool(a:Int, b:Int, cameraX:Int, cameraY:int, centerX:int, centerY:int)
	
		If Distance(a, b, centerX, centerY) > HexField.RADIUS Return False
		If (a < verts[6] + cameraX)
			Return False
		End
		If (a > verts[0] + cameraX)
			Return False
		End
		If (b < verts[3] + cameraY)
			Return False
		End
		If (b > verts[9] + cameraY)
			Return False
		End
		If (GetSlope(verts[0] + cameraX, verts[1] + cameraY, verts[2] + cameraX, verts[3] + cameraY) * (a - (verts[0] + cameraX)) - (verts[1] + cameraY) + b < 0)
			Return False
		End
		If (GetSlope(verts[4] + cameraX, verts[5] + cameraY, verts[6] + cameraX, verts[7] + cameraY) * (a - (verts[4] + cameraX)) - (verts[5] + cameraY) + b < 0)
			Return False
		End
		If (GetSlope(verts[6] + cameraX, verts[7] + cameraY, verts[8] + cameraX, verts[9] + cameraY) * (a - (verts[6] + cameraX)) - (verts[7] + cameraY) + b > 0)
			Return False
		End
		If (GetSlope(verts[10] + cameraX, verts[11] + cameraY, verts[12] + cameraX, verts[13] + cameraY) * (a - (verts[12] + cameraX)) - (verts[13] + cameraY) + b > 0)
			Return False
		End
		Return True
	End
	
	Method Render:Void(cameraX:Int, cameraY:int, centerX:Int, centerY:int)
		If img = Null
			#If CONFIG="debug"
				ResetColorAlpha()
				Return
			#end
			If Distance(xPos + cameraX, yPos + cameraY, centerX, centerY) < HexField.RADIUS + Hex.SIZE
				For Local temp:Int = 0 Until verts.Length() -2 Step 2
					DrawLine(verts[temp] + cameraX, verts[temp + 1] + cameraY, verts[temp + 2] + cameraX, verts[temp + 3] + cameraY)
				Next
			End 
		End 
		ResetColorAlpha()
	End
	Method isDupe:Bool(x:Int, y:Int, z:int)
		If Self.x = x And Self.y = y And Self.z = z Then Return True
		Return False
	End
End

