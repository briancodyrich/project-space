Strict

Import space_game

Class HexObject
	Const PLANET:int = 1
	Const STAR:Int = 2
	Const POI:Int = 3
	Const ENEMY:Int = 4
	Const ASTEROID:Int = 5
	Const QUEST:Int = 6
	
	Const STORE_SIZE:Int = 90
	Const CREW_SIZE:Int = 5
	
	Const ACTION_POI:String = "Investigate"
	Const ACTION_DOCK:String = "Dock"
	Const ACTION_MINE:String = "Mine"
	Const ACTION_QUEST:String = "Quest"

	Field x:Int
	Field y:Int
	Field z:int
	Field xPos:Float
	Field yPos:Float
	Field type:int
	Field radius:int
	Field option:String
	Field ship:Ship
	Field store:Inventory
	Field asteroid:Asteroid
	Field callSign:String
	Field crew:Stack<Crew>
	Field quests:Stack<Quest>
	Field script:String
	Field visible:bool
	Field radar:bool
	Field name:String
	Field questid:int
	Field rgb:Int[]
	Method New(x:Int, y:Int, z:Int, xPos:Float, yPos:Float, type:int, ship:Ship = Null, store:Inventory = Null, asteroid:Asteroid = Null, crew:Stack<Crew> = Null, quests:Stack<Quest> = Null)
		Self.x = x
		Self.y = y
		Self.z = z
		Self.xPos = xPos
		Self.yPos = yPos
		Self.type = type	
		Self.ship = ship
		Self.crew = crew
		Self.store = store
		Self.asteroid = asteroid
		GetContext()
	End
	
	Method isProtected:Bool()
		If type = PLANET Or type = STAR Then Return True
		Return False
	End
	Method GetFilteredStore:Inventory(filter:ItemFilter)
		Local i:Inventory = New Inventory(STORE_SIZE)
		If store = Null Then Return i
		Local index:Int = 0
		For Local temp:int = 0 Until store.slots.Length()
			If store.slots[temp] <> Null
				If filter.canTouch(store.slots[temp].type)
					i.slots[index] = store.slots[temp]
					store.slots[temp] = Null
					index += 1
				End
			End
		Next
		Return i
	End
	Method ReturnStore:Void(i:Inventory)
		Print "return"
		For Local temp:Int = 0 Until i.slots.Length()
			If i.slots[temp] <> Null
				For Local xx:Int = 0 Until store.slots.Length()
					If store.slots[xx] = Null
						store.slots[xx] = i.slots[temp]
						i.slots[temp] = Null
						Exit
					End
				End
			End
		Next
		store.Sort(True, True)
	End
		
	Method OnSave:JsonObject()
		Local data:JsonObject = New JsonObject()
		Local pos:JsonArray = New JsonArray(5)
		pos.SetInt(0, x)
		pos.SetInt(1, y)
		pos.SetInt(2, z)
		pos.SetInt(3, type)
		If type = QUEST
			pos.SetInt(4, questid)
		Else
			If visible
				pos.SetInt(4, 1)
			Else
				pos.SetInt(4, 0)
			End
		End
		data.Set("pos", pos)
		
		If store <> Null
			data.Set("store", store.OnSave())
		End
		If asteroid <> Null
			data.Set("asteroid", asteroid.OnSave())
		End
		If crew <> Null And Not crew.IsEmpty()
			Local crewArr:JsonArray = New JsonArray(crew.Length())
			For Local temp:Int = 0 Until crew.Length()
				crewArr.Set(temp, crew.Get(temp).OnSave())
			Next
			data.Set("crew", crewArr)
		End
		If quests <> Null And Not quests.IsEmpty()
			Local qArr:JsonArray = New JsonArray(quests.Length())
			For Local temp:Int = 0 Until quests.Length()
				qArr.Set(temp, quests.Get(temp).OnSave())
			Next
			data.Set("quests", qArr)
		End
		If ship <> Null
			data.Set("ship", ship.OnSave())
		End
		If name.Length() > 0
			data.SetString("name", name)
		End
		Return data
	End
	
	Method OnLoad:Void(data:JsonObject, grid:Stack<Hex>)
		Local pos:JsonArray = JsonArray(data.Get("pos"))
		Self.x = pos.GetInt(0)
		Self.y = pos.GetInt(1)
		Self.z = pos.GetInt(2)
		For Local temp:Int = 0 Until grid.Length
			If grid.Get(temp).isDupe(x, y, z)
				xPos = grid.Get(temp).xPos
				yPos = grid.Get(temp).yPos
				Exit
			End
		Next
		name = data.GetString("name", "")
		Self.type = pos.GetInt(3)
		If type = QUEST
			questid = pos.GetInt(4)
		Else
			If pos.GetInt(4) = 1
				visible = True
			End
		End
		Local obj:JsonObject = JsonObject(data.Get("store"))
		If obj <> Null
			store = New Inventory(HexObject.STORE_SIZE)
			store.OnLoad(obj)
		End
		If data.Contains("crew")
			crew = New Stack<Crew>()
			Local crewArr:JsonArray = JsonArray(data.Get("crew"))
			If crewArr.Length() > 0
				For Local count:Int = 0 Until crewArr.Length()
					Local c:Crew = New Crew(0, 0, Null, Null)
					c.OnLoad(JsonObject(crewArr.Get(count)))
					crew.Push(c)
				Next
			End
		End
		If data.Contains("quests")
			quests = New Stack<Quest>()
			Local qArr:JsonArray = JsonArray(data.Get("quests"))
			If qArr.Length() > 0
				For Local temp:Int = 0 Until qArr.Length()
					Local o:JsonObject = JsonObject(qArr.Get(temp))
					Local d:JsonObject = QuestLoader.GetQuest(o.GetString("name"))
					If d <> Null
						quests.Push(New Quest(d))
					End
				Next
			End
		End
		obj = JsonObject(data.Get("ship"))
		If obj <> Null
			ship = New Ship(1220, 100, True)
			ship.OnLoad(obj)
		End
		obj = JsonObject(data.Get("asteroid"))
		If obj <> Null
			asteroid = New Asteroid(1220, 100, False, obj)
		End
		GetContext()
	End
	Method GetContext:Void()
		Select type
			Case STAR
				radar = False
				visible = True
				radius = (Hex.SIZE * 3) - 20
				rgb =[255, 255, 0]
			Case PLANET
				option = ACTION_DOCK
				visible = True
				radar = True
				radius = Hex.SIZE - 20
				rgb =[0, 0, 255]
			Case ASTEROID
				option = ACTION_MINE
				radar = True
				radius = Hex.SIZE - 35
				rgb =[110, 110, 110]
			Case POI
				option = ACTION_POI
				radar = True
				radius = Hex.SIZE - 35
				rgb =[0, 255, 255]
			Case QUEST
				option = ACTION_QUEST
				radar = True
				visible = True
				radius = Hex.SIZE - 35
				rgb =[255, 0, 255]
			Case ENEMY
				option = ""
			Default
				option = ""
		End
	
	End
	
	Method GetReg:String()
		Local cArr:int[] =[45, int(Rnd(65, 91)), int(Rnd(65, 91)), int(Rnd(65, 91)), int(Rnd(65, 91)), int(Rnd(48, 58)), int(Rnd(48, 58))]
		Return String.FromChars(cArr)
	End
	
	Method Render:Void(cameraX:Int, cameraY:Int, centerX:Int, centerY:int)
	'	If Game.Cursor.Down()
		#rem
			Local mx:Int = Game.Cursor.x()
			Local my:Int = Game.Cursor.y()
			Local dist:float = Distance(mx, my, centerX, centerY)
			Local theta:float = ATan2(mx - centerX, my - centerY)
			Print "dist: " + dist + " theta: " + theta
			SetColor(255, 0, 0)
			DrawLine(centerX, centerY, centerX + (Sin(theta) * dist), centerY + (Cos(theta) * dist))
			ResetColorAlpha()
		#end
	'	End
		If visible
			Local dist:float = Distance(xPos + cameraX, yPos + cameraY, centerX, centerY)
			If radar
				If dist >= HexField.RADIUS - radius - 3
					Local theta:float = ATan2( (xPos + cameraX) - centerX, (yPos + cameraY) - centerY)
					Local nx:Int = centerX + (Sin(theta) * (HexField.RADIUS - radius - 3))
					Local ny:int = centerY + (Cos(theta) * (HexField.RADIUS - radius - 3))
					SetColor(rgb[0], rgb[1], rgb[2])
					DrawCircle(nx, ny, radius)
				'	DrawLine(centerX, centerY, centerX + (Sin(theta) * dist), centerY - (Cos(theta) * dist))
					ResetColorAlpha()
					Return
				End
			End

			If dist < HexField.RADIUS + radius
				SetColor(rgb[0], rgb[1], rgb[2])
				DrawCircle(xPos + cameraX, yPos + cameraY, radius)
				ResetColorAlpha()
			End
			#rem
			If type = STAR
				Local theta:float = ATan2(centerX - (xPos + cameraX), centerY - (yPos + cameraY))
				Print theta
				SetColor(255, 0, 0)
				DrawLine(centerX, centerY, centerX + (Cos(theta) * dist), centerY + (Sin(theta) * dist))
			End
			#end
		End
			
			#rem
			Local dist:Float = Distance(cx + cameraX, cy + cameraY, centerX, centerY)
			If type = STAR
				If dist < HexField.RADIUS + ( (Hex.SIZE * 3) - 20)
					SetColor(255, 255, 0)
					DrawCircle(cx + cameraX, cy + cameraY, radius)
				End
			End
			If type = ASTEROID
				If dist < HexField.RADIUS + ( (Hex.SIZE) - 35)
					SetColor(110, 110, 110)
					DrawCircle(cx + cameraX, cy + cameraY, (Hex.SIZE) - 35)
				Else
					RenderRadar(cameraX, cameraY, centerX, centerY)
				End
			End
			If type = PLANET
				If dist < HexField.RADIUS + ( (Hex.SIZE) - 20)
					SetColor(0, 0, 255)
					DrawCircle(cx + cameraX, cy + cameraY, (Hex.SIZE) - 20)
				Else
					RenderRadar(cameraX, cameraY, centerX, centerY)
				End
			End
			If type = POI
				If dist < HexField.RADIUS + ( (Hex.SIZE) - 35)
					SetColor(0, 255, 255)
					DrawCircle(cx + cameraX, cy + cameraY, (Hex.SIZE) - 35)
				Else
					RenderRadar(cameraX, cameraY, centerX, centerY)
				End
			End
			#end
	End
	Method RenderRadar:Void(cameraX:Int, cameraY:Int, centerX:Int, centerY:int, dist:float)
		Local cx:int = xPos + cameraX
		Local cy:int = yPos + cameraY
		Local theta:float = ATan2(cx - centerX, cy - centerY)
		If type = ASTEROID
			Local d:float = HexField.RADIUS - Hex.SIZE + 35
			SetColor(110, 110, 110)
			DrawCircle(centerX + (Cos(theta) * d), centerY + (Sin(theta) * d), Hex.SIZE - 35)
		End
		If type = PLANET
			Local d:float = HexField.RADIUS - Hex.SIZE + 20
			SetColor(0, 0, 255)
			DrawCircle(centerX + (Sin(theta) * d), centerY + (Cos(theta) * d), Hex.SIZE - 20)
		End
		If type = POI
			#rem
			If Distance(cx + cameraX, cy + cameraY, centerX, centerY) < HexField.RADIUS + ( (Hex.SIZE) - 35)
				SetColor(0, 255, 255)
				DrawCircle(cx + cameraX, cy + cameraY, (Hex.SIZE) - 35)
			Else
				RenderRadar(cameraX, cameraY, centerX, centerY)
			End
			#end
		End
	End
End
#rem
Function GetCallSign:String(type:Int)
	Const CALL_SIGN_UNKNOWN:String = "???"
	Const CALL_SIGN_PIRATE:String = "SKBO"
	Const CALL_SIGN_POLICE:String = "UPDF"
	Const CALL_SIGN_TRADER:String = "AAGT"
End
#end