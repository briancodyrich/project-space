Strict

Import space_game

Class Asteroid
	Field x_offset:Int
	Field y_offset:Int
	Field mirrored:Bool
	Field tiles:int[] = New Int[Ship.MAX_X * Ship.MAX_Y]
	
	Method New(x_offest:Int, y_offset:Int, mirrored:bool, rate:float)
		Self.x_offset = x_offest
		Self.y_offset = y_offset
		Self.mirrored = mirrored
		Local count:Int = 120 * rate
		Local x:Int
		Local y:Int
		tiles[10 * Ship.MAX_X + 10] = Rnd(2, 5)
		While count > 0
			x = Rnd(Ship.MAX_X)
			y = Rnd(Ship.MAX_X)
			If isValid(x, y) And isEmpty(x, y) And hasNeighbors(x, y)
				tiles[x * Ship.MAX_X + y] = Rnd(2, 5)
				count -= 1
			End
		Wend
	End
	
	Method New(x_offest:Int, y_offset:Int, mirrored:bool, data:JsonObject)
		Self.x_offset = x_offest
		Self.y_offset = y_offset
		Self.mirrored = mirrored
		OnLoad(data)
	End
	
	Method OnSave:JsonObject()
		Local iStack:Stack<Int> = New Stack<Int>()
		Local data:JsonObject = New JsonObject()
		For Local temp:Int = 0 Until tiles.Length()
			If tiles[temp] > 0
				iStack.Push(temp)
				iStack.Push(tiles[temp])
			End
		Next
		Local tArr:JsonArray = New JsonArray(iStack.Length())
		For Local temp:Int = 0 Until iStack.Length()
			tArr.SetInt(temp, iStack.Get(temp))
		Next
		
		data.Set("tiles", tArr)
		Return data
	End
	
	Method OnLoad:Void(data:JsonObject)
		Local rockArr:JsonArray = JsonArray(data.Get("tiles"))
		For Local count:Int = 0 Until rockArr.Length() Step 2
			tiles[rockArr.GetInt(count)] = rockArr.GetInt(count + 1)
		End
	End
	
	Method isDead:Bool()
		For Local temp:Int = 0 Until tiles.Length()
			If tiles[temp] > 0 Then Return False
		End
		Return True
	End
	
	Method Render:Void()
		PushMatrix()
		
		Translate(x_offset + MirrorOffset(), y_offset)
		If mirrored Then Transform(-1, 0, 0, 1, 0, 0)
	
		
		For Local x:Int = 0 Until Ship.MAX_X
			For Local y:Int = 0 Until Ship.MAX_Y
				If tiles[x * Ship.MAX_X + y] > 0
					DrawImage(Tile.img_rock, (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2, (y * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2)
				End
			Next
		Next

		
		ResetColorAlpha()
		
	
		PopMatrix()
	End
	
	Method MirrorOffset:Int()
		If mirrored Then Return Ship.MAX_X * Ship.GRID_SIZE
		Return 0
	End
	
	Method isMouseOver:Bool(x:Int, y:Int, x_offset:Int, y_offset:int, mirrored:bool)
		If mirrored
			If Game.Cursor.x() > (x_offset + (Ship.MAX_X * Ship.GRID_SIZE) - (x * 30) - 30) And Game.Cursor.x() < (x_offset + (Ship.MAX_X * Ship.GRID_SIZE) - (x * 30)) And Game.Cursor.y() > (y_offset + (y * 30)) And Game.Cursor.y() < (y_offset + (y * 30) + 30) Then Return True
		Else
			If Game.Cursor.x() > (x_offset + (x * 30)) And Game.Cursor.x() < (x_offset + (x * 30) + 30) And Game.Cursor.y() > (y_offset + (y * 30)) And Game.Cursor.y() < (y_offset + (y * 30) + 30) Then Return True
		End
		Return False
	End
	Method isPointOver:Bool(x:Int, y:Int, x_offset:Int, y_offset:int, mirrored:bool, pointX:Int, pointY:int)
		If mirrored
			If pointX > (x_offset + (Ship.MAX_X * Ship.GRID_SIZE) - (x * 30) - 30) And pointX < (x_offset + (Ship.MAX_X * Ship.GRID_SIZE) - (x * 30)) And pointY > (y_offset + (y * 30)) And pointY < (y_offset + (y * 30) + 30) Then Return True
		Else
			If pointX > (x_offset + (x * 30)) And pointX < (x_offset + (x * 30) + 30) And pointY > (y_offset + (y * 30)) And pointY < (y_offset + (y * 30) + 30) Then Return True
		End
		Return False
	End
	Method isValid:Bool(x:Int, y:Int)
		If x <= 0 Return False
		If y <= 0 Return False
		If x >= Ship.MAX_X - 1 Return False
		If y >= Ship.MAX_X - 1 Return False
		Return True
	End
	Method isEmpty:Bool(x:Int, y:Int)
		If tiles[x * Ship.MAX_X + y] = 0 Then Return True
		Return False
	End
	Method hasNeighbors:Bool(x:Int, y:Int)
		If tiles[ (x + 1) * Ship.MAX_X + y] > 0 Then Return True
		If tiles[ (x - 1) * Ship.MAX_X + y] > 0 Then Return True
		If tiles[x * Ship.MAX_X + (y + 1)] > 0 Then Return True
		If tiles[x * Ship.MAX_X + (y - 1)] > 0 Then Return True
		Return False
	End
End