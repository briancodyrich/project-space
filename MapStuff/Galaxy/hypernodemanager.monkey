Strict

Import space_game

Class HyperNodeManager Implements Loadable
	Const MIN_RANGE:int = 200
	Const MAX_RANGE:Int = 300
	Const CONNECTION_RANGE:Int = 300
	Const NEIGHBORHOOD_RANGE:Int = 1000
	Const NODE_COUNT:Int = 2500
	Const TIMEOUT:Int = 100
	Const SHOW_TIER_COLOR:Bool = False
	Const SHOW_ENTIRE_MAP:Bool = True
	Const LEVEL_RAD:Int = 140 'old 1500
	Const MAX_TIER:Int = 5
	
	Const INTMAX:Int = 2147483647
	Field nodes:Stack<HyperNode>
	Field path:Stack<Int>
	Field genDone:Bool

	Global operationCounter:int
	Method New()
		nodes = New Stack<HyperNode>()
		path = New Stack<Int>
	End
	Method Start:Void()
		nodes.Push(New HyperNode(ScreenWidth / 2, ScreenHeight / 2, HyperNode.VISITED))
		GenerateNodes(0)
		VisitNode(0)
	End
	Method Bulid:Void()
		If nodes.Length() = 0 Then Return
		If nodes.Length() = NODE_COUNT
			genDone = True
		End
		While nodes.Length() < NODE_COUNT
			Local r:Int = Rnd(nodes.Length())
			If nodes.Get(r).status = HyperNode.VISIBLE
				VisitNode(r)
				Return
			Else
				Local found:Bool = False
				While r < nodes.Length() -1
					r += 1
					If nodes.Get(r).status = HyperNode.VISIBLE
						VisitNode(r)
						Return
					End
				Wend
				If found = True Then Continue
				For Local temp:Int = 1 Until nodes.Length()
					If nodes.Get(temp).status = HyperNode.VISIBLE
						VisitNode(temp)
						Return
					End
				Next
				If found = False
					Return
				End
			End
		Wend
	End
	
	Method Update:Void()
		For Local temp:Int = 0 Until nodes.Length()
			nodes.Get(temp).economy.Update(Economy.STAGE_ONE)
		Next
		'buy metal
		For Local homeNode:HyperNode = EachIn nodes
			If homeNode.economy.getNeeds(0) < 0
				For Local temp:Int = 0 Until homeNode.edges.Length()
					Local testNode:HyperNode = nodes.Get(homeNode.edges.Get(temp).node)
					If testNode.economy.getNeeds(0) > 0
						Local max:Float = Min(Abs(homeNode.economy.getNeeds(0)), testNode.economy.getNeeds(0))
						homeNode.economy.Buy(0, max, 1.0)
						testNode.economy.Sell(0, max, 1.0)
					End
				Next
			End
		Next
		For Local temp:Int = 0 Until nodes.Length()
			nodes.Get(temp).economy.Update(Economy.STAGE_TWO)
		Next
		For Local homeNode:HyperNode = EachIn nodes
			For Local good_type:Int = 1 Until Economy.MAX_TYPES
				If homeNode.economy.getNeeds(good_type) < 0
					For Local temp:Int = 0 Until homeNode.edges.Length()
						Local testNode:HyperNode = nodes.Get(homeNode.edges.Get(temp).node)
						If testNode.economy.getNeeds(good_type) > 0
							Local max:Float = Min(Abs(homeNode.economy.getNeeds(good_type)), testNode.economy.getNeeds(good_type))
							homeNode.economy.Buy(good_type, max, 1.0)
							testNode.economy.Sell(good_type, max, 1.0)
						End
					Next
				End
			Next	
			'nodes.Get(temp).economy.Update(Economy.STAGE_RAW_MATERIAL)
		Next
		For Local temp:Int = 0 Until nodes.Length()
			nodes.Get(temp).economy.Update(Economy.STAGE_THREE)
		Next
		
	End
	
	Method OnSave:JsonObject()
		Local obj:JsonObject = New JsonObject
		Local nodeArr:JsonArray = New JsonArray(nodes.Length())
		For Local temp:Int = 0 Until nodes.Length()
			nodeArr.Set(temp, nodes.Get(temp).OnSave())
		Next
		obj.Set("Nodes", nodeArr)
		Return obj
	End
	
	Method OnLoad:Void(data:JsonObject)
		Local gArr:JsonArray = JsonArray(data.Get("Nodes"))
		nodes = New Stack<HyperNode>()
		For Local temp:Int = 0 Until gArr.Length()
			Local node:HyperNode = New HyperNode(JsonArray(gArr.Get(temp)))
			nodes.Push(node)
		Next
		SetLinks(CONNECTION_RANGE)
		If nodes.Length() >= NODE_COUNT
			genDone = True
		End
	End
	
	Method FileName:String()
		Return "_galaxy.json"
	End
	Method GenerateNodes:Void(parent:int)
		'	Print "gen: " + parent
		Local pNode:HyperNode = nodes.Get(parent)
		If pNode.status = HyperNode.UNKNOWN Then Return
		Local ne:Stack<Int> = GetNodesInRange(parent)
		Local reTest:Stack<Int> = New Stack<Int>
		Local temp:Int = 0
		While pNode.edges.Length() < pNode.min_connections
			For temp = 0 To TIMEOUT
				Local dist:Int = Rnd(MIN_RANGE, MAX_RANGE)
				Local theta:Float = Rnd(6) * (2 * PI)
				Local tx:Int = pNode.x + (dist * Cosr(theta))
				Local ty:Int = pNode.y + (dist * Sinr(theta))
				Local fail:Bool = False
				For Local t2:Int = 0 Until ne.Length()
					If nodes.Get(ne.Get(t2)).failsPlacementTest(tx, ty)
						fail = True
						Exit
					End
				Next
				If Not fail
					Local n:HyperNode = New HyperNode(tx, ty, pNode.status - 1)
					nodes.Push(n)
					reTest.Push(nodes.Length() -1)
					ne.Push(nodes.Length() -1)
					GetConnections(ne)
				End
			Next
			If temp > TIMEOUT Then Exit
			
		End
		For Local z:Int = 0 Until reTest.Length()
			GenerateNodes(reTest.Get(z))
		Next
	End
	
	Method GetConnections:Void(l:Stack<Int>)
		For Local x:Int = 0 Until l.Length()
			For Local y:Int = x + 1 Until l.Length
				Local d:Int = Distance(nodes.Get(l.Get(x)).x, nodes.Get(l.Get(x)).y, nodes.Get(l.Get(y)).x, nodes.Get(l.Get(y)).y)
				If d < CONNECTION_RANGE
					If not nodes.Get(l.Get(x)).hasEdge(y) Then nodes.Get(l.Get(x)).AddEdge(l.Get(y), d)
					If not nodes.Get(l.Get(y)).hasEdge(l.Get(x)) Then nodes.Get(l.Get(y)).AddEdge(l.Get(x), d)
				End
			Next
		Next
	End
	
	Method GetNodesInRange:Stack<Int>(index:Int)
		Local s:Stack<Int> = New Stack<Int>()
		Local cx:int = nodes.Get(index).x
		Local cy:int = nodes.Get(index).y
		For Local temp:Int = 0 Until nodes.Length()
			If Abs(nodes.Get(temp).x - cx) < NEIGHBORHOOD_RANGE And Abs(nodes.Get(temp).y - cy) < NEIGHBORHOOD_RANGE
				s.Push(temp)
			End
		Next
		Return s
	End
	#rem
	Method BuildMap:Void()
	If nodes.Length() >= NODE_COUNT
	If Not genDone
	SetLinks(CONNECTION_RANGE)
	genDone = True
	Else
	Return
	End
	End
		
	Local l:int = nodes.Length()
	Local max:Int = nodes.Length() +1
	While l < Min(max, NODE_COUNT)
	Local dist:Int = Rnd(MIN_RANGE, MAX_RANGE)
	Local theta:Float = Rnd(6) * (2 * PI)
	Local ref:Int = Rnd(nodes.Length() / 10, nodes.Length())
	Local tx:Int = nodes.Get(ref).x + (dist * Cosr(theta))
	Local ty:Int = nodes.Get(ref).y + (dist * Sinr(theta))
	Local found:bool = True
	For Local temp:Int = 0 Until nodes.Length()
	operationCounter += 1
	If Distance(tx, ty, nodes.Get(temp).x, nodes.Get(temp).y) < MIN_RANGE
	found = False
	temp = nodes.Length()
	End
	Next
	If found
	nodes.Push(New HyperNode(tx, ty))
	l = nodes.Length()
	End
	Wend
	'SetLinks(CONNECTION_RANGE)
	End
	#end
	
	Method GetCost:Int()
		If path = Null Or path.IsEmpty() Then Return 0
		Local last:int = path.Length() -1
		Local a:HyperNode = nodes.Get(path.Get(last - 1))
		Local b:HyperNode = nodes.Get(path.Get(last))
		Return a.cost + Distance(a.x, a.y, b.x, b.y)
	End
	
	Method FindPath:Void(start:Int, target:Int)
		path.Clear()
		For Local temp:Int = 0 Until nodes.Length()
			nodes.Get(temp).cost = INTMAX
			nodes.Get(temp).checked = False
		Next
		nodes.Get(start).checked = True
		nodes.Get(start).cost = 0
		Local current:Int = start
		Repeat
			For Local temp:Int = 0 Until nodes.Get(current).edges.Length()
				operationCounter += 1
				If nodes.Get(current).edges.Get(temp).node = target
					SetPath(start, target)
					Return
				End
				If nodes.Get(current).cost + nodes.Get(current).edges.Get(temp).weight < nodes.Get(nodes.Get(current).edges.Get(temp).node).cost
					nodes.Get(nodes.Get(current).edges.Get(temp).node).cost = nodes.Get(current).cost + nodes.Get(current).edges.Get(temp).weight
				End
			End
			Local cost:Int = INTMAX
			Local n:Int = -1
			For Local cycle:Int = 0 Until nodes.Length()
				If nodes.Get(cycle).checked = False And nodes.Get(cycle).cost < cost
					n = cycle
					cost = nodes.Get(cycle).cost
				End
			Next
			If cost = INTMAX
				Return
			End
			nodes.Get(n).checked = True
			current = n
		Forever
	End
	
	Method SetPath:Void(start:Int, target:int)
		path.Insert(0, target)
		While start <> target
			Local c:Int = nodes.Get(target).cost
			Local n:Int = -1
			For Local temp:Int = 0 Until nodes.Get(target).edges.Length()
				If nodes.Get(nodes.Get(target).edges.Get(temp).node).cost < c
					n = nodes.Get(target).edges.Get(temp).node
					c = nodes.Get(nodes.Get(target).edges.Get(temp).node).cost
				End
			Next
			target = n
			path.Insert(0, target)
		Wend
	End
	
	Method SetLinks:Void(r:int)

		For Local temp:Int = 0 Until nodes.Length()
			nodes.Get(temp).edges.Clear()
		Next
		For Local x:Int = 0 Until nodes.Length()
			For Local y:Int = 0 Until nodes.Length()
				If x <> y
					Local d:Int = Distance(nodes.Get(x).x, nodes.Get(x).y, nodes.Get(y).x, nodes.Get(y).y)
					If d < r
						nodes.Get(x).AddEdge(y, d)
					End
				End
			Next
		Next
		SetVisibility()
	End
	
	Method SetVisibility:Void()
		#rem
		For Local temp:Int = 0 Until nodes.Length()
		If nodes.Get(temp).
		nodes.Get(temp).visible = True
		For Local t2:Int = 0 Until nodes.Get(temp).edges.Length()
		nodes.Get(nodes.Get(temp).edges.Get(t2).node).visible = True
		Next
		End
		Next
		BuildMissingSystems()
		#end
	End
	
	Method VisitNode:Void(index:Int)
		nodes.Get(index).status = HyperNode.VISITED
		Local n:HyperNode = nodes.Get(index)
		For Local temp:Int = 0 Until n.edges.Length()
			Local testN:Int = n.edges.Get(temp).node
			Local nc:HyperNode = nodes.Get(testN)
			If nc.status = HyperNode.UNKNOWN
				nc.status = HyperNode.VISIBLE
				GenerateNodes(testN)
			End
		Next
	End
	
	Method BuildMissingSystems:Void()
		#rem
		For Local temp:Int = 0 Until nodes.Length()
		If (nodes.Get(temp).visible)
		If isMissingSaveData(temp)
		BuildSystem(temp)
		End
		End
		Next
		#end
	End
	
	Method GetSystemData:SystemType(index:Int)
		If isMissingSaveData(index)
			Return BuildSystem(index)
		Else
			Return LoadSystemSave(index)
		End
	End
	
	Method BuildSystem:SystemType(index:Int)
		Local sdat:SystemType = New SystemType(index, nodes.Get(index).level)
		If Player.nameGen <> Null
			sdat.NameSystem(Player.nameGen.GetName(index))
		End
		Local sys:HexField = New HexField(sdat)
		sys.BuildSystem()
		Loader.SaveGame(Player.accountName, sys)
		Loader.SaveGame(Player.accountName, sdat)
		Return sdat
	End
	
	Method LoadSystemSave:SystemType(index:Int)
		Local sdat:SystemType = New SystemType(index, 0)
		Loader.LoadGame(Player.accountName, sdat)
		Return sdat
	End
	
	Method isMissingSaveData:Bool(index:Int)
		Return not (Loader.FileExists(Player.accountName, index + "_map.json") And Loader.FileExists(Player.accountName, index + "_core.json"))
	End

	
	Method Render:Void(one:Int, two:int, xoff:Int, yoff:int)
		PushMatrix
		Translate(xoff, -yoff)
		
		For Local temp:Int = 0 Until nodes.Length()
			If ( Not nodes.Get(temp).status = HyperNode.UNKNOWN) And ( Not SHOW_ENTIRE_MAP) Then Continue
			If SHOW_TIER_COLOR
				Select(nodes.Get(temp).level / 10)
					Case 0
						SetColor(255, 0, 0)
					Case 1
						SetColor(255, 128, 0)
					Case 2
						SetColor(255, 255, 0)
					Case 3
						SetColor(0, 255, 0)
					Case 4
						SetColor(0, 0, 255)
					Case 5
						SetColor(255, 0, 255)
					Default
						SetColor(255, 255, 255)
				End
			Else
				
	
				SetColor(255, 255, 255)
			End
			For Local ec:Int = 0 Until nodes.Get(temp).edges.Length()
				DrawLine(nodes.Get(temp).x, nodes.Get(temp).y, nodes.Get(nodes.Get(temp).edges.Get(ec).node).x, nodes.Get(nodes.Get(temp).edges.Get(ec).node).y)
			Next
			ResetColorAlpha()
		Next
		
		For Local temp:Int = 0 Until nodes.Length()
			If temp = one Or temp = two
				nodes.Get(temp).Render(True)
			Else
				nodes.Get(temp).Render()
			End
		Next
		SetColor(255, 0, 0)
		For Local temp:Int = 0 Until path.Length() -1
			DrawLine(nodes.Get(path.Get(temp)).x, nodes.Get(path.Get(temp)).y, nodes.Get(path.Get(temp + 1)).x, nodes.Get(path.Get(temp + 1)).y)
		Next
		SetColor(255, 255, 255)
		PopMatrix
	End
End