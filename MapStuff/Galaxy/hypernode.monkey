Strict

Import space_game

Class HyperNode
	Const SIZE:Int = 30
	Const VISITED:Int = 2
	Const VISIBLE:Int = 1
	Const UNKNOWN:Int = 0
	Field x:int
	Field y:Int
	Field edges:Stack<Edge>
	Field level:int
	Field cost:int
	Field checked:Bool
	Field status:int
	Field economy:Economy
	Field max_connections:Int
	Field min_connections:int
	
	Method New(x:Int, y:Int, status:int)
		Self.x = x
		Self.y = y
		Self.status = status
		min_connections = 2 + Rnd(3)
		max_connections = min_connections + Rnd(3)
		economy = New Economy()
		edges = New Stack<Edge>()
		level = Distance(x, y, ScreenWidth / 2, ScreenHeight / 2) / HyperNodeManager.LEVEL_RAD
	End
	Method New(data:JsonArray)
		edges = New Stack<Edge>()
		x = data.GetInt(0)
		y = data.GetInt(1)
		status = data.GetInt(2)
		economy = New Economy(JsonArray(data.Get(3)))
		level = Distance(x, y, ScreenWidth / 2, ScreenHeight / 2) / HyperNodeManager.LEVEL_RAD
	End
	Method AddEdge:Void(node:Int, weight:int)
		edges.Push(New Edge(node, weight))
	End
	
	Method hasEdge:Bool(index:Int)
		For Local temp:Int = 0 Until edges.Length()
			If index = edges.Get(temp).node Then Return True
		Next
		Return False
	End

	
	Method failsPlacementTest:Bool(tx:Int, ty:int)
		Local d:Int = Distance(x, y, tx, ty)
		If d < HyperNodeManager.MIN_RANGE Then Return True
		If d > HyperNodeManager.MAX_RANGE Then Return False ' don't care condition
		If edges.Length() >= max_connections Then Return True
		Return False
	End
	Method OnSave:JsonArray()
		'Local e:JsonArray = New JsonArray( (edges.Length() * 2) + 4)
		Local e:JsonArray = New JsonArray(4)
		e.SetInt(0, x)
		e.SetInt(1, y)
		e.SetInt(2, status)
		e.Set(3, economy.OnSave())
		
		'We are having the game rebuild this each time instead of saving it
		#rem 
		For Local temp:Int = 0 Until edges.Length()
			e.SetInt( (temp * 2) + 4, edges.Get(temp).node)
			e.SetInt( (temp * 2) + 5, edges.Get(temp).weight)
		Next
		#end
		Return e
	End
	
	Method Render:Void(red:bool = False)
		SetColor(0, 0, 0)
		DrawCircle(x, y, SIZE)
		If red
			SetColor(0, 0, 255)
		Else
			If status = VISITED
				SetColor(0, 255, 0)
			End
			If status = VISIBLE
				SetColor(255, 255, 0)
			End
			If status = UNKNOWN
				SetColor(255, 0, 0)
			End
		End
		If HyperNodeManager.SHOW_TIER_COLOR
			Select(level / 10)
				Case 0
					SetColor(255, 0, 0)
				Case 1
					SetColor(255, 128, 0)
				Case 2
					SetColor(255, 255, 0)
				Case 3
					SetColor(0, 255, 0)
				Case 4
					SetColor(0, 0, 255)
				Case 5
					SetColor(255, 0, 255)
				Default
					SetColor(255, 255, 255)
			End
		End
		#If CONFIG="debug"
			DrawArc(x, y, SIZE, SIZE, 0, 360, 3)
		#Else
			DrawArc(x, y, SIZE, SIZE, 0, 360, 30)
		#End
		
		ResetColorAlpha()
	End
	
	Method isMouseOver:Bool(px:Int, py:Int, xOff:Int, yOff:int)
		If Not (status > UNKNOWN or HyperNodeManager.SHOW_ENTIRE_MAP) Then Return False
		If Distance(x, y, px - xOff, py + yOff) < SIZE Then Return True
		Return False
	End
End

Class Edge
	Field node:int
	Field weight:int
	Method New(node:Int, weight:Int)
		Self.node = node
		Self.weight = weight
	End
	
End
