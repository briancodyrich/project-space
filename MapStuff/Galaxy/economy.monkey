Strict

Import space_game

Class Economy
	Const METAL:Int = 0
	Const PARTS:Int = 1
	Const FOOD:Int = 2
	Const GOODS:Int = 3
	Const MAX_TYPES:Int = 4
	
	Const MAX:Float = 1.0
	Const BACKUP:Float = 0.2
	
	Const STAGE_ONE:Int = 1
	Const STAGE_TWO:Int = 2
	Const STAGE_THREE:Int = 3
	Const BASE:Float = 2.5
	Const TRADE_MULT:float = 1.5
	Field technology:float
	Field industry:float
	Field warfare:float
	Field resources:float
	Field agriculture:float
	Field population:float
	Field goods:Float[4]
	Field trade:Float[4]
	Field useage:float
	
	Field food:Item
	Field fuel:Item
	Field antimatter:Item
	
	Method New()
		technology = GetRandom(2)
		industry = GetRandom(2)
		warfare = GetRandom(2)
		resources = GetRandom(2)
		agriculture = GetRandom(2)
		population = GetRandom(3)
		food = New Item(ItemLoader.GetItem("Supplies"))
		fuel = New Item(ItemLoader.GetItem("Fuel"))
		antimatter = New Item(ItemLoader.GetItem("Antimatter"))
	End
	
	Method New(data:JsonArray)
		technology = data.GetFloat(0)
		industry = data.GetFloat(1)
		warfare = data.GetFloat(2)
		resources = data.GetFloat(3)
		agriculture = data.GetFloat(4)
		population = data.GetFloat(5)
		goods[0] = data.GetFloat(6)
		goods[1] = data.GetFloat(7)
		goods[2] = data.GetFloat(8)
		goods[3] = data.GetFloat(9)
		food = New Item(ItemLoader.GetItem("Supplies"))
		fuel = New Item(ItemLoader.GetItem("Fuel"))
		antimatter = New Item(ItemLoader.GetItem("Antimatter"))
	End
	
	Method OnSave:JsonArray()
		Local e:JsonArray = New JsonArray(10)
		e.SetFloat(0, technology)
		e.SetFloat(1, industry)
		e.SetFloat(2, warfare)
		e.SetFloat(3, resources)
		e.SetFloat(4, agriculture)
		e.SetFloat(5, population)
		e.SetFloat(6, goods[0])
		e.SetFloat(7, goods[1])
		e.SetFloat(8, goods[2])
		e.SetFloat(9, goods[3])
		Return e
	End
	Method GetSingleItemValue:Int(type:Int)
		If type = Item.FOOD
			Return food.cost * GetPrice(Item.FOOD)
		End
		If type = Item.FUEL
			Return fuel.cost * GetPrice(Item.FUEL)
		End
		If type = Item.ANTIMATER
			Return antimatter.cost * GetPrice(Item.ANTIMATER)
		End
		Return 0
	End
	Method GetPrice:float(type:Int)
		If type >= Item.REACTOR And type < Item.FUEL
			Return GetPrice(goods[PARTS], trade[PARTS])
		End
		If type = Item.METAL
			Return GetPrice(goods[METAL], trade[METAL])
		End
		If type = Item.FUEL Or type = Item.ANTIMATER
			Return GetPrice(goods[GOODS], trade[GOODS])
		End
		If type = (Item.FOOD)
			Return GetPrice(goods[FOOD], trade[FOOD])
		End
		Return 1.0
	End
	Method GetPrice:Float(items:Float, trade:float)
		Return Pow(BASE, (trade * TRADE_MULT) - (items - BACKUP))
	End
	
	Method Update:Void(stage:int)
		If stage = STAGE_ONE
			useage = Rnd(population * 0.3) + (population * 0.8)
			Reset()
			Add(METAL, resources)
			Add(GOODS, industry)
			Add(FOOD, agriculture)
		Else If stage = STAGE_TWO
			Convert(METAL, PARTS, technology, 1.0)
		Else If stage = STAGE_THREE
			Remove(PARTS, useage)
			Remove(FOOD, useage)
			Remove(GOODS, useage)
		End
	End
	Method getNeeds:float(type:int)
		Select type
			Case METAL
				Return goods[METAL] - technology - BACKUP
			Case PARTS
				Return goods[PARTS] - useage - BACKUP
			Case FOOD
				Return goods[FOOD] - useage - BACKUP
			Case GOODS
				Return goods[GOODS] - useage - BACKUP
		End
		Return 0.0
	End
	Method Buy:Void(type:Int, ammount:Float, cost:float)
		goods[type] += ammount
		trade[type] += ammount * cost
	End
	Method Sell:Void(type:Int, ammount:Float, cost:float)
		goods[type] -= ammount
		trade[type] -= ammount * cost
	End
	Method GetRandom:Float(norm:Int)
		Local test:Float
		For Local temp:Int = 1 To norm
			test += Rnd()
		End
		Return (test / float(norm))
	End
	Method PrintStuff:Void()
		Print "tech: " + technology
		Print "res: " + resources
		Print "industry: " + industry
		Print "aggriculture: " + agriculture
		Print "pop: " + population
		Print "METAL: " + SigDig(GetPrice(Item.METAL), 2) + "=" + goods[METAL] + "/" + trade[METAL]
		Print "PARTS: " + SigDig(GetPrice(Item.REACTOR), 2) + "=" + goods[PARTS] + "/" + trade[PARTS]
		Print "FUEL: " + SigDig(GetPrice(Item.FUEL), 2) + "=" + goods[GOODS] + "/" + trade[GOODS]
		Print "FOOD: " + SigDig(GetPrice(Item.FOOD), 2) + "=" + goods[FOOD] + "/" + trade[FOOD]
	End
	Method Convert:Void(from:Int, into:Int, rate:Float, ratio:float)
		If goods[from] < rate Then rate = goods[from]
		goods[from] -= rate
		goods[into] += rate * ratio
	End
	Method Add:Void(type:Int, rate:float)
		goods[type] = Clamp(goods[type] + rate, 0.0, MAX)
	End
	Method Remove:Void(type:Int, rate:Float)
		goods[type] = goods[type] - rate
	End
	Method Reset:Void()
		For Local temp:Int = 0 Until trade.Length()
			trade[temp] = 0.0
		Next
		For Local temp:Int = 0 Until goods.Length()
			goods[temp] = Clamp(goods[temp], 0.0, MAX)
		Next
	End
End