Strict

Import space_game

Class Event Abstract
	Const JSON_LOCATION:String = "Strings/Scripts/Events/"
	Const TYPE_POI:String = "poi.json"
	Const TYPE_RANDOM_ENCOUNTER:String = "random.json"
	Const DEBUG_PRINT:Bool = True
	Global current:float
	
	Function GetEvent:String(type:String)
		If DEBUG_PRINT
			Print("get event: " + JSON_LOCATION + type)
		End
		current = 0
		Local arr:JsonArray
		Try
			arr = JsonArray(New JsonParser(app.LoadString(JSON_LOCATION + type)).ParseValue())
		Catch e:JsonError
			Print "Missing Event: " + JSON_LOCATION + type
			ThrowErrorMessage("Missing Event", JSON_LOCATION + type)
			Return "NULL+NULL"
		End 
		Local eventList:Stack<SubEvent> = New Stack<SubEvent>
		For Local temp:Int = 0 Until arr.Length()
			eventList.Push(New SubEvent(JsonObject(arr.Get(temp))))
		Next
		Local rand:Float = Rnd(current)
		If DEBUG_PRINT
			Print "roll: " + rand
		End
		For Local temp:Int = 0 Until arr.Length()
			If eventList.Get(temp).inRange(rand)
				If eventList.Get(temp).sub <> Null
					Return GetEvent(eventList.Get(temp).sub)
				Else If eventList.Get(temp).run <> "ERROR"
					Return GetEvent(eventList.Get(temp).run)
				Else
					Print("r: " + eventList.Get(temp).returnScript + "+" + eventList.Get(temp).extra)
					Return eventList.Get(temp).returnScript + "+" + eventList.Get(temp).extra
				End
			End
		Next
		Return "NULL+NULL"
	End
	
	Function GetEvent:String(type:JsonArray)
		current = 0
		Local eventList:Stack<SubEvent> = New Stack<SubEvent>
		For Local temp:Int = 0 Until type.Length()
			eventList.Push(New SubEvent(JsonObject(type.Get(temp))))
		Next
		Local rand:Float = Rnd(current)
		If DEBUG_PRINT
			Print "roll: " + rand
		End
		For Local temp:Int = 0 Until type.Length()
			If eventList.Get(temp).inRange(rand)
				If eventList.Get(temp).sub <> Null
					Return GetEvent(eventList.Get(temp).sub)
				Else If eventList.Get(temp).run <> "ERROR"
					If DEBUG_PRINT
						Print("Run Event: " + eventList.Get(temp).run)
					End
					Return GetEvent(eventList.Get(temp).run)
				Else
					Print("r: " + eventList.Get(temp).returnScript + "+" + eventList.Get(temp).extra)
					Return eventList.Get(temp).returnScript + "+" + eventList.Get(temp).extra
				End
			End
		Next
		Return "NULL+NULL"
	End
End
 
Class SubEvent
	Field sub:JsonArray
	Field returnScript:String
	Field run:String
	Field name:String
	Field extra:String
	Field rangeStart:Float
	Field rangeStop:Float
	Method New(data:JsonObject)
		If data.Get("sub") <> Null
			sub = JsonArray(data.Get("sub"))
		End
		returnScript = data.GetString("return", "NULL")
		extra = data.GetString("extra", "NULL")
		name = data.GetString("name", "MISSING NAME")
		run = data.GetString("run", "ERROR")
		rangeStart = Event.current
		rangeStop = rangeStart + (data.GetInt("odds", 0) * GetMod(data.GetString("mod", "NULL")))
		If Event.DEBUG_PRINT
			Print name + "   " + rangeStart + "/" + rangeStop
		End
		Event.current = rangeStop
		
	End
	
	Method inRange:Bool(num:Float)
		If num >= rangeStart And num <= rangeStop
	 		Return True
		End
		Return False
	End
	
	Method GetMod:Float(type:String)
		Select type
			Case ""
				Return 1.0
			Case "NULL"
				Return 0.0
			Case "PIRATE_AREA"
				Return Player.map.hexField.area.danger
			Default
				Print("MOD " + type + " NOT FOUND")
				Return 1.0
		End
	End
	
End