Import space_game

Class MapData
	Field system:int
	Field level:int
	Field x:Int
	Field y:Int
	Field z:Int
	Field targetX:Int
	Field targetY:Int
	Field targetZ:Int
	Field nextX:Int
	Field nextY:Int
	Field nextZ:int
	Field hexField:HexField
	Field economy:Economy
	Field deathFlag:bool
	Field quitFlag:bool
	
	Method OnSave:JsonObject()
		Local data:JsonObject = New JsonObject()
		data.SetInt("system", system)
		data.SetInt("x", x)
		data.SetInt("y", y)
		data.SetInt("z", z)
		data.SetInt("level", level)
		data.Set("economy", economy.OnSave())
		Return data
	End
	Method OnLoad:Void(data:JsonObject)
		system = data.GetInt("system")
		economy = New Economy(JsonArray(data.Get("economy")))
		x = data.GetInt("x")
		y = data.GetInt("y")
		z = data.GetInt("z")
		level = data.GetInt("level")
		targetX = x
		targetY = y
		targetZ = z
		nextX = x
		nextY = y
		nextZ = z
	End
	Method GotoEdge:Void()
		Repeat
			Local h:Hex = hexField.hex.Get(Rnd(hexField.hex.Length()))
			If h.isEdge
				x = h.x
				y = h.y
				z = h.z
				targetX = x
				targetY = y
				targetZ = z
				Exit
			End
		
		Forever
	End
	Method GotoPlanet:Void()
		For Local obj:HexObject = EachIn hexField.spaceObjects
			If obj.type = HexObject.PLANET
				x = obj.x
				y = obj.y
				z = obj.z
				targetX = x
				targetY = y
				targetZ = z
				hexField.grabObject = obj
				Return
			End
		Next
	End
	Method SkipToDocking:Void()
		For Local obj:HexObject = EachIn hexField.spaceObjects
			If obj.type = HexObject.PLANET
				If x = obj.x And y = obj.y And z = obj.z
					targetX = x
					targetY = y
					targetZ = z
					hexField.grabObject = obj
					Return
				End 
			End
		Next
		GotoPlanet()
	End
	Method SwitchSystem:Void(num:int)
		Loader.SaveGame(Player.accountName, hexField)
		system = num
		hexField = New HexField(systemData)
		Loader.LoadGame(Player.accountName, hexField)
		GotoEdge()
	End
End