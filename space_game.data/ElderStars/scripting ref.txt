
		############################################################
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
		#++++++++++++++			System Calls		 ++++++++++++++#
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
		############################################################

		
		
##########################################################
##################		goto		######################
##########################################################

Loads screen

----- Arguments -----
screen_number
---------------------

example useage
[
"goto",
"5"
]

##########################################################
#################		throw		######################
##########################################################

Shows an error popup
Each new line is another line on the popup

----- Arguments -----
line1
line2
---------------------

example useage
[
"throw",
"Error!",
"Something is broke :/"
]

##########################################################
#################		wait		######################
##########################################################

Waits x millisecs before moving to the next item

----- Arguments -----
milliseconds
---------------------
example useage
[
"wait",
"6000"
]

		############################################################
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
		#++++++++++++++			Comms Screen		 ++++++++++++++#
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
		############################################################
		
##########################################################
##################		load enemy		##################
##########################################################
Loads a ship into the enemy slot
all ships must be in Strings/Ships

----- Arguments -----
path || folder || <TIER>folder
---------------------

example useage
[
"load enemy",
"<TIER>Pirates/Normal" //This will automatically select the current tier and load a random ship from this folder
]

example useage
[
"load enemy",
"T0/Pirates/Normal" //This will load a random ship from this folder
]

example useage
[
"load enemy",
"T0/Pirates/Normal/PirateCr.ess" //must supply absolute path for this
]

##########################################################
##################		add button		##################
##########################################################
Creates buttons with scripts attached

the buttons will only run the script once
----- Arguments -----
Text
script
...
---------------------

example useage
[
"add buttons",
"Press Me", //will make a button that says press me
"script.ess" // and runs script.ess when clicked
]


