Import space_game

Class Modcodes
	'these are auto used mods
	Const FIRE:int = 1 'Fire supression
	Const BIO_SCAN:Int = 2 'show life
	Const EMP_SHIELD:Int = 3 'protects against EMP
	Const SCAN:Int = 4
	Const ARMOR:Int = 5 'higher starting health
	Const HEAT_SINK:Int = 6 ' remove extra heat per tick
	
	'these are mods that have to be activated by the player
	Const EMP:Int = 100
	Const SHIELD_RECHARGE:Int = 101
	Const THERMAL_RELEASE:Int = 102
	Const AFTERBURNER:Int = 103
	Const STEALTH:Int = 104
	
	'these are player activated
	Const INFINITE_SHIELD:Int = 500
	
	
	Function CodeToString:string(code:Int)
		If code = FIRE Then Return "FIRE"
		If code = BIO_SCAN Then Return "BIO_SCAN"
		If code = EMP_SHIELD Then Return "EMP_SHIELD"
		If code = SCAN Then Return "SCAN"
		If code = ARMOR Then Return "ARMOR"
		If code = HEAT_SINK Then Return "HEAT_SINK"
		If code = THERMAL_RELEASE Then Return "THERMAL_RELEASE"
		If code = AFTERBURNER Then Return "AFTERBURNER"
		If code = STEALTH Then Return "STEALTH"
		Return "NULL"
	End
End