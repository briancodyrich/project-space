Strict

Import space_game


Class Item
	Const ICON_LOCATION:String = "icons/"
	
	Const DAMAGE_MULTIPLIER:Float = 0.005
	Const QUALITY_MULTIPLIER:Float = 0.75

	Const NULLTYPE:Int = 0
	Const REACTOR:Int = 1
	Const BATTERY:Int = 2
	Const RADIATOR:Int = 3
	Const OXYGENGEN:Int = 4
	Const ENGINE:Int = 5
	Const VENT:Int = 6
	Const SHIELD:Int = 7
	Const DODGE:Int = 8
	Const HEALTH:Int = 9
	Const COOLANT_TANK:Int = 10
	Const COMPUTER:Int = 11
	Const HARDPOINT:Int = 12
	Const SOLAR:Int = 13
	Const SYSTEM:Int = 14
	
	Const FUEL:Int = 30
	Const ANTIMATER:Int = 31
	Const FOOD:Int = 32
	Const SCRAP:Int = 33
	Const WEAPON:Int = 34
	Const METAL:Int = 35
	Const ILLEGAL:Int = 36
	Const MINING_DRONE:Int = 37
	Const MISSILE:Int = 38
	Const CANNON_BALL:Int = 39
	Const THERMITE:Int = 40
	
	Const GENERIC_PART:Int = 100
	
	Field id:String
	Field displayName:String
	Field formatedName:string
	Field comp:ComponentInterface
	Field weapon:Weapon
	Field combat_effect:JsonObject
	Field mass:Int
	Field type:Int
	Field cost:int
	Field descr:Stack<String>
	Field damage:Int
	Field qualityLevel:Int = -1
	Field hasQualityLevel:bool
	Field count:int
	Field max:Int
	Field icon:Image
	Field effect:string = "NULL"
	Field power:int
	
	Field min_level:Int
	Field max_level:Int
	Field drop_range:int
	
	
	Method New(data:JsonObject, damage:Int = 0, qualityLevel:Int = 0)
		
		min_level = data.GetInt("min_level", -100)
		max_level = data.GetInt("max_level", 100)
		drop_range = data.GetInt("drop_range", 5)
		id = data.GetString("id")
		displayName = data.GetString("display_name", "NULL")
		Local type_string:String = data.GetString("type").ToUpper()
		If type_string = "REACTOR" Then Self.type = REACTOR
		If type_string = "BATTERY" Then Self.type = BATTERY
		If type_string = "RADIATOR" Then Self.type = RADIATOR
		If type_string = "VENT" Then Self.type = VENT
		If type_string = "OXYGEN_GENERATOR" Then Self.type = OXYGENGEN
		If type_string = "ENGINE" Then Self.type = ENGINE
		If type_string = "SHIELD" Then Self.type = SHIELD
		If type_string = "DODGE" Then Self.type = DODGE
		If type_string = "HEALTH" Then Self.type = HEALTH
		If type_string = "COOLANT_TANK" Then Self.type = COOLANT_TANK
		If type_string = "COMPUTER" Then Self.type = COMPUTER
		If type_string = "HARDPOINT" Then Self.type = HARDPOINT
		If type_string = "SOLAR" Then Self.type = SOLAR
		If type_string = "FUEL" Then Self.type = FUEL
		If type_string = "ANTIMATTER" Then Self.type = ANTIMATER
		If type_string = "FOOD" Then Self.type = FOOD
		If type_string = "SCRAP" Then Self.type = SCRAP
		If type_string = "MINING_DRONE" Then Self.type = MINING_DRONE
		If type_string = "SYSTEM"
			Self.type = SYSTEM
			Self.effect = data.GetString("effect")
			Self.power = data.GetInt("power")
			combat_effect = JsonObject(data.Get("combat_effect"))
			If combat_effect.ToJson() = "{}" Then combat_effect = Null
		End
		If type_string = "METAL" Then Self.type = METAL
		If type_string = "MISSILE" Then Self.type = MISSILE
		If type_string = "CANNON_BALL" Then Self.type = CANNON_BALL
		If type_string = "THERMITE" Then Self.type = THERMITE
		If type_string = "WEAPON"
			Self.type = WEAPON
			Self.weapon = New Weapon(data)
		End
		
		If Self.type < 30 And Self.type > 0 And Self.type <> SYSTEM
			Select type
				Case Component.REACTOR
					comp = New Reactor(0, 0, data, 0, 0, True, damage, qualityLevel)
				Case Component.BATTERY
					comp = New Battery(0, 0, data, 0, 0, True, damage, qualityLevel)
				Case Component.ENGINE
					comp = New Engine(0, 0, data, 0, 0, True, damage, qualityLevel)
				Case Component.HEALTH
					comp = New HealingTube(0, 0, data, 0, 0, True, damage, qualityLevel)
				Case Component.SHIELD
					comp = New Shield(0, 0, data, 0, 0, True, damage, qualityLevel)
				Case Component.COOLANT_TANK
					comp = New CoolantTank(0, 0, data, 0, 0, True, damage, qualityLevel)
				Case Component.COMPUTER
					comp = New Computer(0, 0, data, 0, 0, True, damage, qualityLevel)
				Case Component.HARDPOINT
					comp = New Hardpoint(0, 0, data, 0, 0, True, damage, qualityLevel)
				Case Component.SOLAR
					comp = New Solar(0, 0, data, 0, 0, True, damage, qualityLevel)
				Default
					Print "Could not find " + type + " for part " + id
			End
			If data.Get("hasQuality") <> Null
				hasQualityLevel = True
			End
		End
		
		
		
		icon = LoadImage(ICON_LOCATION + data.GetString("icon"),, Image.MidHandle)
		Self.damage = damage
		Self.qualityLevel = qualityLevel
		
		If comp <> Null
			formatedName = comp.getFormatedName()
		Else
			formatedName = displayName
		End
		mass = data.GetInt("mass")
		descr = New Stack<String>
		Local arr:JsonArray = JsonArray(data.Get("descr"))
		For Local count:Int = 0 Until arr.Length()
			descr.Push(arr.GetString(count))
		Next
		
		max = Pow(2, data.GetInt("stack"))
		cost = data.GetInt("cost")
		count = 1
		If icon = Null
			icon = LoadImage(ICON_LOCATION + "default.png",, Image.MidHandle)
		End
	End
	
	Method GetMass:Int()
		Return Min(count, max) * mass
	End
	#rem
	Method GetScore:Float()
		Select type
			Case REACTOR
				Local stat:Reactor = Reactor(comp.getSelf())
				Local bad:Float
				Local good:Float
				Local val:float
				bad = (stat.heatProduction * stat.GetEfficiency(True,, False))
				good = (stat.energyProduction * stat.GetEfficiency())
				val = good / bad
				Print "reactor: " + val
				Return val
			Case BATTERY
				Local stat:Battery = Battery(comp.getSelf())
				Local bad:Float
				Local good:Float
				Local val:float
				good = (stat.capacity * stat.GetEfficiency())
				bad = 1.0 / (stat.dischargeRate * stat.GetEfficiency())
				val = good / bad
				Print "battery: " + val
				Return val
			Case ENGINE
				Local stat:Engine = Engine(comp.getSelf())
				Local bad:Float
				Local good:Float
				Local val:float
				good = stat.thrust * stat.GetEfficiency()
				bad = (
				bad = 1.0 / (stat.dischargeRate * stat.GetEfficiency())
				val = good / bad
				Print "engine: " + val
				Return val
				
				Print "engine"
				Local self_item:Engine = Engine(comp.getSelf())
				Local new_item:Engine = Engine(n.comp.getSelf())
				val += (self_item.heatProduction * self_item.GetEfficiency(True,, False)) / (new_item.heatProduction * new_item.GetEfficiency(True,, False))
				Print val
				val += (self_item.energyProduction * self_item.GetEfficiency(True,, False)) / (new_item.energyProduction * new_item.GetEfficiency(True,, False))
				Print val
				val += (new_item.thrust * new_item.GetEfficiency()) / (self_item.thrust * self_item.GetEfficiency())
				Print val
				val += ( (new_item.thrust * new_item.GetEfficiency()) / new_item.fuelUse) / ( (self_item.thrust * self_item.GetEfficiency()) / self_item.fuelUse)
				Print val
				val /= 4.0
				Print val
				If val < 1 Then Return True
				Return False
			Case HEALTH
			Case SHIELD
			Case COOLANT_TANK
			Case SYSTEM
			Case WEAPON
		End
		Return 0.0
	End
	#end
	Method isCompWeaker:Bool(n:Item)
		Local good:float
		Local bad:float
		Local val:float
		Select n.type
			Case REACTOR
				Local self_item:Reactor = Reactor(comp.getSelf())
				Local new_item:Reactor = Reactor(n.comp.getSelf())
				'bad is self first
				bad += 1.0 - (new_item.heatProduction * new_item.GetEfficiency(True,, False)) / (self_item.heatProduction * self_item.GetEfficiency(True,, False))
				good += 1.0 - (new_item.energyProduction * new_item.GetEfficiency()) / (self_item.energyProduction * self_item.GetEfficiency())
				val = good - bad
				Print "reactor: " + good + "/" + bad + "   total: " + val
				If val > 0 Then Return True
				Return False
			Case BATTERY
				Local self_item:Battery = Battery(comp.getSelf())
				Local new_item:Battery = Battery(n.comp.getSelf())
				good += 1.0 - (new_item.capacity * new_item.GetEfficiency()) / (self_item.capacity * self_item.GetEfficiency())
				good += 1.0 - (new_item.dischargeRate * new_item.GetEfficiency()) / (self_item.dischargeRate * self_item.GetEfficiency())
				val = good
				Print "battery: " + good + "   total: " + val
				If val > 0 Then Return True
				Return False
			Case ENGINE
				Local self_item:Engine = Engine(comp.getSelf())
				Local new_item:Engine = Engine(n.comp.getSelf())
				bad += 1.0 - (new_item.heatProduction * new_item.GetEfficiency(True,, False)) / (self_item.heatProduction * self_item.GetEfficiency(True,, False))
				bad += 1.0 - (new_item.fuelUse) / float(self_item.fuelUse)
				good += 1.0 - (new_item.thrust * new_item.GetEfficiency()) / (self_item.thrust * self_item.GetEfficiency())
				good += 1.0 - ( (new_item.thrust * new_item.GetEfficiency()) / new_item.fuelUse) / ( (self_item.thrust * self_item.GetEfficiency()) / self_item.fuelUse)
				val = good - bad
				Print "engine: " + good + "/" + bad + "   total: " + val
				If val > 0 Then Return True
				Return False
			Case HEALTH
			Case SHIELD
			Case COOLANT_TANK
			Case SYSTEM
			Case WEAPON
			Default
			Print "UNKNOWN TYPE: " + n.type
		End
		Return False
	
	End
	
	Method GetValue:Int(area:Economy, singleItem:Bool = False)
		If comp = Null
			If singleItem
				Return cost * area.GetPrice(type)
			Else
				Return Min(count, max) * cost * area.GetPrice(type)
			End
			
		End
		Return (cost * (1 - (comp.getPermDamage() * DAMAGE_MULTIPLIER)) * (1 + comp.getQualityLevel() * QUALITY_MULTIPLIER)) * area.GetPrice(type)
	End
	
	Method GetRepairCost:Int(area:Economy)
		If comp = Null
			Return 0
		End
		Return ( (cost * (1 + comp.getQualityLevel() * QUALITY_MULTIPLIER) * area.GetPrice(type)) - GetValue(area))
	End
	
	Method GetDescr:Stack<String>(i:Item = Null, printValue:bool = False, area:Economy = Null)
		If i <> Null
			Return GetCompair(i)
		End
		Local rs:Stack<String> = New Stack<String>
		If descr = Null Return rs
		For Local temp:Int = 0 Until descr.Length()
			Local s:String = descr.Get(temp)
			If s.Contains("<BR>")
				If printValue = True And area <> Null
					s = s.Replace("<BR>", FormatCredits(int(GetValue(area))))
				Else
					Continue
				End
			End
			If s.Contains("<HEAT>")
				s = s.Replace("<HEAT>", comp.GetPrintable(Component.PRINTABLE_HEAT))
			End
			If s.Contains("<POWER_USE>")
				s = s.Replace("<POWER_USE>", comp.GetPrintable(Component.PRINTABLE_POWER_USE))
			End
			If s.Contains("<POWER_GEN>")
				s = s.Replace("<POWER_GEN>", comp.GetPrintable(Component.PRINTABLE_POWER_GEN))
			End
			If s.Contains("<STORAGE_SIZE>")
				s = s.Replace("<STORAGE_SIZE>", comp.GetPrintable(Component.PRINTABLE_STORAGE_SIZE))
			End
			If s.Contains("<DISCHARGE_RATE>")
				s = s.Replace("<DISCHARGE_RATE>", comp.GetPrintable(Component.PRINTABLE_DISCHARGE_RATE))
			End
			If s.Contains("<EFFICIENCY>")
				s = s.Replace("<EFFICIENCY>", comp.GetPrintable(Component.PRINTABLE_EFFICIENCY))
			End
			If weapon <> Null
				If s.Contains("<G_HDPS>")
					s = s.Replace("<G_HDPS>", SigDig(weapon.GetVal(Weapon.G_HDPS), 1))
				End
				If s.Contains("<G_CDPS>")
					s = s.Replace("<G_CDPS>", SigDig(weapon.GetVal(Weapon.G_CDPS), 1))
				End
				If s.Contains("<G_SDPS>")
					s = s.Replace("<G_SDPS>", SigDig(weapon.GetVal(Weapon.G_SDPS), 1))
				End
				If s.Contains("<G_HEAT>")
					s = s.Replace("<G_HEAT>", SigDig(weapon.GetVal(Weapon.G_HEAT), 1))
				End
			End
			If s.Contains("<RECHARGE_TIME>")
				s = s.Replace("<RECHARGE_TIME>", comp.GetPrintable(Component.PRINTABLE_RECHARGE_TIME))
			End
			If s.Contains("<RECHARGE_USE>")
				s = s.Replace("<RECHARGE_USE>", comp.GetPrintable(Component.PRINTABLE_RECHARGE_USE))
			End
			If s.Contains("<THRUST>")
				s = s.Replace("<THRUST>", comp.GetPrintable(Component.PRINTABLE_THRUST))
			End
			If s.Contains("<FUEL>")
				s = s.Replace("<FUEL>", comp.GetPrintable(Component.PRINTABLE_FUEL_USE))
			End
			If s.Contains("<QUALITY>")
				s = s.Replace("<QUALITY>", comp.QualityLevelToString(comp.getQualityLevel()))
			End
			If s.Contains("<MASS>")
				s = s.Replace("<MASS>", GetMass())
			End
			rs.Push(s)
		Next
		Return rs
	End
	
	
	Method GetCompair:Stack<String>(i:Item)
		Local rs:Stack<String> = New Stack<String>
		If descr = Null Return rs
		For Local temp:Int = 0 Until descr.Length()
			Local s:String = descr.Get(temp)
			If s.Contains("<BR>") Then Continue
			If s.Contains("<HEAT>")
				Local v:int = -i.comp.GetPrintable(Component.PRINTABLE_HEAT) + comp.GetPrintable(Component.PRINTABLE_HEAT)
				If v > 0
					s = s.Replace("<HEAT>", (comp.GetPrintable(Component.PRINTABLE_HEAT) + "#ff0000 (+" + v + ")#ffffff"))
				Else If v = 0
					s = s.Replace("<HEAT>", (comp.GetPrintable(Component.PRINTABLE_HEAT)))
				Else
					s = s.Replace("<HEAT>", (comp.GetPrintable(Component.PRINTABLE_HEAT) + "#00ff00 (" + v + ")#ffffff"))
				End
			End
			If s.Contains("<POWER_USE>")
				Local v:int = -i.comp.GetPrintable(Component.PRINTABLE_POWER_USE) + comp.GetPrintable(Component.PRINTABLE_POWER_USE)
				If v > 0
					s = s.Replace("<POWER_USE>", (comp.GetPrintable(Component.PRINTABLE_POWER_USE) + "#ff0000 (+" + v + ")#ffffff"))
				Else If v = 0
					s = s.Replace("<POWER_USE>", (comp.GetPrintable(Component.PRINTABLE_POWER_USE)))
				Else
					s = s.Replace("<POWER_USE>", (comp.GetPrintable(Component.PRINTABLE_POWER_USE) + "#00ff00 (" + v + ")#ffffff"))
				End
			End
			If s.Contains("<POWER_GEN>")
				Local v:int = -i.comp.GetPrintable(Component.PRINTABLE_POWER_GEN) + comp.GetPrintable(Component.PRINTABLE_POWER_GEN)
				If v > 0
					s = s.Replace("<POWER_GEN>", (comp.GetPrintable(Component.PRINTABLE_POWER_GEN) + "#00ff00 (+" + v + ")#ffffff"))
				Else If v = 0
					s = s.Replace("<POWER_GEN>", (comp.GetPrintable(Component.PRINTABLE_POWER_GEN)))
				Else
					s = s.Replace("<POWER_GEN>", (comp.GetPrintable(Component.PRINTABLE_POWER_GEN) + "#ff0000 (" + v + ")#ffffff"))
				End
			End
			If s.Contains("<STORAGE_SIZE>")
				Local v:int = -i.comp.GetPrintable(Component.PRINTABLE_STORAGE_SIZE) + comp.GetPrintable(Component.PRINTABLE_STORAGE_SIZE)
				If v > 0
					s = s.Replace("<STORAGE_SIZE>", (comp.GetPrintable(Component.PRINTABLE_STORAGE_SIZE) + "#00ff00 (+" + v + ")#ffffff"))
				Else If v = 0
					s = s.Replace("<STORAGE_SIZE>", (comp.GetPrintable(Component.PRINTABLE_STORAGE_SIZE)))
				Else
					s = s.Replace("<STORAGE_SIZE>", (comp.GetPrintable(Component.PRINTABLE_STORAGE_SIZE) + "#ff0000 (" + v + ")#ffffff"))
				End
			End
			If s.Contains("<DISCHARGE_RATE>")
				Local v:int = -i.comp.GetPrintable(Component.PRINTABLE_DISCHARGE_RATE) + comp.GetPrintable(Component.PRINTABLE_DISCHARGE_RATE)
				If v > 0
					s = s.Replace("<DISCHARGE_RATE>", (comp.GetPrintable(Component.PRINTABLE_DISCHARGE_RATE) + "#00ff00 (+" + v + ")#ffffff"))
				Else If v = 0
					s = s.Replace("<DISCHARGE_RATE>", (comp.GetPrintable(Component.PRINTABLE_DISCHARGE_RATE)))
				Else
					s = s.Replace("<DISCHARGE_RATE>", (comp.GetPrintable(Component.PRINTABLE_DISCHARGE_RATE) + "#ff0000 (" + v + ")#ffffff"))
				End
			End
			If s.Contains("<EFFICIENCY>")
				Local v:int = -i.comp.GetPrintable(Component.PRINTABLE_EFFICIENCY) + comp.GetPrintable(Component.PRINTABLE_EFFICIENCY)
				If v > 0
					s = s.Replace("<EFFICIENCY>", (comp.GetPrintable(Component.PRINTABLE_EFFICIENCY) + "#ff0000 (+" + v + ")#ffffff"))
				Else If v = 0
					s = s.Replace("<EFFICIENCY>", (comp.GetPrintable(Component.PRINTABLE_EFFICIENCY)))
				Else
					s = s.Replace("<EFFICIENCY>", (comp.GetPrintable(Component.PRINTABLE_EFFICIENCY) + "#00ff00 (" + v + ")#ffffff"))
				End
			End
			If s.Contains("<THRUST>")
				Local v:int = -i.comp.GetPrintable(Component.PRINTABLE_THRUST) + comp.GetPrintable(Component.PRINTABLE_THRUST)
				If v > 0
					s = s.Replace("<THRUST>", (comp.GetPrintable(Component.PRINTABLE_THRUST) + "#00ff00 (+" + v + ")#ffffff"))
				Else If v = 0
					s = s.Replace("<THRUST>", (comp.GetPrintable(Component.PRINTABLE_THRUST)))
				Else
					s = s.Replace("<THRUST>", (comp.GetPrintable(Component.PRINTABLE_THRUST) + "#ff0000 (" + v + ")#ffffff"))
				End
			End
			If s.Contains("<FUEL>")
				Local v:int = -i.comp.GetPrintable(Component.PRINTABLE_FUEL_USE) + comp.GetPrintable(Component.PRINTABLE_FUEL_USE)
				If v > 0
					s = s.Replace("<FUEL>", (comp.GetPrintable(Component.PRINTABLE_FUEL_USE) + "#00ff00 (+" + v + ")#ffffff"))
				Else If v = 0
					s = s.Replace("<FUEL>", (comp.GetPrintable(Component.PRINTABLE_FUEL_USE)))
				Else
					s = s.Replace("<FUEL>", (comp.GetPrintable(Component.PRINTABLE_FUEL_USE) + "#ff0000 (" + v + ")#ffffff"))
				End
			End
			#rem
			If s.Contains("<DPS>")
				'If (-i.weapon.GetDPS() +weapon.GetDPS())
				s = s.Replace("<DPS>", SigDig(-i.weapon.GetDPS() +weapon.GetDPS(), 1))
			End
			#end
			If s.Contains("<RECHARGE_TIME>")
				Local v:int = -i.comp.GetPrintable(Component.PRINTABLE_RECHARGE_TIME) + comp.GetPrintable(Component.PRINTABLE_RECHARGE_TIME)
				If v > 0
					s = s.Replace("<RECHARGE_TIME>", (comp.GetPrintable(Component.PRINTABLE_RECHARGE_TIME) + "#ff0000 (+" + v + ")#ffffff"))
				Else If v = 0
					s = s.Replace("<RECHARGE_TIME>", (comp.GetPrintable(Component.PRINTABLE_RECHARGE_TIME)))
				Else
					s = s.Replace("<RECHARGE_TIME>", (comp.GetPrintable(Component.PRINTABLE_RECHARGE_TIME) + "#00ff00 (" + v + ")#ffffff"))
				End
			End
			If s.Contains("<RECHARGE_USE>")
				Local v:int = -i.comp.GetPrintable(Component.PRINTABLE_RECHARGE_USE) + comp.GetPrintable(Component.PRINTABLE_RECHARGE_USE)
				If v > 0
					s = s.Replace("<RECHARGE_USE>", (comp.GetPrintable(Component.PRINTABLE_RECHARGE_USE) + "#ff0000 (+" + v + ")#ffffff"))
				Else If v = 0
					s = s.Replace("<RECHARGE_USE>", (comp.GetPrintable(Component.PRINTABLE_RECHARGE_USE)))
				Else
					s = s.Replace("<RECHARGE_USE>", (comp.GetPrintable(Component.PRINTABLE_RECHARGE_USE) + "#00ff00 (" + v + ")#ffffff"))
				End
			End
			If s.Contains("<MASS>")
				Local v:Int = -i.GetMass() +GetMass()
				If v > 0
					s = s.Replace("<MASS>", (comp.getMass() + "#ff0000 (+" + v + ")#ffffff"))
				Else If v = 0
					s = s.Replace("<MASS>", (comp.getMass()))
				Else
					s = s.Replace("<MASS>", (comp.getMass() + "#00ff00 (" + v + ")#ffffff"))
				End
			End
			If s.Contains("<QUALITY>")
				If comp.getQualityLevel() > i.comp.getQualityLevel()
					s = s.Replace("<QUALITY>", "#00ff00" + comp.QualityLevelToString(comp.getQualityLevel()))
				Else If comp.getQualityLevel() < i.comp.getQualityLevel()
					s = s.Replace("<QUALITY>", "#ff0000" + comp.QualityLevelToString(comp.getQualityLevel()))
				Else
					s = s.Replace("<QUALITY>", comp.QualityLevelToString(comp.getQualityLevel()))
				End
				
			End
			rs.Push(s)
		Next
		Return rs
	End
	
End