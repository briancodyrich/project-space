Strict

Import space_game

Class Inventory Implements Loadable
	Field slots:Item[]
	
	Method New(size:int)
		slots = New Item[size]
	End
	
	Method OnSave:JsonObject()
		Local data:JsonObject = New JsonObject()
		Local count:Int = 0
		For Local temp:Int = 0 Until slots.Length()
			If slots[temp] <> Null
				count += 1
			End
		Next
		Local x:Int = 0
		Local arr:JsonArray = New JsonArray(count)
		For Local temp:Int = 0 Until slots.Length()
			If slots[temp] <> Null
				Local item:JsonObject = New JsonObject()
				item.SetString("name", slots[temp].id)
				item.SetInt("position", temp)
				item.SetInt("count", slots[temp].count)
				If slots[temp].damage > 0
					item.SetInt("damage", slots[temp].damage)
				End
				If slots[temp].qualityLevel > 0
					item.SetInt("quality", slots[temp].qualityLevel)
				End
				arr.Set(x, item)
				x += 1
			End
		Next
		data.Set("Inventory", arr)
		Return data
	End
	
	Method FileName:String()
		Return ""
	End
	
	Method OnLoad:Void(data:JsonObject)
		If data = Null Then Return
		Local arr:JsonArray = JsonArray(data.Get("Inventory"))
		If arr = Null Then Return
		For Local temp:Int = 0 Until arr.Length()
			Local item:JsonObject = JsonObject(arr.Get(temp))
			If ItemLoader.DoesItemExist(item.GetString("name"))
				slots[item.GetInt("position")] = New Item(ItemLoader.GetItem(item.GetString("name")), item.GetInt("damage", 0), item.GetInt("quality", 0))
				slots[item.GetInt("position")].count = item.GetInt("count")
			End
		Next
	End
	
	Method DestroyInventory:Void()
		For Local temp:Int = 0 Until slots.Length()
			slots[temp] = Null
		Next
	End
	
	Method GiveItem:int(name:String, count:Int)

		If not ItemLoader.DoesItemExist(name)
			Print "item " + name + " does not exist"
			Return -1
		End
		
		For Local temp:Int = 0 Until slots.Length()
			If slots[temp] <> Null and slots[temp].id = name
				If slots[temp].count < slots[temp].max
					Local num:int = (slots[temp].max - slots[temp].count)
					slots[temp].count += Min(num, count)
					count -= Min(num, count)
					If count <= 0 Return 0
				End
			End
		Next
		For Local temp:Int = 0 Until slots.Length()
			If slots[temp] = Null
				slots[temp] = New Item(ItemLoader.GetItem(name))
				count -= 1
				If count <= 0 Return 0
				If slots[temp].count < slots[temp].max
					Local num:int = (slots[temp].max - slots[temp].count)
					slots[temp].count += Min(num, count)
					count -= Min(num, count)
					If count <= 0 Return 0
				End
			End
		Next
		Return count
	End
	
	Method Sort:Bool(isStore:Bool = False, repairItems:Bool = False)
		If repairItems
			For Local temp:Int = 0 Until slots.Length()
				If slots[temp] <> Null
					slots[temp].damage = 0
					If slots[temp].comp <> Null
						slots[temp].comp.setPermDamage(0)
						slots[temp].formatedName = slots[temp].comp.getFormatedName()
					End
					
				End
			Next
		End
		CombineItems(isStore)
		GroupItems()
		Return True
		Local count:int
		Local hold:Int = -1
		Local sorted:bool
		While (count < slots.Length())
			If slots[count] = Null And hold = -1
				hold = count
			End
			If hold <> - 1 And slots[count] <> Null
				slots[hold] = slots[count]
				slots[count] = Null
				count = hold
				hold = -1
				sorted = True
			End
			count += 1
		End
		Return sorted
	End
	Method CombineItems:Void(isStore:bool)
		For Local temp:Int = 0 Until slots.Length()
			If slots[temp] <> Null
				If isStore
					For Local tt:Int = temp + 1 Until slots.Length()
						If slots[tt] <> Null and slots[temp].id = slots[tt].id And slots[temp].damage = slots[tt].damage And slots[temp].qualityLevel = slots[tt].qualityLevel
							slots[temp].count += slots[tt].count
							slots[tt] = Null
						End
					Next
				Else If slots[temp].count < slots[temp].max
					For Local tt:Int = temp + 1 Until slots.Length()
						If slots[tt] <> Null and slots[temp].id = slots[tt].id And slots[temp].count < slots[temp].max
							Local remove:Int = Min(slots[temp].max - slots[temp].count, slots[tt].count)
							slots[temp].count += remove
							slots[tt].count -= remove
							If slots[tt].count <= 0 Then slots[tt] = Null
						End
					Next
				End
			End
		Next
	End
	Method GroupItems:Void()
		Local n:Item[] = New Item[slots.Length()]
		Local count:Int = 0
		For Local temp:Int = 0 Until slots.Length()
			If slots[temp] <> Null
				Local str:String = slots[temp].id
				n[count] = slots[temp]
				slots[temp] = Null
				count += 1
				For Local tt:Int = temp + 1 Until slots.Length()
					If slots[tt] <> Null and slots[tt].id = str
						n[count] = slots[tt]
						slots[tt] = null
						count += 1
					End
				Next
			End
		Next
		slots = n
	End
	
	Method RemoveItem:Bool(id:String, count:Int)
		If (CountItem(id) < count) Return False
		For Local temp:Int = slots.Length() -1 To 0 Step - 1
			If slots[temp] <> Null
				If slots[temp].id = id
					If slots[temp].count > count
						slots[temp].count -= count
						Return True
					Else If slots[temp].count = count
						slots[temp] = Null
						Return True
					Else
						count -= slots[temp].count
						slots[temp] = Null
					End
				End
			End
		Next
		Return False
	End
	Method RemoveItem:Bool(type:int, count:Int)
		If (CountItem(type) < count) Return False
		For Local temp:Int = slots.Length() -1 To 0 Step - 1
			If slots[temp] <> Null
				If slots[temp].type = type
					If slots[temp].count > count
						slots[temp].count -= count
						Return True
					Else If slots[temp].count = count
						slots[temp] = Null
						Return True
					Else
						count -= slots[temp].count
						slots[temp] = Null
					End
				End
			End
		Next
		Return False
	End
	
	Method CountSlots:Int(count_filled:Bool, count_open:bool)
		Local count:Int
		For Local temp:Int = 0 Until slots.Length()
			If slots[temp] = Null
				If count_open Then count += 1
			Else
				If count_filled Then count += 1
			End
		Next
		Return count
	End
	
	Method CountItem:Int(type:Int)
		Local count:Int
		For Local temp:Int = 0 Until slots.Length()
			If slots[temp] <> Null and slots[temp].type = type
				count += slots[temp].count
			End
		Next
		Return count
	End
	Method CountItemValue:Int(type:Int, area:SystemType)
		Local value:Int
		For Local temp:Int = 0 Until slots.Length()
			If slots[temp] <> Null and slots[temp].type = type
				value += (slots[temp].count * slots[temp].cost * area.GetPrice(type))
			End
		Next
		Return value
	End
	Method CountItem:Int(id:String)
		Local count:Int
		For Local temp:Int = 0 Until slots.Length()
			If slots[temp] <> Null and slots[temp].id = id
				count += slots[temp].count
			End
		Next
		Return count
	End
	Method hasItem:Bool(type:Int)
		If CountItem(type) > 0 Return True
		Return False
	End
	Method hasItem:Bool(name:string)
		If CountItem(name) > 0 Return True
		Return False
	End
	
	Method GetMass:Int()
		Local mass:Int = 0
		For Local i:int = 0 Until slots.Length()
			If slots[i] <> Null
				mass += slots[i].GetMass()
			End
		Next
		Return mass
	End
End



