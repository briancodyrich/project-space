Import space_game

Class LeveledList
	Const QUALITY_MULTIPLIER:Float = 1.45
	Const PART_STORE:Int = 1
	Const MOD_STORE:Int = 2
	Field partStore:Stack<Item>
	Field modStore:Stack<Item>
	Field area:Economy
	Field level:int
	
	Method New(area:Economy, level:int)
		Self.area = area
	End
	
	Method BuildModStore:Void()
		Local iStack:Stack<ItemData> = ItemLoader.GetModList()
		modStore = New Stack<Item>()
		For Local temp:Int = 0 Until iStack.Length()
			Local data:JsonObject = ItemLoader.GetItem(iStack.Get(temp).id)
			Local tItem:Item = New Item(data, 0, 0)
			If level >= tItem.min_level And level <= tItem.max_level And tItem.max > 0
				modStore.Push(tItem)
			End
		Next
	End
	Method BuildPartsStore:Void()
		Local iStack:Stack<ItemData> = ItemLoader.GetPartList()
		partStore = New Stack<Item>()
		For Local temp:Int = 0 Until iStack.Length()
			Local data:JsonObject = ItemLoader.GetItem(iStack.Get(temp).id)
			Local tItem:Item = New Item(data, 0, 0)
			If level >= tItem.min_level And level <= tItem.max_level And tItem.max > 0
				partStore.Push(tItem)
			End
		Next
	End
	
	Method GetDropRNG:float(i:Item, quality:int)
		Local adjustedRarity:Float = 100
		Local scale:Float = 8 / float(i.max_level - i.min_level)
		Local base:Float = LeveledList.QUALITY_MULTIPLIER - (area.technology / 10.0)
	'	Print "ql: " + quality
		Local exp:Float = Abs( (quality - (level - i.min_level) * scale))
	'	Print "base: " + base
	'	Print "exp : " + exp
		adjustedRarity *= (Pow(base, -exp))
	'	Print "lv: " +level
	'	Print "rarity: " + adjustedRarity
		Return adjustedRarity 
	End
	
	Method GetStoreRNG:Float(quality:Int)
		Local odd:Float = 50
		For Local temp:Int = 0 Until quality
			odd /= 2.0
		Next
		Return odd
	End
	
	Method AdjustQuality:Item(i:Item, is_store:bool)
		For Local temp:Int = 0 Until 50
			Local lv:int = Rnd(8)
			If is_store
				If Rnd(100) < GetStoreRNG(lv)
					Return New Item(ItemLoader.GetItem(i.id), 0, lv)
				End
			Else
				If Rnd(100) < GetDropRNG(i, lv)
					Return New Item(ItemLoader.GetItem(i.id), 0, lv)
				End
			End
		Next
		Return New Item(ItemLoader.GetItem(i.id), 0, 0)
	End
	
	Method GetItem:Item(type:Int, is_store:bool)
		Local l:Stack<Item>
		Select type
			Case PART_STORE
			l = partStore
			Case MOD_STORE
			l = modStore
		End
		
		If l = Null Or l.IsEmpty() Then Return Null
		Repeat
			Local i:Item = l.Get(Rnd(l.Length()))
			If i.hasQualityLevel
				Return AdjustQuality(i, is_store)
			Else
				Return New Item(ItemLoader.GetItem(i.id))
			End
		Forever
	Return Null
	End
	
	
End
