Strict

Import space_game

Class RaceLoader
	Global data:StringMap<StringMap<RaceDataExt>> = New StringMap<StringMap<RaceDataExt>>()
	Global dataP:StringMap<StringMap<Bool>> = New StringMap<StringMap<Bool>>()
	Const PLAYABLE:Int = 1
	Const ANY:Int = 2
	Function LoadRace:Void(path:String, mode:int)
		Print "HELLO " + path
		Local dat:JsonObject
		Try
			dat = JsonObject(app.LoadString(path))
		Catch e:JsonError
			Print "unable to load " + path
			Return
		End
		Local raceName:String = dat.GetString("race", "NULL")
		Local raceSex:String = dat.GetString("sex", "M")
		raceName = raceName.ToUpper()
		raceSex = raceSex.ToUpper()
		If raceName.Compare("NULL") = 0
			Print "missing race name " + path
			Return
		End
		If not data.Contains(raceName)
			data.Add(raceName, New StringMap<RaceDataExt>())
			dataP.Add(raceName, New StringMap<Bool>())
		End
		If dataP.Get(raceName).Contains(raceSex)
			Print "Race " + raceName + "-" + raceSex + " is protected"
			Return
		End
		If mode = PackageManager.SET
			If Not data.Get(raceName).Set(raceSex, New RaceDataExt(dat))
				Print "Race " + raceName + "-" + raceSex + " was overwritten"
			End
		Else If mode = PackageManager.ADD
			If Not data.Get(raceName).Add(raceSex, New RaceDataExt(dat))
				Print "Race " + raceName + "-" + raceSex + " already exists"
			End
		Else If mode = PackageManager.LOCK
			If Not data.Get(raceName).Set(raceSex, New RaceDataExt(dat))
				Print "Race " + raceName + "-" + raceSex + " was overwritten"
			End
			dataP.Get(raceName).Add(raceSex, True)
		End
	End
	Function GetRace:RaceData(name:String = "ANY", sex:String = "ANY", tag:String = "ANY")
		Local t:Stack<String> = New Stack<String>()
		t.Push(tag)
		Return GetRace(name, sex, t)
	End
	Function TagMatch:Bool(race:String, sex:String, tags:JsonArray)
		Local t:Stack<String> = New Stack<String>()
		For Local temp:Int = 0 Until tags.Length()
			t.Push(tags.GetString(temp))
		Next
		Return TagMatch(race, sex, t)
	End
	Function TagMatch:Bool(race:String, sex:String, tags:Stack<String>)
		race = race.ToUpper()
		sex = sex.ToUpper()
		For Local x:StringMap<RaceDataExt> = EachIn data.Values()
			For Local y:RaceDataExt = EachIn x.Values()
				If (y.race.ToUpper() = race)
					If (y.sex.ToUpper() = sex)
						Return y.TagMatch(tags)
					End
				End
			Next
		Next
		Return False
	End
	
	
	Function GetRace:RaceData(name:String = "ANY", sex:String = "ANY", tags:Stack<String>)
		name = name.ToUpper()
		sex = sex.ToUpper()
		Local ss:Stack<RaceDataExt> = New Stack<RaceDataExt>()
		For Local x:StringMap<RaceDataExt> = EachIn data.Values()
			Local miniStack:Stack<RaceDataExt> = New Stack<RaceDataExt>()
			For Local y:RaceDataExt = EachIn x.Values()
				If (y.race.ToUpper() = name Or name = "ANY")
					If (y.sex.ToUpper() = sex Or sex = "ANY")
						If y.TagMatch(tags)
							miniStack.Push(y)
						End
					End
				End
			Next
			If Not miniStack.IsEmpty()
				ss.Push(miniStack.Get(Rnd(miniStack.Length())))
			End
		Next
		
		If ss.IsEmpty()
			Print "NO RACE FOUND"
			Return Null
		End
		Return New RaceData(ss.Get(Rnd(ss.Length())))
	End
End

Class RaceData
	Field race:String
	Field sex:String
	Field tags:Stack<String>
	Field sprite:String
	Field first_name:String
	Field last_name:String
	Field can_heal:bool
	Field perk_base:Int
	Field perk_random:Int
	Field base_health:Int
	Field base_melee:Int
	Field regen_time:Int
	Field oxygen_use:Int
	Field food_use:Int
	Field fire_damage:Int
	Field fire_defense:Int
	Field gamma_defense:Int
	Field ion_defense:int
	Method New(type:RaceDataExt)
		race = type.race
		sex = type.sex
		tags = type.tags
		first_name = type.GetFirstName()
		last_name = type.GetLastName()
		sprite = type.sprite
		can_heal = type.can_heal
		perk_base = type.perk_base
		perk_random = type.perk_random
		base_health = type.base_health
		base_melee = type.base_melee
		regen_time = type.regen_time
		oxygen_use = type.oxygen_use
		food_use = type.food_use
		fire_damage = type.fire_damage
		fire_defense = type.fire_defense
		ion_defense = type.ion_defense
		gamma_defense = type.gamma_defense
	End
End

Class RaceDataExt Extends RaceData
	Field first_name_list:Stack<String>
	Field last_name_list:Stack<String>
	Field playable:bool
	Method New(data:JsonObject)
		
		race = data.GetString("race", "NULL")
		sex = data.GetString("sex", "M")
		sprite = data.GetString("sprite", "NULL")
		playable = data.GetBool("playable", True)
		can_heal = data.GetBool("can_heal", True)
		perk_base = data.GetInt("perk_base", 1)
		perk_random = data.GetInt("perk_random", 2)
		base_health = data.GetInt("base_health", 100)
		base_melee = data.GetInt("base_melee", 2)
		regen_time = data.GetInt("regen_time", 600)
		oxygen_use = data.GetInt("oxygen_use", 1)
		food_use = data.GetInt("food_use", 1)
		fire_damage = data.GetInt("fire_damage", 1)
		fire_defense = data.GetInt("fire_defense", 0)
		ion_defense = data.GetInt("ion_defense", 0)
		gamma_defense = data.GetInt("gamma_defense", 0)
		
		
		first_name_list = New Stack<String>()
		Local arr:JsonArray = JsonArray(data.Get("first_name"))
		For Local temp:Int = 0 Until arr.Length()
			first_name_list.Push(arr.GetString(temp))
		Next
		
		last_name_list = New Stack<String>()
		arr = JsonArray(data.Get("last_name"))
		For Local temp:Int = 0 Until arr.Length()
			last_name_list.Push(arr.GetString(temp))
		Next
		
		tags = New Stack<String>()
		tags.Push("ANY")
		arr = JsonArray(data.Get("tags"))
		For Local temp:Int = 0 Until arr.Length()
			tags.Push(arr.GetString(temp).ToUpper())
		Next
	End
	
	Method TagMatch:Bool(tag:Stack<String>)
		Local found:Bool = False
		For Local x:Int = 0 Until tag.Length()
			Local t:String = tag.Get(x).ToUpper()
			If t.StartsWith("-")
				t = t.Replace("-", "")
				For Local y:Int = 0 Until tags.Length()
					If tags.Get(y) = t Then Return False
				Next
			Else
				For Local y:Int = 0 Until tags.Length()
					If tags.Get(y) = t Then found = True
				Next
			End
		Next
		Return found
	End
	
	Method GetFirstName:String()
		Local str:String = first_name_list.Get(Rnd(first_name_list.Length()))
		
		
		Return str
	End
	Method GetLastName:String()
		Local str:String = last_name_list.Get(Rnd(last_name_list.Length()))
		
		Return str
	End
End

