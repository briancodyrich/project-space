
Import space_game

Class PackageManager
	Const SET:Int = 0
	Const ADD:Int = 1
	Const LOCK:Int = 2
	Function Start:Void()
		Local configFile:String = app.LoadString("config.json")
		If configFile.Length() = 0
			Print("MISSING CONFIG FILE")
		Else
			Local config:JsonObject = New JsonObject(configFile)
			Local arr:JsonArray
			If config.Contains("packages")
				arr = JsonArray(config.Get("packages"))
				For Local temp:Int = 0 Until arr.Length()
					LoadPackage(arr.GetString(temp))
				Next
			End
		End
	End
	
	Function LoadPackage:Void(path:String)
		
		Local packageFile:String = app.LoadString(path)
		If packageFile.Length() = 0
			Print("MISSING PACKAGE FILE")
		Else
			Local package:JsonArray
			Try
				package = JsonArray(New JsonParser(packageFile).ParseValue())
			Catch e:JsonError
				Print "unable to load " + path
				Return
			End
			For Local temp:Int = 0 Until package.Length()
				Local config:JsonObject = JsonObject(package.Get(temp))
				Local arr:JsonArray
				Local mode:Int = SET
				If config.Contains("mode")
					If config.GetString("mode").Contains("add") Then mode = ADD
					If config.GetString("mode").Contains("lock") Then mode = LOCK
				End
				If config.Contains("scripts")
					arr = JsonArray(config.Get("scripts"))
					For Local x:Int = 0 Until arr.Length()
						LoadScript(arr.GetString(x), mode)
					Next
				End
				If config.Contains("items")
					arr = JsonArray(config.Get("items"))
					For Local x:Int = 0 Until arr.Length()
						LoadItems(arr.GetString(x), mode)
					Next
				End
				If config.Contains("entities")
					arr = JsonArray(config.Get("entities"))
					For Local x:Int = 0 Until arr.Length()
						LoadEntities(arr.GetString(x), mode)
					Next
				End
				If config.Contains("ships")
					arr = JsonArray(config.Get("ships"))
					For Local x:Int = 0 Until arr.Length()
						LoadShips(arr.GetString(x), mode)
					Next
				End
				If config.Contains("perks")
					arr = JsonArray(config.Get("perks"))
					For Local x:Int = 0 Until arr.Length()
						LoadPerks(arr.GetString(x), mode)
					Next
				End
				If config.Contains("races")
					arr = JsonArray(config.Get("races"))
					For Local x:Int = 0 Until arr.Length()
						LoadRaces(arr.GetString(x), mode)
					Next
				End
				If config.Contains("quests")
					arr = JsonArray(config.Get("quests"))
					For Local x:Int = 0 Until arr.Length()
						LoadQuests(arr.GetString(x), mode)
					Next
				End
				If config.Contains("constants")
					Constants.Set(JsonObject(config.Get("constants")))
				End
			Next
		End 
	End
	Function LoadScript(path:String, mode:int)
		If path.EndsWith("*")
			Local d:String[] = LoadDir(CurrentDir() + "/data/" + path.Replace("/*", ""), True)
			For Local temp:Int = 0 Until d.Length()
				ScriptManager.LoadScript(path.Replace("*", d[temp]), mode)
			Next
		Else
			ScriptManager.LoadScript(path, mode)
		End
	End
	Function LoadItems(path:String, mode:int)
		If path.EndsWith("*")
			Local d:String[] = LoadDir(CurrentDir() + "/data/" + path.Replace("/*", ""), True)
			For Local temp:Int = 0 Until d.Length()
				ItemLoader.LoadItems(path.Replace("*", d[temp]), mode)
			Next
		Else
			ItemLoader.LoadItems(path, mode)
		End
	End
	Function LoadPerks(path:String, mode:int)
		If path.EndsWith("*")
			Local d:String[] = LoadDir(CurrentDir() + "/data/" + path.Replace("/*", ""), True)
			For Local temp:Int = 0 Until d.Length()
				PerkLoader.LoadPerks(path.Replace("*", d[temp]), mode)
			Next
		Else
			PerkLoader.LoadPerks(path, mode)
		End
	End
	Function LoadShips(path:String, mode:int)
		If path.EndsWith("*")
			Local d:String[] = LoadDir(CurrentDir() + "/data/" + path.Replace("/*", ""), True)
			For Local temp:Int = 0 Until d.Length()
				ShipLoader.LoadShip(path.Replace("*", d[temp]), mode)
			Next
		Else
			ShipLoader.LoadShip(path, mode)
		End
	End
	Function LoadEntities(path:String, mode:int)
		If path.EndsWith("*")
			Local d:String[] = LoadDir(CurrentDir() + "/data/" + path.Replace("/*", ""), True)
			For Local temp:Int = 0 Until d.Length()
				EntityLoader.LoadEntity(path.Replace("*", d[temp]), mode)
			Next
		Else
			EntityLoader.LoadEntity(path, mode)
		End
	End
	Function LoadQuests(path:String, mode:int)
		If path.EndsWith("*")
			Local d:String[] = LoadDir(CurrentDir() + "/data/" + path.Replace("/*", ""), True)
			For Local temp:Int = 0 Until d.Length()
				QuestLoader.LoadQuest(path.Replace("*", d[temp]), mode)
			Next
		Else
			QuestLoader.LoadQuest(path, mode)
		End
	End
	Function LoadRaces(path:String, mode:int)
		If path.EndsWith("*")
			Local d:String[] = LoadDir(CurrentDir() + "/data/" + path.Replace("/*", ""), True)
			For Local temp:Int = 0 Until d.Length()
				RaceLoader.LoadRace(path.Replace("*", d[temp]), mode)
			Next
		Else
			RaceLoader.LoadRace(path, mode)
		End
	End
End