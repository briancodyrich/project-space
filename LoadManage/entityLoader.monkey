Strict

Import space_game

Class EntityLoader
	Global data:StringMap<StringMap<JsonObject>> = New StringMap<StringMap<JsonObject>>()
	Global dataP:StringMap<StringMap<Bool>> = New StringMap<StringMap<Bool>>()
	Function LoadEntity:Void(path:String, mode:int)
		Local dat:JsonObject
		Try
			dat = JsonObject(app.LoadString(path))
		Catch e:JsonError
			Print "unable to load " + path
			Return
		End
		Local eName:String = StripDir(path)
		Local faction:String = dat.GetString("faction", "NULL")
		faction = faction.ToUpper()
		If faction.Compare("NULL") = 0
			Print "missing faction data " + path
			Return
		End
		If not data.Contains(faction)
			data.Add(faction, New StringMap<JsonObject>())
			dataP.Add(faction, New StringMap<Bool>())
		End
		If dataP.Get(faction).Contains(eName)
			Print "Entity " + eName + " in faction " + faction + " is protected"
			Return
		End
		If mode = PackageManager.SET
			If Not data.Get(faction).Set(eName, dat)
				Print "Entity " + eName + " in faction " + faction + " was overwritten"
			End
		Else If mode = PackageManager.ADD
			If Not data.Get(faction).Add(eName, dat)
				Print "Entity " + eName + " in faction " + faction + " already exists"
			End
		Else If mode = PackageManager.LOCK
			If Not data.Get(faction).Set(eName, dat)
				Print "Entity " + eName + " in faction " + faction + " was overwritten"
			End
			dataP.Get(faction).Add(eName, True)
		End
	End
	
	Function GetFactionList:Stack<String>()
		Local ss:Stack<String> = New Stack<String>
		For Local s:String = EachIn data.Keys()
			ss.Push(s)
		Next
		Return ss
	End
	Function GetMemberList:Stack<String>(faction:String)
		Local ss:Stack<String> = New Stack<String>()
		faction = faction.ToUpper()
		If data.Contains(faction)
			Local dat:StringMap<JsonObject> = data.Get(faction)
			For Local s:String = EachIn dat.Keys()
				ss.Push(s)
			Next
		End
		Return ss
	End
	Function GetMember:JsonObject(faction:String)
		Local o:Stack<JsonObject> = New Stack<JsonObject>()
		faction = faction.ToUpper()
		If data.Contains(faction)
			Local dat:StringMap<JsonObject> = data.Get(faction)
			For Local s:JsonObject = EachIn dat.Values()
				o.Push(s)
			Next
			If o.Length() > 0
				Return o.Get(Rnd(o.Length()))
			End
		End
		Return Null
	End
	Function GetMember:JsonObject(faction:String, type:String, level:int)
		Local o:Stack<JsonObject> = New Stack<JsonObject>()
		faction = faction.ToUpper()
		type = type.ToUpper()
		If data.Contains(faction)
			Local dat:StringMap<JsonObject> = data.Get(faction)
			For Local s:JsonObject = EachIn dat.Values()
				If level >= s.GetInt("min_level", 100000) And level <= s.GetInt("max_level", -100000)
					If s.GetString("type", "NORMAL") = type Or type = "ANY"
						o.Push(s)
					End
				End
			Next
			If o.Length() > 0
				Return o.Get(Rnd(o.Length()))
			End
		End
		Return Null
	End
	Function GetMemberList:Stack<JsonObject>(faction:String, type:String, level:int)
		Local o:Stack<JsonObject> = New Stack<JsonObject>()
		faction = faction.ToUpper()
		type = type.ToUpper()
		If data.Contains(faction)
			Local dat:StringMap<JsonObject> = data.Get(faction)
			For Local s:JsonObject = EachIn dat.Values()
				If level >= s.GetInt("min_level", 100000) And level <= s.GetInt("max_level", -100000)
					If s.GetString("type", "NORMAL") = type Or type = "ANY"
						o.Push(s)
					End
				End
			Next
			Return o
		End
		Return Null
	End
	
End