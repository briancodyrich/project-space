Strict

Import space_game

Class ItemLoader
	Global data:StringMap<JsonObject> = New StringMap<JsonObject>()
	Global dataP:StringMap<Bool> = New StringMap<Bool>()
	Function LoadItems:Void(path:String, mode:int)
		Local arr:JsonArray
		Try
			arr = JsonArray(New JsonParser(app.LoadString(path)).ParseValue())
		Catch e:JsonError
			Print "unable to load " + path
			Return
		End
		
		For Local temp:Int = 0 Until arr.Length()
			Local obj:JsonObject = JsonObject(arr.Get(temp))
			Local name:String = obj.GetString("id")
			If dataP.Contains(name)
				Print "Item " + name + " is protected"
				Return
			End
			If mode = PackageManager.SET
				If Not data.Set(obj.GetString("id"), obj)
					Print "item " + obj.GetString("id") + " was overwritten"
				End
			Else If mode = PackageManager.ADD
				If Not data.Add(obj.GetString("id"), obj)
					Print "item " + obj.GetString("id") + " already exsists"
				End
			Else If mode = PackageManager.LOCK
				If Not data.Set(obj.GetString("id"), obj)
					Print "item " + obj.GetString("id") + " was overwritten"
				End
				dataP.Add(name, True)
			End
		Next
	End
	
	Function GetItem:JsonObject(id:String)
		Return data.Get(id)
	End
	
	Function DoesItemExist:Bool(id:String)
		Return data.Contains(id)
	End
	Function GetItemList:ItemStack(type:Int = -1)
		Local ss:ItemStack = New ItemStack()
		For Local s:JsonObject = EachIn data.Values()
			Local tt:Int = GetItemType(s.GetString("type", "null"))
			If (type = -1 Or tt = type) And s.GetInt("stack") <> - 1
				ss.Push(New ItemData(s.GetString("id"), tt))
			End
		Next
		ss.Sort()
		Return ss
	End
	Function GetItemType:Int(type:String)
		If type = "REACTOR" Then Return Item.REACTOR
		If type = "BATTERY" Then Return Item.BATTERY
		If type = "RADIATOR" Then Return Item.RADIATOR
		If type = "VENT" Then Return Item.VENT
		If type = "OXYGEN_GENERATOR" Then Return Item.OXYGENGEN
		If type = "ENGINE" Then Return Item.ENGINE
		If type = "SHIELD" Then Return Item.SHIELD
		If type = "DODGE" Then Return Item.DODGE
		If type = "HEALTH" Then Return Item.HEALTH
		If type = "COOLANT_TANK" Then Return Item.COOLANT_TANK
		If type = "COMPUTER" Then Return Item.COMPUTER
		If type = "HARDPOINT" Then Return Item.HARDPOINT
		If type = "SOLAR" Then Return Item.SOLAR
		If type = "FUEL" Then Return Item.FUEL
		If type = "ANTIMATTER" Then Return Item.ANTIMATER
		If type = "FOOD" Then Return Item.FOOD
		If type = "SCRAP" Then Return Item.SCRAP
		If type = "MINING_DRONE" Then Return Item.MINING_DRONE
		If type = "SYSTEM" Then Return Item.SYSTEM
		If type = "METAL" Then Return Item.METAL
		If type = "WEAPON" Then Return Item.WEAPON
		Return Item.NULLTYPE
	End
	Function GetPartList:ItemStack()
		Local ss:ItemStack = New ItemStack()
		For Local s:JsonObject = EachIn data.Values()
			Local type:Int = GetItemType(s.GetString("type"))
			If type < Item.FUEL And type > 0 and type <> Item.SYSTEM
				ss.Push(New ItemData(s.GetString("id"), type))
			End
		Next
		ss.Sort()
		Return ss
	End
	Function GetModList:ItemStack()
		Local ss:ItemStack = New ItemStack()
		For Local s:JsonObject = EachIn data.Values()
			Local type:Int = GetItemType(s.GetString("type"))
			If type = Item.WEAPON OR type = Item.SYSTEM
				ss.Push(New ItemData(s.GetString("id"), type))
			End
		Next
		ss.Sort()
		Return ss
	End
	
	Function GetEquipList:ItemStack()
		Local ss:ItemStack = New ItemStack()
		For Local s:JsonObject = EachIn data.Values()
			Local type:Int = GetItemType(s.GetString("type"))
			If (type < Item.FUEL And type > 0 or type = Item.WEAPON) And s.GetInt("stack", -1) <> - 1
				ss.Push(New ItemData(s.GetString("id"), type))
			End
		Next
		ss.Sort()
		Return ss
	End
		
	Function GetQualityLevel:Int(tech:Float)
		Local lev:Int = 0
		For Local x:Int = 0 Until (tech * 5) + 1
			Local r:int = Rnd(0, 10000)
			If r < 39 Then lev = Max(0, lev)
			If r < 78 Then lev = Max(7, lev)   '1/256
			If r < 156 Then lev = Max(6, lev)  '1/128
			If r < 312 Then lev = Max(5, lev)  '1/64
			If r < 625 Then lev = Max(4, lev)  '1/32
			If r < 1250 Then lev = Max(3, lev) '1/16
			If r < 2500 Then lev = Max(2, lev) '1/8
			If r < 5000 Then lev = Max(1, lev) '1/4
			lev = Max(0, lev)             '1/2  ---- actually is 1/1.985
		Next
		Return lev
	End
End

Class ItemData
	Field id:String
	Field type:Int
	Method New(id:String, type:Int)
		Self.id = id
		Self.type = type
	End
End

Class ItemStack Extends Stack<ItemData>
	Method New(data:ItemData[])
		Super.New( data )
	End

	Method Compare:Int(lhs:ItemData, rhs:ItemData)
		If lhs.type > rhs.type Then Return - 1
		If lhs.type < rhs.type Then Return 1
		Return 0
	End

End
