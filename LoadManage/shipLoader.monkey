Strict

Import space_game

Class ShipLoader
	Global data:StringMap<JsonObject> = New StringMap<JsonObject>()
	Global dataP:StringMap<Bool> = New StringMap<Bool>()
	Function LoadShip:Void(path:String, mode:int)
		Local arr:JsonArray
		Try
			arr = JsonArray(New JsonParser(app.LoadString(path)).ParseValue())
		Catch e:JsonError
			Print "unable to ship " + path
		End
		Local name:String = StripDir(path)
		If path.EndsWith(".ship")
			If dataP.Contains(name)
				Print("ship " + name + " is proteteted")
				Return
			End
			If mode = PackageManager.SET
				If Not (data.Set(name, New JsonObject(app.LoadString(path))))
					Print name + " has been overwritten"
				End
			End
			If mode = PackageManager.ADD
				If Not (data.Add(name, New JsonObject(app.LoadString(path))))
					Print name + " already exists"
				End
			End
			If mode = PackageManager.LOCK
				If Not (data.Set(name, New JsonObject(app.LoadString(path))))
					Print name + " has been overwritten"
				End
				dataP.Add(name, True)
			End
		Else
			Print "unknown ship format " + path
		End
	End
	
	Function GetShip:JsonObject(name:String)
		Print name
		Return data.Get(name)
	End
	
	Function DoesShipExist:Bool(name:String)
		Return data.Contains(name)
	End
	
End