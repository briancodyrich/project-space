Strict


Import space_game

Class ScriptManager Abstract
	Const DEBUG_PRINT:Bool = True
	Global current:float
	
	
	Const DEBUG_CHOOSE:Bool = False
	Const BUTTON_START:Int = 0
	Const BUTTON_WIDTH:Int = 160
	Const BUTTON_HEIGHT:Int = 56
	Const BUTTON_SPACING:Int = 200
	Global debugStack:Stack<PackedScript> = New Stack<PackedScript>()
	Global debugBtn:Stack<MenuButton> = New Stack<MenuButton>()
	Global debugText:Stack<String> = New Stack<String>()
	
	Global ess:StringMap<JsonArray> = New StringMap<JsonArray>()
	Global eso:StringMap<JsonObject> = New StringMap<JsonObject>()
	Global est:StringMap<JsonObject> = New StringMap<JsonObject>()
	Global say:StringMap<JsonArray> = New StringMap<JsonArray>()
	
	Global essP:StringMap<Bool> = New StringMap<Bool>()
	Global esoP:StringMap<Bool> = New StringMap<Bool>()
	Global estP:StringMap<Bool> = New StringMap<Bool>()
	Global sayP:StringMap<Bool> = New StringMap<Bool>()
	
	Function LoadScript:Void(path:String, mode:int)
		Local name:String = StripDir(path)
		If path.EndsWith(".eso")
			If esoP.Contains(name)
				Print("script " + name + " is proteteted")
				Return
			End
			If mode = PackageManager.SET
				If Not (eso.Set(name, New JsonObject(app.LoadString(path))))
					Print name + " has been overwritten"
				End
			End
			If mode = PackageManager.ADD
				If Not (eso.Add(name, New JsonObject(app.LoadString(path))))
					Print name + " already exists"
				End
			End
			If mode = PackageManager.LOCK
				If Not (eso.Set(name, New JsonObject(app.LoadString(path))))
					Print name + " has been overwritten"
				End
				esoP.Add(name, True)
			End
		Else If path.EndsWith(".ess")
			If essP.Contains(name)
				Print("script " + name + " is proteteted")
				Return
			End
			Local data:JsonArray
			Try
				data = JsonArray(New JsonParser(app.LoadString(path)).ParseValue())
			Catch e:JsonError
				Print "unable to load " + name
				Return
			End
			If esoP.Contains(name)
				Print("script " + name + " is proteteted")
				Return
			End
			If mode = PackageManager.SET
				If Not (ess.Set(name, data))
					Print name + " has been overwritten"
				End
			End
			If mode = PackageManager.ADD
				If Not (ess.Add(name, data))
					Print name + " already exists"
				End
			End
			If mode = PackageManager.LOCK
				If Not (ess.Set(name, data))
					Print name + " has been overwritten"
				End
				essP.Add(name, True)
			End
		Else If path.EndsWith(".txt")
			If sayP.Contains(name)
				Print("say " + name + " is proteteted")
				Return
			End
			Local data:JsonArray
			Try
				data = JsonArray(New JsonParser(app.LoadString(path)).ParseValue())
			Catch e:JsonError
				Print "unable to load " + name
				Return
			End
			If mode = PackageManager.SET
				If Not (say.Set(name, data))
					Print name + " has been overwritten"
				End
			End
			If mode = PackageManager.ADD
				If Not (say.Add(name, data))
					Print name + " already exists"
				End
			End
			If mode = PackageManager.LOCK
				If Not (say.Set(name, data))
					Print name + " has been overwritten"
				End
				sayP.Add(name, True)
			End
		Else If path.EndsWith(".est")
			If estP.Contains(name)
				Print("script " + name + " is proteteted")
				Return
			End
			If mode = PackageManager.SET
				If Not (est.Set(name, New JsonObject(app.LoadString(path))))
					Print name + " has been overwritten"
				End
			End
			If mode = PackageManager.ADD
				If Not (est.Add(name, New JsonObject(app.LoadString(path))))
					Print name + " already exists"
				End
			End
			If mode = PackageManager.LOCK
				If Not (est.Set(name, New JsonObject(app.LoadString(path))))
					Print name + " has been overwritten"
				End
				estP.Add(name, True)
			End
		Else
			Print "Unknown type: " + path
		End
	End
	
	Function GetObject:JsonObject(type:String)
		Return eso.Get(type)
	End
	
	Function GetSay:Stack<String>(type:String)
		Local r:Stack<String> = New Stack<String>()
		Local sayArr:JsonArray
		If say.Contains(type)
			sayArr = say.Get(type)
		Else
			ThrowErrorMessage("Missing say", type)
			Return Null
		End
		If sayArr.Length() > 0
			Local talks:JsonArray = JsonArray(sayArr.Get(Rnd(sayArr.Length())))
			If talks.Length() > 0
				For Local temp:Int = 0 Until talks.Length()
					r.Push(talks.GetString(temp))
				Next
				Return r
			Else
				ThrowErrorMessage("Missing text", type)
				Return Null
			End
		Else
			ThrowErrorMessage("Missing text", type)
			Return Null
		End
		Return Null
	End
	Function GetScript:Stack<Script>(type:String)
		Local actorStack:Stack<Crew>
		If DEBUG_PRINT
			Print("get script: " + type)
		End
		Local scriptStack:Stack<PackedScript> = New Stack<PackedScript>()
		Local scriptArray:JsonArray
		current = 0
		If type.EndsWith(".ess")
			If ess.Contains(type)
				scriptArray = ess.Get(type)
			Else
				ThrowErrorMessage("Missing script", type)
				Return Null
			End
		Else If type.EndsWith(".est")
			Local template:JsonObject
			
			If est.Contains(type)
				template = est.Get(type)
			Else
				ThrowErrorMessage("Missing template", type)
				Return Null
			End
			Try
				scriptArray = JsonArray(template.Get("template"))
			Catch e:JsonError
				ThrowErrorMessage("Unable to parse template", type)
				Return Null
			End
			actorStack = GetActorStack(template.GetString("select", "NULL"), template.GetString("stat", "NULL"))
			If actorStack = Null
				ThrowErrorMessage("invalid template argeuments", type)
				Return Null
			End
		Else
			ThrowErrorMessage("Unknown filetype", type)
			Return Null
		End
		Local min:Int = 0
		Local max:Int = 1
		If actorStack <> Null
			Print("actors: " + actorStack.Length())
			max = actorStack.Length()
		End
		
		Local rStack:Stack<Script> = New Stack<Script>()
		Repeat
			Local c:Crew = Null
			If actorStack <> Null
				c = actorStack.Get(min)
				Print c.first_name + " " + c.last_name
			End
			min += 1
			For Local temp:Int = 0 Until scriptArray.Length()
				scriptStack.Push(New PackedScript(JsonObject(scriptArray.Get(temp)), c))
			Next
			If scriptStack.Length() > 1
				Local rand:Float = Rnd(current)
				If DEBUG_PRINT
					Print("rand: " + rand)
				End
				For Local temp:Int = 0 Until scriptStack.Length()
					If scriptStack.Get(temp).inRange(rand)
						If DEBUG_PRINT
							Print(scriptStack.Get(temp).name + " selected")
						End
						rStack.Push(scriptStack.Get(temp).ParseScript().ToArray())
					End
				Next
			Else If scriptStack.Length() = 1
				If DEBUG_PRINT
					Print(scriptStack.Get(0).name + " selected")
				End
				rStack.Push(scriptStack.Get(0).ParseScript().ToArray())
			End
		Until (min = max)
		Return rStack
	End
	
	Function DebugUpdate:Void()
		For Local temp:Int = 0 Until debugBtn.Length()
			debugBtn.Get(temp).Poll()
			If debugBtn.Get(temp).hit
				Player.interaction.scripts = debugStack.Get(temp).ParseScript()
				debugBtn.Clear()
				debugText.Clear()
				debugStack.Clear()
				Return
			End
		Next
	End
	
	Function DebugRender:Void()
		For Local temp:Int = 0 Until debugBtn.Length()
			debugBtn.Get(temp).RenderScaled(0, 0, 0.75)
			Game.white_font.Draw(debugText.Get(temp), debugBtn.Get(temp).x + debugBtn.Get(temp).w / 2, debugBtn.Get(temp).y + debugBtn.Get(temp).h / 6, 0.5)
		Next
	End
	
	Function GetActorStack:Stack<Crew>(sel:String, by:String)
		If sel.Contains("ALL")
			Return Player.ship.crew
		End
		Return Null
	End
	

	Function GetMod:Float(type:String, actor:Crew)
		Select type
			Case ""
				Return 1.0
			Case "NULL"
				Return 1.0
			Case "PIRATE_AREA"
				Return 1.0
			Case "ESCAPE_CHANCE"
				Local v:Float = GetSigmoid(Player.ship.GetThrust(, True) - Player.ship.targetShip.GetThrust(, True), -4, 3) * 100
				If Player.ship.GetThrust(, True) = 0 Then v = 0
				If v > Rnd(100) Then Return 1.0
				Return 0.0
		End
		
		If actor <> Null 'these are type that are tied to a crew member
			Select type
				Case "BRAWL_WIN"
					Local player_health:float
					Local player_damage:float
					Local target_health:float
					Local target_damage:float
					
					For Local temp:Int = 0 Until Player.ship.crew.Length()
						player_health += Player.ship.crew.Get(temp).health
						player_damage += Player.ship.crew.Get(temp).GetCombatDamage()
					Next
					If Player.ship.targetShip <> Null
						For Local temp:Int = 0 Until Player.ship.targetShip.crew.Length()
							target_health += Player.ship.targetShip.crew.Get(temp).health
							target_damage += Player.ship.targetShip.crew.Get(temp).GetCombatDamage()
						Next
					Else
						Return 1.0
					End
					Print "player: " + player_health + "/" + player_damage
					Print "target: " + target_health + "/" + target_damage
					Local odds:Float = ( (player_health / target_damage) / (target_health / player_damage))
					Print "win odds: " + odds
					Return odds
				Case "BRAWL_LOSE"
					Local player_health:float
					Local player_damage:float
					Local target_health:float
					Local target_damage:float
					
					For Local temp:Int = 0 Until Player.ship.crew.Length()
						player_health += Player.ship.crew.Get(temp).health
						player_damage += Player.ship.crew.Get(temp).GetCombatDamage()
					Next
					If Player.ship.targetShip <> Null
						For Local temp:Int = 0 Until Player.ship.targetShip.crew.Length()
							target_health += Player.ship.targetShip.crew.Get(temp).health
							target_damage += Player.ship.targetShip.crew.Get(temp).GetCombatDamage()
						Next
					Else
						Return 1.0
					End
					Print "player: " + player_health + "/" + player_damage
					Print "target: " + target_health + "/" + target_damage
					Local odds:Float = 1.0 / ( (player_health / target_damage) / (target_health / player_damage))
					Print "lose odds: " + odds
					Return odds
			End
		End
		
		
		
		Print("MOD " + type + " NOT FOUND")
		Return 1.0
	End
	Function GetValue:Int(type:String)
		Select type
			Case "<S_CREDITS>"
				Return Min(Player.getCredits(), Int(Player.ship.GetValue(Player.map.economy) * 0.02))
		End
		Return -1
	End
	
	Function GetCrew:Crew(stat:String)
		Local c:Crew
		Select stat
			Case "<CREW_TOUGHNESS>"
				Local hp:Int = 0
				For Local temp:Int = 0 Until Player.ship.crew.Length()
					If Player.ship.crew.Get(temp).GetMaxHp() > hp
						c = Player.ship.crew.Get(temp)
					End
				Next
		End
		Return c
	End
	
End

Class Script
	'Error Codes
	Const BAD_CODE:Int = -3
	Const MISSING_TYPE:Int = -2
	Const MISSING_KEY:Int = -1
	
	
	'System Calls
	Const RUN:Int = 101 		'Load Script
	Const THROW_ERROR:Int = 102 'throw error message
	Const GOTO:Int = 103 		'Load screen
	Const WAIT:Int = 104		 'Wait
	Const PRINTLN:Int = 105
	Const DAMAGE:Int = 106 'run damage script
	Const REMOVE:Int = 107 ' remove something
	Const CANCEL:Int = 108 ' stop doing something
	Const SHUTDOWN:Int = 109 ' clears all scripts
	
	
	'Comms Screen
	Const SAY:Int = 200
	Const LOCK_SCREEN:Int = 201
	Const UNLOCK_SCREEN:Int = 202
	Const LOAD_SHIP:Int = 203
	Const ADD_BUTTON:Int = 204
	Const NULL_BUTTON:Int = 205
	Const CLEAR_BUTTON:Int = 206
	Const BOARDING_COMBAT:Int = 207
	Const LOAD_LOOT:Int = 208
	
	Field key:Int
	Field name:String
	Field actor:Crew
	Field values:Stack<String>
	Method New(name:String, data:JsonArray, actor:Crew)
		Self.name = name
		Self.actor = actor
		key = MISSING_KEY
		values = Null
		For Local temp:int = 0 Until data.Length()
			Local v:String = data.GetString(temp)
			If temp = 0
				key = GetCode(v)
			Else
				If values = Null
					values = New Stack<String>()
				End
				If key = SAY And v.EndsWith(".txt")
					Local add:Stack<String> = ScriptManager.GetSay(v)
					If add <> Null
						For Local x:Int = 0 Until add.Length()
							values.Push(Format(add.Get(x)))
						Next
					End
				Else
					values.Push(Format(v))
				End
			End
		Next
	End
	
	Method Format:String(str:String)
		If str.Contains("<S_CREDITS>")
			Return str.Replace("<S_CREDITS>", ScriptManager.GetValue("<S_CREDITS>"))
		End
		If actor <> Null 'actor scripts
			If str.Contains("<NAME>")
				Return str.Replace("<NAME>", actor.first_name)
			End
		End
		Return str
	End
	
	
	Method GetCode:Int(str:String)
		Select str.ToLower()
			'System Calls
			Case "run"
				Return RUN
			Case "goto"
				Return GOTO
			Case "throw"
				Return THROW_ERROR
			Case "wait"
				Return WAIT
			Case "println"
				Return PRINTLN
			Case "damage"
				Return DAMAGE
			Case "remove"
				Return REMOVE
			Case "cancel"
				Return CANCEL
			Case "shutdown"
				Return SHUTDOWN
				
				
			'Comms screen
			Case "load ship"
				Return LOAD_SHIP
			Case "load loot"
				Return LOAD_LOOT
			Case "say"
				Return SAY
			Case "lock"
				Return LOCK_SCREEN
			Case "unlock"
				Return UNLOCK_SCREEN
			Case "add button"
				Return ADD_BUTTON
			Case "null button"
				Return NULL_BUTTON
			Case "clear button"
				Return CLEAR_BUTTON
			Case "boarding combat"
				Return BOARDING_COMBAT
			
			Default
				If ScriptManager.DEBUG_PRINT
					Print "bad key: " + str
				End
				Return BAD_CODE
		End
	End
	
	Method minArgs:bool(num:Int)
		If values.Length() >= num Then Return True
		Return False
	End
	Method exactArgs:bool(num:Int)
		If values.Length() = num Then Return True
		Return False
	End
End

Class PackedScript
	Field rangeStart:Float
	Field rangeStop:Float
	Field name:String
	Field payload:JsonArray
	Field actor:Crew
	Method New(data:JsonObject, actor:Crew = Null)
		Self.actor = actor
		rangeStart = ScriptManager.current
		rangeStop = rangeStart + (data.GetInt("odds", 1) * ScriptManager.GetMod(data.GetString("mod", "NULL"), actor))
		ScriptManager.current = rangeStop
		
		name = data.GetString("name", "NULL")
		payload = JsonArray(data.Get("run"))
		If ScriptManager.DEBUG_PRINT
			Print name + "   " + rangeStart + "/" + rangeStop
		End
	End
	Method inRange:Bool(num:Float)
		If num >= rangeStart And num <= rangeStop
			Return True
		End
		Return False
	End
	Method ParseScript:Stack<Script>()
		Local s:Stack<Script> = New Stack<Script>()
		For Local temp:Int = 0 Until payload.Length()
			s.Push(New Script(name, JsonArray(payload.Get(temp)), actor))
		Next
		Return s
	End
End
