Strict

Import space_game

Class QuestLoader
	Global data:StringMap<JsonObject> = New StringMap<JsonObject>()
	Global dataP:StringMap<Bool> = New StringMap<Bool>()
	Function LoadQuest:Void(path:String, mode:int)
		Local dat:JsonObject
		Try
			dat = JsonObject(app.LoadString(path))
		Catch e:JsonError
			Print "unable to load " + path
			Return
		End
		Local qName:String = StripDir(path)
		If dataP.Contains(qName)
			Print "quest " + qName + " is protected"
			Return
		End
		If mode = PackageManager.SET
			If Not data.Set(qName, dat)
				Print "quest " + qName + " was overwritten"
			End
		Else If mode = PackageManager.ADD
			If Not data.Add(qName, dat)
				Print "quest " + qName + " already exsists"
			End
		Else If mode = PackageManager.LOCK
			If Not data.Set(qName, dat)
				Print "quest " + qName + " was overwritten"
			End
			dataP.Add(qName, True)
		End
	End
	Function GetQuestList:Stack<JsonObject>(level:int)
		Local quests:Stack<JsonObject> = New Stack<JsonObject>
		For Local s:JsonObject = EachIn data.Values()
		
		Next
		Return quests
	End
	Function GetQuest:JsonObject(name:String)
		If Not data.Contains(name)
			Return Null
		End
		Local d:JsonObject = JsonObject(data.Get(name).ToJson())
		d.SetString("name", name)
		Return d
	End
End
