Strict

Import space_game

Class PerkLoader
	Global data:StringMap<JsonObject> = New StringMap<JsonObject>()
	Global dataP:StringMap<Bool> = New StringMap<Bool>()
	Function LoadPerks:Void(path:String, mode:int)
		Local arr:JsonArray
		Try
			arr = JsonArray(New JsonParser(app.LoadString(path)).ParseValue())
		Catch e:JsonError
			Print "unable to load " + path
			Return
		End
		
		For Local temp:Int = 0 Until arr.Length()
			Local obj:JsonObject = JsonObject(arr.Get(temp))
			Local name:String = obj.GetString("name")
			If dataP.Contains(name)
				Print "Item " + name + " is protected"
				Return
			End
			If mode = PackageManager.SET
				If Not data.Set(obj.GetString("name"), obj)
					Print "item " + obj.GetString("name") + " was overwritten"
				End
			Else If mode = PackageManager.ADD
				If Not data.Add(obj.GetString("name"), obj)
					Print "item " + obj.GetString("name") + " already exsists"
				End
			Else If mode = PackageManager.LOCK
				If Not data.Set(obj.GetString("name"), obj)
					Print "item " + obj.GetString("name") + " was overwritten"
				End
				dataP.Add(name, True)
			End
		Next
	End
	Function GetPerkList:Stack<JsonObject>(race:String, sex:String)
		Local perks:Stack<JsonObject> = New Stack<JsonObject>
		For Local s:JsonObject = EachIn data.Values()
			If RaceLoader.TagMatch(race, sex, JsonArray(s.Get("tags")))
				perks.Push(s)
			End
		Next
		Return perks
	End
	Function GetPerk:JsonObject(name:String)
		Return data.Get(name)
	End
End
