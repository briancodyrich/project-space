Strict

#GLFW_WINDOW_TITLE="SpaceGame"
#GLFW_WINDOW_WIDTH=1280
#GLFW_WINDOW_HEIGHT=720
#GLFW_WINDOW_RESIZABLE=True
#GLFW_WINDOW_FULLSCREEN=false

#OPENGL_DEPTH_BUFFER_ENABLED=True

#TEXT_FILES="*.txt|*.xml|*.json|*.ess|*.eso|*.est|*.esp|*.ship|*.fnt"


Import mojo

'Internal engine components
Import jimlib ''Import all jimlib modules
Import angelfont_exts ''Still using CSFont as a class name


'Ui
Import UIStuff.popup
Import UIStuff.panel
Import UIStuff.orders
Import UIStuff.powerbar
Import UIStuff.contextMenu
Import UIStuff.logviewer
Import UIStuff.systemData
Import UIStuff.shipstatviewer
Import UIStuff.info.infopane
Import UIStuff.info.simpleship
Import UIStuff.info.crewui
Import UIStuff.questViewer


''Import Screens
Import GameScreens.debug.DebugItems
Import GameScreens.debug.Editor
Import GameScreens.debug.EditorItems
Import GameScreens.debug.Equipment
Import GameScreens.debug.shipstatus

Import GameScreens.dock.dockingReport
Import GameScreens.dock.crewDocked
Import GameScreens.dock.shipDocked
Import GameScreens.dock.shipStore
Import GameScreens.dock.storeScreen
Import GameScreens.dock.hireCrew
Import GameScreens.dock.jobBoard

Import GameScreens.event.combatScreen
Import GameScreens.event.Communication
Import GameScreens.event.crate
Import GameScreens.event.Death
Import GameScreens.event.EndCombat
Import GameScreens.event.Mining
Import GameScreens.map.Hyperspace
Import GameScreens.map.SystemMap
Import GameScreens.map.GalaxyMap
Import GameScreens.menu.crewMenu
Import GameScreens.menu.shipMenu
Import GameScreens.Splash
Import GameScreens.Start
Import GameScreens.NewGame
Import GameScreens.LoadGame

'Loaders
Import LoadManage.itemloader
Import LoadManage.packageManager
Import LoadManage.scriptManager
Import LoadManage.shipLoader
Import LoadManage.entityLoader
Import LoadManage.raceLoader
Import LoadManage.perkLoader
Import LoadManage.questLoader

''Import Ship data
Import ShipStuff.tile
Import ShipStuff.ship
Import ShipStuff.components
Import ShipStuff.status
Import ShipStuff.modspecial
Import ShipStuff.drone
Import ShipStuff.alarm
Import ShipStuff.Parts.reactor
Import ShipStuff.Parts.battery
Import ShipStuff.Parts.oxygen
Import ShipStuff.Parts.radiator
Import ShipStuff.Parts.engine
Import ShipStuff.Parts.vent
Import ShipStuff.Parts.tube
Import ShipStuff.Parts.shield
Import ShipStuff.Parts.coolant
Import ShipStuff.Parts.computer
Import ShipStuff.Parts.hardpoint
Import ShipStuff.Parts.solar
Import ShipStuff.Parts.extender
Import ShipStuff.Weapons.damagecodes
Import ShipStuff.Weapons.weapon
Import ShipStuff.Weapons.weaponloader
Import ShipStuff.Weapons.target
Import ShipStuff.Weapons.bolt

'Import reporting and stats
Import StatStuff.report
Import StatStuff.logger
Import StatStuff.shipStat
Import StatStuff.deltaItem
Import StatStuff.quest

'Items
Import Items.inventory
Import Items.item 
Import Items.modcodes
Import Items.leveledList

''Import Crew Stuff
Import CrewStuff.crew
Import CrewStuff.perks
Import CrewStuff.races
Import CrewStuff.equipment
Import CrewStuff.CrewAICodes
Import CrewStuff.pathfinding
Import CrewStuff.skills
Import CrewStuff.morale

'//particle stuff
Import GameStuff.Particle.photonManager
Import GameStuff.Particle.boxdata

'//Import Crew Stuff

Import GameStuff.debug
Import GameStuff.loader
Import GameStuff.settings
Import GameStuff.keybind
Import GameStuff.texteffect
Import GameStuff.namegen 

'//Import map stuff
Import MapStuff.System.hex
Import MapStuff.System.hexfield
Import MapStuff.System.hexobject
Import MapStuff.System.asteroid
Import MapStuff.Galaxy.economy
Import MapStuff.Galaxy.hypernode
Import MapStuff.Galaxy.hypernodemanager
Import MapStuff.mapdata
Import MapStuff.event

'// Ai
Import AI.taskmanager
Import AI.task
Import AI.aichoice

Import player

Global dt:DeltaTimer
Global Player:PlayerData
Global Settings:SettingsData




Function Main:Int()
	
	New Game()
	Return 0
End



Class Game Extends App
	Field letterbox:Image 'Lettebox image
	Const PRINT_CLICK:bool = True
	Global gold_font:CSFont
	Global white_font:CSFont
	Global plain_white_font:CSFont
	Field bg:Image
	
	
	'Generic image assets
	Global default_img:Image  'Placeholder image if something goes wrong
	
	'Screen switching / transitioning
	Global CurrentScreen:nScreen 'Current screen. Switching this forces a screen change and unloads the previous screen from RAM.		
	Global CurrentScreenNumber:Int  'This is set by the SwitchScreen function.
	Global LastScreenNumber:Int
	Global RootScreenNumber:int
	Global LastRootScreenNumber:Int
	Global FadeIn:ScaledFadeTransition, FadeOut:ScaledFadeTransition
	Global fadeState:Int  'Signals state for fade in/out. 0: None; 1: Out; 2-3: Loading; 4: In
	
	Global InputPaused:Bool = False
	Global Cursor:GamepadPointer = New GamepadPointer  'Game cursor, for UI elements!
	Global Keys:KeyBind = New KeyBind()
	
	Global ScreenToSwitchTo:Int 'Holding variable; next screen after transition
	Global SwitchCondition:String  'General argument holding variable for screen switch.  EG: Map location	
	
	Global Audio:AudioSystem
	Global UnderParticle:PhotonManager
	Global OverParticle:PhotonManager
	
	Global showFPS:Bool = True
	Global showPC:Bool = false
	Global lastMillisec:int
	Global lastFPS:int
	Global FPSCount:Int
	
	Global Swap:SwapSpace
	Global popup:Popup
	Global isFrozen:Bool

	Method OnCreate:Int()
		SetUpdateRate 60
		dt = New DeltaTimer(60)
		RandomizeSeed()
		initAutoScale(1920, 1080, True) 'Start autoscale
		Cursor.Init()
		Audio.Init()
		PackageManager.Start()
		LoadPhotonAssets()
		SoundEffect.Init()
	    Swap = New SwapSpace()
		UnderParticle = New PhotonManager(3000, True, True)
		OverParticle = New PhotonManager(3000, True, True)
		Tile.img_hull = LoadImage("ship/hull.png",, Image.MidHandle)
		Tile.img_floor = LoadImage("ship/floor.png",, Image.MidHandle)
		Tile.img_floor_DAMAGED_1 = LoadImage("ship/floor_DAMAGED_1.png",, Image.MidHandle)
		Tile.img_floor_DAMAGED_2 = LoadImage("ship/floor_DAMAGED_2.png",, Image.MidHandle)
		Tile.img_floor_DAMAGED_3 = LoadImage("ship/floor_DAMAGED_3.png",, Image.MidHandle)
		Tile.img_door_NS = LoadImage("ship/door_NS.png",, Image.MidHandle)
		Tile.img_door_NS_OPEN = LoadImage("ship/door_NS_OPEN.png",, Image.MidHandle)
		Tile.img_door_EW = LoadImage("ship/door_EW.png",, Image.MidHandle)
		Tile.img_door_EW_OPEN = LoadImage("ship/door_EW_OPEN.png",, Image.MidHandle)
		Tile.img_scaffolding = LoadImage("ship/scaffolding.png",, Image.MidHandle)
		Tile.img_air_lock_OPEN = LoadImage("ship/air_lock_OPEN.png",, Image.MidHandle)
		Tile.img_air_lock_CLOSED = LoadImage("ship/air_lock_CLOSED.png",, Image.MidHandle)
		Tile.img_walkway = LoadImage("ship/walkway.png",, Image.MidHandle)
		Tile.img_rock = LoadImage("ship/rock.png",, Image.MidHandle)
		Tile.img_shield = LoadImage("ship/shield.png",, Image.MidHandle)
		Tile.img_bed_N = LoadImage("ship/bed_N.png",, Image.MidHandle)
		Tile.img_bed_S = LoadImage("ship/bed_S.png",, Image.MidHandle)
		Tile.img_bed_E = LoadImage("ship/bed_E.png",, Image.MidHandle)
		Tile.img_bed_W = LoadImage("ship/bed_W.png",, Image.MidHandle)
		
		EquipmentManager.weapon_icon = LoadImage("slot_icons/icon_slot_weapon.png",, Image.MidHandle)
		EquipmentManager.system_icon = LoadImage("slot_icons/icon_slot_system.png",, Image.MidHandle)
		EquipmentManager.solar_icon = LoadImage("slot_icons/icon_slot_solar.png",, Image.MidHandle)
		EquipmentManager.shield_icon = LoadImage("slot_icons/icon_slot_shield.png",, Image.MidHandle)
		EquipmentManager.reactor_icon = LoadImage("slot_icons/icon_slot_reactor.png",, Image.MidHandle)
		EquipmentManager.engine_icon = LoadImage("slot_icons/icon_slot_engine.png",, Image.MidHandle)
		EquipmentManager.crew_icon = LoadImage("slot_icons/icon_slot_crew.png",, Image.MidHandle)
		EquipmentManager.coolant_icon = LoadImage("slot_icons/icon_slot_coolant.png",, Image.MidHandle)
		EquipmentManager.battery_icon = LoadImage("slot_icons/icon_slot_battery.png",, Image.MidHandle)
		
		
		
		Settings = New SettingsData()
		Loader.Init(Settings)
		
	'	Game.Keys.RebindKey(KEY_S, KEY_O)
	'	Game.Keys.RebindKey(KEY_D, KEY_E)
	'	Game.Keys.RebindKey(KEY_F, KEY_U)
	'	Game.Keys.RebindKey(KEY_I, KEY_I)
		bg = LoadImage("bg.jpg")
		letterbox = LoadImage("letterbox.png")
		default_img = LoadImage("default.png")
		
		Print "name gen test"
		Local n:NameGenerator = New NameGenerator(Seed)
		Print "start"
		For Local temp:Int = 0 Until 10
			Print n.GetName(temp)
		Next
		#rem
		Local xp:Int = 150
		Local count:Int = 0
		For Local temp:Int = 1 Until 11
			xp *= 2
			count += xp
			Print "(" + temp + ") =" + count
		Next
		#end
		FadeIn = New ScaledFadeTransition(0, 0, 0, True)
		FadeOut = New ScaledFadeTransition(0, 0, 0, False)
		CurrentScreen = New SplashScreen()   'Bootstrap the game.
		
		Return 0
	End

	Method OnUpdate:Int()
		Cursor.Poll()  'Updates cursor state and positions
		If PRINT_CLICK
			If Game.Cursor.Hit()
				Print Game.Cursor.x() + "/" + Game.Cursor.y()
			End
		End
		If Player <> Null And Player.galaxy <> Null And not Player.galaxy.genDone
			Player.galaxy.Bulid()
		End
		
		#If TARGET="glfw"
		If KeyHit(KEY_CLOSE)
			Stop()
		EndIf
		#End
		
		
		
		dt.UpdateDelta()
		If showFPS
			If Millisecs() - (lastMillisec) > 1000
				lastMillisec = Millisecs()
				lastFPS = FPSCount
				FPSCount = 0
			Else
				FPSCount += 1
			End
		End
		
		If Game.popup <> Null And Game.popup.isScreenFrozen()
			isFrozen = True
			Game.popup.Update()
			Return 0
		Else
			isFrozen = False
		End
		
		
		CurrentScreen.Update()

		'Update the transition faders.				
		FadeOut.Update()
		FadeIn.Update()
		
		Return 0
	End
	
	Method OnRender:Int()
		Cls()
		ScaleScreen()
		
		UpdateAsyncEvents()
		CurrentScreen.Render()  'Render the current screen.
		'Render and check the faders.
		Select fadeState
		  Case FadeStates.OUT
			If FadeOut.Finished Then  'Need to switch screens.
				FadeOut.RenderFullFrame()
				DoSwitch(ScreenToSwitchTo, SwitchCondition)
				fadeState = FadeStates.LOADING  'Prepare to switch to a fade-in next frame.
			End If
			
		  Case FadeStates.LOADING
			FadeOut.RenderFullFrame()
			fadeState = FadeStates.FINISHED_LOADING

		  Case FadeStates.FINISHED_LOADING
			FadeIn.Start(250)
			FadeIn.RenderFullFrame()
			FadeOut.Reset()		
			fadeState = FadeStates.IN
			
		  Case FadeStates.IN
			If FadeIn.Finished Then  'Return input to normal
				'InputPaused = False
				FadeIn.Reset()
				fadeState = 0
			End If
		End Select
		
		Cursor.Draw()
					
		FadeIn.Render()
		FadeOut.Render()  'This is below the check to ensure Finished frame is rendered at resource load.
		
	
		
		If Game.popup <> Null And Game.popup.isScreenFrozen()
			Game.popup.Render()
		End
		Return 0
	End
	
	Method DoTexturedLetterbox:Void()
		Local lbSize:Float = SlideAmount * ScaleFactor  'Letterbox size
	
		SetMatrix(1, 0, 0, 1, 0, 0); SetAlpha(1)
		SetScissor(0, 0, lbSize, DeviceHeight())  'Left Letterbox
		
		For Local y:Int = 0 To DeviceHeight() Step 512
			For Local x:Int = 0 To DeviceWidth() / 2 Step 256
				DrawImage(letterbox, x, y)
			Next
		Next
		
		SetScissor(DeviceWidth() -lbSize, 0, Ceil(lbSize), DeviceHeight())  'Right letterbox
		
		For Local y:Int = 0 To DeviceHeight() Step 512
			For Local x:Int = DeviceWidth() / 2 To DeviceWidth() Step 256
				DrawImage(letterbox, x, y)
			Next
		Next
		
		SetAlpha(0.2); SetColor(0, 0, 0)
		DrawRect(DeviceWidth() -lbSize, 4.5, 4.5, DeviceHeight())
		SetAlpha(1); SetColor(255, 255, 255)
		
	End Method	
	
	Function SwitchScreen:Void(ScreenNumber:Int, Condition:String = "") 'Starts the fader to switch, with a string arg (for maps)
		If Game.popup <> Null And Game.popup.isScreenFrozen()
			Return
		End
		InputPaused = True
		ScreenToSwitchTo = ScreenNumber 
		SwitchCondition = Condition
		FadeOut.Start(150)
		fadeState = FadeStates.OUT
	End Function
	
	Function SetRootScreen:Void(ScreenNumber:Int)
		If RootScreenNumber = ScreenNumber Then Return
		LastRootScreenNumber = RootScreenNumber
		RootScreenNumber = ScreenNumber
	End
	
	Function DoSwitch:Int(ScreenNumber:Int, Condition:String = "") 'Manual switch;  instant.  Try to avoid doing this
		CurrentScreen = Null  'Dereference screen.
		If ScreenNumber = Screens.Root
			ScreenNumber = RootScreenNumber
		End
		If ScreenNumber = Screens.LastRoot
			ScreenNumber = LastRootScreenNumber
		End
		If ScreenNumber = Screens.LastScreen
			ScreenNumber = LastScreenNumber
		End
		LastScreenNumber = CurrentScreenNumber
		Select ScreenNumber
			Case Screens.Splash 
				CurrentScreen = New SplashScreen()
			Case Screens.TestGame
				CurrentScreen = New CombatScreen()
			Case Screens.Editor
				CurrentScreen = New EditorScreen()
			Case Screens.ShipStatus
				CurrentScreen = New ShipStatusScreen()
			Case Screens.SystemMap
				CurrentScreen = New SystemMapScreen()
			Case Screens.Start
				CurrentScreen = New StartScreen()
			Case Screens.CrewStatusMenu
				CurrentScreen = New CrewMenuScreen()
			Case Screens.Equipment
				CurrentScreen = New EquipScreen()
			Case Screens.NewGame
				CurrentScreen = New NewGameScreen()
			Case Screens.DebugItems
				CurrentScreen = New DebugItemsScreen()
			Case Screens.EndCombat
				CurrentScreen = New EndCombatScreen()
			Case Screens.LoadGame
				CurrentScreen = New LoadGameScreen()
			Case Screens.GoodsStore
				CurrentScreen = New GoodsStoreScreen()
			Case Screens.PartsStore
				CurrentScreen = New PartsStoreScreen()
			Case Screens.ModStore
				CurrentScreen = New ModStoreScreen()
			Case Screens.Crate
				CurrentScreen = New CrateScreen()
			Case Screens.Dock
				CurrentScreen = New DockingScreen()
			Case Screens.Asteroid
				CurrentScreen = New MiningScreen()
			Case Screens.EditorItems
				CurrentScreen = New EditorItemsScreen()
			Case Screens.Death
				CurrentScreen = New DeathScreen()
			Case Screens.Communication
				CurrentScreen = New CommsScreen()
			Case Screens.Hyperspace
				CurrentScreen = New HyperSpaceScreen()
			Case Screens.ShipStatusMenu
				CurrentScreen = New ShipMenuScreen()
			Case Screens.ShipStatusDock
				CurrentScreen = New ShipDockedScreen()
			Case Screens.CrewStatusDock
				CurrentScreen = New CrewDockedScreen()
			Case Screens.ShipStore
				CurrentScreen = New ShipStoreScreen()
			Case Screens.HireCrew
				CurrentScreen = New HireCrewScreen()
			Case Screens.GalaxyMap
				CurrentScreen = New GalaxyMapScreen()
			Case Screens.WeaponStore
				CurrentScreen = New WeaponStoreScreen()
			Case Screens.JobBoard
				CurrentScreen = New JobBoardScreen()
				
			Default
				Error("Screen '" + ScreenNumber + "' not found")
		End Select
		
		CurrentScreenNumber = ScreenNumber
		
		Return CurrentScreenNumber
	End Function
		
	Function Stop:Void()  'Exit App
		Error("")
	End Function 	
End Class

Class Screens 'Enum
	Const LastScreen:Int = -3
	Const LastRoot:Int = -2
	Const Root:Int = -1
	Const Splash:Int = 0
	Const TestGame:Int = 1
	Const Editor:Int = 2
	Const Crate:Int = 3
	Const ShipStatus:Int = 4
	Const Death:Int = 5
	Const SystemMap:Int = 6
	Const Start:Int = 7
	Const CrewStatusMenu:Int = 8
	Const Equipment:Int = 9
	Const NewGame:Int = 10
	Const DebugItems:Int = 11
	Const EndCombat:Int = 12
	Const LoadGame:Int = 13
	Const GoodsStore:Int = 14
	Const Dock:Int = 15
	Const Asteroid:Int = 16
	Const EditorItems:Int = 17
	Const Communication:Int = 18
	Const Hyperspace:Int = 19
	Const ShipStatusMenu:Int = 20
	Const PartsStore:Int = 21
	Const ModStore:Int = 22
	Const ShipStatusDock:Int = 23
	Const CrewStatusDock:Int = 24
	Const ShipStore:Int = 25
	Const HireCrew:Int = 26
	Const GalaxyMap:Int = 27
	Const WeaponStore:Int = 28
	Const JobBoard:Int = 29
End Class



	
'Transition
Class ScaledFadeTransition Extends Transition
	Field r:Int, g:Int, b:Int  'Fade color
	Field fadeIn:Bool 'Am I fading in?

	Method New(r:Int, g:Int, b:Int, FadeIn:Bool = False)
		Self.r = r
		Self.g = g
		Self.b = b
		fadeIn = FadeIn
	End Method

	Method Render:Void()
		Super.Render()  'Required
		If Started = False And Finished = False Then Return
	
		SetColor(r, g, b)
		If fadeIn Then SetAlpha(1 - Percent) Else SetAlpha(Percent)
		DrawRect(DeviceX(0) - 1, DeviceY(0) - 1, 2 + DeviceX(1) - DeviceX(0), 2 + DeviceY(1) - DeviceY(0))
		SetColor(255, 255, 255); SetAlpha(1)
			
		'If Finished and fadeIn = False Then DrawText("Loading...", DeviceX(0) + 8, DeviceY(0) + 8)
	End Method
	
	Method RenderFullFrame:Void()
		SetColor(r, g, b)
		DrawRect(DeviceX(0) - 1, DeviceY(0) - 1, 2 + DeviceX(1) - DeviceX(0), 2 + DeviceY(1) - DeviceY(0))
		SetColor(255, 255, 255)
		'If fadeIn = False Then DrawText("Loading...", DeviceX(0) + 8, DeviceY(0) + 8)
	End Method
	
End Class

	
		
Function Stop:Void()  'Exit App
	Error("")
End Function

'Summary:  Sets the randomizer's seed to a value based on the current time.
Function RandomizeSeed:Void()
	Local time:= GetDate()
	''Ints are signed in monkey. Let's take half the MinValue of an int, then add the
	'current time and day of the month in milliseconds.
	Seed = -1073741824 + time[6] + (time[5] * 1000) + (time[4] * 60000) + (time[3] * 3600000) +
			(time[2] * 86400000) + (time[1] * $3DCC5000) + (time[0])
	#If CONFIG="debug"		
	Print "Seed: " + Seed
	#End
	
End Function

Function ResetColorAlpha:Void()
	SetColor(255, 255, 255)
	SetAlpha(1.0)
End

Class SwapSpace
	Field boltStack:IntStack
	Field mineStack:IntStack
	Method New()
		boltStack = New IntStack()
	End
End

Class DeltaTimer
	Field targetfps:Float = 60
	Field currentticks:Float
	Field lastticks:Float
	Field frametime:Float
	Field delta:Float
	
	Method New (fps:Float)
		targetfps = fps
		lastticks = Millisecs()
	End
	
	Method UpdateDelta:Void()
		currentticks = Millisecs()
		frametime = currentticks - lastticks
		delta = frametime / (1000.0 / targetfps)
		lastticks = currentticks
	End
End

Class SoundEffect
	Private
	Global laser:Cue
	Global missile:Cue
	Global beam:Cue
	Global enemy_impact:Stack<Cue>
	Global spark:Stack<Cue>
	Global weld:Stack<Cue>
	Global shield_impact:Cue
	Global shield_on:Cue
	Global shield_off:Cue
	Global radar:Cue
	Global alarm:Cue
	Global alert:Cue
	Global cannon:Cue
	Global thermal_release:Cue
	Global shield_booster:Cue
	Global afterburner:Cue
	Global stealth:Cue
	
	Global chan:ChannelRange
  

	Public
	Const GAME_SOUND_LOCATION:String = "sounds/"
	Const NONE:Int = 0
	Const LASER:Int = 1
	Const ENEMY_IMPACT:Int = 2
	Const SHIELD_IMPACT:Int = 3
	Const SHIELD_ON:Int = 4
	Const SHIELD_OFF:Int = 5
	Const ALARM:Int = 6
	Const RADAR:Int = 7
	Const ALERT:Int = 8
	Const MISSILE:Int = 9
	Const BEAM:Int = 10
	Const CANNON:Int = 11
	Const SPARK:Int = 12
	Const THERMAL_RELEASE:Int = 13
	Const SHIELD_BOOSTER:Int = 14
	Const AFTERBURNER:Int = 15
	Const STEALTH:Int = 16
	Const WELD:Int = 17

		 	
	Function Init:Void()
		chan = New ChannelRange(4, 21)
		laser = Load("laser",, 0.8, 0.1)
		enemy_impact = New Stack<Cue>()
		enemy_impact.Push(Load("ex1",, 0.8, 0.1))
		enemy_impact.Push(Load("ex2",, 0.8, 0.1))
		enemy_impact.Push(Load("ex3",, 0.8, 0.1))
		enemy_impact.Push(Load("ex4",, 0.8, 0.1))
		enemy_impact.Push(Load("ex5",, 0.8, 0.1))
		spark = New Stack<Cue>()
		spark.Push(Load("spark1",, 0.8, 0.1))
		spark.Push(Load("spark2",, 0.8, 0.1))
		spark.Push(Load("spark3",, 0.8, 0.1))
		spark.Push(Load("spark4",, 0.8, 0.1))
		spark.Push(Load("spark5",, 0.8, 0.1))
		spark.Push(Load("spark6",, 0.8, 0.1))
		spark.Push(Load("spark7",, 0.8, 0.1))
		spark.Push(Load("spark8",, 0.8, 0.1))
		spark.Push(Load("spark9",, 0.8, 0.1))
		weld = New Stack<Cue>()
		weld.Push(Load("weld",, 0.8, 0.1))
		shield_impact = Load("shield_hit",, 0.8, 0.1)
		shield_on = Load("shield_on",, 0.8, 0.1)
		shield_off = Load("shield_off",, 0.8, 0.1)
		radar = Load("radar",, 0.8, 0.1)
		alarm = Load("alarm",, 0.8, 0.1)
		alert = Load("alert",, 0.8, 0.1)
		missile = Load("missile",, 0.8, 0.1)
		beam = Load("beam",, 0.8, 0.1)
		cannon = Load("cannon",, 0.8, 0.1)
		thermal_release = Load("thermal_release",, 0.8, 0.1)
		shield_booster = Load("shield_booster",, 0.8, 0.1)
		afterburner = Load("afterburner",, 0.8, 0.1)
		stealth = Load("stealth",, 0.8, 0.1)

	End Function
	
	Function Load:Cue(cueName:String, loops:Bool = False, basevolume:Float = 1, volumeVariation:Float = 0, basepanning:Float = 0)
		Return New Cue(LoadSound(GAME_SOUND_LOCATION + cueName + AudioSystem.SoundFormat), loops, basevolume, volumeVariation, basepanning)
	End Function

	  
	Function Play:Void(type:Int)
		Select type
			Case LASER
				laser.Play(chan.GetNext())
			Case ENEMY_IMPACT
				enemy_impact.Get(Rnd(enemy_impact.Length())).Play(chan.GetNext())
			Case SPARK
				spark.Get(Rnd(spark.Length())).Play(chan.GetNext())
			Case SHIELD_IMPACT
				shield_impact.Play(chan.GetNext())
			Case SHIELD_OFF
				shield_off.Play(chan.GetNext())
			Case SHIELD_ON
				shield_on.Play(chan.GetNext())
			Case ALARM
				alarm.Play(chan.GetNext())
			Case ALERT
				alert.Play(chan.GetNext())
			Case RADAR
				radar.Play(chan.GetNext())
			Case MISSILE
				missile.Play(chan.GetNext())
			Case BEAM
				beam.Play(chan.GetNext())
			Case CANNON
				cannon.Play(chan.GetNext())
			Case SHIELD_BOOSTER
				shield_booster.Play(chan.GetNext())
			Case THERMAL_RELEASE
				thermal_release.Play(chan.GetNext())
			Case AFTERBURNER
				afterburner.Play(chan.GetNext())
			Case WELD
				weld.Get(Rnd(weld.Length())).Play(chan.GetNext())
			Default
				Print "Can't play sound " + type
		End Select
		
	End Function
	
End Class

Function GetPrettyTime:String(seconds:Int)
	Local min:Int = seconds / 60
	seconds -= (min * 60)
	Return PadNumber(min, 2) + ":" + PadNumber(seconds, 2)
End

Function PrettyStats:Void(x:Int, y:Int, offset:Int, stra:String, strb:String)
	Game.white_font.Draw(stra, x, y)
	Game.white_font.Draw(strb, x + offset, y)
End

Function ThrowErrorMessage:Void(type:String, msg:String)
	Local message:Stack<String>
	message = New Stack<String>
	message.Push(type)
	message.Push(msg)
	Game.popup = New Popup("Error", message,,,,, "Close", Popup.OK)
End

Function ThrowErrorMessage:Void(message:Stack<String>)
	Game.popup = New Popup("Error", message,,,,, "Close", Popup.OK)
End

Function DrawFPS:Void()
	If Game.lastFPS >= 59
		If Game.showPC
			Game.white_font.Draw("60   " + (Game.OverParticle.GetParticleCount() +Game.UnderParticle.GetParticleCount()), 0, 70)
		Else
			Game.white_font.Draw("60", 0, 120, 0)
		End
	Else
		If Game.showPC
			Game.white_font.Draw(Game.lastFPS + "   " + (Game.OverParticle.GetParticleCount() +Game.UnderParticle.GetParticleCount()), 0, 70)
		Else
			Game.white_font.Draw(Game.lastFPS, 0, 120, 0)
		End
	End
End

#rem
Class SoundEffect
	Private
	Global drop:SoundArray
	Global rotate:SoundArray
	Global slide:SoundArray
	Global newElement:Cue
	Global boom:Cue        'Transmute boom
	Global boom2:Cue       'Mimic boom
	Global coinHit:Cue     'Impact sound for punching the prize box, equip sound
	Global poofSparkle:Cue 'Belltree.  Used for grid commands
	Global tick:Cue        'Counter tick
	Global clock:Cue       'Tick tock
	Global create:Cue[7]   'Transmute sounds
	Global harp:Cue[8]     'Button hover sounds
	Global fire:Cue        'Pick/Lance boom
	Global puff:Cue        'tmOverMax puff
	Global wood:Cue        'Woodblock (inquiry and notification sounds)
	Global alert:Cue       'Generic alert sound
	Global info:Cue        'Generic alert sound
	Global unlock:Cue      'Generic alert sound
	Global boing:Cue       'Endlevel failure
	Global success:Cue     'Endlevel success
	
		
	Global chan:ChannelRange
  
	'Pet Battle Sounds
	Global punch_1:Cue

	Public
	Const GAME_SOUND_LOCATION:String = "sounds/"
	Const COMBAT_SOUND_LOCATION:String = "battle_sounds/"
	Const NONE:Int = 0
	Const BOOM:Int = 1
	Const BOOM2:Int = 100  'Evil boom
	Const CREATE:Int = 2
	Const DROP:Int = 3
	Const SLIDE:Int = 4
	Const NEW_ELEMENT:Int = 5
	Const ROTATE:Int = 6
	Const CLOCK:Int = 7
	Const HARP:Int = 8
	Const TICK:Int = 9
	Const POOFSPARKLE:Int = 10
	Const COINHIT:Int = 11
	Const WOOD:Int = 12
	Const FIRE:Int = 13   'Boom sound for pick/lance
	Const PUFF:Int = 14   'Puff sound for over max tm level
	Const ALERT = 15        'Ding sound for alerts
	Const INFO = 16           'Ding sound for info
	Const UNLOCK = 17           'Ding sound for unlock
	Const BOING = 18      'Alert sound for endlevel failure
	Const SUCCESS = 19    'Alert sound for endlevel success
	
	'Pet Battle Sounds
	Const PUNCH_1:Int = 50
		 	
	Function Init()
		chan = New ChannelRange(4, 21)
		drop = New SoundArray(chan)
		rotate = New SoundArray(chan)
		slide = New SoundArray(New ChannelRange(0, 1))

		newElement = Load("NewElement",, 1, 0.1)
		boom = Load("Boom",, 0.8, 0.1)
		boom2 = Load("BoomEvil",, 0.7, 0.1)
		fire = Load("FireSweep",, 1, 0.1)
		puff = Load("Puff1",, 1, 0.1)

		coinHit = Load("CoinHit",, 0.7, 0.3)
		poofSparkle = Load("PoofSparkle",,, 0.1)

		tick = Load("Tick",, 1, 0.2)
		clock = Load("Clock", True, 0.9, 0.1)

		wood = Load("Inquiry",, 0.8, 0.1)
		alert = Load("Alert",, 0.4, 0.1)
		info = Load("Info",, 0.1)
		boing = Load("Boing",, 0.4)
		success = Load("LevelComplete",, 0.3)
		
		unlock = Load("unlock_full_version",, 0.3)
		
		For Local i:Int = 0 To 6
			create[i] = Load("Create_" + i,, 1, 0.1)
		Next

		For Local x:Int = 1 To 4
			drop.sounds.Push(Load("Drop" + x,, 1, 0.1))
			slide.sounds.Push(Load("Slide" + x,, 1, 0.1))
			rotate.sounds.Push(Load("Rotate" + x,, 1, 0.1))
		Next
		
		For Local i:Int = 0 To 7
			harp[i] = Load("/harp/" + (i + 1),, 1, 0.4)
		Next
		
		'Pet Battle Sounds
		punch_1 = LoadBattle("punch_01",, 1, 0.1)
	End Function
	
	Function Load:Cue(cueName:String, loops:Bool = False, basevolume:Float = 1, volumeVariation:Float = 0, basepanning:Float = 0)
		Return New Cue(LoadSound(GAME_SOUND_LOCATION + cueName + AudioSystem.SoundFormat), loops, basevolume, volumeVariation, basepanning)
	End Function

	Function LoadBattle:Cue(cueName:String, loops:Bool = False, basevolume:Float = 1, volumeVariation:Float = 0, basepanning:Float = 0)
		Return New Cue(LoadSound(COMBAT_SOUND_LOCATION + cueName + AudioSystem.SoundFormat), loops, basevolume, volumeVariation, basepanning)
	End Function
	  
	Function Play:Void(type:Int)
		Select type
			Case BOOM
				boom.Play(2)
			Case BOOM2
				boom2.Play(2)
			Case FIRE
				fire.Play(3)
			Case PUFF
				puff.Play(3)
			Case NEW_ELEMENT
				newElement.Play(3)
			Case CREATE
				If boardCombo >= 0 And boardCombo < 7 'put current combo variable here
					create[boardCombo].Play(chan.GetNext())
				Else
					create[6].Play(chan.GetNext())
				End If
			Case DROP
				drop.Play()
			Case SLIDE
				'Print "slide"
				slide.Play()
			Case ROTATE
				'Print "current: " + chan.current
				rotate.Play()
				'Print "now: " + chan.current
      
			Case CLOCK   'WARNING:  LOOPING SOUND
				clock.Play(3)
			Case TICK
				tick.Play(2)

			Case HARP
				harp[Rnd(harp.Length)].Play(chan.GetNext())
			Case COINHIT
				coinHit.Play(chan.GetNext())
			Case POOFSPARKLE
				poofSparkle.Play(chan.GetNext())
			Case WOOD
				wood.Play(chan.GetNext())
			Case ALERT
				alert.Play(chan.GetNext())
			Case INFO
				info.Play(chan.GetNext())
			Case UNLOCK
				unlock.Play(chan.GetNext())
			Case BOING
				boing.Play(2)
			Case SUCCESS
				success.Play(2)
			
							
				'Pet Battle Sounds
			Case PUNCH_1
				punch_1.Play()
		                
			Default
				Print "Can't play sound " + type
				Game.Console.PushMessage("SoundEffect:  Can't play sound " + type)
		End Select
		
	End Function
	
	Function Stop:Void(type:Int)
		Select type
			Case CLOCK
				clock.Stop(3)
		End Select
	End Function
End Class

#end