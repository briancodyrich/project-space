Strict

Import space_game

Class Task
	Const MINOR_REPAIR:Int = 50
	Const MAJOR_REPAIR:Int = 51
	Const SYSTEM_REPAIR:Int = 52
	Const PILOT_SHIP:Int = 53
	Const FIRE:Int = 54
	Const HEAL_SELF:Int = 60
	Const USE_COMPUTER:Int = 61
	Const PATCH_HULL:Int = 62
	Const SLEEP:Int = 63
	Const USE_GUNS:Int = 64
	Field x:Int
	Field y:Int
	Field type:Int
	Field ignore_overlap:bool
	
	Method New(x:Int, y:Int, type:int)
		Self.x = x
		Self.y = y
		Self.type = type
	End
	
	Method isMatch:Bool(x:Int, y:Int, type:int)
		If Self.x <> x Then Return False
		If Self.y <> y Then Return False
		If Self.type <> type Then Return False
		Return True
	End
	
	Method isMatch:Bool(x:Int, y:Int)
		If Self.x <> x Then Return False
		If Self.y <> y Then Return False
		Return True
	End
	
End