Import space_game

Class TaskManager
	Field taskList:Stack<Task>
	Field permList:Stack<Task>
	Field hull_is_damaged:bool
'	Field comptaskList:Stack<Task>
	
	Method New()
		taskList = New Stack<Task>
		permList = New Stack<Task>
	'	comptaskList = New Stack<Task>
	End
	
	Method Reset:Void()
	
	End
	
	Method AddPerm:Void(x:Int, y:Int, type:Int)
		If isDupePerm(x, y) Then Return
		permList.Push(New Task(x, y, type))
	End
	
	Method RemovePerm:Void(x:Int, y:Int)
		For Local temp:Int = 0 Until permList.Length()
			If permList.Get(temp).isMatch(x, y)
				permList.Remove(temp)
				Return
			End
		Next
	End
	
	Method RemoveAll:Void(type:Int)
		For Local x:Int = 0 Until taskList.Length()
			If taskList.Get(x).type = type
				taskList.Remove(x)
			End
		Next
	End
	
	
	Method Add:Void(x:Int, y:Int, type:Int)
		If isDupeTask(x, y, type) Then Return
		taskList.Push(New Task(x, y, type))
	End
	
	Method Remove:Void(x:Int, y:Int, type:Int)
		For Local temp:Int = 0 Until taskList.Length()
			If taskList.Get(temp).isMatch(x, y, type)
				taskList.Remove(temp)
				Return
			End
		Next
		Print "tried to remove task that does not exist"
	End
	
	Method TaskAtFeet:Int(x:int, y:Int, aiCode:Int)
		Select aiCode
			Case AICode.AI_MECHANIC
			For Local temp:Int = 0 Until taskList.Length()
				If taskList.Get(temp).isMatch(x, y, Task.SYSTEM_REPAIR) Then Return Task.SYSTEM_REPAIR
				If taskList.Get(temp).isMatch(x, y, Task.MAJOR_REPAIR) Then Return Task.MAJOR_REPAIR
				If taskList.Get(temp).isMatch(x, y, Task.MINOR_REPAIR) Then Return Task.MINOR_REPAIR
			Next
			Case AICode.AI_HEAL_SELF_HIGH
				For Local temp:Int = 0 Until permList.Length()
					If permList.Get(temp).isMatch(x, y, Task.HEAL_SELF) Then Return Task.HEAL_SELF
				Next
			Case AICode.AI_HEAL_SELF_LOW
				For Local temp:Int = 0 Until permList.Length()
					If permList.Get(temp).isMatch(x, y, Task.HEAL_SELF) Then Return Task.HEAL_SELF
				Next
			Case AICode.AI_MEDIC
				For Local temp:Int = 0 Until permList.Length()
					If permList.Get(temp).isMatch(x, y, Task.HEAL_SELF) Then Return Task.HEAL_SELF
				Next
			Case AICode.AI_FIREFIGHTER
				For Local temp:Int = 0 Until taskList.Length()
					If taskList.Get(temp).isMatch(x, y, Task.FIRE) Then Return Task.FIRE
				Next
			
			Case AICode.AI_PILOT
				For Local temp:Int = 0 Until permList.Length()
					If permList.Get(temp).isMatch(x, y, Task.USE_COMPUTER) Then Return Task.PILOT_SHIP
				Next
			Case AICode.AI_GUNNER
				For Local temp:Int = 0 Until permList.Length()
					If permList.Get(temp).isMatch(x, y, Task.USE_GUNS) Then Return Task.USE_GUNS
				Next
			Case AICode.AI_RESTING
				For Local temp:Int = 0 Until permList.Length()
					If permList.Get(temp).isMatch(x, y, Task.SLEEP) Then Return Task.SLEEP
				Next
		End
		Return -1
	End
	Method PermAtFeet:bool(x:int, y:Int, type:Int)
		For Local temp:Int = 0 Until permList.Length()
			If permList.Get(temp).isMatch(x, y, type) Then Return True
		Next
		Return False
	End
	
	Method isBlockingJob:Bool(x:Int, y:Int)
		For Local temp:Int = 0 Until permList.Length()
			If permList.Get(temp).isMatch(x, y) Then Return True
		Next
		For Local temp:Int = 0 Until taskList.Length()
			If taskList.Get(temp).isMatch(x, y) Then Return True
		Next
		Return False
	End
	
	
	Method isDupeTask:Bool(x:Int, y:Int, type:Int)
		For Local temp:Int = 0 Until taskList.Length()
			If taskList.Get(temp).isMatch(x, y, type) Then Return True
		Next
		Return False
	End
	Method isDupePerm:Bool(x:Int, y:Int)
		For Local temp:Int = 0 Until permList.Length()
			If permList.Get(temp).isMatch(x, y) Then Return True
		Next
		Return False
	End
	
	Method GetJobCount:Int()
		Return taskList.Length()
	End
	Method GetPermCount:Int()
		Return permList.Length()
	End
	
	Method PrintAllTasks:Void()
		Print "start"
		For Local temp:Int = 0 Until taskList.Length()
			Print taskList.Get(temp).x + "/" + taskList.Get(temp).y
			Select taskList.Get(temp).type
				Case Task.MINOR_REPAIR
					Print "minor repair"
				Case Task.MAJOR_REPAIR
					Print "major repair"
				Case Task.SYSTEM_REPAIR
					Print "system repair"
				Case Task.PILOT_SHIP
					Print "pilot ship"
				Case Task.FIRE
					Print "put out fire"
				Default
					Print "unknown task"
			End
		Next
		Print "end"
	End
	
End

