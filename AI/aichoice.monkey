Strict

Import space_game

Class AIPicker
	Const SURRENDER_SHIELD_CUTOFF:Int = 20
	Const SURRENDER_CRITICAL_IGNORE:Int = 15
	Const SURRENDER_COOLDOWN:Int = 40
	Const MIN_VALUE_CHANGE:Float = 0.05
	Const MAX_CHOICES:Int = 27
	
	Const RUN_SHIELD_PERCENT:Int = 20
	
	
	Field choices:AIChoice[MAX_CHOICES]
	Field ep:Int
	Field rp:Int
	Field sp:int
	Field last:float
	Method New()
		For Local temp:Int = 0 Until MAX_CHOICES
			choices[temp] = New AIChoice()
		Next
	End
	
	Method ProcessEngine:Void(obj:Engine)
		AIChoice.damage_value_base += 0.8
		If obj.isActive() And not obj.overheated
			For Local temp:Int = 0 Until MAX_CHOICES
				Local ch:AIChoice = choices[temp]
				ch.power += (obj.energyProduction * obj.GetEfficiency(True,, False)) * GetMult(ch.ep, 2.0)
				ch.heat -= (obj.heatProduction * obj.GetEfficiency(True,, False)) * GetMult(ch.ep, 2.0)
			Next
		Else
			AIChoice.system_damage_value += 1.0
		End
	End
	Method ProcessReactor:Void(obj:Reactor)
		AIChoice.damage_value_base += 0.8
		If obj.isActive()
			For Local temp:Int = 0 Until MAX_CHOICES
				Local ch:AIChoice = choices[temp]
				ch.power += (obj.energyProduction * obj.GetEfficiency(True,, False)) * GetMult(ch.rp, 1.5)
				ch.heat -= (obj.heatProduction * obj.GetEfficiency(True,, False)) * GetMult(ch.rp, 2.0)
			Next
		Else
			AIChoice.system_damage_value += 1.2
		End
	End
	Method ProcessShield:Void(obj:Shield)
		AIChoice.damage_value_base += 1.0
		If obj.isActive()
			Local ch:AIChoice
			For Local temp:Int = 0 Until MAX_CHOICES
				ch = choices[temp]
				'obj.power = int(obj.capacity * obj.GetEfficiency()) / 2.0
				If obj.power < int(obj.capacity * obj.GetEfficiency())
					ch.power += obj.energyProduction * obj.GetEfficiency(True,, False) * Pow(2, ch.sp - 1)
				End
				ch.shield += obj.power
				ch.shield_size += obj.capacity * obj.GetEfficiency()
			Next
				AIChoice.shieldPOW += ch.shield
				AIChoice.shieldMAX += ch.shield_size
		Else
			Local ch:AIChoice
			For Local temp:Int = 0 Until MAX_CHOICES
				ch = choices[temp]
				ch.shield += obj.power
				ch.shield_size += obj.capacity * obj.GetEfficiency()
			Next
			AIChoice.system_damage_value += 1
			AIChoice.shieldPOW += ch.shield
			AIChoice.shieldMAX += ch.shield_size
		End
	End
	Method ProcessHealth:Void(obj:HealingTube)
		If obj.isActive()
			For Local temp:Int = 0 Until MAX_CHOICES
				Local ch:AIChoice = choices[temp]
				ch.power += obj.energyProduction
			Next
		End
	End
	
	Method ProcessHardPoint:Void(obj:Hardpoint)
		AIChoice.damage_value_base += 1.0
		If obj.isActive()
			If obj.gun <> Null
				For Local temp:Int = 0 Until MAX_CHOICES
					Local ch:AIChoice = choices[temp]
					ch.power -= obj.gun.power_needed
				Next
			End
		Else
			AIChoice.system_damage_value += 1.8
		End
	End
	
	Method ProcessBattery:Void(obj:Battery)
		AIChoice.damage_value_base += 0.5
		If obj.isActive()
			For Local temp:Int = 0 Until MAX_CHOICES
				Local ch:AIChoice = choices[temp]
				ch.batt_rate += Min(Int(obj.dischargeRate * obj.GetEfficiency()), obj.power)
			'	obj.power = (obj.capacity * obj.GetEfficiency()) / 4.0
				ch.batt_power += obj.power
				ch.batt_size += obj.capacity * obj.GetEfficiency()
			Next
		Else
			AIChoice.system_damage_value += 0.5
		End
	End
	Method ProcessCoolant:Void(obj:CoolantTank)
		AIChoice.damage_value_base += 1.0
		If obj.isActive()
			For Local temp:Int = 0 Until MAX_CHOICES
				Local ch:AIChoice = choices[temp]
				ch.coolant += obj.heatPacket
				ch.coolant_size += obj.capacity * obj.GetEfficiency()
			Next
		Else
			AIChoice.system_damage_value += 1.2
		End
	End
	
	Method Reset:Void(heat:int)
		For Local ep:Int = 0 Until 3
			For Local rp:Int = 0 Until 3
				For Local sp:Int = 0 Until 3
					choices[ (ep * 9) + (rp * 3) + sp].Reset(ep, rp, sp, heat)
				Next
			Next
		Next
	End
	
	Method PrintAll:Void()
		For Local temp:Int = 0 Until MAX_CHOICES
			Local c:AIChoice = choices[temp]
			Print(c.ep + "-" + c.rp + "-" + c.sp + "   pow: " + c.power + " heat: " + c.heat + " shield: " + c.shield + "/" + c.shield_size + " batt: " + c.batt_power + "/" + c.batt_size + "  cool: " + c.coolant + "/" + c.coolant_size)
			Print("#EV#" + c.ev + "#CV#" + c.cv + "#RV#" + c.rv + "#SV#" + c.sv + "###" + c.val)
		Next

	End
	
	Method ProcessValue:Void(print_choice:Bool)
		For Local temp:Int = 0 Until MAX_CHOICES
			choices[temp].GetRange()
		Next
		AIChoice.GetScale()
		Local max:Float = -3
		Local tv:Float
		Local selected:Int = 0
		
		AIChoice.damage_value = (AIChoice.system_damage_value / AIChoice.damage_value_base) * 100
		AIChoice.shield_value = (AIChoice.shieldPOW / float(AIChoice.shieldMAX)) * 100
		If AIChoice.damage_value >= RUN_SHIELD_PERCENT Then AIChoice.run_shields = True
		For Local temp:Int = 0 Until MAX_CHOICES
			tv = choices[temp].ProcessValue()
			If tv > max
				selected = temp
				max = tv
			End
		Next
		If Abs(max - last) > MIN_VALUE_CHANGE
			last = max
			ep = choices[selected].ep
			rp = choices[selected].rp
			sp = choices[selected].sp
			If print_choice
				Print(choices[selected].ep + "-" + choices[selected].rp + "-" + choices[selected].sp + " selected")
				Print("EV: " + choices[selected].ev)
				Print("CV: " + choices[selected].cv)
				Print("RV: " + choices[selected].rv)
				Print("SV: " + choices[selected].sv)
				Print("TV: " + choices[selected].val)
			End
			
		Else
		
		End
		
		
		
		'PrintAll()
	End
	
	Method ProcessSurrender:Int(val:Int)
		If AIChoice.shield_value > SURRENDER_SHIELD_CUTOFF
			Return (Max(0, val - SURRENDER_COOLDOWN))
		Else
			Return (Max(0, val + (AIChoice.damage_value - SURRENDER_CRITICAL_IGNORE)))
		End
	End
	
	Method GetReal:Int(x:Int)
		Return x + 1
	End
End

Class AIChoice
	Const ENGINE_VALUE:Float = 0.3
	Field ep:Int
	Field ev:Float
	Field rp:Int
	Field rv:float
	Field sp:Int
	Field sv:float
	Field cv:float
	Field batt_power:int
	Field batt_rate:int
	Field batt_size:Int
	Field power:Int
	Field heat:Int
	Field coolant:int
	Field coolant_size:Int
	Field shield:Int
	Field shield_size:int
	Field mass:int
	
	Field dead:bool
	Field val:float
	
	'should these go?
	
	Global damage_value_base:float
	Global system_damage_value:float
	Global damage_value:int
	
	Global shieldPOW:Int
	Global shieldMAX:int
	Global shield_value:int
	
	Global run_shields:Bool
	
	Global min_heat:float = 0
	Global max_heat:float = 0
	Global shift_heat:Float
	Global mid_heat:float
	Global min_power:float = 0
	Global max_power:float = 0
	Global shift_power:float
	Global mid_power:float
	Global min_thrust:Float = 0
	Global max_thrust:Float = 0
	Global shift_thrust:Float = 0
	Global mid_thrust:Float = 0
	
	Method GetRange:Void()
		If power + batt_rate < 0
			dead = True
		Else
			If power < min_power
				min_power = power
			End
			If power > max_power
				max_power = power
			End
		End
		If heat < min_heat
			min_heat = heat
		End
		If heat > max_heat
			max_heat = heat
		End
	End
	
	Function GetScale:Void()
		shift_heat = (-min_heat)
		shift_power = (-min_power)
		shift_thrust = (-min_thrust)
		min_heat += shift_heat
		max_heat += shift_heat
		min_power += shift_power
		max_power += shift_power
		mid_heat = (min_heat + max_heat) / 2.0
		mid_power = (min_power + max_power) / 2.0
	End
	
	
	
	
	Method ProcessValue:float()
		Local batteryPercent:Float
		Local coolantPercent:Float
		Local shieldPercent:float
		If dead
			val = -3
			Return -3
		End
		If batt_size = 0
			batteryPercent = 0.0
			run_shields = True
		Else
			batteryPercent = (batt_size - batt_power) / float(batt_size)
		End
		
		If coolant_size = 0
			coolantPercent = 0.0
			run_shields = True
		Else
			coolantPercent = (coolant_size - coolant) / float(coolant_size)
		End
		If shield_size = 0
			shieldPercent = 0.0
			run_shields = True
		Else
			shieldPercent = (shield_size - shield) / float(shield_size)
		End
		
		If shieldPercent < 0.1 Then run_shields = True
		
		#rem
		If run_shields
			thrust = (thrust / float(mass)) * Ship.SHIELD_SPEED_PENALTY
		Else
			thrust = (thrust / float(mass))
		End
		#end
		
		cv = ( (float(heat + shift_heat) / mid_heat) - 1) * (1.0 - coolantPercent)
		ev = ep * ENGINE_VALUE 
		rv = ( (float(power + shift_power) / mid_power) - 1) * (batteryPercent)
		sv = (sp - 2) * shieldPercent
		#rem
		Print "PS"
		Print batteryPercent
		Print coolantPercent
		Print shieldPercent
		Print ev
		Print rv
		Print sv
		val = ev + rv + sv
		Return val
		#end
		val = cv + ev + rv + sv
		Return val
	End
	
	Method Reset:Void(ep:Int, rp:Int, sp:int, heat:int)
		Self.ep = ep + 1
		Self.rp = rp + 1
		Self.sp = sp + 1
		batt_power = 0
		batt_rate = 0
		batt_size = 0
		power = 0
		dead = False
		Self.heat = heat
		Self.mass = mass
		coolant = 0
		coolant_size = 0
		shield = 0
		shield_size = 0
		damage_value = 0
		system_damage_value = 0
		damage_value_base = 0
		shieldPOW = 0
		shieldMAX = 0
		shield_value = 0
		run_shields = False
		
		
		min_heat = 10000
		max_heat = -10000
		min_power = 10000
		max_power = -10000
	End
End