Strict

Import space_game


Class TextEffect
	Const TYPE_DECODE_WIPE:Int = 1
	Const TYPE_RAND:Int = 2
	Const TYPE_TERMINAL:Int = 3
	Const TYPE_INSTANT:Int = 4
	
	Const WAIT:String = "WAIT="
	Const SCREEN_SWITCH:String = "SWITCH="
	Field x:Int
	Field y:Int
	Field type:int
	Field message:String
	Field decodeMsg:String
	Field decodeArr:Int[]
	Field printMsg:String
	Field counter:Int
	Field uc:String
	Field speed:Int
	Field done:bool
	Field started:bool
	Field blinkCounter:Int = 0
	Field blinkRate:Int = 10
	Field camera:TCam
	Field move:int
	Method New(x:Int, y:Int, type:int, message:String, camera:TCam = Null, move:int = 0)
		Self.x = x
		Self.y = y
		Self.type = type
		Self.message = message
		Self.camera = camera
		Self.move = move
		Select type
			Case TYPE_INSTANT
				printMsg = message
				If camera <> Null
					camera.Set(move)
				End
			Case TYPE_RAND
				speed = 3
				uc = ""
			Case TYPE_DECODE_WIPE
				decodeMsg = ""
				speed = 10
				For Local temp:Int = 0 Until message.Length()
					decodeMsg += String.FromChar(Rnd(32, 256))
				Next
				decodeArr = decodeMsg.ToChars()
			Case TYPE_TERMINAL
				speed = 2
				If camera <> Null
					camera.Move(move)
				End
				uc = ""
		End
	End
	
	Method Update:Void(isLast:bool = False)
		If done Then Return
		If printMsg = message
			done = True
			Return
		End
		If type = TYPE_RAND
			blinkCounter += 1
			If counter < (message.Length() * speed)
				printMsg = message[0 .. (counter / speed)] + String.FromChar(message[Min( (counter / speed) + 1, message.Length() -1)] + counter mod speed) + uc
				counter += 1
			Else
				printMsg = message
			End
		Else If type = TYPE_DECODE_WIPE
			Local endStr:Int[] = message.ToChars()
			For Local temp:Int = 0 Until decodeMsg.Length()
				If decodeArr[temp] <> endStr[temp]
					For Local s:Int = 0 Until speed
						If decodeArr[temp] = endStr[temp]
							printMsg = String.FromChars(decodeArr)
							
							Return
						Else If decodeArr[temp] < endStr[temp]
							decodeArr[temp] += 1
						Else
							decodeArr[temp] -= 1
						End
					Next
					printMsg = String.FromChars(decodeArr)
					Return
				End
			Next
		Else If type = TYPE_TERMINAL
			If counter < (message.Length() * speed)
				printMsg = message[0 .. (counter / speed)]
				counter += 1
			Else
				printMsg = message
			End
		End
	End
	
	Method Render:Void()
		If camera = Null
			Game.white_font.Draw(printMsg, x, y)
		Else
			Game.white_font.Draw(printMsg, x, y + camera.offset)
		End
	End
End

Class TCam
	Field offset:Int
	Field x:Int
	Field y:Int
	Field w:Int
	Field h:Int
	Field move:int
	Method New(x:Int, y:Int, w:Int, h:Int)
		Self.x = x
		Self.y = y
		Self.w = w
		Self.h = h
	End
	Method Update:Void()
		If move > 0
			offset -= 1
			move -= 1
		Else If move < 0
			offset += 1
			move += 1
		End
	End
	Method Move:Void(a:int)
		move += a
	End
	Method Set:Void(a:Int)
		offset -= a
	End
End