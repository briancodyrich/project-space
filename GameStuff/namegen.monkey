Strict

Import space_game


Class NameGenerator
	Const PREFIX_LOCATION:string = "Strings/Planets/prefix.json"
	Const SUFFIX_LOCATION:String = "Strings/Planets/suffix.json"
	Field prefix_list:IntMap<String>
	Field pre_max:int
	Field suffix_list:IntMap<String>
	Field suff_max:int
	Field lookup_table:Stack<Int>
	Method New(s:int)
		prefix_list = New IntMap<String>()
		suffix_list = New IntMap<String>()
		lookup_table = New Stack<Int>()
		Seed = s
		Local arr:JsonArray = JsonArray(New JsonParser(app.LoadString(PREFIX_LOCATION)).ParseValue())
		For Local temp:Int = 0 Until arr.Length()
			prefix_list.Add(temp, arr.GetString(temp))
		Next
		pre_max = prefix_list.Count()
		arr = JsonArray(New JsonParser(app.LoadString(SUFFIX_LOCATION)).ParseValue())
		For Local temp:Int = 0 Until arr.Length()
			suffix_list.Add(temp, arr.GetString(temp))
		Next
		suff_max = suffix_list.Count()
		Local nums:Stack<Int> = New Stack<Int>()
		For Local temp:Int = 0 Until pre_max * suff_max
			nums.Push(temp)
		Next
		While Not nums.IsEmpty()
			Local rnd:Int = Rnd(nums.Length())
			lookup_table.Push(nums.Get(rnd))
			nums.Remove(rnd)
		Wend
		Print "max system: " + (pre_max * suff_max * 10)
	End
	
	Method GetName:String(num:Int)
		Local roman:Int
		While num > lookup_table.Length()
			num -= lookup_table.Length()
			roman += 1
		Wend
		Local v:Int = lookup_table.Get(num)
		Local p:Int = v / suff_max
		Local s:Int = v - (p * suff_max)
		Return suffix_list.Get(s) + " " + prefix_list.Get(p) + GetRoman(roman, False)
		
	End
	Function GetRoman:String(val:int, include_dash:bool)
		Local s:String
		If include_dash
			s = "-"
		Else
			s = " "
		End
		Select val
			Case 1
				Return s + "I"
			Case 2
				Return s + "II"
			Case 3
				Return s + "III"
			Case 4
				Return s + "IV"
			Case 5
				Return s + "V"
			Case 6
				Return s + "VI"
			Case 7
				Return s + "VII"
			Case 8
				Return s + "VIII"
			Case 9
				Return s + "IV"
			Case 10
			Return s + "X"
		End
		Return ""
	End
End