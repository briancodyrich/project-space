Strict

Import brl.json
Import os

Interface Loadable
	Method FileName:String()
	Method OnSave:JsonObject()
	Method OnLoad:Void(data:JsonObject)
End

Class Loader
	Const MAX_NAME_CHARS:Int = 20
	Const APP_NAME:String = "Elder Stars"
	Private
	Global settings:Loadable
	Public
	
	Function Init:Void(data:Loadable)
		settings = data
		CreateDir(GetEnv("APPDATA") + "/" + APP_NAME)
		Local s:String = LoadString(GetEnv("APPDATA") + "/" + APP_NAME + "/settings.json")
		If s.Length() = 0 Then Return
		settings.OnLoad(New JsonObject(s))
	End
	
	Function DeleteSave:Int(playerName:String)
		return DeleteDir(GetEnv("APPDATA") + "/" + APP_NAME + "/" + playerName, True)
	End
	
	Function LoadGame:bool(playerName:String, data:Loadable)
		playerName.Trim()
		Local s:String = LoadString(GetEnv("APPDATA") + "/" + APP_NAME + "/" + playerName + "/" + data.FileName())
		If s.Length() = 0 Then Return False
		data.OnLoad(New JsonObject(s))
		Return True
	End
	
	Function FileExists:Bool(playerName:String, file:string)
		If FileSize(GetEnv("APPDATA") + "/" + APP_NAME + "/" + playerName + "/" + file) = -1
			Return False
		End
		Return True
	End
	
	Function ListSaves:String[] ()
		Local d:String[] = LoadDir(GetEnv("APPDATA") + "/" + APP_NAME)
		Local r:Stack<String> = New Stack<String>
		For Local temp:Int = 0 Until d.Length()
			If not d[temp].Contains(".")
				r.Push(d[temp])
			End
		Next
		Return r.ToArray()
	End
	
	Function SaveGame:Void(playerName:String, data:Loadable)
		SaveString(settings.OnSave().ToJson(), GetEnv("APPDATA") + "/" + APP_NAME + "/settings.json")
		playerName = playerName.Trim()
		If playerName.Length() = 0 Then Return
		CreateDir(GetEnv("APPDATA") + "/" + APP_NAME + "/" + playerName)
		SaveString(data.OnSave().ToJson(), GetEnv("APPDATA") + "/" + APP_NAME + "/" + playerName + "/" + data.FileName())
	End
	
	Function isValidName:Bool(playerName:String)
		playerName = playerName.Trim()
		Local saves:string[] = ListSaves()
		If playerName.Length() = 0 Or playerName.Length() > MAX_NAME_CHARS Then Return False
		Local char:Int[] = playerName.ToChars()
		For Local temp:Int = 0 Until char.Length()
			If Not ( (char[temp] >= 97 And char[temp] <= 122) Or (char[temp] >= 65 And char[temp] <= 90) or (char[temp] >= 48 And char[temp] <= 57) Or char[temp] = 32) Then Return False
		Next
		For Local temp:Int = 0 Until saves.Length()
			If saves[temp].ToLower().Compare(playerName.ToLower()) = 0 Then Return False
		Next
		Return True
	End
End