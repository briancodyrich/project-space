Strict

Import GameStuff.Particle.photonManager


Class Beam Abstract
	Private
'	Global fire:Image
	
	Public
	Function Init:Void(particleAtlas:TextureAtlas)

	End
	Function Init:Void()
		'fire = LoadImage("particles/pt_blue.png",, Image.MidHandle)
	End
	Function Flush:Void()

	End
	Function Create:Void(p:Photon, x:Int, y:Int, id:int)
		p.x0 = p.x
		p.y0 = p.y
		p.x2 = x + (Ship.GRID_SIZE / 2)
		p.y2 = y + (Ship.GRID_SIZE / 2)
		p.x1 = (x - p.x)
		p.y1 = (y - p.y)
		p.theta = id
		p.effectTimer = 0
		p.lifeTimer = 120
	End
	Function Render:Void(p:Photon)
	'	If p.img = Null Then Return
		SetColor(255, 0, 0)
		If PhotonManager.useZone
			SetScaledScissor(0, 0, PhotonManager.zoneStart, ScreenHeight)
			If p.theta <> 0
				DrawLine(p.x0, p.y0, p.x2, p.y2)
			Else
				DrawLine(p.x0, p.y0, p.x2 + p.x1, p.y2 + p.y1)
			End
			SetScaledScissor(PhotonManager.zoneEnd, 0, ScreenWidth - PhotonManager.zoneEnd, ScreenHeight)
			If p.theta <> 0
				DrawLine(p.x0, p.y0, p.x2, p.y2)
			Else
				DrawLine(p.x0, p.y0, p.x2 + p.x1, p.y2 + p.y1)
			End
			SetScaledScissor(0, 0, ScreenWidth, ScreenHeight)
		Else
			If p.theta <> 0
				DrawLine(p.x0, p.y0, p.x2, p.y2)
			Else
				DrawLine(p.x0, p.y0, p.x2 + p.x1, p.y2 + p.y1)
			End
		End
		ResetColorAlpha()
	End
	Function Update:Void(p:Photon)
		p.lifeTimer -= 1
		If p.lifeTimer <= 0
			If p.theta <> 0
				Game.Swap.boltStack.Push(p.theta)
			End
			p.alive = False
		End
	End
End