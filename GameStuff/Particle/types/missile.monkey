Strict

Import GameStuff.Particle.photonManager


Class Missile Abstract
	Private
	Global fire:Image
	
	Public
	Function Init:Void(particleAtlas:TextureAtlas)

	End
	Function Init:Void()
		fire = LoadImage("particles/missile.png",, Image.MidHandle)
	End
	Function Flush:Void()

	End
	Function Create:Void(p:Photon, x:Int, y:Int, id:int)
		p.x0 = p.x
		p.y0 = p.y
		p.x2 = x
		p.y2 = y
		p.x1 = (p.x + x) / 2.0
		p.y1 = (p.y + y) / 2.0
		p.id = id
		p.theta = ATan2(x - p.x, y - p.y) + 270
		p.effectTimer = 0
		p.lifeTimer = 0
		p.alpha = 1.0
		p.img = fire
	End
	Function Render:Void(p:Photon)
		If p.img = Null Then Return
		If PhotonManager.useZone
			SetScaledScissor(0, 0, PhotonManager.zoneStart, ScreenHeight)
			DrawImage(p.img, p.x, p.y, p.theta, 1.0, 1.0)
			SetScaledScissor(PhotonManager.zoneEnd, 0, ScreenWidth - PhotonManager.zoneEnd, ScreenHeight)
			DrawImage(p.img, p.x, p.y, p.theta, 1.0, 1.0)
			SetScaledScissor(0, 0, ScreenWidth, ScreenHeight)
		Else
			DrawImage(p.img, p.x, p.y, p.theta, 1.0, 1.0)
		End
	End
	Function Update:Void(p:Photon)
		p.effectTimer += 0.01
		If p.effectTimer < 1
			p.x = Lerp(p.x0, p.x1, p.effectTimer)
			p.y = Lerp(p.y0, p.y1, p.effectTimer)
		End
		If p.effectTimer > 3
			p.lifeTimer += 0.01
			p.x = Lerp(p.x0, p.x1, 1 + p.lifeTimer)
			p.y = Lerp(p.y0, p.y1, 1 + p.lifeTimer)
		End
		If p.lifeTimer > 0.9
			If p.id <> 0
				Game.Swap.boltStack.Push(p.id)
			End
		End
		
		If p.lifeTimer > 1 And p.id <> 0
			p.alive = False
		End
		
		If p.x < 0 or p.x > ScreenWidth Then p.alive = False
		If p.y < 0 or p.y > ScreenHeight Then p.alive = False
		If p.lifeTimer < 0 Then p.alive = False
	End
End