Strict

Import GameStuff.Particle.photonManager

Class Spark Abstract
	Private
	Global fire:Image
	Public
	Function Init:Void(particleAtlas:TextureAtlas)
		
	End
	Function Init:Void()
		fire = LoadImage("particles/pt_aura_3.png",, Image.MidHandle)
	End
	Function Create:Void(p:Photon)
		'		p.lifeTimer
		p.alpha = 1.0
		p.x0 = p.x
		p.y0 = p.y
		p.dy = Rnd(-0.5, 0.5)
		p.dx = Rnd(-0.5, 0.5)
		p.img = fire
	'	If fromRight
			p.theta = Rnd(360)
		'		p.alpha = 0.5
		'		p.dx = Rnd(5, 15)
		'		If Not fromRight Then p.dx = - (p.dx)
		'		p.dy = -Rnd(10, 20)
		'		p.lifeTimer = Rnd(30, 70)
		'		p.gravity = 0
		'		p.img = fire
	End
	Function Render:Void(p:Photon)
		If p.img = Null Then Return
		SetAlpha(p.alpha)
		DrawImage(p.img, p.x, p.y)
		SetAlpha(1.0)
	End
	Function Update:Void(p:Photon)
	'	p.x += (Cos(p.theta) * p.dx) * dt.delta + Rnd(-2, 2)
		p.alpha -= 0.02
	'	p.y += (-Sin(p.theta) * p.dy) * dt.delta + Rnd(-2, 2)
		p.x += Rnd(-1, 1)
		p.y += Rnd(-1, 1)
		'	If p.dy >= 80 Then p.dy = 80
		If p.alpha <= 0.0 Then p.alive = False
		'	If p.alpha <= 0.05 Then p.alive = False
	End
End

#rem
Class Spark Abstract
Private
Global fire:Image
Const GRAVITY:Float = 0.1 '0.05
Public
Function Init:Void(particleAtlas:TextureAtlas)
		
End
Function Init:Void()
fire = LoadImage("particles/pt_aura_3.png",, Image.MidHandle)
End
Function Create:Void(p:Photon)
'		p.lifeTimer
p.alpha = 1.0
p.x0 = p.x
p.y0 = p.y
p.dy = Rnd(-1, -2)
p.dx = Rnd(1, 2)
p.img = fire
'	If fromRight
p.theta = Rnd(120, 90)
'		p.alpha = 0.5
'		p.dx = Rnd(5, 15)
'		If Not fromRight Then p.dx = - (p.dx)
'		p.dy = -Rnd(10, 20)
'		p.lifeTimer = Rnd(30, 70)
'		p.gravity = 0
'		p.img = fire
End
Function Render:Void(p:Photon)
If p.img = Null Then Return
SetAlpha(p.alpha)
DrawImage(p.img, p.x, p.y)
SetAlpha(1.0)
End
Function Update:Void(p:Photon)
p.x += (Cos(p.theta) * p.dx) * dt.delta
p.alpha -= 0.02
p.y += (-Sin(p.theta) * p.dx + p.dy) * dt.delta
p.dy = (p.dy + GRAVITY) * dt.delta
'	If p.dy >= 80 Then p.dy = 80
If p.alpha <= 0.0 Then p.alive = False
'	If p.alpha <= 0.05 Then p.alive = False
End
End
#end