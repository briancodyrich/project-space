Strict

Import GameStuff.Particle.photonManager


Class EngineFire Abstract
	Private
	Global fire:Image
	
	Public
	Function Init:Void(particleAtlas:TextureAtlas)

	End
	Function Init:Void()
		fire = LoadImage("particles/pt_blue.png",, Image.MidHandle)
	End
	Function Flush:Void()

	End
	Function Create:Void(p:Photon)
		If p.mirrored
			p.dx = -0.25 'original .3
		Else
			p.dx = 0.25
		End
		p.dy = 0
		p.alpha = 1.0
		'p.lifeTimer = 60
		p.img = fire
	End
	Function Render:Void(p:Photon)
		If p.img = Null Then Return
		SetAlpha(p.alpha)
		DrawImage(p.img, p.x, p.y + p.dy)
		DrawImage(p.img, p.x, p.y - p.dy)
		DrawImage(p.img, p.x, p.y)
		SetAlpha(1.0)
	End
	Function Update:Void(p:Photon)
		p.x -= p.dx
		p.dy += 0.1
		p.alpha -= 0.007
		If p.alpha < 0.00 Then p.alive = False
		If p.x < 0 or p.x > ScreenWidth Then p.alive = False
		If p.y < 0 or p.y > ScreenHeight Then p.alive = False
		If p.lifeTimer < 0 Then p.alive = False
	End
End