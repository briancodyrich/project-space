Strict 
Import space_game 


Class BoxShipData
	Field x:Int
	Field y:Int
	Field xoff:Int
	Field yoff:int
	Field m:bool
	Field id:int
	Method New(x:Int, y:Int, xoff:Int, yoff:Int, m:Bool, id:Int = 0)
		Self.x = x
		Self.y = y
		Self.xoff = xoff
		Self.yoff = yoff
		Self.m = m
		Self.id = id
	End
End