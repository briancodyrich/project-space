
Strict

Import types.enginefire
Import types.laser
Import types.steam
Import types.fire
Import types.beam
Import types.text
Import types.missile
Import types.spark
Import types.weld
Import photon
Import space_game

Class PhotonTypes
	Const EngineFire:Int = 1
	Const Laser:Int = 2
	Const Steam:Int = 3
	Const Fire:Int = 4
	Const Beam:Int = 5
	Const Text:Int = 6
	Const Missile:Int = 7
	Const Spark:Int = 8
	Const Weld:Int = 9
	
	Function GetType:Int(str:String)
		Select str.ToUpper()
			Case "LASER"
			Return Laser
			Case "CANNON"
			Return Laser
			Case "THERMITE"
			Return Laser
			Case "BEAM"
			Return Beam
			Case "MISSILE"
			Return Missile
			Case "WELD"
			Return Weld
		End
		Return 0
	End
End





Class PhotonManager
	Private
	Field _isStatic:Bool
	Field _usedelta:bool
	''for static
	Field pArray:Photon[]
	Field pSize:int
	Field pStack:Stack<Photon>
	Public
	Const useZone:Bool = true
	Const DrawZone:Bool = False
	Const zoneStart:Int = 700
	Const zoneEnd:Int = 1300
	Field isPaused:bool
	
	'Public
	Method New(particleSize:Int, isStatic:Bool = True, useDeltaTiming:Bool = False)
		_isStatic = isStatic
		If _isStatic
			pArray = New Photon[particleSize]
			For Local x:Int = 0 Until pArray.Length()
				pArray[x] = New Photon()
			Next
			pSize = 0
		End
		_usedelta = useDeltaTiming
	End
	
	Method Pause:Void(paused:Bool)
		isPaused = paused
	End
	
	Method Add:Void(x:int, y:int, x_offset:Int, y_offset:Int, mirrored:bool, type:Int)
		If pSize < pArray.Length()
			pArray[pSize].SetStart(GetTrueX(x, x_offset, mirrored), GetTrueY(y, y_offset), mirrored, type)
			Select type
				Case PhotonTypes.EngineFire
					EngineFire.Create(pArray[pSize])
				'Case PhotonTypes.Laser
				'	Laser.Create(pArray[pSize])
				Case PhotonTypes.Steam
					Steam.Create(pArray[pSize])
				Case PhotonTypes.Fire
					Fire.Create(pArray[pSize])
				Case PhotonTypes.Spark
					Spark.Create(pArray[pSize])
				Case PhotonTypes.Weld
					Weld.Create(pArray[pSize])
			End
			pSize += 1
		Else
		'	Print "Too many particles :O"
		End
	End
	
	Method Add:Void(x:int, y:int, x_offset:Int, y_offset:Int, mirrored:bool, type:Int, data:Object)
		If pSize < pArray.Length()
			pArray[pSize].SetStart(GetTrueX(x, x_offset, mirrored), GetTrueY(y, y_offset), mirrored, type)
			Select type
				Case PhotonTypes.Laser
					Local box:BoxShipData = BoxShipData(data)
					Laser.Create(pArray[pSize], GetTrueX(box.x, box.xoff, box.m), GetTrueY(box.y, box.yoff), box.id)
				Case PhotonTypes.Beam
					Local box:BoxShipData = BoxShipData(data)
					Beam.Create(pArray[pSize], GetTrueX(box.x, box.xoff, box.m), GetTrueY(box.y, box.yoff), box.id)
				Case PhotonTypes.Missile
					Local box:BoxShipData = BoxShipData(data)
					Missile.Create(pArray[pSize], GetTrueX(box.x, box.xoff, box.m), GetTrueY(box.y, box.yoff), box.id)
				Case PhotonTypes.Text
					Text.Create(pArray[pSize], UnboxString(data))
			End
			pSize += 1
		Else
		'	Print "Too many particles :O"
		End
	End
	
	Method Command:Void(command:int)
		Select command
		
			Default
		End
		
		
	End
	
	Method Command:Void(command:String, data:Object)
		
	End
	
	Method GetTrueX:Int(x:Int, x_offset:Int, mirrored:Bool)
		If mirrored = False
			Return x + x_offset
		Else
			Return x_offset + (Ship.MAX_X * Ship.GRID_SIZE) - x
		End
	End
	
	Method GetTrueY:Int(y:Int, y_offset:Int)
		Return y + y_offset
	End
	
	
	Method Render:Void()
		SetBlend(AdditiveBlend)
		For Local x:Int = 0 Until pSize
			Select(pArray[x].GetType())
				Case PhotonTypes.EngineFire
					EngineFire.Render(pArray[x])
				Case PhotonTypes.Laser
					Laser.Render(pArray[x])
				Case PhotonTypes.Steam
					Steam.Render(pArray[x])
				Case PhotonTypes.Fire
					Fire.Render(pArray[x])
				Case PhotonTypes.Beam
					Beam.Render(pArray[x])
				Case PhotonTypes.Text
					Text.Render(pArray[x])
				Case PhotonTypes.Missile
					Missile.Render(pArray[x])
				Case PhotonTypes.Spark
					Spark.Render(pArray[x])
				Case PhotonTypes.Weld
					Weld.Render(pArray[x])
'				Case PhotonTypes.BlownTrailPhoton
'					BlownTrailPhoton.Render(pArray[x])
'				Case PhotonTypes.SparkPhoton
'					SparkPhoton.Render(pArray[x])
'				Case PhotonTypes.BallisticPhoton
'					BallisticPhoton.Render(pArray[x])
'				Case PhotonTypes.OMGlobPhoton
'					OMGlobPhoton.Render(pArray[x])
'				Case PhotonTypes.OMGlobPhoton2
'					OMGlobPhoton2.Render(pArray[x])
'				Case PhotonTypes.GoldPhoton
'					GoldPhoton.Render(pArray[x])
				Default
					Print "Particle type not found " + pArray[x].GetType()
			End
		Next
		SetBlend(AlphaBlend)
		If DrawZone
			DrawLine(zoneStart, 0, zoneStart, ScreenHeight)
			DrawLine(zoneEnd, 0, zoneEnd, ScreenHeight)
		End
	End
	
	Method Update:Void()
		If isPaused Then Return
		Local current:Int = 0
		While current < pSize

			Select pArray[current].GetType()
				Case PhotonTypes.EngineFire
					EngineFire.Update(pArray[current])
				Case PhotonTypes.Laser
					Laser.Update(pArray[current])
				Case PhotonTypes.Steam
					Steam.Update(pArray[current])
				Case PhotonTypes.Fire
					Fire.Update(pArray[current])
				Case PhotonTypes.Beam
					Beam.Update(pArray[current])
				Case PhotonTypes.Text
					Text.Update(pArray[current])
				Case PhotonTypes.Missile
					Missile.Update(pArray[current])
				Case PhotonTypes.Spark
					Spark.Update(pArray[current])
				Case PhotonTypes.Weld
					Weld.Update(pArray[current])
'				Case PhotonTypes.BlownTrailPhoton
'					BlownTrailPhoton.Update(pArray[current])
'				Case PhotonTypes.SparkPhoton
'					SparkPhoton.Update(pArray[current])
'				Case PhotonTypes.BallisticPhoton
'					BallisticPhoton.Update(pArray[current])
'				Case PhotonTypes.OMGlobPhoton
'					OMGlobPhoton.Update(pArray[current])
'				Case PhotonTypes.OMGlobPhoton2
'					OMGlobPhoton2.Update(pArray[current])
'				Case PhotonTypes.GoldPhoton
'					GoldPhoton.Update(pArray[current])
			End
			If pArray[current].hasSubPhotons()
				For Local a:Int = 0 Until pArray[current].emitSize
				'	Print "size: " + pArray[current].emitSize
					If pArray[current].toEmit[a].hasDemands()
						Command(pArray[current].toEmit[a].type)
					Else
'						Add(pArray[current].toEmit[a].GetX(), pArray[current].toEmit[a].GetY(), pArray[current].toEmit[a].GetType())
					End
				Next
				pArray[current].emitSize = 0
			End
			
			If (pArray[current].alive = False)
				pArray[current].Copy(pArray[pSize - 1])
				pSize -= 1
			Else
				current += 1
			End
		Wend
	End	
	Method GetParticleCount:Int()
		If _isStatic Then Return pSize
		Return pStack.Length()
	End
	
	Method Clear:Void()
		If _isStatic
			pSize = 0
		Else
			pStack.Clear()
		End	
	End
End

Function LoadPhotonAssets:Void()
	'Local particleAtlas:TextureAtlas = New TextureAtlas(GAME_PARTICLE_LOCATION)
	EngineFire.Init()
	Laser.Init()
	Missile.Init()
	Steam.Init()
	Fire.Init()
	Spark.Init()
	Weld.Init()
End
