
Strict

Import space_game

Class SettingsData Implements Loadable
	Field test:String
	Field xpLog:Int[]
	Field logging:bool
	Field battles:Int
	Method New()
		xpLog = New Int[SkillManager.MISSING_SKILL_TEST]
	End
	Method FileName:String()
		Return "main.json"
	End
	Method OnSave:JsonObject()
		Local data:JsonObject = New JsonObject()
		data.Set("Keymap", Game.Keys.OnSave())
		data.SetString("data", test)
		data.SetInt("battles", battles)
		Local a:JsonArray = New JsonArray(xpLog.Length())
		For Local temp:Int = 0 Until xpLog.Length()
			a.SetInt(temp, xpLog[temp])
		Next
		data.Set("log", a)
		Return data
	End
	Method OnLoad:Void(data:JsonObject)
		test = data.GetString("data")
		If data.Contains("Keymap")
			Game.Keys.OnLoad(JsonObject(data.Get("Keymap")))
		End
		If data.Contains("log")
			Local a:JsonArray = (JsonArray(data.Get("log")))
			For Local temp:Int = 0 Until a.Length()
				xpLog[temp] = a.GetInt(temp)
			Next
		End
		battles = data.GetInt("battles", 0)
	End
	Method startLog:Void()
		battles += 1
	End
	Method leftEarly:Void()
		battles -= 1
	End
	Method LogXP:Void(type:Int, num:Int)
		If Game.CurrentScreenNumber = Screens.TestGame
			xpLog[type] += num
		End
	End
End