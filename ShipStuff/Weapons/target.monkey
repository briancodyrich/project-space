Strict

Import space_game


Class Target
	Field x:Int
	Field y:Int
	Field system:Hardpoint
	
	Method New(x:Int, y:Int, system:Hardpoint)
		Self.x = x
		Self.y = y
		Self.system = system
	End
	
	Method Render:Void()
		If system.isReady()
			SetColor(0, 255, 0)
		Else
			SetColor(255, 0, 0)
		End
		SetAlpha(0.5)
		DrawCircle(x, y, 20)
		ResetColorAlpha()
	End
	
	Method Update:Void()
		
	End
End