Strict

Import space_game

Class Weapon
	Const NO_AMMO:int = 0
	Const MISSILE:Int = 1
	Const CANNON:Int = 2
	Const THERMITE:Int = 3
	
	Const G_HEAT:Int = 100
	Const G_HDPS:Int = 101
	Const G_SDPS:Int = 102
	Const G_CDPS:Int = 103
	Field type:Int
	Field id:string
	Field img_gun:Image
	Field heat_gen:Int
	Field power_needed:Int
	Field cycle_time:Int
	Field power:int
	Field mass:int
	Field ammo_type:int
	Field boltData:JsonObject
	Field hotbar_name:String
	
	Field extend:Int
	
	
	Method New(data:JsonObject)
		img_gun = LoadImage("parts/" + data.GetString("img_gun"))
		id = data.GetString("id")
		hotbar_name = data.GetString("hotbar_name", "null")
		heat_gen = data.GetInt("heat_gen")
		power_needed = data.GetInt("power_needed")
		cycle_time = data.GetInt("cycle_time")
		mass = data.GetInt("mass")
		boltData = JsonObject(data.Get("bolt"))
		Select boltData.GetString("type")
			Case "MISSILE"
			ammo_type = MISSILE
			Case "CANNON"
			ammo_type = CANNON
			Case "THERMITE"
			ammo_type = THERMITE
		End
		
	End
	
	Method isReady:Bool()
		If power = cycle_time Then Return True
		Return false
	End
	
	Method AmmoTest:bool(ammo:AmmoCounter)
		If ammo_type = MISSILE And ammo.missile <= 0
			power = 0
			Return True
		Else
			ammo.missile -= 1
			Return False
		End
		If ammo_type = CANNON And ammo.cannon <= 0
			power = 0
			Return True
		Else
			ammo.cannon -= 1
			Return False
		End
		If ammo_type = THERMITE And ammo.thermite <= 0
			power = 0
			Return True
		Else
			ammo.thermite -= 1
			Return False
		End
		Return False
	End
	
	Method Render:Void(x:Int, y:Int, active:Bool, isPaused:bool)
		If Not isPaused
			If active
				extend += 1
			Else
				extend -= 1
			End
		End
		
		extend = Clamp(extend, 0, 25)
		If img_gun <> Null
			DrawImage(img_gun, ( (x + 1) * 30) + extend, (y * 30))
		Else
			DrawRect( ( (x + 1) * 30) + extend, (y * 30), 30, 30)
		End
		
		DrawImage(Tile.img_hull, ( (x + 1) * 30) + 15, (y * 30) + 15)
	End
	
	Method GetVal:float(type:Int)
		Select type
			Case G_CDPS
				Return (float(1000) / (cycle_time * 333)) * boltData.GetInt("crew_damage") * boltData.GetInt("fragments")
			Case G_HDPS
				Return (float(1000) / (cycle_time * 333)) * boltData.GetInt("hull_damage") * boltData.GetInt("fragments")
			Case G_SDPS
				Return (float(1000) / (cycle_time * 333)) * boltData.GetInt("shield_damage") * boltData.GetInt("fragments")
			Case G_HEAT
				Return heat_gen
		End
		Return 0.0
	End
End