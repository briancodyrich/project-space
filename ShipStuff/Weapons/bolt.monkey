Strict

Import space_game

Class Bolt
	Global idGlobal:Int
	Field x:Int
	Field id:int
	Field y:int
	Field type:String
	Field effect:String
	Field ignore_shield:Bool
	Field ignore_armor:Bool
	Field ignore_defence:Bool
	Field fragments:Int
	Field accuracy:int
	Field bolt_count:Int
	Field hull_damage:Int
	Field shield_damage:Int
	Field crew_damage:int
	Field targetShielded:bool
	Method New(x:Int, y:Int, bolt:JsonObject, targetShielded:bool)
		idGlobal += 1
		id = idGlobal
		Self.x = x
		Self.y = y
		Self.targetShielded = targetShielded
		
		type = bolt.GetString("type", "NULL")
		effect = bolt.GetString("effect", "NONE")
		ignore_shield = bolt.GetBool("ignore_shield", False)
		ignore_armor = bolt.GetBool("ignore_armor", False)
		ignore_defence = bolt.GetBool("ignore_defence", False)
		fragments = bolt.GetInt("fragments", 1)
		accuracy = bolt.GetInt("accuracy", 100)
		bolt_count = bolt.GetInt("bolts", 1)
		hull_damage = bolt.GetInt("hull_damage", 0)
		shield_damage = bolt.GetInt("shield_damage", 0)
		crew_damage = bolt.GetInt("crew_damage", 0)
	End
	#rem
	Method New(x:Int, y:Int, damage:Int, fragments:int, targetShielded:bool)
		idGlobal += 1
		id = idGlobal
		Self.x = x
		Self.y = y
		Self.damage = damage
		Self.fragments = fragments
		Self.targetShielded = targetShielded
	End
	#end
End