Strict

Import space_game


Class Battery Extends Component
	Field power:Int
	Field chargeing:Bool
	Field dischargeing:bool
	Field imgON:Image
	Field imgOFF:Image
	Field imgZERO:Image
	Field imgONE:Image
	Field imgTWO:Image
	Field state:int
	Field simflag:bool
	Field buffer:int
	Field buffer_max:int
	Field burnout:bool
	
	Method New(x:Int, y:int, data:JsonObject, x_offset:int, y_offset:int, mirrored:bool, damage:Int, level:int)
		Super.New(x, y, Component.BATTERY, data.GetString("id", "NULL"), data.GetString("display_name", "NULL"), x_offset, y_offset, mirrored, damage, level)
		imgON = LoadImage("parts/" + data.GetString("imgON"))
		imgOFF = LoadImage("parts/" + data.GetString("imgOFF"))
		imgZERO = LoadImage("parts/" + data.GetString("imgZERO"))
		imgONE = LoadImage("parts/" + data.GetString("imgONE"))
		imgTWO = LoadImage("parts/" + data.GetString("imgTWO"))
		mass = data.GetInt("mass")
		scrap_value = data.GetInt("scrap_value")
		capacity = data.GetInt("capacity")
		dischargeRate = data.GetInt("dischargeRate")
		efficiency = data.GetInt("efficiency")
		power = capacity * GetEfficiency()
	End
	
	Method getPowerLevel:int()
		Return (capacity * GetEfficiency()) / 30
	End
	 
	Method setQualityLevel:Void(level:Int)
		Super.setQualityLevel(level)
		power = capacity * GetEfficiency()
	End
	
	Method Update:Void(shipPower:int = 2, force_update:bool)
		power = Min(power, int(capacity * GetEfficiency()))
		chargeing = False
		dischargeing = False
		electricPacket = dischargeRate * GetEfficiency()
		If simflag
			power = capacity * GetEfficiency()
			simflag = False
		End
	End
	Method SaveState:Void()
		memory[0] = power
	End
	Method LoadState:Void()
		power = memory[0]
	End
	Method useUnit:Int(pool:Int, type:Int)
		If type = Component.POWER And isActive()
			If pool >= efficiency And power < capacity * GetEfficiency()
				chargeing = True
				pool -= efficiency
				If burnout
					buffer += 1
					If buffer >= buffer_max
						burnout = False
						power += buffer
						buffer = 0
					End
				Else
					power += 1
				End
				If power > capacity * GetEfficiency() Then power = capacity * GetEfficiency()
			End
		End
		
		If type = Component.RESTOREBATTERY And isActive()
			If pool > 0 And power < capacity * GetEfficiency()
				If burnout
					buffer += 1
					pool -= 1
					If buffer >= buffer_max
						burnout = False
						power += buffer
						buffer = 0
					End
				Else
					pool -= 1
					power += 1
				End
				
				If power > capacity * GetEfficiency() Then power = capacity * GetEfficiency()
			End
		End
		
		If type = Component.USEBATTERY And isActive()
			Local pd:Int = Min(electricPacket, power)
			dischargeing = True
			electricPacket = 0
			power -= pd
			If power = 0
				burnout = True
				buffer = 0
				buffer_max = (capacity * GetEfficiency() / 10)
			End
			pool += pd
		End
		Return pool
	End
	
	Method isPowerCircuit:Bool()
		Return True
	End
	
	Method isActive:Bool()
		If health >= 100 And isOnline() And Not isBroken() Then Return True
		Return False
	End
	
	Method UpdateStatus:Void()
		status.Clear()
		status.Push(getFormatedName())
		status.Push(QualityLevelToString(qualityLevel))
		status.Push("Power: " + power + "/" + int(capacity * GetEfficiency()))
		status.Push("Health: " + health + "/" + maxHealth)
		If permDamage > 0 Then
			status.Push("Damage Lv: " + permDamage)
		End
	End
	
	Method Render:Void(view:int = 0, isPaused:bool, explosion:int)
		If isActive() And explosion <= 0
			If power >= capacity * GetEfficiency() * 0.75
				DrawImage(imgON, (x * GRID_SIZE), (y * GRID_SIZE), expT)
			Else If power >= capacity * GetEfficiency() * 0.50
				DrawImage(imgTWO, (x * GRID_SIZE), (y * GRID_SIZE), expT)
			Else If power >= capacity * GetEfficiency() * 0.25
				DrawImage(imgONE, (x * GRID_SIZE), (y * GRID_SIZE), expT)
			Else
				DrawImage(imgZERO, (x * GRID_SIZE), (y * GRID_SIZE), expT)
			End
		Else
			DrawImage(imgOFF, (explosion * expX) + (x * GRID_SIZE), (explosion * expY) + (y * GRID_SIZE),(explosion*expT), 1.0, 1.0)
		End
		If imgON <> Null
			
		Else
			Super.Render(view, isPaused, explosion )
		End
	End
End