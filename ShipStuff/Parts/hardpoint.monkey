Strict

Import space_game


Class Hardpoint Extends Component
	Field imgON:Image
	Field imgOFF:Image
	Field imgDAM:Image
	Field gun:Weapon
	Field simflag:bool
	Field shutdown:bool = False
	Method New(x:Int, y:int, data:JsonObject, x_offset:int, y_offset:int, mirrored:bool, damage:Int, level:int)
		Super.New(x, y, Component.HARDPOINT, data.GetString("id", "NULL"), data.GetString("display_name", "NULL"), x_offset, y_offset, mirrored, damage, level)
		mass = data.GetInt("mass")
		scrap_value = data.GetInt("scrap_value")
		imgON = LoadImage("parts/" + data.GetString("imgON"))
		imgOFF = LoadImage("parts/" + data.GetString("imgOFF"))
		imgDAM = LoadImage("parts/" + data.GetString("imgDAM"))
	End
	
	Method getPowerLevel:int()
		If gun <> Null
		
			Select gun.boltData.GetString("effect")
				Case "DAMAGE"
					Return (gun.GetDPS() * 40)
				Case "SHIELD"
					Return (gun.GetDPS() * 15)
				Case "FIRE"
					Return (gun.GetDPS() * 60)
					
				Default
				Print "Powerlevel for weapon not found"
			End
			Return (gun.GetDPS() * 15)
		End
		Return 0
	End
	
	Method Update:Void(shipPower:Int = 2, force_update:bool)
		shutdown = True
		If Not force_update
			If heatPacket > 0 And Not simflag Then Game.OverParticle.Add( (x * 30) + Rnd(-5, 5) + 15, (y * 30) + Rnd(-5, 5) + 15, x_offset, y_offset, mirrored, PhotonTypes.Steam)
		End 
		If gun = Null
			electricPacket = 0
		Else
			If simflag
				gun.power = 0
				heatPacket = gun.heat_gen / gun.cycle_time
				simflag = False
			End
			If gun.power < gun.cycle_time
				electricPacket = -gun.power_needed
			Else
				electricPacket = 0
				If heatPacket = 0 Then shutdown = False
			End
		End
	End
	Method SaveState:Void()
		If gun <> Null
			memory[0] = heatPacket
			memory[1] = gun.power
		End
	End
	Method LoadState:Void()
		If gun <> Null
			heatPacket = memory[0]
			gun.power = memory[1]
		End
	End
	Method isReady:Bool()
		If gun = Null Then Return False
		If isActive() = False Then Return False
		If heatPacket = 0 Then Return gun.isReady()
		Return False
	End
	
	Method FireGun:Int()
		heatPacket += gun.heat_gen
		gun.power = 0
		Return gun.ammo_type
	End
	
	Method useUnit:Int(pool:Int, type:Int)
		If type = Component.POWER
			If electricPacket < 0 And pool >= - electricPacket
				shutdown = False
				pool += electricPacket
				electricPacket = 0
				gun.power += 1
			End
		End
		If type = Component.HEAT
			If heatPacket > 0
				If isActive()
					Local use:Int = Min(heatPacket, pool)
					heatPacket -= use
					pool -= use
					If heatPacket > 0 Then shutdown = True
				Else
				
				End
			End
		End
		Return pool
	End
	
	Method isPowerCircuit:Bool()
		Return True
	End
	
	Method isHeatCircuit:Bool()
		Return True
	End
	
	Method isActive:Bool()
		If health >= 100 And Not isBroken() Then Return True
		Return False
	End
	
	Method UpdateStatus:Void()
		status.Clear()
		If isActive()
			If gun <> Null
				status.Push(gun.id)
				status.Push("%" + (int( (float(gun.power) / gun.cycle_time * 100))))
				status.Push( (gun.power) + "/" + gun.cycle_time)
				status.Push("heat: " + heatPacket)
				
			Else
				status.Push("NO WEAPON")
			End
			status.Push("Health: " + health + "/" + maxHealth)
			If permDamage > 0 Then
				status.Push("Damage Lv: " + permDamage)
			End
		Else
			status.Push("InActive")
			status.Push("Health: " + health + "/" + maxHealth)
			If permDamage > 0 Then
				status.Push("Damage Lv: " + permDamage)
			End
		End
	End
	
	Method Render:Void(view:int = 0, isPaused:bool, explosion:int)
		If gun <> Null
			gun.Render(x, y, ( not shutdown) And isOnline(), isPaused)
		End
		If isActive() And explosion <= 0 And heatPacket = 0 And electricPacket = 0 And gun <> Null
			If imgON <> Null
				DrawImage(imgON, (x * 30), (y * 30))
			Else
				Super.Render(view, isPaused, explosion)
			End
		Else If isDamaged() Or isBroken()
			If imgDAM <> Null
				DrawImage(imgDAM, (explosion * expX) + (x * GRID_SIZE), (explosion * expY) + (y * GRID_SIZE), (explosion * expT), 1.0, 1.0)
			Else
				Super.Render(view, isPaused,explosion)
			End
		
		Else
			If imgOFF <> Null
				DrawImage(imgOFF, (explosion * expX) + (x * GRID_SIZE), (explosion * expY) + (y * GRID_SIZE), (explosion * expT), 1.0, 1.0)
			Else
				Super.Render(view, isPaused,explosion)
			End
		End
	End
End