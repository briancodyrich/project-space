Strict

Import space_game

Class Shield Extends Component
	Field rechargeCounter:int
	Field minPower:Bool
	
	Field power:int
	Field shieldBoost:int
	Field boostEffect:int
	
	Field simflag:bool
	Field imgON:Image
	Field imgOFF:Image
	Field imgDAM:Image
	Field noPower:bool
	Method New(x:Int, y:int, data:JsonObject, x_offset:int, y_offset:int, mirrored:bool, damage:Int, level:int)
		Super.New(x, y, Component.SHIELD, data.GetString("id", "NULL"), data.GetString("display_name", "NULL"), x_offset, y_offset, mirrored, damage, level)
		mass = data.GetInt("mass")
		scrap_value = data.GetInt("scrap_value")
		imgON = LoadImage("parts/" + data.GetString("imgON"))
		imgOFF = LoadImage("parts/" + data.GetString("imgOFF"))
		imgDAM = LoadImage("parts/" + data.GetString("imgDAM"))
		energyProduction = data.GetInt("energyProduction")
		rechargeRate = data.GetInt("rechargeRate")
		capacity = data.GetInt("capacity")
		power = capacity * GetEfficiency()
	End
	
	
	Method getPowerLevel:int()
		Return (capacity * GetEfficiency()) / 5
	End
	
	Method Update:Void(shipPower:Int = 1, force_update:Bool)
		minPower = False
		electricPacket = 0
		While rechargeCounter >= rechargeRate
			rechargeCounter -= rechargeRate
			power = Min(power + 1, int(capacity * GetEfficiency()))
		End
		shieldBoost = shipPower
		power = Min(power, int(capacity * GetEfficiency()))
		If shipPower > Ship.OFF And isActive()
			If (power < int(capacity * GetEfficiency())) Or simflag
				electricPacket = (energyProduction * GetEfficiency(True,, False) * Pow(2, shipPower - 1))
			End
		End
		If shipPower = 0
			noPower = True
		Else
			noPower = False
		End
		simflag = False
	End
	Method SaveState:Void()
		memory[0] = rechargeCounter
		memory[1] = power
	End
	Method LoadState:Void()
		rechargeCounter = memory[0]
		power = memory[1]
	End
	Method useUnit:Int(pool:Int, type:Int)
		If type = Component.POWER
			If electricPacket < 0 And pool >= - electricPacket
				pool += electricPacket
				electricPacket = 0
				rechargeCounter += (shieldBoost + boostEffect)
				boostEffect = 0
			End
		End
		Return pool
	End
	
	Method setQualityLevel:Void(level:Int)
		Super.setQualityLevel(level)
		power = capacity * GetEfficiency()
	End
	
	
	Method isPowerCircuit:Bool()
		Return True
	End
	
	Method isActive:Bool()
		If health >= 100 And isOnline() And Not isBroken() And electricPacket >= 0 Then Return True
		Return False
	End
	
	Method UpdateStatus:Void()
		status.Clear()
		status.Push(getFormatedName())
		status.Push(QualityLevelToString(qualityLevel))
		If isActive()
			status.Push("Active")
			status.Push("Power: " + power + "/" + int(capacity * GetEfficiency()))
		Else
			status.Push("InActive")
		End
		status.Push("Health: " + health + "/" + maxHealth)
		If permDamage > 0 Then
			status.Push("Damage Lv: " + permDamage)
		End
	End
	
	Method Render:Void(view:int = 0, isPaused:bool, explosion:int)
		If isActive() And explosion <= 0 And Not noPower 
			If imgON <> Null
				DrawImage(imgON, (x * 30), (y * 30))
			Else
				Super.Render(view, isPaused, explosion)
			End
			
		Else If isDamaged() Or isBroken()
			If imgDAM <> Null
				DrawImage(imgDAM, (explosion * expX) + (x * GRID_SIZE), (explosion * expY) + (y * GRID_SIZE), (explosion * expT), 1.0, 1.0)
			Else
				Super.Render(view, isPaused,explosion)
			End
		Else
			If imgOFF <> Null
				DrawImage(imgOFF, (explosion * expX) + (x * GRID_SIZE), (explosion * expY) + (y * GRID_SIZE),(explosion*expT), 1.0, 1.0)
			Else
				Super.Render(view, isPaused,explosion)
			End
		End
	End
End