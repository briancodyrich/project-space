Strict

Import space_game

Class HealingTube Extends Component
	Field imgON:Image
	Field imgOFF:Image
	Method New(x:Int, y:int, data:JsonObject, x_offset:int, y_offset:int, mirrored:bool, damage:Int, level:int)
		Super.New(x, y, Component.HEALTH, data.GetString("id", "NULL"), data.GetString("display_name", "NULL"), x_offset, y_offset, mirrored, damage, level)
		mass = data.GetInt("mass")
		scrap_value = data.GetInt("scrap_value")
		imgON = LoadImage("parts/" + data.GetString("imgON"))
		imgOFF = LoadImage("parts/" + data.GetString("imgOFF"))
		energyProduction = data.GetInt("energyProduction")
	End
	
	Method getBasePrice:Int()
		Return 200
	End
	
	Method Update:Void(shipPower:Int = 2, force_update:Bool)
		electricPacket = 0
		If isActive() Then electricPacket = energyProduction '- (damagePower * getDamageLevel()) 'taking this out for now
	End
	Method useUnit:Int(pool:Int, type:Int)
		If type = Component.POWER
			If electricPacket < 0 And pool >= - electricPacket
				pool += electricPacket
				electricPacket = 0
			End
		End
		Return pool
	End
	
	Method isPowerCircuit:Bool()
		Return True
	End
	
	
	Method isActive:Bool()
		If electricPacket >= 0 And Not isDamaged() And Not isBroken() And isOnline() Then Return true
		Return False
	End
	
	Method UpdateStatus:Void()
		status.Clear()
		status.Push(getFormatedName())
		If isActive()
			status.Push("Active")
		Else
			status.Push("InActive")
		End
		status.Push("Health: " + health + "/" + maxHealth)
		If permDamage > 0 Then
			status.Push("Damage Lv: " + permDamage)
		End
	End
	
	Method Render:Void(view:int = 0, isPaused:bool, explosion:int)
		If isActive() And explosion <= 0
			If imgON <> Null
				DrawImage(imgON, (x * 30), (y * 30))
			Else
				Super.Render(view, isPaused, explosion)
			End
		Else
			If imgOFF <> Null
				DrawImage(imgOFF, (explosion * expX) + (x * GRID_SIZE), (explosion * expY) + (y * GRID_SIZE),(explosion*expT), 1.0, 1.0)
			Else
				Super.Render(view, isPaused,explosion)
			End
		End
	End
End