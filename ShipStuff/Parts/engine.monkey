Strict

Import space_game


Class Engine Extends Component
	Field heatTank:Int
	Field maxHeat:int
	Field currentThrust:int
	Field overheated:bool
	Field extend:Int = 0
	Field retract:Bool = True
	Field noPower:bool
	Field imgON:Image
	Field imgOFF:Image
	Field imgOUT:Image
	Field imgDAM:Image
	Field bXoff:Float
	Field bYoff:Float
	Field bT:float
	Method New(x:Int, y:int, data:JsonObject, x_offset:int, y_offset:int, mirrored:bool, damage:Int, level:int)
		Super.New(x, y, Component.ENGINE, data.GetString("id", "NULL"), data.GetString("display_name", "NULL"), x_offset, y_offset, mirrored, damage, level)
		'Game.UnderParticle.Clear()
	'	Game.OverParticle.Clear()
		fuelUse = data.GetInt("fuelUse")
		mass = data.GetInt("mass")
		scrap_value = data.GetInt("scrap_value")
		imgON = LoadImage("parts/" + data.GetString("imgON"))
		imgOFF = LoadImage("parts/" + data.GetString("imgOFF"))
		imgDAM = LoadImage("parts/" + data.GetString("imgDAM"))
		imgOUT = LoadImage("parts/" + data.GetString("imgOUT"))
		energyProduction = data.GetInt("energyProduction")
		maxHeat = data.GetInt("maxHeat")
		heatProduction = data.GetInt("heatProduction")
		thrust = data.GetInt("thrust")
		bXoff = Rnd(-0.3, 0.3)
		bYoff = Rnd(-0.3, 0.3)
		bT = Rnd(-0.5, 0.5)
	End
	
	
	Method getPowerLevel:int()
		Return (thrust * GetEfficiency()) / 600
	End
	
	Method Update:Void(shipPower:Int = 2, force_update:Bool)
		If Not force_update
			If extend = 20
				Game.UnderParticle.Add( ( (x - 1) * 30) - 15, (y * 30) + 15, x_offset, y_offset, mirrored, PhotonTypes.EngineFire)
			End
			If heatPacket > 0
				Game.OverParticle.Add( (x * 30) + Rnd(-5, 5) + 15, (y * 30) + Rnd(-5, 5) + 15, x_offset, y_offset, mirrored, PhotonTypes.Steam)
				If overheated
					Game.OverParticle.Add( (x * 30) + Rnd(-5, 5) + 15, (y * 30) + Rnd(-5, 5) + 15, x_offset, y_offset, mirrored, PhotonTypes.Steam)
				End
			End
		End
		currentThrust = 0
		electricPacket = 0
		heatTank = 0
		
		lastHeat = heatPacket
		
		If shipPower = 0 And Not force_update Then retract = True Else retract = False
		If isActive()
			If heatPacket < maxHeat And overheated = False
				If shipPower = 1
					heatTank = (heatProduction * GetEfficiency(True,, False)) / 2
					heatPacket += (heatProduction * GetEfficiency(True,, False)) / 2
					electricPacket = (energyProduction * GetEfficiency(True,, False)) / 2
					currentThrust = (thrust * GetEfficiency() / 2)
				End
				If shipPower = 2
					heatTank = (heatProduction * GetEfficiency(True,, False))
					heatPacket += (heatProduction * GetEfficiency(True,, False))
					electricPacket = (energyProduction * GetEfficiency(True,, False))
					currentThrust = (thrust * GetEfficiency())
				End
				If shipPower = 3
					heatTank = (heatProduction * GetEfficiency(True,, False)) * 2
					heatPacket += (heatProduction * GetEfficiency(True,, False)) * 2
					electricPacket = (energyProduction * GetEfficiency(True,, False)) * 2
					currentThrust = (thrust * GetEfficiency() * 1.5)
				End
				If shipPower = 0
					noPower = True
				Else
					noPower = False
				End
			Else
				If overheated = False
					permDamage += Component.OVERHEAT_DAMAGE
				End
				overheated = True
				If heatPacket > maxHeat Then heatPacket = maxHeat
			End
		End
		
		If overheated = True
			If heatPacket <= maxHeat * 0.75
				overheated = False
			End
		End
		
	End
	Method useUnit:Int(pool:Int, type:Int)
		If type = Component.POWER
			If electricPacket < 0 And pool >= - electricPacket
				If health >= 100
					pool += electricPacket
					electricPacket = 0
				End
			End
		End
		If type = Component.HEAT
			If heatPacket > 0
				Local use:Int = Min(heatPacket, pool)
				heatPacket -= use
				pool -= use
			End
		End
		Return pool
	End
	Method SaveState:Void()
		memory[0] = heatPacket
		memory[1] = lastHeat
	End
	Method LoadState:Void()
		heatPacket = memory[0]
		lastHeat = memory[1]
	End
	
	Method isPowerCircuit:Bool()
		Return True
	End
	
	Method isHeatCircuit:Bool()
		Return True
	End
	
	Method isActive:Bool()
		If electricPacket >= 0 And health >= 100 And isOnline() And Not isBroken() Then Return True
		Return False
	End
	
	Method UpdateStatus:Void()
		status.Clear()
		status.Push(getFormatedName())
		status.Push(QualityLevelToString(qualityLevel))
		If isActive()
			status.Push("Active")
		Else
			status.Push("InActive")
		End
		If overheated Then status.Push("OVERHEATED")
		status.Push("Heat: " + heatPacket + "/" + maxHeat)
		status.Push("Health: " + health + "/" + maxHealth)
		If permDamage > 0 Then
			status.Push("Damage Lv: " + permDamage)
		End
	End
	
	Method Render:Void(view:int = 0, isPaused:bool, explosion:int)
		Local a:Float = GetAlpha()
		SetAlpha(1.0)
		DrawImage(imgOUT, (explosion * bXoff) + ( (x - 1) * 30) - extend, (explosion * bYoff) + (y * 30), expT * explosion, 1.0, 1.0)
		SetAlpha(a)
		If Not isPaused
			If (isActive() and Not overheated) and not retract
				extend += 1
			Else
				extend -= 1
			End
		End
		If explosion <= 0
			DrawImage(Tile.img_hull, ( (x - 1) * 30) + 15, (y * 30) + 15)
		End
		extend = Clamp(extend, 0, 20)
		If isActive() And Not overheated And explosion <= 0 And Not noPower 
			If imgON <> Null
				DrawImage(imgON, (x * 30), (y * 30))
			Else
				Super.Render(view, isPaused, explosion)
			End
		Else If isDamaged() Or isBroken()
			If imgDAM <> Null
				DrawImage(imgDAM, (explosion * expX) + (x * GRID_SIZE), (explosion * expY) + (y * GRID_SIZE), (explosion * expT), 1.0, 1.0)
			Else
				Super.Render(view, isPaused,explosion)
			End
		Else
			If imgOFF <> Null
				DrawImage(imgOFF, (explosion * expX) + (x * GRID_SIZE), (explosion * expY) + (y * GRID_SIZE),(explosion*expT), 1.0, 1.0)
			Else
				Super.Render(view, isPaused,explosion)
			End
		End
	End
End