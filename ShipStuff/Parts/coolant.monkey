Strict

Import space_game

Class CoolantTank Extends Component
	Field heatTank:int
	Field imgON:Image
	Field imgOFF:Image
	Field imgDAM:Image
	Field overloadFlag:bool
	Field used:bool
	Field simflag:bool
	Method New(x:Int, y:int, data:JsonObject, x_offset:int, y_offset:int, mirrored:bool, damage:Int, level:int)
		Super.New(x, y, Component.COOLANT_TANK, data.GetString("id", "NULL"), data.GetString("display_name", "NULL"), x_offset, y_offset, mirrored, damage, level)
		imgON = LoadImage("parts/" + data.GetString("imgON"))
		imgOFF = LoadImage("parts/" + data.GetString("imgOFF"))
		imgDAM = LoadImage("parts/" + data.GetString("imgDAM"))
		mass = data.GetInt("mass")
		scrap_value = data.GetInt("scrap_value")
		capacity = data.GetInt("capacity")
	End
	
	
	Method getPowerLevel:int()
		Return (capacity * GetEfficiency()) / 100
	End
	
	Method Update:Void(shipPower:Int = 2, force_update:bool)
		heatPacket = Min(int(capacity * GetEfficiency()), heatPacket)
		If simflag
			heatPacket = 0
			simflag = False
		End
		If Not force_update
			If heatPacket >= capacity * GetEfficiency() * 0.8
				Game.OverParticle.Add( (x * 30) + Rnd(-5, 5) + 15, (y * 30) + Rnd(-5, 5) + 15, x_offset, y_offset, mirrored, PhotonTypes.Steam)
				If heatPacket = capacity * GetEfficiency()
					Game.OverParticle.Add( (x * 30) + Rnd(-5, 5) + 15, (y * 30) + Rnd(-5, 5) + 15, x_offset, y_offset, mirrored, PhotonTypes.Steam)
				End
			End
		End 
		'	If heatPacket > 0 Then heatPacket -= 1
		overloadFlag = False
		used = False
		heatTank = (capacity * GetEfficiency()) -heatPacket
	End
	Method SaveState:Void()
		memory[0] = heatPacket
		memory[1] = heatTank
	End
	Method LoadState:Void()
		heatPacket = memory[0]
		heatTank = memory[1]
	End
	Method useUnit:Int(pool:Int, type:Int)
		If type = Component.GETHEATCAP
			If used = True Return - 1
			If Not isActive() Then Return - 1
			pool = heatTank
			heatTank = 0
			used = True
		End
	
		If type = Component.HEAT And isActive()
			If pool > 0
				Local storage:Int = Min(10, int(capacity * GetEfficiency()) -heatPacket)
				heatPacket += Min(storage, pool)
				pool -= Min(storage, pool)
			Else If pool < 0
				Local storage:Int = Clamp(heatPacket, 0, 10)
				heatPacket -= Min(storage, -pool)
				pool += Min(storage, -pool)
			End
				
		End
		If type = Component.DUMPHEAT And isActive()
			heatPacket = Max(0, heatPacket - pool)
		End
		Return pool
	End
	
	Method getHeat:Int()
		Return heatPacket
	End
	
	Method isHeatCircuit:Bool()
		Return True
	End
	
	Method isActive:Bool()
		If health >= 100 And isOnline() And Not isBroken() Then Return True
		Return False
	End
	
	Method UpdateStatus:Void()
		status.Clear()
		status.Push(getFormatedName())
		status.Push(QualityLevelToString(qualityLevel))
		If isActive()
			status.Push("Tank: " + heatPacket + "/" + int(capacity * GetEfficiency()))
			status.Push("Health: " + health + "/" + maxHealth)
			If permDamage > 0 Then
				status.Push("Damage Lv: " + permDamage)
			End
		Else
			If heatPacket = capacity
				status.Push("Tank Full")
			Else
				status.Push("Inactive")
			End
			status.Push("Tank: " + heatPacket + "/" + int(capacity * GetEfficiency()))
			status.Push("Health: " + health + "/" + maxHealth)
			If permDamage > 0 Then
				status.Push("Damage Lv: " + permDamage)
			End
		End
	End
	
	Method Render:Void(view:int = 0, isPaused:Bool, explosion:int)
		If isActive() And (heatPacket < capacity) And isOnline() And explosion <= 0
			If imgON <> Null
				DrawImage(imgON, (x * 30), (y * 30))
			Else
				Super.Render(view, isPaused, explosion)
			End
		Else If isDamaged() Or isBroken()
			If imgDAM <> Null
				DrawImage(imgDAM, (explosion * expX) + (x * GRID_SIZE), (explosion * expY) + (y * GRID_SIZE), (explosion * expT), 1.0, 1.0)
			Else
				Super.Render(view, isPaused,explosion)
			End
		Else
			If imgOFF <> Null
				DrawImage(imgOFF, (explosion * expX) + (x * GRID_SIZE), (explosion * expY) + (y * GRID_SIZE),(explosion*expT), 1.0, 1.0)
			Else
				Super.Render(view, isPaused,explosion)
			End
		End
	End
End