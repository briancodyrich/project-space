Strict

Import space_game


Class Solar Extends Component
	Field maxHeat:Int
	Field overheated:bool
	Field imgON:Image
	Field imgOFF:Image
	
	Method New(x:Int, y:int, data:JsonObject, x_offset:int, y_offset:int, mirrored:bool, damage:Int, level:int)
		Super.New(x, y, Component.SOLAR, data.GetString("id", "NULL"), data.GetString("display_name", "NULL"), x_offset, y_offset, mirrored, damage, level)
		imgON = LoadImage("parts/" + data.GetString("imgON"))
		imgOFF = LoadImage("parts/" + data.GetString("imgOFF"))
		mass = data.GetInt("mass")
		scrap_value = data.GetInt("scrap_value")
		energyProduction = data.GetInt("energyProduction")
	End
	
	
	Method getPowerLevel:int()
		Return (energyProduction * GetEfficiency()) * 2
	End
	
	Method Render:Void(view:int = 0, isPaused:bool, explosion:int)
		If isActive() And explosion <= 0
			If imgON <> Null
				DrawImage(imgON, (x * 30), (y * 30))
			Else
				Super.Render(view, isPaused, explosion)
			End
		Else
			If imgOFF <> Null
				DrawImage(imgOFF, (explosion * expX) + (x * GRID_SIZE), (explosion * expY) + (y * GRID_SIZE),(explosion*expT), 1.0, 1.0)
			Else
				Super.Render(view, isPaused, explosion)
			End
		End
	End
	
	Method useUnit:Int(pool:Int, type:Int)
		If isActive()
			If type = Component.POWER
				pool += electricPacket
				electricPacket = 0
			End
		End
		Return pool
	End
	
	Method Update:Void(shipPower:int, force_update:bool)
		electricPacket = energyProduction * GetEfficiency()
		If electricPacket < 0
			electricPacket = 0
		End
	End
	
	Method isPowerCircuit:Bool()
		Return True
	End
	
	
	Method isActive:Bool()
		If not isDamaged() and Not isBroken() And isOnline() Then Return True
		Return false
	End
	
	
	Method UpdateStatus:Void()
		status.Clear()
		status.Push(getFormatedName())
		status.Push("Health: " + health + "/" + maxHealth)
		If permDamage > 0 Then
			status.Push("Damage Lv: " + permDamage)
		End
	End
	
	'	Method Render:Void(view:int = 0)
	
	'End
End
