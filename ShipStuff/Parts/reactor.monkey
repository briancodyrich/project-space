Strict

Import space_game


Class Reactor Extends Component
	Field maxHeat:Int
	Field overheated:bool
	Field imgON:Image
	Field imgOFF:Image
	Field imgDAM:Image
	Field noPower:bool
	
	Method New(x:Int, y:int, data:JsonObject, x_offset:int, y_offset:int, mirrored:bool, damage:Int, level:int)
		Super.New(x, y, Component.REACTOR, data.GetString("id", "NULL"), data.GetString("display_name", "NULL"), x_offset, y_offset, mirrored, damage, level)
		imgON = LoadImage("parts/" + data.GetString("imgON"))
		imgOFF = LoadImage("parts/" + data.GetString("imgOFF"))
		imgDAM = LoadImage("parts/" + data.GetString("imgDAM"))
		mass = data.GetInt("mass")
		scrap_value = data.GetInt("scrap_value")
		energyProduction = data.GetInt("energyProduction")
		maxHeat = data.GetInt("maxHeat")
		heatProduction = data.GetInt("heatProduction")
	End
	
	
	Method getPowerLevel:int()
		Return (energyProduction * GetEfficiency()) * 3
	End
	
	Method Render:Void(view:int = 0, isPaused:Bool, explosion:int)
		If isActive() And explosion <= 0 And Not noPower
			If imgON <> Null
				DrawImage(imgON, (x * 30), (y * 30))
			Else
				Super.Render(view, isPaused, explosion)
			End
		Else If isDamaged() Or isBroken()
			If imgDAM <> Null
				DrawImage(imgDAM, (explosion * expX) + (x * GRID_SIZE), (explosion * expY) + (y * GRID_SIZE), (explosion * expT), 1.0, 1.0)
			Else
				Super.Render(view, isPaused,explosion)
			End
		Else
			If imgOFF <> Null
				DrawImage(imgOFF, (explosion * expX) + (x * GRID_SIZE), (explosion * expY) + (y * GRID_SIZE),(explosion*expT), 1.0, 1.0)
			Else
				Super.Render(view, isPaused,explosion)
			End
		End
	End
	
	Method useUnit:Int(pool:Int, type:Int)
		If isActive()
			If type = Component.POWER
				pool += electricPacket
				electricPacket = 0
			End
		End
		If type = Component.HEAT
			If heatPacket > 0
				Local use:Int = Min(heatPacket, pool)
				heatPacket -= use
				pool -= use
			End
		End
		
		
		Return pool
	End
	
	Method Update:Void(shipPower:int, force_update:bool)
		If heatPacket > 0
			If Not force_update
				Game.OverParticle.Add( (x * 30) + Rnd(-5, 5) + 15, (y * 30) + Rnd(-5, 5) + 15, x_offset, y_offset, mirrored, PhotonTypes.Steam)
				If overheated
					Game.OverParticle.Add( (x * 30) + Rnd(-5, 5) + 15, (y * 30) + Rnd(-5, 5) + 15, x_offset, y_offset, mirrored, PhotonTypes.Steam)
				End
			End
		End
		lastHeat = heatPacket
		If heatPacket < maxHeat And overheated = False
			If shipPower = 1
				electricPacket = (energyProduction * GetEfficiency()) / 2
				If isActive() Then heatPacket += (heatProduction * GetEfficiency(True,, False)) / 2
			End
			If shipPower = 2
				electricPacket = energyProduction * GetEfficiency()
				If isActive() Then heatPacket += (heatProduction * GetEfficiency(True,, False))
			End
			If shipPower = 3
				electricPacket = energyProduction * GetEfficiency() * 1.5
				If isActive() Then heatPacket += (heatProduction * GetEfficiency(True,, False)) * 2
			End
			If shipPower = 0
				noPower = True
			Else
				noPower = False
			End

		Else
			If overheated = False
				permDamage += Component.OVERHEAT_DAMAGE
			End
			overheated = True
			If heatPacket > maxHeat Then heatPacket = maxHeat
		End
		
		If overheated = True
			If heatPacket <= maxHeat * 0.75
				overheated = False
			End
		End
	End
	Method SaveState:Void()
		memory[0] = heatPacket
		memory[1] = lastHeat
	End
	Method LoadState:Void()
		heatPacket = memory[0]
		lastHeat = memory[1]
	End
	Method isPowerCircuit:Bool()
		Return True
	End
	
	Method isHeatCircuit:Bool()
		Return True
	End
	
	Method isActive:Bool()
		If overheated Then Return False
		If isDamaged() Or isBroken() Then Return False
		Return isOnline()
	End
	
	
	Method UpdateStatus:Void()
		status.Clear()
		status.Push(getFormatedName())
		status.Push(QualityLevelToString(qualityLevel))
		status.Push("Heat: " + heatPacket + "/" + maxHeat)
		If overheated Then status.Push("OVERHEATED")
		status.Push("Health: " + health + "/" + maxHealth)
		If permDamage > 0 Then
			status.Push("Damage Lv: " + permDamage)
		End
	End
	
'	Method Render:Void(view:int = 0)
	
	'End
End
