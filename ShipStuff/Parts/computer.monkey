Strict

Import space_game

Class Computer Extends Component
	Field imgON:Image
	Field imgOFF:Image
	Field system:Item
	Method New(x:Int, y:int, data:JsonObject, x_offset:int, y_offset:int, mirrored:bool, damage:Int, level:int)
		Super.New(x, y, Component.COMPUTER, data.GetString("id", "NULL"), data.GetString("display_name", "NULL"), x_offset, y_offset, mirrored, damage, level)
		mass = data.GetInt("mass")
		scrap_value = data.GetInt("scrap_value")
		imgON = LoadImage("parts/" + data.GetString("imgON"))
		imgOFF = LoadImage("parts/" + data.GetString("imgOFF"))
	End
	
	
	Method isControlCircuit:Bool()
		Return True
	End
	Method isActive:Bool()
		If health >= 100 and isOnline() And Not isBroken() Then Return True
		Return False
	End
	
	Method UpdateStatus:Void()
		status.Clear()
		status.Push(getFormatedName())
		If system <> Null
			status.Push(system.id)
		End
		status.Push("Health: " + health + "/" + maxHealth)
		If permDamage > 0 Then
			status.Push("Damage Lv: " + permDamage)
		End
	End
	
	Method Render:Void(view:int = 0, isPaused:bool, explosion:int)
		If isActive() And explosion <= 0
			If imgON <> Null
				DrawImage(imgON, (x * 30), (y * 30))
			Else
				Super.Render(view, isPaused, explosion)
			End
		Else
			If imgOFF <> Null
				DrawImage(imgOFF, (explosion * expX) + (x * GRID_SIZE), (explosion * expY) + (y * GRID_SIZE),(explosion*expT), 1.0, 1.0)
			Else
				Super.Render(view, isPaused,explosion)
			End
		End
	End
End