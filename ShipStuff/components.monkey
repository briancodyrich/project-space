Strict

Import space_game


Class Component Implements ComponentInterface
	Const NULLTYPE:Int = 0
	Const REACTOR:Int = 1 'done
	Const BATTERY:Int = 2 'batterys
	Const RADIATOR:Int = 3
	Const OXYGENGEN:Int = 4
	Const ENGINE:Int = 5 'done
	Const VENT:Int = 6
	Const SHIELD:Int = 7 'Shields
	Const DODGE:Int = 8
	Const HEALTH:Int = 9 'done
	Const COOLANT_TANK:Int = 10 'done
	Const COMPUTER:Int = 11 'done
	Const HARDPOINT:Int = 12 'Done
	Const SOLAR:Int = 13 'done
	Const SYSTEM_SLOT:Int = 14
	Const SHIELD_EXTENDER:Int = 15
	Const GRID_SIZE:Int = 30
	
	
	Const BREAKING_POINT:Int = 50
	Const BROKEN_LEVEL:Int = 5
	
	Const HEALING_PERSCRAP:Int = 10
	
	Const PLACEMENT_ANYWHERE:Int = 0
	Const PLACEMENT_OUTSIDE:Int = 1
	Const PLACEMENT_FRONT:Int = 2
	Const PLACEMENT_REAR:Int = 3
	
	Const POWER:Int = 100
	Const HEAT:Int = 200
	Const OXYGEN:Int = 300
	Const USEBATTERY:int = 400
	Const RESTOREBATTERY:Int = 500
	Const GETHEATCAP:Int = 600
	Const DUMPHEAT:Int = 700
	
	Const SCRAP_PER_DAMAGE:Int = 5
	
	Const DAMAGE_LEVEL_1:String = "Cracked "
	Const DAMAGE_LEVEL_2:String = "Damaged "
	Const DAMAGE_LEVEL_3:String = "Faulty "
	Const DAMAGE_LEVEL_4:String = "Defective "
	Const DAMAGE_LEVEL_5:String = "Broken "
	
	Const QUALITY_LEVEL_0:String = "Stock"
	Const QUALITY_LEVEL_1:String = "Normal"
	Const QUALITY_LEVEL_2:String = "Improved"
	Const QUALITY_LEVEL_3:String = "Great"
	Const QUALITY_LEVEL_4:String = "Excellent"
	Const QUALITY_LEVEL_5:String = "Optimal"
	Const QUALITY_LEVEL_6:String = "Legendary"
	Const QUALITY_LEVEL_7:String = "Transcendent"
	
	Const QUALITY_COLOR_0:Int = $7e7e7e
	Const QUALITY_COLOR_1:Int = $e7e7e7
	Const QUALITY_COLOR_2:Int = $ffffff
	Const QUALITY_COLOR_3:Int = $62d770
	Const QUALITY_COLOR_4:Int = $ffff66
	Const QUALITY_COLOR_5:Int = $50a0f2
	Const QUALITY_COLOR_6:Int = $802be0
	Const QUALITY_COLOR_7:Int = $f48519
	
	Const DAMAGE_PERCENT:Float = 0.2
	Const NORMAL_QUALITY:Float = 0.1
	Const LOW_QUALITY:Float = 0.05
	
	Const OVERHEAT_DAMAGE:Int = 5
	
	Const PRINTABLE_HEAT:Int = 1
	Const PRINTABLE_POWER_USE:Int = 2
	Const PRINTABLE_POWER_GEN:Int = 3
	Const PRINTABLE_STORAGE_SIZE:Int = 4
	Const PRINTABLE_DISCHARGE_RATE:Int = 5
	Const PRINTABLE_EFFICIENCY:int = 6
	Const PRINTABLE_RECHARGE_TIME:Int = 7
	Const PRINTABLE_RECHARGE_USE:Int = 8
	Const PRINTABLE_THRUST:Int = 9
	Const PRINTABLE_QUALITY_LEVEL:Int = 10
	Const PRINTABLE_FUEL_USE:Int = 11
	
	
	Field x:Int
	Field y:Int
	Field id:String
	Field displayName:String
	
	Field malfunction:bool
	Field online:bool
	Field permDamage:int
	Field qualityLevel:Int
	
	Field type:int
	Field slotType:int
	Field heatPacket:Int
	Field electricPacket:Int
	Field oxygenPacket:Int
	Field lastHeat:int
	Field health:Int
	Field maxHealth:Int
	Field mass:int
	Field memory:float[]
	Field status:Stack<String>
	
	Field tested:bool
	Field scrap_value:int
	Field marked_for_scrap:Bool
	
	
	Field x_offset:Int
	Field y_offset:Int
	Field mirrored:bool
	
	'possible values
	Field dischargeRate:Int
	Field rechargeRate:int
	Field efficiency:int
	Field capacity:int
	Field thrust:int
	Field fuelUse:int
	
	Field energyProduction:Int
	Field heatProduction:Int
	Field expX:Float
	Field expY:Float
	Field expT:float
	
	
	
	Method New(x:Int, y:int, type:int = NULLTYPE, id:string, displayName:String, x_offset:Int, y_offset:Int, mirrored:bool, damage:Int, level:int)
		status = New Stack<String>
		memory = New float[10]
		expX = Rnd(-0.2, 0.2)
		expY = Rnd(-0.2, 0.2)
		expT = Rnd(-1, 1)
		Self.id = id
		Self.displayName = displayName
		Self.x = x
		Self.y = y
		Self.type = type
		If type = SHIELD_EXTENDER
			slotType = BATTERY
		Else
			slotType = type
		End
		health = 100
		maxHealth = 100
		Self.qualityLevel = level
		Self.permDamage = damage
		online = True
		Self.scrap_value = scrap_value
		Self.x_offset = x_offset
		Self.y_offset = y_offset
		Self.mirrored = mirrored
	End
	
	Method getSlotType:Int()
		Return slotType
	End
	
	Method SetPosition:Void(x:Int, y:Int, x_offset:Int, y_offset:int)
		Self.x = x
		Self.y = y
		Self.x_offset = x_offset
		Self.y_offset = y_offset
		Self.mirrored = False
	End
	
	Method GetPrintable:Int(type:Int)
		Select type
			Case PRINTABLE_HEAT
				Return heatProduction * GetEfficiency(True,, False)
			Case PRINTABLE_POWER_USE
				Return - (energyProduction * GetEfficiency(True,, False))
			Case PRINTABLE_THRUST
				Return (thrust * GetEfficiency())
			Case PRINTABLE_POWER_GEN
				Return energyProduction * GetEfficiency()
			Case PRINTABLE_STORAGE_SIZE
				Return capacity * GetEfficiency()
			Case PRINTABLE_DISCHARGE_RATE
				Return dischargeRate * GetEfficiency()
			Case PRINTABLE_EFFICIENCY 'used by battery
				Return efficiency
			Case PRINTABLE_RECHARGE_TIME 'used by shields
				Return ( (capacity * GetEfficiency()) * rechargeRate) / 3 ' don't change this 3 its based on the tick rate
			Case PRINTABLE_RECHARGE_USE 'used by shields
				Return - (energyProduction * (GetEfficiency(True,, False)) * rechargeRate)
			Case PRINTABLE_FUEL_USE 'used by engines
				Return ( (thrust * GetEfficiency()) / fuelUse)
		End
		Return -999
	End
	
	Method getRandomQualityLevel:Int(min:Int = 0, max:Int = 7)
		Local r:int = Rnd(0, 10000)
		If r < 39 Return Clamp(0, min, max)
		If r < 78 Return Clamp(7, min, max)   '1/256
		If r < 156 Return Clamp(6, min, max)  '1/128
		If r < 312 Return Clamp(5, min, max)  '1/64
		If r < 625 Return Clamp(4, min, max)  '1/32
		If r < 1250 Return Clamp(3, min, max) '1/16
		If r < 2500 Return Clamp(2, min, max) '1/8
		If r < 5000 Return Clamp(1, min, max) '1/4
		Return Clamp(0, min, max)             '1/2  ---- actually is 1/1.985
	End
	
	Method GetSlotType:Int()
		Return slotType
	End
	
	Method getQualityLevel:Int()
		Return qualityLevel
	End
	
	Method setQualityLevel:Void(level:Int)
		qualityLevel = level
	End
	
	Method QualityLevelToString:String(level:Int)
		If level = 0 Then Return QUALITY_LEVEL_0
		If level = 1 Then Return QUALITY_LEVEL_1
		If level = 2 Then Return QUALITY_LEVEL_2
		If level = 3 Then Return QUALITY_LEVEL_3
		If level = 4 Then Return QUALITY_LEVEL_4
		If level = 5 Then Return QUALITY_LEVEL_5
		If level = 6 Then Return QUALITY_LEVEL_6
		If level = 7 Then Return QUALITY_LEVEL_7
		Return ""
	End
	Method QualityLevelToColor:Int(level:Int)
		If level = 0 Then Return QUALITY_COLOR_0
		If level = 1 Then Return QUALITY_COLOR_1
		If level = 2 Then Return QUALITY_COLOR_2
		If level = 3 Then Return QUALITY_COLOR_3
		If level = 4 Then Return QUALITY_COLOR_4
		If level = 5 Then Return QUALITY_COLOR_5
		If level = 6 Then Return QUALITY_COLOR_6
		If level = 7 Then Return QUALITY_COLOR_7
		Return 0
	End
	
	Method getPowerLevel:Int()
		Return 0
	End
	
	Method getType:Int()
		Return type
	End
	
	Method getSelf:Object()
		Return Self
	End
	
	Method useUnit:Int(pool:Int, type:Int)
		Return pool
	End
	
	Method getX:Int()
		Return x
	End
	
	Method getId:String()
		Return id
	End
	
	Method SaveState:Void()
		
	End
	
	Method LoadState:Void()
		
	End
	
	Method getFormatedName:String()
		Select(getDamageLevel())
			Case 1
				Return DAMAGE_LEVEL_1 + displayName
			Case 2
				Return DAMAGE_LEVEL_2 + displayName
			Case 3
				Return DAMAGE_LEVEL_3 + displayName
			Case 4
				Return DAMAGE_LEVEL_4 + displayName
			Default
		End
		If (getDamageLevel() >= BROKEN_LEVEL) Then Return DAMAGE_LEVEL_5 + displayName
		Return displayName
	End
	
	Method getY:Int()
		Return y
	End
	
	Method getHeat:Int()
		Return heatPacket
	End
	Method getMaxHealth:Int()
		Return maxHealth 
	End
	Method getPower:int()
		Return electricPacket
	End
	
	Method getHealth:Int()
		Return health
	End
	
	Method isBroken:Bool()
		If permDamage >= BREAKING_POINT
			permDamage = BREAKING_POINT
			Return True
		End
		Return False
	End
	
	Method GetEfficiency:Float(reverseValues:Bool = False, calculateQuality:Bool = True, normalQuality:bool = True)
		If getDamageLevel() >= BROKEN_LEVEL Then Return 0
		If reverseValues
			If calculateQuality
				Return (1 + (qualityLevel * QualityType(normalQuality))) * (1 + (getDamageLevel() * DAMAGE_PERCENT))
			Else
				Return (1 + (getDamageLevel() * DAMAGE_PERCENT))
			End
		Else
			If calculateQuality
				Return (1 + (qualityLevel * QualityType(normalQuality))) * (1 - (getDamageLevel() * DAMAGE_PERCENT))
			Else
				Return (1 - (getDamageLevel() * DAMAGE_PERCENT))
			End
		
		End
	End
	
	Method takeDamage:Void(damage:int)
		health -= damage
		If damage > 0
			permDamage += (maxHealth - health) / 10
		End
		If health < 0 Then health = 0
	End
	
	Method isDamaged:Bool()
		If health < 100 Then Return True
		Return False
	End
	
	Method useRefinedScrap:Void()
		permDamage -= HEALING_PERSCRAP
		If permDamage < 0 Then permDamage = 0
	End
	
	Method takeScrap:int()
		permDamage += SCRAP_PER_DAMAGE
		Return scrap_value 
	End
	
	Method isOverHeating:Bool()
		If lastHeat < heatPacket Then Return True
		Return False
	End
	
	Method getMass:Int()
		Return mass
	End
	
	Method getOxygen:Int()
		Return oxygenPacket
	End
	
	Method isActive:Bool()
		Return False
	End
	Method isPowerCircuit:Bool()
		Return False
	End
	Method isHeatCircuit:Bool()
		Return False
	End
	Method isOxygenCircuit:Bool()
		Return False
	End
	Method isAccessCircuit:Bool()
		Return False
	End
	Method isOnline:Bool()
		Return online
	End
	Method setState:Void(state:Bool)
		online = state
	End
	
	Method ResetHeatUseage:Void()
		heatPacket = lastHeat
	End
	
	Method getTested:Bool()
		Return tested
	End
	Method setTested:Void(val:Bool)
		tested = val
	End
	Method getStatus:Stack<String>()
		Return status
	End
	Method getPermDamage:Int()
		Return permDamage 
	End
	Method setPermDamage:void(damage:int)
		permDamage = damage
	End
	Method getDamageLevel:Int()
		Return permDamage / 10
	End
	
	
	Method UpdateStatus:Void()
		status.Clear()
		status.Push("NULL")
	End
	
	Method Render:Void(view:int = 0, isPaused:Bool, explosion:int)
		SetColor(0, 0, 255)
		DrawRect( (x * 30), (y * 30), 30, 30)
		ResetColorAlpha()
	End
	Method isMouseOver:Bool(x_offset:Int, y_offset:int, mirrored:bool)
		#rem
		If Game.Cursor.x() > (x_offset + (x * 30)) And Game.Cursor.x() < (x_offset + (x * 30) + 30) And Game.Cursor.y() > (y_offset + (y * 30)) And Game.Cursor.y() < (y_offset + (y * 30) + 30) Then Return True
		Return False
		#end
		If mirrored
			If Game.Cursor.x() > (x_offset + (Ship.MAX_X * Ship.GRID_SIZE) - (x * 30) - 30) And Game.Cursor.x() < (x_offset + (Ship.MAX_X * Ship.GRID_SIZE) - (x * 30)) And Game.Cursor.y() > (y_offset + (y * 30)) And Game.Cursor.y() < (y_offset + (y * 30) + 30) Then Return True
		Else
			If Game.Cursor.x() > (x_offset + (x * 30)) And Game.Cursor.x() < (x_offset + (x * 30) + 30) And Game.Cursor.y() > (y_offset + (y * 30)) And Game.Cursor.y() < (y_offset + (y * 30) + 30) Then Return True
		End
		Return False
	End
	
	
	Method SetComponentAlpha:Void(view:Int)
		Select view
			Case Tile.NORMAL_VIEW
			Case Tile.ELECTRIC_VIEW
				If isPowerCircuit()
					SetAlpha(0.6)
				Else
					SetAlpha(0.1)
				End
			Case Tile.HEAT_VIEW
				If isHeatCircuit()
					SetAlpha(0.6)
				Else
					SetAlpha(0.1)
				End
			Case Tile.OXYGEN_VIEW
				If isOxygenCircuit()
					SetAlpha(0.6)
				Else
					SetAlpha(0.1)
				End
			Case Tile.DAMAGE_VIEW
				If permDamage >= 10
					SetAlpha(1.0)
				Else
					SetAlpha(0.2)
				End
		End
	End
	
	Method Update:Void(shipPower:int = 2, force_update:bool)
		
	End
End

Interface ComponentInterface
	Method GetPrintable:Int(type:Int)
	Method SetPosition:Void(x:Int, y:Int, x_offset:Int, y_offset:int)
	Method Update:Void(shipPower:int, force_update:bool)
	Method UpdateStatus:Void()
	Method Render:Void(view:Int, isPaused:Bool, explosion:int)
	Method getType:int()
	Method getSlotType:int()
	Method isActive:bool()
	Method isPowerCircuit:Bool()
	Method isBroken:Bool()
	Method isDamaged:Bool()
	Method isHeatCircuit:Bool()
	Method isOxygenCircuit:Bool()
	Method isOverHeating:Bool()
	Method isAccessCircuit:Bool()
	Method isOnline:Bool()
	Method ResetHeatUseage:Void()
	Method setState:Void(state:bool)
	Method getMaxHealth:Int()
	Method getPowerLevel:Int()
	Method useRefinedScrap:Void()
	Method getPermDamage:Int()
	Method setPermDamage:Void(damage:Int)
	Method getQualityLevel:Int()
	Method getRandomQualityLevel:Int(min:Int, max:int)
	Method setQualityLevel:Void(level:Int)
	Method getStatus:Stack<String>()
	Method getMass:Int()
	Method getId:String()
	Method getFormatedName:String()
	Method SaveState:Void()
	Method LoadState:Void()
	Method getDamageLevel:Int()
	Method takeScrap:int()
	Method getHeat:Int()
	Method getSelf:Object()
	Method getPower:Int()
	Method getOxygen:Int()
	Method useUnit:Int(pool:Int, type:Int)
	Method getHealth:Int()
	Method SetComponentAlpha:Void(view:Int)
	Method takeDamage:Void(damage:int)
	Method QualityLevelToString:String(level:Int)
	Method QualityLevelToColor:Int(hex:Int)
	
	Method getTested:Bool()
	Method setTested:Void(val:Bool)
	Method getX:Int()
	Method getY:Int()
	Method isMouseOver:Bool(x_offset:Int, y_offset:int, mirrored:bool)
	
	
	
	
End

Function QualityType:Float(normal:Bool)
	If normal Then Return Component.NORMAL_QUALITY
	Return Component.LOW_QUALITY
End

Function FixFloatingPoint:Float(old:Float)
	Print old
	Local hold:Float = old - int(old)
	Print old - int(old)
	If hold >= 0.98 Then Return int(old) + 1
	Return old
End

Class PacketStack<T> Extends Stack<T>
	Method Compare:Int(lhs:T, rhs:T)
		If lhs.distance < rhs.distance Return - 1
		If lhs.distance > rhs.distance Return 1
		Return 0
	End
End

Class Packet
	Field device:ComponentInterface
	Field distance:Int
	
	Method New(device:ComponentInterface, distance:Int)
		Self.device = device
		Self.distance = distance
	End
	
End

Function HexToRed:int(hex:Int)
	Return (hex Shr 16) Mod 256
End
Function HexToGreen:int(hex:Int)
	Return (hex Shr 8) Mod 256
End
Function HexToBlue:int(hex:Int)
	Return hex Mod 256
End



'Class
