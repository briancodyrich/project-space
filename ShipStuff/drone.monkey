Strict

Import space_game


Class Drone
	Const MINING:Int = 1

	
	Const MODE_TARGET:Int = 0
	Const MODE_MOVING_TO_TARGET:Int = 1
	Const MODE_MINING:Int = 2
	Const MODE_FIND_HOME:Int = 3
	Const MODE_MOVING_TO_HOME:Int = 4
	Const MODE_ORE_DROPOFF:Int = 5
	Const MODE_DONE:Int = 6
	Field x:float
	Field y:float
	Field homeX:Int
	Field homeY:int
	Field startX:Int
	Field startY:int
	Field endX:Int
	Field endY:int
	Field laserX:Int
	Field laserY:Int
	Field tileX:Int
	Field tileY:int
	Field type:Int
	Field item:Item
	Field ship:Ship
	Field asteroid:Asteroid
	Field mode:Int = 0
	Field times:Float = 200
	Field mineCounter:Float = 0
	Field maxMine:Float = 200
	Field count:float
	Field rate:float
	Field haveOre:bool
	Field returnHome:Bool
	
	Field metals:ItemStack
	
	Method New(type:int, item:Item, ship:Ship)
		Self.item = item
		Self.ship = ship
		Self.type = type
		Repeat
			Local randX:Int = Rnd(Ship.MAX_X)
			Local randY:Int = Rnd(Ship.MAX_Y)
			If ship.pipes[ (randX * Ship.MAX_X) + randY].floor
				x = ship.x_offset + (randX * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2
				y = ship.y_offset + (randY * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2
				Exit
			End
		Forever
		homeX = x
		homeY = y
		startX = x
		startY = y
		metals = ItemLoader.GetItemList(Item.METAL)
		
	End
	
	Method SetTarget:Void(target:Asteroid, rate:float)
		Self.asteroid = target
		Self.rate = rate
	End
	
	Method Update:Void()
		If mode = MODE_DONE Then Return
		#rem
		If returnHome And Not (mode = MODE_FIND_HOME Or mode = MODE_MOVING_TO_HOME Or mode = MODE_DONE)
			mode = MODE_FIND_HOME
		End
		#end
		If mode = MODE_TARGET
			ResetCounters()
			If asteroid.isDead()
				mode = MODE_FIND_HOME
			Else
				Repeat
					Local randX:Int = Rnd(Ship.MAX_X)
					Local randY:Int = Rnd(Ship.MAX_Y)
					If asteroid.tiles[ (randX * Ship.MAX_X) + randY] > 0
						tileX = randX
						tileY = randY
						laserX = asteroid.x_offset + (randX * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2
						laserY = asteroid.y_offset + (randY * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2
						Local theta:Float = Rnd(PI * 2)
						Local rad:Float = Rnd(20, 80)
						endX = (Cosr(theta) * rad) + laserX
						endY = (Sinr(theta) * rad) + laserY
						mode += 1
						ResetCounters()
						Exit
					End
				Forever
			End
		Else If mode = MODE_MOVING_TO_TARGET
			If asteroid.isDead()
				ResetCounters()
				mode = MODE_FIND_HOME
			End
			If count >= times
				mode += 1
			Else
				x = Ease(count, startX, endX - startX, times)
				y = Ease(count, startY, endY - startY, times)
				If Distance(startX, startY, endX, endY) > 500
					count += 1 * dt.delta
				Else
					count += 3 * dt.delta
				End
			End
		Else If mode = MODE_MINING
			If returnHome
				mode = MODE_FIND_HOME
				Return
			End
			
			mineCounter += dt.delta * 1
			If Not tileExists()
				ResetCounters()
				If asteroid.isDead()
					mode = MODE_FIND_HOME
				Else
					mode = MODE_TARGET
				End
			End
			If mineCounter >= maxMine
				If tileExists()
					ResetCounters()
					If mineTile()
						haveOre = True
						mode += 1
					Else
						If tileExists()
							'keep on trucking
						Else If asteroid.isDead()
							mode = MODE_FIND_HOME
						Else
							startX = x
							startY = y
							mode = MODE_TARGET
						End
					End
				Else
					ResetCounters()
					mode = MODE_TARGET
				End
			End
		Else If mode = MODE_FIND_HOME
			ResetCounters()
			endX = homeX
			endY = homeY
			mode += 1
		Else If mode = MODE_MOVING_TO_HOME
			If count >= times
				If asteroid.isDead() Or returnHome
					DropOre()
					mode = MODE_DONE
					Return
				End
				mode += 1
			Else
				x = Ease(count, startX, endX - startX, times)
				y = Ease(count, startY, endY - startY, times)
				count += 1 * dt.delta
			End
		Else If mode = MODE_ORE_DROPOFF
			ResetCounters()
			DropOre()
			mode = MODE_TARGET
		Else If mode = MODE_DONE
			'stop moving forever
		End
	End
	't = current
	'b = start
	'c = change
	'd = duration
	Method Ease:float(t:float, b:float, c:float, d:float)
		t /= d / 2;
		if (t < 1) return c/2*t*t + b;
		t -= 1
		return -c/2 * (t*(t-2) - 1) + b;
	End
	
	Method DropOre:Void()
		If haveOre
			haveOre = False
			Local m:String = metals.Get(Rnd(metals.Length())).id
			Local success:Int = Player.ship.items.GiveItem(m, 1)
			Local elementStr:String
			Select m
				Case "Cobalt"
					elementStr = "+Co"
				Case "Tungsten"
					elementStr = "+W"
				Case "Platinum"
					elementStr = "+Pt"
				Case "Gold"
					elementStr = "+Au"
				Default
					elementStr = "+??"
			End
			If success = 0
				Game.OverParticle.Add(homeX, homeY - (Ship.GRID_SIZE / 2), 0, 0, False, PhotonTypes.Text, BoxString(elementStr))
			Else
				Game.OverParticle.Add(homeX, homeY - (Ship.GRID_SIZE / 2), 0, 0, False, PhotonTypes.Text, BoxString("#ff0000Full"))
			End
		End
	End
	
	Method ResetCounters:Void()
		startX = x
		startY = y
		count = 0
		mineCounter = 0
	End
	
	Method tileExists:Bool()
		If asteroid.tiles[ (tileX * Ship.MAX_X) + tileY] > 0 Then Return True
		Return False
	End
	
	Method mineTile:bool()
		If asteroid.tiles[ (tileX * Ship.MAX_X) + tileY] <= 0
			Return False
		End
		asteroid.tiles[ (tileX * Ship.MAX_X) + tileY] -= 1
		If Rnd(100) < rate * 100
			Return True
		End
		Return False
	End
	
	Method Render:Void()
		SetColor(0, 0, 255)
		DrawCircle(x, y, 10)
		ResetColorAlpha()
		If mode = MODE_MINING And Not returnHome And tileExists()
			DrawLine(x, y, laserX + Rnd(-10, 10), laserY + (Rnd(-10, 10)))
		End
	End
End