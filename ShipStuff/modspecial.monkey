Strict
Import space_game


Class ModSpecial
	Const TRIGGER:Int = 1
	Const BURN_FUEL:Int = 100
	Field effect:int
	Field type:int
	Field state:Int
	Field cooldownCounter:int
	Field cooldown:Int
	Field tickCounter:int
	Field stress:Int
	Field used:int
	Field power:Int
	Field ticks:int
	Field computer:Computer
	Field isActive:bool
	Field hotbar_name:string
	
	Method New(data:JsonObject, c:Computer = Null)
		computer = c
		Self.hotbar_name = data.GetString("hotbar_name", "null")
		Select data.GetString("effect")
			Case "EMP"
				effect = Modcodes.EMP
			Case "SHIELD_RECHARGE"
				effect = Modcodes.SHIELD_RECHARGE
			Case "THERMAL_RELEASE"
				effect = Modcodes.THERMAL_RELEASE
			Case "AFTERBURNER"
				effect = Modcodes.AFTERBURNER
			Case "STEALTH"
				effect = Modcodes.STEALTH
			Default
				Print "effect " + data.GetString("effect") + " not found"
		End
		Select data.GetString("type")
			Case "TRIGGER"
				type = TRIGGER
			
			Default
				Print "type " + data.GetString("type") + " not found"
		End
		stress = data.GetInt("stress")
		ticks = data.GetInt("ticks")
		cooldown = data.GetInt("cooldown")
		power = data.GetInt("power")
	End
	
	Method CombatReset:Void()
		cooldownCounter = 0
		used = 0
		tickCounter = 0
	End
	Method Reset:Void()
		tickCounter += 1
		If tickCounter >= ticks
			isActive = False
			used += 1
			cooldownCounter = 0
			tickCounter = 0
		End
	End
	Method canActivate:Bool()
		If computer <> Null And computer.isActive() = False
			Return False
		End
		Return True
	End
	Method Activate:Int()
		If canActivate() = False Then Return 0
		
		If cooldownCounter = (cooldown + (used * stress))
			isActive = True
			Select effect
				Case Modcodes.EMP
				Case Modcodes.SHIELD_RECHARGE
					SoundEffect.Play(SoundEffect.SHIELD_BOOSTER)
				Case Modcodes.THERMAL_RELEASE
					SoundEffect.Play(SoundEffect.THERMAL_RELEASE)
				Case Modcodes.AFTERBURNER
					SoundEffect.Play(SoundEffect.AFTERBURNER)
				Case Modcodes.STEALTH
					SoundEffect.Play(SoundEffect.STEALTH)
					Return BURN_FUEL
			End
		End
		Return 0
	End
	Method Update:Int()
		If isActive And canActivate() = False
			isActive = False
			used += 1
			cooldownCounter = 0
			tickCounter = 0
		End
		If Not isActive And canActivate() And cooldownCounter < (cooldown + (used * stress))
			cooldownCounter += 1
		End
		If isActive Return effect
		Return 0
	End
End