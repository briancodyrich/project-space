Strict

Import space_game


Class Ship
	Field x_offset:Int
	Field y_offset:Int
	Field mirrored:Bool
	Field shield_overlay:ShieldOverlay
	Field alpha:Float = 1.0
	Field pathcount:int
	Field particles:PhotonManager
	Const OFF:Int = 0
	Const LOW:Int = 1
	Const NORMAL:Int = 2
	Const OVERLOAD:Int = 3
	Const MAX_X:Int = 21
	Const MAX_Y:Int = 21
	Const GRID_SIZE:Int = 30
	Const FUEL_SIZE:Int = 1000
	Const MOVEMENT_COST:Int = 2000
	Const TRADE_IN_VALUE:Float = 0.9
	
	Const SURRENDER_LIMIT:Int = 4500
	Const DAMAGE_AP:Int = 8
	
	Const BROKEN_SHIELDS_FUNCTION:Bool = True
	Const MIN_SHIELD_PERCENT:Float = 0.1
	Const SMALL_SPARK_COUNT:Int = 3
	Const SMALL_SPARK_CHANCE:Int = 20
	Const BIG_SPARK_COUNT:Int = 30
	Const BIG_SPARK_CHANCE:Int = 2
	
	
	Const UPDATERATE:Int = 20
	Const FIRE_UPDATE_RATE:Int = 20
	Const PATHFINDING_COOLDOWN:Int = 0 '5
	Const SHIELD_SPEED_PENALTY:Float = 0.7
	Const NO_PILOT_PENALTY:Float = 0.40
	Const GUNNER_BONUS:Float = 1.5
	Const HEALTH_BASE:Int = 30
	Const FIRE_SPREAD_CHANCE:Int = 3
	Const FIRE_HEALTH:int = 200
	Const OXYGEN_PER_TILE:Float = 0.3
	Const FIRE_OXYGEN_BURN:Float = 1.2
	Const OXYGEN_DUMP:Float = 0.3
	
	Field td:int
	Field counter:int
	Field clock:Clock
	Field fireCounter:int
	Field explosion:int
	
	Field combatAI:AIPicker
	Field surrenderTimer:int
	Field quick_sim:bool
	Field heatRelease:int
	
	Field pipes:Tile[] = New Tile[MAX_X * MAX_Y]
	Field comp:Stack<ComponentInterface>
	Field compSlots:Stack<Slot>
	Field crew:Stack<Crew>
	Field crewSlots:Int
	Field items:Inventory
	Field systems:Item[]
	Field modSpecial:Stack<ModSpecial>
	
	Field systemSlots:Int
	Field defence:int
	Field mass:int
	
	Field hasSurrendered:bool
	Field surrenderAccepted:bool
	
	Field fuel:int
	Field storageSpace:Int
	Field value:int
	Field taskmanager:TaskManager
	
	'//controls
	Field enginePower:Int
	Field reactorPower:Int
	Field shieldPower:int
	Field dumpHeat:Bool
	
	Field batteryUnits:Int
	Field batteryMax:Int
	
	Field heatUnits:Int
	Field heatMax:int
	Field heatOutput:int
	Field heatGen:int
	
	Field shieldUnits:Int
	Field shieldMax:int
	
	Field powerUnits:Int
	Field powerMax:int
	
	Field HP:Int
	Field maxHP:Int
	Field AP:Int
	Field maxAP:int
	
	Field hullHealth:Int = 100
	
	Field thrust:float
	Field thrust_bonus:float
	Field batteryDrain:Bool
	Field engineOverheating:Bool
	Field reactorOverheating:Bool
	Field insuficientPower:Bool
	
	
	Field inCombat:Bool
	Field isBoarding:bool
	Field isCallingHome:bool
	
	Field targetShip:Ship
	Field aiControlled:bool
	
	Field bolts:Stack<Bolt>
	Field pathSaver:int
	
	'//stuff
	Field scrapElectronics:float
	
	Field fuel_mass:Int 'don't mess with this
	
	Field pilot_ai_on:Bool = True
	Field shield_on:Bool = True
	Field gun_ai_on:Bool = True
	Field dump_oxygen:Bool = False
	Field call_sign:String
	Field canSurrender:Bool
	Field surrender_reward:Int
	Field bounty_reward:int
	
	Field audio_shields_on:bool
	
	Field alerts:AlarmManager
	
	Method New(x_offest:Int, y_offset:Int, mirrored:bool)
		shield_overlay = New ShieldOverlay()
		Self.x_offset = x_offest
		Self.y_offset = y_offset
		Self.mirrored = mirrored
		If mirrored = False
			particles = New PhotonManager(300, True, True)
		End
		compSlots = New Stack<Slot>()
		fuel = FUEL_SIZE
		items = New Inventory()
		enginePower = NORMAL
		reactorPower = NORMAL
		shieldPower = NORMAL
		dumpHeat = True
		taskmanager = New TaskManager
		For Local x:Int = 0 Until pipes.Length()
			pipes[x] = New Tile()
		Next
		comp = New Stack<ComponentInterface>
		crew = New Stack<Crew>
		bolts = New Stack<Bolt>
		modSpecial = New Stack<ModSpecial>()
	End
	
	Method FileName:String()
		Return ""
	End
	
	Method AddCrew:Void(c:Crew)
		c.ship = Self
		crew.Push(c)
		BuildCrew()
	End
	
	Method BuildCrew:Void()
		If crew.IsEmpty()
			For Local temp:Int = 0 Until comp.Length()
				If comp.Get(temp).getType() = Component.HEALTH
					crew.Push(New Crew(comp.Get(temp).getX(), comp.Get(temp).getY(), Self, RaceLoader.GetRace("ANY", "ANY", "PLAYABLE")))
				End
			Next
		Else
			Local cc:Int = 0
			While cc < crew.Length()
				Local randX:Int = Rnd(MAX_X)
				Local randY:Int = Rnd(MAX_Y)
				If pipes[ (randX * MAX_X) + randY].isWalkable(False)
					If not crew.Get(cc).isDupe(randX, randY)
						crew.Get(cc).x = randX
						crew.Get(cc).y = randY
						cc += 1
					End
				End
			Wend
		End
	End
	Method SetCrewAi:Void()
		Local index:Int = SkillManager.SelectBest(crew, SkillManager.SKILL_PILOT, False)
		If index >= 0
			crew.Get(index).SetBasicJob(Crew.JOB_PILOT)
		End
		index = SkillManager.SelectBest(crew, SkillManager.SKILL_REPAIR, False)
		If index >= 0
			crew.Get(index).SetBasicJob(Crew.JOB_BACKUP_GUNNER)
		End
		index = SkillManager.SelectBest(crew, SkillManager.SKILL_GUNNER, False)
		If index >= 0
			crew.Get(index).SetBasicJob(Crew.JOB_GUNNER)
		End
		index = SkillManager.SelectBest(crew, SkillManager.SKILL_GUNNER, False)
		While index <> - 1
			crew.Get(index).SetBasicJob(Crew.JOB_MECHANIC)
			index = SkillManager.SelectBest(crew, SkillManager.SKILL_GUNNER, False)
		Wend
		
	End
	
	Method ShowCaseShip:Void()
		For Local temp:Int = 0 Until comp.Length()
			If comp.Get(temp).getType() = Component.ENGINE
				Local r:Engine = Engine(comp.Get(temp).getSelf())
				r.extend = 0
			End
			If comp.Get(temp).getType() = Component.HARDPOINT
				Local r:Hardpoint = Hardpoint(comp.Get(temp).getSelf())
				If r.gun <> Null
					r.gun.power = r.gun.cycle_time
					r.gun.extend = 0
				End
				r.online = True
			End
		Next
	End
	
	Method TurnOffGuns:Void()
		For Local temp:Int = 0 Until comp.Length()
			If comp.Get(temp).getType() = Component.HARDPOINT
				comp.Get(temp).setState(False)
			End
		End
	End
	
	Method isNull:bool()
		If GetMass() = 0 Then Return True
		Return False
	End
	
	Method GetValue:Int(area:Economy, equipsOnly:Bool = False, tradein_value:bool = False)
		Local value:Int
		'
		For Local temp:Int = 0 Until comp.Length()
			Local i:Item
			If comp.Get(temp).getType() = Component.COMPUTER
				Local c:Computer = Computer(comp.Get(temp).getSelf())
				If c.system <> Null
					value += c.system.GetValue(area)
				End
			Else If comp.Get(temp).getType() = Component.HARDPOINT
				Local h:Hardpoint = Hardpoint(comp.Get(temp).getSelf())
				If h.gun <> Null
					If ItemLoader.DoesItemExist(h.gun.id)
						i = New Item(ItemLoader.GetItem(h.gun.id))
						value += i.GetValue(area)
					End
				End
			Else
				i = New Item(ItemLoader.GetItem(comp.Get(temp).getId()), comp.Get(temp).getPermDamage(), comp.Get(temp).getQualityLevel())
				value += i.GetValue(area)
			End
		Next
		For Local temp:Int = 0 Until systems.Length()
			If systems[temp] <> Null
				value += systems[temp].GetValue(area)
			End
		Next
		Local addedValue:Int = 0
		If not equipsOnly
			addedValue += Self.value
			For Local temp:Int = 1 To 14
				value += GetSlotValue(temp)
			Next
			addedValue += maxHP * 20
			addedValue += (maxHP * (1.0 + (maxAP / 100.0))) * 30
			addedValue += heatRelease * 200
			addedValue += GetValueGrowth(storageSpace / 3, 9, 500)
			addedValue += GetValueGrowth(crewSlots, 2, 3000)
			addedValue *= Pow(2, defence / 20)
		End
		If tradein_value
			addedValue *= TRADE_IN_VALUE
		End
		Return value + addedValue
	End
	
	Method GetSlotValue:Int(type:Int)
		Local count:Int = 0
		For Local temp:Int = 0 Until compSlots.Length()
			If compSlots.Get(temp).type = type Then count += 1
		Next
		Select type
			Case Component.REACTOR
				Return GetValueGrowth(count, 3, 500)
			Case Component.BATTERY
				Return GetValueGrowth(count, 3, 500)
			Case Component.RADIATOR
				'Removed
			Case Component.OXYGENGEN
				'Removed
			Case Component.ENGINE
				Return GetValueGrowth(count, 4, 300)
			Case Component.VENT
				'removed
			Case Component.SHIELD
				Return GetValueGrowth(count, 2, 1500)
			Case Component.DODGE
				'removed
			Case Component.HEALTH
				Return GetValueGrowth(count, 4, 100)
			Case Component.COOLANT_TANK
				Return GetValueGrowth(count, 3, 600)
			Case Component.COMPUTER
				Return GetValueGrowth(count, 1, 2500)
			Case Component.HARDPOINT
				Return GetValueGrowth(count, 2, 5000)
			Case Component.SOLAR
				Return GetValueGrowth(count, 3, 500)
			Case Component.SYSTEM_SLOT
				Return GetValueGrowth(count, 2, 3000)
		End
		Return 0
	End
	
	Method GetFood:Int()
		Local useage:Int = 0
		For Local temp:Int = 0 Until crew.Length()
			useage += crew.Get(temp).GetFoodCost()
		Next
		If useage = 0 Then Return - 1
		Local food:Int = items.CountItem(Item.FOOD)
		Return (Clock.DAY / Crew.FOOD_TICK / (Crew.MAX_FOOD / float(useage))) * food
	End
	
	Method RepairHull:Void(rep:Int)
		HP += rep
	End
	
	Method GetWarpCode:Int()
		If enginePower = 0 Then Return WarpCode.ENGINES_OFFLINE
		If GetRange() <= 0
			If items.CountItem(Item.FUEL) = 0
				Return WarpCode.NO_FUEL
			End
			Return WarpCode.ENGINES_OFFLINE
		End
		If heatUnits > 0 Then Return WarpCode.HIGH_HEAT
		If powerUnits < 0 Then Return WarpCode.LOW_POWER
		Return WarpCode.READY_TO_WARP 
		
	End
	
	
	Method OnSave:JsonObject()
		Local data:JsonObject = New JsonObject()
		Local count:Int = 0
		Local num:Int = 0
		Local flags:Int = 0
		For Local x:Int = 0 Until pipes.Length()
			If pipes[x].floor or pipes[x].door or pipes[x].hull or pipes[x].scaffolding or pipes[x].airlock or pipes[x].walkway Or pipes[x].flag
				flags += 1
				num += 2
			End
		Next
		Local flagArr:JsonArray = New JsonArray(flags)
		Local pipeArr:JsonArray = New JsonArray(num)
		Local flagCount:Int = 0
		For Local x:Int = 0 Until pipes.Length()
			If pipes[x].floor or pipes[x].door or pipes[x].hull or pipes[x].scaffolding or pipes[x].airlock or pipes[x].walkway Or pipes[x].flag
				pipeArr.SetInt(count, x)
				count += 1
				Local temp:Int = 0
				If pipes[x].coolant Then temp += 1
				If pipes[x].power Then temp += 2
				If pipes[x].flag
					temp += 4
					flagArr.SetInt(flagCount, pipes[x].code)
					flagCount += 1
				End
				If pipes[x].floor Then temp += 8
				If pipes[x].door Then temp += 16
				If pipes[x].hull Then temp += 32
				If pipes[x].scaffolding Then temp += 64
				If pipes[x].airlock Then temp += 128
				'256
				pipeArr.SetInt(count, temp)
				count += 1
				
			End
		Next
		data.Set("Pipes", pipeArr)
		data.Set("Flags", flagArr)
		
		Local compArr:JsonArray = New JsonArray(comp.Length())
		For Local x:Int = 0 Until comp.Length()
			Local obj:JsonObject = New JsonObject
			obj.SetString("name", comp.Get(x).getId())
			obj.SetInt("x", comp.Get(x).getX())
			obj.SetInt("y", comp.Get(x).getY())
			obj.SetInt("damage", comp.Get(x).getPermDamage())
			obj.SetInt("level", comp.Get(x).getQualityLevel())
			If comp.Get(x).getType() = Component.HARDPOINT
				If Hardpoint(comp.Get(x).getSelf()).gun <> Null
					obj.SetString("gun", Hardpoint(comp.Get(x).getSelf()).gun.id)
				End
			End
			If comp.Get(x).getType() = Component.COMPUTER
				If Computer(comp.Get(x).getSelf()).system <> Null
					obj.SetString("system", Computer(comp.Get(x).getSelf()).system.id)
				End
			End
			compArr.Set(x, obj)
		Next
		data.Set("Objects", compArr)
		If compSlots.Length() = 0
			SetSlots()
		End
		Local sysArr:JsonArray = New JsonArray(systems.Length())
		For Local systemp:Int = 0 Until systems.Length()
			If systems[systemp] = Null
				sysArr.SetString(systemp, "null")
			Else
				sysArr.SetString(systemp, systems[systemp].id)
			End
		End
		data.Set("Systems", sysArr)
		Local slotArr:JsonArray = New JsonArray(compSlots.Length() * 3)
		For Local temp:Int = 0 Until compSlots.Length()
			slotArr.SetInt( (temp * 3), compSlots.Get(temp).x)
			slotArr.SetInt( (temp * 3) + 1, compSlots.Get(temp).y)
			slotArr.SetInt( (temp * 3) + 2, compSlots.Get(temp).type)
		Next
		data.Set("Slots", slotArr)
		
		Local crewArr:JsonArray = New JsonArray(crew.Length())
		For Local temp:Int = 0 Until crew.Length()
			crewArr.Set(temp, crew.Get(temp).OnSave())
		Next
		data.Set("Crew", crewArr)
		data.SetInt("heatRelease", heatRelease)
		data.SetInt("storageSpace", storageSpace)
		data.SetInt("HP", HP)
		data.SetInt("maxHP", maxHP)
		data.SetInt("AP", AP)
		data.SetInt("maxAP", maxAP)
		data.SetInt("mass", mass)
		data.SetInt("value", value)
		data.SetInt("systemSlots", systemSlots)
		data.Set("Items", items.OnSave())
		Return data
	End
	
	Method OnLoad:Void(data:JsonObject, extra:JsonObject = Null)'forceStock:Bool = False)
		For Local temp:Int = 0 Until pipes.Length()
			pipes[temp] = New Tile()
		Next
		crewSlots = 0
		Local pipeArr:JsonArray = JsonArray(data.Get("Pipes"))
		Local flagArr:JsonArray = JsonArray(data.Get("Flags"))
		Local flagCount:Int = 0
		If pipeArr.Length() > 0
			For Local count:Int = 0 Until pipeArr.Length() Step 2
				Local temp:Int = pipeArr.GetInt(count + 1)
				Local index:Int = pipeArr.GetInt(count)
				pipes[index].coolant = Bool(temp Mod 2)
				pipes[index].power = Bool(temp Shr 1 Mod 2)
				pipes[index].flag = Bool(temp Shr 2 Mod 2)
				pipes[index].floor = Bool(temp Shr 3 Mod 2)
				pipes[index].door = Bool(temp Shr 4 Mod 2)
				pipes[index].hull = Bool(temp Shr 5 Mod 2)
				pipes[index].scaffolding = Bool(temp Shr 6 Mod 2)
				pipes[index].airlock = Bool(temp Shr 7 Mod 2)
				pipes[index].walkway = Bool(temp Shr 8 Mod 2)
				pipes[index].health = 100
				If pipes[index].flag
					If flagArr <> Null And flagCount < flagArr.Length()
						pipes[index].code = flagArr.GetInt(flagCount)
						flagCount += 1
						If pipes[index].isFlag(FlagCode.BED)
							crewSlots += 1
							taskmanager.AddPerm(Tile.indexToX(index), Tile.indexToY(index), Task.SLEEP)
						End
					End
				End
			Next
		End
		systemSlots = data.GetInt("systemSlots")
		systems = New Item[systemSlots]
		If data.Get("Systems") <> Null
			Local sysArr:JsonArray = JsonArray(data.Get("Systems"))
			For Local count:Int = 0 Until sysArr.Length()
				If sysArr.GetString(count) <> "null" And count < systems.Length()
					If ItemLoader.DoesItemExist(sysArr.GetString(count))
						systems[count] = New Item(ItemLoader.GetItem(sysArr.GetString(count)))
					End
				End
			Next
		End
		
		comp.Clear()
		Local compArr:JsonArray = JsonArray(data.Get("Objects"))
		If compArr.Length() > 0
			For Local count:Int = 0 Until compArr.Length()
				Local item:JsonObject = JsonObject(compArr.Get(count))
				If AddComponent(item.GetInt("x"), item.GetInt("y"), item.GetString("name"), item.GetInt("damage"), item.GetInt("level"))
					If item.GetString("gun") <> ""
						If ItemLoader.DoesItemExist(item.GetString("gun"))
							Hardpoint(comp.Top().getSelf()).gun = New Weapon(ItemLoader.GetItem(item.GetString("gun")))
						End
					End
					If item.GetString("system") <> ""
						If ItemLoader.DoesItemExist(item.GetString("system"))
							Computer(comp.Top().getSelf()).system = New Item(ItemLoader.GetItem(item.GetString("system")))
						End
					End
				End
			Next
		End
		#rem
		If data.Contains("min") And forceStock = False
			For Local temp:Int = 0 Until comp.Length()
				comp.Get(temp).setQualityLevel(comp.Get(temp).getRandomQualityLevel(data.GetInt("min"), data.GetInt("max")))
			Next
		End
		#end
		Local slotArr:JsonArray = JsonArray(data.Get("Slots"))
		compSlots.Clear()
		If slotArr.Length() > 0
			If data.Get("Slots") <> Null
				For Local count:Int = 0 Until slotArr.Length() Step 3
					compSlots.Push(New Slot(slotArr.GetInt(count), slotArr.GetInt(count + 1), slotArr.GetInt(count + 2)))
				Next
			End
		End
		
		If data.Get("Crew") <> Null
			Local crewArr:JsonArray = JsonArray(data.Get("Crew"))
			crew.Clear()
			If crewArr.Length() > 0
				For Local count:Int = 0 Until crewArr.Length()
					Local c:Crew = New Crew(0, 0, Self, Null)
					c.OnLoad(JsonObject(crewArr.Get(count)))
					crew.Push(c)
				Next
			End
		End
	 	If extra <> Null
			canSurrender = extra.GetBool("can_surrender", True)
			Local so:float = extra.GetInt("surrender_reward", 1)
			surrender_reward = so + Rnd(so)
			so = extra.GetInt("bounty_reward", 1)
			bounty_reward = so + Rnd(so)
			Local cNum:int = extra.GetInt("crew_number", 2)
			For Local temp:Int = 0 Until cNum
				If temp < comp.Length()
					Local c:Crew = New Crew(comp.Get(temp).getX(), comp.Get(temp).getY(), Self, RaceLoader.GetRace("ANY", "ANY", "PLAYABLE"))
					crew.Push(c)
				End
			Next
			BuildCrew()
		End
		
		
		storageSpace = data.GetInt("storageSpace")
		systemSlots = data.GetInt("systemSlots")
		HP = data.GetInt("HP")
		maxHP = data.GetInt("maxHP")
		AP = data.GetInt("AP")
		maxAP = data.GetInt("maxAP")
		heatRelease = data.GetInt("heatRelease")
		mass = data.GetInt("mass")
		value = data.GetInt("value")
		items = New Inventory(storageSpace)
		items.OnLoad(JsonObject(data.Get("Items")))
		shield_overlay.SetShip(Self)
		UpdateMods()
	End
	
	Method UpdateMods:Void()
		modSpecial.Clear()
		For Local temp:Int = 0 Until comp.Length()
			If comp.Get(temp).getType() = Component.COMPUTER
				Local c:Computer = Computer(comp.Get(temp))
				If c.system <> Null
					If c.system.combat_effect <> Null
						modSpecial.Push(New ModSpecial(c.system.combat_effect, c))
					End
				End
			End
		Next
		For Local temp:Int = 0 Until systems.Length()
			If systems[temp] <> Null
				If systems[temp].combat_effect <> Null
					modSpecial.Push(New ModSpecial(systems[temp].combat_effect))
				End
			End
		Next
	End
	
	Method InitStorage:Void()
		items = New Inventory(storageSpace)
	End
	
	Method CombatReset:Void()
		For Local temp:Int = 0 Until comp.Length()
			If comp.Get(temp).getType() = Component.HARDPOINT
				If Hardpoint(comp.Get(temp).getSelf()).gun <> Null
					Hardpoint(comp.Get(temp).getSelf()).gun.power = 0
				End
			End
		Next
		For Local temp:Int = 0 Until modSpecial.Length()
			modSpecial.Get(temp).CombatReset()
		Next
	End
	
	Method GetSystemEffect:Int(code:Int)
		Local v:Int = 0
		For Local temp:Int = 0 Until comp.Length()
			If comp.Get(temp).getType() = Component.COMPUTER And comp.Get(temp).isActive()
				Local c:Computer = Computer(comp.Get(temp))
				If c.system <> Null And c.system.effect <> "NULL"
					If c.system.effect = Modcodes.CodeToString(code)
						v += c.system.power
					End
				End
			End
		Next
		For Local temp:Int = 0 Until systems.Length()
			If systems[temp] <> Null And systems[temp].effect <> "NULL"
				If systems[temp].effect = Modcodes.CodeToString(code)
					v += systems[temp].power
				End
			End
		Next
		Return v
	End
	
	Method SetSlots:Void()
		For Local temp:Int = 0 Until comp.Length()
			compSlots.Push(New Slot(comp.Get(temp).getX(), comp.Get(temp).getY(), comp.Get(temp).getSlotType()))
		Next
		For Local temp:Int = 0 Until systemSlots
			compSlots.Push(New Slot(temp, 0, Component.SYSTEM_SLOT))
		Next
	End
	
	Method DoesPermExist:Bool(crewx:Int, crewy:Int, type:int)
		If pathSaver <> 0 Then Return False
		For Local temp:Int = 0 Until taskmanager.permList.Length()
			If taskmanager.permList.Get(temp).type = type
				If doesPathOverlap(crewx, crewy, taskmanager.permList.Get(temp).x, taskmanager.permList.Get(temp).y) = False
					Return True
				End
			End
		Next
		Return False
	End
	
	Method GetJob:Int(crewx:Int, crewy:Int, aiType:Int)
		If pathSaver <> 0 Then Return 0
		Local retCode:Int = 0
		Select aiType
			Case AICode.AI_MECHANIC
				For Local temp:Int = 0 Until taskmanager.taskList.Length()
					If taskmanager.taskList.Get(temp).type = Task.SYSTEM_REPAIR
						If doesPathOverlap(crewx, crewy, taskmanager.taskList.Get(temp).x, taskmanager.taskList.Get(temp).y) = False
							Return Task.SYSTEM_REPAIR
						End
					End
					If taskmanager.taskList.Get(temp).type = Task.MAJOR_REPAIR
						If doesPathOverlap(crewx, crewy, taskmanager.taskList.Get(temp).x, taskmanager.taskList.Get(temp).y) = False
							Return Task.MAJOR_REPAIR
							
						End
					End
					If taskmanager.taskList.Get(temp).type = Task.MINOR_REPAIR
						If doesPathOverlap(crewx, crewy, taskmanager.taskList.Get(temp).x, taskmanager.taskList.Get(temp).y) = False
							retCode = Task.MINOR_REPAIR
						End
					End
				Next
			Case AICode.AI_PILOT
				If enginePower = OFF Or not pilot_ai_on Then Return 0
				For Local temp:Int = 0 Until taskmanager.permList.Length()
					If taskmanager.permList.Get(temp).type = Task.USE_COMPUTER
						If doesPathOverlap(crewx, crewy, taskmanager.permList.Get(temp).x, taskmanager.permList.Get(temp).y) = False
							Return Task.PILOT_SHIP
						End
					End
				Next
			Case AICode.AI_GUNNER
				If Not gun_ai_on Then Return 0
				For Local temp:Int = 0 Until taskmanager.permList.Length()
					If taskmanager.permList.Get(temp).type = Task.USE_GUNS
						If doesPathOverlap(crewx, crewy, taskmanager.permList.Get(temp).x, taskmanager.permList.Get(temp).y) = False
							Return Task.USE_GUNS
						End
					End
				Next
			Case AICode.AI_RESTING
				If targetShip = Null
					For Local temp:Int = 0 Until taskmanager.permList.Length()
						If taskmanager.permList.Get(temp).type = Task.SLEEP
							If doesPathOverlap(crewx, crewy, taskmanager.permList.Get(temp).x, taskmanager.permList.Get(temp).y) = False
								Return Task.SLEEP
							End
						End
					Next
				End
			Case AICode.AI_FIREFIGHTER
				For Local t:Int = 0 Until taskmanager.taskList.Length()
					If taskmanager.taskList.Get(t).type = Task.FIRE
						If doesPathOverlap(crewx, crewy, taskmanager.taskList.Get(t).x, taskmanager.taskList.Get(t).y) = False
							Return Task.FIRE
						End
					End
				Next
		End
		
		Return retCode
	End
	
	
	
	Method doesPathOverlap:Bool(crewx:Int, crewy:int, x:Int, y:int)
		If crewx = x And crewy = y Then Return False
		For Local temp:Int = 0 Until crew.Length()
			If crew.Get(temp).x = x And crew.Get(temp).y = y Then Return True
			If crew.Get(temp).target_x = x And crew.Get(temp).target_y = y Then Return True
		Next
		Return False
	End
	
	
	
	
	Method DestroyWalkWay:bool()
		For Local temp:Int = 0 Until crew.Length()
			If crew.Get(temp).hasBoarded = True Then Return False
			If pipes[crew.Get(temp).x * Ship.MAX_X + crew.Get(temp).y].floor = False Then Return False
		Next
		For Local temp:Int = 0 Until pipes.Length()
			If targetShip.pipes[temp].walkway = True
				targetShip.pipes[temp].walkway = False
				targetShip.pipes[temp].o2 = 0
			End
			If pipes[temp].walkway = True
				pipes[temp].walkway = False
				pipes[temp].o2 = 0
			End
			If pipes[temp].airlock = True Then pipes[temp].doorOpen = False
			If targetShip.pipes[temp].airlock = True Then targetShip.pipes[temp].doorOpen = False
		Next
		isBoarding = False
		isCallingHome = False
		Return True
	End
	
	Method DrawOutline:Void()
		DrawRectOutline(x_offset, y_offset, MAX_X * GRID_SIZE, MAX_Y * GRID_SIZE)
	End
	
	Method ExtendWalkway:Void()
		For Local x:Int = 0 Until MAX_X
			For Local y:Int = 0 Until MAX_Y
				If pipes[x * MAX_X + y].airlock
					If pipes[x * MAX_X + y + 1].isVacuum()
						FloodWalkWay(x, y + 1)
						targetShip.ExtendWalkway()
						isBoarding = True
						Return
					Else If pipes[x * MAX_X + y - 1].isVacuum()
						FloodWalkWay(x, y - 1)
						targetShip.ExtendWalkway()
						isBoarding = True
						Return
					End
				End
			Next
		Next
	End
	
	Method FloodWalkWay:Void(x:Int, y:int)
		If RoomExists(x, y)
			If pipes[x * MAX_X + y].isVacuum()
				pipes[x * MAX_X + y].walkway = True
			Else
				Return
			End
		Else
			Return
		End
		FloodWalkWay(x, y + 1)
		FloodWalkWay(x, y - 1)
	End
	
	
	
	
	Method AddBolts:Void(b:Bolt)
		bolts.Push(b)
	End
	
	Method isDead:Bool()
		If surrenderAccepted Then Return True
		If crew.Length() = 0 Then Return True
		Return False
	End
	
	Method SubtractBolts:Void(id:int)
		For Local temp:Int = 0 Until bolts.Length()
			If bolts.Get(temp).id = id
				TakeDamage(bolts.Get(temp))
				bolts.Remove(temp)
				temp -= 1
			End
		Next
	End
	
	Method AiStart:Void()
		aiControlled = True
		scrapElectronics = 0
		enginePower = OVERLOAD
		reactorPower = OVERLOAD
		shieldPower = LOW
		gun_ai_on = True
		combatAI = New AIPicker()
		SetCrewAi()
		'//assign jobs to crew
	End
	
	Method UpdateAI:Void()
	
		
		
		If crew.Length() = 0 or targetShip.isDead() Or isDead()
			aiControlled = False
			enginePower = 0
			shieldPower = 0
			reactorPower = 2
			shield_on = False
			For Local temp:Int = 0 Until comp.Length()
				If comp.Get(temp).getType() = Component.HARDPOINT
					comp.Get(temp).setState(False)
				End
			Next
			Return
		End
		combatAI.Reset(heatRelease)
		For Local temp:Int = 0 Until comp.Length()
			If comp.Get(temp).getType() = Component.ENGINE
				combatAI.ProcessEngine(Engine(comp.Get(temp).getSelf()))
			End
			If comp.Get(temp).getType() = Component.REACTOR
				combatAI.ProcessReactor(Reactor(comp.Get(temp).getSelf()))
			End
			If comp.Get(temp).getType() = Component.SHIELD
				combatAI.ProcessShield(Shield(comp.Get(temp).getSelf()))
			End
			If comp.Get(temp).getType() = Component.HEALTH
				combatAI.ProcessHealth(HealingTube(comp.Get(temp).getSelf()))
			End
			If comp.Get(temp).getType() = Component.HARDPOINT
				combatAI.ProcessHardPoint(Hardpoint(comp.Get(temp).getSelf()))
			End
			If comp.Get(temp).getType() = Component.BATTERY
				combatAI.ProcessBattery(Battery(comp.Get(temp).getSelf()))
			End
			If comp.Get(temp).getType() = Component.COOLANT_TANK
				combatAI.ProcessCoolant(CoolantTank(comp.Get(temp).getSelf()))
			End
		Next
		If mirrored
			combatAI.ProcessValue(False)
		Else
			combatAI.ProcessValue(True)
		End
		surrenderTimer = combatAI.ProcessSurrender(surrenderTimer)
		If surrenderTimer > SURRENDER_LIMIT Then hasSurrendered = canSurrender
		If surrenderTimer > SURRENDER_LIMIT * 8
			For Local temp:Int = 0 Until crew.Length()
				crew.Get(temp).TakeDamage(10)
			Next
		End
		If Not mirrored And KeyHit(KEY_L)
			combatAI.PrintAll()
		End
		enginePower = combatAI.ep
		reactorPower = combatAI.rp
		shieldPower = combatAI.sp
		shield_on = AIChoice.run_shields
	
		
		For Local temp:Int = 0 Until comp.Length()
			#rem
			If comp.Get(temp).getPermDamage() >= 30 And scrapElectronics > 1
				scrapElectronics -= 1
				comp.Get(temp).useRefinedScrap()
			End
			#end
			'	Print "start combat target"
			If comp.Get(temp).getType() = Component.HARDPOINT
				Local h:Hardpoint = Hardpoint(comp.Get(temp).getSelf())
				If h.isReady()
					Repeat
						Local randx:Int = Rnd(0, Ship.MAX_X)
						Local randy:int = Rnd(0, Ship.MAX_Y)
						Local tt:Int = 10
						While (tt > 0)
							Local num:Int = Rnd(targetShip.comp.Length())
							If targetShip.comp.Get(num).isDamaged() = False
								tt = 0
								randx = targetShip.comp.Get(num).getX()
								randy = targetShip.comp.Get(num).getY()
							End
							tt -= 1
						Wend
						If targetShip.pipes[randx * Ship.MAX_X + randy].isDamageable()
							If Rnd(h.gun.boltData.GetInt("accuracy", 100)) > targetShip.GetDodge(True) - GetAccuracyBonus(h.x, h.y)
								If quick_sim
									targetShip.TakeDamage(New Bolt(randx, randy, h.gun.boltData, targetShip.areShieldsActive()))
								Else
									targetShip.AddBolts(New Bolt(randx, randy, h.gun.boltData, targetShip.areShieldsActive()))
									Local box:BoxShipData = New BoxShipData(randx * 30, randy * 30, targetShip.x_offset, targetShip.y_offset, targetShip.mirrored, Bolt.idGlobal)
									Game.OverParticle.Add( ( (h.getX() +2) * 30) + 15, (h.getY() * 30) + 15, x_offset, y_offset, mirrored, PhotonTypes.GetType(h.gun.boltData.GetString("type")), box)
									Select h.FireGun()
										Case Weapon.MISSILE
											items.RemoveItem(Item.MISSILE, 1)
										Case Weapon.CANNON
											items.RemoveItem(Item.CANNON_BALL, 1)
										Case Weapon.THERMITE
											items.RemoveItem(Item.THERMITE, 1)
									End
									Select h.gun.boltData.GetString("type")
										Case "MISSILE"
											SoundEffect.Play(SoundEffect.MISSILE)
										Case "BEAM"
											SoundEffect.Play(SoundEffect.BEAM)
										Case "CANNON"
											SoundEffect.Play(SoundEffect.CANNON)
										Case "THERMITE"
											SoundEffect.Play(SoundEffect.CANNON)
										Default
											SoundEffect.Play(SoundEffect.LASER)
									End
								End
								Return
							Else
								If quick_sim Then Return
								Local box:BoxShipData = New BoxShipData(randx * 30, randy * 30, targetShip.x_offset, targetShip.y_offset, targetShip.mirrored)
								Game.OverParticle.Add( ( (h.getX() +2) * 30) + 15, (h.getY() * 30) + 15, x_offset, y_offset, mirrored, PhotonTypes.GetType(h.gun.boltData.GetString("type")), box)
								Select h.gun.boltData.GetString("type")
									Case "MISSILE"
										SoundEffect.Play(SoundEffect.MISSILE)
									Case "BEAM"
										SoundEffect.Play(SoundEffect.BEAM)
									Case "CANNON"
										SoundEffect.Play(SoundEffect.CANNON)
									Case "THERMITE"
										SoundEffect.Play(SoundEffect.CANNON)
									Default
										SoundEffect.Play(SoundEffect.LASER)
								End
								Select h.FireGun()
									Case Weapon.MISSILE
										items.RemoveItem(Item.MISSILE, 1)
									Case Weapon.CANNON
										items.RemoveItem(Item.CANNON_BALL, 1)
									Case Weapon.THERMITE
										items.RemoveItem(Item.THERMITE, 1)
								End
								Return
							End
						End
						
					Forever
				End
			End
			'	Print "exit combat target"
		Next
	End
	
	Method GetDodge:Int(level_pilot:Bool = False, thrust_overide:Float = 0.0)
		If targetShip = Null Then Return 100
		If thrust = 0 Then Return 0
		Local engine_bonus:Int
		If thrust_overide = 0.0
			engine_bonus = Min( (GetShieldMod() / (targetShip.GetShieldMod() * alpha) * 16), (50.0 / alpha))
		Else
			engine_bonus = Min( (GetShieldMod() / thrust_overide) * 16, 50.0)
		End
		Local ship_piloted:bool
		
		For Local temp:Int = 0 Until crew.Length()
			If crew.Get(temp).workCode = AICode.WORK_CODE_PILOT
				If level_pilot
					engine_bonus += crew.Get(temp).skills.UseSkill(SkillManager.SKILL_PILOT, 1)
				Else
					engine_bonus += crew.Get(temp).skills.UseSkill(SkillManager.SKILL_PILOT, 0)
				End
				ship_piloted = True
			End
		End
		
		
		If ship_piloted = False
			engine_bonus *= NO_PILOT_PENALTY
		End
		If engine_bonus > 100 Then Return 100
		Return engine_bonus
	End
	
	Method GetAccuracyBonus:Int(x:Int, y:int)
		Local count:Int
		Local total:Int
		For Local temp:Int = 0 Until crew.Length()
			If crew.Get(temp).workCode = AICode.WORK_CODE_WEAPONS
				If x = crew.Get(temp).x And y = crew.Get(temp).y
					Return crew.Get(temp).skills.UseSkill(SkillManager.SKILL_GUNNER, 1) * GUNNER_BONUS
				End
				count += 1
				total += crew.Get(temp).skills.UseSkill(SkillManager.SKILL_GUNNER, 0)
			End
		End
		If count = 0 Then Return 0
		Return total / count
	End
	Method GetShieldMod:Float()
		If areShieldsActive()
			Return thrust * SHIELD_SPEED_PENALTY
		End
		Return thrust
	End
	
	Method TestNeeds:Void()
		#rem
		needs.Reset()
		'//hull damage
		For Local temp:Int = 0 Until pipes.Length()
		If pipes[temp].health < maxHeath
		needs.minorDamage = True
		If pipes[temp].health < 100
		If pipes[temp].oxygen or pipes[temp].power or pipes[temp].coolant
		needs.majorDamage = True
		End
		End
		If needs.minorDamage And needs.majorDamage Then Return
		End
		Next
		If needs.majorDamage = True Then Return
		For Local temp:Int = 0 Until comp.Length()
		If comp.Get(temp).getHealth() < 100 Then needs.majorDamage = True
		Return
		Next
		#end
	End
	
	Method TakeDamage:Void(b:Bolt)
		If RoomExists(b.x, b.y) = False Then Return
		If pipes[b.x * MAX_X + b.y].isImpactPoint() = False Then Return
		Local power:Int = b.shield_damage
		If b.targetShielded And Not b.ignore_shield
			Local lastPower:Int
			Repeat
				lastPower = power
				For Local temp:Int = 0 Until comp.Length()
					If comp.Get(temp).getType() = Component.SHIELD And (comp.Get(temp).isActive() Or BROKEN_SHIELDS_FUNCTION)
						Local s:Shield = Shield(comp.Get(temp).getSelf())
						If s.power > 0
							power -= 1
							s.power -= 1
							If power <= 0
								Game.OverParticle.Add( (b.x * GRID_SIZE) + (GRID_SIZE / 2), (b.y * GRID_SIZE), x_offset, y_offset, mirrored, PhotonTypes.Text, BoxString("#00ffff" + b.shield_damage))
								If not quick_sim Then SoundEffect.Play(SoundEffect.SHIELD_IMPACT)
								shield_overlay.NewImpact(b.x, b.y)
								Return
							End
						End
					End
				Next
			Until (lastPower = power)
		End
		If not quick_sim Then SoundEffect.Play(SoundEffect.ENEMY_IMPACT)
		Local count:Int = b.fragments
		Local rad:Int = 0
		Repeat
			Local testX:Int = Rnd(-rad, rad) + b.x
			Local testY:Int = Rnd(-rad, rad) + b.y
			
			If RoomExists(testX, testY)
				Print "health: " + HP
				If pipes[testX * MAX_X + testY].isDamageable()
					HP -= b.hull_damage
					count -= 1
					If HP < maxHP Or b.ignore_armor
						If b.effect = "FIRE"
							pipes[testX * MAX_X + testY].fire = FIRE_HEALTH
							AddFirefightingTiles(testX, testY)
						End
						If b.hull_damage > 0
							Game.OverParticle.Add( (testX * GRID_SIZE) + (GRID_SIZE / 2), (testY * GRID_SIZE), x_offset, y_offset, mirrored, PhotonTypes.Text, BoxString("#ff0000" + b.hull_damage))
						End
						If (Rnd(1) > (HP / float(maxHP)))
							pipes[testX * MAX_X + testY].fire = FIRE_HEALTH
							AddFirefightingTiles(testX, testY)
						End
						If pipes[testX * MAX_X + testY].coolant or pipes[testX * MAX_X + testY].power
							If pipes[testX * MAX_X + testY].health < 100
								taskmanager.Add(testX, testY, Task.MAJOR_REPAIR)
							End
						Else
							If pipes[testX * MAX_X + testY].health < 100
								taskmanager.Add(testX, testY, Task.MINOR_REPAIR)
							End
						End
						For Local temp:Int = 0 Until comp.Length()
							If comp.Get(temp).getX() = testX And comp.Get(temp).getY() = testY
								comp.Get(temp).takeDamage(b.hull_damage)
								If comp.Get(temp).getHealth() < 100
									taskmanager.Add(comp.Get(temp).getX(), comp.Get(temp).getY(), Task.SYSTEM_REPAIR)
								End
							End
						Next
						If HP <= 0 And explosion <= 0
							explosion = 1
							For Local temp:Int = 0 Until comp.Length()
								comp.Get(temp).setState(False)
							Next
						End
						For Local x:Int = 0 Until crew.Length()
							If HP / float(maxHP) < 0.2
								crew.Get(x).TakeDamage(Rnd(20))
							End
							If HP <= 0
								crew.Get(x).dead = True
							End
							If crew.Get(x).x = testX And crew.Get(x).y = testY
								crew.Get(x).TakeDamage(b.crew_damage, b.effect) 'normal dagame
							End
						Next
					End
				End
			End
			If rad < 3 Then rad += 1
		Until count <= 0
		Print "exited damage loop"
	End

	Method AddComponent:Bool(x:Int, y:Int, name:String, damage:int = 0, level:Int = 0)
	
		For Local count:Int = 0 Until comp.Length()
			If comp.Get(count).getX() = x And comp.Get(count).getY() = y
				Return False
			End
		Next
		If ItemLoader.DoesItemExist(name)
			Local temp:JsonObject = ItemLoader.GetItem(name)
			Select ItemLoader.GetItemType(temp.GetString("type"))
				Case Component.REACTOR
					comp.Push(New Reactor(x, y, temp, x_offset, y_offset, mirrored, damage, level))
					Return True
				Case Component.BATTERY
					comp.Push(New Battery(x, y, temp, x_offset, y_offset, mirrored, damage, level))
					Return True
				Case Component.ENGINE
					comp.Push(New Engine(x, y, temp, x_offset, y_offset, mirrored, damage, level))
					Return True
				Case Component.HEALTH
					comp.Push(New HealingTube(x, y, temp, x_offset, y_offset, mirrored, damage, level))
					Return True
				Case Component.SHIELD
					comp.Push(New Shield(x, y, temp, x_offset, y_offset, mirrored, damage, level))
					Return True
				Case Component.COOLANT_TANK
					comp.Push(New CoolantTank(x, y, temp, x_offset, y_offset, mirrored, damage, level))
					Return True
				Case Component.COMPUTER
					comp.Push(New Computer(x, y, temp, x_offset, y_offset, mirrored, damage, level))
					Return True
				Case Component.HARDPOINT
					comp.Push(New Hardpoint(x, y, temp, x_offset, y_offset, mirrored, damage, level))
					Return True
				Case Component.SOLAR
					comp.Push(New Solar(x, y, temp, x_offset, y_offset, mirrored, damage, level))
					Return true
				Default
					Print name + " type not found"
					Return False
			End
		Else
			Return False
			Print name + " not found"
		End
	End
	Method AddComponent:Void(x:Int, y:Int, system:ComponentInterface)
	
		For Local count:Int = 0 Until comp.Length()
			If comp.Get(count).getX() = x And comp.Get(count).getY() = y
				Return
			End
		Next
		system.SetPosition(x, y, x_offset, y_offset)
		comp.Push(system)
	End
	
	Method ProcessMods:Void(type:Int, power:int)
		Select type
			Case Modcodes.EMP
			Print "EMP activated"
			If GetSystemEffect(Modcodes.EMP_SHIELD) = 0
				For Local temp:Int = 0 Until comp.Length()
					If comp.Get(temp).getType() = Component.BATTERY
						Local c:Battery = Battery(comp.Get(temp).getSelf())
						c.power = 0
					End
					If comp.Get(temp).getType() = Component.SHIELD
						Local c:Shield = Shield(comp.Get(temp).getSelf())
						c.power = 0
					End
					If comp.Get(temp).getType() = Component.HARDPOINT
						Local c:Hardpoint = Hardpoint(comp.Get(temp).getSelf())
						If c.gun <> Null
							c.gun.power = 0
						End
					End
				Next
			End
			If targetShip.GetSystemEffect(Modcodes.EMP_SHIELD) = 0
				For Local temp:Int = 0 Until targetShip.comp.Length()
					If targetShip.comp.Get(temp).getType() = Component.BATTERY
						Local c:Battery = Battery(targetShip.comp.Get(temp).getSelf())
						c.power = 0
					End
					If targetShip.comp.Get(temp).getType() = Component.SHIELD
						Local c:Shield = Shield(targetShip.comp.Get(temp).getSelf())
						c.power = 0
					End
					If targetShip.comp.Get(temp).getType() = Component.HARDPOINT
						Local c:Hardpoint = Hardpoint(targetShip.comp.Get(temp).getSelf())
						If c.gun <> Null
							c.gun.power = 0
						End
					End
				Next
			End
			
			'next
			Case Modcodes.AFTERBURNER
				thrust_bonus += power / float(100)
			Case Modcodes.STEALTH
				alpha = 0.5
			Case Modcodes.SHIELD_RECHARGE
				Print "Shield recharge activated"
				For Local temp:Int = 0 Until comp.Length()
					If comp.Get(temp).getType() = Component.SHIELD
						Local c:Shield = Shield(comp.Get(temp).getSelf())
						c.boostEffect += power
					End
				Next
			Case Modcodes.THERMAL_RELEASE
				Print "thermal release activated"
				For Local temp:Int = 0 Until comp.Length()
					If comp.Get(temp).getType() = Component.COOLANT_TANK
						Local c:CoolantTank = CoolantTank(comp.Get(temp).getSelf())
						c.permDamage += 5
						c.heatPacket = 0
					End
				Next
			Default
				Print "effect not found"
		End
	End
	
	Method Update:Void(force_update:bool = False, quick_sim:Bool = False)
		Self.quick_sim = quick_sim
		If explosion > 0
			explosion += 1
		End
		If Not force_update
		 	If particles <> Null
		 		particles.Update()
			End
			If areShieldsActive()
				If Not mirrored
					If audio_shields_on = False
						SoundEffect.Play(SoundEffect.SHIELD_ON)
						audio_shields_on = True
					End
				End
				shield_overlay.IncreaseAlpha()
			Else
				If audio_shields_on = True
					SoundEffect.Play(SoundEffect.SHIELD_OFF)
					audio_shields_on = False
				End
				shield_overlay.DecreaseAlpha()
			End
		End
		taskmanager.hull_is_damaged = (HP < maxHP)
		pathSaver = Max(0, pathSaver - 1)
		
		If Self.quick_sim
			If clock <> Null Then clock.tick(20)
			For Local xx:Int = 0 Until 20
				For Local temp:Int = 0 Until crew.Length()
					crew.Get(temp).Update()
					If crew.Get(temp).isDead()
						If Self = Player.ship
							Player.accounting.CrewDeath(crew.Get(temp))
						End
						crew.Remove(temp)
						temp -= 1
					End
				Next
			
			Next
		Else If not force_update
			If clock <> Null Then clock.tick(1)
			For Local temp:Int = 0 Until crew.Length()
				crew.Get(temp).Update()
				If crew.Get(temp).isDead()
					If Self = Player.ship
						Player.accounting.CrewDeath(crew.Get(temp))
					End
					crew.Remove(temp)
					temp -= 1
				End
			Next
			If pathcount > 0
				Print "total: " + pathcount
				pathcount = 0
			End
		End
		If alerts <> Null
			alerts.Update()
		End
		counter = (counter + 1) Mod UPDATERATE
		If counter <> 0 And Not (force_update Or quick_sim) Then Return
		fireCounter = (fireCounter + 1) Mod FIRE_UPDATE_RATE
		thrust_bonus = 1.0
		alpha = 1.0
		If aiControlled Then UpdateAI()
		For Local temp:Int = 0 Until modSpecial.Length()
			Local val:Int = modSpecial.Get(temp).Update()
			If val <> 0
				ProcessMods(val, modSpecial.Get(temp).power)
				modSpecial.Get(temp).Reset()
			End
		Next
	
		batteryDrain = False
		engineOverheating = False
		reactorOverheating = False
		shieldUnits = 0
		shieldMax = 0
		heatUnits = 0
		heatMax = 0
		heatGen = 0
		powerUnits = 0
		powerMax = 0
		batteryUnits = 0
		batteryMax = 0
		heatOutput = 0
		Local ammo:AmmoCounter = New AmmoCounter(items.CountItem(Item.MISSILE), items.CountItem(Item.CANNON_BALL), items.CountItem(Item.THERMITE))
		
		
		If canFireEngines() = False Then enginePower = 0
		Local spark_sound:bool
		For Local temp:Int = 0 Until comp.Length()
			If Not mirrored And comp.Get(temp).isDamaged() And explosion = 0
				Local r:float = Rnd(100)
				#rem
				If r < SMALL_SPARK_CHANCE
					Local xoff:Int = Rnd(-9, 9)
					Local yoff:Int = Rnd(-9, 9)
					For Local x:Int = 0 Until SMALL_SPARK_COUNT
						Game.OverParticle.Add(comp.Get(temp).getX() * GRID_SIZE + (GRID_SIZE / 2) + xoff, comp.Get(temp).getY() * GRID_SIZE + (GRID_SIZE / 2) + yoff, x_offset, y_offset, False, PhotonTypes.Spark)
					Next
				End
				#end
				If r < BIG_SPARK_CHANCE
					spark_sound = True
					Local xoff:Int = Rnd(-3, 3)
					Local yoff:Int = Rnd(-3, 3)
					For Local x:Int = 0 Until BIG_SPARK_COUNT
						particles.Add(comp.Get(temp).getX() * GRID_SIZE + (GRID_SIZE / 2) + xoff, comp.Get(temp).getY() * GRID_SIZE + (GRID_SIZE / 2) + yoff, 0, 0, False, PhotonTypes.Spark)
					Next
				End
			End
			
			If force_update
				comp.Get(temp).SaveState()
			End
			If comp.Get(temp).getType() = Component.REACTOR
				comp.Get(temp).Update(reactorPower, force_update Or quick_sim)
			Else If comp.Get(temp).getType() = Component.ENGINE
				If Engine(comp.Get(temp).getSelf()).extend > 0
					shield_overlay.CloseShields(comp.Get(temp).getX(), comp.Get(temp).getY())
				End
				comp.Get(temp).Update(enginePower, force_update Or quick_sim)
			Else If comp.Get(temp).getType() = Component.SHIELD
				comp.Get(temp).Update(shieldPower, force_update Or quick_sim)
			Else
				comp.Get(temp).Update(2, force_update)
			End
		Next
		If spark_sound Then SoundEffect.Play(SoundEffect.SPARK)
		If explosion > 0 Then Return
		RunPowerSim()
		For Local temp:Int = 0 Until comp.Length()
			If comp.Get(temp).getType() = Component.ENGINE And comp.Get(temp).isActive() = False
				comp.Get(temp).ResetHeatUseage()
			End
		Next
		If not force_update Then RunOxygenSim()
		RunHeatSim()
		
		
		'// after sim stuffs
		Local force:Int
		For Local temp:Int = 0 Until comp.Length()
			If comp.Get(temp).getType() = Component.COOLANT_TANK And comp.Get(temp).isActive() = True
				heatUnits += comp.Get(temp).getHeat()
				heatMax += CoolantTank(comp.Get(temp).getSelf()).capacity * CoolantTank(comp.Get(temp).getSelf()).GetEfficiency()
			End
			If comp.Get(temp).getType() = Component.ENGINE And comp.Get(temp).isActive() = True
				force += Engine(comp.Get(temp).getSelf()).currentThrust
			End
			If comp.Get(temp).getType() = Component.SHIELD And (comp.Get(temp).isActive() = True Or BROKEN_SHIELDS_FUNCTION)
				If Shield(comp.Get(temp).getSelf()).power < 0 Then Shield(comp.Get(temp).getSelf()).power = 0
				shieldUnits += Shield(comp.Get(temp).getSelf()).power
				shieldMax += Shield(comp.Get(temp).getSelf()).capacity * Shield(comp.Get(temp).getSelf()).GetEfficiency()
			End
			If comp.Get(temp).getType() = Component.BATTERY And comp.Get(temp).isActive() = True
				batteryUnits += Battery(comp.Get(temp).getSelf()).power
				batteryMax += Battery(comp.Get(temp).getSelf()).capacity * Battery(comp.Get(temp).getSelf()).GetEfficiency()
			End
			
			If comp.Get(temp).getType() = Component.HEALTH
				If comp.Get(temp).isActive()
					taskmanager.AddPerm(comp.Get(temp).getX(), comp.Get(temp).getY(), Task.HEAL_SELF)
				Else
					taskmanager.RemovePerm(comp.Get(temp).getX(), comp.Get(temp).getY())
				End
			End
			If comp.Get(temp).getType() = Component.COMPUTER
				If comp.Get(temp).isActive()
					taskmanager.AddPerm(comp.Get(temp).getX(), comp.Get(temp).getY(), Task.USE_COMPUTER)
				Else
					taskmanager.RemovePerm(comp.Get(temp).getX(), comp.Get(temp).getY())
				End
			End
			If comp.Get(temp).getType() = Component.HARDPOINT
				Local r:Hardpoint = Hardpoint(comp.Get(temp).getSelf())
				If r.gun <> Null
					If r.isActive()
						taskmanager.AddPerm(comp.Get(temp).getX(), comp.Get(temp).getY(), Task.USE_GUNS)
					Else
						taskmanager.RemovePerm(comp.Get(temp).getX(), comp.Get(temp).getY())
					End
					If r.gun.AmmoTest(ammo)
						r.setState(False)
					End
				End
				
			End
			If force_update
				comp.Get(temp).LoadState()
			End
		Next
		For Local temp:Int = 0 Until crew.Length()
			crew.Get(temp).updateFlag = True
		Next
		If alerts <> Null And Not force_update
			If batteryMax = 0
				alerts.Monitor(AlarmManager.BatteryLevel, 100)
			Else
				alerts.Monitor(AlarmManager.BatteryLevel, 100 - (batteryUnits / float(batteryMax) * 100))
			End
			If insuficientPower
				alerts.Monitor(AlarmManager.SystemOffline, 50)
			Else
				alerts.Monitor(AlarmManager.SystemOffline, 0)
			End
		End
		
		If heatMax = 0
			heatUnits = 1
			heatMax = 1
			If alerts <> Null And Not force_update
				alerts.Monitor(AlarmManager.HeatLevel, 100)
			End
			
		Else
			If alerts <> Null And Not force_update
				alerts.Monitor(AlarmManager.HeatLevel, (heatUnits / float(heatMax) * 100))
			End
			
		End
		thrust = (force * thrust_bonus) / float(GetMass())
	End
	
	Method MirrorOffset:Int()
		If mirrored Then Return MAX_X * GRID_SIZE
		Return 0
	End
	
	Method areShieldsActive:Bool()
		If shield_on Then Return canActivateShields()
		Return False
	End
	
	Method canActivateShields:Bool()
		If alpha < 1.0 Then Return False
		If shieldUnits / float(shieldMax) > MIN_SHIELD_PERCENT Then Return True
		Return False
	End
	
	Method Render:Void(view:Int = 0, isPaused:bool)
		Local xMin:Int = 100
		Local xMax:Int = -100
		Local yMin:Int = 100
		Local yMax:Int = -100
		If Not isPaused
			shield_overlay.Update()
		End
		PushMatrix()
		Translate(x_offset + MirrorOffset(), y_offset)
		SetAlpha(alpha)
		If mirrored Then Transform(-1, 0, 0, 1, 0, 0)
		For Local x:Int = 0 Until MAX_X
			For Local y:Int = 0 Until MAX_Y
				If x < xMin And pipes[x * MAX_X + y].isVacuum() = False Then xMin = x
				If x > xMax And pipes[x * MAX_X + y].isVacuum() = False Then xMax = x
				If y < yMin And pipes[x * MAX_X + y].isVacuum() = False Then yMin = y
				If y > xMax And pipes[x * MAX_X + y].isVacuum() = False Then yMax = y
				If pipes[x * MAX_X + y].door
					If RoomExists(x, y + 1) And RoomExists(x, y - 1)
						If pipes[x * MAX_X + y - 1].hull And pipes[x * MAX_X + y + 1].hull
							pipes[x * MAX_X + y].Render(x, y, view, False, explosion)
						Else
							pipes[x * MAX_X + y].Render(x, y, view, True, explosion)
						End
					End
				Else
					pipes[x * MAX_X + y].Render(x, y, view, False, explosion)
				End
			Next
		Next
		'	End
		
		For Local temp:Int = 0 Until comp.Length()
			comp.Get(temp).SetComponentAlpha(view)
			If view <> Tile.HULL_VIEW
				comp.Get(temp).Render(view, isPaused, explosion)
			End
		Next
		ResetColorAlpha()
		'//draw your crew
		If particles <> Null
			particles.Render()
		End
		If mirrored And targetShip <> Null
			If targetShip.GetSystemEffect(Modcodes.BIO_SCAN) <> 0
				For Local temp:Int = 0 Until crew.Length()
					If crew.Get(temp).hasBoarded = False Then crew.Get(temp).Render()
				Next
			End
		Else
			For Local temp:Int = 0 Until crew.Length()
				If crew.Get(temp).hasBoarded = False Then crew.Get(temp).Render()
			Next
		End
		
		'/draw guests
		If targetShip <> Null
			For Local temp:Int = 0 Until targetShip.crew.Length()
				If targetShip.crew.Get(temp).hasBoarded = True Then targetShip.crew.Get(temp).Render()
			Next
		End
		shield_overlay.Render(isPaused)
		td += 1
		If alerts <> Null
			alerts.Render()
		End
		PopMatrix()
	End
	
	Method GetRepairCost:Int(area:Economy)
		Local v:Int = 0
		For Local temp:Int = 0 Until comp.Length()
			If ItemLoader.DoesItemExist(comp.Get(temp).getId())
				Local i:Item = New Item(ItemLoader.GetItem(comp.Get(temp).getId()), comp.Get(temp).getPermDamage(), comp.Get(temp).getQualityLevel())
				v += i.GetRepairCost(area)
			End
		Next
		Return v
	End
	
	Method RepairAll:Void()
		For Local temp:Int = 0 Until comp.Length()
			comp.Get(temp).setPermDamage(0)
		'	comp.Get(temp).
		Next
	End
	
	Method GetMass:int(grabItem:Item = Null, dryOnly:bool = False, mass_adjust:int = 0, ignore_parts:Bool = False)
		Local mass:int = mass_adjust
		For Local temp:Int = 0 Until pipes.Length()
			If pipes[temp].power Then mass += 100
			If pipes[temp].coolant Then mass += 100
			If pipes[temp].floor Then mass += 300
			If pipes[temp].hull Then mass += 250
			If pipes[temp].door Then mass += 200
			If pipes[temp].isFlag(FlagCode.BED) Then mass += 600
			If pipes[temp].scaffolding Then mass += 50
		Next
		
		mass *= (1 + (float(Self.mass) / 100))
		If not ignore_parts
			For Local temp:Int = 0 Until comp.Length()
				mass += comp.Get(temp).getMass()
				If comp.Get(temp).getType() = Component.HARDPOINT
					Local h:Hardpoint = Hardpoint(comp.Get(temp).getSelf())
					If h.gun <> Null
						mass += h.gun.mass
					End
				End
				If comp.Get(temp).getType() = Component.COMPUTER
					Local c:Computer = Computer(comp.Get(temp).getSelf())
					If c.system <> Null
						mass += c.system.mass
					End
				End
			Next
			For Local temp:Int = 0 Until systems.Length()
				If systems[temp] <> Null
					mass += systems[temp].mass
				End
			Next
		End
		If dryOnly Then Return mass
		mass += items.GetMass()
		If grabItem <> Null
			mass += grabItem.GetMass()
		End
		Return mass
	End
	
	Method CalculateHP:Void()
		HP = 0
		For Local temp:Int = 0 Until pipes.Length()
			If pipes[temp].floor Then HP += HEALTH_BASE
		Next
		maxHP = HP
		AP = maxAP
		HP *= 1 + (float(AP + GetSystemEffect(Modcodes.ARMOR)) / 100)
	End
	Method HealShip:Void(full_heal:Bool = False)
		If full_heal
			AP = maxAP
		Else
			If HP < maxHP
				Local percent:int = 100 - (HP / float(maxHP) * 100)
				AP = Max(0, AP - (percent / DAMAGE_AP))
			End
		End
		HP = maxHP * (1 + (float(AP + GetSystemEffect(Modcodes.ARMOR)) / 100))
	End
	Method BurnFuel:Int()
		Local d:Int = MOVEMENT_COST
		If GetThrust() <= 0 Return - 1
		If GetFuelUseage() <= 0 Return - 1
		Local counter:Int
		While (d > 0)
			Local power:Int = Int(GetThrust() * 100)
			d -= power
			fuel -= GetFuelUseage()
			If fuel <= 0
				If items.hasItem(Item.FUEL)
					items.RemoveItem(Item.FUEL, 1)
					Player.accounting.UsedItem(Item.FUEL, 1)
					fuel += FUEL_SIZE
				Else
					fuel = 0
					Return -1
				End
			End
			counter += 1
		Wend
		Return counter
	End
	
	Method GetRange:Int()
		If GetThrust() <= 0 Return 0
		If GetFuelUseage() <= 0 Return 0
		Local range:Int = 0
		Local f:Int = fuel
		Local m:float = GetMass()
		Local t:int = int(GetThrust(True))
		Local use:Int = GetFuelUseage()
		Local travel:Int = MOVEMENT_COST
		If fuel_mass = 0
			Local i:Item = New Item(ItemLoader.GetItem("Fuel"))
			fuel_mass = i.GetMass()
			
		End
		Local fuel_rods:Int = 0
		fuel_rods += items.CountItem(Item.FUEL)

		
		While f > 0
			Local power:Int = Int( (t / m) * 100)
			f -= use
			If (f <= 0)
				If (fuel_rods <= 0)
					Return range
				End
				fuel_rods -= 1
				m -= fuel_mass
				f += FUEL_SIZE
			End
			travel -= power
			If (travel <= 0)
				range += 1
				travel = MOVEMENT_COST
			End
		Wend
		Return range
		
		
	End
	
	Method GetThrust:float(getRaw:Bool = False, getEditior:Bool = False)
		If getRaw = True
			Return thrust * float(GetMass())
		End
		If getEditior
			Local t:Float = 0
			For Local temp:Int = 0 Until comp.Length()
				If comp.Get(temp).getType() = Component.ENGINE
					t += Engine(comp.Get(temp).getSelf()).thrust * Engine(comp.Get(temp)).GetEfficiency()
				End
			Next
			Return t / GetMass()
		End
		Return thrust
	End
	
	Method GetFuelUseage:Int()
		Local useage:int
		For Local temp:Int = 0 Until comp.Length()
			If comp.Get(temp).getType() = Component.ENGINE
				useage += Engine(comp.Get(temp).getSelf()).fuelUse
			End
		Next
		If enginePower = LOW
			useage /= 3
		Else If enginePower = OVERLOAD
			useage *= 2
		End
		Return useage
	End
	
	#rem
	Method GetAiChoice:Void(ai:AIPicker)
		ai.Reset(heatRelease)
		For Local temp:Int = 0 Until comp.Length()
			If comp.Get(temp).getType() = Component.ENGINE
				ai.ProcessEngine(Engine(comp.Get(temp).getSelf()))
			End
			If comp.Get(temp).getType() = Component.REACTOR
				ai.ProcessReactor(Reactor(comp.Get(temp).getSelf()))
			End
			If comp.Get(temp).getType() = Component.SHIELD
				ai.ProcessShield(Shield(comp.Get(temp).getSelf()))
			End
			If comp.Get(temp).getType() = Component.HEALTH
				ai.ProcessHealth(HealingTube(comp.Get(temp).getSelf()))
			End
			If comp.Get(temp).getType() = Component.HARDPOINT
				ai.ProcessHardPoint(Hardpoint(comp.Get(temp).getSelf()))
			End
			If comp.Get(temp).getType() = Component.BATTERY
				ai.ProcessBattery(Battery(comp.Get(temp).getSelf()))
			End
			If comp.Get(temp).getType() = Component.COOLANT_TANK
				ai.ProcessCoolant(CoolantTank(comp.Get(temp).getSelf()))
			End
		Next
		ai.ProcessValue()
	End
	#end
	Method GetPowerUseage:Int(ep:Int, rp:Int, sp:int)
		Local power:int
		For Local temp:Int = 0 Until comp.Length()
			If comp.Get(temp).getType() = Component.ENGINE
				Local r:Engine = Engine(comp.Get(temp).getSelf())
				power -= (r.energyProduction * r.GetEfficiency(True,, False)) * GetMult(ep, 2.0)
			End
			If comp.Get(temp).getType() = Component.SHIELD
				Local r:Shield = Shield(comp.Get(temp).getSelf())
				power -= (r.energyProduction * r.GetEfficiency(True,, False)) * sp
			End
			If comp.Get(temp).getType() = Component.HEALTH
				Local r:HealingTube = HealingTube(comp.Get(temp).getSelf())
				power -= r.energyProduction
			End
			If comp.Get(temp).getType() = Component.HARDPOINT
				Local r:Hardpoint = Hardpoint(comp.Get(temp).getSelf())
				If r.gun <> Null
					power += r.gun.power_needed
				End
			End
		Next
		Return power
	End
	Method GetPowerProduction:Int(rp:int)
		Local power:int
		For Local temp:Int = 0 Until comp.Length()
			If comp.Get(temp).getType() = Component.REACTOR
				Local r:Reactor = Reactor(comp.Get(temp).getSelf())
				power += (r.energyProduction * r.GetEfficiency()) * GetMult(rp, 1.5)
			End
			If comp.Get(temp).getType() = Component.SOLAR
				Local r:Solar = Solar(comp.Get(temp).getSelf())
				power += (r.energyProduction * r.GetEfficiency())
			End
		Next
		Return power
	End
	Method GetBatteryProduction:Int()
		Local power:int
		For Local temp:Int = 0 Until comp.Length()
			If comp.Get(temp).getType() = Component.BATTERY
				Local r:Battery = Battery(comp.Get(temp).getSelf())
				power += (r.dischargeRate * r.GetEfficiency())
			End
		Next
		Return power
	End
	
	Method GetHeatProduction:Int(ep:Int, rp:int)
		Local heat:int
		For Local temp:Int = 0 Until comp.Length()
			If comp.Get(temp).getType() = Component.REACTOR
				Local r:Reactor = Reactor(comp.Get(temp).getSelf())
				heat += (r.heatProduction * r.GetEfficiency(True,, False)) * GetMult(rp, 2.0)
			End
			If comp.Get(temp).getType() = Component.ENGINE
				Local e:Engine = Engine(comp.Get(temp).getSelf())
				heat += (e.heatProduction * e.GetEfficiency(True,, False)) * GetMult(ep, 2.0)
			End
		Next
		Return heat
	End
	#rem old get heat code
	Method GetHeatUseage:float()
		Local heat:float
		For Local x:Int = 0 Until MAX_X
			For Local y:Int = 0 Until MAX_Y
				If pipes[x * MAX_X + y].hull
					heat += 0.4
					If RoomExists(x - 1, y)
						If pipes[ (x - 1) * MAX_X + y].isVacuum()
							heat += 0.5
						End
					End
					If RoomExists(x + 1, y)
						If pipes[ (x + 1) * MAX_X + y].isVacuum()
							heat += 0.5
						End
					End
					If RoomExists(x, y - 1)
						If pipes[x * MAX_X + y - 1].isVacuum()
							heat += 0.5
						End
					End
					If RoomExists(x, y + 1)
						If pipes[x * MAX_X + y + 1].isVacuum()
							heat += 0.5
						End
					End

				End
				If pipes[x * MAX_X + y].floor
					heat += 0.2
					Local Cflag:bool
					For Local temp:Int = 0 Until comp.Length()
						If comp.Get(temp).getX() = x And comp.Get(temp).getY() = y Then Cflag = True
					Next
					If Cflag = False Then heat += 0.4
				End
				If pipes[x * MAX_X + y].door
					heat += 0.3
				End

			Next
		Next
		
		Return heat
	End
	#end	
	Method RunOxygenSim:Void()
		Local power:Float = Clamp(HP / float(HP), 0.25, 1.0)
		#rem
		For Local temp:Int = 0 Until comp.Length()
			comp.Get(temp).setTested(False)
		Next
		#end
		
		ResetPipes()
		For Local temp:Int = 0 Until pipes.Length()
			If pipes[temp].isVacuum() Then pipes[temp].o2 = 0
			If pipes[temp].floor
				If dump_oxygen
					pipes[temp].o2 = Max(0.0, pipes[temp].o2 - OXYGEN_DUMP)
				Else
					pipes[temp].o2 += OXYGEN_PER_TILE * power
				End
			End
			If pipes[temp].health <= Tile.LEAK_THRESHOLD_LOW
				pipes[temp].o2 = Max(0.0, pipes[temp].o2 - Tile.AIR_LEAK)
			End
			If pipes[temp].health <= Tile.LEAK_THRESHOLD_MED
				pipes[temp].o2 = Max(0.0, pipes[temp].o2 - Tile.AIR_LEAK)
			End
			If pipes[temp].health <= Tile.LEAK_THRESHOLD_HIGH
				pipes[temp].o2 = Max(0.0, pipes[temp].o2 - Tile.AIR_LEAK)
			End
		Next
	
		'breathing will go here
		For Local x:Int = 0 Until crew.Length()
			crew.Get(x).RunOxygenSim()
		Next
		Local firecount:Int = 0
		taskmanager.RemoveAll(Task.FIRE)
		For Local tx:Int = 0 Until MAX_X
			For Local ty:Int = 0 Until MAX_Y
				If pipes[tx * MAX_X + ty].fire > 0
					pipes[tx * MAX_X + ty].fire -= GetSystemEffect(Modcodes.FIRE)
					firecount += 1
				End
				If pipes[tx * MAX_X + ty].fire > 0
					AddFirefightingTiles(tx, ty)
					If fireCounter = 0
						For Local temp:Int = 0 Until comp.Length()
							If comp.Get(temp).getX() = tx and comp.Get(temp).getY() = ty
								comp.Get(temp).takeDamage(20)
								For Local t:Int = 0 Until comp.Length()
									If comp.Get(t).getX() = tx And comp.Get(t).getY() = ty
										If comp.Get(t).getHealth() < 100
											taskmanager.Add(comp.Get(t).getX(), comp.Get(t).getY(), Task.SYSTEM_REPAIR)
										End
									End
								Next
							End
						Next
					End
					If pipes[tx * MAX_X + ty].o2 > 2
						pipes[tx * MAX_X + ty].o2 -= float(1.5) ' take this out and you will crash the compiler
						pipes[tx * MAX_X + ty].o2 = Max(0.0, pipes[tx * MAX_X + ty].o2 - FIRE_OXYGEN_BURN)
						Game.OverParticle.Add( (tx * GRID_SIZE) + Rnd(-5, 5) + GRID_SIZE / 2, (ty * GRID_SIZE) + Rnd(-5, 5) + GRID_SIZE / 2, x_offset, y_offset, mirrored, PhotonTypes.Fire)
					Else
						Game.OverParticle.Add( (tx * GRID_SIZE) + Rnd(-5, 5) + GRID_SIZE / 2, (ty * GRID_SIZE) + Rnd(-5, 5) + GRID_SIZE / 2, x_offset, y_offset, mirrored, PhotonTypes.Fire)
						pipes[tx * MAX_X + ty].fire = Max(pipes[tx * MAX_X + ty].fire - 3, 0)
					End
					If Rnd(100) < FIRE_SPREAD_CHANCE And pipes[tx * MAX_X + ty].o2 > 2
						Local r:Int = 10
						Local flag:Bool = False
						While (flag = False And r >= 0)
							Local randx:Int = Rnd(-2, 2)
							Local randy:int = Rnd(-2, 2)
							If pipes[ (tx + randx) * MAX_X + ty + randy].floor And pipes[ (tx + randx) * MAX_X + ty + randy].fire = 0
								pipes[ (tx + randx) * MAX_X + ty + randy].fire = FIRE_HEALTH
								flag = True
							End
							r -= 1
						Wend
					End
				End
			Next
		Next

		If alerts <> Null
			alerts.Monitor(AlarmManager.FireAlarm, firecount)
		End
		'// oxygen sim stuff
		ResetPipes(True)
		For Local x:Int = 0 Until MAX_X
			For Local y:Int = 0 Until MAX_Y
				If pipes[x * MAX_X + y].floor And pipes[x * MAX_X + y].tested = False And pipes[x * MAX_X + y].tested_final = False
					FloodPipes(x, y, Tile.AIR)
					Local s:Stack<Int> = New Stack<Int>
					Local e:Stack<int> = New Stack<int>
					Local total:float
					Local hull_open:bool
					For Local xx:Int = 0 Until MAX_X
						For Local yy:Int = 0 Until MAX_Y
							If pipes[xx * MAX_X + yy].tested = True
								If pipes[xx * MAX_X + yy].isLink(xx, yy)
									targetShip.ResetPipes(True)
									For Local tx:Int = 0 Until Ship.MAX_X
										For Local ty:Int = 0 Until Ship.MAX_Y
											If targetShip.pipes[tx * Ship.MAX_X + ty].isLink(tx, ty)
												targetShip.FloodPipes(tx, ty, Tile.AIR)
											End
										Next
									Next
									For Local xxx:Int = 0 Until MAX_X
										For Local yyy:Int = 0 Until MAX_Y
											If targetShip.pipes[xxx * MAX_X + yyy].tested = True
												If targetShip.pipes[xxx * MAX_X + yyy].isVacuum()
													hull_open = True
												End
												e.Push(xxx * MAX_X + yyy)
												total += targetShip.pipes[xxx * MAX_X + yyy].o2
											End
										Next
									Next
								End
								If pipes[xx * MAX_X + yy].isVacuum() Then hull_open = True
								pipes[xx * MAX_X + yy].tested_final = True
								s.Push(xx * MAX_X + yy)
								total += pipes[xx * MAX_X + yy].o2
							End
						Next
					Next
					ResetPipes()
					For Local temp:int = 0 Until s.Length()
						If hull_open = False
							pipes[s.Get(temp)].o2 = Clamp(total / (s.Length() +e.Length()), 0.0, Tile.MAX_OXYGEN)
						Else
							pipes[s.Get(temp)].o2 = 0
						End
					Next
					For Local temp:int = 0 Until e.Length()
						If hull_open = False
							targetShip.pipes[e.Get(temp)].o2 = Clamp(total / (s.Length() +e.Length()), 0.0, Tile.MAX_OXYGEN)
						Else
							targetShip.pipes[e.Get(temp)].o2 = 0
						End
					Next
				End
				
				
			Next
		Next
		
		
		
		
		
	End
	
	Method AddFirefightingTiles:Void(tx:Int, ty:Int)
		'taskmanager.Add(tx, ty, Task.FIRE)
		For Local x:Int = -1 To 1
			For Local y:Int = -1 To 1
				If (RoomExists(tx + x, ty + y)) And pipes[ (tx + x) * MAX_X + ty + y].floor
					taskmanager.Add(tx + x, ty + y, Task.FIRE)
				End
			Next
		Next
		#rem
		If (RoomExists(tx + 1, ty))
			If pipes[ (tx + 1) * MAX_X + ty].floor And pipes[ (tx + 1) * MAX_X + ty].fire = 0
				taskmanager.Add(tx + 1, ty, Task.FIRE)
			End
		End
		If (RoomExists(tx + 1, ty + 1))
			If pipes[ (tx + 1) * MAX_X + ty].floor And pipes[ (tx + 1) * MAX_X + ty].fire = 0
				taskmanager.Add(tx + 1, ty, Task.FIRE)
			End
		End
		If (RoomExists(tx - 1, ty))
			If pipes[ (tx - 1) * MAX_X + ty].floor And pipes[ (tx - 1) * MAX_X + ty].fire = 0
				taskmanager.Add(tx - 1, ty, Task.FIRE)
			End
		End
		If (RoomExists(tx, ty + 1))
			If pipes[tx * MAX_X + ty + 1].floor And pipes[tx * MAX_X + ty + 1].fire = 0
				taskmanager.Add(tx, ty + 1, Task.FIRE)
			End
		End
		If (RoomExists(tx, ty - 1))
			If pipes[tx * MAX_X + ty - 1].floor And pipes[tx * MAX_X + ty - 1].fire = 0
				taskmanager.Add(tx, ty - 1, Task.FIRE)
			End
		End
		#end
	End
	
	Method RunPowerSim:Void()
		For Local temp:Int = 0 Until comp.Length()
			comp.Get(temp).setTested(False)
		Next
		insuficientPower = False
		ResetPipes()
		Local reactor_flag:bool
		Repeat
			For Local x:Int = 0 Until comp.Length()
				If (comp.Get(x).getType() = Component.SOLAR or comp.Get(x).getType() = Component.REACTOR or comp.Get(x).getType() = Component.BATTERY And (comp.Get(x).getTested() = False And comp.Get(x).isActive()))
					If Not reactor_flag And comp.Get(x).getType() = Component.BATTERY Then Continue
					Local s:PacketStack<Packet> = New PacketStack<Packet>
					GetConnnectedComponents(s, comp.Get(x).getX(), comp.Get(x).getY(), Tile.POWER, 0)
					s.Sort()
					Local power:Int = 0
					
					'// normal pull
					Local last_length:Int
					Repeat
						last_length = s.Length()
						For Local temp:Int = 0 Until s.Length()
							If s.Get(temp).device.getType() = Component.REACTOR Or s.Get(temp).device.getType() = Component.SOLAR
								Local last_power:int = power
								power = s.Get(temp).device.useUnit(power, Component.POWER)
								powerMax += power - last_power
								s.Remove(temp)
								temp -= 1
							Else If s.Get(temp).device.getType() <> Component.BATTERY
								power = s.Get(temp).device.useUnit(power, Component.POWER)
								If s.Get(temp).device.getPower() = 0
									s.Remove(temp)
									temp -= 1
								End
							End
						Next
					Until (last_length = s.Length())
					powerUnits += power
					'/battery testing
					Local has_batteries:Bool
					Local needs_batteries:Bool
					Local used_batteries:bool
					For Local temp:Int = 0 Until s.Length()
						If s.Get(temp).device.getType() = Component.BATTERY
							has_batteries = True
						Else
							needs_batteries = True
							
						End
					Next
					
					
					'//use batteries
					Local last_power:Int
					Local increase:int
					If needs_batteries And Not has_batteries
						powerUnits = 0
						insuficientPower = True
					End
					If has_batteries = True And needs_batteries = True
						Repeat
							last_power = power
							For Local temp:Int = 0 Until s.Length()
								If s.Get(temp).device.getType() = Component.BATTERY And s.Get(temp).device.getPower() <> 0
									powerUnits += power
									increase -= power
									power = s.Get(temp).device.useUnit(power, Component.USEBATTERY)
									powerUnits -= power
									increase += power
									used_batteries = True
								Else If s.Get(temp).device.getType() <> Component.BATTERY
									powerUnits -= power
									power = s.Get(temp).device.useUnit(power, Component.POWER)
									powerUnits += power
									If s.Get(temp).device.getPower() = 0
										s.Remove(temp)
										temp -= 1
									End
								End
							Next
						Until (last_power = power)
						powerUnits = powerUnits + increase
					End
					'//charge batteries
					If has_batteries
						Repeat
							last_power = power
							For Local temp:Int = 0 Until s.Length()
								If s.Get(temp).device.getType() = Component.BATTERY
									If used_batteries
										power = s.Get(temp).device.useUnit(power, Component.RESTOREBATTERY)
									Else
										power = s.Get(temp).device.useUnit(power, Component.POWER)
									End
								Else
									insuficientPower = True
								End
							Next
						
						Until last_power = power
					End
					
					
					s.Clear()
				End
			Next
			Local battery_flag:Bool
			For Local x:Int = 0 Until comp.Length()
				If comp.Get(x).getTested() = False
					If comp.Get(x).getType() = Component.BATTERY And comp.Get(x).isActive() and LineExists(comp.Get(x).getX(), comp.Get(x).getY(), Tile.POWER)
						battery_flag = True
						reactor_flag = True
					End
				End
			Next
			If battery_flag

			Else
				Return
			End
		Forever
	End
	

	Method RunHeatSim:Void()
		Local radPool:int = heatRelease + GetSystemEffect(Modcodes.HEAT_SINK)
		Local radcount:int
		For Local temp:Int = 0 Until comp.Length()
			comp.Get(temp).setTested(False)
			If comp.Get(temp).getType() = Component.COOLANT_TANK And comp.Get(temp).isActive()
				radcount += 1
			End
		Next
		If radcount = 0 Then radPool = 0 Else radPool /= radcount
		ResetPipes()
		For Local x:Int = 0 Until comp.Length()
			If comp.Get(x).getType() = Component.COOLANT_TANK and comp.Get(x).getTested() = False
				Local s:PacketStack<Packet> = New PacketStack<Packet>
				GetConnnectedComponents(s, comp.Get(x).getX(), comp.Get(x).getY(), Tile.COOLANT, 0)
				s.Sort()
				Local power:Int = 0
				Local power_limit:Int = 0
				Local useage:Int = 0
				'// normal pull
				Local last_power:Int
				Repeat
					last_power = power
					For Local temp:Int = 0 Until s.Length()
						If s.Get(temp).device.getType() = Component.COOLANT_TANK
							Local add:Int = s.Get(temp).device.useUnit(power, Component.GETHEATCAP)
							If dumpHeat And add <> - 1 Then power += radPool
							If add <> - 1
								power += add
								power_limit += add
								'	startHeat += add
							End
						Else If s.Get(temp).device.getType() <> Component.RADIATOR
							Local sub:Int = s.Get(temp).device.useUnit(power, Component.HEAT)
							useage += (power - sub)
							power = sub
							If s.Get(temp).device.getHeat() = 0
								s.Remove(temp)
								temp -= 1
							End
						End
					Next
				Until (last_power = power)
				'	Print "use: " + useage
				power = power_limit - power
				Local pStart:int = power
				'Print "power1: " + power
				Repeat
					last_power = power
					For Local temp:Int = 0 Until s.Length()
						If s.Get(temp).device.getType() = Component.COOLANT_TANK
							power = s.Get(temp).device.useUnit(power, Component.HEAT)
						End
					Next
				Until (last_power = power)
				'	Print "power2: " + power
				heatOutput += (power - pStart) + useage
				heatGen += useage
				
				s.Clear()
			End
		Next
	End
	
	Method ResetPipes:Void(hard_reset:Bool = False)
		For Local temp:int = 0 Until pipes.Length()
			pipes[temp].tested = False
			If hard_reset pipes[temp].tested_final = False
		Next
	End
	
	Method RoomExists:bool(x:Int, y:Int)
		If x < 0 Then Return False
		If x >= MAX_X Then Return False
		If y < 0 Then Return False
		If y >= MAX_Y Then Return False
		Return True
	End
	
	Method canEVA:Bool()
		If thrust <= 0 And inCombat = False Then Return True
		Return False
	End
	
	Method canFireEngines:Bool()
		For Local temp:Int = 0 Until crew.Length()
			If isBoarding Then Return False
			If pipes[crew.Get(temp).x * Ship.MAX_X + crew.Get(temp).y].isVacuum() Then Return False
		Next
		Return True
	End
	
	Method GetConnnectedComponents:Void(s:PacketStack<Packet>, x:Int, y:Int, type:int, distance:int)
		If LineExists(x, y, type) = False Then Return
		If pipes[x * MAX_X + y].tested = True Then Return
		pipes[x * MAX_X + y].tested = True
		For Local temp:Int = 0 Until comp.Length()
			If comp.Get(temp).getX() = x And comp.Get(temp).getY() = y And comp.Get(temp).getTested() = False And comp.Get(temp).isDamaged() = False And (comp.Get(temp).isOnline() Or (comp.Get(temp).getType() = Component.HARDPOINT And type = Tile.COOLANT))
				Select type
					Case Tile.POWER
						If comp.Get(temp).isPowerCircuit()
							s.Push(New Packet(comp.Get(temp), distance))
							comp.Get(temp).setTested(True)
						End
					Case Tile.OXYGEN
						If comp.Get(temp).isOxygenCircuit()
							s.Push(New Packet(comp.Get(temp), distance))
							comp.Get(temp).setTested(True)
						End
					Case Tile.COOLANT
						If comp.Get(temp).isHeatCircuit()
							s.Push(New Packet(comp.Get(temp), distance))
							comp.Get(temp).setTested(True)
						End
				End
			End
		Next
		GetConnnectedComponents(s, x + 1, y, type, distance + 1)
		GetConnnectedComponents(s, x - 1, y, type, distance + 1)
		GetConnnectedComponents(s, x, y + 1, type, distance + 1)
		GetConnnectedComponents(s, x, y - 1, type, distance + 1)
	End
	
	Method FloodPipes:Void(x:Int, y:int, type:int)
		If LineExists(x, y, type) = False Then Return
		If pipes[x * MAX_X + y].tested = True Then Return
		If pipes[x * MAX_X + y].tested_final = True Then Return
		pipes[x * MAX_X + y].tested = True
		If type = Tile.AIR and pipes[x * MAX_X + y].isVacuum() Then Return
		FloodPipes(x, y + 1, type)
		FloodPipes(x, y - 1, type)
		If type = Tile.AIR And pipes[x * MAX_X + y].walkway Then Return
		FloodPipes(x + 1, y, type)
		FloodPipes(x - 1, y, type)
		

	End
	
	Method LineExists:Bool(x:Int, y:Int, type:Int)
		If x < 0 Return False
		If x >= MAX_X Return False
		If y < 0 Return False
		If y >= MAX_Y Return False
		If type = Tile.AIR Then Return pipes[x * MAX_X + y].isOpen()
		If pipes[x * MAX_X + y].health < Tile.BROKEN_LINE_THRESHOLD Then Return False
		If type = Tile.POWER Then Return pipes[x * MAX_X + y].power
		If type = Tile.COOLANT Then Return pipes[x * MAX_X + y].coolant
		Return False
	End
	
End

Class WarpCode
	Const READY_TO_WARP:Int = 0
	Const ENGINES_OFFLINE:Int = 1
	Const NO_FUEL:Int = 2
	Const LOW_POWER:Int = 3
	Const HIGH_HEAT:Int = 4
End

Class Slot
	Field x:Int
	Field y:Int
	Field type:Int
	Method New(x:Int, y:Int, type:Int)
		Self.x = x
		Self.y = y
		Self.type = type
	End
End

Function GetValueGrowth:Int(count:Int, min:Int, value:int)
	If count <= min
		Return value * count
	Else
		Return value * min + (value * Pow(2, count - min + 1))
	End
End

Function GetFuelMult:float(state:Int)
	If state = 3 Then Return 2.0
	If state = 1 Then Return 0.3333333
	Return 1.0
End

Function GetMult:Float(val:Int, max:float)
	If max = 1.5
		If val = 1 Then Return 0.5
		If val = 2 Then Return 1.0
		If val = 3 Then Return 1.5
	End
	If max = 2.0
		If val = 1 Then Return 0.5
		If val = 2 Then Return 1.0
		If val = 3 Then Return 2.0
	End
	Return 0.0
End

Class AmmoCounter
	Field missile:Int
	Field cannon:Int
	Field thermite:Int
	Method New(missile:Int, cannon:Int, thermite:Int)
		Self.missile = missile
		Self.cannon = cannon
		Self.thermite = thermite
	End
End
