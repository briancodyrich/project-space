Strict

Import space_game

Class FlagCode
	Const BED:int = 1000
	Const BED_N:Int = 1
	Const BED_S:Int = 2
	Const BED_E:Int = 3
	Const BED_W:Int = 4
End

Class Tile
	Const DOOR_NS:Int = 1
	Const DOOR_EW:Int = 2
	Const POWER:Int = 1
	Const COOLANT:Int = 2
	Const OXYGEN:Int = 3
	Const AIR:Int = 4
	
	Const NORMAL_VIEW:Int = 100
	Const ELECTRIC_VIEW:Int = 200
	Const OXYGEN_VIEW:Int = 300
	Const HEAT_VIEW:Int = 400
	Const HULL_VIEW:Int = 500
	Const DAMAGE_VIEW:Int = 600
	
	Const MAX_OXYGEN:Float = 12.0
	Const MIN_BREATHABLE:Float = 3.0
	Const LEAK_THRESHOLD_LOW:int = 75
	Const LEAK_THRESHOLD_MED:int = 50
	Const LEAK_THRESHOLD_HIGH:int = 25
	Const BROKEN_LINE_THRESHOLD:Int = 75
	Const AIR_LEAK:Float = 0.50
	
	Global img_hull:Image
	Global img_floor:Image
	Global img_floor_DAMAGED_1:Image
	Global img_floor_DAMAGED_2:Image
	Global img_floor_DAMAGED_3:Image
	Global img_door_NS:Image
	Global img_door_NS_OPEN:Image
	Global img_door_EW:Image
	Global img_door_EW_OPEN:Image
	Global img_air_lock_OPEN:Image
	Global img_air_lock_CLOSED:Image
	Global img_scaffolding:Image
	Global img_walkway:Image
	Global img_rock:Image
	Global img_bed_N:Image
	Global img_bed_S:Image
	Global img_bed_E:Image
	Global img_bed_W:Image
	Global img_shield:Image
	
	Field power:Bool
	Field coolant:Bool
	Field flag:bool
	Field floor:Bool
	Field hull:bool
	Field door:bool
	Field link:bool
	Field airlock:bool
	Field walkway:bool
	Field scaffolding:bool
	Field doorOpen:bool
	
	
	Field health:Int
	Field o2:float
	Field fire:int
	
	Field code:int
	
	Field expX:Float
	Field expY:Float
	Field expT:float
	
	'//game stuff
	Field tested:bool '// for floodfill
	Field tested_final:bool
	Field offset:float = 0
	
	
	
	Method New()
		health = 100
		expX = Rnd(-0.3, 0.3)
		expY = Rnd(-0.3, 0.3)
		expT = Rnd(-0.5, 0.5)
		o2 = MAX_OXYGEN
	End
	
	Method useOxygen:bool(num:Int)
		If (num = 0) Return True
		If (num < 0)
			o2 -= num
			Return True
		End
		If o2 >= MIN_BREATHABLE Then
			o2 -= num
			Return True
		End
		Return false
	End
	
	Method isFlag:Bool(type:Int)
		If type = FlagCode.BED And code >= 1 And code <= 4 Then Return True
		Return False
	End
	
	Method FightFire:Void(pow:Int)
		fire -= pow
		fire = Max(0, fire)
	End
	
	Method setOxygen:Void(oxygen:float)
		Self.o2 = oxygen
		If o2 > MAX_OXYGEN Then o2 = MAX_OXYGEN
	End
	
	Method takeDamage:Void(damage:Int)
		If isDamageable()
			health -= damage
			If health < 0 Then health = 0
		Else
			Print "something tried to damage a undamageable tile"
		End 
	End
	
	Method isOpen:Bool()
		If floor Then Return True
		If hull Then Return False
		If door
			If doorOpen Then Return True
			Return False
		End
		If airlock
			If doorOpen Then Return True
			Return False
		End
		Return True
	End
	
	Method isLink:Bool(x:Int, y:int)
		If walkway
			If x = 0 or y = 0 or x = (Ship.MAX_X - 1) or y = (Ship.MAX_Y - 1) Then Return True
		End
		Return false
	End
	
	Method isDamageable:Bool()
		If floor or scaffolding Then Return True
		Return false
	End
	
	Method isImpactPoint:Bool()
		If floor or hull or door or scaffolding or airlock Then Return True
		Return False
	End
	
	Function indexToX:Int(index:Int)
		Return index / Ship.MAX_X
	End
	Function indexToY:Int(index:int)
		Return index - (indexToX(index)*Ship.MAX_X)
	End
	
	Method isWalkable:Bool(can_eva:Bool)
		If floor Then Return True
		If door Then Return True
		If airlock Then Return True
		If walkway Then Return True
		If isVacuum() And can_eva Then Return True
		Return False
	End
	
	Method isVacuum:Bool()
		If Not floor And Not hull And Not door And Not airlock And Not walkway And Not scaffolding Then Return True
		Return false
	End
	
	Method Render:Void(x:int, y:int, view:int, isN:bool = False, explosion:int)

	
		Local xOff:Float = explosion * expX
		Local yOff:Float = explosion * expY
		
		If floor
			If health <= LEAK_THRESHOLD_HIGH
				DrawImage(img_floor_DAMAGED_3, (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2 + xOff, (y * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2) + yOff, explosion * expT, 1.0, 1.0)
			Else If health <= LEAK_THRESHOLD_MED
				DrawImage(img_floor_DAMAGED_2, (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2 + xOff, (y * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2) + yOff, explosion * expT, 1.0, 1.0)
			Else If health <= LEAK_THRESHOLD_LOW
				DrawImage(img_floor_DAMAGED_1, (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2 + xOff, (y * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2) + yOff, explosion * expT, 1.0, 1.0)
			Else
				DrawImage(img_floor, (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2 + xOff, (y * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2) + yOff, explosion * expT, 1.0, 1.0)
			End
		Else If hull
			DrawImage(img_hull, (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2 + xOff, (y * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2) + yOff, explosion * expT, 1.0, 1.0)
			
		Else If door
			If doorOpen
				If isN
					DrawImage(img_door_NS_OPEN, (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2 + xOff, (y * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2) + yOff, explosion * expT, 1.0, 1.0)
				Else
					DrawImage(img_door_EW_OPEN, (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2 + xOff, (y * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2) + yOff, explosion * expT, 1.0, 1.0)
				End
			Else
				If isN
					DrawImage(img_door_NS, (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2 + xOff, (y * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2) + yOff, explosion * expT, 1.0, 1.0)
				Else
					DrawImage(img_door_EW, (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2 + xOff, (y * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2) + yOff, explosion * expT, 1.0, 1.0)
				End
			End
		Else If scaffolding
			DrawImage(img_scaffolding, (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2 + xOff, (y * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2) + yOff, explosion * expT, 1.0, 1.0)
		Else If walkway
			DrawImage(img_walkway, (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2 + xOff, (y * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2) + yOff, explosion * expT, 1.0, 1.0)
		Else If airlock
			If doorOpen
				DrawImage(img_air_lock_OPEN, (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2 + xOff, (y * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2) + yOff, explosion * expT, 1.0, 1.0)
			Else
				DrawImage(img_air_lock_CLOSED, (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2 + xOff, (y * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2) + yOff, explosion * expT, 1.0, 1.0)
			End
		End
		If flag
			If code = FlagCode.BED_E
				DrawImage(img_bed_E, (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2 + xOff, (y * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2) + yOff, explosion * expT, 1.0, 1.0)
			Else If code = FlagCode.BED_W
				DrawImage(img_bed_W, (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2 + xOff, (y * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2) + yOff, explosion * expT, 1.0, 1.0)
			Else If code = FlagCode.BED_S
				DrawImage(img_bed_S, (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2 + xOff, (y * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2) + yOff, explosion * expT, 1.0, 1.0)
			Else If code = FlagCode.BED_N
				DrawImage(img_bed_N, (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2 + xOff, (y * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2) + yOff, explosion * expT, 1.0, 1.0)
			End
		End
		If o2 < 10 And Not isVacuum() And Not door And Not scaffolding And Not airlock And view = NORMAL_VIEW And explosion <= 0
			SetColor(255, 0, 0)
			SetAlpha(Clamp(1 - (o2 / 10), 0.0, 0.8))
			DrawRect( (x * Ship.GRID_SIZE), (y * Ship.GRID_SIZE), Ship.GRID_SIZE, Ship.GRID_SIZE)
			ResetColorAlpha()
		End
		
		If isLink(x, y)
			SetColor(0, 255, 0)
			DrawRect( (x * Ship.GRID_SIZE), (y * Ship.GRID_SIZE), Ship.GRID_SIZE, Ship.GRID_SIZE)
			ResetColorAlpha()
		End
		
		If view = Tile.ELECTRIC_VIEW And power = True
			If health >= BROKEN_LINE_THRESHOLD Then SetColor(255, 255, 0) Else SetColor(255, 0, 0)
			DrawRect( (x * 30), (y * 30), 30, 30)
			SetColor(255, 255, 255)
		End
		If view = Tile.HEAT_VIEW And coolant
			If health >= BROKEN_LINE_THRESHOLD Then SetColor(0, 0, 255) Else SetColor(255, 0, 0)
			DrawRect( (x * 30), (y * 30), 30, 30)
			SetColor(255, 255, 255)
		End
		
		If view = Tile.OXYGEN_VIEW
			If health >= BROKEN_LINE_THRESHOLD Then SetColor(0, 255, 0) Else SetColor(255, 0, 0)
			DrawRect( (x * 30), (y * 30), 30, 30)
			SetColor(255, 255, 255)
		End
		
	End
	
	Method isMouseOver:Bool(x:Int, y:Int, x_offset:Int, y_offset:int, mirrored:bool)
		If mirrored
			If Game.Cursor.x() > (x_offset + (Ship.MAX_X * Ship.GRID_SIZE) - (x * 30) - 30) And Game.Cursor.x() < (x_offset + (Ship.MAX_X * Ship.GRID_SIZE) - (x * 30)) And Game.Cursor.y() > (y_offset + (y * 30)) And Game.Cursor.y() < (y_offset + (y * 30) + 30) Then Return True
		Else
			If Game.Cursor.x() > (x_offset + (x * 30)) And Game.Cursor.x() < (x_offset + (x * 30) + 30) And Game.Cursor.y() > (y_offset + (y * 30)) And Game.Cursor.y() < (y_offset + (y * 30) + 30) Then Return True
		End
		Return False
	End
	Method isPointOver:Bool(x:Int, y:Int, x_offset:Int, y_offset:int, mirrored:bool, pointX:Int, pointY:int)
		If mirrored
			If pointX > (x_offset + (Ship.MAX_X * Ship.GRID_SIZE) - (x * 30) - 30) And pointX < (x_offset + (Ship.MAX_X * Ship.GRID_SIZE) - (x * 30)) And pointY > (y_offset + (y * 30)) And pointY < (y_offset + (y * 30) + 30) Then Return True
		Else
			If pointX > (x_offset + (x * 30)) And pointX < (x_offset + (x * 30) + 30) And pointY > (y_offset + (y * 30)) And pointY < (y_offset + (y * 30) + 30) Then Return True
		End
		Return False
	End
End

Class ShieldOverlay
	Const OVERLAY_MAX_X:Int = Ship.MAX_X + 2
	Const OVERLAY_MAX_Y:Int = Ship.MAX_Y + 2
	Const GRID_SIZE:Int = Ship.GRID_SIZE
	Const ALPHA_CHANGE:Float = 0.005
	Const ALPHA_DECAY:Float = 0.78
	Const MAX_ALPHA:Float = 0.3
	Const MIN_ALPHA:Float = 0.1
	Const MAX_RADIUS:Int = 15
	
	Global ImpactID:Int = 1
	
	Const STATE_CHANGE:Int = 30
	Const OFFSET_MULT:Int = 2
	Const RAD:Int = 10
	Field tiles:ShieldTile[]
	Field alpha:Float
	Field counter:Int
	Field radius:float
	Field theta:int
	Field backwards_color:bool
	Field backwards_rad:bool
	Field rgb:Int[]
	Field offsets:Int[]
	
	Method New()
		rgb =[255, 0, 255]
		offsets =[RAD, RAD, -RAD, RAD, RAD, -RAD, -RAD, -RAD]
		backwards_color = True
		alpha = 0.0
		tiles = New ShieldTile[OVERLAY_MAX_X * OVERLAY_MAX_Y]
		For Local temp:Int = 0 Until tiles.Length()
			tiles[temp] = New ShieldTile()
		Next
	End
	Method NewImpact:Void(x:Int, y:Int)
		x += 1
		y += 1
		ImpactID += 1
		Local impactL:Stack<Impact > = New Stack<Impact > ()
		Local temp:Impact = New Impact(x, y, ImpactID, 0.4, 0)
		impactL.Push(temp)
		For Local i:Impact = EachIn impactL
			GetTile(i.x, i.y).NewImpact(i)
			Local test:Impact = New Impact(i.x, i.y + 1, i.id, i.alpha * ALPHA_DECAY, i.offset + OFFSET_MULT)
			If canImpact(test)
				impactL.Push(test)
			End
			test = New Impact(i.x, i.y - 1, i.id, i.alpha * ALPHA_DECAY, i.offset + OFFSET_MULT)
			If canImpact(test)
				impactL.Push(test)
			End
			test = New Impact(i.x + 1, i.y, i.id, i.alpha * ALPHA_DECAY, i.offset + OFFSET_MULT)
			If canImpact(test)
				impactL.Push(test)
			End
			test = New Impact(i.x - 1, i.y, i.id, i.alpha * ALPHA_DECAY, i.offset + OFFSET_MULT)
			If canImpact(test)
				impactL.Push(test)
			End
		Next
	End
	Method CloseShields:Void(x:Int, y:Int)
		x += 1
		y += 1
		GetTile(x - 3, y).fadeout = -60
		GetTile(x - 3, y + 1).fadeout = -60
		GetTile(x - 3, y - 1).fadeout = -60
	'	GetTile(x - 3, y).type = ShieldTile.INTERIOR
	'	GetTile(x - 3, y + 1).type = ShieldTile.INTERIOR
	'	GetTile(x - 3, y - 1).type = ShieldTile.INTERIOR
	End
	
	Method canImpact:Bool(i:Impact)
		If i.alpha < MIN_ALPHA Then Return False
		If Not TileExists(i.x, i.y) Then Return False
		Return GetTile(i.x, i.y).CanImpact(i)
	End
	Method Update:Void()
		counter += 1
		
		If backwards_color
			rgb[0] -= 1
			If rgb[0] <= 155
				backwards_color = False
			End
		Else
			rgb[0] += 1
			If rgb[0] >= 254
				backwards_color = True
			End
		End
	End
	Method Render:Void(isPaused:bool)
		SetAlpha(alpha)
		SetColor(rgb[0], rgb[1], rgb[2])
		SetBlend(AdditiveBlend)
		For Local x:Int = 0 Until OVERLAY_MAX_X
			For Local y:Int = 0 Until OVERLAY_MAX_Y
				If tiles[ (x * OVERLAY_MAX_X) + y].type = ShieldTile.SHIELD or tiles[ (x * OVERLAY_MAX_X) + y].type = ShieldTile.INTERIOR
					tiles[ (x * OVERLAY_MAX_X) + y].Render(x, y, offsets, alpha, isPaused)
				End
			Next
		Next
		ResetColorAlpha()
		SetBlend(AlphaBlend)
		#rem
		If alpha > 0.05
		SetBlend(AdditiveBlend)
		SetColor(rgb[0], rgb[1], rgb[2])
		SetAlpha(alpha)
		DrawImage(Tile.img_shield, (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2, (y * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2)
		For Local temp:Int = 0 Until 4
		DrawImage(Tile.img_shield, offsets[temp * 2] + (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2, offsets[ (temp * 2) + 1] + (y * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2)
		Next
		ResetColorAlpha()
		SetBlend(AlphaBlend)
		End
		'For Local x:Int = 0 Until MAX_X
		'	For Local y:Int = 0 Until MAX_Y
		#end
	End
	
	
	Method SetShip:Void(ship:Ship)
		For Local temp:Int = 0 Until tiles.Length()
			tiles[temp] = New ShieldTile()
		Next
		For Local x:Int = 0 Until Ship.MAX_X
			For Local y:Int = 0 Until Ship.MAX_Y
				If not ship.pipes[ (x * Ship.MAX_X) + y].isVacuum()
					tiles[ (x + 1) * OVERLAY_MAX_X + (y + 1)].type = ShieldTile.INTERIOR
				End
			Next
		Next
		For Local x:Int = 0 Until OVERLAY_MAX_X
			For Local y:Int = 0 Until OVERLAY_MAX_Y
				If GetTile(x, y).type = ShieldTile.EXTERIOR
					If TouchingType(x, y, ShieldTile.INTERIOR, False)
						GetTile(x, y).type = ShieldTile.DEAD_ZONE
					End
				End
			Next
		Next
		For Local x:Int = 0 Until OVERLAY_MAX_X
			For Local y:Int = 0 Until OVERLAY_MAX_Y
				If GetTile(x, y).type = ShieldTile.EXTERIOR
					If TouchingType(x, y, ShieldTile.DEAD_ZONE, True)
						GetTile(x, y).type = ShieldTile.SHIELD
					End
				End
			Next
		Next
		For Local x:Int = 0 Until OVERLAY_MAX_X
			For Local y:Int = 0 Until OVERLAY_MAX_Y
				If GetTile(x, y).type = ShieldTile.SHIELD
					If not TouchingType(x, y, ShieldTile.EXTERIOR, False)
						GetTile(x, y).type = ShieldTile.DEAD_ZONE
					End
				End
			Next
		Next
		For Local temp:Int = 0 Until tiles.Length()
			If tiles[temp].type = ShieldTile.DEAD_ZONE Then tiles[temp].type = ShieldTile.INTERIOR
		Next
	End
	
	Method TileExists:bool(x:Int, y:Int)
		If x < 0 Then Return False
		If x >= OVERLAY_MAX_X Then Return False
		If y < 0 Then Return False
		If y >= OVERLAY_MAX_Y Then Return False
		Return True
	End
	
	Method GetTile:ShieldTile(x:Int, y:Int)
		Return tiles[x * OVERLAY_MAX_X + y]
	End
	
	Method TouchingType:Bool(x:Int, y:Int, type:Int, ns_only:bool)
		If TileExists(x + 1, y) And GetTile(x + 1, y).type = type Then Return True
		If TileExists(x - 1, y) And GetTile(x - 1, y).type = type Then Return True
		If not ns_only
			If TileExists(x + 1, y + 1) And GetTile(x + 1, y + 1).type = type Then Return True
			If TileExists(x + 1, y - 1) And GetTile(x + 1, y - 1).type = type Then Return True
			If TileExists(x - 1, y + 1) And GetTile(x - 1, y + 1).type = type Then Return True
			If TileExists(x - 1, y - 1) And GetTile(x - 1, y - 1).type = type Then Return True
		End
		If TileExists(x, y + 1) And GetTile(x, y + 1).type = type Then Return True
		If TileExists(x, y - 1) And GetTile(x, y - 1).type = type Then Return True
		Return False
	End
	Method CountType:Int(x:Int, y:Int, type:Int, ns_only:bool)
		Local count:Int = 0
		If TileExists(x + 1, y) And GetTile(x + 1, y).type = type Then count += 1
		If TileExists(x - 1, y) And GetTile(x - 1, y).type = type Then count += 1
		If Not ns_only
			If TileExists(x + 1, y + 1) And GetTile(x + 1, y + 1).type = type Then count += 1
			If TileExists(x + 1, y - 1) And GetTile(x + 1, y - 1).type = type Then count += 1
			If TileExists(x - 1, y + 1) And GetTile(x - 1, y + 1).type = type Then count += 1
			If TileExists(x - 1, y - 1) And GetTile(x - 1, y - 1).type = type Then count += 1
		End
		If TileExists(x, y + 1) And GetTile(x, y + 1).type = type Then count += 1
		If TileExists(x, y - 1) And GetTile(x, y - 1).type = type Then count += 1
		Return count
	End
	
	Method IncreaseAlpha:Void()
		alpha = Min(alpha + ALPHA_CHANGE, MAX_ALPHA)
	End
	Method DecreaseAlpha:Void()
		alpha = (Max(alpha - ALPHA_CHANGE, 0.0))
	End
End

Class Impact
	Field x:Int
	Field y:int
	Field id:Int
	Field alpha:float
	Field offset:Int
	Method New(x:int, y:int, id:Int, alpha:Float, offset:Int)
		Self.x = x
		Self.y = y
		Self.id = id
		Self.alpha = alpha
		Self.offset = offset
	End
	
End

Class ShieldTile
	Const EXTERIOR:Int = 0
	Const INTERIOR:Int = 1
	Const DEAD_ZONE:Int = 2
	Const SHIELD:Int = 3
	Const HIDDEN_SHIELD:Int = 4
	Const GRID_SIZE:Int = Ship.GRID_SIZE
	Const DEBUG_RENDER:Bool = False
	Const ALPHA_DECAY:float = 0.005
	Field type:Int
	Field impacts:Stack<Impact>
	Field fadeout:int
	Field shield_alpha_adjust:float = 1.0
	Method New()
		impacts = New Stack<Impact>()
	End
	
	Method NewImpact:Void(i:Impact)
		impacts.Push(i)
	End
	Method CanImpact:Bool(i:Impact)
		If type <> INTERIOR Then Return False
		For Local temp:Int = 0 Until impacts.Length()
			If impacts.Get(temp).id = i.id Then Return False
		Next
		Return True
	End
	
	Method getImpactAlpha:Float(isPaused:bool)
		Local alpha:Float = 0.0
		For Local temp:Int = 0 Until impacts.Length()
			If impacts.Get(temp).alpha <= 0.0
				impacts.Remove(temp)
				temp -= 1
			Else If impacts.Get(temp).offset > 0
				If Not isPaused
					impacts.Get(temp).offset -= 1
				End
			Else
				alpha += impacts.Get(temp).alpha
				If Not isPaused
					impacts.Get(temp).alpha -= ALPHA_DECAY
				End
			End
		Next
		Return alpha
	End
	Method Render:Void(x:Int, y:Int, offsets:Int[], alpha:float, isPaused:bool)
		x -= 1
		y -= 1
		If DEBUG_RENDER
			SetAlpha(0.3)
			If type = INTERIOR
				SetColor(0, 255, 0)
				DrawRect( (x * GRID_SIZE), (y * GRID_SIZE), GRID_SIZE, GRID_SIZE)
			Else If type = DEAD_ZONE
				SetColor(255, 0, 0)
				DrawRect( (x * GRID_SIZE), (y * GRID_SIZE), GRID_SIZE, GRID_SIZE)
			Else If type = SHIELD
				SetColor(0, 0, 255)
				DrawRect( (x * GRID_SIZE), (y * GRID_SIZE), GRID_SIZE, GRID_SIZE)
			Else If type = HIDDEN_SHIELD
				SetColor(255, 0, 255)
				DrawRect( (x * GRID_SIZE), (y * GRID_SIZE), GRID_SIZE, GRID_SIZE)
			End
			ResetColorAlpha()
		Else If type = SHIELD
			If Not isPaused
				fadeout = Clamp(fadeout + 1, -60, 1)
				If fadeout <= 0
					shield_alpha_adjust = Max(0.0, shield_alpha_adjust - 0.01)
				Else
					shield_alpha_adjust = Min(1.0, shield_alpha_adjust + 0.005)
				End
			End
			alpha *= shield_alpha_adjust
			If alpha > 0.005
				SetAlpha(alpha)
				DrawImage(Tile.img_shield, (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2, (y * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2)
				For Local temp:Int = 0 Until 4
					DrawImage(Tile.img_shield, x + offsets[temp * 2] + (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2, y + offsets[ (temp * 2) + 1] + (y * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2)
				Next
			End
		Else If type = INTERIOR And Not impacts.IsEmpty()
			SetAlpha(getImpactAlpha(isPaused))
			DrawImage(Tile.img_shield, (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2, (y * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2)
			For Local temp:Int = 0 Until 4
				DrawImage(Tile.img_shield, x + offsets[temp * 2] + (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2, y + offsets[ (temp * 2) + 1] + (y * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2)
			Next
		End
	'	SetAlpha(0.2)
	'	DrawRect( (x * GRID_SIZE), (y * GRID_SIZE), GRID_SIZE, GRID_SIZE)
	'	ResetColorAlpha()
		#rem
		DrawImage(Tile.img_shield, (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2, (y * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2)
		For Local temp:Int = 0 Until 4
			DrawImage(Tile.img_shield, offsets[temp * 2] + (x * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2, offsets[ (temp * 2) + 1] + (y * Ship.GRID_SIZE) + Ship.GRID_SIZE / 2)
		Next
		#end
	End
End
