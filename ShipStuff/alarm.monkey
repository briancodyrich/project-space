Strict

Import space_game

Class Alarm
	Const LEVEL_DEAD:Int = 0
	Const LEVEL_WARN:Int = 1
	Const LEVEL_ALERT:Int = 2
	Const LEVEL_ALARM:int = 3
	Const SOUND_NONE:Int = 0
	Const SOUND_BUZZ:Int = 1
	Const SOUND_ALARM:Int = 2
	
	Const DEAD:Int = 0
	Const WARN:Int = 1
	Const ALERT:Int = 2
	Const ALARM:Int = 3
	Const CRITICAL:Int = 4
	
	Const WIDTH:int = 400
	Const HEIGHT:Int = 60
	Const SPEED:Int = 20
	Const DECAY:Float = 0.01
	Const MAX_ALPHA:Float = 0.8
	Global RGB:Int[] =[18, 173, 42, 153, 204, 255, 255, 204, 51, 255, 102, 102]
	Field x:Int, y:Int
	Field rgb:Int[]
	Field type:int
	Field text:String[]
	Field level:int
	Field start:Int
	Field goal:int
	Field last_level:int
	Field time:Int
	Field alpha:float
	Field effect:Int[]
	Method New(x:Int, y:Int, goal:Int, type:Int, level:Int, effect:Int[], text:String[])
		Self.x = x
		Self.y = y
		Self.time = 0
		Self.start = y
		Self.goal = goal
		Self.type = type
		Self.level = level
		Self.effect = effect
		Self.text = text
		Self.alpha = MAX_ALPHA
		rgb =[RGB[ (level * 3)], RGB[ (level * 3) + 1], RGB[ (level * 3) + 2]]
	End
	Method Render:Void(position:int)
		If effect[Min(level, effect.Length() -1)] = DEAD
			SetColor(18, 173, 42)
		Else If effect[Min(level, effect.Length() -1)] = WARN
			SetColor(153, 204, 255)
		Else If effect[Min(level, effect.Length() -1)] = ALERT
			SetColor(255, 204, 51)
		Else If effect[Min(level, effect.Length() -1)] = ALARM
			SetColor(255, 102, 120)
		Else If effect[Min(level, effect.Length() -1)] = CRITICAL
			SetColor(255, 8, 127)
		End
	'	SetColor(RGB[ (level * 3)], RGB[ (level * 3) + 1], RGB[ (level * 3) + 2])
		SetAlpha(alpha)
		DrawRectOutline(x, y, WIDTH, HEIGHT)
		Game.white_font.Draw(text[Min(level, text.Length() -1)], x + (WIDTH / 2), y + (HEIGHT / 6), 0.5)
		ResetColorAlpha()
	End
	Method Update:int(min:Int)
		Local r_s:int = 0
		If last_level <> level
			If level = 0
				r_s = 0
			Else If last_level < level
				If effect[Min(level, effect.Length() -1)] = DEAD
					r_s = 0
				Else If effect[Min(level, effect.Length() -1)] = WARN
					r_s = 0
				Else If effect[Min(level, effect.Length() -1)] = ALERT
					r_s = 1
				Else If effect[Min(level, effect.Length() -1)] = ALARM
					r_s = 2
				Else If effect[Min(level, effect.Length() -1)] = CRITICAL
					r_s = 2
				End
			End
			last_level = level
		End
		If rgb[0] > RGB[ (level * 3)]
			rgb[0] -= 3
		Else If rgb[0] < RGB[ (level * 3)]
			rgb[0] += 3
		End
		If rgb[1] > RGB[ (level * 3) + 1]
			rgb[1] -= 3
		Else If rgb[1] < RGB[ (level * 3) + 1]
			rgb[1] += 3
		End
		If rgb[2] > RGB[ (level * 3) + 2]
			rgb[2] -= 3
		Else If rgb[2] < RGB[ (level * 3) + 2]
			rgb[2] += 3
		End
		If min <> goal
			start = y
			time = 0
			goal = min
		End
		If time <= SPEED
			y = easeOutQuad(time, start, goal - start, SPEED)
			time += 1
		End
		If level = 0
			alpha = 0
			'alpha = Max(alpha - DECAY, 0.0)
		Else
			'alpha = Min(alpha + DECAY, MAX_ALPHA)
		End
		Return r_s
	End
	Method SetLevel:bool(level:Int)
		If level <> last_level
			Self.level = level
			Return True
		End
		Self.level = level
		Return False
	End
	Method isType:Bool(type:Int)
		If Self.type = type Then Return True
		Return False
	End
	Method GetSortLv:Int()
		Return effect[Min(level, effect.Length() -1)]
	End
End

Class AlarmStack<T> Extends Stack<T>
	Method Compare:Int(lhs:T, rhs:T)
		If lhs.GetSortLv() < rhs.GetSortLv() Return - 1
		If lhs.GetSortLv() > rhs.GetSortLv() Return 1
		Return 0
	End
End

Class AlarmManager
	Const TestTimer:Int = 1
	Const FireAlarm:Int = 2
	Const HeatLevel:Int = 3
	Const BatteryLevel:Int = 4
	Const SystemOffline:Int = 5
	Const START_X:Int = (Ship.MAX_X * Ship.GRID_SIZE) -80
	Const START_Y:Int = (Ship.MAX_Y * Ship.GRID_SIZE) - Alarm.HEIGHT
	Const PADDING:Int = 6
	Field alerts:AlarmStack<Alarm>
	Field range:Stack<AlarmRange>
	Method New()
		alerts = New AlarmStack<Alarm>()
		range = New Stack<AlarmRange>()
	'	range.Push(New AlarmRange(TestTimer,[0, 10, 20, 100],[Alarm.DEAD, Alarm.ALERT, Alarm.ALARM],["[TEST]", "[FIRE]", "[MAJOR FIRE]"]))
	'	range.Push(New AlarmRange(TestTimer,[0, 10, 20, 100],[Alarm.DEAD, Alarm.ALERT, Alarm.ALARM],["[TEST]", "[FIRE]", "[MAJOR FIRE]"]))
		range.Push(New AlarmRange(FireAlarm,[0, 2, 4, 20],[Alarm.DEAD, Alarm.ALERT, Alarm.ALARM],["", "[FIRE]", "[MAJOR FIRE]"]))
		range.Push(New AlarmRange(HeatLevel,[0, 60, 90, 100],[Alarm.DEAD, Alarm.WARN, Alarm.ALERT, Alarm.ALARM],["", "[HIGH HEAT]", "[CRITICAL HEAT]", "[COOLANT SYSTEM OFFLINE]"]))
		range.Push(New AlarmRange(BatteryLevel,[0, 50, 75, 100],[Alarm.DEAD, Alarm.WARN, Alarm.ALERT, Alarm.ALARM],["", "[BACKUP POWER DEPLETING]", "[BACKUP POWER LOW]", "[BACKUP POWER OFFLINE]"]))
		range.Push(New AlarmRange(SystemOffline,[0, 40, 100, 200],[Alarm.DEAD, Alarm.ALERT, Alarm.ALERT, Alarm.ALERT],["", "[INSUFFICIENT POWER]", "[INSUFFICIENT POWER]", "[INSUFFICIENT POWER]"]))
	End
	Method Render:Void()
		For Local temp:Int = 0 Until alerts.Length()
			alerts.Get(temp).Render(0)
		Next
	End
	Method Update:Void()
		Local max:int = 0
		For Local temp:Int = 0 Until alerts.Length()
			Local s:Int = alerts.Get(temp).Update(getMin(temp))
			If s > max
				max = s
			End
			If alerts.Get(temp).alpha <= 0.0
				alerts.Remove(temp)
				temp -= 1
			End
		Next
		If max = 1 Then SoundEffect.Play(SoundEffect.ALERT)
		If max = 2 Then SoundEffect.Play(SoundEffect.ALARM)
	End
	Method Monitor:Void(type:Int, stat:float)
		If AlarmExists(type)
			If SetLevel(type, GetStatus(type, GetLevel(type), stat))
				alerts.Sort()
			End
		Else
			Local lv:Int = GetStatus(type, 0, stat)
			If lv > 0
				Local a:Alarm = New Alarm(START_X, START_Y, getMin(alerts.Length()), type, lv, GetEffect(type), GetString(type))
				alerts.Push(a)
				alerts.Sort(False)
			End
		End
	End
	Method AlarmExists:Bool(type:Int)
		For Local temp:Int = 0 Until alerts.Length()
			If alerts.Get(temp).isType(type) Then Return True
		Next
		Return False
	End
	Method SetLevel:Bool(type:Int, level:Int)
		For Local temp:Int = 0 Until alerts.Length()
			If alerts.Get(temp).isType(type)
				Return alerts.Get(temp).SetLevel(level)
			End
		Next
		Return False
	End
	Method GetLevel:Int(type:Int)
		For Local temp:Int = 0 Until alerts.Length()
			If alerts.Get(temp).isType(type)
				Return alerts.Get(temp).level
			End
		Next
		Return 0
	End
	Method GetStatus:Int(type:Int, state:Int, val:float)
		For Local temp:Int = 0 Until range.Length()
			If range.Get(temp).isType(type)
				Return range.Get(temp).GetStatus(state, val)
			End
		Next
		Return 0
	End
	Method GetEffect:Int[] (type:Int)
		For Local temp:Int = 0 Until range.Length()
			If range.Get(temp).isType(type)
				Return range.Get(temp).GetEffect()
			End
		Next
		Return[0, 0, 0]
	End
	Method GetString:String[] (type:Int)
		For Local temp:Int = 0 Until range.Length()
			If range.Get(temp).isType(type)
				Return range.Get(temp).GetString()
			End
		Next
		Return["[NULL]"]
	End
	
	Method Add:Void(type:Int, level:Int)
		Local a:Alarm = New Alarm(START_X, START_Y, getMin(alerts.Length()), Rnd(9), Rnd(3) + 1,[1, 2, 3], "[TEST]")
		alerts.Push(a)
		alerts.Sort(False)
	End
	Method getMin:Int(index:Int)
		Return 2 + (index * Alarm.HEIGHT) + (PADDING * (index + 1))
	End
End




Class AlarmRange
	Const NONE:Int = Alarm.SOUND_NONE
	Const BUZZ:Int = Alarm.SOUND_BUZZ
	Const ALARM:Int = Alarm.SOUND_ALARM
	Field range:Float[]
	Field effect:Int[]
	Field text:String[]
	Field type:int
	Field size:int
	Method New(type:Int, set:int[], effect:Int[], text:String[])
		Self.type = type
		Self.text = text
		range = New Float[16]
		Self.effect = effect
		For Local x:Int = 0 Until 4
			For Local y:Int = 0 Until 4
				If y = 0 Or y > x
					range[4 * x + y] = set[y]
				Else
					range[4 * x + y] = (set[y - 1] + set[y]) / 2
				End
			Next
		Next
		#rem
		For Local x:Int = 0 Until 4
			Local s:String = ""
			For Local y:Int = 0 Until 4
				s += range[4 * x + y] + "-"
			Next
		Next
		#end
	End
	Method GetStatus:Int(state:int, data:float)
		For Local temp:Int = 3 To 0 Step - 1
			If data >= range[4 * state + temp]
				Return temp
			End	
		Next
		Return 0
	End
	Method GetString:String[] ()
		Return text
	End
	Method GetEffect:Int[] ()
		Return effect
	End
	Method isType:bool(type:int)
		If Self.type = type Then Return True
		Return False
	End
	
End










