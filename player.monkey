Strict

Import space_game

Class PlayerData Implements Loadable
	
	Private
	Field credits:Int
	
	Public
	Field accountName:String
	Field seed:int
	Field ship:Ship
	Field map:MapData
	Field galaxy:HyperNodeManager
	Field paused:bool
	Field panel:ShipPanel
	Field menu:ContextMenu
	Field accounting:Report
	Field quests:QuestManager
	Field clock:Clock
	Field gameMode:Int
	Field chat:Logger
	Field isDocked:bool
	Field lootData:JsonObject
	
	Field nameGen:NameGenerator
	Field interaction:InteractionEvent
	
	Method New(seed:Int, accountName:String)
		Self.seed = seed
		Self.accountName = accountName
		map = New MapData()
		menu = New ContextMenu()
		map.x = 0
		map.y = 0
		map.z = 0
		nameGen = New NameGenerator(Self.seed)
		interaction = New InteractionEvent()
		galaxy = New HyperNodeManager()
		accounting = New Report()
		quests = New QuestManager()
		chat = New Logger()
		clock = New Clock()
	End
	Method FileName:String()
		Return "_main.json"
	End
	Method OnSave:JsonObject()
		Local data:JsonObject = New JsonObject()
		data.SetInt("credits", credits)
		data.SetInt("seed", seed)
		data.SetInt("time", clock.time)
		data.SetInt("day", clock.day)
		data.SetInt("time", clock.time)
		data.SetBool("isDocked", isDocked)
		data.Set("map", map.OnSave())
		data.Set("ship", ship.OnSave())
		data.Set("panel", panel.OnSave())
		data.Set("quests", quests.OnSave())
		data.SetString("accountName", accountName)
		data.SetInt("gamemode", gameMode)
		Return data
	End
	Method OnLoad:Void(data:JsonObject)
		credits = data.GetInt("credits")
		If Constants.loadsamoney Then credits = 999999999
		seed = data.GetInt("seed")
		clock.day = data.GetInt("day")
		clock.time = data.GetInt("time")
		data.GetBool("isDocked", False)
		ship.clock = clock
		nameGen = New NameGenerator(seed)
		accountName = data.GetString("accountName")
		map.OnLoad(JsonObject(data.Get("map")))
		ship.OnLoad(JsonObject(data.Get("ship")))
		quests.OnLoad(JsonObject(data.Get("quests")))
		panel = New ShipPanel()
		panel.OnLoad(JsonObject(data.Get("panel")))
		gameMode = data.GetInt("gamemode")
	End
	
	Method addQuest:Void(quest:Quest)
		quests.addQuest(quest)
		map.hexField.AddQuest(quests.id)
		Print "add quest"
	End
	
	Method rewardQuest:Void(quest:Quest)
		Print "rewardQuest"
		For Local r:Reward = EachIn quest.rewards
			Select r.type.ToUpper()
				Case "CREDITS"
					Player.accounting.LogCredits(CreditLog.BOUNTY, r.value, CreditLog.PAY_NOW)
					Player.chat.Log(LogItem.BANK, FormatCredits(r.value) + " collected")
				Case "STANDING"
					If r.value < 0
						Player.quests.AddStanding(r.faction, r.value)
						Player.chat.Log(LogItem.NEGATIVE_STANDING, "relations with " + r.faction + " has decreased")
					Else If r.value > 0
						Player.quests.AddStanding(r.faction, r.value)
						Player.chat.Log(LogItem.POSITIVE_STANDING, "relations with " + r.faction + " has increased")
					End
				
			End
		Next
		quests.removeQuest(quest.id)
	End
	
	Method getCredits:Int()
		Return credits
	End
	
	Method addCredits:Void(credits:Int)
		Self.credits += credits
	End
	Method SaveGame:Void()
		Loader.SaveGame(accountName, Self)
		Loader.SaveGame(accountName, Self.map.hexField)
		Loader.SaveGame(accountName, Self.galaxy)
		Loader.SaveGame(accountName, Self.chat)
		Loader.SaveGame(accountName, Self.accounting)
	End
	Method LoadGame:Void()
		Loader.LoadGame(accountName, Self)
		Loader.LoadGame(accountName, Self.galaxy)
		
		map.hexField = New HexField(map.economy, Player.map.system, Player.map.level, Player.nameGen.GetName(Player.map.system))
		Loader.LoadGame(accountName, Self.map.hexField)
		
		
		Loader.LoadGame(accountName, Self.chat)
		Loader.LoadGame(accountName, Self.accounting)
	End
End

Class InteractionEvent
	Field scripts:Stack<Script>
	Field heap:Stack<Int>
	Field locked:bool
	
	Method New()
		scripts = New Stack<Script>
		heap = New Stack<Int>
	End
	
	Method LoadScript:Void(path:String)
		If ScriptManager.DEBUG_CHOOSE
			'Self.scripts = ScriptManager.DebugGetScript(path)
		Else
			Self.scripts = ScriptManager.GetScript(path)
		End
	End
	
	#rem
	Method SetEvent:Void(scriptStack:Stack<Script>)
		Self.scripts = scriptStack
	End
	#end
	
	Method HasScript:Bool()
		If scripts = Null Then Return False
		Return Not scripts.IsEmpty()
	End
	
	Method GetScript:Script()
		Return scripts.Get(0)
	End
	
	Method DestroyScript:Void()
		If Not scripts.IsEmpty() Then scripts.Remove(0)
	End
	Method HandShake:Void()
		locked = False
	End
	
	Method InsertScripts:Bool(ss:Stack<Script>)
		If (ss = Null) Then Return True
		For Local temp:Int = 0 Until ss.Length()
			scripts.Insert(temp + 1, ss.Get(temp))
		Next
		Return True
	End
	
	Method Run:Bool(s:Script)
		If locked Then Return False
		'try system calls
		Select s.key
			Case Script.RUN
				If (s.minArgs(1))
					InsertScripts(ScriptManager.GetScript(s.values.Get(0)))
				End
			Case Script.THROW_ERROR
				ThrowErrorMessage(s.values)
			Case Script.GOTO
				If s.minArgs(1)
					locked = True
					Game.SwitchScreen(Int(s.values.Get(0)))
				End
				
			Case Script.WAIT
				If s.minArgs(1)
					If heap.IsEmpty()
						heap.Push(Millisecs() +Int(s.values.Get(0)))
						Return False
					Else
						If Millisecs() >= heap.Get(0)
							heap.Clear()
							Return True
						End
						Return False
					End
				End
			Case Script.CANCEL
				If s.minArgs(1)
					If s.values.Get(0).Contains("movement")
						Player.map.hexField.CancelMovement()
					End
				End
			Case Script.SHUTDOWN
				scripts.Clear()
			Case Script.PRINTLN
				If s.minArgs(1)
					Print(s.values.Get(0))
				End
			Case Script.REMOVE
				If s.minArgs(2)
					If s.values.Get(0).Contains("credits")
						Player.credits -= Int(s.values.Get(1))
					Else If s.values.Get(0).Contains("item")
						Local num:Int = Int(s.values.Get(1))
						Print "remove items: " + num
						While num > 0 And Player.ship.items.CountSlots(True, False) > 0
							Local rnd:Int = Rnd(Player.ship.items.slots.Length())
							If Player.ship.items.slots[rnd] <> Null
								Player.ship.items.slots[rnd] = Null
								num -= 1
							End
						Wend
					Else If s.values.Get(0).Contains("system")
						Local num:Int = Int(s.values.Get(1))
						Local timeout:Int = 100
						Print "remove systems: " + num
						While num > 0
							Local rnd:Int = Rnd(Player.ship.comp.Length())
							If Player.ship.comp.Get(rnd).getType() <> Component.COMPUTER and Player.ship.comp.Get(rnd).getType() <> Component.HARDPOINT
								Player.ship.comp.Remove(rnd)
								num -= 1
							End
							timeout -= 1
							If timeout <= 0 Then num = 0
						Wend
					End
				End
			Case Script.DAMAGE
				Local damage:JsonObject
				Local target:Int = 0
				If s.minArgs(1)
					damage = ScriptManager.GetObject(s.values.Get(0))
					If damage = Null
						ThrowErrorMessage("Unable to load script object", s.values.Get(0))
						Return True
					End
					If s.minArgs(2)
						target = Int(s.values.Get(1))
					End
				Else
					Return True
				End
				
				If target <> 0
					For Local tries:Int = 0 Until 50
						Local num:Int = Rnd(Player.ship.comp.Length())
						If Player.ship.comp.Get(num).getType() = target
							Local tx:Int = Player.ship.comp.Get(num).getX()
							Local ty:Int = Player.ship.comp.Get(num).getY()
							If Player.ship.pipes[tx * Ship.MAX_X + ty].isDamageable()
								Player.ship.TakeDamage(New Bolt(tx, ty, damage, False))
								Return True
							End
						End
					Next
				Else
					Local done:Bool = False
					While Not done
						Local tx:Int = Rnd(Ship.MAX_X)
						Local ty:Int = Rnd(Ship.MAX_Y)
						If Player.ship.pipes[tx * Ship.MAX_X + ty].isDamageable()
							Player.ship.TakeDamage(New Bolt(tx, ty, damage, False))
							done = True
						End
					Wend
				End
			Return True
			Default
			
				If ScriptManager.DEBUG_PRINT
					Print "missing system call: " + s.key
				End
		End
		Return True
	End
End

Class Clock
	Const DAY:Int = 172800
	Const HOUR:Int = 7200
	Const MIN:Int = 120
	
	Field pay_crew:bool
	Field time:Int
	Field day:Int
	
	Method tick:Void(x:Int)
		time += x
		If time >= DAY
			Print "new day"
			time -= DAY
			day += 1
			pay_crew = True
		End
	End
	Method GetTimeStamp:String()
		Local r:String = "[" + Format(day)
		Local num:Int = time / HOUR
		r += "|" + Format(num)
		num = time - (num * HOUR)
		num /= MIN
		r += "|" + Format(num) + "]"
		Return r
	End
	Method Format:String(num:Int)
		If num < 10
			Return "0" + num
		End
		Return num
	End
End


Class GameConst
	Const GAMEMODE_EASY:Int = 1
	Const GAMEMODE_NORMAL:Int = 2
	Const GAMEMODE_HARDCORE:Int = 3
	
End