Strict

Import space_game

Class QuestManager
	Field id:Int
	Field standing:StringMap<Int>
	Field chain:StringMap<Int>
	Field quests:Stack<Quest>
	Field currentQuest:int
	Const SHIP_X_OFFSET:Int = 500
	Const SHIP_Y_OFFSET:int = 500
	Method New()
		standing = New StringMap<Int>()
		chain = New StringMap<Int>()
		quests = New Stack<Quest>()
	End
	
	Method OnSave:JsonObject()
		Local data:JsonObject = New JsonObject()
		data.SetInt("id", id)
		Local arr:JsonArray = New JsonArray(standing.Count() * 2)
		Local count:Int
		For Local str:String = EachIn standing.Keys()
			arr.SetString(count, str)
			arr.SetInt(count + 1, standing.Get(str))
			count += 2
		Next
		data.Set("standing", arr)
		arr = New JsonArray(chain.Count() * 2)
		count = 0
		For Local str:String = EachIn chain.Keys()
			arr.SetString(count, str)
			arr.SetInt(count + 1, chain.Get(str))
			count += 2
		Next
		data.Set("chain", arr)
		arr = New JsonArray(quests.Length())
		For Local temp:Int = 0 Until arr.Length()
			arr.Set(temp, quests.Get(temp).OnSave())
		Next
		data.Set("quests", arr)
		Return data
	End
	Method OnLoad:Void(data:JsonObject)
		id = data.GetInt("id")
		
		standing.Clear()
		Local arr:JsonArray = JsonArray(data.Get("standing"))
		For Local temp:Int = 0 Until arr.Length() Step 2
			standing.Add(arr.GetString(temp), arr.GetInt(temp + 1))
		Next
		
		chain.Clear()
		arr = JsonArray(data.Get("chain"))
		For Local temp:Int = 0 Until arr.Length() Step 2
			chain.Add(arr.GetString(temp), arr.GetInt(temp + 1))
		Next
		
		quests.Clear()
		arr = JsonArray(data.Get("quests"))
		For Local temp:Int = 0 Until arr.Length()
			Local obj:JsonObject = JsonObject(arr.Get(temp))
			Local d:JsonObject = QuestLoader.GetQuest(obj.GetString("name"))
			If d <> Null
				quests.Push(New Quest(d))
			End
		Next
	End
	
	Method canAccept:Bool(quest:Quest)
		Return True
	End
	
	Method CheckStanding:Int(faction:String)
		Return standing.Get(faction)
	End
	
	Method AddStanding:Void(faction:String, value:Int)
		Local current:int = CheckStanding(faction)
		standing.Set(faction, current + value)
	End
	
	Method addQuest:Void(data:Quest)
		id += 1
		data.id = id
		quests.Push(data)
	End
	Method removeQuest:Void(id:Int)
		For Local temp:Int = 0 Until quests.Length()
			If quests.Get(temp).id = id
				quests.Remove(temp)
			End
		Next
	End
	Method rewardQuest:Void(id:Int)
		
	End
	Method getQuest:Quest(id:Int)
		For Local temp:Int = 0 Until quests.Length()
			If quests.Get(temp).id = id
				Return quests.Get(temp)
			End
		Next
		Return Null
	End
End

Class Quest
	Field id:int
	Field rewards:Stack<Reward>
	Field ship:Ship
	Field script:String
	
	'for viewing
	Field buttonText:String
	Field descr:Stack<String>
	
	Method New(data:JsonObject)
		If data.Contains("ship")
			Local dat:JsonObject = JsonObject(data.Get("ship"))
			Local jShip:JsonObject = EntityLoader.GetMember(dat.GetString("faction"), dat.GetString("type"), Player.map.level)
			If jShip <> Null
				Local shipName:String = jShip.GetString("ship", "NULL")
				Local sData:JsonObject = ShipLoader.GetShip(shipName)
				If sData = Null
					ThrowErrorMessage("Unable to load ship", shipName)
				Else
					ship = New Ship(QuestManager.SHIP_X_OFFSET, QuestManager.SHIP_Y_OFFSET, True)
					ship.OnLoad(sData, jShip)
					ship.call_sign = Parse(jShip.GetString("name", "????"))
					ship.canSurrender = False
				End
				
			End
			
		End
		script = data.GetString("run", "stdlib_null.ess")
		buttonText = data.GetString("button_text", "?????")
		descr = New Stack<String>()
		If data.Contains("descr")
			Local arr:JsonArray = JsonArray(data.Get("descr"))
			For Local temp:Int = 0 Until arr.Length()
				descr.Push(arr.GetString(temp))
			Next
		End
		rewards = New Stack<Reward>()
		If data.Contains("rewards")
			Local arr:JsonArray = JsonArray(data.Get("rewards"))
			For Local temp:Int = 0 Until arr.Length()
				rewards.Push(New Reward(JsonObject(arr.Get(temp))))
			Next
		End
	End
	Method New()
		rewards = New Stack<Reward>()
		descr = New Stack<String>()
	End
	Method OnSave:JsonObject()
		Local data:JsonObject = New JsonObject()
		data.SetInt("id", id)
		If ship <> Null
			data.Set("ship", ship.OnSave())
		End
		Local descrArr:JsonArray = New JsonArray(descr.Length())
		For Local temp:Int = 0 Until descr.Length()
			descrArr.SetString(temp, descr.Get(temp))
		Next
		data.SetString("buttonText", buttonText)
		data.Set("descr", descrArr)
		Local rewardArr:JsonArray = New JsonArray(rewards.Length())
		For Local temp:Int = 0 Until rewards.Length()
			rewardArr.Set(temp, rewards.Get(temp).OnSave())
		Next
		data.Set("rewards", rewardArr)
		Return data
	End
	Method OnLoad:Void(data:JsonObject)
		id = data.GetInt("id")
		If data.Contains("ship")
			ship = New Ship(QuestManager.SHIP_X_OFFSET, QuestManager.SHIP_Y_OFFSET, True)
			ship.OnLoad(data.Get("ship"))
		End
		descr.Clear()
		If data.Contains("descr")
			Local arr:JsonArray = JsonArray(data.Get("descr"))
			For Local temp:Int = 0 Until arr.Length()
				descr.Push(arr.GetString(temp))
			Next
		End
		rewards.Clear()
		If data.Contains("rewards")
			Local arr:JsonArray = JsonArray(data.Get("rewards"))
			For Local temp:Int = 0 Until arr.Length()
				Local r:Reward = New Reward()
				rewards.Push(r.OnLoad(JsonObject(arr.Get(temp))))
			Next
		End
		buttonText = data.GetString("buttonText")
	End
	'if you change this copy it to comms screen
	Method Parse:String(str:String)
		If str.Contains("<RAND>")
			Local cArr:int[] =[int(Rnd(65, 91)), int(Rnd(65, 91)), int(Rnd(65, 91)), int(Rnd(65, 91)), int(Rnd(48, 58)), int(Rnd(48, 58))]
			Return str.Replace("<RAND>", String.FromChars(cArr))
		End
		If str.Contains("<CALL_SIGN>")
			
			If ship <> Null
				Return str.Replace("<CALL_SIGN>", ship.call_sign)
			End
		End
		Return str
	End
End

Class Reward
	Field type:String
	Field faction:String
	Field value:Int
	Method New(data:JsonObject)
		type = data.GetString("type", "NULL")
		faction = data.GetString("faction", "NULL")
		value = data.GetInt("value", 0)
		Select type
			Case "CREDITS"
			value += Rnd(value)
		End
	End
	Method New()
		
	End
	Method OnSave:JsonObject()
		Local data:JsonObject = New JsonObject()
		data.SetString("type", type)
		data.SetString("faction", faction)
		data.SetInt("value", value)
		Return data
	End
	Method OnLoad:Void(data:JsonObject)
		type = data.GetString("type", "NULL")
		faction = data.GetString("Faction", "NULL")
		value = data.GetInt("value", 0)
	End
End