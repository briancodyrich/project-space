﻿Strict

Import space_game

Class Report Implements Loadable
	Const SAVE_REPORTS:Int = 1000
	Const DEATH_PAYOUT_MULT:Int = 20
	Const DISPLAY_OFFSET:Int = 200
	
	Const BASE_INSURANCE:Float = 0.005
	Const INSURANCE_CUTOFF_UPPER:Float = 0.04
	Const INSURANCE_CUTOFF_LOWER:Float = 0.028
	Const INSURANCE_INCREASE:Float = 1.9
	Const INSURANCE_DECREASE:Float = 0.98
	Const INSURANCE_CUTOFF_DECREASE:Float = 0.985
	
	Const INSURED:Int = 1
	Const UNINSURED:Int = 2
	Const BANNED:Int = 3
	
	Const DOCKING:Int = 1
	
	Field current:ReportItem
	Field last:ReportItem
	Field delta:DeltaReport
	Field history:Stack<DeltaReport>
	Field bills:Stack<CreditLog>
	Field rate:float
	Field insuranceStatus:int
	
	Method New()
		bills = New Stack<CreditLog>()
		history = New Stack<DeltaReport>()
		rate = BASE_INSURANCE
		insuranceStatus = INSURED
	End

	Method FileName:String()
		Return "_report.json"
	End
	Method OnSave:JsonObject()
		Local data:JsonObject = New JsonObject()
		Local arr:JsonArray
		If Not history.IsEmpty()
			arr = New JsonArray(history.Length())
			For Local temp:Int = 0 Until history.Length()
				arr.Set(temp, history.Get(temp).OnSave())
			Next
			data.Set("history", arr)
		End
		
		If Not bills.IsEmpty()
			Print "Bills: " + bills.Length()
			arr = New JsonArray(bills.Length())
			For Local temp:Int = 0 Until bills.Length()
				arr.Set(temp, bills.Get(temp).OnSave())
			Next
			data.Set("bills", arr)
		End
		
		If current <> Null Then data.Set("current", current.OnSave())
		If last <> Null Then data.Set("last", last.OnSave())
		data.SetFloat("rate", rate)
		data.SetInt("insuranceStatus", insuranceStatus)
		Return data
	End

	Method OnLoad:Void(data:JsonObject)
		Local arr:JsonArray
		history.Clear()
		If data.Contains("history")
			arr = JsonArray(data.Get("history"))
			For Local temp:Int = 0 Until arr.Length()
				history.Push(New DeltaReport(JsonArray(arr.Get(temp))))
			Next
		End
		
		bills.Clear()
		If data.Contains("bills")
			arr = JsonArray(data.Get("bills"))
			For Local temp:Int = 0 Until arr.Length()
				bills.Push(New CreditLog(JsonObject(arr.Get(temp))))
			Next
		End
		If data.Contains("current")
			Print "has current"
			current = New ReportItem(JsonObject(data.Get("current")))
		Else
			current = New ReportItem(Player.ship, Player.map.economy, bills)
		End
		If data.Contains("last")
			Print "has last"
			last = New ReportItem(JsonObject(data.Get("last")))
		End
		rate = data.GetFloat("rate", BASE_INSURANCE)
		insuranceStatus = data.GetInt("insuranceStatus", INSURED)
		
	End
	
	Method DailyPayout:Void(crew_mult:Int, ins_mult:Int)
		Local s:Ship = Player.ship
		Local wages:int
		
		Local status:Int = insuranceStatus
		If s = Null Then Return
		Local insurance:Int = InsurancePayment(ins_mult)
		Local str:String = ""
		
		For Local temp:Int = 0 Until s.crew.Length()
			wages += (s.crew.Get(temp).GetWages()) * crew_mult
			s.crew.Get(temp).loyalty += (Crew.LOYALTY_PER_DAY * crew_mult)
		Next
		
		If Player.getCredits() >= wages
			LogCredits(CreditLog.WAGES, -wages, CreditLog.PAY_NOW)
		Else
			'no money code goes here	
		End
		If wages <> 0
			str += FormatCredits(wages) + " wages paid"
		End
		If insurance <> 0
			If str.Length() > 0
				str += "~n" + FormatCredits(insurance) + " insurance paid"
			Else
				str += "¤" + FormatCredits(insurance) + " insurance paid"
			End
			LogCredits(CreditLog.INSURANCE_PAYMENT, -insurance, CreditLog.PAY_NOW)
		End
		If status = Report.INSURED And insuranceStatus = Report.UNINSURED
			If str.Length() > 0
				str += "~nInsurance policy canceled"
			Else
				str += "Insurance policy canceled"
			End
		End
		If status = Report.BANNED And insuranceStatus = Report.INSURED
			If str.Length() > 0
				str += "~nInsurance policy reinstated"
			Else
				str += "Insurance policy reinstated"
			End
		End
		If str.Length() > 0
			Player.chat.Log(LogItem.BANK, str)
		End

	End
	
	Method InsurancePayment:Int(ins_mult:int)
		Local cost:Int
		If insuranceStatus = INSURED
			cost = Player.ship.GetValue(New Economy()) * rate
			If Player.getCredits() >= cost
				rate = Max(BASE_INSURANCE, rate * INSURANCE_DECREASE)
			Else
				insuranceStatus = UNINSURED
				Return 0
			End
		Else If insuranceStatus = BANNED
			rate = Max(BASE_INSURANCE, rate * INSURANCE_CUTOFF_DECREASE)
			If rate < INSURANCE_CUTOFF_LOWER
				insuranceStatus = INSURED
			End
		End
		Return cost
	End
	Method InsurancePayout:Void()
		If insuranceStatus = INSURED
			Player.ship.RepairAll()
			Local payout:Int = Player.ship.GetValue(New Economy())
			LogCredits(CreditLog.INSURANCE_PAYOUT, payout, CreditLog.PAY_NOW)
			rate *= INSURANCE_INCREASE
			Local say:String = "¤" + payout + " insurance collected"
			If rate >= INSURANCE_CUTOFF_UPPER
				insuranceStatus = UNINSURED
				say += "~ninsurance policy canceled"
			End
			
			Player.chat.Log(LogItem.BANK, say)
		End
	End
	
	Method onDocking:Void()
		Local cost:Int = Player.ship.GetRepairCost(Player.map.economy)
		If cost <> 0
			If Player.getCredits() >= cost
				LogCredits(CreditLog.REPAIRS, -cost, True)
				Player.ship.RepairAll()
				Player.chat.Log(LogItem.BANK, "¤" + cost + " paid for repairs")
			Else
				'Player.chat.Log(LogItem.BANK, "unabl")
			End
		End
		last = current
		ProcessPayments(DOCKING)
		current = New ReportItem(Player.ship, Player.map.economy, bills)
		delta = New DeltaReport(current, last, Player.ship, Player.map.economy)
		history.Push(delta)
		bills.Clear()
	End
	Method unDock:Void()
		last = current
		current = New ReportItem(Player.ship, Player.map.economy, bills)
		Print "undock"
		If history.Length() = 1
			If history.Get(0).OnSave().ToJson().Length() = 2
				history.Clear()
			End
		Else If history.Length() > 1
			If history.Get(history.Length() -1).OnSave().ToJson().Length() = 2
				history.Remove(history.Length() -1)
			End
		End
	End
	
	Method ProcessPayments:Void(type:Int)
		For Local temp:Int = 0 Until bills.Length()
			If not bills.Get(temp).collected
				If bills.Get(temp).type = CreditLog.BOUNTY
					If type = DOCKING
						Print bills.Get(temp).credits
						Player.addCredits(bills.Get(temp).credits)
					Else
						bills.Get(temp).credits = 0
					End
				Else
					Player.addCredits(bills.Get(temp).credits)
					Print bills.Get(temp).credits
				End
			End
		Next

	End
	
	Method LogCredits:Void(type:Int, credits:Int, behavior:Int)
		Select behavior
			Case CreditLog.PAY_NOW
				Player.addCredits(credits)
				bills.Push(New CreditLog(type, credits, True))
				Return
			Case CreditLog.PAY_LATER
				bills.Push(New CreditLog(type, credits, False))
				Return
			Case CreditLog.LOG_USEAGE
				bills.Push(New CreditLog(type, credits, True))
				Return
		End
		Print "UNKNOWN LOG BEHAVIOR"
	End
	
	Method UsedItem:Void(type:Int, count:Int)
		Select type
			Case Item.FOOD
				LogCredits(CreditLog.FOOD_USE, -count * current.foodPrice, CreditLog.LOG_USEAGE)
			Case Item.FUEL
				LogCredits(CreditLog.FUEL_USE, -count * current.fuelPrice, CreditLog.LOG_USEAGE)
			Case Item.ANTIMATER
				LogCredits(CreditLog.ANTIMATTER_USE, -count * current.antimatterCost, CreditLog.LOG_USEAGE)
		End
	End
	
	Method PrintReport:Void(x:Int, y:int, st:String)
		Local i:Incrementer = New Incrementer(y, 30)
		PrettyStats(x, i.getNext(), DISPLAY_OFFSET, "Report:", st)
		If delta.fuelCost  <> 0
			PrettyStats(x, i.getNext(), DISPLAY_OFFSET, "Fuel Costs:", FormatNeg(delta.fuelCost))
		End
		If delta.wagesPaid <> 0
			PrettyStats(x, i.getNext(), DISPLAY_OFFSET, "Crew Costs:", FormatNeg(delta.wagesPaid))
		End
		If delta.foodCost <> 0
			PrettyStats(x, i.getNext(), DISPLAY_OFFSET, "Food Costs:", FormatNeg(delta.foodCost))
		End
		If delta.deathBenefitsPaid <> 0
			PrettyStats(x, i.getNext(), DISPLAY_OFFSET, "Death Payout:", FormatNeg(delta.deathBenefitsPaid))
		End
		If delta.bounties <> 0
			PrettyStats(x, i.getNext(), DISPLAY_OFFSET, "Bounties:", FormatNeg(delta.bounties))
		End
		If delta.repairCost <> 0
			PrettyStats(x, i.getNext(), DISPLAY_OFFSET, "Repair Costs:", FormatNeg(delta.repairCost))
		End
		If delta.deltaShipWorth <> 0
			PrettyStats(x, i.getNext(), DISPLAY_OFFSET, "Ship Value:", FormatNeg(delta.deltaShipWorth))
		End
		If delta.deltaHold <> 0
			PrettyStats(x, i.getNext(), DISPLAY_OFFSET, "Cargo Value:", FormatNeg(delta.deltaHold))
		End
		If delta.deltaCredits <> 0
			PrettyStats(x, i.getNext(), DISPLAY_OFFSET, "Credits:", FormatNeg(delta.deltaCredits))
		End
		If delta.insurancePayout <> 0
			PrettyStats(x, i.getNext(), DISPLAY_OFFSET, "Insurance:", FormatNeg(delta.insurancePayout))
		End
		If delta.insurancePayment <> 0
			PrettyStats(x, i.getNext(), DISPLAY_OFFSET, "Insurance:", FormatNeg(delta.insurancePayment))
		End
		i.getNext()
		If delta.deltaWorth <> 0
			PrettyStats(x, i.getNext(), DISPLAY_OFFSET, "Net Worth:", FormatNeg(delta.deltaWorth))
		End
		If delta.deltaWorth <> 0
			PrettyStats(x, i.getNext(), DISPLAY_OFFSET, "Ave Worth:", FormatNeg(delta.deltaAverageWorth))
		End
	End
	
	Method FormatNeg:String(val:Int)
		If val < 0
			Return "#ff0000" + val
		Else
			Return "+" + val
		End
	End
	
	
	Method CrewDeath:Void(c:Crew)
		Local alert:String = c.first_name + " " + c.last_name + " has died"
		If Player.gameMode <> GameConst.GAMEMODE_EASY
			Local cost:Int = (c.GetWages() * DEATH_PAYOUT_MULT)
			If Player.getCredits() >= cost
				LogCredits(CreditLog.CREW_DEATH, -cost, CreditLog.PAY_NOW)
				alert += "~n¤" + cost + " death payout"
			Else
				'alert += "~n¤ death payout"
			End
		End
		Player.chat.Log(LogItem.CREW, alert)
		For Local cr:Crew = EachIn Player.ship.crew
			cr.MoraleEvent(Morale.CREW_DEATH, 1)
		Next

	End
End

Class CreditLog
	Const WAGES:Int = 1
	Const BOUNTY:Int = 2
	Const INSURANCE_PAYMENT:Int = 4
	Const INSURANCE_PAYOUT:Int = 5
	Const CREW_DEATH:Int = 6
	Const REPAIRS:Int = 7
	Const FOOD_USE:Int = 8
	Const FUEL_USE:Int = 9
	Const ANTIMATTER_USE:Int = 10
	
	Const PAY_NOW:Int = 100
	Const PAY_LATER:Int = 101
	Const LOG_USEAGE:Int = 102
	
	Field type:Int
	Field credits:Int
	Field collected:bool
	Method New(type:Int, credits:Int, collected:bool)
		Self.type = type
		Self.credits = credits
		Self.collected = collected
	End
	Method New(data:JsonObject)
		type = data.GetInt("type")
		credits = data.GetInt("credits")
		collected = data.GetBool("collected")
	End
	
	Method OnSave:JsonObject()
		Local data:JsonObject = New JsonObject()
		Print "type: " + type
		data.SetInt("type", type)
		data.SetInt("credits", credits)
		data.SetBool("collected", collected)
		Return data
	End
	
End

Class ReportItem
	Field fuelPrice:Int
	Field foodPrice:Int
	Field antimatterPrice:int
	Field fuelCost:Int
	Field foodCost:Int
	Field antimatterCost:int
	Field holdValue:int
	Field credits:Int
	Field shipWorth:int
	Field repairCost:Int
	Field netWorth:int
	Field averageNetWorth:int
	Field wagesPaid:Int
	Field bounties:Int
	Field surrender:int
	Field insurancePayment:int
	Field insurancePayout:Int
	Field deathBenefitsPaid:int
	Field bill_tally:int
	Method New(ship:Ship, area:Economy, pay:Stack<CreditLog>)
		fuelPrice = area.GetSingleItemValue(Item.FUEL)
		foodPrice = area.GetSingleItemValue(Item.FOOD)
		antimatterPrice = area.GetSingleItemValue(Item.ANTIMATER)
		shipWorth = ship.GetValue(area)
		credits = Player.getCredits()
		holdValue = GetHoldValue(ship, area)
		netWorth = credits + ship.GetValue(area) + GetHoldValue(ship, area, True)
		Local a:Economy = New Economy()
		averageNetWorth = credits + ship.GetValue(a) + GetHoldValue(ship, a, True)
		For Local temp:Int = 0 Until pay.Length()
			Select pay.Get(temp).type
				Case CreditLog.WAGES
					wagesPaid += pay.Get(temp).credits
				Case CreditLog.BOUNTY
					bounties += pay.Get(temp).credits
				Case CreditLog.INSURANCE_PAYMENT
					insurancePayment += pay.Get(temp).credits
				Case CreditLog.INSURANCE_PAYOUT
					insurancePayout += pay.Get(temp).credits
				Case CreditLog.CREW_DEATH
					deathBenefitsPaid += pay.Get(temp).credits
				Case CreditLog.REPAIRS
					repairCost += pay.Get(temp).credits
				Case CreditLog.FOOD_USE
					foodCost += pay.Get(temp).credits
				Case CreditLog.FUEL_USE
					fuelCost += pay.Get(temp).credits
				Case CreditLog.ANTIMATTER_USE
					antimatterCost += pay.Get(temp).credits
					
				Default
					Print "DELTA REPORT FAILED: " + pay.Get(temp).type
			End
		Next
		bill_tally = wagesPaid + bounties + surrender + insurancePayment + insurancePayout + deathBenefitsPaid + repairCost
	End
	Method New(data:JsonObject)
	 	fuelCost = data.GetInt("fuelCost")
		foodCost = data.GetInt("foodCost")
		antimatterCost = data.GetInt("antimatterCost")
		fuelPrice = data.GetInt("fuelPrice")
		foodPrice = data.GetInt("foodPrice")
		antimatterPrice = data.GetInt("antimatterPrice")
		holdValue = data.GetInt("holdValue")
		credits = data.GetInt("credits")
		shipWorth = data.GetInt("shipWorth")
		repairCost = data.GetInt("repairCost")
		netWorth = data.GetInt("netWorth")
		averageNetWorth = data.GetInt("averageNetWorth")
		wagesPaid = data.GetInt("wagesPaid")
		bounties = data.GetInt("bounties")
		surrender = data.GetInt("surrender")
		insurancePayment = data.GetInt("insurancePayment")
		insurancePayout = data.GetInt("insurancePayout")
		deathBenefitsPaid = data.GetInt("deathBenefitsPaid")
		bill_tally = data.GetInt("bill_tally")
	End
	
	Method OnSave:JsonObject()
		Local data:JsonObject = New JsonObject()
		data.SetInt("fuelCost", fuelCost)
		data.SetInt("foodCost", foodCost)
		data.SetInt("antimatterCost", antimatterCost)
		data.SetInt("fuelPrice", fuelPrice)
		data.SetInt("foodPrice", foodPrice)
		data.SetInt("antimatterPrice", antimatterPrice)
		data.SetInt("holdValue", holdValue)
		data.SetInt("credits", credits)
		data.SetInt("shipWorth", shipWorth)
		data.SetInt("repairCost", repairCost)
		data.SetInt("netWorth", netWorth)
		data.SetInt("averageNetWorth", averageNetWorth)
		data.SetInt("wagesPaid", wagesPaid)
		data.SetInt("bounties", bounties)
		data.SetInt("surrender", surrender)
		data.SetInt("insurancePayment", insurancePayment)
		data.SetInt("insurancePayout", insurancePayout)
		data.SetInt("deathBenefitsPaid", deathBenefitsPaid)
		data.SetInt("bill_tally", bill_tally)
		Return data
	End

	Method GetHoldValue:Int(ship:Ship, area:Economy, calculateAll:Bool = False)
		Local holdC:int
		For Local temp:Int = 0 Until ship.items.slots.Length()
			If ship.items.slots[temp] <> Null
				If (ship.items.slots[temp].type <> Item.FUEL And ship.items.slots[temp].type <> Item.FOOD And ship.items.slots[temp].type <> Item.ANTIMATER) Or calculateAll
					holdC += ship.items.slots[temp].GetValue(area)
				End
			End
		Next
		Return holdC
	End
End


Class DeltaReport
	Field fuelCost:Int
	Const D_FUEL:Int = 1
	Field foodCost:Int
	Const D_FOOD:Int = 2
	Field deltaHold:Int '3
	Const D_HOLD:Int = 3
	Field deltaCredits:int '4
	Const D_CREDITS:Int = 4
	Field deltaWorth:int '5
	Const D_WORTH:Int = 5
	Field deltaAverageWorth:Int
	Const D_AVE_WORTH:Int = 6
	Field deltaShipWorth:Int
	Const D_SHIP_WORTH:Int = 7
	Field wagesPaid:Int '8
	Const WAGES_PAID:Int = 8
	Field bounties:int '9
	Const BOUNTIES:Int = 9
	Field surrender:Int '10
	Const SURRENDER:Int = 10
	Field deathBenefitsPaid:Int '11
	Const DEATH_BENEFITS_PAID:Int = 11
	Field repairCost:int '12
	Const REPAIR_COST:Int = 12
	Field insurancePayout:int '13
	Const I_PAYOUT:Int = 13
	Field insurancePayment:int '14
	Const I_PAYMENT:Int = 14
	Field bill_tally:Int
	Const BILL_TALLY:Int = 15
	
	Field ss:Stack<Int>
	
	Method OnSave:JsonArray()
		ss = New Stack<Int>()
		add(D_FUEL, fuelCost)
		add(D_FOOD, foodCost)
		add(D_HOLD, deltaHold)
		add(D_CREDITS, deltaCredits)
		add(D_WORTH, deltaWorth)
		add(D_AVE_WORTH, deltaAverageWorth)
		add(D_SHIP_WORTH, deltaShipWorth)
		add(WAGES_PAID, wagesPaid)
		add(BOUNTIES, bounties)
		add(SURRENDER, surrender)
		add(DEATH_BENEFITS_PAID, deathBenefitsPaid)
		add(REPAIR_COST, repairCost)
		add(I_PAYOUT, insurancePayout)
		add(I_PAYMENT, insurancePayment)
		add(BILL_TALLY, bill_tally)
		
		Local arr:JsonArray = New JsonArray(ss.Length())
		For Local temp:Int = 0 Until ss.Length()
			arr.SetInt(temp, ss.Get(temp))
		Next
		Return arr
	End
	
	Method New(arr:JsonArray)
		For Local temp:Int = 0 Until arr.Length() Step 2
			Local k:Int = arr.GetInt(temp)
			Local v:Int = arr.GetInt(temp + 1)
			Select k
				Case D_FUEL
				    fuelCost = v
				Case D_FOOD
				    foodCost = v
				Case D_HOLD
				    deltaHold = v
				Case D_CREDITS
				    deltaCredits = v
				Case D_WORTH
				    deltaWorth = v
				Case D_AVE_WORTH
				    deltaAverageWorth = v
				Case D_SHIP_WORTH
				    deltaShipWorth = v
				Case WAGES_PAID
				    wagesPaid = v
				Case BOUNTIES
				    bounties = v
				Case SURRENDER
				    surrender = v
				Case DEATH_BENEFITS_PAID
				    deathBenefitsPaid = v
				Case REPAIR_COST
				    repairCost = v
				Case I_PAYOUT
				    insurancePayout = v
				Case I_PAYMENT
				    insurancePayment = v
				Case BILL_TALLY
					bill_tally = v
			End
		Next
	End
	
	Method New(current:ReportItem, last:ReportItem, ship:Ship, area:Economy)
		repairCost = current.repairCost
		surrender = current.surrender
		bounties = current.bounties
		wagesPaid = current.wagesPaid
		deathBenefitsPaid = current.deathBenefitsPaid
		insurancePayment = current.insurancePayment
		foodCost = current.foodCost
		fuelCost = current.fuelCost
		deltaHold = current.holdValue - last.holdValue
		deltaCredits = (current.credits - current.bill_tally) - (last.credits - last.bill_tally)
		deltaShipWorth = (current.shipWorth) - (last.shipWorth)
		deltaWorth = current.netWorth - last.netWorth
		deltaAverageWorth = current.averageNetWorth - last.averageNetWorth
	End
	
	Method add:Void(num:Int, value:Int)
		If value <> 0
			ss.Push(num)
			ss.Push(value)
		End
	End

End