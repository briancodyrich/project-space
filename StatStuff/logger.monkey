Strict
Import space_game


Class Logger Implements Loadable
	Const COMMS:Int = 1
	Const RADAR:Int = 2
	
	Const MAX_LOGS:Int = 1000
	Field logs:Stack<LogItem>
	Method New()
		logs = New Stack<LogItem>()
	End
	Method FileName:String()
		Return "_logs.json"
	End
	
	Method Log:Void(log:LogItem)
		logs.Push(log)
	End
	
	Method Log:Void(type:Int, msg:String)
		logs.Push(New LogItem(type,msg))
	End
	
	Method OnSave:JsonObject()
		Local data:JsonObject = New JsonObject()
		Local arr:JsonArray = New JsonArray(logs.Length() * 2)
		For Local temp:Int = 0 Until logs.Length()
			arr.SetInt(temp * 2, logs.Get(temp).type)
			arr.SetString( (temp * 2) + 1, logs.Get(temp).message)
		Next
		data.Set("logs", arr)
		Return data
	End
	
	Method OnLoad:Void(data:JsonObject)
		logs.Clear()
		Local arr:JsonArray = JsonArray(data.Get("logs"))
		For Local temp:Int = 0 Until arr.Length() Step 2
			logs.Push(New LogItem(arr.GetInt(temp), arr.GetString(temp + 1)))
		Next
	End
	
End

Class LogItem
	Const COMMS:Int = 1
	Const RADAR:Int = 2
	Const BANK:Int = 3
	Const FAILURE:Int = 4
	Const CREW:Int = 5
	Const EVENT:Int = 6
	Const POSITIVE_STANDING:Int = 7
	Const NEGATIVE_STANDING:Int = 8
	Const COMMS_STRING:String = "#00ffff[ COMMS ]"
	Const RADAR_STRING:String = "#ff8634[ RADAR ]"
	Const BANK_STRING:String = "#00dd00[ FINANCE ]"
	Const FAILURE_STRING:String = "#ff0000[ FAILURE ]"
	Const EVENT_STRING:String = "#ffff00[ EVENT ]"
	Const CREW_STRING:String = "#ffcc00[ CREW ]"
	Const UNKNOWN_SOURCE:String = "#ff32b1[ ????? ]"
	Const POSITIVE_STRING:String = "#00ff00[ STANDING ]"
	Const NEGATIVE_STRING:String = "#ff0000[ STANDING ]"
	Field type:Int
	Field message:String
	Method New(type:Int, message:String)
		Self.type = type
		Self.message = message
	End
	
	Method GetType:String()
		If type = COMMS
			Return COMMS_STRING
		End
		If type = RADAR
			Return RADAR_STRING
		End
		If type = BANK
			Return BANK_STRING
		End
		If type = CREW
			Return CREW_STRING
		End
		If type = FAILURE
			Return FAILURE_STRING
		End
		If type = EVENT
			Return EVENT_STRING
		End
		If type = POSITIVE_STANDING
			Return POSITIVE_STRING
		End
		If type = NEGATIVE_STANDING
			Return NEGATIVE_STRING
		End
		Return UNKNOWN_SOURCE 
	End
End