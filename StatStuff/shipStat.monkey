Strict
Import space_game


Class Stat
	Field data:float
	Field delta:float
	Field isFloat:bool
	Method New(isFloat:Bool = False)
		Self.isFloat = isFloat 
	End
	Method Reset:Void()
		data = 0
		delta = 0
	End
	Method Combine:Void()
		data += delta
	End	
	Method Set:Void(num:Float)
		If isFloat
			data = num
		Else
			data = int(num)
		End
	End
	Method SetDelta:Void(num:Float)
		If isFloat
			delta = num
		Else
			delta = int(num)
		End
	End
	Method Set:Void(data:Float, delta:float)
		Set(data)
		SetDelta(delta)
	End
	Method Add:Void(num:Float)
		If isFloat
			data += num
		Else
			data += int(num)
		End
	End
	Method Subtract:Void(num:Float)
		If isFloat
			data -= num
		Else
			data -= int(num)
		End
	End
	Method AddDelta:Void(num:Float, second:float)
		If isFloat
			delta += num
			delta -= second
		Else
			delta += int(num)
			delta -= int(second)
		End
	End
	Method SubtractDelta:Void(num:Float, second:float)
		If isFloat
			delta -= num
			delta += second
		Else
			delta -= int(num)
			delta += int(second)
		End
	End
End

Class ShipStat

	Const N:Int = 1
	Const D:Int = 2
	
	Const HEAT_SINK:int = 0
	Const T_POW_GEN:Int = 1
	Const T_POW_USE:Int = 2
	Const T_HEAT_GEN:Int = 3
	Const T_HEAT_USE:Int = 4
	Const POWER_PER:Int = 5
	Const BACKUP_PER:Int = 6
	Const POWER_CAP_PER:Int = 7
	Const HEAT_PER:Int = 8
	Const REACTOR_POW:Int = 9
	Const REACTOR_HEAT:Int = 10
	Const ENGINE_POW:Int = 11
	Const ENGINE_HEAT:Int = 12
	Const MISC_POW:Int = 13
	Const BATT_POW:Int = 14
	Const BATT_TANK:int = 15
	Const SHIELD_POWER:int = 16
	Const SHIELD_TANK:Int = 17
	Const SHIELD_RECHARGE:Int = 18
	Const HULL_DPS:Int = 19
	Const SHIELD_DPS:Int = 20
	Const CREW_DPS:Int = 21
	Const GUN_POW:Int = 22
	Const GUN_HEAT:Int = 23
	Const COOLANT_TANK:int = 24
	Const CREW:Int = 25
	Const CREW_CAP:Int = 26
	Const ARMOR:Int = 27
	Const SLOTS:Int = 28
	Const HULL:Int = 29
	Const MASS:Int = 30
	Const THRUST:Int = 31
	Const TW:Int = 32
	Const EFFICIENCY:Int = 33
	Const FUEL_USE:Int = 34
	Const HOLD_SIZE:Int = 35
	Const STAT_SIZE:Int = 36

	Field ship:Ship
	Field delta:DeltaItem
	Field data:Stat[] = New Stat[STAT_SIZE]
	Field fuel_mass:int
	
	Method New(ship:Ship, delta:DeltaItem)
		Self.ship = ship
		Self.delta = delta
		data[T_POW_GEN] = New Stat()
		data[T_POW_USE] = New Stat()
		data[T_HEAT_GEN] = New Stat()
		data[T_HEAT_USE] = New Stat()
		data[POWER_PER] = New Stat()
		data[BACKUP_PER] = New Stat()
		data[POWER_CAP_PER] = New Stat()
		data[HEAT_PER] = New Stat()
		data[REACTOR_POW] = New Stat()
		data[REACTOR_HEAT] = New Stat()
		data[ENGINE_POW] = New Stat()
		data[ENGINE_HEAT] = New Stat()
		data[MISC_POW] = New Stat()
		data[BATT_POW] = New Stat()
		data[BATT_TANK] = New Stat()
		data[SHIELD_POWER] = New Stat()
		data[SHIELD_TANK] = New Stat()
		data[SHIELD_RECHARGE] = New Stat(True)
		data[HULL_DPS] = New Stat(True)
		data[SHIELD_DPS] = New Stat(True)
		data[CREW_DPS] = New Stat(True)
		data[GUN_POW] = New Stat()
		data[GUN_HEAT] = New Stat(True)
		data[COOLANT_TANK] = New Stat()
		data[HEAT_SINK] = New Stat()
		data[CREW] = New Stat()
		data[CREW_CAP] = New Stat()
		data[ARMOR] = New Stat()
		data[SLOTS] = New Stat()
		data[HULL] = New Stat()
		data[MASS] = New Stat()
		data[THRUST] = New Stat()
		data[TW] = New Stat(True)
		data[EFFICIENCY] = New Stat(True)
		data[FUEL_USE] = New Stat()
		data[HOLD_SIZE] = New Stat()
		Local i:Item = New Item(ItemLoader.GetItem("Fuel"))
		fuel_mass = i.GetMass()
	End

	Method Update:Void(normalize:bool, deltaShip:ShipStat = Null)
		Local ep:Int = 2
		Local rp:Int = 2
		Local sp:int = 2
		If normalize = False
			ep = ship.enginePower
			rp = ship.reactorPower
			sp = ship.shieldPower
		End
		Reset()
		
		'normal processing
		data[HEAT_SINK].Set(ship.heatRelease)
		data[HOLD_SIZE].Set(ship.storageSpace)
		data[CREW].Set(ship.crew.Length())
		data[CREW_CAP].Set(ship.crewSlots)
		data[HULL].Set(ship.HP)
		data[ARMOR].Set(ship.HP - ship.maxHP)
		data[MASS].Add(ship.GetMass(,,, True))
		data[MASS].AddDelta(0, 0)
		data[SLOTS].Set(ship.systemSlots)
		'Print "start"
		'null item processsing
		If delta <> Null And delta.new_item <> Null And delta.null_delta
			If delta.new_item.type = Component.REACTOR
				Local dc:Reactor = Reactor(delta.new_item.comp.getSelf())
				data[REACTOR_POW].AddDelta( (dc.energyProduction * dc.GetEfficiency()) * GetMult(rp, 1.5), 0)
				data[REACTOR_HEAT].AddDelta( (dc.heatProduction * dc.GetEfficiency(True,, False)) * GetMult(rp, 2.0), 0)
				data[MASS].AddDelta(dc.getMass(), 0)
			End
			If delta.new_item.type = Component.ENGINE
				Local dc:Engine = Engine(delta.new_item.comp.getSelf())
				data[ENGINE_POW].SubtractDelta( (dc.energyProduction * dc.GetEfficiency(True,, False)) * GetMult(ep, 2.0), 0)
				data[ENGINE_HEAT].AddDelta( (dc.heatProduction * dc.GetEfficiency(True,, False)) * GetMult(ep, 2.0), 0.0)
				data[THRUST].AddDelta( (dc.thrust * dc.GetEfficiency()) * GetMult(ep, 2.0), 0.0)
				data[FUEL_USE].AddDelta( (dc.fuelUse * GetFuelMult(ep)), 0.0)
				data[MASS].AddDelta(dc.getMass(), 0)
			'	engine_power_use[1] -= (dc.energyProduction * dc.GetEfficiency(True,, False)) * GetMult(ep, 2.0)
			'	engine_heat[1] += (dc.heatProduction * dc.GetEfficiency(True,, False)) * GetMult(ep, 2.0)
			End
			If delta.new_item.type = Component.BATTERY
				Local dc:Battery = Battery(delta.new_item.comp.getSelf())
				data[BATT_TANK].AddDelta(dc.capacity * dc.GetEfficiency(), 0)
				data[BATT_POW].AddDelta(dc.dischargeRate * dc.GetEfficiency(), 0.0)
				data[MASS].AddDelta(dc.getMass(), 0)
			'	batt_tank[1] += dc.capacity * dc.GetEfficiency()
			'	batt_power[1] += dc.dischargeRate * dc.GetEfficiency()
			End
			If delta.new_item.type = Component.SHIELD
				Local dc:Shield = Shield(delta.new_item.comp.getSelf())
				data[SHIELD_TANK].AddDelta(dc.capacity * dc.GetEfficiency(), 0)
				data[SHIELD_RECHARGE].AddDelta(3.0 / (dc.rechargeRate / float(sp)), 0)
				data[SHIELD_POWER].SubtractDelta( (dc.energyProduction * dc.GetEfficiency(True,, False) * Pow(2, sp - 1)), 0)
				data[MASS].AddDelta(dc.getMass(), 0)
				'shield_tank[1] += dc.capacity * dc.GetEfficiency()
				'shield_recharge[1] += 3.0 / (dc.rechargeRate / float(sp))
			'	shield_power_use[1] -= (dc.energyProduction * dc.GetEfficiency(True,, False) * Pow(2, sp - 1))
			End
			If delta.new_item.type = Item.WEAPON 
				Local gun:Weapon = delta.new_item.weapon
				If gun <> Null
					Local cycle_time:float = gun.cycle_time / 3.0
					data[SHIELD_DPS].AddDelta(gun.boltData.GetInt("shield_damage") / cycle_time, 0)
					data[HULL_DPS].AddDelta( (gun.boltData.GetInt("hull_damage") * gun.boltData.GetInt("fragments")) / cycle_time, 0)
					data[CREW_DPS].AddDelta( (gun.boltData.GetInt("crew_damage") * gun.boltData.GetInt("fragments")) / cycle_time, 0)
					data[GUN_POW].AddDelta(gun.power_needed, 0)
					data[GUN_HEAT].AddDelta(gun.heat_gen / cycle_time, 0)
					data[MASS].AddDelta(gun.mass, 0)
'					shield_dps[1] += gun.boltData.GetInt("shield_damage") / cycle_time
'					hull_dps[1] += (gun.boltData.GetInt("hull_damage") * gun.boltData.GetInt("fragments")) / cycle_time
'					crew_dps[1] += (gun.boltData.GetInt("crew_damage") * gun.boltData.GetInt("fragments")) / cycle_time
'					gun_power_use[1] += gun.power_needed
'					gun_hps[1] += gun.heat_gen / cycle_time
				End
			End
			If delta.new_item.type = Component.COOLANT_TANK
				Local dc:CoolantTank = CoolantTank(delta.new_item.comp.getSelf())
				data[COOLANT_TANK].AddDelta(dc.capacity * dc.GetEfficiency(), 0)
				data[MASS].AddDelta(dc.getMass(), 0)
				'coolant_tank[1] += dc.capacity * dc.GetEfficiency()
			End
			If delta.new_item.type = Component.HEALTH
				Local dc:HealingTube = HealingTube(delta.new_item.comp.getSelf())
				data[MISC_POW].SubtractDelta(dc.energyProduction, 0)
				data[MASS].AddDelta(dc.getMass(), 0)
			'	misc_power_use[0] -= dc.energyProduction
			End
		End
		
'		heat_release[0] = ship.heatRelease
'		crew = ship.crew.Length()
'		crew_cap = ship.crewSlots
'		hull = ship.HP
'		armor = Max(0, (ship.HP - ship.maxHP))
		
		
		For Local temp:Int = 0 Until ship.systems.Length()
			If ship.systems[temp] <> Null
				data[MASS].Add(ship.systems[temp].mass)
			End
		Next
		
		For Local temp:Int = 0 Until ship.comp.Length()
			If ship.comp.Get(temp).getType() = Component.COMPUTER
				Local r:Computer = Computer(ship.comp.Get(temp).getSelf())
				Local mass:Int = r.getMass()
				If r.system <> Null
					mass += r.system.mass
				End
				If delta <> Null And delta.new_item <> Null And delta.old_item <> Null
					#rem
					If delta.new_item.type = r.type And delta.old_item.type = r.type
						If delta.shipx = r.getX() And delta.shipy = r.getY()
							Local dc:Reactor = Reactor(delta.new_item.comp.getSelf())
							data[MASS].AddDelta(dc.getMass(), mass)
							'	engine_power_use[1] -= (dc.energyProduction * dc.GetEfficiency(True,, False)) * GetMult(ep, 2.0)
							'	engine_heat[1] += (dc.heatProduction * dc.GetEfficiency(True,, False)) * GetMult(ep, 2.0)
							'	engine_power_use[1] += pow
							'	engine_heat[1] -= heat
						End
					End
					#end
				End
				data[MASS].Add(mass)
			End
			If ship.comp.Get(temp).getType() = Component.REACTOR
				Local r:Reactor = Reactor(ship.comp.Get(temp).getSelf())
				Local pow:Int = (r.energyProduction * r.GetEfficiency()) * GetMult(rp, 1.5)
				Local heat:Int = (r.heatProduction * r.GetEfficiency(True,, False)) * GetMult(rp, 2.0)
				Local mass:Int = r.getMass()
				If delta <> Null And delta.new_item <> Null And delta.old_item <> Null
					If delta.new_item.type = r.type And delta.old_item.type = r.type
						If delta.shipx = r.getX() And delta.shipy = r.getY()
							Local dc:Reactor = Reactor(delta.new_item.comp.getSelf())
							data[REACTOR_POW].AddDelta( (dc.energyProduction * dc.GetEfficiency()) * GetMult(rp, 1.5), pow)
							data[REACTOR_HEAT].AddDelta( (dc.heatProduction * dc.GetEfficiency(True,, False)) * GetMult(rp, 2.0), heat)
							data[MASS].AddDelta(dc.getMass(), mass)
							'	engine_power_use[1] -= (dc.energyProduction * dc.GetEfficiency(True,, False)) * GetMult(ep, 2.0)
							'	engine_heat[1] += (dc.heatProduction * dc.GetEfficiency(True,, False)) * GetMult(ep, 2.0)
							'	engine_power_use[1] += pow
							'	engine_heat[1] -= heat
						End
					End
				End
				data[REACTOR_POW].Add(pow)
				data[REACTOR_HEAT].Add(heat)
				data[MASS].Add(mass)
			End
				
			If ship.comp.Get(temp).getType() = Component.ENGINE
				Local r:Engine = Engine(ship.comp.Get(temp).getSelf())
				Local pow:Int = (r.energyProduction * r.GetEfficiency(True,, False)) * GetMult(ep, 2.0)
				Local heat:Int = (r.heatProduction * r.GetEfficiency(True,, False)) * GetMult(ep, 2.0)
				Local thrust:Int = (r.thrust * r.GetEfficiency()) * GetMult(ep, 2.0)
				Local use:Int = r.fuelUse * GetFuelMult(ep)
				Local mass:Int = r.getMass()
				If delta <> Null And delta.new_item <> Null And delta.old_item <> Null
					If delta.new_item.type = r.type And delta.old_item.type = r.type
						If delta.shipx = r.getX() And delta.shipy = r.getY()
							Local dc:Engine = Engine(delta.new_item.comp.getSelf())
							data[ENGINE_POW].SubtractDelta( (dc.energyProduction * dc.GetEfficiency(True,, False)) * GetMult(ep, 2.0), pow)
							data[ENGINE_HEAT].AddDelta( (dc.heatProduction * dc.GetEfficiency(True,, False)) * GetMult(ep, 2.0), heat)
							data[THRUST].AddDelta( (dc.thrust * dc.GetEfficiency()) * GetMult(ep, 2.0), thrust)
							data[FUEL_USE].AddDelta(dc.fuelUse * GetFuelMult(ep), use)
							data[MASS].AddDelta(dc.getMass(), mass)
						'	engine_power_use[1] -= (dc.energyProduction * dc.GetEfficiency(True,, False)) * GetMult(ep, 2.0)
						'	engine_heat[1] += (dc.heatProduction * dc.GetEfficiency(True,, False)) * GetMult(ep, 2.0)
						'	engine_power_use[1] += pow
						'	engine_heat[1] -= heat
						End
					End
				End
				data[ENGINE_POW].Subtract(pow)
				data[ENGINE_HEAT].Add(heat)
				data[THRUST].Add(thrust)
				data[FUEL_USE].Add(use)
				data[MASS].Add(mass)
			End
			If ship.comp.Get(temp).getType() = Component.BATTERY
				Local r:Battery = Battery(ship.comp.Get(temp).getSelf())
				Local tank:Int = r.capacity * r.GetEfficiency()
				Local pow:Int = r.dischargeRate * r.GetEfficiency()
				Local mass:Int = r.getMass()
				If delta <> Null And delta.new_item <> Null And delta.old_item <> Null
					If delta.new_item.type = r.type And delta.old_item.type = r.type
						If delta.shipx = r.getX() And delta.shipy = r.getY()
							Local dc:Battery = Battery(delta.new_item.comp.getSelf())
							data[BATT_TANK].AddDelta(dc.capacity * dc.GetEfficiency(), tank)
							data[BATT_POW].AddDelta(dc.dischargeRate * dc.GetEfficiency(), pow)
							data[MASS].AddDelta(dc.getMass(), mass)
'							batt_tank[1] += dc.capacity * dc.GetEfficiency()
'							batt_power[1] += dc.dischargeRate * dc.GetEfficiency()
'							batt_tank[1] -= tank
'							batt_power[1] -= pow
						End
					End
				End
				data[BATT_TANK].Add(tank)
				data[BATT_POW].Add(pow)
				data[MASS].Add(mass)
'				batt_tank[0] += tank
'				batt_power[0] += pow
			End
			If ship.comp.Get(temp).getType() = Component.SHIELD
				Local r:Shield = Shield(ship.comp.Get(temp).getSelf())
				Local tank:Int = r.capacity * r.GetEfficiency()
				Local recharge:Float = 3.0 / (r.rechargeRate / float(sp))
				Local pow:Int = (r.energyProduction * r.GetEfficiency(True,, False) * Pow(2, sp - 1))
				Local mass:Int = r.getMass()
				
				If delta <> Null And delta.new_item <> Null And delta.old_item <> Null
					If delta.new_item.type = r.type And delta.old_item.type = r.type
						If delta.shipx = r.getX() And delta.shipy = r.getY()
							Local dc:Shield = Shield(delta.new_item.comp.getSelf())
							data[SHIELD_TANK].AddDelta(dc.capacity * dc.GetEfficiency(), tank)
							data[SHIELD_RECHARGE].AddDelta(3.0 / (dc.rechargeRate / float(sp)), recharge)
							data[SHIELD_POWER].SubtractDelta( (dc.energyProduction * dc.GetEfficiency(True,, False) * Pow(2, sp - 1)), pow)
							data[MASS].AddDelta(dc.getMass(), mass)
'							shield_tank[1] += dc.capacity * dc.GetEfficiency()
'							shield_recharge[1] += 3.0 / (dc.rechargeRate / float(sp))
'							shield_power_use[1] -= (dc.energyProduction * dc.GetEfficiency(True,, False) * Pow(2, sp - 1))
'							shield_tank[1] -= tank
'							shield_recharge[1] -= recharge
'							shield_power_use[1] += pow
						End
					End
				End
				data[SHIELD_TANK].Add(tank)
				data[SHIELD_RECHARGE].Add(recharge)
				data[SHIELD_POWER].Subtract(pow)
				data[MASS].Add(mass)
'				shield_tank[0] += tank
'				shield_recharge[0] += recharge
'				shield_power_use[0] -= pow
			End
			If ship.comp.Get(temp).getType() = Component.HARDPOINT
				Local r:Hardpoint = Hardpoint(ship.comp.Get(temp).getSelf())
				Local s_dps:float
				Local h_dps:Float
				Local c_dps:Float
				Local pow:Int
				Local hps:float
				Local mass:int
				If r.gun <> Null
					Local cycle_time:float = r.gun.cycle_time / 3.0
					s_dps = r.gun.boltData.GetInt("shield_damage") / cycle_time
					h_dps = (r.gun.boltData.GetInt("hull_damage") * r.gun.boltData.GetInt("fragments")) / cycle_time
					c_dps = (r.gun.boltData.GetInt("crew_damage") * r.gun.boltData.GetInt("fragments")) / cycle_time
					pow = r.gun.power_needed
					hps = r.gun.heat_gen / cycle_time
					mass = r.gun.mass
				End
				
				If delta <> Null And delta.new_item <> Null And delta.old_item <> Null
					If delta.new_item.type = Item.WEAPON And delta.old_item.type = Item.WEAPON
						If delta.shipx = r.getX() And delta.shipy = r.getY()
							Local gun:Weapon = delta.new_item.weapon
							If gun <> Null
								Local cycle_time:float = gun.cycle_time / 3.0
								data[SHIELD_DPS].AddDelta(gun.boltData.GetInt("shield_damage") / cycle_time, s_dps)
								data[HULL_DPS].AddDelta( (gun.boltData.GetInt("hull_damage") * gun.boltData.GetInt("fragments")) / cycle_time, h_dps)
								data[CREW_DPS].AddDelta( (gun.boltData.GetInt("crew_damage") * gun.boltData.GetInt("fragments")) / cycle_time, c_dps)
								data[GUN_POW].AddDelta(gun.power_needed, pow)
								data[GUN_HEAT].AddDelta(gun.heat_gen / cycle_time, hps)
								data[MASS].AddDelta(gun.mass, mass)
'								shield_dps[1] += gun.boltData.GetInt("shield_damage") / cycle_time
'								hull_dps[1] += (gun.boltData.GetInt("hull_damage") * r.gun.boltData.GetInt("fragments")) / cycle_time
'								crew_dps[1] += (gun.boltData.GetInt("crew_damage") * r.gun.boltData.GetInt("fragments")) / cycle_time
'								gun_power_use[1] += gun.power_needed
'								gun_hps[1] += gun.heat_gen / cycle_time
							End
'							shield_dps[1] -= s_dps
'							hull_dps[1] -= h_dps
'							crew_dps[1] -= c_dps
'							gun_power_use[1] -= pow
'							gun_hps[1] -= hps
						End
					End
				End
				If r.gun <> Null
					data[SHIELD_DPS].Add(s_dps)
					data[HULL_DPS].Add(h_dps)
					data[CREW_DPS].Add(c_dps)
					data[GUN_POW].Add(pow)
					data[GUN_HEAT].Add(hps)
					data[MASS].Add(mass)
'					shield_dps[0] += s_dps
'					hull_dps[0] += h_dps
'					crew_dps[0] += c_dps
'					gun_power_use[0] += pow
'					gun_hps[0] += hps
				End
			End
			If ship.comp.Get(temp).getType() = Component.COOLANT_TANK
				Local r:CoolantTank = CoolantTank(ship.comp.Get(temp).getSelf())
				Local tank:Int = r.capacity * r.GetEfficiency()
				Local mass:Int = r.getMass()
				If delta <> Null And delta.new_item <> Null And delta.old_item <> Null
					If delta.new_item.type = r.type And delta.old_item.type = r.type
						If delta.shipx = r.getX() And delta.shipy = r.getY()
							Local dc:CoolantTank = CoolantTank(delta.new_item.comp.getSelf())
							data[COOLANT_TANK].AddDelta(dc.capacity * dc.GetEfficiency(), tank)
							data[MASS].AddDelta(dc.getMass(), mass)
						'	coolant_tank[1] += dc.capacity * dc.GetEfficiency()
						'	coolant_tank[1] -= tank
						End
					End
				End
				data[COOLANT_TANK].Add(tank)
				data[MASS].Add(mass)
				'coolant_tank[0] += tank
			End
			If ship.comp.Get(temp).getType() = Component.HEALTH
				Local r:HealingTube = HealingTube(ship.comp.Get(temp).getSelf())
				Local pow:Int = r.energyProduction
				Local mass:Int = r.getMass()
				If delta <> Null And delta.new_item <> Null And delta.old_item <> Null
					If delta.new_item.type = r.type And delta.old_item.type = r.type
						If delta.shipx = r.getX() And delta.shipy = r.getY()
							Local dc:HealingTube = HealingTube(delta.new_item.comp.getSelf())
							data[MISC_POW].SubtractDelta(dc.energyProduction, pow)
							data[MASS].AddDelta(dc.getMass(), mass)
						End
					End
				End
				data[MISC_POW].Subtract(pow)
				data[MASS].Add(mass)
			End
		Next
		
		
		data[T_POW_GEN].Set(data[REACTOR_POW].data, data[REACTOR_POW].delta)
		data[T_POW_USE].Set(data[MISC_POW].data + data[ENGINE_POW].data + data[SHIELD_POWER].data + data[GUN_POW].data)
		data[T_POW_USE].SetDelta(data[MISC_POW].delta + data[ENGINE_POW].delta + data[SHIELD_POWER].delta + data[GUN_POW].delta)
		
	'	data[POWER_PER].Set( (data[T_POW_USE].data / data[T_POW_GEN].data * 100))
	'	data[POWER_PER].SetDelta( ( ( (data[T_POW_USE].data + data[T_POW_USE].delta) / (data[T_POW_GEN].data + data[T_POW_GEN].delta)) * 100) - data[POWER_PER].data)
		data[POWER_PER].Set(GetPowerPercent(data[T_POW_GEN].data, data[T_POW_USE].data, data[BATT_POW].data))
		data[POWER_PER].SetDelta(GetPowerPercent(data[T_POW_GEN].data + data[T_POW_GEN].delta, data[T_POW_USE].data + data[T_POW_USE].delta, data[BATT_POW].data + data[BATT_POW].delta) - data[POWER_PER].data)
		data[T_HEAT_GEN].Set(data[REACTOR_HEAT].data + data[ENGINE_HEAT].data)
		data[T_HEAT_GEN].SetDelta(data[REACTOR_HEAT].delta + data[ENGINE_HEAT].delta)
		data[HEAT_PER].Set(data[T_HEAT_GEN].data / data[HEAT_SINK].data * 100)
		data[HEAT_PER].SetDelta( ( (data[T_HEAT_GEN].data + data[T_HEAT_GEN].delta) / data[HEAT_SINK].data * 100) - data[HEAT_PER].data)
		data[TW].Set(data[THRUST].data / data[MASS].data)
		data[TW].SetDelta( ( (data[THRUST].data + data[THRUST].delta) / (data[MASS].data + data[MASS].delta) - data[TW].data))
		data[EFFICIENCY].Set(GetEff(data[THRUST].data, data[MASS].data, data[FUEL_USE].data))
		If data[MASS].delta = 0
			data[EFFICIENCY].SetDelta(0)
		Else
			data[EFFICIENCY].SetDelta(GetEff(data[THRUST].data + data[THRUST].delta, data[MASS].data + data[MASS].delta, data[FUEL_USE].data + data[FUEL_USE].delta) - data[EFFICIENCY].data)
		End
		Combine()
	End
	
	Method GetEff:float(t:float, m:float, use:int)
		If t = 0 Return 0
		If m <= 0 Return 0
		Local range:Int = 0
		Local f:Int = ship.fuel
		Local travel:Int = Ship.MOVEMENT_COST
		Local fuel_rods:Int = 0
		fuel_rods += ship.items.CountItem(Item.FUEL)
		While f > 0
			Local power:Int = Int( (t / m) * 100)
			f -= use
			If (f <= 0)
				If (fuel_rods <= 0)
					Return range / float(ship.items.CountItem(Item.FUEL))
				End
				fuel_rods -= 1
				m -= fuel_mass
				f += Ship.FUEL_SIZE
			End
			travel -= power
			If (travel <= 0)
				range += 1
				travel = Ship.MOVEMENT_COST
			End
		Wend
		Return range / float(ship.items.CountItem(Item.FUEL))
	End
	
	Method GetPowerPercent:Int(gen:Int, use:Int, battery:Int)
		If gen = 0 Then Return 200
		If gen >= use
			Return use / float(gen) * 100
		Else
			If battery = 0 Then Return 200
			use -= gen
			Local r:Int = 100 + (use / float(battery) * 100)
			If r > 200 Then Return 200
			Return r
		End
	End
	
	Method GetDelta:Void(deltaShip:ShipStat)
		For Local temp:Int = 0 Until data.Length()
			data[temp].Set(deltaShip.data[temp].data, deltaShip.data[temp].data - data[temp].data)
		Next
	End
	
	Method GetInt:Int(stat:Int, isDelta:bool = False)
		If isDelta
			Return data[stat].delta
		Else
			Return data[stat].data
		End
	End
	Method GetFloat:float(stat:Int, isDelta:bool = False)
		If isDelta
			Return data[stat].delta
		Else
			Return data[stat].data
		End
	End
	
	Method Combine:Void()
		For Local temp:Int = 0 Until data.Length()
			data[temp].Combine()
		Next
'		total_power_gen[0] += total_power_gen[1]
'		total_power_use[0] += total_power_use[1]
'		total_heat_gen[0] += total_heat_gen[1]
'		power_percent[0] += power_percent[1]
'		power_cap_percent[0] += power_cap_percent[1]
'		backup_percent[0] += backup_percent[1]
'		heat_percent[0] += heat_percent[1]
'		reactor_power[0] += reactor_power[1]
'		reactor_heat[0] += reactor_heat[1]
'		engine_power_use[0] += engine_power_use[1]
'		misc_power_use[0] += misc_power_use[1]
'		engine_heat[0] += engine_heat[1]
'		heat_release[0] += heat_release[1]
'		batt_power[0] += batt_power[1]
'		batt_tank[0] += batt_tank[1]
'		shield_tank[0] += shield_tank[1]
'		shield_recharge[0] += shield_recharge[1]
'		shield_power_use[0] += shield_power_use[1]
'		coolant_tank[0] += coolant_tank[1]
'		gun_hps[0] += gun_hps[1]
'		gun_power_use[0] += gun_power_use[1]
'		hull_dps[0] += hull_dps[1]
'		shield_dps[0] += shield_dps[1]
'		crew_dps[0] += crew_dps[1]
	End
	
	Method Reset:Void()
		For Local temp:Int = 0 Until data.Length()
			If data[temp] = Null
				data[temp] = New Stat()
			End
			data[temp].Reset()
		Next
'		total_power_gen =[0, 0]
'		total_power_use = [0,0]
'		total_heat_gen =[0, 0]
'		power_percent =[0, 0]
'		power_cap_percent =[0, 0]
'		heat_percent =[0, 0]
'		reactor_power = [0,0]
'		reactor_heat = [0,0]
'		engine_power_use = [0,0]
'		misc_power_use = [0,0]
'		engine_heat = [0,0]
'		heat_release = [0,0]
'		batt_power = [0,0]
'		batt_tank = [0,0]
'		shield_tank = [0,0]
'		shield_recharge =[0.0, 0.0]
'		shield_power_use = [0,0]
'		coolant_tank = [0,0]
'		gun_hps =[0.0, 0.0]
'		gun_power_use =[0, 0]
'		hull_dps =[0.0, 0.0]
'		shield_dps =[0.0, 0.0]
'		crew_dps =[0.0, 0.0]
	End
	
	Function FormatDelta:String(val:Int, reverse_color:bool, is_percent:bool = False)
		Local r:String
		If val = 0
			Return ""
		Else If reverse_color
			If val > 0
				r = " #ff0000+" + val
			Else
				r = " #00ff00" + val
			End
		Else
			If val > 0
				r = " #00ff00+" + val
			Else
				r = " #ff0000" + val
			End
		End
		If is_percent
			Return r + "%"
		Else
			Return r
		End
	End
	Function FormatDelta:String(val:Float, reverse_color:Bool, sig_figs:int)
		If val = 0
			Return ""
		Else If reverse_color
			If val > 0
				Return " #ff0000+" + SigDig(val, sig_figs)
			Else
				Return " #00ff00" + SigDig(val, sig_figs)
			End
		Else
			If val > 0
				Return " #00ff00+" + SigDig(val, sig_figs)
			Else
				Return " #ff0000" + SigDig(val, sig_figs)
			End
		End
	End
	
	
	Method TestComponent:Void(type:Int, simflag:bool)
	#rem
		For Local temp:Int = 0 Until ship.comp.Length()
			If ship.comp.Get(temp).getType() <> type Then Continue
			If ship.comp.Get(temp).getType() = Component.REACTOR
				Local r:Reactor = Reactor(ship.comp.Get(temp).getSelf())
				reactor_power += (r.energyProduction * r.GetEfficiency(True,, False)) * GetMult(ship.reactorPower, 1.5)
				reactor_heat += (r.heatProduction * r.GetEfficiency(True,, False)) * GetMult(ship.reactorPower, 2.0)
			End
			
			If ship.comp.Get(temp).getType() = Component.ENGINE
				Local r:Engine = Engine(ship.comp.Get(temp).getSelf())
				engine_power += (r.energyProduction * r.GetEfficiency(True,, False)) * GetMult(ship.enginePower, 2.0)
				engine_heat += (r.heatProduction * r.GetEfficiency(True,, False)) * GetMult(ship.enginePower, 2.0)
			End
			If ship.comp.Get(temp).getType() = Component.BATTERY
				Local r:Battery = Battery(ship.comp.Get(temp).getSelf())
				If simflag Then r.simflag = True
				batt_tank += r.capacity * r.GetEfficiency()
				batt_output += r.dischargeRate * r.GetEfficiency()
			End
			If ship.comp.Get(temp).getType() = Component.SHIELD
				Local r:Shield = Shield(ship.comp.Get(temp).getSelf())
				If simflag Then r.simflag = True
				shield_tank += r.capacity * r.GetEfficiency()
				If ship.shieldPower <> 0 And r.isActive()
					shield_time += ( (r.capacity * r.GetEfficiency()) * r.rechargeRate) / float(ship.shieldPower) / 3
				End
			End
			If ship.comp.Get(temp).getType() = Component.COOLANT_TANK
				Local r:CoolantTank = CoolantTank(ship.comp.Get(temp).getSelf())
				If simflag Then r.simflag = True
				coolant_tank += r.capacity * r.GetEfficiency()
			End
			If ship.comp.Get(temp).getType() = Component.HARDPOINT
				Local r:Hardpoint = Hardpoint(ship.comp.Get(temp).getSelf())
				If simflag Then r.simflag = True
				If r.gun <> Null
					gun_power -= r.gun.power_needed
					gun_heat += r.gun.heat_gen / float(r.gun.cycle_time)
				End
			End
			
		Next
	#end
	End
	
End