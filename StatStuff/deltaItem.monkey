Strict

Import space_game

Class DeltaItem
	Const NOTHING:Int = 0
	Const INVENTORY:Int = 1
	Const STORE:Int = 2
	Field old_item:Item
	Field new_item:Item
	Field grab_from:int
	Field null_delta:bool
	Field shipx:Int
	Field shipy:int
	Method New()
		
	End
End