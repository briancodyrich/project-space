Strict

Import space_game


Class SplashScreen extends nScreen
	Field delay:Int = 90
	Field logo:Image

	Method New()
		Init()
	End Method
	
	Method Init:Int()
		logo = LoadImage("SplashIcon.png",, Image.MidHandle)
		Print "Starting..."
		Return 0
	End Method
	
	Method Update:Int()
			delay -= 1
			
			If delay = 0 Then Game.SwitchScreen(Screens.Start)
			
			If delay = 1 Then  'Load really long-loading permanent assets!
				Print("Loading vwidth_fonts...")
			'	Game.vwidth_font = New CSFont("fonts/hightower36",,, 0.75)  '27px
			'	Game.large_font = New CSFont("fonts/hightower72",,, 0.5)  '36px
			
			'	Game.number_font = New CSFont("fonts/trajan62",,, 0.5)  '31 px
			
				Game.white_font = New CSFont("fonts/mp_white_black_border",,, 1.0)
				Game.plain_white_font = New CSFont("fonts/mp_plain_white",,, 1.0)
				Game.gold_font = New CSFont("fonts/mp_golden_black_border",,, 1.0)
			
'			
'				Game.white_font = New CSFont("fonts/hightower72",,, 1.0)
'				Game.plain_white_font = New CSFont("fonts/hightower72",,, 1.0)
'				Game.gold_font = New CSFont("fonts/hightower72",,, 1.0)
				
			'	Game.mp_white_font = New CSFont("fonts/mp_white_black_border",,, 1.0)
			'	Game.mp_gold_font = New CSFont("fonts/mp_golden_black_border",,, 0.5)
			 '
			End If
			
'			If Game.BackKeyHit Then Game.Stop()
			If KeyHit(KEY_F12) or TouchHit(3) Then
				'
			End If
			Return 0
	End Method
	
	Method Render:Int()
		Cls()
		'If we're on IOS, we don't want it to fade in, because we're bringing the game in from another loading screen
		'and we want it to appear like the screen didn't change.
		#If TARGET="ios"
		#Else
			SetAlpha(Min(1.0, (90 - delay) / 30.0))
		#End
		
		SetAlpha(1)
		
		#If CONFIG="debug" 
		#End
		
		Return 0
		
	End Method
End Class