Strict

Import space_game
Import UIStuff.inputbox

	
Class LoadGameScreen Extends nScreen
	Field btnStack:Stack<MenuButton>
	Field deleteStack:Stack<MenuButton>
	Field saves:String[]
	Field deleteName:String
	Method New()
		Init()
		btnStack = New Stack<MenuButton>
		deleteStack = New Stack<MenuButton>
		saves = Loader.ListSaves()
		For Local count:Int = 0 Until saves.Length()
			Local del:MenuButton = New MenuButton(ScreenWidth / 2 + 90, 500 + (count * 60), 160, 56)
			del.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
			Local btn:MenuButton = New MenuButton(ScreenWidth / 2 - 90, 500 + (count * 60), 160, 56)
			btn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
			btnStack.Push(btn)
			deleteStack.Push(del)
		Next
		
		Player = New PlayerData(Seed, "")
		Player.ship = New Ship(100, 100, False)
	End
	Method Init:Int()

		Return 0
	End
	
	Method Update:Int()
		If Game.popup <> Null And not Game.popup.isScreenFrozen() And Game.popup.isActiveMessage(deleteName)
			Print Game.popup.getMessageCode()
			If Game.popup.getMessageCode() = Popup.OK
				Loader.DeleteSave(deleteName)
				Game.popup = Null
				If Loader.ListSaves().Length() > 0
					Game.DoSwitch(Screens.LoadGame)
				Else
					Game.DoSwitch(Screens.NewGame)
				End
			End
			deleteName = ""
			Game.popup = Null
		End
	
	
		For Local temp:Int = 0 Until btnStack.Length()
			Local btn:MenuButton = btnStack.Get(temp)
			btn.Poll()
			If btn.hit
				Player.accountName = saves[temp]
				Player.LoadGame()
				If Player.ship.isNull() Or Player.ship.isDead() 'map flag
					Player.map.SkipToDocking()
					Player.accounting.delta = Player.accounting.history.Get(Player.accounting.history.Length() -1)
					Game.SetRootScreen(Screens.SystemMap)
					Game.SwitchScreen(Screens.Dock)
				Else If Player.isDocked
					Player.map.SkipToDocking()
					Game.SetRootScreen(Screens.SystemMap)
				Else
					Game.SwitchScreen(Screens.SystemMap)
				End
				
			End
			
			Local del:MenuButton = deleteStack.Get(temp)
			del.Poll()
			If del.hit
				Local message:Stack<String>
				message = New Stack<String>
				message.Push("Are you sure you want to delete this?")
				deleteName = saves[temp]
				Game.popup = New Popup(deleteName, message, "Yes", Popup.OK,,, "Cancel", Popup.CANCEL)
			End
		Next
		
		If Game.Keys.KeyHit(KEY_ESCAPE)
			Game.SwitchScreen(Screens.Root)
		End

		Return 0
	End
	
	Method Render:Int()
		For Local temp:Int = 0 Until btnStack.Length()
			Local btn:MenuButton = btnStack.Get(temp)
			Local del:MenuButton = deleteStack.Get(temp)
			btn.RenderScaled(0, 0, 0.75)
			del.RenderScaled(0, 0, 0.75)
			Game.white_font.Draw(saves[temp], btn.x + btn.w / 2, btn.y + btn.h / 6, 0.5)
			Game.white_font.Draw("Delete", del.x + del.w / 2, del.y + del.h / 6, 0.5)
		Next
		Return 0
		
	End

End
