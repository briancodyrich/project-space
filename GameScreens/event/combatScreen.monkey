Strict

Import space_game
Import UIStuff.hotbar


	
Class CombatScreen Extends nScreen
	Const UPDATE_SPEED:Int = 20
	Const DISPLAY_VALUES:Bool = False
	Field updateCount:int
	Field shipTest:Ship
	Field enemyShip:Ship
'	Field shipviewBtn:MenuButton
'	Field powerviewBtn:MenuButton
'	Field heatviewBtn:MenuButton
	'Field oxygenViewBtn:MenuButton
'	Field hullViewBtn:MenuButton
'	Field damageViewBtn:MenuButton
	Field crewStatus:CrewStatusSmall
	
	Field exitBtn:MenuButton
	
	Field offlineToggleBtn:MenuButton
	Field surrenderIgnored:bool
	
	Field compGrab:ComponentInterface
	Field crewGrab:Crew
	
	
	Field view:int = Tile.NORMAL_VIEW
	
	Field boarding_status:Int
	
	Field deathCounter:Int
	
	
	Method New()
		Game.SetRootScreen(Screens.TestGame)
		If Player <> Null Then Player.paused = True
		shipTest = New Ship(200, 100, False) 'was 100
		If sandBoxShip <> Null
			Player = New PlayerData(0, "")
			Player.paused = true
			shipTest.OnLoad(sandBoxShip.OnSave())
			enemyShip = New Ship(1220, 100, True)
			enemyShip.OnLoad(sandBoxShip.OnSave())
			Player.ship = shipTest
			Player.panel = New ShipPanel()
		Else If Player.ship.targetShip <> Null
			Player.map.hexField.locked = False
			shipTest = Player.ship
			enemyShip = Player.ship.targetShip
			Print "logging battle"
			Settings.startLog()
		Else
			Player.map.hexField.locked = False
			shipTest = Player.ship
			enemyShip = Player.map.hexField.grabObject.ship
		End
		shipTest.targetShip = enemyShip
		enemyShip.targetShip = shipTest
		shipTest.alerts = New AlarmManager()
		shipTest.CombatReset()
		enemyShip.CombatReset()
		Player.panel.UpdateHotbar()
		If sandBoxShip <> Null
			For Local temp:Int = 0 Until shipTest.comp.Length()
				If shipTest.comp.Get(temp).getType() = Component.HEALTH
					shipTest.crew.Push(New Crew(shipTest.comp.Get(temp).getX(), shipTest.comp.Get(temp).getY(), shipTest, RaceLoader.GetRace("ANY", "ANY", "PLAYABLE")))
				End
			Next
			shipTest.AiStart()
			shipTest.aiControlled = False
			shipTest.enginePower = Ship.NORMAL
			shipTest.reactorPower = Ship.NORMAL
			shipTest.shieldPower = Ship.NORMAL
		End
		
		If enemyShip.crew.IsEmpty()
			enemyShip.BuildCrew()
		End
		
		enemyShip.AiStart()
		
		
		offlineToggleBtn = New MenuButton(1000, 750, 160, 56)
		offlineToggleBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		
		#rem
		shipviewBtn = New MenuButton(0, 0, 160, 56)
		shipviewBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		
		powerviewBtn = New MenuButton(170, 0, 160, 56)
		powerviewBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		heatviewBtn = New MenuButton(340, 0, 160, 56)
		heatviewBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		oxygenViewBtn = New MenuButton(510, 0, 160, 56)
		oxygenViewBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		hullViewBtn = New MenuButton(680, 0, 160, 56)
		hullViewBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		damageViewBtn = New MenuButton(850, 0, 160, 56)
		damageViewBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		#end
		exitBtn = New MenuButton(1020, 0, 160, 56)
		exitBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		Player.panel.SetMode(ShipPanel.COMBAT_MODE)
		crewStatus = New CrewStatusSmall(0, 100, 600, 150, shipTest.crew)
	'	sandboxBtn = New MenuButton(452, 0, 226, 74)
	'	sandboxBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
	End
	Method Init:Int()
		Return 0
	End

	Method Update:Int()
	
		crewStatus.Update()
		If Player.ship.isDead()
			deathCounter += 1
			Player.quests.removeQuest(Player.quests.currentQuest)
		End
		If deathCounter >= 1100
			Player.ship.alerts = Null
			Player.map.deathFlag = True
			Player.map.GotoPlanet()
			Game.SwitchScreen(Screens.Dock)
			deathCounter = 0
		End
		If Game.popup <> Null And not Game.popup.isScreenFrozen() And Game.popup.isActiveMessage("Surrender")
			Print Game.popup.getMessageCode()
			If Game.popup.getMessageCode() = Popup.OK
				Game.popup = Null
				enemyShip.surrenderAccepted = True
				Player.addCredits(enemyShip.surrender_reward)
				Player.chat.Log(LogItem.BANK, enemyShip.call_sign + " has transfered " + FormatCredits(enemyShip.surrender_reward))
			End
			surrenderIgnored = True
			Game.popup = Null
		End
		If enemyShip.hasSurrendered And Not surrenderIgnored
			If enemyShip.surrender_reward > 0
				Local message:Stack<String>
				message = New Stack<String>
				message.Push("Accept Surrender?")
				message.Push("Offer: " + FormatCredits(enemyShip.surrender_reward))
				Game.popup = New Popup("Surrender", message, "Accept", Popup.OK,,, "Ignore", Popup.CANCEL)
			Else
				surrenderIgnored = True
			End
			
		End
	'	Print "play: " + shipTest.heatRelease
	'	Print "othe: " + enemyShip.heatRelease
		Player.panel.Update()
		If compGrab <> Null
			offlineToggleBtn.Poll()
			If offlineToggleBtn.hit
				compGrab.setState( not compGrab.isOnline())
			End
		End
		If not Game.Swap.boltStack.IsEmpty()
			Repeat
				Local dat:Int = Game.Swap.boltStack.Pop()
				shipTest.SubtractBolts(dat)
				If enemyShip <> Null
					enemyShip.SubtractBolts(dat)
				End
			Until (Game.Swap.boltStack.IsEmpty())
		End
		
		If Not Player.paused Then Game.OverParticle.Update()
		If Not Player.paused Then Game.UnderParticle.Update()
		#rem
		shipviewBtn.Poll()
		If shipviewBtn.hit
			view = Tile.NORMAL_VIEW
		End
		
		powerviewBtn.Poll()
		If powerviewBtn.hit
			view = Tile.ELECTRIC_VIEW
		End
		heatviewBtn.Poll()
		If heatviewBtn.hit
			view = Tile.HEAT_VIEW
		End
		oxygenViewBtn.Poll()
		If oxygenViewBtn.hit
			view = Tile.OXYGEN_VIEW
		End
		hullViewBtn.Poll()
		If hullViewBtn.hit
			view = Tile.HULL_VIEW
		End
		damageViewBtn.Poll()
		If damageViewBtn.hit
			view = Tile.DAMAGE_VIEW
			shipTest.ExtendWalkway()
		End
		#end
		If Player.paused = False
			shipTest.Update()
			If enemyShip <> Null
				enemyShip.Update()
			End
		End
		
		If Game.Keys.KeyHit(KEY_SPACE)
			If Player.paused = True Then Player.paused = False Else Player.paused = True
			Print "start " + Millisecs()
		End
		
		If Game.Keys.KeyHit(KEY_P)
			If shipTest.aiControlled = True
				shipTest.aiControlled = False
			Else
				shipTest.AiStart()
			End
		End
		
		If compGrab <> Null
			compGrab.UpdateStatus()
		
		End
		
		If Game.Keys.KeyHit(KEY_ENTER)
			For Local x:Int = 0 Until Ship.MAX_X
				For Local y:Int = 0 Until Ship.MAX_Y
					If shipTest.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
						'shipTest.shield_overlay.NewImpact(x, y)
						
						Local temp:Weapon = New Weapon(ItemLoader.GetItem("Basic Laser"))
						shipTest.TakeDamage(New Bolt(x, y, temp.boltData, shipTest.areShieldsActive()))
					End
				Next
			Next
		End
		
		If Game.Keys.KeyHit(KEY_I)
			shipTest.taskmanager.PrintAllTasks()
		End
		
		
		If Game.Keys.KeyHit(KEY_H)
			If shipTest.dumpHeat = True Then shipTest.dumpHeat = False Else shipTest.dumpHeat = True
		End
		
	
		If Game.Cursor.Hit
			For Local temp:Int = 0 Until shipTest.comp.Length()
				If shipTest.comp.Get(temp).isMouseOver(shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
					compGrab = shipTest.comp.Get(temp)
					compGrab.UpdateStatus()
				End
			Next
			For Local temp:Int = 0 Until shipTest.crew.Length()
				If shipTest.crew.Get(temp).isMouseOver() Then crewGrab = shipTest.crew.Get(temp)
			Next
			For Local x:Int = 0 Until Ship.MAX_X
				For Local y:Int = 0 Until Ship.MAX_Y
					If shipTest.pipes[x * Ship.MAX_X + y].door or shipTest.pipes[x * Ship.MAX_X + y].airlock
						If shipTest.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
							If shipTest.pipes[x * Ship.MAX_X + y].doorOpen = True
								shipTest.pipes[x * Ship.MAX_X + y].doorOpen = False
							Else
								shipTest.pipes[x * Ship.MAX_X + y].doorOpen = True
							End
						End
					End
					#rem
					If shipTest.isBoarding
						If enemyShip.pipes[x * Ship.MAX_X + y].door or enemyShip.pipes[x * Ship.MAX_X + y].airlock
							If enemyShip.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, enemyShip.x_offset, enemyShip.y_offset, enemyShip.mirrored)
								If enemyShip.pipes[x * Ship.MAX_X + y].doorOpen = True
									enemyShip.pipes[x * Ship.MAX_X + y].doorOpen = False
								Else
									enemyShip.pipes[x * Ship.MAX_X + y].doorOpen = True
								End
							End
						End
					End
					#end
				Next
			Next
		End
		
		If Game.Keys.KeyHit(KEY_ESCAPE) And Game.LastRootScreenNumber = Screens.Editor
			Game.SwitchScreen(Screens.Editor)
			Return 0
		End
		
		If enemyShip.isDead()
			If Game.Keys.KeyHit(KEY_ESCAPE)
				Player.ship.HealShip(False)
				If enemyShip.surrenderAccepted
					Game.SwitchScreen(Screens.SystemMap)
				Else
					Game.SwitchScreen(Screens.EndCombat)
				End
			End
			exitBtn.Poll()
			If exitBtn.hit
				Player.ship.HealShip(False)
				If enemyShip.surrenderAccepted
					Game.SwitchScreen(Screens.SystemMap)
				Else
					Game.SwitchScreen(Screens.EndCombat)
				End
			End
		End
		
		If Game.Keys.KeyHit(KEY_END)
			enemyShip.crew.Clear()
			Settings.leftEarly()
		End
		
		If Game.Keys.KeyHit(KEY_HOME)
			enemyShip.hasSurrendered = True
			Settings.leftEarly()
		End
		

		
		Return 0
	End
	
	
	Method Render:Int()
		If view = Tile.NORMAL_VIEW Then Game.UnderParticle.Render()
		#rem
		shipviewBtn.RenderScaled(0, 0, 0.75)
		
		Game.white_font.Draw("Normal", shipviewBtn.x + shipviewBtn.w / 2, shipviewBtn.y + shipviewBtn.h / 6, 0.5)
		powerviewBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Power", powerviewBtn.x + powerviewBtn.w / 2, powerviewBtn.y + powerviewBtn.h / 6, 0.5)
		heatviewBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Coolant", heatviewBtn.x + heatviewBtn.w / 2, heatviewBtn.y + heatviewBtn.h / 6, 0.5)
		oxygenViewBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Oxygen", oxygenViewBtn.x + oxygenViewBtn.w / 2, oxygenViewBtn.y + oxygenViewBtn.h / 6, 0.5)
		damageViewBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Damage", damageViewBtn.x + damageViewBtn.w / 2, damageViewBtn.y + damageViewBtn.h / 6, 0.5)
		hullViewBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Hull", hullViewBtn.x + hullViewBtn.w / 2, hullViewBtn.y + hullViewBtn.h / 6, 0.5)
		#end
		If enemyShip.isDead()
			exitBtn.RenderScaled(0, 0, 0.75)
			Game.white_font.Draw("Exit", exitBtn.x + exitBtn.w / 2, exitBtn.y + exitBtn.h / 6, 0.5)
		End
		
		'no draw zon
		
		shipTest.Render(view, Player.paused)
		'shipTest.DrawOutline()
		enemyShip.Render(view, Player.paused)
		'enemyShip.DrawOutline()
		If Player.paused
			Game.white_font.Draw("Paused", 950, 30)
		End
			#rem
			Local i:Incrementer = New Incrementer(130, 40)
			Game.white_font.Draw("Tasks: " + shipTest.taskmanager.GetJobCount(), 950, i.getNext())
			Game.white_font.Draw("events: " + shipTest.taskmanager.GetPermCount(), 950, i.getNext())
			Game.white_font.Draw("Shields: " + AIChoice.shield_value, 950, i.getNext())
			Game.white_font.Draw("Quit: " + enemyShip.surrenderTimer, 950, i.getNext())
			#end
		
		
		
		
		
		If enemyShip <> Null
			Game.white_font.Draw("ACC: " + (100 - enemyShip.GetDodge() +shipTest.GetAccuracyBonus(0, 0)), 800, 790)
			Game.white_font.Draw("EVA: " + (shipTest.GetDodge() -enemyShip.GetAccuracyBonus(0, 0)), 800, 820)
		End
		
		If crewGrab <> Null
			If crewGrab.order_x <> - 1
				SetColor(0, 255, 0)
				DrawRectOutline( (crewGrab.order_x * Ship.GRID_SIZE) + crewGrab.ship.x_offset, (crewGrab.order_y * Ship.GRID_SIZE) + crewGrab.ship.y_offset, 30, 30)
				ResetColorAlpha()
			End
			If crewGrab.dead
				crewGrab = Null
			Else
				#rem
				Local s:Stack<String> = crewGrab.getStatus()
				Local space:Int = 400
				For Local temp:Int = 0 Until s.Length()
					Game.white_font.Draw(s.Get(temp), 800, space)
					space += 30
				Next
				#end
			End
			If Game.Cursor.RightHit()
				For Local x:Int = 0 Until Ship.MAX_X
					For Local y:Int = 0 Until Ship.MAX_Y
						If shipTest.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
							crewGrab.order_x = x
							crewGrab.order_y = y
						End
					Next
				Next
			End
		End
		If compGrab <> Null
			For Local x:Int = 0 Until compGrab.getStatus().Length()
				Game.white_font.Draw(compGrab.getStatus().Get(x), 1200, 750 + (x * 30))
				offlineToggleBtn.RenderScaled(0, 0, 0.75)
				If compGrab.isOnline()
					Game.white_font.Draw("Deactivate", offlineToggleBtn.x + offlineToggleBtn.w / 2, offlineToggleBtn.y + offlineToggleBtn.h / 6, 0.5)
				Else
					Game.white_font.Draw("Activate", offlineToggleBtn.x + offlineToggleBtn.w / 2, offlineToggleBtn.y + offlineToggleBtn.h / 6, 0.5)
				End
				
			Next
		End
		
		crewStatus.Render()
		
	'	'DrawRectOutline(shipTest.x_offset, shipTest.y_offset, shipTest.MAX_X * shipTest.GRID_SIZE, shipTest.MAX_Y * shipTest.GRID_SIZE)
	'	DrawRectOutline(1220, shipTest.y_offset, shipTest.MAX_X * shipTest.GRID_SIZE, shipTest.MAX_Y * shipTest.GRID_SIZE)
		
		Player.panel.Render()
	
	
		If view = Tile.NORMAL_VIEW Then Game.OverParticle.Render()
		
		
		
		If Game.showFPS
			DrawFPS()
		End
		Return 0
	
	End
End











