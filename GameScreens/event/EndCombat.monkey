Strict

Import space_game
Import UIStuff.inventorymanager
Import UIStuff.storage
	
Class EndCombatScreen Extends nScreen

	Const PERCENT_ITEM_CHANCE:Int = 5
	Field grab:GrabItem
	Field mapBtn:MenuButton
	'Field shipTest:Ship
	Field grid:InventoryManager
	Field store:Storage
	Method New()
		grab = New GrabItem()
		mapBtn = New MenuButton(0, 0, 160, 56)
		mapBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		Player.ship.alerts = Null
		Local quest:Quest = Player.quests.getQuest(Player.quests.currentQuest)
		If quest <> Null
			Player.rewardQuest(quest)
		End
		Player.quests.currentQuest = 0
		Local enemyShip:Ship = Player.ship.targetShip
		Local bounty:Int = enemyShip.bounty_reward
		If bounty > 0
			Player.accounting.LogCredits(CreditLog.BOUNTY, bounty, CreditLog.PAY_LATER)
			Player.chat.Log(LogItem.BANK, enemyShip.call_sign + " has a " + FormatCredits(bounty) + " bounty")
		End
		Local t:Inventory = New Inventory(30)
		If enemyShip.items.slots.Length() > 0
			For Local temp:Int = 0 Until enemyShip.items.slots.Length()
				If enemyShip.items.slots[temp] <> Null
					t.GiveItem(enemyShip.items.slots[temp].id, Rnd(enemyShip.items.slots[temp].count + 1))
				End
			Next
		End
		If Player.ship.targetShip.comp.Length() > 0
			For Local temp:Int = 0 Until Player.ship.targetShip.comp.Length()
				If Rnd(100) < PERCENT_ITEM_CHANCE
					For Local tt:Int = 0 Until t.slots.Length()
						If t.slots[tt] = Null
							Print Player.ship.targetShip.comp.Get(temp).getId()
							If Player.ship.targetShip.comp.Get(temp).getType() = Component.HARDPOINT
								t.slots[tt] = New Item(ItemLoader.GetItem(Hardpoint(Player.ship.targetShip.comp.Get(temp).getSelf()).gun.id), 0)
							Else If Player.ship.targetShip.comp.Get(temp).getType() = Component.COMPUTER
								t.slots[tt] = Computer(Player.ship.targetShip.comp.Get(temp).getSelf()).system
							Else
								t.slots[tt] = New Item(ItemLoader.GetItem(Player.ship.targetShip.comp.Get(temp).getId()), Player.ship.targetShip.comp.Get(temp).getPermDamage(), Player.ship.targetShip.comp.Get(temp).getQualityLevel())
							End
							Exit
						End
					Next
				Else If Rnd(100) < PERCENT_ITEM_CHANCE
					t.GiveItem("Spare Parts", 1)
				End
			Next
		End
		Player.ship.targetShip = Null
		Local pd:PassData = New PassData(PassData.INVENTORY, PassData.STORE)
		grid = New InventoryManager(100, 100, 5, 6, pd, Player.ship.items, grab)
		pd = New PassData(PassData.STORE, PassData.INVENTORY)
		store = New Storage(900, 100, 5, 6, pd, t, grab, Storage.STORAGE_CHEST)
	End
	Method Init:Int()
		Return 0
	End
	
	
	Method Update:Int()
	
	
		'this order is important
		store.Update()
		grid.Update()
		grab.Update()
		'this order is important
		
		mapBtn.Poll()
		If mapBtn.hit
			If grab.item <> Null
				grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.INVENTORY))
				grid.Update()
				If grab.success <> True
					grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.STORE))
					store.Update()
				End
			End
			Player.SaveGame()
			Game.SwitchScreen(Screens.SystemMap)
		End
		If Game.Keys.KeyHit(KEY_ESCAPE)
			If grab.item <> Null
				grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.INVENTORY))
				grid.Update()
				If grab.success <> True
					grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.STORE))
					store.Update()
				End
			End
			Player.SaveGame()
			Game.SwitchScreen(Screens.LastRoot)
		End
		Return 0
	End
	
	
	Method Render:Int()
		mapBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Map", mapBtn.x + mapBtn.w / 2, mapBtn.y + mapBtn.h / 6, 0.5)
		
		store.Render()
		grid.Render()
		grab.Render()
		If Game.showFPS
			DrawFPS()
		End
		Return 0
	
	End
End
