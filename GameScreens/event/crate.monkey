Strict

Import space_game
Import UIStuff.inventorymanager
Import UIStuff.storage

	
Class CrateScreen Extends nScreen

	Const PERCENT_ITEM_CHANCE:Int = 5
	Field grab:GrabItem
	Field mapBtn:MenuButton

	'Field shipTest:Ship
	Field grid:InventoryManager
	Field store:Storage
	Method New()
		grab = New GrabItem()
		mapBtn = New MenuButton(0, 0, 160, 56)
		mapBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		Local t:Inventory = New Inventory(30)
		Local counter:Int = 0
		If Player.lootData <> Null
			Local arr:JsonArray = JsonArray(Player.lootData.Get("drops", Null))
			If arr <> Null
				For Local temp:Int = 0 Until arr.Length()
					Local obj:JsonObject = JsonObject(arr.Get(temp))
					If counter < t.slots.Length()
						If obj <> Null
							If Rnd(100) < obj.GetInt("odds", 0)
								Select obj.GetString("type", "NULL").ToUpper()
									Case "PARTS"
										Local i:Item = Player.map.hexField.lList.GetItem(LeveledList.PART_STORE, False)
										If i <> Null
											If i.count < i.max
												i.count = Clamp(Int(Rnd(i.max) + 1), 1, i.max)
											End
											t.slots[counter] = i
											counter += 1
										End
								End
							End
						End
					End

				Next
			End
		End
		Local pd:PassData = New PassData(PassData.INVENTORY, PassData.STORE)
		grid = New InventoryManager(100, 100, 5, 6, pd, Player.ship.items, grab)
		pd = New PassData(PassData.STORE, PassData.INVENTORY)
		store = New Storage(900, 100, 5, 6, pd, t, grab, Storage.STORAGE_CHEST)
		Player.ship.targetShip = Null
		Player.lootData = Null
	End
	Method Init:Int()
		Return 0
	End
	
	
	Method Update:Int()
	
	
		'this order is important
		store.Update()
		grid.Update()
		grab.Update()
		'this order is important
		
		mapBtn.Poll()
		If mapBtn.hit
			If grab.item <> Null
				grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.INVENTORY))
				grid.Update()
				If grab.success <> True
					grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.STORE))
					store.Update()
				End
			End
			Player.SaveGame()
			Game.SwitchScreen(Screens.SystemMap)
		End
		If Game.Keys.KeyHit(KEY_ESCAPE)
			If grab.item <> Null
				grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.INVENTORY))
				grid.Update()
				If grab.success <> True
					grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.STORE))
					store.Update()
				End
			End
			Player.SaveGame()
			Game.SwitchScreen(Screens.Root)
		End
		Return 0
	End
	
	
	Method Render:Int()
		mapBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Map", mapBtn.x + mapBtn.w / 2, mapBtn.y + mapBtn.h / 6, 0.5)
		
		store.Render()
		grid.Render()
		grab.Render()
		If Game.showFPS
			DrawFPS()
		End
		Return 0
	
	End
End
