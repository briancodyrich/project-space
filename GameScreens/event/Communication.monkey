Strict

Import space_game
	
Class CommsScreen Extends nScreen
	
	Const TEXT_START_X:Int = 500
	Const TEXT_START_Y:Int = 150
	Const TEXT_SPACING:Int = 30
	Const BUTTON_START:Int = 0
	Const BUTTON_WIDTH:Int = 160
	Const BUTTON_HEIGHT:Int = 56
	Const BUTTON_SPACING:Int = 170
	
	Const SHIP_FOLDER:String = "Strings/Ships/"
	
	Const EXIT_TIME:Int = 600
	
	Field exit_timer:Int = 0
	Field enemyShip:Ship
	Field talks:Stack<TextEffect>
	Field btn:Stack<MenuButton>
	Field btnEffect:Stack<String>
	Field btnText:Stack<String>
	Field screenLocked:bool
	
	Method New()
		talks = New Stack<TextEffect>()
		btn = New Stack<MenuButton>()
		btnEffect = New Stack<String>()
		btnText = New Stack<string>()
	
		Player.interaction.HandShake()
		Player.SaveGame()
		
		'script = ScriptManager.GetScript(Player.interaction.path)
		
			#rem

			For Local temp:int = 0 Until script.options.Length()
				Local t:MenuButton = New MenuButton(BUTTON_START + (BUTTON_SPACING * temp), 0, BUTTON_WIDTH, BUTTON_HEIGHT)
				t.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
				btn.Push(t)
				btnText.Push(JsonObject(script.options.Get(temp)).GetString("text", "MISSING TEXT"))
				btnEffect.Push(JsonObject(script.options.Get(temp)).GetString("effect", "NULL"))
			Next
			#end
			
		'Can get stuck in here if you are waiting, hmm
		While Player.interaction.HasScript()
			If Run(Player.interaction.GetScript())
				Player.interaction.DestroyScript()
			End
		Wend
		If Player.quests.currentQuest <> 0
			Local q:Quest = Player.quests.getQuest(Player.quests.currentQuest)
			If q <> Null
				If q.ship <> Null
					Print "not null"
					enemyShip = New Ship(1220, 100, True)
					enemyShip.OnLoad(q.ship.OnSave())
					enemyShip.targetShip = Player.ship
					Player.ship.targetShip = enemyShip
					enemyShip.canSurrender = False
					enemyShip.bounty_reward = 0
				End
			End
		End
	End
	Method Init:Int()
		Return 0
	End
	
	Method Update:Int()
		'shit for running scripts
		While Player.interaction.HasScript()
			If Run(Player.interaction.GetScript())
				Player.interaction.DestroyScript()
			End
		Wend
		
		If Game.Keys.KeyHit(KEY_ESCAPE) And screenLocked = False
			Game.SwitchScreen(Screens.Root)
		End
		
		If Game.Keys.KeyHit(KEY_END)
			Game.SwitchScreen(Screens.Root)
		End
		For Local temp:Int = 0 Until btn.Length()
			btn.Get(temp).Poll()
			If btn.Get(temp).hit
				Player.interaction.LoadScript(btnEffect.Get(temp))
				btnEffect.Set(temp, "stdlib_null.ess")
			End
		Next
		For Local temp:Int = 0 Until talks.Length()
			If temp = 0 Or talks.Get(temp - 1).done
				talks.Get(temp).Update()
			End
		Next
		
		#rem
		Select effect
			Case "FIGHT"
				Player.interaction.ResetEvent()
				Player.map.hexField.ClearCurrent()
				Game.SwitchScreen(Screens.TestGame)
			Case "EXIT"
				Player.interaction.ResetEvent()
				Player.map.hexField.ClearCurrent()
				Game.SwitchScreen(Screens.SystemMap)
			Case "SEARCH"
				If Player.interaction.extra = "BOMB"
					Local ft:Script = ScriptManager.GetScript("Poi/explosion.json")
					For Local temp:Int = 0 Until ft.text.Length()
						talks.Push(New TextEffect(TEXT_START_X, TEXT_START_Y + (TEXT_SPACING * talks.Length()), TextEffect.TYPE_RAND, ft.text.Get(temp)))
					Next
					DamagePlayer(, True)
				Else
					Game.SwitchScreen(Screens.Crate)
				End
			Case "RUN"
				Local v:Float = GetSigmoid(Player.ship.GetThrust(, True) - Player.ship.targetShip.GetThrust(, True), MIN_SPEED, MAX_SPEED) * 100
				If Player.ship.GetThrust(, True) = 0 Then v = 0
				If v > Rnd(100)
					Local ft:Script = ScriptManager.GetScript("Pirates/escape_success.json")
					For Local temp:Int = 0 Until ft.text.Length()
						talks.Push(New TextEffect(TEXT_START_X, TEXT_START_Y + (TEXT_SPACING * talks.Length()), TextEffect.TYPE_RAND, ft.text.Get(temp)))
					Next
					exit_timer = -1
				Else
					Local ft:Script = ScriptManager.GetScript("Pirates/escape_fail.json")
					For Local temp:Int = 0 Until ft.text.Length()
						talks.Push(New TextEffect(TEXT_START_X, TEXT_START_Y + (TEXT_SPACING * talks.Length()), TextEffect.TYPE_RAND, ft.text.Get(temp)))
					Next
					DamagePlayer(Component.ENGINE)
				End
			Case "NULL"
				
			Default
				ThrowErrorMessage("Option not found", effect)
		End
		#end
		If ScriptManager.DEBUG_CHOOSE
			ScriptManager.DebugUpdate()
		End
		Return 0
	End
	Method Run:Bool(s:Script)
	'Local d:String[] = LoadDir(CurrentDir() +
		Select s.key
			Case Script.LOAD_SHIP
				Local jShip:JsonObject
				Local faction:String = "NULL"
				Local type:String = "ANY"
				If s.minArgs(2)
					faction = s.values.Get(0)
					type = s.values.Get(1)
					Print faction + "/" + type
					jShip = EntityLoader.GetMember(faction, type, Player.map.level)
				Else If s.minArgs(1)
				Else If s.minArgs(1)
					faction = s.values.Get(0)
					jShip = EntityLoader.GetMember(faction, type, Player.map.level)
				End
				
				If jShip = Null
					ThrowErrorMessage("Unable to load entity", faction + "/" + type)
					Player.interaction.InsertScripts(ScriptManager.GetScript("stdlib_error.ess"))
					Return True
				End
				Local shipName:String = jShip.GetString("ship", "NULL")
				Local sData:JsonObject = ShipLoader.GetShip(shipName)
				If sData = Null
					ThrowErrorMessage("Unable to load ship", shipName)
					Player.interaction.InsertScripts(ScriptManager.GetScript("stdlib_error.ess"))
					Return True
				End
				enemyShip = New Ship(1220, 100, True)
				enemyShip.call_sign = Parse(jShip.GetString("name", "????"))
				enemyShip.OnLoad(sData, jShip)
				enemyShip.targetShip = Player.ship
				Player.ship.targetShip = enemyShip
				Return True
			Case Script.ADD_BUTTON
				For Local temp:int = 0 Until s.values.Length() Step 2
					Local t:MenuButton = New MenuButton(BUTTON_START + (BUTTON_SPACING * btn.Length()), 0, BUTTON_WIDTH, BUTTON_HEIGHT)
					t.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
					btn.Push(t)
					btnText.Push(s.values.Get(temp))
					btnEffect.Push(s.values.Get(temp + 1))
				Next
			Case Script.LOAD_LOOT
				Local loot:JsonObject
				If s.minArgs(1)
					loot = ScriptManager.GetObject(s.values.Get(0))
					If loot = Null
						ThrowErrorMessage("Unable to load script object", s.values.Get(0))
						Return True
					End
					Player.lootData = loot
					Return True
				Else
					Return True
				End
			Case Script.LOCK_SCREEN
				screenLocked = True
				Return True
			Case Script.UNLOCK_SCREEN
				screenLocked = False
				Return True
			Case Script.SAY
				For Local temp:Int = 0 Until s.values.Length()
					Local str:String = Parse(s.values.Get(temp))
					talks.Push(New TextEffect(TEXT_START_X, TEXT_START_Y + (TEXT_SPACING * talks.Length()), TextEffect.TYPE_TERMINAL, str))
				Next
				Return True
			Case Script.CLEAR_BUTTON
				btn.Clear()
				btnEffect.Clear()
				btnText.Clear()
				Return True
			Case Script.NULL_BUTTON
				For Local temp:Int = 0 Until btnEffect.Length()
					btnEffect.Set(temp, "stdlib_null.ess")
				Next
				Return True
			Case Script.BOARDING_COMBAT
				Local turn:Int = Rnd(2)
				Repeat
					If Player.ship.crew.Length() = 0
						talks.Push(New TextEffect(TEXT_START_X, TEXT_START_Y + (TEXT_SPACING * talks.Length()), TextEffect.TYPE_TERMINAL, "Battle lost"))
						Exit
					End
					If Player.ship.targetShip.crew.Length() = 0
						talks.Push(New TextEffect(TEXT_START_X, TEXT_START_Y + (TEXT_SPACING * talks.Length()), TextEffect.TYPE_TERMINAL, "Battle won"))
						Exit
					End
					If turn = 0 'player
						For Local c:Crew = EachIn Player.ship.crew
							If Player.ship.targetShip.crew.Length() = 0 Then Exit
							Local tc:Crew = Player.ship.targetShip.crew.Get(Rnd(Player.ship.targetShip.crew.Length()))
							tc.TakeDamage(c.GetCombatDamage())
							If tc.health / float(tc.GetMaxHp()) < 0.2
								Player.ship.targetShip.crew.RemoveEach(tc)
								talks.Push(New TextEffect(TEXT_START_X, TEXT_START_Y + (TEXT_SPACING * talks.Length()), TextEffect.TYPE_TERMINAL, tc.first_name + " has fled"))
							End
						End
					Else If turn = 1 'enemy
						For Local c:Crew = EachIn Player.ship.targetShip.crew
							If Player.ship.crew.Length() = 0 Then Exit
							Local tc:Crew = Player.ship.crew.Get(Rnd(Player.ship.crew.Length()))
							tc.TakeDamage(c.GetCombatDamage())
						End
						For Local temp:Int = 0 Until Player.ship.crew.Length()
							Player.ship.crew.Get(temp).Update()
							If Player.ship.crew.Get(temp).isDead()
								Player.accounting.CrewDeath(Player.ship.crew.Get(temp))
								talks.Push(New TextEffect(TEXT_START_X, TEXT_START_Y + (TEXT_SPACING * talks.Length()), TextEffect.TYPE_TERMINAL, Player.ship.crew.Get(temp).first_name + " has died"))
								Player.ship.crew.Remove(temp)
								temp -= 1
							End
						Next
						If Player.ship.crew.Length() = 0 Then Exit
					End
					turn += 1
					If turn = 2 Then turn = 0
				Forever
			Return True
			Default
				Return (Player.interaction.Run(s)) 'Try system calls
		End
		Return True
	End
	
	'if you change this copy it to quest
	Method Parse:String(str:String)
		If str.Contains("<RAND>")
			Local cArr:int[] =[int(Rnd(65, 91)), int(Rnd(65, 91)), int(Rnd(65, 91)), int(Rnd(65, 91)), int(Rnd(48, 58)), int(Rnd(48, 58))]
			Return str.Replace("<RAND>", String.FromChars(cArr))
		End
		If str.Contains("<CALL_SIGN>")
			
			If enemyShip <> Null
				Return str.Replace("<CALL_SIGN>", enemyShip.call_sign)
			End
		End
		Return str
	End
	#rem
	Method DamagePlayer:Void(target:Int = 0, majorExplosion:Bool = False)
		If target <> 0
			For Local tries:Int = 0 Until 50
				Local num:Int = Rnd(Player.ship.comp.Length())
				If Player.ship.comp.Get(num).getType() = target
					Local tx:Int = Player.ship.comp.Get(num).getX()
					Local ty:Int = Player.ship.comp.Get(num).getY()
					If Player.ship.pipes[tx * Ship.MAX_X + ty].isDamageable()
						Local temp:Weapon
						If majorExplosion
							temp = New Weapon(WeaponLoader.GetWeapon("Major Explosion"))
						Else
							temp = New Weapon(WeaponLoader.GetWeapon("Minor Explosion"))
						End
						Player.ship.health = Player.ship.maxHealth
						Player.ship.TakeDamage(New Bolt(tx, ty, temp.boltData, False))
						Return
					End
				End
			Next
		End
		
		
		
		
		Local done:Bool = False
		While Not done
			Local tx:Int = Rnd(Ship.MAX_X)
			Local ty:Int = Rnd(Ship.MAX_Y)
			If Player.ship.pipes[tx * Ship.MAX_X + ty].isDamageable()
				Local temp:Weapon
				If majorExplosion
					temp = New Weapon(WeaponLoader.GetWeapon("Major Explosion"))
				Else
					temp = New Weapon(WeaponLoader.GetWeapon("Minor Explosion"))
				End
				Player.ship.health = Player.ship.maxHealth
				Player.ship.TakeDamage(New Bolt(tx, ty, temp.boltData, False))
				done = True
			End
		Wend
	End
	#end
	
	Method Render:Int()
		Game.UnderParticle.Render()
		For Local temp:Int = 0 Until talks.Length()
			talks.Get(temp).Render()
		Next
		
		For Local temp:Int = 0 Until btn.Length()
			btn.Get(temp).RenderScaled(0, 0, 0.75)
			Game.white_font.Draw(btnText.Get(temp), btn.Get(temp).x + btn.Get(temp).w / 2, btn.Get(temp).y + btn.Get(temp).h / 6, 0.5)
		Next
		
		If enemyShip <> Null
			enemyShip.Render(, False)
		End
			Player.ship.Render(, False)
		If ScriptManager.DEBUG_CHOOSE
			ScriptManager.DebugRender()
		End
		Game.OverParticle.Render()
		If Game.showFPS
			DrawFPS()
		End
		Return 0
	
	End
	
End
