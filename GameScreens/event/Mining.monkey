Strict

Import space_game
Import UIStuff.hotbar


	
Class MiningScreen Extends nScreen
	Const UPDATE_SPEED:Int = 20
	Const DISPLAY_VALUES:Bool = True
	Field updateCount:int
	Field shipTest:Ship
	Field asteroid:Asteroid
	Field shipviewBtn:MenuButton
	
	Field exitBtn:MenuButton
	
	Field compGrab:ComponentInterface
	Field crewGrab:Crew
	
	Field droneStack:Stack<Drone>
	
	Field view:int = Tile.NORMAL_VIEW
	Field releaseCounter:int
	Field leaveMap:bool
	Field showItems:bool
	
	Field grid:InventoryManager
	Field grab:GrabItem
	
	
	Method New()
		asteroid = Player.map.hexField.grabObject.asteroid
		droneStack = New Stack<Drone>
		shipTest = Player.ship
		shipTest.CombatReset()
		Player.panel.asteroid = asteroid
		Print "drone count: " + Player.ship.items.CountItem(Item.MINING_DRONE)
		For Local temp:Int = 0 Until Player.ship.items.CountItem(Item.MINING_DRONE)
			droneStack.Push(New Drone(Drone.MINING, Null, shipTest))
		Next
		grab = New GrabItem()
		Local pd:PassData = New PassData(PassData.INVENTORY, PassData.NO_TARGET)
		grid = New InventoryManager(700, 100, 5, 4, pd, shipTest.items, grab)
		grid.filter.BlackList([37])
		grid.AddTrash(5, 3)
		shipviewBtn = New MenuButton(0, 0, 160, 56)
		shipviewBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		exitBtn = New MenuButton(170, 0, 160, 56)
		exitBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		'	sandboxBtn = New MenuButton(452, 0, 226, 74)
		'	sandboxBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
	End
	Method Init:Int()
		Return 0
	End

	Method Update:Int()
		If leaveMap
			Local missingDrones:Bool = False
			For Local temp:Int = 0 Until droneStack.Length()
				If droneStack.Get(temp).mode <> Drone.MODE_DONE
					missingDrones = True
					Print droneStack.Get(temp).mode
				End
			Next
			Print "test"
			If missingDrones = False
				If asteroid.isDead()
					Player.map.hexField.ClearCurrent()
				End
				If grab.item <> Null
					grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.INVENTORY))
					grid.Update()
					grab.ClearGrab()
					showItems = False
				End
				leaveMap = False
				Player.SaveGame()
				Game.SwitchScreen(Screens.SystemMap)
			End
		End
		If showItems Then grid.Update()
		grab.Update()
		'This order is important
		Player.panel.Update()
		If Not Player.paused Then shipTest.Update()
		If Not Player.paused Then Game.OverParticle.Update()
		If Not Player.paused Then Game.UnderParticle.Update()
		shipviewBtn.Poll()
		If shipviewBtn.hit
			showItems = Not showItems
			If grab.item <> Null
				grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.INVENTORY))
				grid.Update()
				grab.ClearGrab()
			End
		End
		
		exitBtn.Poll()
		If exitBtn.hit
			leaveMap = True
			If Player.ship.items.CountItem(Item.MINING_DRONE) = 0
				Player.SaveGame()
				Game.SwitchScreen(Screens.SystemMap)
			Else
				For Local temp:Int = 0 Until droneStack.Length()
					droneStack.Get(temp).returnHome = True
				Next
			End
		End
		
		If Game.Keys.KeyHit(KEY_SPACE)
			If Player.paused = True Then Player.paused = False Else Player.paused = True
			Print "start " + Millisecs()
		End
		
		
		If compGrab <> Null
			compGrab.UpdateStatus()
		End
		
	
		If Game.Cursor.Hit
			For Local temp:Int = 0 Until shipTest.comp.Length()
				If shipTest.comp.Get(temp).isMouseOver(shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
					compGrab = shipTest.comp.Get(temp)
					compGrab.UpdateStatus()
				End
			Next
			For Local temp:Int = 0 Until shipTest.crew.Length()
				If shipTest.crew.Get(temp).isMouseOver() Then crewGrab = shipTest.crew.Get(temp)
			Next
			For Local x:Int = 0 Until Ship.MAX_X
				For Local y:Int = 0 Until Ship.MAX_Y
					If shipTest.pipes[x * Ship.MAX_X + y].door or shipTest.pipes[x * Ship.MAX_X + y].airlock
						If shipTest.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
							If shipTest.pipes[x * Ship.MAX_X + y].doorOpen = True
								shipTest.pipes[x * Ship.MAX_X + y].doorOpen = False
							Else
								shipTest.pipes[x * Ship.MAX_X + y].doorOpen = True
							End
						End
					End
				Next
			Next
		End
		For Local temp:Int = 0 Until Min(droneStack.Length(), releaseCounter / 30)
			droneStack.Get(temp).Update()
		Next
		releaseCounter += 1
		
		
		If Game.Keys.KeyHit(KEY_ESCAPE)
			leaveMap = True
			If Player.ship.items.CountItem(Item.MINING_DRONE) = 0
				Player.SaveGame()
				Game.SwitchScreen(Screens.SystemMap)
			Else
				For Local temp:Int = 0 Until droneStack.Length()
					droneStack.Get(temp).returnHome = True
				Next
			End
		End
		
		Return 0
	End
	
	
	Method Render:Int()
		If view = Tile.NORMAL_VIEW Then Game.UnderParticle.Render()
		shipviewBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Items", shipviewBtn.x + shipviewBtn.w / 2, shipviewBtn.y + shipviewBtn.h / 6, 0.5)
		'If enemyShip.isDead()
			exitBtn.RenderScaled(0, 0, 0.75)
			Game.white_font.Draw("Exit", exitBtn.x + exitBtn.w / 2, exitBtn.y + exitBtn.h / 6, 0.5)
	'	End
			
		
		asteroid.Render()
		For Local temp:Int = 0 Until droneStack.Length()
			droneStack.Get(temp).Render()
		Next
		shipTest.Render(view, False)
	
		If Player.paused
			Game.white_font.Draw("Paused", 950, 80)
		End

		
		If crewGrab <> Null
			If crewGrab.order_x <> - 1
				SetColor(0, 255, 0)
				DrawRectOutline( (crewGrab.order_x * Ship.GRID_SIZE) + crewGrab.ship.x_offset, (crewGrab.order_y * Ship.GRID_SIZE) + crewGrab.ship.y_offset, 30, 30)
				ResetColorAlpha()
			End
			If crewGrab.dead
				crewGrab = Null
			Else
				Local s:Stack<String> = crewGrab.getStatus()
				Local space:Int = 400
				For Local temp:Int = 0 Until s.Length()
					Game.white_font.Draw(s.Get(temp), 800, space)
					space += 30
				Next
			End
			If Game.Cursor.RightHit()
				For Local x:Int = 0 Until Ship.MAX_X
					For Local y:Int = 0 Until Ship.MAX_Y
						If shipTest.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
							crewGrab.order_x = x
							crewGrab.order_y = y
						End
					Next
				Next
			End
		End
		If compGrab <> Null
			For Local x:Int = 0 Until compGrab.getStatus().Length()
				Game.white_font.Draw(compGrab.getStatus().Get(x), 1200, 750 + (x * 30))
			Next
		End
		
		Player.panel.Render()
		If showItems Then grid.Render()
		grab.Render()
		If view = Tile.NORMAL_VIEW Then Game.OverParticle.Render()
		
		
		
		If Game.showFPS
			DrawFPS()
		End
		Return 0
	
	End
End