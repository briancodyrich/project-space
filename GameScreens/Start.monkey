﻿Strict

Import space_game
	
Class StartScreen Extends nScreen
	Field startBtn:MenuButton
	Field loadBtn:MenuButton
	Field exitBtn:MenuButton
	Field editorBtn:MenuButton
	Field logo:Image
	Method New()
		startBtn = New MenuButton( (ScreenWidth / 2) - 90, (ScreenHeight / 2) + 200 / 2, 160, 56)
		loadBtn = New MenuButton( (ScreenWidth / 2) - 90, (ScreenHeight / 2) + 320 / 2, 160, 56)
		exitBtn = New MenuButton( (ScreenWidth / 2) - 90, (ScreenHeight / 2) + 440 / 2, 160, 56)
		editorBtn = New MenuButton( (ScreenWidth / 2) - 90, (ScreenHeight / 2) + 560 / 2, 160, 56)
		startBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		loadBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		exitBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		editorBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		logo = LoadImage("logo.jpg",, Image.MidHandle)
		Game.white_font.Resize(0.5)
		Game.SetRootScreen(Screens.Start)
		Player = Null
		Local s:SkillManager = New SkillManager()
		Print "battles: " + Settings.battles
		For Local temp:Int = 0 Until Settings.xpLog.Length()
			s.GainXP(temp, Settings.xpLog[temp])
			Print s.deleteThis[temp] + " Lv " + s.types.Get(temp).level + " - " + int(s.types.Get(temp).GetPercent() * 100) + "% to next lv"
		Next
		Game.OverParticle.Clear()
		Game.UnderParticle.Clear()
	End
	Method Init:Int()
		Return 0
	End
	
	
	Method Update:Int()

		Game.OverParticle.Update()
		startBtn.Poll()
		If startBtn.hit
			Game.SwitchScreen(Screens.NewGame)
		End
		loadBtn.Poll()
		If loadBtn.hit
			If Loader.ListSaves().Length() > 0
				Game.SwitchScreen(Screens.LoadGame)
			Else
				Game.SwitchScreen(Screens.NewGame)
			End
		End
		exitBtn.Poll()
		If exitBtn.hit
			Game.Stop()
		End
		If (Constants.editor)
			editorBtn.Poll()
			If editorBtn.hit
				Game.SwitchScreen(Screens.Editor)
			End
		End
		
		If Game.Keys.KeyHit(KEY_ESCAPE)
			Stop()
		End
	
		Return 0
	End
	
	Method Render:Int()
		DrawImage(logo, ScreenWidth / 2, ScreenHeight / 2 - 80)
		startBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("New", startBtn.x + startBtn.w / 2, startBtn.y + startBtn.h / 6, 0.5)
		loadBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Load", loadBtn.x + loadBtn.w / 2, loadBtn.y + loadBtn.h / 6, 0.5)
		exitBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Exit", exitBtn.x + exitBtn.w / 2, exitBtn.y + exitBtn.h / 6, 0.5)
		If (Constants.editor)
			editorBtn.RenderScaled(0, 0, 0.75)
			Game.white_font.Draw("Editor", editorBtn.x + editorBtn.w / 2, editorBtn.y + editorBtn.h / 6, 0.5)
		End
		'Text test area
		'Game.white_font.Draw("¤", ScreenWidth / 2, 100, 0.5)
		Game.OverParticle.Render()
		If Game.showFPS
			DrawFPS()
		End
		Return 0
	
	End
End
