Strict

Import space_game
Import UIStuff.inventorymanager
Import UIStuff.equipmentmanager
	
Class ShipDockedScreen Extends nScreen
	Field shipTest:Ship
	Field grid:InventoryManager
	Field equips:EquipmentManager
	Field grab:GrabItem
	Field crewGrab:Crew
	Method New()
		grab = New GrabItem()
		Player.menu.LockMenu()
		shipTest = New Ship(20, 100, False)
		shipTest = Player.ship
		Game.UnderParticle.Clear()
		shipTest.ShowCaseShip()
		Local pd:PassData = New PassData(PassData.INVENTORY, PassData.EQUIPMENT)
		grid = New InventoryManager(1400, 100, 5, 4, pd, shipTest.items, grab)
		grid.AddTrash(5, 3)
		pd = New PassData(PassData.EQUIPMENT, PassData.INVENTORY)
		equips = New EquipmentManager(800, 100, 5, 4, pd, shipTest, grab)
		'equips.filter.WhiteList([])
	End
	Method Init:Int()
		Return 0
	End
	
	
	Method Update:Int()
		If Game.popup <> Null And not Game.popup.isScreenFrozen() And Game.popup.isActiveMessage("Exit")
			Print Game.popup.getMessageCode()
			If Game.popup.getMessageCode() = Popup.OK
				Game.popup = Null
				Player.SaveGame()
				Game.SwitchScreen(Screens.Start)
			End
			Game.popup = Null
		End
		Player.menu.Update()
		If Player.menu.isLockTriggered()
			If grab.item <> Null
				grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.EQUIPMENT))
				equips.Update()
				If grab.success <> True
					grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.INVENTORY))
					grid.Update()
				End
			End
			Player.ship.UpdateMods()
			Player.panel.UpdateHotbar()
			Player.menu.SwitchKey()
			
		End
		If Game.Keys.KeyHit(KEY_ESCAPE) And Not Player.ship.isDead() And Not Player.ship.isNull()
			If grab.item <> Null
				grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.EQUIPMENT))
				equips.Update()
				If grab.success <> True
					grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.INVENTORY))
					grid.Update()
				End
			End
			Player.ship.UpdateMods()
			Player.panel.UpdateHotbar()
			Player.menu.UnlockMenu()
			Player.accounting.unDock()
			Player.SaveGame()
			Game.SwitchScreen(Screens.Root)
		End
	
		
	
	
		'This order is important
		grid.Update()
		If equips.Update()
			shipTest.Update(True)
		Else If not Player.panel.Update()
			shipTest.Update()
		End
		grab.Update()
		'This order is important
		
		Game.OverParticle.Update()
		Game.UnderParticle.Update()
		
		'CODE for ship interaction
		If Game.Cursor.Hit
			For Local temp:Int = 0 Until shipTest.crew.Length()
				If shipTest.crew.Get(temp).isMouseOver() Then crewGrab = shipTest.crew.Get(temp)
			Next
			For Local x:Int = 0 Until Ship.MAX_X
				For Local y:Int = 0 Until Ship.MAX_Y
					If shipTest.pipes[x * Ship.MAX_X + y].door or shipTest.pipes[x * Ship.MAX_X + y].airlock
						If shipTest.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
							If shipTest.pipes[x * Ship.MAX_X + y].doorOpen = True
								shipTest.pipes[x * Ship.MAX_X + y].doorOpen = False
							Else
								shipTest.pipes[x * Ship.MAX_X + y].doorOpen = True
							End
						End
					End
				Next
			Next
		End
		If crewGrab <> Null
			If Game.Cursor.RightHit()
				For Local x:Int = 0 Until Ship.MAX_X
					For Local y:Int = 0 Until Ship.MAX_Y
						If shipTest.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
							crewGrab.order_x = x
							crewGrab.order_y = y
						End
					Next
				Next
			End
		End
		'END CODE for ship interaction
	
		If Game.Keys.KeyHit(KEY_ESCAPE)
			If Not Player.ship.isDead() And Not Player.ship.isNull()
				Player.menu.UnlockMenu()
				Player.accounting.unDock()
				Player.SaveGame()
				Game.SwitchScreen(Screens.Root)
			Else
				Local message:Stack<String>
				message = New Stack<String>
				message.Push("Exit to main screen?")
				Game.popup = New Popup("Exit", message, "Yes", Popup.OK,,, "Cancel", Popup.CANCEL)
			End
		End
		Player.panel.Update(True)
		Return 0
	End
	
	
	Method Render:Int()
		Game.UnderParticle.Render()
		Player.menu.Render()
		shipTest.Render(, False)
		
		Player.panel.Render(True)
	
		
		
		'this order is important
		equips.Render()
		grid.Render()
		'this order is important
		
		grab.Render()
		If Game.showFPS
			DrawFPS()
		End
		Game.OverParticle.Render()
		Return 0
	
	End
End

