Strict

Import space_game
	
Class DockingScreen Extends nScreen
	Field max:int
	Field current:int
	Field logs:LogViewer
	Method New()
		Player.menu.Build(Screens.Dock)
		If Player.map.deathFlag And not Player.map.quitFlag
			Player.accounting.InsurancePayout()
			Player.ship.OnLoad(ShipLoader.GetShip("Null.ship"))
			Player.accounting.onDocking()
			Player.SaveGame()
			Player.map.deathFlag = False
		End
		Player.map.quitFlag = False
		If Player.ship <> Null
			Player.ship.HealShip(True)
		End
		Player.isDocked = True
		max = Player.accounting.history.Length() -1
		current = max
		logs = New LogViewer(175, 70, 800, 700, Player.chat)
	End
	Method Init:Int()
		Return 0
	End
	
	
	Method Update:Int()
		If Game.popup <> Null And not Game.popup.isScreenFrozen() And Game.popup.isActiveMessage("Exit")
			Print Game.popup.getMessageCode()
			If Game.popup.getMessageCode() = Popup.OK
				Game.popup = Null
				Player.SaveGame()
				Game.SwitchScreen(Screens.Start)
			End
			Game.popup = Null
		End
		Player.menu.Update()
		logs.Update()
		If Game.Keys.KeyHit(KEY_ESCAPE)
			If Not Player.ship.isDead() And Not Player.ship.isNull()
				Player.accounting.unDock()
				Player.SaveGame()
				Game.SwitchScreen(Screens.Root)
			Else
				Local message:Stack<String>
				message = New Stack<String>
				message.Push("Exit to main screen?")
				Game.popup = New Popup("Exit", message, "Yes", Popup.OK,,, "Cancel", Popup.CANCEL)
			End
		End
		If Game.Cursor.WheelDown()
			Local t:Int = Clamp(current - 1, 0, max)
			If current <> t
				current = t
				Player.accounting.delta = Player.accounting.history.Get(current)
			End
		End
		If Game.Cursor.WheelUp()
			Local t:Int = Clamp(current + 1, 0, max)
			If current <> t
				current = t
				Player.accounting.delta = Player.accounting.history.Get(current)
			End
		End
		Return 0
	End
	
	
	Method Render:Int()
		Player.menu.Render()
		logs.Render()
		
		
		Player.accounting.PrintReport(1100, 100, current + "/" + max)
		
		
		
		
		
		
		If Game.showFPS
			DrawFPS()
		End
		Return 0
	
	End
End
