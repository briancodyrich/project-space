Strict

Import space_game
Import UIStuff.inventorymanager
Import UIStuff.equipmentmanager

Class ShipStoreScreen Extends nScreen
	Field pShip:Ship
	Field area:Economy
	Field viewer:ShipScroller
	Field enginePower:PowerBar
	Field reactorPower:PowerBar
	Field shieldPower:PowerBar
	Field statPanel:Panel 
	Field info:InfoPane
	Method New()
		pShip = New Ship(360, 100, False)
		pShip.OnLoad(Player.ship.OnSave())
		area = Player.map.economy
		pShip.items = New Inventory(1)
		pShip.items.GiveItem("Fuel", 8)
		pShip.crew.Clear()
		
		'info pane stuff
		info = New InfoPane()
		info.area = area
		info.ship1 = pShip
		info.ship2 = New Ship(360, 100, False)
		statPanel = New Panel(600, 800, info)
		statPanel.Attach(New SimpleShipA(350, 300, info, True))
		statPanel.Attach(New SimpleShipB(350, 300, info, True))
		statPanel.Attach(New SimpleShipStore(350, 300, info, True))
		viewer = New ShipScroller(360, 100, False, New Economy(), EntityLoader.GetMemberList("Ship Store", "ANY", 0), info)
		viewer.SetEquipment(viewer.x + viewer.w + 20, viewer.y, 5, 4)
		viewer.AddButton(viewer.x, viewer.y + viewer.h, ShipScroller.LAST_SHIP, "Last")
		viewer.AddButton(viewer.x + ShipScroller.BTN_W + 10, viewer.y + viewer.h, ShipScroller.VIEW_EQUIPMENT, "Loadout", "Close")
		viewer.AddButton(viewer.x + (ShipScroller.BTN_W * 2) + 20, viewer.y + viewer.h, ShipScroller.NEXT_SHIP, "Next")
		viewer.AddButton(viewer.x + (ShipScroller.BTN_W * 3) + 30, viewer.y + viewer.h, ShipScroller.BUY_SHIP, "Buy")
		info.Attach(viewer)
		info.Attach(statPanel)
		info.Update()
		'enginePower = New PowerBar(ShipPanel.NORMAL_OFFSET, 800, PowerBar.TYPE_ENGINE, ship.enginePower, ship)
		'reactorPower = New PowerBar(ShipPanel.NORMAL_OFFSET, 900, PowerBar.TYPE_REACTOR, ship.reactorPower, ship)
		'shieldPower = New PowerBar(ShipPanel.NORMAL_OFFSET, 1000, PowerBar.TYPE_SHIELD, ship.shieldPower, ship)
	End
	Method Init:Int()
		Return 0
	End
	
	
	Method Update:Int()
		info.Update()
		If Game.popup <> Null And not Game.popup.isScreenFrozen() And Game.popup.isActiveMessage("Exit")
			Print Game.popup.getMessageCode()
			If Game.popup.getMessageCode() = Popup.OK
				Game.popup = Null
				Player.SaveGame()
				Game.SwitchScreen(Screens.Start)
			End
			Game.popup = Null
		End
		If Game.popup <> Null And not Game.popup.isScreenFrozen() And Game.popup.isActiveMessage("ShipCrew")
			Print Game.popup.getMessageCode()
			If Game.popup.getMessageCode() = Popup.OK
				Game.popup = Null
				viewer.buy_flag = True
			Else
				Game.popup = Null
			End
		End
		If Game.popup <> Null And not Game.popup.isScreenFrozen() And Game.popup.isActiveMessage("ShipItems")
			Print Game.popup.getMessageCode()
			If Game.popup.getMessageCode() = Popup.OK
				Game.popup = Null
				Local index:Int = 0
				While Player.ship.items.CountSlots(True, False) > info.ship2.crewSlots
					If Player.ship.items.slots[index] = Null
						index += 1
					Else
						Local i:Item = Player.ship.items.slots[index]
						Player.addCredits(i.GetValue(area))
						Player.ship.items.slots[index] = Null
						index += 1
					End
				Wend
				viewer.Do(ShipScroller.BUY_SHIP)
			Else
				Game.popup = Null
			End
			
		End
		If viewer.buy_flag = True
			viewer.buy_flag = False
			Local value:Int = info.ship2.GetValue(area) - Player.ship.GetValue(area, False, True)
			Player.addCredits(-value)
			Player.chat.Log(LogItem.BANK, "New ship purchased")
			Local hold:Inventory = Player.ship.items
			hold.Sort()
			Local crew:Stack<Crew> = New Stack<Crew>()
			For Local temp:Int = 0 Until Player.ship.crew.Length()
				Local c:Crew = New Crew(0, 0, Null, Null)
				c.OnLoad(Player.ship.crew.Get(temp).OnSave())
				crew.Push(c)
			Next
			Player.ship.OnLoad(info.ship2.OnSave())
			Player.ship.crew.Clear()
			Player.ship.items.DestroyInventory()
			For Local temp:Int = 0 Until crew.Length()
				crew.Get(temp).ship = Player.ship
				Player.ship.crew.Push(crew.Get(temp))
			Next
			Player.ship.BuildCrew()
			For Local temp:Int = 0 Until hold.slots.Length()
				If hold.slots[temp] <> Null
					Player.ship.items.slots[temp] = hold.slots[temp]
				End
			Next
			pShip.OnLoad(Player.ship.OnSave())
			pShip.items = New Inventory(1)
			pShip.items.GiveItem("Fuel", 8)
			pShip.crew.Clear()
			info.ship1 = pShip
		End
		Player.menu.Update()
		Player.panel.Update()
		'Player.panel.stats.Update()
		
		If Game.Keys.KeyHit(KEY_ESCAPE)
			If Not Player.ship.isDead() And Not Player.ship.isNull()
				Player.menu.UnlockMenu()
				Player.accounting.unDock()
				Player.SaveGame()
				Game.SwitchScreen(Screens.Root)
			Else
				Local message:Stack<String>
				message = New Stack<String>
				message.Push("Exit to main screen?")
				Game.popup = New Popup("Exit", message, "Yes", Popup.OK,,, "Cancel", Popup.CANCEL)
			End
		End
		Player.panel.Update(True)
		Game.OverParticle.Update()
		Game.UnderParticle.Update()
		
		Return 0
	End
	
	
	Method Render:Int()
		Game.UnderParticle.Render()
		info.Render()
		Player.menu.Render()
		Player.panel.Render(, True)
	
	'	pShip.Render(, False)
		
		'Player.panel.Render(False)
	
		
		If Game.showFPS
			DrawFPS()
		End
		Game.OverParticle.Render()
		Return 0
	
	End
End

