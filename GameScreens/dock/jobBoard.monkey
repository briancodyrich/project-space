Strict

Import space_game
	
Class JobBoardScreen Extends nScreen
	Field quests:Stack<Quest>
	Field jobBoard:QuestViewer
	Method New()
		quests = New Stack<Quest>()
		quests.Push(New Quest(QuestLoader.GetQuest("normal_bounty.eso")))
		quests.Push(New Quest(QuestLoader.GetQuest("normal_bounty.eso")))
		quests.Push(New Quest(QuestLoader.GetQuest("normal_bounty.eso")))
		Print quests.Length()
		jobBoard = New QuestViewer(175, 70, 1500, 1000, quests)
	End
	Method Init:Int()
		Return 0
	End
	
	
	Method Update:Int()
		If Game.popup <> Null And not Game.popup.isScreenFrozen() And Game.popup.isActiveMessage("Exit")
			Print Game.popup.getMessageCode()
			If Game.popup.getMessageCode() = Popup.OK
				Game.popup = Null
				Player.SaveGame()
				Game.SwitchScreen(Screens.Start)
			End
			Game.popup = Null
		End
		jobBoard.Update()
		Player.menu.Update()
		If Game.Keys.KeyHit(KEY_ESCAPE)
			If Not Player.ship.isDead() And Not Player.ship.isNull()
				Player.accounting.unDock()
				Player.SaveGame()
				Game.SwitchScreen(Screens.Root)
			Else
				Local message:Stack<String>
				message = New Stack<String>
				message.Push("Exit to main screen?")
				Game.popup = New Popup("Exit", message, "Yes", Popup.OK,,, "Cancel", Popup.CANCEL)
			End
		End
		Return 0
	End
	
	
	Method Render:Int()
		Player.menu.Render()
		jobBoard.Render()
		If Game.showFPS
			DrawFPS()
		End
		Return 0
	
	End
End
