Strict

Import space_game
Import UIStuff.inventorymanager
Import UIStuff.storage

Class PartsStoreScreen Extends StoreScreen
	Method New()
		Super.New(New ItemFilter([1, 2, 5, 7, 9, 10, 13], True))
	End
End

Class GoodsStoreScreen Extends StoreScreen
	Method New()
		Super.New(New ItemFilter([30, 31, 32, 33, 35], True))
	End
End


Class ModStoreScreen Extends StoreScreen
	Method New()
		Super.New(New ItemFilter([Item.SYSTEM], True))
	End
End

Class WeaponStoreScreen Extends StoreScreen
	Method New()
		Super.New(New ItemFilter([Item.WEAPON, Item.MISSILE, Item.CANNON_BALL, Item.THERMITE], True))
	End
End
	
Class StoreScreen Extends nScreen
	Field grab:GrabItem
	'Field shipTest:Ship
	Field grid:InventoryManager
	Field equips:EquipmentManager
	Field store:Storage
	Field storeData:Economy
	Field info:InfoPane
	Field statPanel:Panel
	Field delta:DeltaItem
	Method New(filter:ItemFilter)
		grab = New GrabItem()
		delta = New DeltaItem()
		storeData = Player.map.economy
		Local inv:Inventory = Player.map.hexField.grabObject.GetFilteredStore(filter)
		If inv = Null
			inv = New Inventory(HexObject.STORE_SIZE)
			Print "missing store"
		End
		Local pd:PassData = New PassData(PassData.EQUIPMENT, PassData.STORE, PassData.INVENTORY)
		equips = New EquipmentManager(230, 100, 5, 4, pd, Player.ship, grab, storeData, delta)
		'equips.filter.WhiteList([1, 2, 5, 7, 9, 10, 13])
		pd = New PassData(PassData.INVENTORY, PassData.EQUIPMENT, PassData.STORE)
		grid = New InventoryManager(1320, 100, 5, 4, pd, Player.ship.items, grab, storeData, delta)
		pd = New PassData(PassData.STORE, PassData.EQUIPMENT, PassData.INVENTORY)
		store = New Storage(775, 100, 5, 4, pd, inv, grab, Storage.STORE, storeData, delta)
		'store.filter.WhiteList([1, 2, 5, 7, 9, 10, 13])
		Player.menu.LockMenu()
		
		'info pane stuff
		info = New InfoPane()
		statPanel = New Panel(600, 800, info)
		statPanel.Attach(New SimpleShipA(350, 300, info, True))
		statPanel.Attach(New SimpleShipB(350, 300, info, True))
		statPanel.Attach(New SimpleShipC(350, 300, info, True))
		info.Attach(statPanel)
		info.ship1 = Player.ship
		info.delta = delta
		info.Update()
	End
	Method Init:Int()
		Return 0
	End
	
	Method Update:Int()
		If Game.popup <> Null And not Game.popup.isScreenFrozen() And Game.popup.isActiveMessage("Exit")
			Print Game.popup.getMessageCode()
			If Game.popup.getMessageCode() = Popup.OK
				Game.popup = Null
				If grab.item <> Null
					grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.INVENTORY))
					grid.Update()
					If grab.success = True Then Print "success" Else Print "fail"
					If grab.success <> True
						grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.STORE))
						store.Update()
					End
					grab.ClearGrab()
				End
				Player.map.hexField.grabObject.ReturnStore(store.items)
				Player.menu.UnlockMenu()
				Player.accounting.unDock()
				Player.SaveGame()
				Game.SwitchScreen(Screens.Start)
			End
			Game.popup = Null
		End
		
		info.Update()
		
		Player.menu.Update()
		If Player.panel.Update() = False
			Player.ship.Update()
		End
		Player.panel.BackgroundUpdate()
		If Player.menu.isLockTriggered()
			If grab.item <> Null
				grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.INVENTORY))
				grid.Update()
				If grab.success <> True
					grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.STORE))
					store.Update()
				End
				grab.ClearGrab()
			End
			Player.map.hexField.grabObject.ReturnStore(store.items)
			Player.menu.SwitchKey()
		End
		'this order is important
		store.Update()
		equips.Update()
		grid.Update()
		grab.Update()
		'this order is important
		
		If Game.Keys.KeyHit(KEY_ESCAPE)
			If Not Player.ship.isDead() And Not Player.ship.isNull()
				If grab.item <> Null
					grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.INVENTORY))
					grid.Update()
					If grab.success = True Then Print "success" Else Print "fail"
					If grab.success <> True
						grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.STORE))
						store.Update()
					End
					grab.ClearGrab()
				End
				Player.map.hexField.grabObject.ReturnStore(store.items)
				Player.menu.UnlockMenu()
				Player.accounting.unDock()
				Player.SaveGame()
				Game.SwitchScreen(Screens.Root)
			Else
				Local message:Stack<String>
				message = New Stack<String>
				message.Push("Exit to main screen?")
				Game.popup = New Popup("Exit", message, "Yes", Popup.OK,,, "Cancel", Popup.CANCEL)
			End
		End
		Return 0
	End
	Method Render:Int()
		Player.menu.Render()
		Player.panel.Render()
		info.Render()
		store.Render()
		equips.Render()
		grid.Render()
		grab.Render()
		If Game.showFPS
			DrawFPS()
		End
		Return 0
	
	End
End
