Strict

Import space_game

	
Class CrewDockedScreen Extends nScreen
	Field selectedCrew:Crew
	Field sendOnLeave:MenuButton
	Field grabBox:AiGrabBox
	Field crewAiGrab:CrewAiBox
	Field grabAI:AiIcon
	Field canSendOnLeave:Bool
	Field fireBtn:Stack<MenuButton>
	Field testShip:Ship
	
	Field status:Stack<CrewStatusLarge>
	Method New()
		testShip = Player.ship
		fireBtn = New Stack<MenuButton>()
		If testShip.crew.Length() > 0 Then selectedCrew = testShip.crew.Get(0)
		status = New Stack<CrewStatusLarge>()
		grabBox = New AiGrabBox(1750, 200)
		crewAiGrab = New CrewAiBox(1600, 200, grabBox)
		sendOnLeave = New MenuButton(340, 60, 160, 56)
		sendOnLeave.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		Local totalV:float
		For Local c:Crew = EachIn testShip.crew
			For Local temp:Int = 0 Until c.morale.Length()
				If c.morale.Get(temp).type = Morale.TIME_WORKED
					totalV += c.morale.Get(temp).GetCost()
				End
				If c.morale.Get(temp).type = Morale.TIME_OFF
					totalV += c.morale.Get(temp).GetCost()
				End
			Next
		Next
		Print totalV
		If totalV > 0 Then canSendOnLeave = True
		
		For Local temp:Int = 0 Until testShip.crew.Length()
			status.Push(New CrewStatusLarge(0, 0, testShip.crew.Get(temp)))
			Local b:MenuButton = New MenuButton(0, 0, 160, 56)
			b.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
			fireBtn.Push(b)
		Next
		For Local temp:Int = 0 Until status.Length()
			status.Get(temp).x = 250 + (temp * CrewStatusLarge.WIDTH) + (temp * 20)
			status.Get(temp).y = 300
			fireBtn.Get(temp).x = status.Get(temp).x
			fireBtn.Get(temp).y = status.Get(temp).y - 100
		Next
	End
	Method Init:Int()
		Return 0
	End
	
	Method Update:Int()

		If Game.popup <> Null And not Game.popup.isScreenFrozen() And Game.popup.isActiveMessage("Exit")
			Print Game.popup.getMessageCode()
			If Game.popup.getMessageCode() = Popup.OK
				Game.popup = Null
				Player.SaveGame()
				Player = Null
				Game.SwitchScreen(Screens.Start)
			End
			Game.popup = Null
		End
		If canSendOnLeave = True
			sendOnLeave.Poll()
			If sendOnLeave.hit
				Player.clock.tick(Clock.DAY * 3)
				Player.clock.pay_crew = False
				Player.accounting.DailyPayout(3,1)
				For Local c:Crew = EachIn testShip.crew
					c.MoraleEvent(Morale.TIME_OFF, 172800)
					c.MoraleReset(Morale.TIME_WORKED)
				Next
				Game.SwitchScreen(Screens.Dock)
			End
		End
		
		If Game.Keys.KeyHit(KEY_A)
			For Local c:Crew = EachIn testShip.crew
				c.MoraleEvent(Morale.TIME_OFF, 172800)
				c.TakeDamage(50)
				c.MoraleReset(Morale.TIME_WORKED)
			Next
		End
		If Player <> Null
			Player.ship.Update()
			Player.menu.Update()
		End
		Game.UnderParticle.Update()
		Game.OverParticle.Update()
		For Local temp:Int = 0 Until status.Length()
			status.Get(temp).x = 250 + (temp * CrewStatusLarge.WIDTH) + (temp * 20)
			status.Get(temp).y = 300
			fireBtn.Get(temp).x = status.Get(temp).x
			fireBtn.Get(temp).y = status.Get(temp).y - 100
			status.Get(temp).Update()
			If Game.Cursor.Hit()
				If status.Get(temp).isMouseOver()
					selectedCrew = status.Get(temp).getCrew()
				End
			End
			fireBtn.Get(temp).Poll()
			If fireBtn.Get(temp).hit
				Local c:Crew = Player.ship.crew.Get(temp)
				Player.chat.Log(LogItem.CREW, c.first_name + " was fired")
				Player.ship.crew.Remove(temp)
				Player.map.hexField.grabObject.crew.Push(c)
				status.Remove(temp)
				fireBtn.Remove(temp)
			End
		Next
		If grabAI = Null And Game.Cursor.Hit()
			If grabBox.isMouseOver()
				grabAI = grabBox.grabAi()
			End
			If crewAiGrab.isMouseOver()
				grabAI = crewAiGrab.GrabAi()
			End
		End
		crewAiGrab.Update(grabAI, selectedCrew)
		If Not Game.Cursor.Down()
			grabAI = Null
		End
		If Game.Keys.KeyHit(KEY_ESCAPE)
			If Not Player.ship.isDead() And Not Player.ship.isNull()
				Player.accounting.unDock()
				Player.SaveGame()
				Game.SwitchScreen(Screens.Root)
			Else
				Local message:Stack<String>
				message = New Stack<String>
				message.Push("Exit to main screen?")
				Game.popup = New Popup("Exit", message, "Yes", Popup.OK,,, "Cancel", Popup.CANCEL)
			End
		End
		
		Return 0
		
	End
	
	
	Method Render:Int()
		If Player <> Null
			Player.menu.Render()
		End
		crewAiGrab.Render(selectedCrew)
		grabBox.Render()
		
		If grabAI <> Null
			grabAI.Render(Game.Cursor.x() -60, Game.Cursor.y() -60)
		End
		
		
		
		
		For Local temp:Int = 0 Until status.Length()
			status.Get(temp).Render(selectedCrew)
			fireBtn.Get(temp).RenderScaled(0, 0, 0.75)
			Game.white_font.Draw("Fire", fireBtn.Get(temp).x + fireBtn.Get(temp).w / 2, fireBtn.Get(temp).y + fireBtn.Get(temp).h / 6, 0.5)
		Next
	
		

		If canSendOnLeave = True
			sendOnLeave.RenderScaled(0, 0, 0.75)
			Game.white_font.Resize(0.4)
			Game.white_font.Draw("Grant Time Off", sendOnLeave.x + sendOnLeave.w / 2, sendOnLeave.y + sendOnLeave.h / 6, 0.5)
			Game.white_font.Resize(0.5)
		End
		
		
		
		
		If Game.showFPS
			DrawFPS()
		End
		Return 0
		
	End
End

