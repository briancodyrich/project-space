Strict

Import space_game

	
Class HireCrewScreen Extends nScreen
	Field selectedCrew:Crew
	Field status:Stack<CrewStatusLarge>
	Field hireBtn:Stack<MenuButton>
	Field cost:Stack<Int>
	Method New()
		If Player.ship.crew.Length() > 0 Then selectedCrew = Player.ship.crew.Get(0)
		status = New Stack<CrewStatusLarge>()
		hireBtn = New Stack<MenuButton>()
		cost = New Stack<Int>()
		Print "wages"
		For Local temp:Int = 0 Until Player.map.hexField.grabObject.crew.Length()
			Print Player.map.hexField.grabObject.crew.Get(temp).GetHireCost()
			cost.Push(Player.map.hexField.grabObject.crew.Get(temp).GetHireCost())
			status.Push(New CrewStatusLarge(0, 0, Player.map.hexField.grabObject.crew.Get(temp)))
			Local b:MenuButton = New MenuButton(0, 0, 160, 56)
			b.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
			hireBtn.Push(b)
		Next
		For Local temp:Int = 0 Until status.Length()
			status.Get(temp).x = 250 + (temp * CrewStatusLarge.WIDTH) + (temp * 20)
			status.Get(temp).y = 300
			hireBtn.Get(temp).x = status.Get(temp).x
			hireBtn.Get(temp).y = status.Get(temp).y - 100
		Next
	End
	Method Init:Int()
		Return 0
	End
	
	
	Method Update:Int()

		If Game.popup <> Null And not Game.popup.isScreenFrozen() And Game.popup.isActiveMessage("Exit")
			Print Game.popup.getMessageCode()
			If Game.popup.getMessageCode() = Popup.OK
				Game.popup = Null
				Player.SaveGame()
				Game.SwitchScreen(Screens.Start)
			End
			Game.popup = Null
		End
		Player.ship.Update()
		Player.menu.Update()
		Game.UnderParticle.Update()
		Game.OverParticle.Update()
		For Local temp:Int = 0 Until status.Length()
			status.Get(temp).x = 250 + (temp * CrewStatusLarge.WIDTH) + (temp * 20)
			status.Get(temp).y = 300
			hireBtn.Get(temp).x = status.Get(temp).x
			hireBtn.Get(temp).y = status.Get(temp).y - 100
			If Game.Cursor.Hit()
				If status.Get(temp).isMouseOver()
					selectedCrew = status.Get(temp).getCrew()
				End
			End
			hireBtn.Get(temp).Poll()
			If hireBtn.Get(temp).hit
				If Player.getCredits() >= cost.Get(temp)
					If not Player.ship.isNull()
						Local c:Crew = New Crew(0, 0, Player.ship, Null)
						c.OnLoad(Player.map.hexField.grabObject.crew.Get(temp).OnSave())
						c.SetBasicJob(Crew.JOB_MECHANIC)
						Player.ship.AddCrew(c)
						Player.chat.Log(LogItem.CREW, c.first_name + " was hired")
						Player.accounting.LogCredits(CreditLog.WAGES, -cost.Get(temp), CreditLog.PAY_NOW)
						Player.map.hexField.grabObject.crew.Remove(temp)
						status.Remove(temp)
						cost.Remove(temp)
						hireBtn.Remove(temp)
					End
				End
			End
		Next
		If Game.Keys.KeyHit(KEY_ESCAPE)
			If Not Player.ship.isDead() And Not Player.ship.isNull()
				Player.accounting.unDock()
				Player.SaveGame()
				Game.SwitchScreen(Screens.Root)
			Else
				Local message:Stack<String>
				message = New Stack<String>
				message.Push("Exit to main screen?")
				Game.popup = New Popup("Exit", message, "Yes", Popup.OK,,, "Cancel", Popup.CANCEL)
			End
		End
		
		Return 0
		
	End
	
	
	Method Render:Int()
		Player.menu.Render()
			
		For Local temp:Int = 0 Until status.Length()
			status.Get(temp).Render(selectedCrew)
			hireBtn.Get(temp).RenderScaled(0, 0, 0.75)
			Game.white_font.Draw("Hire", hireBtn.Get(temp).x + hireBtn.Get(temp).w / 2, hireBtn.Get(temp).y + hireBtn.Get(temp).h / 6, 0.5)
			Local x:Int = status.Get(temp).x
			Local y:Int = status.Get(temp).y - 150
			Game.white_font.Draw(FormatCredits(cost.Get(temp)), x, y)
		Next
		
		If Game.showFPS
			DrawFPS()
		End
		Return 0
		
	End
End

