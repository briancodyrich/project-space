Strict

Import space_game
Import UIStuff.inventorymanager
Import UIStuff.equipmentmanager
	
Class ShipMenuScreen Extends nScreen
	Field shipTest:Ship
	Field grid:InventoryManager
	Field equips:EquipmentManager
	Field grab:GrabItem
	Field crewGrab:Crew
	Field lv:int
	Method New()
		grab = New GrabItem()
		Player.menu.LockMenu()
		shipTest = New Ship(20, 100, False)
		shipTest = Player.ship
		shipTest.ShowCaseShip()
		Local pd:PassData = New PassData(PassData.INVENTORY, PassData.EQUIPMENT)
		grid = New InventoryManager(1400, 100, 5, 4, pd, shipTest.items, grab)
		grid.AddTrash(5, 3)
		pd = New PassData(PassData.EQUIPMENT, PassData.INVENTORY)
		equips = New EquipmentManager(800, 100, 5, 4, pd, shipTest, grab)
		equips.filter.WhiteList([])
		lv = 0
	End
	Method Init:Int()
		Return 0
	End
	
	
	Method Update:Int()
		Player.menu.Update()
		If Player.menu.isLockTriggered()
			If grab.item <> Null
				grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.EQUIPMENT))
				equips.Update()
				If grab.success <> True
					grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.INVENTORY))
					grid.Update()
				End
			End
			Player.ship.UpdateMods()
			Player.panel.UpdateHotbar()
			Player.menu.SwitchKey()
			Player.menu.UnlockMenu()
		End
		If Game.Keys.KeyHit(KEY_ESCAPE)
			If grab.item <> Null
				grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.EQUIPMENT))
				equips.Update()
				If grab.success <> True
					grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.INVENTORY))
					grid.Update()
				End
			End
			Player.ship.UpdateMods()
			Player.panel.UpdateHotbar()
			Player.menu.UnlockMenu()
			Game.SwitchScreen(Screens.Root)
		End
		#rem
		If Game.Keys.KeyHit(KEY_ENTER)
			lv -= 1
			Print lv
			Player.ship.alerts.Monitor(AlarmManager.TestTimer, lv)
		End
		If Game.Keys.KeyHit(KEY_SPACE)
			lv += 1
			Print lv
			Player.ship.alerts.Monitor(AlarmManager.TestTimer, lv)
		End
		#end
		If Game.Keys.KeyHit(KEY_ENTER)
			For Local x:Int = 0 Until Ship.MAX_X
				For Local y:Int = 0 Until Ship.MAX_Y
					If shipTest.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
						'shipTest.shield_overlay.NewImpact(x, y)
						Local temp:Weapon = New Weapon(ItemLoader.GetItem("Gamma Ray"))
						shipTest.TakeDamage(New Bolt(x, y, temp.boltData, shipTest.areShieldsActive()))
					End
				Next
			Next
		End
	
		'This order is important
		grid.Update()
		If equips.Update()
			shipTest.Update(True)
		Else If not Player.panel.Update()
			shipTest.Update()
		End
		grab.Update()
		'This order is important
		
		Game.OverParticle.Update()
		Game.UnderParticle.Update()
		
		'CODE for ship interaction
		If Game.Cursor.Hit
			For Local temp:Int = 0 Until shipTest.crew.Length()
				If shipTest.crew.Get(temp).isMouseOver() Then crewGrab = shipTest.crew.Get(temp)
			Next
			For Local x:Int = 0 Until Ship.MAX_X
				For Local y:Int = 0 Until Ship.MAX_Y
					If shipTest.pipes[x * Ship.MAX_X + y].door or shipTest.pipes[x * Ship.MAX_X + y].airlock
						If shipTest.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
							If shipTest.pipes[x * Ship.MAX_X + y].doorOpen = True
								shipTest.pipes[x * Ship.MAX_X + y].doorOpen = False
							Else
								shipTest.pipes[x * Ship.MAX_X + y].doorOpen = True
							End
						End
					End
				Next
			Next
		End
		If crewGrab <> Null
			If Game.Cursor.RightHit()
				For Local x:Int = 0 Until Ship.MAX_X
					For Local y:Int = 0 Until Ship.MAX_Y
						If shipTest.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
							crewGrab.order_x = x
							crewGrab.order_y = y
						End
					Next
				Next
			End
		End
		'END CODE for ship interaction
		
		Return 0
	End
	
	
	Method Render:Int()
		Game.UnderParticle.Render()
		Player.menu.Render()
	
		shipTest.Render(, False)
		
		Player.panel.Render(True)
		
		Game.white_font.Draw("RAN: " + shipTest.GetRange(), 100, 725)
		Game.white_font.Draw("FOD: " + shipTest.items.CountItem(Item.FOOD), 100, 750)
		
		
		'this order is important
		equips.Render()
		grid.Render()
		'this order is important
		
		grab.Render()
		If Game.showFPS
			DrawFPS()
		End
		Game.OverParticle.Render()
		Return 0
	
	End
End

