Strict

Import space_game

Class CrewMenuScreen Extends nScreen
	Field selectedCrew:Crew
	Field grabBox:AiGrabBox
	Field crewAiGrab:CrewAiBox
	Field grabAI:AiIcon
	
	Field status:Stack<CrewStatusLarge>
	
	Method New()
		If Player.ship.crew.Length() > 0 Then selectedCrew = Player.ship.crew.Get(0)
		status = New Stack<CrewStatusLarge>()
		grabBox = New AiGrabBox(1750, 200)
		crewAiGrab = New CrewAiBox(1600, 200, grabBox)
		For Local temp:Int = 0 Until Player.ship.crew.Length()
			status.Push(New CrewStatusLarge(0, 0, Player.ship.crew.Get(temp)))
		Next
		For Local temp:Int = 0 Until status.Length()
			status.Get(temp).x = 250 + (temp * CrewStatusLarge.WIDTH) + (temp * 20)
			status.Get(temp).y = 300
		Next
	End
	Method Init:Int()
		Return 0
	End
	
	
	Method Update:Int()
		Player.ship.Update()
		Player.menu.Update()
		Game.UnderParticle.Update()
		Game.OverParticle.Update()
		For Local temp:Int = 0 Until status.Length()
			status.Get(temp).x = 250 + (temp * CrewStatusLarge.WIDTH) + (temp * 20)
			status.Get(temp).y = 300
			If Game.Cursor.Hit()
				If status.Get(temp).isMouseOver()
					selectedCrew = status.Get(temp).getCrew()
				End
			End
			status.Get(temp).Update()
		Next
		If grabAI = Null And Game.Cursor.Hit()
			If grabBox.isMouseOver()
				grabAI = grabBox.grabAi()
			End
			If crewAiGrab.isMouseOver()
				grabAI = crewAiGrab.GrabAi()
			End
		End
		crewAiGrab.Update(grabAI, selectedCrew)
		If Not Game.Cursor.Down()
			grabAI = Null
		End
		If Game.Keys.KeyHit(KEY_ESCAPE)
			Game.SwitchScreen(Screens.Root)
		End
		
	
		Return 0
	End
	
	
	Method Render:Int()
		Player.menu.Render()
			
		For Local temp:Int = 0 Until status.Length()
			status.Get(temp).Render(selectedCrew)
		Next
		
		crewAiGrab.Render(selectedCrew)
		grabBox.Render()
		If grabAI <> Null
			grabAI.Render(Game.Cursor.x() -60, Game.Cursor.y() -60)
		End
		If Game.showFPS
			DrawFPS()
		End
		Return 0
	End
End
