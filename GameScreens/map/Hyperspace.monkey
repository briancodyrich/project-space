Strict

Import space_game
	
Class HyperSpaceScreen Extends GalaxyMapScreen

	Method New()
		Super.New()
		Player.menu.Build(Screens.Hyperspace)
		Game.SetRootScreen(Screens.Hyperspace)
	End
	Method Init:Int()
		Return 0
	End
	
	
	Method Update:Int()
		Super.Update()
		Return 0
	End
	
	
	Method Render:Int()
		Super.Render()
		
		If Game.showFPS
			DrawFPS()
		End
		Return 0
	End
End