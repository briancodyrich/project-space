Strict

Import space_game
	
Class GalaxyMapScreen Extends nScreen
	Field map:HyperNodeManager
	Field camera:Camera
	Field rootSystemData:Economy
	Field rootSystemIndex:int
	Field targetSystemIndex:int
	Field travelCost:Int
	Field rootDataViewer:SystemData
	Field targetDataViewer:SystemData
	Method New()
		map = Player.galaxy
		
		rootSystemIndex = Player.map.system
		rootSystemData = map.nodes.Get(rootSystemIndex).economy 
	'	Local rn:HyperNode = map.nodes.Get(rootSystemIndex)
		camera = New Camera(ScreenWidth / 2, ScreenHeight / 2)
		rootSystemIndex = Player.map.system
		targetSystemIndex = rootSystemIndex
		rootDataViewer = New SystemData(rootSystemData, 60, 60, 300, 30, 10)
		targetDataViewer = New SystemData(Null, 1200, 60, 300, 30, 10)
	End
	Method Init:Int()
		Return 0
	End
	
	
	Method Update:Int()
	
		If Game.popup <> Null And not Game.popup.isScreenFrozen() And Game.popup.isActiveMessage("Exit")
			Print Game.popup.getMessageCode()
			If Game.popup.getMessageCode() = Popup.OK
				Game.popup = Null
				Player.SaveGame()
				Game.SwitchScreen(Screens.Start)
			End
			Game.popup = Null
		End
		
		If KeyHit(KEY_1)
			ScaleFactor *= 2.0
		End
		If KeyHit(KEY_0)
			ScaleFactor /= 2.0
		End
		Player.menu.Update()
		If Game.Cursor.MiddleHit()
			Print camera.x + "/" + camera.y
			'camera.GoHome()
			Print "home"
		End
		
		If Game.Keys.KeyHit(KEY_S)
			For Local temp:Int = 0 Until map.nodes.Length()
				If map.nodes.Get(temp).isMouseOver(Game.Cursor.x(), Game.Cursor.y(), camera.x, camera.y)
					map.nodes.Get(temp).economy.PrintStuff()
				End
			Next
		End
		
		If Game.Cursor.Hit()
			For Local temp:Int = 0 Until map.nodes.Length()
				If rootSystemIndex <> temp And targetSystemIndex <> temp
					If map.nodes.Get(temp).isMouseOver(Game.Cursor.x(), Game.Cursor.y(), camera.x, camera.y)
						targetSystemIndex = temp
						map.FindPath(rootSystemIndex, targetSystemIndex)
						Print "cost: " + map.GetCost()
						targetDataViewer.UpdateSystem(map.nodes.Get(temp).economy)
					End
				End
			End
		End
		camera.Update()
		If Game.Keys.KeyHit(KEY_ESCAPE)
			If Not Player.ship.isDead() And Not Player.ship.isNull()
				Player.menu.UnlockMenu()
				Player.accounting.unDock()
				Player.SaveGame()
				Game.SwitchScreen(Screens.Root)
			Else
				Local message:Stack<String>
				message = New Stack<String>
				message.Push("Exit to main screen?")
				Game.popup = New Popup("Exit", message, "Yes", Popup.OK,,, "Cancel", Popup.CANCEL)
			End
		End
		If Game.Keys.KeyHit(KEY_A)
			Local time:Int = Millisecs()
			map.Update()
			Print "done " + (Millisecs() -time)
		End
		If Game.Keys.KeyHit(KEY_SPACE)
			#rem
			If rootDataViewer <> Null And targetDataViewer <> Null
				Local arr:Float[] = rootDataViewer.area.GetPriceDelta(targetDataViewer.area.ToArray())
				Local s:String[] = rootDataViewer.area.PrintArray()
				For Local temp:Int = 0 Until arr.Length()
					Print s[temp] + " " + (arr[temp] * 100)
				Next
			End
			#end
		End
		If Game.Keys.KeyHit(KEY_ENTER)' And one <> - 1 And two <> - 1 And one <> two
			For Local temp:Int = 0 Until map.nodes.Length()
				If map.nodes.Get(temp).isMouseOver(Game.Cursor.x(), Game.Cursor.y(), camera.x, camera.y)
					Print temp
					map.VisitNode(temp)
				End
			Next
			
			#rem
			Player.map.SwitchSystem(targetSystemIndex)
			rootSystemData = Player.map.systemData
			targetSystemData = Null
			rootSystemIndex = targetSystemIndex
			targetSystemIndex = rootSystemIndex
			rootDataViewer.UpdateSystem(rootSystemData)
			targetDataViewer.area = Null
			Local rn:HyperNode = map.nodes.Get(rootSystemIndex)
			map.VisitNode(rootSystemIndex)
			camera = New Camera(rn.x, rn.y)
			Player.SaveGame()
			#end
		End
		Return 0
	End
	
	
	Method Render:Int()
		map.Render(targetSystemIndex, rootSystemIndex, camera.x, camera.y)
		Player.menu.Render()
		'rootDataViewer.Render()
		'targetDataViewer.Render()
		
		
		
		If Game.showFPS
			DrawFPS()
		End
		Return 0
	
	End
End

Class Camera
	Const SPEED:Int = 200
	Field homeX:Int
	Field homeY:int
	Field startX:int
	Field startY:Int
	Field grabX:Int
	Field grabY:int
	
	Field x:Int
	Field y:Int
	Field time:Int
	
	Method New(x:int, y:int)
		Self.x = (ScreenWidth / 2) - x
		Self.y = y - (ScreenHeight / 2)
		homeX = Self.x
		homeY = Self.y
		time = SPEED
	End
	
	Method Update:Void()
		If Game.Cursor.Hit()
			If time <> SPEED Then time = SPEED
			grabX = Game.Cursor.x()
			grabY = Game.Cursor.y()
		End
		
		If Game.Cursor.Down()
			x += Game.Cursor.x() -grabX
			y -= Game.Cursor.y() -grabY
			grabX = Game.Cursor.x()
			grabY = Game.Cursor.y()
		End
		
		If time < SPEED
			x = easeInOutSine(time, startX, homeX - startX, SPEED)
			y = easeInOutSine(time, startY, homeY - startY, SPEED)
			time += 1
		End
		
	End
	Method GoHome:Void()
		time = 0
		startX = x
		startY = y
	End
End