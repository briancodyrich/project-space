Strict

Import space_game
	
Class SystemMapScreen Extends nScreen
	Field map:HexField
	Field isSwitching:bool
	Field triggerInput:bool
	Field logs:LogViewer
	Method New()
		map = Player.map.hexField
		Player.interaction.HandShake()
		Player.ship.TurnOffGuns()
		If map.haveMoved
			For Local temp:Int = 0 Until map.hex.Length()
				If map.hex.Get(temp).isDupe(Player.map.x, Player.map.y, Player.map.z)
					Local px:Int = map.hex.Get(temp).xPos
					Local py:Int = map.hex.Get(temp).yPos
					map.cameraX = (map.centerX - px)
					map.cameraY = (map.centerY - py)
				End
			Next
		End
		map.Scan()
		Player.ship.targetShip = Null
		Player.isDocked = False
		Player.menu.Build(Screens.SystemMap)
		Game.SetRootScreen(Screens.SystemMap)
		Player.panel.SetMode(ShipPanel.MAP_MODE)
		logs = New LogViewer(15, 70, 800, 700, Player.chat)
	End
	Method Init:Int()
		Return 0
	End
	
	
	Method Update:Int()
		If Player.ship.isDead() And isSwitching = False
			Player.map.deathFlag = True
			Player.map.GotoPlanet()
			isSwitching = True
			Game.SwitchScreen(Screens.Dock)
		End
		logs.Update()

		If Game.Keys.KeyHit(KEY_ENTER)
			SoundEffect.Play(SoundEffect.SPARK)
			Game.OverParticle.Add(Game.Cursor.x(), Game.Cursor.y(), 0, 0, False, PhotonTypes.Spark)
		#rem
			Print "lv 0"
			Local a:SystemType = New SystemType(0, 0, True)
			Local i:Item = New Item(ItemLoader.GetItem("Basic Engine"), 0, 0)
			Local ll:LeveledList = New LeveledList(a)
			Local arr:Int[8]
			For Local tepm:Int = 0 Until 1000
				Local found:Bool = False
				While found = False
					Local lv:int = Rnd(8)
					If Rnd(100) < ll.GetQualityRNG(i, lv)
						arr[lv] += 1
						found = True
					End
				End
			Next
			
			
			For Local temp:Int = 0 Until 8
				Print "lv: " + temp + " -" + arr[temp]
			Next
			
			
			Print "lv 5"
			a = New SystemType(0, 5, True)
			i = New Item(ItemLoader.GetItem("Basic Engine"), 0, 0)
			ll = New LeveledList(a)
			arr = New Int[8]
			For Local tepm:Int = 0 Until 1000
				Local found:Bool = False
				While found = False
					Local lv:int = Rnd(8)
					If Rnd(100) < ll.GetQualityRNG(i, lv)
						arr[lv] += 1
						found = True
					End
				End
			Next
			
			For Local temp:Int = 0 Until 8
				Print "lv: " + temp + " -" + arr[temp]
			Next
			
			
			Print "lv 10"
			a = New SystemType(0, 10, True)
			i = New Item(ItemLoader.GetItem("Basic Engine"), 0, 0)
			ll = New LeveledList(a)
			arr = New Int[8]
			For Local tepm:Int = 0 Until 1000
				Local found:Bool = False
				While found = False
					Local lv:int = Rnd(8)
					If Rnd(100) < ll.GetQualityRNG(i, lv)
						arr[lv] += 1
						found = True
					End
				End
			Next

			For Local temp:Int = 0 Until 8
				Print "lv: " + temp + " -" + arr[temp]
			Next
			
			
		'	Player.interaction.LoadScript("on_poi.ess")
			'Player.accounting.InsurancePayout()
		'	Print ShipLoader.GetShip("Null.ship").ToJson()
			'Player.ship.OnLoad(ShipLoader.GetShip("Null.ship"))
		'	Player.interaction.LoadScript("on_poi.ess")
			For Local days:Int = 0 Until 500 Step 10
				For Local temp:Int = 0 Until 100
					Local test:Float = GetSigmoid(float(temp), 0.0, 85.0)
					If (0.01 + (days * 0.001) + test) >= 1.0
						Print "<" + temp + "%" + "at " + days + " days"
						Exit
					End
				Next
			Next
			#end
		End
		If Player.interaction.HasScript()
			If (Player.interaction.Run(Player.interaction.GetScript()))
				Player.interaction.DestroyScript()
			End
		End
		
		Player.panel.Update()
		Player.menu.Update()
		If Not map.isMoving()
			Game.UnderParticle.Update()
			Game.OverParticle.Update()
			Player.ship.Update()
		End
		If Game.popup <> Null And not Game.popup.isScreenFrozen() And Game.popup.isActiveMessage("Exit")
			Print Game.popup.getMessageCode()
			If Game.popup.getMessageCode() = Popup.OK
				Game.popup = Null
				Player.SaveGame()
				Game.SwitchScreen(Screens.Start)
			End
			Game.popup = Null
		End
		If Game.Keys.KeyHit(KEY_SPACE)
			Print Player.clock.GetTimeStamp()
			Player.interaction.LoadScript("on_poi.ess")
		'	Print(Game.Cursor.x() +"/" + Game.Cursor.y())
		'	Player.map.hexField.area.technology = 0
		'	Player.map.hexField.area.resources = 0
			'TestKeyBind()
			'Game.SwitchScreen(Screens.Death)
		'	Player.interaction = "Pirates/normal_pirate.json"
		'	Game.SwitchScreen(Screens.Communication)
		End
		
		map.Update()
		If Game.Keys.KeyHit(KEY_ESCAPE)
			Local message:Stack<String>
			message = New Stack<String>
			message.Push("Exit to main screen?")
			Game.popup = New Popup("Exit", message, "Yes", Popup.OK,,, "Cancel", Popup.CANCEL)
		End
		If Game.Keys.KeyHit(KEY_D)
			Game.SwitchScreen(Screens.DebugItems)
		End
		If Game.Keys.KeyHit(KEY_F1)
			Print "x:" + Player.map.x + " y:" + Player.map.y + " z:" + Player.map.z
			Print Player.map.hexField.centerX + "/" + Player.map.hexField.centerY
		End
		
		If ScriptManager.DEBUG_CHOOSE
			ScriptManager.DebugUpdate()
		End
		Return 0
	End
	
	
	Method Render:Int()
		Player.menu.Render()
		map.Render()
		Player.panel.Render()
		logs.Render()
		#rem
		For Local temp:Int = 0 Until intList.Length()
			If temp Mod 2 = 0
				SetColor(255, 255, 255)
			Else
				SetColor(125, 125, 125)
			End
			DrawRect(temp * 10, 0, 10, intList[temp])
			If temp Mod 10 = 0
				SetColor(0, 255, 0)
				DrawRectOutline(temp * 10, 0, 100, 100)
			End
		Next
		SetColor(0, 255, 0)
		For Local temp:Int = 0 Until 10
			For Local temp2:Int = 0 Until 10
				DrawRectOutline(temp * 100, temp2 * 100, 100, 100)
			Next
		Next
		#end
		If ScriptManager.DEBUG_CHOOSE
			ScriptManager.DebugRender()
		End
		If Game.showFPS
			DrawFPS()
		End
		Return 0
	
	End
End


