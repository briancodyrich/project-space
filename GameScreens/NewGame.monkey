Strict

Import space_game
Import UIStuff.inputbox

	
Class NewGameScreen Extends nScreen
	Const SHIP_SELECTION:Int = 0
	Const CREW_SELECTION:Int = 1
	Const DONE:Int = 2
	Field name:InputBox
	Field startBtn:MenuButton
	Field diffBtn:MenuButton
	Field shipBtn:MenuButton
	
	Field viewer:ShipScroller
	
	
	Field mode:int
	
	'	PART 1
	Field systemRollBtn:MenuButton
	Field options:Stack<MenuButton>
	Field text:Stack<String>
	Field startflag:bool
	
	Field crewa:Crew
	Field cstatA:CrewStatusLarge
	Field crewaReroll:MenuButton
	Field crewb:Crew
	Field cstatB:CrewStatusLarge
	Field crewbReroll:MenuButton
	Field info:InfoPane
	Field statPanel:Panel
	
	Method New()
		Init()
		diffBtn = New MenuButton(ScreenWidth / 2 - 75, 500, 160, 56)
		shipBtn = New MenuButton(ScreenWidth / 2 - 75, 560, 160, 56)
		startBtn = New MenuButton(ScreenWidth / 2 - 75, 620, 160, 56)
		Local i:Incrementer = New Incrementer(0, 165)
		options = New Stack<MenuButton>()
		text = New Stack<String>()
		Local b:MenuButton = New MenuButton(i.getNext(), 0, 160, 56)
		b.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		options.Push(b)
		text.Push("Ship")
		b = New MenuButton(i.getNext(), 0, 160, 56)
		b.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		options.Push(b)
		text.Push("Crew")
		b = New MenuButton(i.getNext(), 0, 160, 56)
		b.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		options.Push(b)
		text.Push("Done")
		crewa = New Crew(-1, -1, Null, RaceLoader.GetRace("ANY", "ANY", "PLAYABLE"))
		crewa.skills.RandomLevel(3, False)
		crewa.health = crewa.GetMaxHp()
		cstatA = New CrewStatusLarge(50, 250, crewa)
		crewaReroll = New MenuButton(50, 190, 160, 56)
		crewaReroll.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		crewb = New Crew(-1, -1, Null, RaceLoader.GetRace("ANY", "ANY", "PLAYABLE"))
		crewb.skills.RandomLevel(3, False)
		crewb.health = crewb.GetMaxHp()
		cstatB = New CrewStatusLarge(600, 250, crewb)
		crewbReroll = New MenuButton(600, 190, 160, 56)
		crewbReroll.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		
		systemRollBtn = New MenuButton(750, 820, 160, 56)
		startBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		diffBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		shipBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		systemRollBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		name = New InputBox(ScreenWidth / 2 - 150, 400, 300, 50, "", "Name")
		info = New InfoPane()
		info.ship2 = New Ship(20, 100, False)
		statPanel = New Panel(600, 800, info)
		statPanel.Attach(New SimpleShipA(350, 300, info, True))
		statPanel.Attach(New SimpleShipB(350, 300, info, True))
		statPanel.Attach(New SimpleShipC(350, 300, info, True))
		viewer = New ShipScroller(360, 100, False, New Economy(), EntityLoader.GetMemberList("New Game", "ANY", 0), info)
		viewer.SetEquipment(viewer.x + viewer.w + 20, viewer.y, 5, 4)
		viewer.AddButton(viewer.x, viewer.y + viewer.h, ShipScroller.LAST_SHIP, "Last")
		viewer.AddButton(viewer.x + ShipScroller.BTN_W + 10, viewer.y + viewer.h, ShipScroller.VIEW_EQUIPMENT, "Loadout", "Close")
		viewer.AddButton(viewer.x + (ShipScroller.BTN_W * 2) + 20, viewer.y + viewer.h, ShipScroller.NEXT_SHIP, "Next")
		info.Attach(viewer)
		info.Update()
		Player = New PlayerData(Seed, "")
		Player.galaxy.Start()
	End
	Method Init:Int()

		Return 0
	End
	
	Method Update:Int()
		If Game.Keys.KeyHit(KEY_ESCAPE)
			Game.SwitchScreen(Screens.Root)
		End

		For Local temp:Int = 0 Until options.Length()
			options.Get(temp).Poll
			If options.Get(temp).hit
				mode = temp
			End
		Next
		
		If mode = SHIP_SELECTION
			info.Update()
			
		
		
		Else If mode = CREW_SELECTION
			crewaReroll.Poll()
			If crewaReroll.hit
				crewa = New Crew(-1, -1, Null, RaceLoader.GetRace("ANY", "ANY", "PLAYABLE"))
				crewa.skills.RandomLevel(3, False)
				crewa.health = crewa.GetMaxHp()
				cstatA.member = crewa
			End
			crewbReroll.Poll()
			If crewbReroll.hit
				crewb = New Crew(-1, -1, Null, RaceLoader.GetRace("ANY", "ANY", "PLAYABLE"))
				crewb.skills.RandomLevel(3, False)
				crewb.health = crewb.GetMaxHp()
				cstatB.member = crewb
			End
		Else If mode = DONE
			startBtn.Poll()
			
			name.Update()
			If startBtn.hit
				If Loader.isValidName(name.GetText())
					startflag = True
					Player.accountName = name.GetText()
					Player.galaxy.Update()
					Player.map.economy = Player.galaxy.nodes.Get(0).economy
					Player.ship = New Ship(100, 100, False)
					Player.ship.OnLoad(viewer.GetShip().OnSave())
					Player.ship.AddCrew(crewa)
					Player.ship.AddCrew(crewb)
					Player.ship.SetCrewAi()
					Player.panel = New ShipPanel()
					Player.ship.clock = Player.clock
					Player.panel = New ShipPanel()
					Player.map.system = 0
					Player.map.hexField = New HexField(Player.map.economy, 0, 0, Player.nameGen.GetName(0))
					Player.map.hexField.BuildSystem()
					Player.map.GotoPlanet()
					Player.addCredits(10000)
					Loader.SaveGame(Player.accountName, Player)
					Loader.SaveGame(Player.accountName, Player.map.hexField)
					Player.accounting.current = New ReportItem(Player.ship, Player.map.economy, Player.accounting.bills)
					Game.SwitchScreen(Screens.SystemMap)
				Else
					If name.GetText().Length() = 0
						ThrowErrorMessage("Missing Name", "")
					Else
						ThrowErrorMessage("Invalid Name", "")
					End
				End
			End
		End
		Return 0
	End

	
	Method Render:Int()
		For Local temp:Int = 0 Until options.Length()
			Local b:MenuButton = options.Get(temp)
			b.RenderScaled(0, 0, 0.75)
			Game.white_font.Draw(text.Get(temp), b.x + b.w / 2, b.y + b.h / 6, 0.5)
		Next
		
		If mode = SHIP_SELECTION
			info.Render()
		Else If mode = CREW_SELECTION
			cstatA.Render()
			cstatB.Render()
			crewaReroll.RenderScaled(0, 0, 0.75)
			Game.white_font.Draw("Reroll", crewaReroll.x + crewaReroll.w / 2, crewaReroll.y + crewaReroll.h / 6, 0.5)
			crewbReroll.RenderScaled(0, 0, 0.75)
			Game.white_font.Draw("Reroll", crewbReroll.x + crewbReroll.w / 2, crewbReroll.y + crewbReroll.h / 6, 0.5)
		Else If mode = DONE
			If name.text.Length() > 0 And Not startflag
				If Loader.isValidName(name.text)
					name.Render()
				Else
					name.Render([255, 0, 0])
				End
			Else
				name.Render()
			End
			startBtn.RenderScaled(0, 0, 0.75)
			Game.white_font.Draw("Start", startBtn.x + startBtn.w / 2, startBtn.y + startBtn.h / 6, 0.5)
		End
		
		
		Return 0
		
	End

End

Class SelectionChoice
	
End