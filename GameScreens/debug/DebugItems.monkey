Strict

Import space_game
Import UIStuff.inventorymanager
Import UIStuff.storage

	
Class DebugItemsScreen Extends nScreen
	Const GENERATE_DAMAGED:Bool = False
	Field grab:GrabItem
	Field mapBtn:MenuButton
	Field shipBtn:MenuButton
	Field crewBtn:MenuButton
	Field equipBtn:MenuButton
	
	'Field shipTest:Ship
	Field grid:InventoryManager
	Field store:Storage
	Field otherGrid:InventoryManager
	
	Method New()
		grab = New GrabItem()
		mapBtn = New MenuButton(0, 0, 160, 56)
		shipBtn = New MenuButton(170, 0, 160, 56)
		equipBtn = New MenuButton(340, 0, 160, 56)
		crewBtn = New MenuButton(510, 0, 160, 56)
		mapBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		shipBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		equipBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		
	
	
		'shipTest = New Ship(20, 100, False)
	'	shipTest = Player.ship
		Local debug:Stack<Item> = New Stack<Item>
		Local itemList:ItemStack = ItemLoader.GetItemList()
		For Local getItem:ItemData = EachIn itemList
			Local data:JsonObject = ItemLoader.GetItem(getItem.id)
			Local i:Item = New Item(data)
			i.count = i.max
			debug.Push(i)
			If GENERATE_DAMAGED And i.type < Item.FUEL And i.type <> Item.SYSTEM
				For Local yy:Int = 10 To 50 Step 10
					Local ii:Item = New Item(data, yy, 0)
					debug.Push(ii)
				Next
			End
			If i.hasQualityLevel
				For Local xx:Int = 1 To 7
					Local j:Item = New Item(data, 0, xx)
					debug.Push(j)
					If GENERATE_DAMAGED
						For Local yy:Int = 10 To 50 Step 10
							Local ii:Item = New Item(data, yy, xx)
							debug.Push(ii)
						Next
					End
				Next
			End
		Next
		Local t:Inventory = New Inventory(debug.Length())
		t.slots = debug.ToArray()
		Print "item number: " + debug.Length()
		Local pd:PassData = New PassData(PassData.INVENTORY, PassData.STORE)
		grid = New InventoryManager(100, 100, 5, 6, pd, Player.ship.items, grab)
		pd = New PassData(PassData.STORE, PassData.INVENTORY)
		store = New Storage(900, 100, 5, 6, pd, t, grab, Storage.DEBUG_INFINITE)
	End
	Method Init:Int()
		Return 0
	End
	
	
	Method Update:Int()
		mapBtn.Poll()
		If mapBtn.hit
			Game.SwitchScreen(Screens.SystemMap)
		End
		shipBtn.Poll()
		If shipBtn.hit
			Game.SwitchScreen(Screens.ShipStatus)
		End
		equipBtn.Poll()
		If equipBtn.hit
			Game.SwitchScreen(Screens.Equipment)
		End
	
	
	
		
		store.Update()
		grid.Update()
		grab.Update()
		
		
		If Game.Keys.KeyHit(KEY_ESCAPE)
			Game.SwitchScreen(Screens.Root)
		End
		Return 0
	End
	
	
	Method Render:Int()
		mapBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Map", mapBtn.x + mapBtn.w / 2, mapBtn.y + mapBtn.h / 6, 0.5)
		shipBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Ship", shipBtn.x + shipBtn.w / 2, shipBtn.y + shipBtn.h / 6, 0.5)
		equipBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Equip", equipBtn.x + equipBtn.w / 2, equipBtn.y + equipBtn.h / 6, 0.5)
	
	
		
		store.Render()
		grid.Render()
		grab.Render()
		If Game.showFPS
			DrawFPS()
		End
		Return 0
	
	End
End
