Strict

Import space_game
Import UIStuff.inventorymanager
Import UIStuff.equipmentmanager
	
Class EquipScreen Extends nScreen
	Field shipTest:Ship
	Field grid:InventoryManager
	Field equips:EquipmentManager
	Field grab:GrabItem
	Field crewGrab:Crew
	Method New()
		grab = New GrabItem()
		Player.menu.LockMenu()
		shipTest = New Ship(20, 100, False)
		shipTest = Player.ship
		Local pd:PassData = New PassData(PassData.INVENTORY, PassData.EQUIPMENT)
		grid = New InventoryManager(1500, 100, 5, 3, pd, shipTest.items, grab)
		pd = New PassData(PassData.EQUIPMENT, PassData.INVENTORY)
		equips = New EquipmentManager(800, 100, 5, 5, pd, shipTest, grab)
	End
	Method Init:Int()
		Return 0
	End
	
	
	Method Update:Int()
		Player.menu.Update()
		If Player.menu.isLockTriggered()
			If grab.item <> Null
				grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.EQUIPMENT))
				equips.Update()
				If grab.success <> True
					grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.INVENTORY))
					grid.Update()
				End
			End
			Player.ship.UpdateMods()
			Player.panel.UpdateHotbar()
			Player.menu.SwitchKey()
			Player.menu.UnlockMenu()
		End
		If Game.Keys.KeyHit(KEY_ESCAPE)
			If grab.item <> Null
				grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.EQUIPMENT))
				equips.Update()
				If grab.success <> True
					grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.INVENTORY))
					grid.Update()
				End
			End
			Player.ship.UpdateMods()
			Player.panel.UpdateHotbar()
			Player.menu.UnlockMenu()
			Game.SwitchScreen(Screens.Root)
		End
	
		
	
	
		'This order is important
		grid.Update()
		If equips.Update()
			shipTest.Update(True)
		Else If not Player.panel.Update()
			shipTest.Update()
		End
		grab.Update()
		'This order is important
		
		Game.OverParticle.Update()
		Game.UnderParticle.Update()
		
		'CODE for ship interaction
		If Game.Cursor.Hit
			For Local temp:Int = 0 Until shipTest.crew.Length()
				If shipTest.crew.Get(temp).isMouseOver() Then crewGrab = shipTest.crew.Get(temp)
			Next
			For Local x:Int = 0 Until Ship.MAX_X
				For Local y:Int = 0 Until Ship.MAX_Y
					If shipTest.pipes[x * Ship.MAX_X + y].door or shipTest.pipes[x * Ship.MAX_X + y].airlock
						If shipTest.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
							If shipTest.pipes[x * Ship.MAX_X + y].doorOpen = True
								shipTest.pipes[x * Ship.MAX_X + y].doorOpen = False
							Else
								shipTest.pipes[x * Ship.MAX_X + y].doorOpen = True
							End
						End
					End
				Next
			Next
		End
		If crewGrab <> Null
			If Game.Cursor.RightHit()
				For Local x:Int = 0 Until Ship.MAX_X
					For Local y:Int = 0 Until Ship.MAX_Y
						If shipTest.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
							crewGrab.order_x = x
							crewGrab.order_y = y
						End
					Next
				Next
			End
		End
		'END CODE for ship interaction
		
		Return 0
	End
	
	
	Method Render:Int()
		Game.UnderParticle.Render()
		Player.menu.Render()
	
	
		DrawRectOutline(shipTest.x_offset, shipTest.y_offset, shipTest.MAX_X * shipTest.GRID_SIZE, shipTest.MAX_Y * shipTest.GRID_SIZE)
		shipTest.Render(, False)
		
		Player.panel.Render(True)
		Game.white_font.Draw("RAN: " + shipTest.GetRange(), 100, 725)
		Game.white_font.Draw("FOD: " + shipTest.items.CountItem(Item.FOOD), 100, 750)		
		
		'this order is important
		equips.Render()
		grid.Render()
		'this order is important
		
		grab.Render()
		If Game.showFPS
			DrawFPS()
		End
		Game.OverParticle.Render()
		Return 0
	
	End
End

