Strict

Import space_game
Import UIStuff.inventorymanager

	
Class ShipStatusScreen Extends nScreen
	Field grab:GrabItem

	Field shipTest:Ship
	Field grid:InventoryManager
	Field crewGrab:Crew
	Method New()
		grab = New GrabItem()
		Player.menu.LockMenu()
	
		shipTest = New Ship(20, 100, False)
		shipTest = Player.ship 
		Local pd:PassData = New PassData(PassData.INVENTORY, PassData.NO_TARGET)
		grid = New InventoryManager(750, 100, 5, 9, pd, shipTest.items, grab)
	End
	Method Init:Int()
		Return 0
	End
	
	
	Method Update:Int()
		Player.menu.Update()
		Player.panel.BackgroundUpdate()
		If Player.panel.Update() = False
			shipTest.Update()
		End

		
		If Player.menu.isLockTriggered()
			If grab.item <> Null
				grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.INVENTORY))
				grid.Update()
			End
			Player.menu.SwitchKey()
			Player.menu.UnlockMenu()
		End
	
	
		'this order is importnt
		grid.Update()
		grab.Update()
		'this order is important
		
		
		
		'CODE for ship interaction
		If Game.Cursor.Hit
			For Local temp:Int = 0 Until shipTest.crew.Length()
				If shipTest.crew.Get(temp).isMouseOver() Then crewGrab = shipTest.crew.Get(temp)
			Next
			For Local x:Int = 0 Until Ship.MAX_X
				For Local y:Int = 0 Until Ship.MAX_Y
					If shipTest.pipes[x * Ship.MAX_X + y].door or shipTest.pipes[x * Ship.MAX_X + y].airlock
						If shipTest.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
							If shipTest.pipes[x * Ship.MAX_X + y].doorOpen = True
								shipTest.pipes[x * Ship.MAX_X + y].doorOpen = False
							Else
								shipTest.pipes[x * Ship.MAX_X + y].doorOpen = True
							End
						End
					End
				Next
			Next
		End
		If crewGrab <> Null
			If Game.Cursor.RightHit()
				For Local x:Int = 0 Until Ship.MAX_X
					For Local y:Int = 0 Until Ship.MAX_Y
						If shipTest.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
							crewGrab.order_x = x
							crewGrab.order_y = y
						End
					Next
				Next
			End
		End
		'END CODE for ship interaction
		
		If Game.Keys.KeyHit(KEY_ESCAPE)
			If grab.item <> Null
				grab.Pass(grab.Get(), New PassData(PassData.NO_TARGET, PassData.INVENTORY))
				grid.Update()
			End
			Player.menu.UnlockMenu()
			Game.SwitchScreen(Screens.Root)
		End
		Return 0
	End
	
	
	Method Render:Int()
		Game.UnderParticle.Render()
		Player.menu.Render()
	
	
		DrawRectOutline(shipTest.x_offset, shipTest.y_offset, shipTest.MAX_X * shipTest.GRID_SIZE, shipTest.MAX_Y * shipTest.GRID_SIZE)
		shipTest.Render(, False)
		grid.Render()
		
		Player.panel.Render(True)
		
		grab.Render()
		If Game.showFPS
			DrawFPS()
		End
		Game.OverParticle.Render()
		Return 0
	
	End
End
