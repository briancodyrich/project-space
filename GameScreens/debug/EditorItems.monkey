Strict

Import space_game
Import UIStuff.inventorymanager
Import UIStuff.storage

Class EditorItemsScreen Extends nScreen
	Const GENERATE_DAMAGED:Bool = false
	Field grab:GrabItem
	Field mapBtn:MenuButton


	'Field shipTest:Ship
	Field grid:InventoryManager
	Field equips:EquipmentManager
	Field store:Storage
	Field otherGrid:InventoryManager
	
	Method New()
		grab = New GrabItem()
		mapBtn = New MenuButton(0, 0, 160, 56)
		mapBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		'shipTest = New Ship(20, 100, False)
		'	shipTest = Player.ship
		Local debug:Stack<Item> = New Stack<Item>
		Local itemList:ItemStack = ItemLoader.GetItemList()
		For Local getItem:ItemData = EachIn itemList
			Local data:JsonObject = ItemLoader.GetItem(getItem.id)
			Local i:Item = New Item(data)
			i.count = i.max
			debug.Push(i)
			If GENERATE_DAMAGED And i.type < Item.FUEL And i.type <> Item.SYSTEM
				For Local yy:Int = 10 To 50 Step 10
					Local ii:Item = New Item(data, yy, 0)
					debug.Push(ii)
				Next
			End
			If i.hasQualityLevel
				For Local xx:Int = 1 To 7
					Local j:Item = New Item(data, 0, xx)
					debug.Push(j)
					If GENERATE_DAMAGED
						For Local yy:Int = 10 To 50 Step 10
							Local ii:Item = New Item(data, yy, xx)
							debug.Push(ii)
						Next
					End
				Next
			End
		Next
		Local t:Inventory = New Inventory(debug.Length())
		t.slots = debug.ToArray()
		Local pd:PassData = New PassData(PassData.EQUIPMENT, PassData.STORE)
		equips = New EquipmentManager(100, 100, 5, 5, pd, sandBoxShip, grab)
		pd = New PassData(PassData.INVENTORY, PassData.STORE)
		grid = New InventoryManager(900, 100, 5, 3, pd, sandBoxShip.items, grab)
		pd = New PassData(PassData.STORE, PassData.EQUIPMENT, PassData.INVENTORY)
		store = New Storage(1320, 100, 5, 4, pd, t, grab, Storage.DEBUG_INFINITE)
	End
	Method Init:Int()
		Return 0
	End
	
	
	Method Update:Int()
		mapBtn.Poll()
		If mapBtn.hit
			Game.SwitchScreen(Screens.Editor)
		End
	
	
	
		
		store.Update()
		equips.Update()
		grid.Update()
		grab.Update()
		
		
		If Game.Keys.KeyHit(KEY_ESCAPE)
			Game.SwitchScreen(Screens.Editor)
		End
		Return 0
	End
	
	
	Method Render:Int()
		mapBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Editor", mapBtn.x + mapBtn.w / 2, mapBtn.y + mapBtn.h / 6, 0.5)
	
		store.Render()
		equips.Render()
		grid.Render()
		grab.Render()
		If Game.showFPS
			DrawFPS()
		End
		Return 0
	
	End
End
