Strict

Import space_game
Import os
Import UIStuff.inputbox

Global sandBoxShip:Ship

Class EditorScreen Extends nScreen
	Const NORMAL_VIEW:Int = 0
	Const POWER_VIEW:Int = 1
	Global lastName:String = ""
	Field shipTest:Ship
	
	Field area:Economy
	'// top toolbar
	Field shipviewBtn:MenuButton
	Field powerviewBtn:MenuButton
	Field heatviewBtn:MenuButton
	Field oxygenViewBtn:MenuButton
	Field sandboxBtn:MenuButton
	Field loadoutBtn:MenuButton
	Field minusCrewBtn:MenuButton
	Field plusCrewBtn:MenuButton
	Field exportBtn:MenuButton
	Field loadBtn:MenuButton
	
	Field plusStorage:MenuButton
	Field minusStorage:MenuButton
	Field plusHeat:MenuButton
	Field minusHeat:MenuButton
	Field plusSlots:MenuButton
	Field minusSlots:MenuButton
	Field plusArmor:MenuButton
	Field minusArmor:MenuButton
	Field plusMass:MenuButton
	Field minusMass:MenuButton
	Field plusValue:MenuButton
	Field minusValue:MenuButton
	
	'// editor buttons
	Field brush:PaintBrush
	Field compButtonStack:Stack<ComponentButton>
	Field tileButtonStack:Stack<TileButton>
	Field weaponButtonStack:Stack<WeaponButton>
	Field roomBtn:MenuButton
	Field compBtn:MenuButton
	Field weaponBtn:MenuButton
	
'	Field min:Int
'	Field max:Int = 7
	Field shipName:InputBox
	
	Field engineP:SimPanel
	Field reactorP:SimPanel
	Field shieldP:SimPanel
	
	Field ai:AIPicker
	
	Field view:int
	Method New()
		ai = New AIPicker()
		engineP = New SimPanel(700, 550, 2)
		reactorP = New SimPanel(700, 600, 2)
		shieldP = New SimPanel(700, 650, 1)
		area = New Economy()
		Game.SetRootScreen(Screens.Editor)
		Game.white_font.Resize(0.5)
		shipName = New InputBox(1020, 75, 300, 50, lastName, "File")
		shipTest = New Ship(20, 100, False)
		If LoadState() <> ""
			If sandBoxShip = Null
				Local obj:JsonObject = New JsonObject(LoadState())
				shipTest.OnLoad(obj)
				sandBoxShip = New Ship(20, 100, False)
			Else
				shipTest.OnLoad(sandBoxShip.OnSave())
			End
			
		End
		shipviewBtn = New MenuButton(0, 0, 160, 56)
		shipviewBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		powerviewBtn = New MenuButton(170, 0, 160, 56)
		powerviewBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		heatviewBtn = New MenuButton(340, 0, 160, 56)
		heatviewBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		oxygenViewBtn = New MenuButton(510, 0, 160, 56)
		oxygenViewBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		sandboxBtn = New MenuButton(680, 0, 160, 56)
		sandboxBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		loadoutBtn = New MenuButton(850, 0, 160, 56)
		loadoutBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		minusCrewBtn = New MenuButton(680, 65, 160, 56)
		minusCrewBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		plusCrewBtn = New MenuButton(850, 65, 160, 56)
		plusCrewBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		plusStorage = New MenuButton(850, 130, 160, 56)
		plusStorage.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		minusStorage = New MenuButton(680, 130, 160, 56)
		minusStorage.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		plusSlots = New MenuButton(850, 195, 160, 56)
		plusSlots.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		minusSlots = New MenuButton(680, 195, 160, 56)
		minusSlots.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		plusArmor = New MenuButton(850, 260, 160, 56)
		plusArmor.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		minusArmor = New MenuButton(680, 260, 160, 56)
		minusArmor.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		plusHeat = New MenuButton(850, 325, 160, 56)
		plusHeat.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		minusHeat = New MenuButton(680, 325, 160, 56)
		minusHeat.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		plusMass = New MenuButton(850, 390, 160, 56)
		plusMass.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		minusMass = New MenuButton(680, 390, 160, 56)
		minusMass.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		plusValue = New MenuButton(850, 455, 160, 56)
		plusValue.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		minusValue = New MenuButton(680, 455, 160, 56)
		minusValue.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		
		
		
		exportBtn = New MenuButton(1020, 0, 160, 56)
		exportBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		loadBtn = New MenuButton(1190, 0, 160, 56)
		loadBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		
		roomBtn = New MenuButton(0, 900, 160, 56)
		roomBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		compBtn = New MenuButton(0, 960, 160, 56)
		compBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		weaponBtn = New MenuButton(0, 1020, 160, 56)
		weaponBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		
		view = Tile.NORMAL_VIEW
		brush = New PaintBrush(shipTest)
		compButtonStack = New Stack<ComponentButton>
		tileButtonStack = New Stack<TileButton>
		weaponButtonStack = New Stack<WeaponButton>
		Local getComp:ItemStack = ItemLoader.GetPartList()
		Local x:Int = 300
		Local y:int = 960
		For Local temp:Int = 0 Until getComp.Length()
			compButtonStack.Push(New ComponentButton(x, y, ItemLoader.GetItem(getComp.Get(temp).id)))
			x += 40
		Next
		Local getWeap:ItemStack = ItemLoader.GetItemList(Item.WEAPON)
		x = 300
		y = 1020
		For Local temp:Int = 0 Until getWeap.Length()
			weaponButtonStack.Push(New WeaponButton(x, y, ItemLoader.GetItem(getWeap.Get(temp).id)))
			x += 40
		Next
		Local i:Incrementer = New Incrementer(300, 40)
		tileButtonStack.Push(New TileButton(i.getNext(), 900, "floor", Tile.img_floor))
		tileButtonStack.Push(New TileButton(i.getNext(), 900, "hull", Tile.img_hull))
		tileButtonStack.Push(New TileButton(i.getNext(), 900, "door", Tile.img_door_NS))
		tileButtonStack.Push(New TileButton(i.getNext(), 900, "airlock", Tile.img_air_lock_CLOSED))
		tileButtonStack.Push(New TileButton(i.getNext(), 900, "scaffolding", Tile.img_scaffolding))
		tileButtonStack.Push(New TileButton(i.getNext(), 900, "flag", Tile.img_bed_N, FlagCode.BED_N))
		tileButtonStack.Push(New TileButton(i.getNext(), 900, "flag", Tile.img_bed_S, FlagCode.BED_S))
		tileButtonStack.Push(New TileButton(i.getNext(), 900, "flag", Tile.img_bed_E, FlagCode.BED_E))
		tileButtonStack.Push(New TileButton(i.getNext(), 900, "flag", Tile.img_bed_W, FlagCode.BED_W))
	'	tileButtonStack.Push(New TileButton(600, 900, "walkway", Tile.img_scaffolding))
		

	End
	Method Init:Int()
		Return 0
	End
	
	Method Update:Int()
		shipName.Update()
		shipTest.shield_overlay.SetShip(shipTest)
		lastName = shipName.GetText()
		brush.Update()
		brush.RemoveInvalid()
		engineP.Update()
		reactorP.Update()
		shieldP.Update()
		
		If Game.Keys.KeyHit(KEY_SPACE)
			'shipTest.GetAiChoice(ai)
			ai.PrintAll()
		End
		
		If Game.Keys.KeyHit(KEY_ESCAPE)
			Game.SwitchScreen(Screens.Start)
		End
		
		If Game.Keys.KeyHit(KEY_RIGHT)
			For Local x:Int = (Ship.MAX_X - 1) To 0 Step - 1
				For Local y:Int = 0 Until Ship.MAX_Y
					If x = Ship.MAX_X
						shipTest.pipes[x * Ship.MAX_X + y] = New Tile()
					Else If x = 0
					
					Else
						shipTest.pipes[x * Ship.MAX_X + y] = shipTest.pipes[ (x - 1) * Ship.MAX_X + y]
					End
				Next
			Next
		End
		
		If Game.Keys.KeyHit(KEY_LEFT)
			For Local x:Int = 0 Until Ship.MAX_X
				For Local y:Int = 0 Until Ship.MAX_Y
					If x = Ship.MAX_X - 1
						shipTest.pipes[ (x - 1) * Ship.MAX_X + y] = New Tile()
					Else
						shipTest.pipes[x * Ship.MAX_X + y] = shipTest.pipes[ (x + 1) * Ship.MAX_X + y]
					End
				Next
			Next
		End
		shipviewBtn.Poll()
		If shipviewBtn.hit
			view = Tile.NORMAL_VIEW
			brush.componentName = brush.last
		End
		powerviewBtn.Poll()
		If powerviewBtn.hit
			view = Tile.ELECTRIC_VIEW
			If brush.componentName <> "" Then brush.last = brush.componentName
			brush.componentName = ""
		End
		heatviewBtn.Poll()
		If heatviewBtn.hit
			view = Tile.HEAT_VIEW
			If brush.componentName <> "" Then brush.last = brush.componentName
			brush.componentName = ""
		End
		oxygenViewBtn.Poll()
		If oxygenViewBtn.hit
			view = Tile.OXYGEN_VIEW
			If brush.componentName <> "" Then brush.last = brush.componentName
			brush.componentName = ""
		End
		
		sandboxBtn.Poll()
		If sandboxBtn.hit
			Local s:JsonObject = shipTest.OnSave()
			SaveState(s.ToJson())
			sandBoxShip = New Ship(20, 100, False)
			sandBoxShip.OnLoad(s)
			Game.SwitchScreen(Screens.TestGame)
		End
		plusCrewBtn.Poll()
		If plusCrewBtn.hit
			shipTest.crewSlots += 1
		End
		minusCrewBtn.Poll()
		If minusCrewBtn.hit
			shipTest.crewSlots = Max(shipTest.crewSlots - 1, 0)
		End
		plusHeat.Poll()
		If plusHeat.hit
			shipTest.heatRelease += 5
		End
		minusHeat.Poll()
		If minusHeat.hit
			shipTest.heatRelease = Max(shipTest.heatRelease - 5, 0)
		End
		
		plusStorage.Poll()
		If plusStorage.hit
			shipTest.storageSpace += 3
		End
		minusStorage.Poll()
		If minusStorage.hit
			shipTest.storageSpace = Max(shipTest.storageSpace - 3, 0)
		End
		plusSlots.Poll()
		If plusSlots.hit
			shipTest.systemSlots += 1
			UpdateShip()
		End
		minusSlots.Poll()
		If minusSlots.hit
			shipTest.systemSlots = Max(shipTest.systemSlots - 1, 0)
			UpdateShip()
		End
		plusArmor.Poll()
		If plusArmor.hit
			shipTest.maxAP += 5
		End
		minusArmor.Poll()
		If minusArmor.hit
			shipTest.maxAP = Max(shipTest.maxAP - 5, 0)
		End
		plusMass.Poll()
		If plusMass.hit
			shipTest.mass += 5
		End
		minusMass.Poll()
		If minusMass.hit
			shipTest.mass -= 5
		End
		plusValue.Poll()
		If plusValue.hit
			shipTest.value += 5000
		End
		minusValue.Poll()
		If minusValue.hit
			shipTest.value -= 5000
		End
		
		'DATA BUTTONS
		If KeyHit(KEY_DELETE)
			For Local temp:Int = 0 Until shipTest.pipes.Length()
				shipTest.pipes[temp] = New Tile()
			Next
			shipTest.comp.Clear()
			shipTest.heatRelease = 0
			shipTest.storageSpace = 0
			shipTest.maxAP = 0
			shipTest.systemSlots = 0
			shipTest.crewSlots = 0
			shipTest.items = New Inventory(0)
			shipTest.systems = New Item[shipTest.systemSlots]
			lastName = ""
			shipName.text = ""
			UpdateShip()
		End
		
		exportBtn.Poll()
		If exportBtn.hit And shipName.GetText() <> ""
			'shipTest.compSlots.Clear()
			Local s:JsonObject = shipTest.OnSave()
			SaveState(s.ToJson())
			SaveString(s.ToJson(), shipName.GetText() + ".ship")
			shipTest.OnLoad(s)
		End
		loadBtn.Poll()
		If loadBtn.hit And shipName.GetText() <> ""
			Local str:String = os.LoadString(shipName.GetText() + ".ship")
			Print "+" + shipName.GetText() + ".ship" + "+"
			Print str
			If str <> ""
				Local s:JsonObject = JsonObject(str)
				If s <> Null
					For Local temp:Int = 0 Until shipTest.pipes.Length()
						shipTest.pipes[temp] = New Tile()
					Next
					shipTest.compSlots.Clear()
					shipTest.comp.Clear()
					shipTest.OnLoad(s)
					SaveState(s.ToJson())
				Else
					Print "null"
				End
			End
		End
		
		loadoutBtn.Poll()
		If loadoutBtn.hit
			shipTest.compSlots.Clear()
			Local s:JsonObject = shipTest.OnSave()
			SaveState(s.ToJson())
			sandBoxShip = New Ship(20, 100, False)
			sandBoxShip.OnLoad(s)
			Game.SwitchScreen(Screens.EditorItems)
		End
		
		roomBtn.Poll()
		If roomBtn.hit
			view = Tile.NORMAL_VIEW
			brush.current = PaintBrush.ROOM
			brush.componentName = ""
		End
		
		compBtn.Poll()
		If compBtn.hit
			view = Tile.NORMAL_VIEW
			brush.current = PaintBrush.COMPONENT
			brush.componentName = ""
		End
		
		weaponBtn.Poll()
		If weaponBtn.hit
			view = Tile.NORMAL_VIEW
			brush.current = PaintBrush.WEAPON
			brush.componentName = ""
		End

		If view = Tile.ELECTRIC_VIEW
			If Game.Cursor.Down
				For Local x:Int = 0 Until Ship.MAX_X
					For Local y:Int = 0 Until Ship.MAX_Y
						If shipTest.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
							shipTest.pipes[x * Ship.MAX_X + y].power = True
						End
					Next
				Next
			ElseIf Game.Cursor.RightDown
				For Local x:Int = 0 Until Ship.MAX_X
					For Local y:Int = 0 Until Ship.MAX_Y
						If shipTest.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
							shipTest.pipes[x * Ship.MAX_X + y].power = False
						End
					Next
				Next
			End
		End
		If view = Tile.HEAT_VIEW
			If Game.Cursor.Down
				For Local x:Int = 0 Until Ship.MAX_X
					For Local y:Int = 0 Until Ship.MAX_Y
						If shipTest.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
							shipTest.pipes[x * Ship.MAX_X + y].coolant = True
						End
					Next
				Next
			ElseIf Game.Cursor.RightDown
				For Local x:Int = 0 Until Ship.MAX_X
					For Local y:Int = 0 Until Ship.MAX_Y
						If shipTest.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
							shipTest.pipes[x * Ship.MAX_X + y].coolant = False
						End
					Next
				Next
			End
		End
		If view = Tile.OXYGEN_VIEW
			#rem
			If Game.Cursor.Down
				For Local x:Int = 0 Until Ship.MAX_X
					For Local y:Int = 0 Until Ship.MAX_Y
						If shipTest.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
							shipTest.pipes[x * Ship.MAX_X + y].shield = True
						End
					Next
				Next
			ElseIf Game.Cursor.RightDown
				For Local x:Int = 0 Until Ship.MAX_X
					For Local y:Int = 0 Until Ship.MAX_Y
						If shipTest.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
							shipTest.pipes[x * Ship.MAX_X + y].shield = False
						End
					Next
				Next
			End
			#end
		End
		
		If brush.current = PaintBrush.ROOM
			If Game.Cursor.Down
				If brush.componentName <> ""
					For Local x:Int = 0 Until Ship.MAX_X
						For Local y:Int = 0 Until Ship.MAX_Y
							If shipTest.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
								If brush.isValidPlacement(x, y, brush.type)
									Select brush.componentName
										Case "flag"
											shipTest.pipes[x * Ship.MAX_X + y].flag = True
											shipTest.pipes[x * Ship.MAX_X + y].code = brush.code
										Case "floor"
											shipTest.pipes[x * Ship.MAX_X + y].floor = True
											shipTest.pipes[x * Ship.MAX_X + y].hull = False
											shipTest.pipes[x * Ship.MAX_X + y].door = False
											shipTest.pipes[x * Ship.MAX_X + y].scaffolding = False
											shipTest.pipes[x * Ship.MAX_X + y].airlock = False
										Case "hull"
											shipTest.pipes[x * Ship.MAX_X + y].floor = False
											shipTest.pipes[x * Ship.MAX_X + y].hull = True
											shipTest.pipes[x * Ship.MAX_X + y].door = False
											shipTest.pipes[x * Ship.MAX_X + y].scaffolding = False
											shipTest.pipes[x * Ship.MAX_X + y].airlock = False
										Case "door"
											shipTest.pipes[x * Ship.MAX_X + y].floor = False
											shipTest.pipes[x * Ship.MAX_X + y].hull = False
											shipTest.pipes[x * Ship.MAX_X + y].door = True
											shipTest.pipes[x * Ship.MAX_X + y].scaffolding = False
											shipTest.pipes[x * Ship.MAX_X + y].airlock = False
										Case "scaffolding"
											shipTest.pipes[x * Ship.MAX_X + y].floor = False
											shipTest.pipes[x * Ship.MAX_X + y].hull = False
											shipTest.pipes[x * Ship.MAX_X + y].door = False
											shipTest.pipes[x * Ship.MAX_X + y].scaffolding = True
											shipTest.pipes[x * Ship.MAX_X + y].airlock = False
										Case "airlock"
											shipTest.pipes[x * Ship.MAX_X + y].floor = False
											shipTest.pipes[x * Ship.MAX_X + y].hull = False
											shipTest.pipes[x * Ship.MAX_X + y].door = False
											shipTest.pipes[x * Ship.MAX_X + y].scaffolding = False
											shipTest.pipes[x * Ship.MAX_X + y].airlock = True
										Case "walkway"
											shipTest.pipes[x * Ship.MAX_X + y].walkway = True
									End
								End
							End
						Next
					Next
				End
			ElseIf view = Tile.NORMAL_VIEW And Game.Cursor.RightDown
				For Local x:Int = 0 Until Ship.MAX_X
					For Local y:Int = 0 Until Ship.MAX_Y
						If shipTest.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
							shipTest.pipes[x * Ship.MAX_X + y].flag = False
							shipTest.pipes[x * Ship.MAX_X + y].code = 0
							shipTest.pipes[x * Ship.MAX_X + y].floor = False
							shipTest.pipes[x * Ship.MAX_X + y].hull = False
							shipTest.pipes[x * Ship.MAX_X + y].door = False
							shipTest.pipes[x * Ship.MAX_X + y].scaffolding = False
						End
					Next
				Next
			End
		End
		'//
		
		If brush.current = PaintBrush.COMPONENT
			For Local temp:Int = 0 Until compButtonStack.Length()
				If compButtonStack.Get(temp).isMouseOver()
					If Game.Cursor.Hit()
						brush.componentName = compButtonStack.Get(temp).getName()
						brush.img = compButtonStack.Get(temp).getImage()
						brush.type = compButtonStack.Get(temp).getRules()
						Print compButtonStack.Get(temp).getName()
					End
				End
			Next	
		End
		If brush.current = PaintBrush.ROOM
			If Game.Cursor.Hit
				For Local temp:Int = 0 Until tileButtonStack.Length()
					If tileButtonStack.Get(temp).isMouseOver()
						brush.componentName = tileButtonStack.Get(temp).getName()
						brush.img = tileButtonStack.Get(temp).getImage()
						brush.type = Component.PLACEMENT_ANYWHERE
						brush.code = tileButtonStack.Get(temp).code
						Print tileButtonStack.Get(temp).getName()
					End
				Next
			End
		End
		If brush.current = PaintBrush.WEAPON
			If Game.Cursor.Hit
				For Local temp:Int = 0 Until shipTest.comp.Length()
					If shipTest.comp.Get(temp).isMouseOver(shipTest.x_offset, shipTest.y_offset, shipTest.mirrored)
						If shipTest.comp.Get(temp).getType() = Component.HARDPOINT
							If ItemLoader.DoesItemExist(brush.weapon)
								Hardpoint(shipTest.comp.Get(temp).getSelf()).gun = New Weapon(ItemLoader.GetItem(brush.weapon))
							End
						End
					End
				Next
			End
		End
		
		If Game.Cursor.Hit
			For Local temp:Int = 0 Until weaponButtonStack.Length()
				If weaponButtonStack.Get(temp).isMouseOver()
					brush.weapon = weaponButtonStack.Get(temp).getName()
				End
			Next
		End
		
		
		
		Return 0
	End
	Method UpdateShip:Void()
		shipTest.compSlots.Clear()
		Local tSys:Item[] = shipTest.systems
		shipTest.systems = New Item[shipTest.systemSlots]
		If shipTest.systems.Length() > 0 And tSys.Length() > 0
			For Local temp:Int = 0 Until shipTest.systems.Length()
				If temp < tSys.Length()
					shipTest.systems[temp] = tSys[temp]
				End
			Next
		End
		shipTest.SetSlots()
	End
	
	Method Render:Int()
		#rem
		minBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Min " + min, minBtn.x + minBtn.w / 2, minBtn.y + minBtn.h / 6, 0.5)
		maxBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Max " + max, maxBtn.x + maxBtn.w / 2, maxBtn.y + maxBtn.h / 6, 0.5)
		#end
		
		engineP.Render()
		reactorP.Render()
		shieldP.Render()
		plusCrewBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Crew +", plusCrewBtn.x + plusCrewBtn.w / 2, plusCrewBtn.y + plusCrewBtn.h / 6, 0.5)
		minusCrewBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Crew -", minusCrewBtn.x + minusCrewBtn.w / 2, minusCrewBtn.y + minusCrewBtn.h / 6, 0.5)
		
		plusHeat.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Heat +", plusHeat.x + plusHeat.w / 2, plusHeat.y + plusHeat.h / 6, 0.5)
		minusHeat.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Heat -", minusHeat.x + minusHeat.w / 2, minusHeat.y + minusHeat.h / 6, 0.5)
		
		plusStorage.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Storage +", plusStorage.x + plusStorage.w / 2, plusStorage.y + plusStorage.h / 6, 0.5)
		minusStorage.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Storage -", minusStorage.x + minusStorage.w / 2, minusStorage.y + minusStorage.h / 6, 0.5)
		
		plusSlots.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Slots +", plusSlots.x + plusSlots.w / 2, plusSlots.y + plusSlots.h / 6, 0.5)
		minusSlots.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Slots -", minusSlots.x + minusSlots.w / 2, minusSlots.y + minusSlots.h / 6, 0.5)
		
		plusArmor.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Armor +", plusArmor.x + plusArmor.w / 2, plusArmor.y + plusArmor.h / 6, 0.5)
		minusArmor.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Armor -", minusArmor.x + minusArmor.w / 2, minusArmor.y + minusArmor.h / 6, 0.5)
		
		plusMass.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Mass +", plusMass.x + plusMass.w / 2, plusMass.y + plusMass.h / 6, 0.5)
		minusMass.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Mass -", minusMass.x + minusMass.w / 2, minusMass.y + minusMass.h / 6, 0.5)
		
		plusValue.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Value +", plusValue.x + plusValue.w / 2, plusValue.y + plusValue.h / 6, 0.5)
		minusValue.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Value -", minusValue.x + minusValue.w / 2, minusValue.y + minusValue.h / 6, 0.5)
		
		
		shipviewBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Normal", shipviewBtn.x + shipviewBtn.w / 2, shipviewBtn.y + shipviewBtn.h / 6, 0.5)
		shipviewBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Normal", shipviewBtn.x + shipviewBtn.w / 2, shipviewBtn.y + shipviewBtn.h / 6, 0.5)
		powerviewBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Power", powerviewBtn.x + powerviewBtn.w / 2, powerviewBtn.y + powerviewBtn.h / 6, 0.5)
		sandboxBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("SandBox", sandboxBtn.x + sandboxBtn.w / 2, sandboxBtn.y + sandboxBtn.h / 6, 0.5)
		loadoutBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Loadout", loadoutBtn.x + loadoutBtn.w / 2, loadoutBtn.y + loadoutBtn.h / 6, 0.5)
		
	
		
		heatviewBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Coolant", heatviewBtn.x + heatviewBtn.w / 2, heatviewBtn.y + heatviewBtn.h / 6, 0.5)
		oxygenViewBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Oxygen", oxygenViewBtn.x + oxygenViewBtn.w / 2, oxygenViewBtn.y + oxygenViewBtn.h / 6, 0.5)
		exportBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Save", exportBtn.x + exportBtn.w / 2, exportBtn.y + exportBtn.h / 6, 0.5)
		loadBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Load", loadBtn.x + loadBtn.w / 2, loadBtn.y + loadBtn.h / 6, 0.5)
		roomBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Rooms", roomBtn.x + roomBtn.w / 2, roomBtn.y + roomBtn.h / 6, 0.5)
		compBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Objects", compBtn.x + compBtn.w / 2, compBtn.y + compBtn.h / 6, 0.5)
		weaponBtn.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw("Weapons", weaponBtn.x + weaponBtn.w / 2, weaponBtn.y + weaponBtn.h / 6, 0.5)
		
		shipTest.Render(view, False)
		shipName.Render()
		
		shipTest.CalculateHP()
		shipTest.HealShip(True)
		
		Local i:Incrementer = New Incrementer(150, 30)
		PrettyStats(1100, i.getNext(), 150, "Dry Mass:", SigDig( (shipTest.GetMass(, True) / 1000.0), 2) + "t")
		PrettyStats(1100, i.getNext(), 150, "Mass:", SigDig( (shipTest.GetMass() / 1000.0), 2) + "t")
		PrettyStats(1100, i.getNext(), 150, "Mass%:", (100 + shipTest.mass))
		PrettyStats(1100, i.getNext(), 150, "S Mass%:", SuggestedMass(shipTest))
		PrettyStats(1100, i.getNext(), 150, "Hull:", (shipTest.maxHP))
		PrettyStats(1100, i.getNext(), 150, "Armor:", (shipTest.HP - shipTest.maxHP))
		PrettyStats(1100, i.getNext(), 150, "Armor%:", (shipTest.AP + "%"))
		PrettyStats(1100, i.getNext(), 150, "Defence:", (shipTest.defence + "%"))
		PrettyStats(1100, i.getNext(), 150, "T/W:", SigDig(shipTest.GetThrust(, True), 2))
		PrettyStats(1100, i.getNext(), 150, "Crew Slots:", shipTest.crewSlots)
		PrettyStats(1100, i.getNext(), 150, "Storage:", shipTest.storageSpace)
		PrettyStats(1100, i.getNext(), 150, "Slots:", shipTest.systemSlots)
		Local hp:Int = shipTest.GetHeatProduction(engineP.val, reactorP.val)
		Local hr:Int = shipTest.heatRelease
		If (hp > hr)
			SetColor(255, 0, 0)
		End
		PrettyStats(1100, i.getNext(), 150, "Heat:", hp + "/" + hr)
		SetColor(255, 255, 255)
		Local pd:Int = shipTest.GetPowerUseage(engineP.val, reactorP.val, shieldP.val)
		Local pg:Int = shipTest.GetPowerProduction(reactorP.val)
		If pd > pg
			SetColor(255, 0, 0)
			PrettyStats(1100, i.getNext(), 150, "Power:", pd + "/" + pg)
			If (pd - pg > shipTest.GetBatteryProduction())
				PrettyStats(1100, i.getNext(), 150, "Batt:", (pd - pg) + "/" + shipTest.GetBatteryProduction())
			Else
				SetColor(255, 255, 255)
				PrettyStats(1100, i.getNext(), 150, "Batt:", (pd - pg) + "/" + shipTest.GetBatteryProduction())
			End
			SetColor(255, 255, 255)
			
		Else
			PrettyStats(1100, i.getNext(), 150, "Power:", pd + "/" + pg)
			PrettyStats(1100, i.getNext(), 150, "Batt:", "0/" + shipTest.GetBatteryProduction())
		End

		
		
		'PrettyStats(1100, i.getNext(), 150, "Power:", shipTest.GetPowerUseage() + "/" + shipTest.GetPowerProduction())
		
		PrettyStats(1100, i.getNext(), 150, "Added Val:", FormatCredits(shipTest.value))
		Local hold:int
		Local comp:Int = shipTest.GetValue(area, True)
		Local total:Int = shipTest.GetValue(area)
		For Local temp:Int = 0 Until shipTest.items.slots.Length()
			If shipTest.items.slots[temp] <> Null
				hold += shipTest.items.slots[temp].GetValue(area)
			End
		Next
		PrettyStats(1100, i.getNext(), 150, "Equips:", FormatCredits(comp))
		PrettyStats(1100, i.getNext(), 150, "Value:", FormatCredits(total))
		
		'PrettyStats(1100, i.getNext(), 150, "Hold:", FormatCredits(hold))
		'PrettyStats(1100, i.getNext(), 150, "Total:", FormatCredits(hold + total))
		
		#rem
		Game.white_font.Draw("Mass: " + shipTest.GetMass() + "kg", 1100, 200)
		Game.white_font.Draw("Storage: " + shipTest.storageSpace + "", 1100, 150)
		If shipTest.GetMass() = 0
			Game.white_font.Draw("Thrust: " + 0, 1100, 250)
		Else
			Game.white_font.Draw("Thrust: " + shipTest.GetThrust(True), 1100, 250)
		End
		Game.white_font.Draw("Power: " + shipTest.GetPowerUseage() + "/" + shipTest.GetPowerProduction(), 1100, 300)
		Game.white_font.Draw("Heat: " + shipTest.GetHeatProduction() + "/" + shipTest.heatRelease, 1100, 350)
		Game.white_font.Draw("Powerlevel: " + shipTest.getPowerLevel(), 1100, 400)
		Game.white_font.Draw("Armor: " + shipTest.armor + "%", 1100, 450)
		Game.white_font.Draw("Slots: " + shipTest.systemSlots, 1100, 500)
		#end
		
		If brush.current = PaintBrush.COMPONENT
			For Local temp:Int = 0 Until compButtonStack.Length()
				compButtonStack.Get(temp).Render()
			Next
		End
		
		If brush.current = PaintBrush.ROOM
			For Local temp:Int = 0 Until tileButtonStack.Length()
				tileButtonStack.Get(temp).Render()
			Next
		End
		
		If brush.current = PaintBrush.WEAPON
			For Local temp:Int = 0 Until weaponButtonStack.Length()
				weaponButtonStack.Get(temp).Render()
			Next
		End
		DrawRectOutline(shipTest.x_offset, shipTest.y_offset, shipTest.MAX_X * shipTest.GRID_SIZE, shipTest.MAX_Y * shipTest.GRID_SIZE)
		DrawRectOutline(shipTest.x_offset + (shipTest.MAX_X / 2) * shipTest.GRID_SIZE, shipTest.y_offset, shipTest.GRID_SIZE, shipTest.MAX_Y * shipTest.GRID_SIZE)
		DrawRectOutline(shipTest.x_offset, shipTest.y_offset + (shipTest.MAX_Y / 2) * shipTest.GRID_SIZE, shipTest.MAX_X * shipTest.GRID_SIZE, shipTest.GRID_SIZE)
		brush.Render()
		Return 0
	End
End

Class PaintBrush
	Const ROOM:int = 1
	Const LINE:Int = 2
	Const COMPONENT:Int = 3
	Const WEAPON:Int = 4
	
	
	Const SHIP_ANYWHERE:Int = 100
	Const SHIP_SCAFFOLDING:Int = 200
	Const SHIP_DOOR:Int = 300
	Const SHIP_AIRLOCK:Int = 400
	
	Field current:Int
	
	Field ship:Ship
	
	Field weapon:String
	
	Field code:int
	
	Field componentName:string
	Field last:String
	Field img:Image
	Field type:int
	
	Method New(ship:Ship)
		Self.ship = ship
		current = ROOM
		componentName = ""
		
	End
	
	Method Render:Void()
		If componentName = "" Then Return
		If current = COMPONENT
			For Local x:Int = 0 Until Ship.MAX_X
				For Local y:Int = 0 Until Ship.MAX_Y
					If ship.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, ship.x_offset, ship.y_offset, ship.mirrored)
						If isValidPlacement(x, y, type)
							DrawImage(img, ship.x_offset + (x * 30), ship.y_offset + (y * 30))
						Else
							SetColor(255, 0, 0)
							DrawImage(img, ship.x_offset + (x * 30), ship.y_offset + (y * 30))
							ResetColorAlpha()
						End
					End
				Next
			Next
		End
		If current = ROOM
			For Local x:Int = 0 Until Ship.MAX_X
				For Local y:Int = 0 Until Ship.MAX_Y
					If ship.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, ship.x_offset, ship.y_offset, ship.mirrored)
						If componentName = "hull" Then type = SHIP_ANYWHERE
						If componentName = "floor" Then type = SHIP_ANYWHERE
						If componentName = "scaffolding" Then type = SHIP_SCAFFOLDING
						If componentName = "door" Then type = SHIP_DOOR
						If componentName = "airlock" Then type = SHIP_AIRLOCK
						If componentName = "walkway" Then type = SHIP_ANYWHERE
						If componentName = "flag" Then type = SHIP_ANYWHERE
						
						If isValidPlacement(x, y, type)
							DrawImage(img, ship.x_offset + (x * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2), ship.y_offset + (y * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2))
						Else
							SetColor(255, 0, 0)
							DrawImage(img, ship.x_offset + (x * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2), ship.y_offset + (y * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2))
							ResetColorAlpha()
						End
					End
				Next
			Next
		End
	End
	
	Method RemoveInvalid:Void()
		For Local temp:Int = 0 Until ship.comp.Length()
			If ItemLoader.DoesItemExist(ship.comp.Get(temp).getId())
				Local part:JsonObject = ItemLoader.GetItem(ship.comp.Get(temp).getId())
				If not isValidPlacement(ship.comp.Get(temp).getX(), ship.comp.Get(temp).getY(), part.GetInt("placement"))
					ship.comp.Remove(temp)
					temp = -1
					UpdateShip()
				End
			End
		Next
		For Local x:Int = 0 Until Ship.MAX_X
			For Local y:Int = 0 Until Ship.MAX_Y
				If ship.pipes[x * Ship.MAX_X + y].door
					ship.pipes[x * Ship.MAX_X + y].door = isValidPlacement(x, y, SHIP_DOOR)
				End
				If ship.pipes[x * Ship.MAX_X + y].scaffolding
					ship.pipes[x * Ship.MAX_X + y].scaffolding = isValidPlacement(x, y, SHIP_SCAFFOLDING)
				End
			'	If ship.pipes[x * Ship.MAX_X + y].airlock
				'	ship.pipes[x * Ship.MAX_X + y].airlock = isValidPlacement(x, y, SHIP_AIRLOCK)
			'	End
			Next
		Next
	End

	Method UpdateShip:Void()
		ship.compSlots.Clear()
		Local tSys:Item[] = ship.systems
		ship.systems = New Item[ship.systemSlots]
		If ship.systems.Length() > 0 And tSys.Length() > 0
			For Local temp:Int = 0 Until ship.systems.Length()
				If temp < tSys.Length()
					ship.systems[temp] = tSys[temp]
				End
			Next
		End
		ship.SetSlots()
	End
	Method Update:Void()
		Select current
			Case ROOM

				
			Case COMPONENT
				If Game.Cursor.Hit
					For Local x:Int = 0 Until Ship.MAX_X
						For Local y:Int = 0 Until Ship.MAX_Y
							If ship.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, ship.x_offset, ship.y_offset, ship.mirrored)
								If componentName <> ""
									If isValidPlacement(x, y, type)
										ship.AddComponent(x, y, componentName)
										UpdateShip()
									End
								End
							End
						Next
					Next
				Else If Game.Cursor.RightHit
					For Local temp:Int = 0 Until ship.comp.Length()
						If ship.comp.Get(temp).isMouseOver(ship.x_offset, ship.y_offset, ship.mirrored)
							ship.comp.Remove(temp)
							UpdateShip()
						End
					Next
				End			
		End
	
	
	
	

	End
	
	Method isValidPlacement:Bool(x:Int, y:Int, type:int)

		Select type
			Case Component.PLACEMENT_ANYWHERE
				If ship.pipes[x * Ship.MAX_X + y].floor Then Return True
			Case Component.PLACEMENT_FRONT
				If not ship.pipes[x * Ship.MAX_X + y].floor Then Return False
				If ship.RoomExists(x + 1, y) And ship.RoomExists(x + 2, y)
					If ship.pipes[ (x + 1) * Ship.MAX_X + y].hull And ship.pipes[ (x + 2) * Ship.MAX_X + y].isVacuum()
						For Local temp:Int = (x + 2) Until Ship.MAX_X
							If Not (ship.pipes[ (temp) * Ship.MAX_X + y].isVacuum() And Not ship.pipes[ (temp) * Ship.MAX_X + y].scaffolding) Then Return False
						Next
						Return True
					End
				End
			Case Component.PLACEMENT_OUTSIDE
				If ship.pipes[x * Ship.MAX_X + y].scaffolding Then Return True
			Case Component.PLACEMENT_REAR
				If not ship.pipes[x * Ship.MAX_X + y].floor Then Return False
				If ship.RoomExists(x - 1, y) And ship.RoomExists(x - 2, y)
					If Not ship.pipes[ (x - 1) * Ship.MAX_X + y].hull Then Return False
					For Local temp:Int = (x - 2) Until 0 Step - 1
						If Not (ship.pipes[ (temp) * Ship.MAX_X + y].isVacuum() And Not ship.pipes[ (temp) * Ship.MAX_X + y].scaffolding) Then Return False
					Next
					Return True
				End
			Case SHIP_AIRLOCK
				Local up:Bool = True
				Local down:Bool = True
				For Local temp:Int = y + 1 Until Ship.MAX_Y
					If Not ship.pipes[x * Ship.MAX_X + temp].isVacuum() Then up = False
				Next
				For Local temp:Int = y - 1 Until 0 Step - 1
					If Not ship.pipes[x * Ship.MAX_X + temp].isVacuum() Then down = False
				Next
				If (up or down) And ( Not (up And down)) Then Return True
				Return False
			Case SHIP_ANYWHERE
				Return True
			Case SHIP_SCAFFOLDING
				If ship.RoomExists(x - 1, y)
					If ship.pipes[ (x - 1) * Ship.MAX_X + y].floor Then Return False
				End
				If ship.RoomExists(x + 1, y)
					If ship.pipes[ (x + 1) * Ship.MAX_X + y].floor Then Return False
				End
				If ship.RoomExists(x, y + 1)
					If ship.pipes[x * Ship.MAX_X + y + 1].floor Then Return False
				End
				If ship.RoomExists(x, y - 1)
					If ship.pipes[x * Ship.MAX_X + y - 1].floor Then Return False
				End
				Return True
			Case SHIP_DOOR
				Local xaxis:Int = 0
				Local yaxis:Int = 0
				If ship.RoomExists(x - 1, y)
					If ship.pipes[ (x - 1) * Ship.MAX_X + y].hull Then xaxis += 1
				End
				If ship.RoomExists(x + 1, y)
					If ship.pipes[ (x + 1) * Ship.MAX_X + y].hull Then xaxis += 1
				End
				If ship.RoomExists(x, y + 1)
					If ship.pipes[x * Ship.MAX_X + y + 1].hull Then yaxis += 1
				End
				If ship.RoomExists(x, y - 1)
					If ship.pipes[x * Ship.MAX_X + y - 1].hull Then yaxis += 1
				End
				If (xaxis = 2 or yaxis = 2) And xaxis + yaxis = 2 Then Return True
		End
		Return False
	End
End

Class ComponentButton
	Field id:String
	Field img:Image
	Field over:bool
	Field rules:int
	Field x:int
	Field y:int
	Method New(x:Int, y:Int, data:JsonObject)
		Self.x = x
		Self.y = y
		Self.id = data.GetString("id")
		Self.rules = data.GetInt("placement")
		Self.img = LoadImage("parts/" + data.GetString("imgON"))
	End
	Method Render:Void()
		If over
			Game.white_font.Draw(id, 300, 870)
		End
		If img <> Null
			DrawImage(img, x, y)
		End
	End
	Method isMouseOver:Bool()
		If Game.Cursor.x() > x And Game.Cursor.x() < x + 30 And Game.Cursor.y() > y And Game.Cursor.y() < y + 30
			over = True
		Else
			over = False
		End
		Return over
	End
	Method getName:String()
		Return id
	End
	Method getImage:Image()
		Return img
	End
	Method getRules:Int()
		Return rules
	End
End

Class WeaponButton
	Field id:String
	Field img:Image
	Field x:int
	Field y:int
	Method New(x:Int, y:Int, data:JsonObject)
		Self.x = x
		Self.y = y
		Self.id = data.GetString("id")
		Self.img = LoadImage("parts/" + data.GetString("img_gun"))
	End
	Method Render:Void()
		If img <> Null
			DrawImage(img, x, y)
		End
	End
	Method isMouseOver:Bool()
		If Game.Cursor.x() > x And Game.Cursor.x() < x + 30 And Game.Cursor.y() > y And Game.Cursor.y() < y + 30 Then Return True
		Return False
	End
	Method getName:String()
		Return id
	End
	Method getImage:Image()
		Return img
	End
End

Class TileButton
	Field id:String
	Field img:Image
	Field rules:int
	Field code:int
	Field x:int
	Field y:int
	Method New(x:Int, y:Int, name:String, img:Image, code:int = 0)
		Self.code = code
		Self.x = x
		Self.y = y
		Self.id = name
		Self.img = img
	End
	Method Render:Void()
		If img <> Null
			DrawImage(img, x + (Ship.GRID_SIZE / 2), y + (Ship.GRID_SIZE / 2))
		End
	End
	Method isMouseOver:Bool()
		If Game.Cursor.x() > x And Game.Cursor.x() < x + 30 And Game.Cursor.y() > y And Game.Cursor.y() < y + 30 Then Return True
		Return False
	End
	Method getName:String()
		Return id
	End
	Method getImage:Image()
		Return img
	End
	Method getRules:Int()
		Return rules
	End
	Method getCode:Int()
		Return code
	End
End

Class Incrementer
	Field start:Int
	Field val:int
	Field move:Int
	Method New(start:Int, move:Int)
		Self.start = start
		Self.val = start
		Self.move = move
	End
	Method getNext:Int()
		Local r:Int = val
		val += move
		Return r
	End
	Method getCurrent:Int()
		If val = start Then Return start
		Return val - move
	End
	Method Reset:Void()
		val = start
	End
End

Function SuggestedMass:int(s:Ship)
	Local mass:Int = 0
	Local base:Float = 1.0 + (s.defence / 20.0)
	mass += ( (s.storageSpace / 9) * 5) * base
	mass += ( (s.AP - 15) * base)
	Return 100 + mass
End


Class SimPanel
	Const SIZE:Int = 50
	Field x:Int
	Field y:int
	Field val:Int
	Method New(x:Int, y:Int, val:Int)
		Self.x = x
		Self.y = y
		Self.val = val
	End
	Method Render:Void()
		For Local temp:Int = 0 Until 4
			DrawRectOutline(x + (SIZE * temp), y, SIZE, SIZE)
		Next
		SetColor(0, 255, 0)
		DrawRect(x + (SIZE * val), y, SIZE, SIZE)
		SetColor(255, 255, 255)
	End
	Method Update:Void()
		If Game.Cursor.Hit
			For Local temp:Int = 0 Until 4
				If Game.Cursor.x() > x + (temp * SIZE) And Game.Cursor.x() < x + (temp * SIZE) + SIZE And Game.Cursor.y() > y And Game.Cursor.y() < y + SIZE
					val = temp
				End
			Next
		End
	End
	
End