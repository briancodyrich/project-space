Import space_game

Class PathType
	Const ORDER:Int = 1
	Const OXYGEN:Int = 2
	Const BAD_ROOM:Int = 3
	Const JOBCODE:Int = 4
	Const PERMCODE:Int = 5
	Const ANYWHERE:Int = 7
	Const OTHER_SHIP:Int = 8
	Const FLOOR:Int = 9
	Const SCRAP_SYSTEM:Int = 10
	Const PLAYER_ORDER:Int = 11
	Const FREE_TILE:Int = 12
	Const RANDOM_ORDER:Int = 13
End