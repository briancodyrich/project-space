Import space_game


Const SKILL_TABLE_LOCATION:string = "Strings/Crew/skills.json"
Const ATTRIBUTE_TABLE_LOCATION:string = "Strings/Crew/attributes.json"
Class SkillManager
	Const SKILL_REPAIR:int = 0
	Const SKILL_TOUGHNESS:Int = 1
	Const SKILL_MEDIC:Int = 2
	Const SKILL_PILOT:Int = 3
	Const SKILL_WELDING:Int = 4
	Const SKILL_GUNNER:Int = 5
	Const MISSING_SKILL_TEST:Int = 6
	Const MAX_LEVEL:Int = MISSING_SKILL_TEST * 10
	
	Field types:IntMap<Skill>
	Field deleteThis:String[] =["Repair", "Toughness", "Medic", "Pilot", "Welding", "Gunner"]
	Method New()
	
		Local arr:JsonArray = JsonArray(New JsonParser(app.LoadString(SKILL_TABLE_LOCATION)).ParseValue())
		types = New IntMap<Skill>()
		For Local count:Int = 0 Until arr.Length()
			Local item:JsonObject = JsonObject(arr.Get(count))
			Local s:Skill = New Skill(item)
			types.Add(GetSkillKey(item.GetString("name").ToUpper()), s)
		Next
		arr = JsonArray(New JsonParser(app.LoadString(ATTRIBUTE_TABLE_LOCATION)).ParseValue())
		For Local count:Int = 0 Until arr.Length()
			Local item:JsonObject = JsonObject(arr.Get(count))
			Local s:Skill = New Skill(item)
			types.Add(GetSkillKey(item.GetString("name").ToUpper()), s)
		Next
		For Local temp:Int = 0 Until MISSING_SKILL_TEST
			If types.Contains(temp)
				If types.Get(temp).is_null_skill Then Print("null skill " + temp)
			Else
				types.Add(temp, New Skill())
				Print("null skill " + temp)
			End
		Next
	End
	
	Method FileName:String()
		Return ""
	End
	
	Function SelectBest:int(cStack:Stack<Crew>, skill:int, ignore_ai:bool)
		Local best:Int = -100
		Local index:Int = -1
		Local counter:Int = 0
		For Local c:Crew = EachIn cStack
			If c.aiCodes[Crew.PLAYER_DEFINED_AI_START] = 0 Or ignore_ai
				If c.skills.types.Get(skill).xp > best
					best = c.skills.types.Get(skill).xp
					index = counter
				End
			End
			counter += 1
		Next
		Return index
	End
	Method GetSkillKey:Int(str:String)
		Select str
			Case "REPAIR"
				Return SKILL_REPAIR
			Case "MEDIC"
				Return SKILL_MEDIC
			Case "PILOT"
				Return SKILL_PILOT
			Case "GUNNER"
				Return SKILL_GUNNER
			Case "WELDING"
				Return SKILL_WELDING
			Case "TOUGHNESS"
				Return SKILL_TOUGHNESS
		End
		Return -1
	End
	
	
	Method OnSave:JsonArray()
		Local data:JsonArray = New JsonArray(types.Count())
		For Local temp:Int = 0 Until types.Count()
			data.SetInt(temp, types.Get(temp).xp)
		Next
		Return data
	End
	Method OnLoad:Void(data:JsonArray)
		For Local temp:Int = 0 Until data.Length()
			types.Get(temp).GainXP(data.GetInt(temp))
		Next
	End
	
	Method getLevel:Int()
		Local lv:int
		For Local temp:Int = 0 Until types.Count()
			lv += types.Get(temp).level
		Next
		Return lv
	End
	
	Method GetKeyList:Int[] ()
		Local l:Int[types.Count()]
		Local counter:int
		For Local temp:Int = EachIn types.Keys()
			l[counter] = temp
			counter += 1
		Next
		Return l
	End
	
	Method RandomLevel:Void(level:Int, random_job:bool)
		Local favoredType:Int
		If random_job = False
			favoredType = Rnd(types.Count())
		End
		While getLevel() < level
			Local rnd:int = Rnd(types.Count())
			If Not random_job And rnd = favoredType
				types.Get(rnd).addRandomXp(3)
			Else
				types.Get(rnd).addRandomXp(6)
			End
		Wend
	End
	
	Method isDone:Bool(skill:Int, counter:Int)
		If types.Get(skill).isDone(counter)
			GainXP(skill, 1)
			Return True
		End
		Return False
	End
	
	Method GetMaxHp:Int(food:int, base_health:int)
		Local healthNeg:Int
		If food < 0
			healthNeg = food / 50
		End
		Return base_health + (types.Get(SKILL_TOUGHNESS).level * types.Get(SKILL_TOUGHNESS).power_base) + healthNeg
	End
	
	Method GetRegenLevel:Int()
		Return types.Get(SKILL_MEDIC).level * 10
	End
	
	Method UseSkill:int(skill:Int, xp:int)
		GainXP(skill, xp)
		Return types.Get(skill).GetPower()
	End
	
	Method GetPercent:Float(skill:Int)
		Return types.Get(skill).GetPercent()
	End
	#rem
	Method GainXP:bool(str:String, xp:Int)
		Select str
			Case "SKILL_REPAIR"
				Return GainXP(SKILL_REPAIR, xp)
			Case "SKILL_TOUGHNESS"
				Return GainXP(SKILL_TOUGHNESS, xp)
			Case "SKILL_MEDIC"
				Return GainXP(SKILL_MEDIC, xp)
			Case "SKILL_PILOT"
				Return GainXP(SKILL_PILOT, xp)
			Case "SKILL_WELDING"
				Return GainXP(SKILL_WELDING, xp)
		End
		Return False
	End
	#end
	
	Method GainXP:bool(skill:Int, xp:Int)
		Settings.LogXP(skill, xp)
		types.Get(skill).GainXP(xp)
	End
	Method GetSkillCosts:Int()
		Local cost:Int
		For Local s:Skill = EachIn types.Values()
			cost += s.GetCost()
		Next
		Return cost
	End
End

Class Skill
	Field xp:Int
	Field level:Int
	Field power_base:Int
	Field xp_base:int
	Field mult:Int
	Field cost:int
	Field code:int
	Field is_skill:bool
	Field is_null_skill:bool
	Field levelFlag:bool
	Method New()
		is_null_skill = True
	End
	Method New(data:JsonObject)
		Self.level = 0
		Self.xp = 0
		Self.power_base = data.GetInt("power_base", -1)
		If power_base = -1 Then is_null_skill = True
		
		Self.mult = data.GetInt("power_mult", -1)
		If mult = -1 Then is_null_skill = True
		
		Self.cost = data.GetInt("cost", -1)
		If cost = -1 Then is_null_skill = True
		
		Self.is_skill = data.GetBool("is_skill", False)
		
		Self.xp_base = data.GetInt("xp_base", -1)
		If xp_base = -1 Then is_null_skill = True
		
	End
	Method GetCost:Int()
		If is_null_skill Then Return 0
		Return cost * (0.5 * (level + 1) * level)
	End
	Method isSkill:Bool()
		Return is_skill
	End
	Method LevelMultiplier(level:Int)
		If level = 0 Return 0
		Return level + LevelMultiplier(level - 1)
	End
	Method isDone:Bool(counter:Int)
		If counter >= power_base - (level * mult)
			Return True
		End
	End
	Method GetPercent:Float()
		Return (xp - lastXP()) / float(nextXP() -lastXP())
	End
	Method GetPower:Int()
		Return mult * level + power_base
	End
	Method nextXP:Int()
		Return xp_base * (-2 + Pow(2, level + 2))
	End
	Method GainXP:Void(xp:int)
		If level >= 10
			xp = nextXP()
			Return
		End
		Self.xp += xp
		If Self.xp >= nextXP()
			levelFlag = True
			level += 1
			GainXP(0)
		End
	End
	Method addRandomXp:Void(div:Float)
		GainXP(Rnd( (nextXP() -lastXP()) / div))
	End
	Method lastXP:Int()
		Return xp_base * (-2 + Pow(2, level + 1))
	End
End


