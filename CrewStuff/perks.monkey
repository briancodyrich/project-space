Import space_game

Class PerkManager
	Const HUNGER:Int = 1
	Const FIRE_DAMAGE:Int = 2
	Const OXYGEN_USE:Int = 3
	Const REPAIR:Int = 4
	Const HEALTH:Int = 5
	Const HEALTH_REGEN:Int = 6
	Const OXYGEN_TANK:Int = 7
	Const REPAIR_POWER:Int = 8
	Const WAGES:Int = 9
	Const MELEE_DAMAGE:Int = 10
	Const MORALE:Int = 11
	Field perks:Stack<Perk>
	
	Method New()
		perks = New Stack<Perk>
	End
	
	Method New(race:String, sex:String, base:Int, rand:int)
		perks = New Stack<Perk>
		Local perkList:Stack<JsonObject> = PerkLoader.GetPerkList(race, sex)
		If perkList.IsEmpty()
			Print "no perks found"
			Return
		End
		Local count:Int = base + (Rnd(rand) + 1)
		Local timeout:Int = 100
		While count > 0
			Local testPerk:JsonObject = perkList.Get(Rnd(perkList.Length))
			If canAddPerk(testPerk)
				AddPerk(testPerk)
				count -= 1
			End
			timeout -= 1
			If timeout <= 0 Then Return
		Wend
	End
	
	Method ClearPerks()
		perks.Clear()
	End
	Method AddPerk(name:String)
		Local data:JsonObject = PerkLoader.GetPerk(name)
		If data = Null
			Print "Unable to load perk " + name
			Return
		End
		perks.Push(New Perk(data))
	End
	Method AddPerk(data:JsonObject)
		perks.Push(New Perk(data))
	End
	Method canAddPerk:Bool(data:JsonObject)
		Local banned:Stack<String> = New Stack<String>()
		Local name:String = data.GetString("name")
		For Local t:Int = 0 Until perks.Length()
			If perks.Get(t).name = name Then Return False
		Next
		Local arr:JsonArray = JsonArray(data.Get("excludes"))
		For Local count:Int = 0 Until arr.Length()
			banned.Push(arr.GetString(count))
		Next
		For Local i:Int = 0 Until banned.Length()
			For Local j:Int = 0 Until perks.Length()
				If banned.Get(i) = perks.Get(j).name Then Return False
			Next
		Next
		Return True
	End
	Method GetPerkEffect:Int(type:Int)
		Local v:Int
		For Local temp:Int = 0 Until perks.Length()
			v += perks.Get(temp).GetEffect(type)
		Next
		Return v
	End
	
	Method getPerkStatus:Void(s:Stack<String>)
		For Local temp:Int = 0 Until perks.Length()
			s.Push(perks.Get(temp).name)
		Next
	End
	

End


Class Perk
	Field descr:String
	Field name:String
	Field effects:Stack<PerkEffect>
	Method New(data:JsonObject)
		descr = data.GetString("descr")
		name = data.GetString("name")
		effects = New Stack<PerkEffect>()
		Local eff:JsonArray = JsonArray(data.Get("effects"))
		For Local count:Int = 0 Until eff.Length()
			effects.Push(New PerkEffect(JsonObject(eff.Get(count))))
		Next
	End
	
	Method GetEffect:Int(type:Int)
		For Local temp:Int = 0 Until effects.Length()
			If effects.Get(temp).type = type
				Return effects.Get(temp).value
			End
		Next
		Return 0
	End
End

Class PerkEffect
	Field type:Int
	Field value:Int
	Method New(data:JsonObject)
		Local v:String = data.GetString("effect")
		If v = "HUNGER" Then type = PerkManager.HUNGER
		If v = "FIRE_DAMAGE" Then type = PerkManager.FIRE_DAMAGE
		If v = "OXYGEN_USE" Then type = PerkManager.OXYGEN_USE
		If v = "REPAIR" Then type = PerkManager.REPAIR
		If v = "HEALTH" Then type = PerkManager.HEALTH
		If v = "HEALTH_REGEN" Then type = PerkManager.HEALTH_REGEN
		If v = "WAGES" Then type = PerkManager.WAGES
		If v = "MELEE_DAMAGE" Then type = PerkManager.MELEE_DAMAGE
		If v = "MORALE" Then type = PerkManager.MORALE
		If type = 0 Then Print "Perk " + data.GetString("effect") + " not found"
		value = data.GetInt("power")
	End
End