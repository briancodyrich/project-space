Strict

Import space_game

Class CSCode
	Const N:Int = 0
	Const S:Int = 1
	Const E:Int = 2
	Const W:Int = 3
	Const NE:Int = 4
	Const SE:Int = 5
	Const NW:Int = 6
	Const SW:Int = 7
	
	Function GetTag:String(n:int)
		Select n
			Case N
			Return "_N"
			Case S
			Return "_S"
			Case E
			Return "_E"
			Case W
			Return "_W"
			Case NE
			Return "_NE"
			Case SE
			Return "_SE"
			Case NW
			Return "_NW"
			Case SW
			Return "_SW"
		End
		Return ""
	End
End

Class Crew Implements Loadable
	Const DRAW_PATHS:Bool = False
	Const MORALE_LOCATION:String = "Strings/Crew/morale.json"
	Const HIRE_COST_MULTIPLIER:int = 25
	Const LOYALTY_PER_DAY:Float = 0.001
	Const FOOD_TICK:Int = 200
	Const MAX_FOOD:Int = 2000
	Const BASE_WAGES:Int = 50
	Const JOB_PILOT:int = 1
	Const JOB_MECHANIC:Int = 2
	Const JOB_BACKUP_GUNNER:Int = 3
	Const JOB_GUNNER:Int = 4
	Const PLAYER_DEFINED_AI_START:Int = 5
	Const PLAYER_DEFINED_AI_STOP:Int = 11
	Const WELD_COOLDOWN:Int = 20
	Const NO_OXYGEN_DAMAGE:Int = 3
	Field weldCounter:int
	Field x:Int
	Field y:Int
	Field x_offset:float
	Field y_offset:float
	Field target_x:Int
	Field target_y:int
	Field order_x:Int = -1
	Field order_y:int = -1
	Field random_x:Int = -1
	Field random_y:Int = -1
	Field first_name:String
	Field last_name:String
	Field sex:String
	Field race:String
	
	Field img:Image[8]
	Field direction:Int
	'These fields can be changed by mods
	Field base_health:Int
	Field base_melee:Int
	Field regen_time:Int
	Field oxygen_use:Int
	Field food_use:Int
	Field fire_damage:int
	Field can_heal:bool
	
	Field gamma_defense:Int
	Field ion_defense:Int
	Field fire_defense:int
	
	Field health:int
	Field oxygen:int
	Field last_oxygen:int
	Field food:Int
	Field ship:Ship
	Field dead:bool
	Field ai:int
	Field aiPLevel:int
	
	Field aiCodes:int[17] '0-4 reserved '12-14 reserved
	Field path:Stack<SimplePath>
	Field skills:SkillManager
	Field workCode:int
	Field hasBoarded:bool
	Field isLeaving:bool
	Field perks:PerkManager
	Field counter:Int
	Field healthCounter:Int
	Field foodCounter:Int
	Field updateFlag:Bool
	
	Field morale:Stack<Morale>
	Field loyalty:float
	
	Method New(x:Int, y:int, ship:Ship, data:RaceData)
		'this should only be null right before onload is called to get this from a file
		direction = CSCode.E
		If data <> Null
			race = data.race
			sex = data.sex
			first_name = data.first_name
			last_name = data.last_name
			For Local temp:Int = 0 Until 8
				img[temp] = LoadImage(data.sprite + CSCode.GetTag(temp) + ".png",, Image.MidHandle)
			Next
			If first_name = "ROBO"
				Local cArr:int[] =[int(Rnd(65, 91)), int(Rnd(65, 91)), int(Rnd(65, 91)), 45, int(Rnd(48, 58)), int(Rnd(48, 58)), int(Rnd(48, 58))]
				first_name = String.FromChars(cArr)
				cArr =[int(Rnd(48, 58)), int(Rnd(48, 58)), int(Rnd(48, 58))]
				last_name = "BAT-" + String.FromChars(cArr)
			End
			base_health = data.base_health
			base_melee = data.base_melee
			regen_time = data.regen_time
			oxygen_use = data.oxygen_use
			food_use = data.food_use
			fire_damage = data.fire_damage
			fire_defense = data.
			can_heal = data.can_heal
			perks = New PerkManager(race, sex, data.perk_base, data.perk_random)
		Else
			perks = New PerkManager()
		End
		Self.x = x
		Self.y = y
		aiCodes[0] = AICode.AI_AVOID_FIRE
		aiCodes[1] = AICode.AI_HEAL_SELF_HIGH
		aiCodes[2] = AICode.AI_FOLLOW_ORDERS
		'aiCodes[4] = AICode.AI_PATHFINDING_BREAKPOINT
		
		'5 on is player defined
		
		
		aiCodes[12] = AICode.AI_HEAL_SELF_LOW
		aiCodes[13] = AICode.AI_RESTING
		'until I can think of the best way to do this
		aiCodes[14] = AICode.AI_DONT_BLOCK
		aiCodes[15] = AICode.AI_RESET
		aiCodes[16] = 'AICode.AI_RESET
		
		#rem
		aiCodes[13] = AICode.AI_RESET
		aiCodes[14] = AICode.AI_DONT_BLOCK
		aiCodes[15] = AICode.AI_PATCH_HULL
		#END
		x_offset = 0
		y_offset = 0
		Self.ship = ship
		oxygen = 100
		food = MAX_FOOD
		path = New Stack<SimplePath>
		skills = New SkillManager()
		health = GetMaxHp()
		morale = New Stack<Morale>()
		loyalty = 0.01
		Local arr:JsonArray = JsonArray(New JsonParser(app.LoadString(MORALE_LOCATION)).ParseValue())
		For Local count:Int = 0 Until arr.Length()
			morale.Push(New Morale(JsonObject(arr.Get(count))))
		Next
		
		
	End
	
	Method FileName:String()
		Return ""
	End
	Method PrintMorale:Void()
		Print first_name 
		For Local m:Morale = EachIn morale
			Print Morale.IdToString(m.type) + " - " + m.GetCost()
		Next
		Print "loyalty - " + loyalty 
	End
	Method OnSave:JsonObject()
		Local data:JsonObject = New JsonObject()
		data.SetInt("x", x)
		data.SetInt("y", y)
		data.SetString("race", race)
		data.SetString("sex", sex)
		data.SetString("first_name", first_name)
		data.SetString("last_name", last_name)
		data.SetInt("health", health)
		data.SetInt("food", food)
		data.SetFloat("loyalty", loyalty)
		
		Local perkArr:JsonArray = New JsonArray(perks.perks.Length())
		For Local temp:Int = 0 Until perks.perks.Length()
			perkArr.SetString(temp, perks.perks.Get(temp).name)
		Next
		data.Set("Perks", perkArr)
		
		Local aiArr:JsonArray = New JsonArray(PLAYER_DEFINED_AI_STOP - PLAYER_DEFINED_AI_START+1)
		For Local temp:Int = PLAYER_DEFINED_AI_START To PLAYER_DEFINED_AI_STOP
			aiArr.SetInt(temp - PLAYER_DEFINED_AI_START, aiCodes[temp])
		Next
		data.Set("Ai", aiArr)
		
		
		
		Local skillArr:JsonArray = New JsonArray(skills.types.Count())
		For Local temp:Int = 0 Until skills.types.Count()
			skillArr.SetInt(temp, skills.types.Get(temp).xp)
		Next
		data.Set("Skills", skillArr)
		
		Local marr:JsonArray = New JsonArray(morale.Length())
		For Local temp:Int = 0 Until morale.Length()
			marr.Set(temp, morale.Get(temp).OnSave())
		Next
		data.Set("Morale", marr)
		
		Return data
	End
	Method OnLoad:Void(data:JsonObject)
		'system = data.GetItem("system").ToString()
		x = data.GetInt("x")
		y = data.GetInt("y")
		race = data.GetString("race")
		sex = data.GetString("sex")
		first_name = data.GetString("first_name")
		last_name = data.GetString("last_name")
		health = data.GetInt("health")
		food = data.GetInt("food")
		loyalty = data.GetFloat("loyalty")
		Local raceData:RaceData = RaceLoader.GetRace(race, sex, "ANY")
		If raceData = Null
			raceData = RaceLoader.GetRace("ANY", "ANY", "PLAYABLE")
			Print "race not found"
		End
		For Local temp:Int = 0 Until 8
			img[temp] = LoadImage(raceData.sprite + CSCode.GetTag(temp) + ".png",, Image.MidHandle)
		Next
		base_health = raceData.base_health
		base_melee = raceData.base_melee
		regen_time = raceData.regen_time
		oxygen_use = raceData.oxygen_use
		food_use = raceData.food_use
		fire_damage = raceData.fire_damage
		gamma_defense = raceData.gamma_defense
		ion_defense = raceData.ion_defense
		fire_defense = raceData.fire_defense
		can_heal = raceData.can_heal
		
		skills = New SkillManager()
		If data.Contains("Skills")
			skills.OnLoad(JsonArray(data.Get("Skills")))
		End
			
		
		Local aiArr:JsonArray = JsonArray(data.Get("Ai"))
		For Local count:Int = 0 Until aiArr.Length()
			aiCodes[count + PLAYER_DEFINED_AI_START] = aiArr.GetInt(count)
		Next
		
		perks.ClearPerks()
		Local perkArr:JsonArray = JsonArray(data.Get("Perks"))
		For Local count:Int = 0 Until perkArr.Length
			perks.AddPerk(perkArr.GetString(count))
		Next
		Local marr:JsonArray = JsonArray(data.Get("Morale"))
		For Local count:Int = 0 Until marr.Length
			Local obj:JsonObject = JsonObject(marr.Get(count))
			For Local temp:Int = 0 Until morale.Length()
				If morale.Get(temp).type = obj.GetInt("type")
					morale.Get(temp).value = obj.GetInt("value")
				End
			Next
		Next
	End
	
	Method GetHireCost:Int()
		Return (100 / GetMorale()) * GetWages() * HIRE_COST_MULTIPLIER
	End
	
	Method GetWages:Int()
		Return (BASE_WAGES + skills.GetSkillCosts()) * (1.0 + (GetBonus(PerkManager.WAGES) / 100.0))
	End
	
	Method hasLevelUp:Bool()
		
	End
	
	Method MoraleEvent:Void(type:Int, val:int)
		For Local temp:Int = 0 Until morale.Length()
			If morale.Get(temp).type = type
				morale.Get(temp).Add(val, GetBonus(PerkManager.MORALE))
			End
		Next
	End
	Method MoraleReset:Void(type:Int)
		For Local temp:Int = 0 Until morale.Length()
			If morale.Get(temp).type = type
				morale.Get(temp).Reset()
			End
		Next
	End
	
	Method GetMorale:Float()
		Local base:Float = 100.0
		For Local temp:Int = 0 Until morale.Length()
			base -= morale.Get(temp).GetCost()
		Next
		Return base
	End
	
	Method GetBonus:int(type:Int)
		Local v:Int
		v += perks.GetPerkEffect(type)
		Return v
	End
	
	Method GetCombatDamage:Int()
		Return Max(1, base_melee + (health / 30) + GetBonus(PerkManager.MELEE_DAMAGE))
	End
	
	Method SetBasicJob:Void(type:Int)
		Select type
			Case JOB_PILOT
				aiCodes[5] = AICode.AI_PILOT
				aiCodes[6] = AICode.AI_FIREFIGHTER
				aiCodes[7] = AICode.AI_MECHANIC
			Case JOB_MECHANIC
				aiCodes[5] = AICode.AI_FIREFIGHTER
				aiCodes[6] = AICode.AI_MECHANIC
			Case JOB_GUNNER
				aiCodes[5] = AICode.AI_GUNNER
				aiCodes[6] = AICode.AI_FIREFIGHTER
				aiCodes[7] = AICode.AI_MECHANIC
			Case JOB_BACKUP_GUNNER
				aiCodes[5] = AICode.AI_FIREFIGHTER
				aiCodes[6] = AICode.AI_MECHANIC
				aiCodes[7] = AICode.AI_GUNNER
		End
	End
	
	
	Method GetMaxHp:Int()
		Return skills.GetMaxHp(food, base_health) + GetBonus(PerkManager.HEALTH)
	End
	
	
	Method getStatus:Stack<String>(simple:Bool = False)
		Local s:Stack<String> = New Stack<String>
		s.Push(first_name + " " + last_name)
		s.Push("Lv: " + skills.getLevel() + " " + race)
		If Not simple
			s.Push("Health: " + health + "/" + GetMaxHp())
			s.Push("Morale: " + GetMorale())
		End
		If Not simple
			s.Push("Oxygen: " + oxygen)
			s.Push("room: " + ship.pipes[x * Ship.MAX_X + y].o2)
		End
		s.Push("wages: " + GetWages())
		If Not simple
			Select ai
				Case AICode.MAJOR_REPAIR
					s.Push("repairing pipes")
				Case AICode.MINOR_REPAIR
					s.Push("repairing hull")
				Case AICode.SYSTEM_REPAIR
					s.Push("repairing system")
				Case AICode.EMERGENCY_NEED_OXYGEN
					s.Push("low oxygen")
				Case AICode.EMERGENCY_REFILL_OXYGEN
					s.Push("refilling oxygen tank")
				Case AICode.MISC_OXYGEN
					s.Push("room is uncomfortable")
				Case AICode.EMERGENCY_NEED_HEALTH
					s.Push("healing self")
				Case AICode.PILOT
					s.Push("piloting ship")
				Case AICode.LOOTING
					s.Push("scraping system")
				Case AICode.WALKING_TO_SHIP
					s.Push("boarding ship")
				Case AICode.ORDERED
					s.Push("following orders")
				Case AICode.FIREFIGHTING
					s.Push("fighting fires")
				Case AICode.MINOR_NEED_HEALTH
					s.Push("healing self")
				Case AICode.PATCHING_HULL
					s.Push("patching hull")
				Case AICode.SLEEPING
					s.Push("resting")
				Default
					s.Push("UNKNOWN AI " + ai)
			End
		End
		If Not simple

		End
		perks.getPerkStatus(s)
		Return s
	End
	
	Method RunOxygenSim:Void()
		last_oxygen = oxygen
		If ship.pipes[x * Ship.MAX_X + y].useOxygen(oxygen_use) = False
			oxygen -= (oxygen_use)
		End
		If oxygen < 100
			If ship.pipes[x * Ship.MAX_X + y].useOxygen(1) = True
				oxygen += 1
			End
		End
		If ship.pipes[x * Ship.MAX_X + y].fire > 0
			TakeDamage(fire_damage)
		End
		If oxygen <= 0
			oxygen = 0
			TakeDamage(NO_OXYGEN_DAMAGE)
		End
	End
	
	Method GetFoodCost:Int()
		Return food_use + GetBonus(PerkManager.HUNGER)
	End
	
	Method Update:Void()
	
	
		MoraleEvent(Morale.TIME_AWAY, 1)
		MoraleEvent(Morale.TIME_WORKED, 1)
		If not ship.pipes[x * Ship.MAX_X + y].isFlag(FlagCode.BED) Then MoraleEvent(Morale.TIRED, 1)	
		For Local temp:Int = 0 Until morale.Length()
			morale.Get(temp).Tick()
		Next
		foodCounter += 1
		If foodCounter Mod FOOD_TICK = 0
			foodCounter = 0
			food -= GetFoodCost()
			If food <= 0 And ship.items.hasItem(Item.FOOD)
				Player.accounting.UsedItem(Item.FOOD, 1)
				ship.items.RemoveItem(Item.FOOD, 1)
				food = MAX_FOOD
			End
			If food <= 0
				If health > GetMaxHp()
					health = GetMaxHp()
					If health <= 0 Then dead = True
				End
			End
		End
		healthCounter += 1
		If healthCounter Mod (regen_time - GetBonus(PerkManager.HEALTH_REGEN) - skills.GetRegenLevel()) = 0
			health += 1
			If health > GetMaxHp()
				health = GetMaxHp()
			End
		End
		workCode = AICode.WORK_CODE_NULL
		Local canInterrupt:bool = Walk()
	'	If not isWalking() Then canInterrupt = True
		If Not updateFlag Then Return
		For Local temp:Int = 0 Until aiCodes.Length()
			'If Not ship.mirrored Then Print "ai#: " + temp + " plev: " + aiPLevel 
			If canInterrupt = False Then Return
			If aiCodes[temp] = AICode.AI_RESET
				aiWait()
				random_x = -1
				random_y = -1
			End
			If aiCodes[temp] = AICode.AI_DONT_BLOCK
				If ship.taskmanager.isBlockingJob(x, y)
					path = FindPath(PathType.FREE_TILE)
					Return
				End
				If not ship.pipes[x * Ship.MAX_X + y].floor
					path = FindPath(PathType.FREE_TILE)
					Return
				End
			End
			If aiCodes[temp] = 0 Then Continue
			If temp = aiPLevel
				If isWalking()
					Return
				Else
					Select ship.taskmanager.TaskAtFeet(x, y, aiCodes[temp])  'try job at feet
						Case -1
							aiWait()
							Return
						Case Task.FIRE
							FightFire()
							Return
						Case Task.SYSTEM_REPAIR
							SystemRepair()
							Return
						Case Task.HEAL_SELF
							HealSelf()
							Return
						Case Task.MAJOR_REPAIR
							MajorRepair()
							Return
						Case Task.MINOR_REPAIR
							MinorRepair()
							Return
						Case Task.PILOT_SHIP
							If ship.enginePower <> Ship.OFF And ship.pilot_ai_on
								PilotShip()
								Return
							End
						Case Task.USE_GUNS
							If ship.gun_ai_on
								ManGuns()
								Return
							End
						Case Task.SLEEP
							direction = CSCode.S
							ai = AICode.AI_RESTING
							Return
						Default
							Print "task"
					End
				End
			End
			
			' if you are done with the job then starting looking again
			If aiCodes[temp] = AICode.AI_HEAL_SELF_HIGH and can_heal'heal self
				If health < GetMaxHp() / 2
					If ship.DoesPermExist(x, y, Task.HEAL_SELF)
						path = FindPath(PathType.PERMCODE, Task.HEAL_SELF)
						aiPLevel = temp
						If path = Null
							Print "SANITY CHECK FAILED (find health)"
						Else
							ai = AICode.EMERGENCY_NEED_HEALTH
							Return
						End
					End
				End
			End
			If aiCodes[temp] = AICode.AI_HEAL_SELF_LOW and can_heal'heal self
				If health < GetMaxHp() And ai
					If ship.DoesPermExist(x, y, Task.HEAL_SELF)
						path = FindPath(PathType.PERMCODE, Task.HEAL_SELF)
						aiPLevel = temp
						If path = Null
							Print "SANITY CHECK FAILED (find health)"
						Else
							ai = AICode.MINOR_NEED_HEALTH
							Return
						End
					End
				End
			End
			If aiCodes[temp] = AICode.AI_AVOID_FIRE And fire_damage > 0
				If Not isWalking()
					If ship.pipes[x * Ship.MAX_X + y].fire > 0
						path = FindPath(PathType.ANYWHERE)
						aiPLevel = temp
						If path = Null
							Print "SANITY CHECK FAILED (avoid fire)"
						Else
							aiWait()
							Return
						End
					End
				End
			End
			If aiCodes[temp] = AICode.AI_FOLLOW_ORDERS
				If hasOrders() And ai <> AICode.ORDERED or ( ( (target_x <> order_x) or (target_y <> order_y)) And hasOrders())
					If PathSanityCheck(PathType.PLAYER_ORDER)
						Print order_x + "//" + order_y
						path = FindPath(PathType.PLAYER_ORDER)
						aiPLevel = temp
						If path <> Null
							ai = AICode.ORDERED
							Return
						End
					End
					Print "order did not pass sanity check"
					ResetOrders()
				End
				If (x = order_x And y = order_y)
					ResetOrders()
					Return
				End
			End
			'ai = AICode.SEARCHING
			Local jobC:int = ship.GetJob(x, y, aiCodes[temp])
			If jobC <> 0
				If jobC = Task.PILOT_SHIP And ship.pilot_ai_on
					path = FindPath(PathType.PERMCODE, Task.USE_COMPUTER)
					aiPLevel = temp
					If path = Null
						Print "SANITY CHECK FAILED (jobcode - use computer)"
					End
				Else If jobC = Task.USE_GUNS And ship.gun_ai_on
						path = FindPath(PathType.PERMCODE, Task.USE_GUNS)
						aiPLevel = temp
						If path = Null
							Print "SANITY CHECK FAILED (jobcode - use computer)"
						End
				Else If jobC = Task.SLEEP
					path = FindPath(PathType.PERMCODE, Task.SLEEP)
					aiPLevel = temp
					If path = Null
						Print "SANITY CHECK FAILED (jobcode - sleep)"
					End
				Else
					path = FindPath(PathType.JOBCODE, jobC)
					aiPLevel = temp
					If path = Null
						Print "SANITY CHECK FAILED (jobcode - " + jobC + ")"
					End
				End
				Return
			End
		Next
		
	End
	
	Method isWalking:Bool()
		If path = Null Then Return False
		If path.Length() > 0 Then Return True
		Return False
	End
	
	Method aiWait:Void()
		counter = 0
		updateFlag = False
		aiPLevel = 100
	End
	
	Method FindRandomTile:Stack<SimplePath>()
		Local xx:Int
		Local yy:Int
		Local found:Bool = False
		While found = False
			xx = Rnd(Ship.MAX_X)
			yy = Rnd(Ship.MAX_X)
			If ship.RoomExists(xx, yy) And ship.pipes[ (xx * Ship.MAX_X) + yy].isWalkable(False) and not ship.taskmanager.isBlockingJob(xx, yy) And Not isDupe(xx, yy)
				found = True
				random_x = xx
				random_y = yy
			End
		Wend
		Return FindPath(PathType.RANDOM_ORDER)
	End
	
	Method FightFire:Void()
		ai = AICode.FIREFIGHTING
		For Local tx:Int = -1 To 1
			For Local ty:Int = -1 To 1
				If (ship.RoomExists(tx + x, ty + y)) And ship.pipes[ (tx + x) * Ship.MAX_X + ty + y].floor
					ship.pipes[ (x + tx) * ship.MAX_X + y + ty].FightFire(1)
				End
			Next
		Next
		#rem
		If (ship.RoomExists(x + 1, y))
			If ship.pipes[ (x + 1) * ship.MAX_X + y].floor And ship.pipes[ (x + 1) * ship.MAX_X + y].fire > 0
				ship.pipes[ (x + 1) * ship.MAX_X + y].fire -= 1
				Return
			End
		End
		If (ship.RoomExists(x - 1, y))
			If ship.pipes[ (x - 1) * ship.MAX_X + y].floor And ship.pipes[ (x - 1) * ship.MAX_X + y].fire > 0
				ship.pipes[ (x - 1) * ship.MAX_X + y].fire -= 1
				Return
			End
		End
		If (ship.RoomExists(x, y + 1))
			If ship.pipes[x * ship.MAX_X + y + 1].floor And ship.pipes[x * ship.MAX_X + y + 1].fire > 0
				ship.pipes[x * ship.MAX_X + y + 1].fire -= 1
				Return
			End
		End
		If (ship.RoomExists(x, y - 1))
			If ship.pipes[x * ship.MAX_X + y - 1].floor And ship.pipes[x * ship.MAX_X + y - 1].fire > 0
				ship.pipes[x * ship.MAX_X + y - 1].fire -= 1
				Return
			End
		End
		#end
	End
	
	Method Walk:bool()
		If path <> Null and path.Length() > 0
			Local up:Int
			Local right:int
			If x < path.Top().x
				x_offset += dt.delta
				right = 1
			End
			If x > path.Top().x
				x_offset -= dt.delta
				right = -1
			End
			If y < path.Top().y
				y_offset += dt.delta
				up = -1
			End
			If y > path.Top().y
				y_offset -= dt.delta
				up = 1
			End
			If up = 0
				If right = 1
					direction = CSCode.E
				Else
					direction = CSCode.W
				End
			Else If right = 0
				If up = 1
					direction = CSCode.N
				Else
					direction = CSCode.S
				End
			Else
				If up = 1 And right = 1 Then direction = CSCode.NE
				If up = 1 And right = -1 Then direction = CSCode.NW
				If up = -1 And right = 1 Then direction = CSCode.SE
				If up = -1 And right = -1 Then direction = CSCode.SW
			End
			
			If Abs(x_offset) >= 30 or Abs(y_offset) >= 30
				x = path.Top().x
				y = path.Top().y
				x_offset = 0
				y_offset = 0
				path.Remove(path.Length())
				If path.Length() > 0
					If ship.canEVA() = False
						If ship.pipes[path.Top().x * Ship.MAX_X + path.Top().y].isVacuum()
							SetTarget(x, y)
							aiWait()
							path = FindPath(PathType.ANYWHERE)
						End
					End
				End
				Return True
			End
			Return False
		End
		Return True
	End
	
	Method SystemRepair:Void()
		ai = AICode.SYSTEM_REPAIR
		direction = CSCode.N
		If ship.mirrored = False
			weldCounter = (weldCounter + 1) Mod WELD_COOLDOWN
			'DrawCircle( (x * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2) + x_offset, (y * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2) + y_offset, 15)
			ship.particles.Add( (x * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2), (y * Ship.GRID_SIZE), 0, 0, False, PhotonTypes.Weld)
			If weldCounter = 0
				SoundEffect.Play(SoundEffect.WELD)
			End
		End
		counter += 1
		If skills.isDone(SkillManager.SKILL_REPAIR, counter + GetBonus(PerkManager.REPAIR))
			counter = 0
			For Local temp:Int = 0 Until ship.comp.Length()
				If ship.comp.Get(temp).getX() = x And ship.comp.Get(temp).getY() = y
					ship.comp.Get(temp).takeDamage(-2)
					If ship.comp.Get(temp).getHealth() >= ship.comp.Get(temp).getMaxHealth()
						ship.taskmanager.Remove(x, y, Task.SYSTEM_REPAIR)
						aiWait()
					End
					Return
				End
			Next
		End
	End
	
	Method HealSelf:Void()
		counter += 1
		direction = CSCode.S
		If skills.isDone(SkillManager.SKILL_MEDIC, counter)
			counter = 0
			health += 1
			If health >= GetMaxHp()
				aiWait()
				Return
			End
		End
	End
	
	Method PilotShip:Void()
		ai = AICode.PILOT
		direction = CSCode.E
		If ship.enginePower = Ship.OFF
			aiWait()
			Return
		End
		workCode = AICode.WORK_CODE_PILOT
	End
	Method ManGuns:Void()
		direction = CSCode.E
		ai = AICode.WEAPONS
		workCode = AICode.WORK_CODE_WEAPONS
	End
	
	Method PatchHull:Void()
		ai = AICode.PATCHING_HULL
		counter += 1
		If skills.isDone(SkillManager.SKILL_WELDING, counter)
			counter = 0
			random_x = -1
			random_y = -1
			Print "HP: " + ship.HP
			ship.RepairHull(1)
			aiWait()
		End
	End
	
	Method MajorRepair:Void()
		direction = CSCode.N
		ai = AICode.MAJOR_REPAIR
		counter += 1
		If skills.isDone(SkillManager.SKILL_REPAIR, counter + GetBonus(PerkManager.REPAIR))
			counter = 0
			Local repair:Int = 3
			If ship.pipes[x * Ship.MAX_X + y].power Then repair -= 1
			If ship.pipes[x * Ship.MAX_X + y].coolant Then repair -= 1
			ship.pipes[x * Ship.MAX_X + y].health += Max(1, repair)
			If ship.pipes[x * Ship.MAX_X + y].health >= ship.hullHealth
				aiWait()
				ship.taskmanager.Remove(x, y, Task.MAJOR_REPAIR)
				Return
			End
		End
	End
	Method MinorRepair:Void()
		ai = AICode.MINOR_REPAIR
		counter += 1
		If skills.isDone(SkillManager.SKILL_REPAIR, counter + GetBonus(PerkManager.REPAIR))
			counter = 0
			Local repair:Int = 3
			If ship.pipes[x * Ship.MAX_X + y].power Then repair -= 1
			If ship.pipes[x * Ship.MAX_X + y].coolant Then repair -= 1
			ship.pipes[x * Ship.MAX_X + y].health += Max(1, repair)
			If ship.pipes[x * Ship.MAX_X + y].health >= ship.hullHealth
				aiWait()
				ship.taskmanager.Remove(x, y, Task.MINOR_REPAIR)
				Return
			End
		End
	End
	
	Method Render:Void()
		If img[direction] = Null
			SetColor(240, 10, 136)
			DrawCircle( (x * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2) + x_offset, (y * Ship.GRID_SIZE) + (Ship.GRID_SIZE / 2) + y_offset, 15)
			ResetColorAlpha()
		Else
			DrawImage(img[direction], (x * Ship.GRID_SIZE) + x_offset + (Ship.GRID_SIZE / 2), (y * Ship.GRID_SIZE) + y_offset)
		End
		
		If DRAW_PATHS
			If path <> Null
				SetColor(0, 255, 0)
				For Local temp:Int = 0 Until path.Length()
					DrawCircle( (path.Get(temp).x * 30) + 15, (path.Get(temp).y * 30) + 15, 5)
				Next
				ResetColorAlpha()
			End
		End
	End
	
	Method TakeDamage:Void(damage:Int, type:String = "NULL")
		Select type.ToUpper()
			Case "ION"
				damage *= ( (100 - ion_defense) / 100.0)
			Case "FIRE"
				damage *= ( (100 - fire_defense) / 100.0)
			Case "GAMMA"
				damage *= ( (100 - gamma_defense) / 100.0)
		End
		If damage <= 0 Then Return
		health -= damage
		skills.GainXP(SkillManager.SKILL_TOUGHNESS, damage)
		MoraleEvent(Morale.DAMAGE, damage)
		If health <= 0 Then
			health = 0
			dead = True
		End
	End
	
	Method isDead:Bool()
		Return dead
	End
	
	Method PathSanityCheck:Bool(type:Int, target:Int = 0)
		If type <> PathType.PLAYER_ORDER
			If ship.pathSaver > 0 Then Return False
		End
		
		Select type
			Case PathType.PLAYER_ORDER
				If isDupe(order_x, order_y)
					Return False
				End
				Return True
'			Case PathType.COMPONENT
'				For Local temp:Int = 0 Until ship.comp.Length()
'					If isDupe(ship.comp.Get(temp).getX(), ship.comp.Get(temp).getY()) = False And ship.comp.Get(temp).getType() = target And ship.comp.Get(temp).isActive()
'						Return True
'					End
'				Next
			Case PathType.SCRAP_SYSTEM
				For Local temp:Int = 0 Until ship.comp.Length()
					If isDupe(ship.comp.Get(temp).getX(), ship.comp.Get(temp).getY()) = False
						Return True
					End
				Next
				
				Return False
			Case PathType.OXYGEN
			For Local x:Int = 0 Until Ship.MAX_X
				For Local y:Int = 0 Until Ship.MAX_Y
					If isDupe(x, y) = False and ship.pipes[x * Ship.MAX_X + y].o2 >= target And (ship.pipes[x * Ship.MAX_X + y].floor or ship.pipes[x * Ship.MAX_X + y].walkway)
						Return True
					End
				
				Next
			Next
		End
		
		Return False
		
	End
	
	Method FindPath:Stack<SimplePath>(type:Int, target:int = 0)
		'Print "pathfind" + Millisecs()
	'	Print first_name
		Local notFlamable:Bool
		If fire_damage <= 0 Then notFlamable = True
		#rem
		If type = PathType.JOBCODE Print "jobcode " + target
		If type = PathType.PERMCODE Print "permcode " + target
		If type = PathType.ANYWHERE Print "moveing to an empty space"
		If type = PathType.OXYGEN Print "oxygen"
		If type = PathType.OTHER_SHIP Print "finding teleporter"
		If type = PathType.SCRAP_SYSTEM Print "finding system to scrap"
		If type = PathType.FREE_TILE Print "moving out of the way"
		If type = PathType.RANDOM_ORDER Print "looking for something to do"
		If type = PathType.PLAYER_ORDER
			Print "finding player order"
			Print order_x + "/" + order_y
		End
		#end
		Local pathList:Stack<PathNode> = New Stack<PathNode>
		pathList.Push(New PathNode(x, y, 0, Null, False))
		For Local tempA:Int = 0 Until pathList.Length()
			Local pn:PathNode = pathList.Get(tempA)
			If pn.dead = True Then Continue
			
			'// path find types
			Select type
				Case PathType.FREE_TILE
					If isDupe(pn.x, pn.y) = False
						If ( not ship.taskmanager.isBlockingJob(pn.x, pn.y) And (ship.pipes[pn.x * Ship.MAX_X + pn.y].fire <= 0 or notFlamable)) And ship.pipes[pn.x * Ship.MAX_X + pn.y].floor
						'	Print "found it (free tile)"
							ship.pathSaver = Ship.PATHFINDING_COOLDOWN
						'	Print pathList.Length()
							SetTarget(pn.x, pn.y)
							Local s:Stack<SimplePath> = New Stack<SimplePath>
							While (pn.last <> Null)
								s.Push(New SimplePath(pn.x, pn.y))
								pn = pn.last
							Wend
							Return s
						End
					End
				Case PathType.RANDOM_ORDER
					If isDupe(pn.x, pn.y) = False
						If pn.x = random_x And pn.y = random_y And (ship.pipes[pn.x * Ship.MAX_X + pn.y].fire <= 0 or notFlamable)
						'	Print "found it (random order)"
							ship.pathSaver = Ship.PATHFINDING_COOLDOWN
						'	Print pathList.Length()
							SetTarget(pn.x, pn.y)
							Local s:Stack<SimplePath> = New Stack<SimplePath>
							While (pn.last <> Null)
								s.Push(New SimplePath(pn.x, pn.y))
								pn = pn.last
							Wend
							Return s
						End
					End
				Case PathType.PLAYER_ORDER
					If isDupe(pn.x, pn.y) = False
						If pn.x = order_x And pn.y = order_y And (ship.pipes[pn.x * Ship.MAX_X + pn.y].fire <= 0 or notFlamable)
						'	Print "found it (order)"
							ship.pathSaver = Ship.PATHFINDING_COOLDOWN
						'	Print pathList.Length()
							SetTarget(pn.x, pn.y)
							Local s:Stack<SimplePath> = New Stack<SimplePath>
							While (pn.last <> Null)
								s.Push(New SimplePath(pn.x, pn.y))
								pn = pn.last
							Wend
							Return s
						End
					End
				Case PathType.PERMCODE
					If isDupe(pn.x, pn.y) = False
						For Local temp:Int = 0 Until ship.taskmanager.permList.Length()
							If ship.taskmanager.permList.Get(temp).isMatch(pn.x, pn.y, target) And (ship.pipes[pn.x * Ship.MAX_X + pn.y].fire <= 0 or notFlamable)
							'	Print "found it (permcode) " + target
								ship.pathSaver = Ship.PATHFINDING_COOLDOWN
							'	Print pathList.Length()
								ship.pathcount += pathList.Length()
								SetTarget(pn.x, pn.y)
								Local s:Stack<SimplePath> = New Stack<SimplePath>
								While (pn.last <> Null)
									s.Push(New SimplePath(pn.x, pn.y))
									pn = pn.last
								Wend
								Return s
							End
						Next
					End
				Case PathType.JOBCODE
					If isDupe(pn.x, pn.y) = False
						For Local temp:Int = 0 Until ship.taskmanager.taskList.Length()
							If ship.taskmanager.taskList.Get(temp).isMatch(pn.x, pn.y, target) And (ship.pipes[pn.x * Ship.MAX_X + pn.y].fire <= 0 or notFlamable)
							'	Print "found it (jobcode) " + target
								ship.pathSaver = Ship.PATHFINDING_COOLDOWN
							'	Print pathList.Length()
								SetTarget(pn.x, pn.y)
								Local s:Stack<SimplePath> = New Stack<SimplePath>
								While (pn.last <> Null)
									s.Push(New SimplePath(pn.x, pn.y))
									pn = pn.last
								Wend
								Return s
							End
						Next
					End
				Case PathType.ANYWHERE
					If isDupe(pn.x, pn.y) = False And pn.x <> target_x And pn.y <> target_y And (ship.pipes[pn.x * Ship.MAX_X + pn.y].fire <= 0 or notFlamable)
					'	Print "found it (move anywhere)"
						ship.pathSaver = Ship.PATHFINDING_COOLDOWN
					'	Print pathList.Length()
						SetTarget(pn.x, pn.y)
						Local s:Stack<SimplePath> = New Stack<SimplePath>
						While (pn.last <> Null)
							s.Push(New SimplePath(pn.x, pn.y))
							pn = pn.last
						Wend
						Return s
					End
				Case PathType.SCRAP_SYSTEM
					For Local x:Int = 0 Until ship.comp.Length()
						If isDupe(pn.x, pn.y) = False
							If ship.comp.Get(x).getX() = pn.x And ship.comp.Get(x).getY() = pn.y
							'	Print "found it (scrap system)"
								ship.pathSaver = Ship.PATHFINDING_COOLDOWN 
							'	Print pathList.Length()
								SetTarget(pn.x, pn.y)
								Local s:Stack<SimplePath> = New Stack<SimplePath>
								While (pn.last <> Null)
									s.Push(New SimplePath(pn.x, pn.y))
									pn = pn.last
								Wend
								Return s
							End
						End
					Next
				Case PathType.FLOOR
					If isDupe(pn.x, pn.y) = False And ship.pipes[pn.x * Ship.MAX_X + pn.y].floor And (ship.pipes[pn.x * Ship.MAX_X + pn.y].fire <= 0 or notFlamable)
						Local s:Stack<SimplePath> = New Stack<SimplePath>
					'	Print "found it (floor tile)"
						ship.pathSaver = Ship.PATHFINDING_COOLDOWN
						SetTarget(pn.x, pn.y)
						While (pn.last <> Null)
							s.Push(New SimplePath(pn.x, pn.y))
							pn = pn.last
						Wend
						Return s
					End
				Case PathType.OXYGEN
					If isDupe(pn.x, pn.y) = False and ship.pipes[pn.x * Ship.MAX_X + pn.y].o2 >= target And (ship.pipes[pn.x * Ship.MAX_X + pn.y].floor or ship.pipes[pn.x * Ship.MAX_X + pn.y].walkway) And (ship.pipes[pn.x * Ship.MAX_X + pn.y].fire <= 0 or notFlamable)
						Local s:Stack<SimplePath> = New Stack<SimplePath>
					'	Print "found it room with o2 > " + target
						ship.pathSaver = Ship.PATHFINDING_COOLDOWN
						SetTarget(pn.x, pn.y)
						While (pn.last <> Null)
							s.Push(New SimplePath(pn.x, pn.y))
							pn = pn.last
						Wend
						Return s
					End	
'				Case PathType.PIPE_DAMAGE
'					If isDupe(pn.x, pn.y) = False and ship.pipes[pn.x * Ship.MAX_X + pn.y].health < 100
'						If ship.pipes[pn.x * Ship.MAX_X + pn.y].coolant or ship.pipes[pn.x * Ship.MAX_X + pn.y].oxygen or ship.pipes[pn.x * Ship.MAX_X + pn.y].power
'							Print "found it (pipe damage)"
'							ship.pathSaver = Ship.PATHFINDING_COOLDOWN
'							SetTarget(pn.x, pn.y)
'							Local s:Stack<SimplePath> = New Stack<SimplePath>
'							While (pn.last <> Null)
'								s.Push(New SimplePath(pn.x, pn.y))
'								pn = pn.last
'							Wend
'							Return s
'						End
'					End
'				Case PathType.HULL_DAMAGE
'					If isDupe(pn.x, pn.y) = False and ship.pipes[pn.x * Ship.MAX_X + pn.y].health < 100
'						Print "found it (hull damage)"
'						ship.pathSaver = Ship.PATHFINDING_COOLDOWN 
'						SetTarget(pn.x, pn.y)
'						Local s:Stack<SimplePath> = New Stack<SimplePath>
'						While (pn.last <> Null)
'							s.Push(New SimplePath(pn.x, pn.y))
'							pn = pn.last
'						Wend
'						Return s
'					End
				Default
					Print "no pathfinding target " + type
			End
			
			
			
			
			
			
			pn.dead = True
			If CheckList(pn.x + 1, pn.y, pathList)
				If isNodeValid(pn.x + 1, pn.y, pn.x, pn.y)
					pathList.Push(New PathNode(pn.x + 1, pn.y, pn.cost + 10, pn, False))
				End
			End
			If CheckList(pn.x - 1, pn.y, pathList)
				If isNodeValid(pn.x - 1, pn.y, pn.x, pn.y)
					pathList.Push(New PathNode(pn.x - 1, pn.y, pn.cost + 10, pn, False))
				End
			End
			If CheckList(pn.x, pn.y + 1, pathList)
				If isNodeValid(pn.x, pn.y + 1, pn.x, pn.y)
					pathList.Push(New PathNode(pn.x, pn.y + 1, pn.cost + 10, pn, False))
				End
			End
			If CheckList(pn.x, pn.y - 1, pathList)
				If isNodeValid(pn.x, pn.y - 1, pn.x, pn.y)
					pathList.Push(New PathNode(pn.x, pn.y - 1, pn.cost + 10, pn, False))
				End
			End
			If CheckList(pn.x + 1, pn.y + 1, pathList)
				If isNodeValid(pn.x + 1, pn.y + 1, pn.x, pn.y)
					pathList.Push(New PathNode(pn.x + 1, pn.y + 1, pn.cost + 14, pn, False))
				End
			End
			If CheckList(pn.x - 1, pn.y - 1, pathList)
				If isNodeValid(pn.x - 1, pn.y - 1, pn.x, pn.y)
					pathList.Push(New PathNode(pn.x - 1, pn.y - 1, pn.cost + 14, pn, False))
				End
			End
			If CheckList(pn.x + 1, pn.y - 1, pathList)
				If isNodeValid(pn.x + 1, pn.y - 1, pn.x, pn.y)
					pathList.Push(New PathNode(pn.x + 1, pn.y - 1, pn.cost + 14, pn, False))
				End
			End
			If CheckList(pn.x - 1, pn.y + 1, pathList)
				If isNodeValid(pn.x - 1, pn.y + 1, pn.x, pn.y)
					pathList.Push(New PathNode(pn.x - 1, pn.y + 1, pn.cost + 14, pn, False))
				End
			End
			
		Next
		
		Print "no path found: " + pathList.Length()
		Return Null
	End
	
	Method isDupe:bool(x:Int, y:Int)
		If hasBoarded
			For Local temp:Int = 0 Until ship.targetShip.crew.Length()
				If x = target_x And y = target_y Then Return False
				If x = ship.targetShip.crew.Get(temp).target_x And y = ship.targetShip.crew.Get(temp).target_y
					Return True
				End
			Next
		Else
			For Local temp:Int = 0 Until ship.crew.Length()
				'If x = target_x And y = target_y Then Return False
				If x = ship.crew.Get(temp).x And y = ship.crew.Get(temp).y 'standing on tile
					If Not (x = Self.x And y = Self.y) 'not self
						Return True
					End
				End
				If x = ship.crew.Get(temp).target_x And y = ship.crew.Get(temp).target_y 'has this target
					If not (x = target_x And y = target_y) 'not self
						Return True
					End
				End
			Next
		End
	

		Return False
	End
	
	Method SetTarget:Void(x:Int, y:Int)
		target_x = x
		target_y = y
	End
	
	Method ResetOrders:Void()
		order_x = -1
		order_y = -1
		aiWait()
	End
	
	Method hasOrders:Bool()
		If order_x > 0 Then Return True
		If order_y > 0 Then Return True
		Return false
	End
	
	Method isNodeValid:Bool(newx:int, newy:int, oldx:Int, oldy:int)
		If ship.RoomExists(newx, newy)
			'//walking in a straight line
			If newx = oldx or newy = oldy
				Return ship.pipes[newx * Ship.MAX_X + newy].isWalkable(ship.canEVA())
			Else
				If ship.pipes[newx * Ship.MAX_X + newy].isWalkable(ship.canEVA()) = False Then Return False
				If newx < oldx And newy < oldy '//nw
					If ship.pipes[ (oldx) * Ship.MAX_X + (oldy - 1)].isWalkable(ship.canEVA()) = False Then Return False 'north
					If ship.pipes[ (oldx - 1) * Ship.MAX_X + (oldy)].isWalkable(ship.canEVA()) = False Then Return False 'west
				#rem
					
					If ship.pipes[ (oldx) * Ship.MAX_X + (oldy + 1)].isWalkable() = False Then Return False 'south
					If ship.pipes[ (oldx - 1) * Ship.MAX_X + (oldy)].isWalkable() = False Then Return False 'west
					If ship.pipes[ (oldx + 1) * Ship.MAX_X + (oldy)].isWalkable() = False Then Return False 'east
				#end
				End
				If newx > oldx And newy > oldy '//se
					If ship.pipes[ (oldx) * Ship.MAX_X + (oldy + 1)].isWalkable(ship.canEVA()) = False Then Return False 'south
					If ship.pipes[ (oldx + 1) * Ship.MAX_X + (oldy)].isWalkable(ship.canEVA()) = False Then Return False 'east
				End
				If newx < oldx And newy > oldy '//sw
					If ship.pipes[ (oldx) * Ship.MAX_X + (oldy + 1)].isWalkable(ship.canEVA()) = False Then Return False 'south
					If ship.pipes[ (oldx - 1) * Ship.MAX_X + (oldy)].isWalkable(ship.canEVA()) = False Then Return False 'west
				End
				If newx > oldx And newy < oldy '//ne
					If ship.pipes[ (oldx) * Ship.MAX_X + (oldy - 1)].isWalkable(ship.canEVA()) = False Then Return False 'north
					If ship.pipes[ (oldx + 1) * Ship.MAX_X + (oldy)].isWalkable(ship.canEVA()) = False Then Return False 'east
				End
				Return True
			End
		End
		Return False
	End
	
	Method CheckList:Bool(x:Int, y:Int, nodes:Stack<PathNode>)
		For Local temp:Int = 0 Until nodes.Length()
			If nodes.Get(temp).x = x And nodes.Get(temp).y = y Then Return False
		Next
		Return True
	End
	
	#rem
	Method isMouseOver:Bool()

		If Game.Cursor.x() > (x_offset + (x * 30)) And Game.Cursor.x() < (x_offset + (x * 30) + 30) And Game.Cursor.y() > (y_offset + (y * 30)) And Game.Cursor.y() < (y_offset + (y * 30) + 30) Then Return True
		Return False

		If ship.mirrored
			If Game.Cursor.x() > (ship.x_offset + (Ship.MAX_X * Ship.GRID_SIZE) - (x * 30) - 30) And Game.Cursor.x() < (x_offset + (Ship.MAX_X * Ship.GRID_SIZE) - (x * 30)) And Game.Cursor.y() > (y_offset + (y * 30)) And Game.Cursor.y() < (y_offset + (y * 30) + 30) Then Return True
		Else
			If Game.Cursor.x() > (x_offset + (x * 30)) And Game.Cursor.x() < (x_offset + (x * 30) + 30) And Game.Cursor.y() > (y_offset + (y * 30)) And Game.Cursor.y() < (y_offset + (y * 30) + 30) Then Return True
		End
		Return False
	End
	#end
	
	Method isMouseOver:Bool()
		If ship.mirrored
			If Game.Cursor.x() > (ship.x_offset + (Ship.MAX_X * Ship.GRID_SIZE) - (x * Ship.GRID_SIZE) - Ship.GRID_SIZE - x_offset)
				If Game.Cursor.x() < (ship.x_offset + (Ship.MAX_X * Ship.GRID_SIZE) - (x * Ship.GRID_SIZE) - x_offset)
					If Game.Cursor.y() > (ship.y_offset + (y * Ship.GRID_SIZE) + y_offset)
						If Game.Cursor.y() < (ship.y_offset + (y * Ship.GRID_SIZE) + y_offset + Ship.GRID_SIZE) Then Return True
					End
				End
			End
		Else
			If Game.Cursor.x() > (ship.x_offset + (x * Ship.GRID_SIZE) + x_offset) And Game.Cursor.x() < (ship.x_offset + (x * Ship.GRID_SIZE) + x_offset + Ship.GRID_SIZE) And Game.Cursor.y() > (ship.y_offset + (y * Ship.GRID_SIZE) + y_offset) And Game.Cursor.y() < (ship.y_offset + (y * Ship.GRID_SIZE) + y_offset + Ship.GRID_SIZE) Then Return True
		End
		
		Return False
	End
End



Class SimplePath
	Field x:Int
	Field y:Int
	Method New(x:Int, y:Int)
		Self.x = x
		Self.y = y
	End
End

Class PathNode
	Field cost:Int
	Field last:PathNode
	Field dead:bool
	Field x:Int
	Field y:int
	Method New(x:Int, y:Int, cost:Int, last:PathNode, dead:bool)
		Self.dead = dead
		Self.x = x
		Self.y = y
		Self.cost = cost
		Self.last = last
'		Print "node"
	'	Print "x: " + x
	'	Print "y: " + y
	'	If dead = True
	'		Print "dead"
	'	Else
	'		Print "not dead"
	'	End
	End
End