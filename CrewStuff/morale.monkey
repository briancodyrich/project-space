Strict

Import space_game

Class Morale
	Const NULL_TYPE:Int = 0
	Const TIME_AWAY:Int = 1
	Const TIME_OFF:Int = 2
	Const DAMAGE:Int = 3
	Const TIME_WORKED:Int = 4
	Const CREW_DEATH:Int = 5
	Const TIRED:Int = 6
	Const VICTORY:Int = 7
	
	Field type:Int
	Field cooldown:float
	Field damage:float
	Field max:float
	Field value:float
	Field isPositiveType:bool
	Method New(data:JsonObject)
		Self.type = IdFromString(data.GetString("name"))
		Self.cooldown = data.GetFloat("cooldown")
		Self.damage = data.GetFloat("damage")
		Self.max = data.GetFloat("max")
		If damage < 0.0 Then isPositiveType = True
		value = 0
	End
	
	Method OnSave:JsonObject()
		Local data:JsonObject = New JsonObject()
		data.SetInt("type", type)
		data.SetFloat("value", value)
		Return data
	End
	
	Method Tick:Void()
		If isPositiveType
			value = Clamp(value + cooldown, max, 0.0)
		Else
			value = Clamp(value - cooldown, 0.0, max)
		End
	End
	Method Reset:Void()
		value = 0
	End
	Method Add:Void(num:Int, morale_mod:int)
		Local mmod:Float = 1.0
		If isPositiveType
			mmod += (morale_mod / 100.0)
		Else
			mmod -= (morale_mod / 100.0)
		End
		value += (damage * num) * mmod
	End
	Method GetCost:Float()
		Return value
	End
	Function IdFromString:Int(s:String)
		s = s.ToUpper()
		Select s
			Case "TIME_AWAY"
			Return TIME_AWAY
			Case "TIME_OFF"
			Return TIME_OFF
			Case "DAMAGE"
			Return DAMAGE
			Case "TIME_WORKED"
			Return TIME_WORKED
			Case "CREW_DEATH"
			Return CREW_DEATH
			Case "TIRED"
			Return TIRED
			Case "VICTORY"
			Return VICTORY
			Default
				Print "MISSING MORALE ID"
		End
		Return 0
	End
	Function IdToString:String(id:int)
		Select id
			Case TIME_AWAY
				Return "TIME_AWAY"
			Case TIME_OFF
				Return "TIME_OFF"
			Case DAMAGE
				Return "DAMAGE"
			Case TIME_WORKED
				Return "TIME_WORKED"
			Case CREW_DEATH
				Return "CREW_DEATH"
			Case TIRED
				Return "TIRED"
			Case VICTORY
				Return "VICTORY"
			
		End
		Return "NO IDEA"
	End 
End





















