Import space_game

Class EquipmentManager
	Global weapon_icon:Image
	Global system_icon:Image
	Global solar_icon:Image
	Global shield_icon:Image
	Global reactor_icon:Image
	Global engine_icon:Image
	Global crew_icon:Image
	Global coolant_icon:Image
	Global battery_icon:Image
	
	
	Const GRID_SIZE:Int = 120
	Const OVER_TIME_POPUP:Int = 12
	Const PADDING:Int = 5
	Const POPUP_W:Int = 3
	Const POPUP_H:Int = 2
	Field over:Item
	Field x:Int
	Field y:Int
	Field rows:Int
	Field cols:Int
	Field overx:Int
	Field overy:Int
	Field lastx:Int
	Field lasty:Int
	Field overtime:Int = 0
	Field alpha:Float
	Field selected:bool
	Field kill:bool
	Field yoffset:Int
	Field ship:Ship
	
	Field vRows:int
	Field yOffset:Int
	Field nub:float = 0
	Field nubpercent:float
	Field nubBar:bool
	Field grab:GrabItem
	Field storeData:Economy
	Field slots:Stack<Stack<EquipmentSlot>>
	Field pass:PassData
	Field filter:ItemFilter
	Field delta:DeltaItem
	Field deltaLock:bool
	Field deltaLockItem:bool
	Field dr:Int = -1
	Field dc:int = -1
	
	Method New(x:int, y:Int, rows:Int, cols:Int, pass:PassData, ship:Ship, grab:GrabItem, storeData:Economy = Null, delta:DeltaItem = Null)
		filter = New ItemFilter()
		Self.pass = pass
		Self.x = x
		Self.y = y
		Self.rows = rows
		Self.cols = cols
		Self.grab = grab
		Self.ship = ship
		Self.storeData = storeData
		Self.delta = delta
		slots = New Stack<Stack<EquipmentSlot>>()
		'Engines
		Local nslot:Stack<EquipmentSlot> = New Stack<EquipmentSlot>
		For Local s:Slot = EachIn ship.compSlots
			If s.type = Component.ENGINE
				nslot.Push(New EquipmentSlot(s.x, s.y, s.type, ship))
			End
		Next
		If nslot.Length() > 0
			While nslot.Length() > rows
				Local tempslot:Stack<EquipmentSlot> = New Stack<EquipmentSlot>()
				For Local temp:Int = 0 Until rows
					tempslot.Push(nslot.Pop())
				Next
				slots.Push(tempslot)
			Wend
			slots.Push(nslot)
		End
		'Weapons
		nslot = New Stack<EquipmentSlot>
		For Local s:Slot = EachIn ship.compSlots
			If s.type = Component.HARDPOINT
				nslot.Push(New EquipmentSlot(s.x, s.y, s.type, ship))
			End
		Next
		If nslot.Length() > 0
			While nslot.Length() > rows
				Local tempslot:Stack<EquipmentSlot> = New Stack<EquipmentSlot>()
				For Local temp:Int = 0 Until rows
					tempslot.Push(nslot.Pop())
				Next
				slots.Push(tempslot)
			Wend
			slots.Push(nslot)
		End
		'Reactors
		nslot = New Stack<EquipmentSlot>
		For Local s:Slot = EachIn ship.compSlots
			If s.type = Component.REACTOR
				nslot.Push(New EquipmentSlot(s.x, s.y, s.type, ship))
			End
		Next
		If nslot.Length() > 0
			While nslot.Length() > rows
				Local tempslot:Stack<EquipmentSlot> = New Stack<EquipmentSlot>()
				For Local temp:Int = 0 Until rows
					tempslot.Push(nslot.Pop())
				Next
				slots.Push(tempslot)
			Wend
			slots.Push(nslot)
		End
		'Ship Slots
		nslot = New Stack<EquipmentSlot>
		For Local s:Slot = EachIn ship.compSlots
			If s.type = Component.SYSTEM_SLOT
				nslot.Push(New EquipmentSlot(s.x, s.y, s.type, ship))
			End
		Next
		If nslot.Length() > 0
			While nslot.Length() > rows
				Local tempslot:Stack<EquipmentSlot> = New Stack<EquipmentSlot>()
				For Local temp:Int = 0 Until rows
					tempslot.Push(nslot.Pop())
				Next
				slots.Push(tempslot)
			Wend
			slots.Push(nslot)
		End
		'Computers
		nslot = New Stack<EquipmentSlot>
		For Local s:Slot = EachIn ship.compSlots
			If s.type = Component.COMPUTER
				nslot.Push(New EquipmentSlot(s.x, s.y, s.type, ship))
			End
		Next
		If nslot.Length() > 0
			While nslot.Length() > rows
				Local tempslot:Stack<EquipmentSlot> = New Stack<EquipmentSlot>()
				For Local temp:Int = 0 Until rows
					tempslot.Push(nslot.Pop())
				Next
				slots.Push(tempslot)
			Wend
			slots.Push(nslot)
		End
		'Shields
		nslot = New Stack<EquipmentSlot>
		For Local s:Slot = EachIn ship.compSlots
			If s.type = Component.SHIELD
				nslot.Push(New EquipmentSlot(s.x, s.y, s.type, ship))
			End
		Next
		If nslot.Length() > 0
			While nslot.Length() > rows
				Local tempslot:Stack<EquipmentSlot> = New Stack<EquipmentSlot>()
				For Local temp:Int = 0 Until rows
					tempslot.Push(nslot.Pop())
				Next
				slots.Push(tempslot)
			Wend
			slots.Push(nslot)
		End
		'coolant
		nslot = New Stack<EquipmentSlot>
		For Local s:Slot = EachIn ship.compSlots
			If s.type = Component.COOLANT_TANK
				nslot.Push(New EquipmentSlot(s.x, s.y, s.type, ship))
			End
		Next
		If nslot.Length() > 0
			While nslot.Length() > rows
				Local tempslot:Stack<EquipmentSlot> = New Stack<EquipmentSlot>()
				For Local temp:Int = 0 Until rows
					tempslot.Push(nslot.Pop())
				Next
				slots.Push(tempslot)
			Wend
			slots.Push(nslot)
		End
		'batteries
		nslot = New Stack<EquipmentSlot>
		For Local s:Slot = EachIn ship.compSlots
			If s.type = Component.BATTERY
				nslot.Push(New EquipmentSlot(s.x, s.y, s.type, ship))
			End
		Next
		If nslot.Length() > 0
			While nslot.Length() > rows
				Local tempslot:Stack<EquipmentSlot> = New Stack<EquipmentSlot>()
				For Local temp:Int = 0 Until rows
					tempslot.Push(nslot.Pop())
				Next
				slots.Push(tempslot)
			Wend
			slots.Push(nslot)
		End
		'Solar
		nslot = New Stack<EquipmentSlot>
		For Local s:Slot = EachIn ship.compSlots
			If s.type = Component.SOLAR
				nslot.Push(New EquipmentSlot(s.x, s.y, s.type, ship))
			End
		Next
		If nslot.Length() > 0
			While nslot.Length() > rows
				Local tempslot:Stack<EquipmentSlot> = New Stack<EquipmentSlot>()
				For Local temp:Int = 0 Until rows
					tempslot.Push(nslot.Pop())
				Next
				slots.Push(tempslot)
			Wend
			slots.Push(nslot)
		End
		'crew tubes
		nslot = New Stack<EquipmentSlot>
		For Local s:Slot = EachIn ship.compSlots
			If s.type = Component.HEALTH
				nslot.Push(New EquipmentSlot(s.x, s.y, s.type, ship))
			End
		Next
		If nslot.Length() > 0
			While nslot.Length() > rows
				Local tempslot:Stack<EquipmentSlot> = New Stack<EquipmentSlot>()
				For Local temp:Int = 0 Until rows
					tempslot.Push(nslot.Pop())
				Next
				slots.Push(tempslot)
			Wend
			slots.Push(nslot)
		End
		
		Local count:int = slots.Length() - (rows)
		If (count > 0)
			vRows = count
		End
	End
	
	Method Render:Void()
		DrawRectOutline(x, y, (cols * GRID_SIZE) + ( (cols + 1) * PADDING), (rows * GRID_SIZE) + ( (rows + 1) * PADDING))
		If vRows > 0
			DrawRectOutline(x + (cols * GRID_SIZE) + ( (cols + 1) * PADDING), y, 30, (rows * GRID_SIZE) + ( (rows + 1) * PADDING))
			DrawRectOutline(x + (cols * GRID_SIZE) + ( (cols + 1) * PADDING) + 1, y + nub + 1, 28, 28 * 3)
			ResetColorAlpha()
		End
		SetScaledScissor(x, y, 5 + (cols * GRID_SIZE) + ( (cols + 1) * PADDING), 5 + (rows * GRID_SIZE) + ( (rows + 1) * PADDING))

		For Local i:Int = 0 Until slots.Length()
			For Local j:Int = 0 Until slots.Get(i).Length()
				If i = dr And j = dc
					SetAlpha(0.2)
					SetColor(255, 255, 0)
					DrawRect(GridToX(i, j), GridToY(i, j), GRID_SIZE, GRID_SIZE)
					ResetColorAlpha()
				End
				If slots.Get(i).Get(j).item <> Null and slots.Get(i).Get(j).item.icon <> Null And slots.Get(i).Get(j).item.count > 0
					DrawRectOutline(GridToX(i, j), GridToY(i, j), GRID_SIZE, GRID_SIZE)
					DrawImage(slots.Get(i).Get(j).item.icon, GridToX(i, j) + (GRID_SIZE / 2), GridToY(i, j) + (GRID_SIZE / 2))
					If slots.Get(i).Get(j).item.comp <> Null
						Local color:int = slots.Get(i).Get(j).item.comp.QualityLevelToColor(slots.Get(i).Get(j).item.comp.getQualityLevel())
						SetColor(HexToRed(color), HexToGreen(color), HexToBlue(color))
						DrawRectOutline(GridToX(i, j), GridToY(i, j), GRID_SIZE, GRID_SIZE)
						SetColor(255, 0, 0)
						DrawRect(GridToX(i, j) + GRID_SIZE - 5, GridToY(i, j) + GRID_SIZE, 5, -Clamp(int(slots.Get(i).Get(j).item.comp.getPermDamage() / 50.0 * GRID_SIZE), 0, GRID_SIZE - 1))
						ResetColorAlpha()
					End
				Else
					Select slots.Get(i).Get(j).type
						Case Component.HARDPOINT
							If weapon_icon <> Null
								DrawImage(weapon_icon, GridToX(i, j) + (GRID_SIZE / 2), GridToY(i, j) + (GRID_SIZE / 2))
							End
						Case Component.SYSTEM_SLOT
							If system_icon <> Null
								DrawImage(system_icon, GridToX(i, j) + (GRID_SIZE / 2), GridToY(i, j) + (GRID_SIZE / 2))
							End
						Case Component.SOLAR
							If solar_icon <> Null
								DrawImage(solar_icon, GridToX(i, j) + (GRID_SIZE / 2), GridToY(i, j) + (GRID_SIZE / 2))
							End
						Case Component.SHIELD
							If shield_icon <> Null
								DrawImage(shield_icon, GridToX(i, j) + (GRID_SIZE / 2), GridToY(i, j) + (GRID_SIZE / 2))
							End
						Case Component.REACTOR
							If reactor_icon <> Null
								DrawImage(reactor_icon, GridToX(i, j) + (GRID_SIZE / 2), GridToY(i, j) + (GRID_SIZE / 2))
							End
						Case Component.ENGINE
							If engine_icon <> Null
								DrawImage(engine_icon, GridToX(i, j) + (GRID_SIZE / 2), GridToY(i, j) + (GRID_SIZE / 2))
							End
						Case Component.HEALTH
							If crew_icon <> Null
								DrawImage(crew_icon, GridToX(i, j) + (GRID_SIZE / 2), GridToY(i, j) + (GRID_SIZE / 2))
							End
						Case Component.COOLANT_TANK
							If coolant_icon <> Null
								DrawImage(coolant_icon, GridToX(i, j) + (GRID_SIZE / 2), GridToY(i, j) + (GRID_SIZE / 2))
							End
						Case Component.BATTERY
							If battery_icon <> Null
								DrawImage(battery_icon, GridToX(i, j) + (GRID_SIZE / 2), GridToY(i, j) + (GRID_SIZE / 2))
							End
					End
					DrawRectOutline(GridToX(i, j), GridToY(i, j), GRID_SIZE, GRID_SIZE)
				End
			Next
		Next
		#rem
		For Local temp:Int = 0 Until items.slots.Length()
		If items.slots[temp] <> Null
		DrawImage(items.slots[temp].icon, GridToX(r, c) + 5 + (GRID_SIZE / 2), GridToY(r, c) + 5 + (GRID_SIZE / 2))
		Game.white_font.Draw(items.slots[temp].count + "/" + items.slots[temp].max, GridToX(r, c) + 5, GridToY(r, c) + 80)
		End
		
		c += 1
		If c >= cols
		r += 1
		c = 0
		End
		Next
		#end
		If selected And over <> Null
			Local xPos:Int = GridToX(overy + 1, overx)
			Local yPos:Int = GridToY(overy + 1, overx)
			Local width:Int = GRID_SIZE * POPUP_W + ( (POPUP_W - 1) * PADDING)
			Local height:Int = GRID_SIZE * POPUP_H + ( (POPUP_H - 1) * PADDING)
			
			
			While (overx + POPUP_W > cols)
				overx -= 1
				xPos = GridToX(overy + 1, overx)
				yPos = GridToY(overy + 1, overx)
			Wend
			If yPos > ( (rows - POPUP_H) * GRID_SIZE) + y + ( (rows + 1) * PADDING)
				overy -= (1 + POPUP_H)
				xPos = GridToX(overy + 1, overx)
				yPos = GridToY(overy + 1, overx)
			End
			SetAlpha(alpha)
			DrawRectOutline(xPos, yPos, width, height)
			SetColor(0, 0, 0)
			DrawRect(xPos + 5, yPos + 5, width - 10, height - 10)
			SetColor(255, 255, 255)
			SetAlpha(alpha)
			Local s:Stack<String>
			Local doComp:Bool
			If grab.item <> Null And (over.type = grab.item.type) And over <> grab.item
				Game.white_font.Draw(grab.item.formatedName, xPos + width / 2, yPos + 10, 0.5)
				s = grab.item.GetDescr(over)
				doComp = True
			Else
				Game.white_font.Draw(over.formatedName, xPos + width / 2, yPos + 10, 0.5)
				If storeData = Null
					s = over.GetDescr()
				Else
					s = over.GetDescr(, True, storeData)
				End
			End
			For Local temp:Int = 0 Until s.Length()
				Game.white_font.Draw(s.Get(temp), xPos + 10, yPos + 50 + (temp * 30))
			Next
			ResetColorAlpha()
		End
		SetScaledScissor(0, 0, ScreenWidth, ScreenHeight)

		
	End
	
	Method StartsWithValidNumber:Bool(str:String)
		Local i:int[] = str.ToChars()
		If i[0] >= 49 And i[0] <= 57 Then Return True
		Return False
	End
	
	Method Update:Bool()
		If delta <> Null
			If delta.new_item = Null
				If not deltaLock
					delta.old_item = Null
					dr = -1
					dc = -1
					delta.null_delta = False
				End
			Else If delta.old_item = Null
				If deltaLock
					delta.old_item = slots.Get(dr).Get(dc).item
					delta.shipx = slots.Get(dr).Get(dc).x
					delta.shipy = slots.Get(dr).Get(dc).y
				Else
					Local ch:Stack<EquipmentSlotMetaData> = New Stack<EquipmentSlotMetaData>()
					For Local r:Int = 0 Until slots.Length()
						For Local c:Int = 0 Until slots.Get(r).Length()
							If delta.new_item.type = slots.Get(r).Get(c).type Or (delta.new_item.type = Item.WEAPON And slots.Get(r).Get(c).type = Component.HARDPOINT)
								If slots.Get(r).Get(c).item = Null
									delta.null_delta = True
									dr = r
									dc = c
								Else
									ch.Push(New EquipmentSlotMetaData(slots.Get(r).Get(c), r, c))
								End
								#rem
								Local val:Float = slots.Get(r).Get(c).item.CompCompair(delta.new_item)
									max = val
									dr = r
									dc = c
									delta.old_item = slots.Get(r).Get(c).item
									delta.shipx = slots.Get(r).Get(c).x
									delta.shipy = slots.Get(r).Get(c).y
								#end
							End
						Next
					Next
					If not delta.null_delta
						If Not ch.IsEmpty()
							Local choice:EquipmentSlotMetaData = ch.Get(0)
							'Print "length: " + ch.Length()
							For Local temp:Int = 1 Until ch.Length()
								If choice.slot.item.isCompWeaker(ch.Get(temp).slot.item)
									choice = ch.Get(temp)
								End
							Next
							delta.old_item = choice.slot.item
							delta.shipx = choice.slot.x
							delta.shipy = choice.slot.y
							dr = choice.r
							dc = choice.c
						End
					End
				End
			End
		End
		Local change:Bool = False
		If Game.Cursor.MiddleHit() And CursorInScreen() 'And grab.item = Null
			kill = True
			nub = 0
			yOffset = 0
		End
		If grab.hasPass And grab.isTarget(pass) And filter.canTouch(grab.item.type) 'get the pass
			For Local r:Int = 0 Until slots.Length()
				For Local c:Int = 0 Until slots.Get(r).Length()
					If slots.Get(r).Get(c).item = Null And grab.success = False And (slots.Get(r).Get(c).type = grab.item.type Or (slots.Get(r).Get(c).type = Component.COMPUTER And grab.item.type = Item.SYSTEM) Or (slots.Get(r).Get(c).type = Component.HARDPOINT And grab.item.type = Item.WEAPON))
						If slots.Get(r).Get(c).type = Component.COMPUTER And grab.item.type = Item.SYSTEM
							For Local temp:Int = 0 Until ship.comp.Length()
								If grab.success Then Exit
								If slots.Get(r).Get(c).x = ship.comp.Get(temp).getX() And slots.Get(r).Get(c).y = ship.comp.Get(temp).getY()
									If grab.storedata <> Null And grab.storedata.type = Storage.STORE
										slots.Get(r).Get(c).item = New Item(ItemLoader.GetItem(grab.item.id), grab.item.damage, grab.item.qualityLevel)
										Computer(ship.comp.Get(temp)).system = slots.Get(r).Get(c).item
										grab.item.count -= 1
										grab.passSuccess()
										change = True
									Else
										slots.Get(r).Get(c).item = grab.item
										Computer(ship.comp.Get(temp)).system = grab.item
										grab.passSuccess()
										change = True
									End
								End
							Next
						Else If slots.Get(r).Get(c).type = Component.HARDPOINT And grab.item.type = Item.WEAPON
							For Local temp:Int = 0 Until ship.comp.Length()
								If grab.success Then Exit
								If slots.Get(r).Get(c).x = ship.comp.Get(temp).getX() And slots.Get(r).Get(c).y = ship.comp.Get(temp).getY()
									If grab.storedata <> Null And grab.storedata.type = Storage.STORE
										slots.Get(r).Get(c).item = New Item(ItemLoader.GetItem(grab.item.id), grab.item.damage, grab.item.qualityLevel)
										Hardpoint(ship.comp.Get(temp)).gun = slots.Get(r).Get(c).item.weapon
										grab.item.count -= 1
										grab.passSuccess()
										change = True
									Else
										slots.Get(r).Get(c).item = grab.item
										Hardpoint(ship.comp.Get(temp)).gun = grab.item.weapon
										grab.passSuccess()
										change = True
									End
								End
							Next
						Else
							If grab.item.type = Item.SYSTEM
								If grab.storedata <> Null And grab.storedata.type = Storage.STORE
									slots.Get(r).Get(c).item = New Item(ItemLoader.GetItem(grab.item.id), grab.item.damage, grab.item.qualityLevel)
									ship.systems[slots.Get(r).Get(c).x] = slots.Get(r).Get(c).item
									grab.item.count -= 1
									grab.passSuccess()
									change = True
								Else
									ship.systems[slots.Get(r).Get(c).x] = grab.item
									slots.Get(r).Get(c).item = grab.item
									grab.passSuccess()
									change = True
								End
							Else
								If grab.storedata <> Null And grab.storedata.type = Storage.STORE
									slots.Get(r).Get(c).item = New Item(ItemLoader.GetItem(grab.item.id), grab.item.damage, grab.item.qualityLevel)
									ship.AddComponent(slots.Get(r).Get(c).x, slots.Get(r).Get(c).y, slots.Get(r).Get(c).item.comp)
									grab.item.count -= 1
									grab.passSuccess()
									change = True
								Else
									ship.AddComponent(slots.Get(r).Get(c).x, slots.Get(r).Get(c).y, grab.item.comp)
									slots.Get(r).Get(c).item = grab.item
									grab.passSuccess()
									change = True
								End
							End
						End
					End
				Next
			Next
		End
		If grab.hasPass And grab.success = False And grab.isHome(pass) 'handle a failure
			If grab.hasBackupTarget()
				grab.swapToBackup()
			Else
				grab.ClearGrab()
			End
		Else If grab.hasPass And grab.success And grab.isHome(pass) 'handle a success
			For Local r:Int = 0 Until slots.Length()
				For Local c:Int = 0 Until slots.Get(r).Length()
					If slots.Get(r).Get(c).item = grab.item
						RemoveComponent(r, c)
						slots.Get(r).Get(c).item = Null
						grab.ClearGrab()
						change = True
					End
				Next
			Next
		End
		If CursorInScreen() And vRows > 0
			If Game.Cursor.WheelUp()
				Local num:Float = (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING) / float(vRows)
				nub -= num
				nub = Clamp(nub, 0.0, float(y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING))
				If nub = 0
					nubpercent = 0
				Else
					nubpercent = 1 + (float(nub - (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING))) / (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING)
				End
				yOffset = -nubpercent * ( (vRows) * GRID_SIZE + ( (vRows)) * PADDING)
			Else If Game.Cursor.WheelDown()
				Local num:Float = (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING) / float(vRows)
				nub += num
				nub = Clamp(nub, 0.0, float(y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING))
				If nub = 0
					nubpercent = 0
				Else
					nubpercent = 1 + (float(nub - (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING))) / (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING)
				End
				yOffset = -nubpercent * ( (vRows) * GRID_SIZE + ( (vRows)) * PADDING)
			End
		End
		
		
		If Game.Cursor.Hit()
			If Game.Cursor.x() > x + (cols * GRID_SIZE) + ( (cols + 1) * PADDING) And Game.Cursor.x() < x + (cols * GRID_SIZE) + ( (cols + 1) * PADDING) + 30 And Game.Cursor.y() > y And Game.Cursor.y() < y + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING
				nubBar = True
			End
		End
		
		If Game.Cursor.Down And nubBar
			nub = Clamp(int(Game.Cursor.y()) -y - 42, 0, y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING)
			If nub = 0
				nubpercent = 0
			Else
				nubpercent = 1 + (float(nub - (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING))) / (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING)
			End
			yOffset = -nubpercent * ( (vRows) * GRID_SIZE + ( (vRows)) * PADDING)
		Else
			nubBar = False
		End
	
		If Not kill
			overx = -1
			overy = -1
		End
		Local found:Bool
		For Local r:Int = 0 Until slots.Length()
			For Local c:Int = 0 Until slots.Get(r).Length()
				If isMouseOver(r, c) And CursorInScreen()
					If Game.Cursor.RightHit()
						If delta <> Null And slots.Get(r).Get(c).item <> Null
							If deltaLock
								If dr = r And dc = c
									deltaLock = False
									dr = -1
									dc = -1
								Else
									dr = r
									dc = c
									deltaLock = True
								End
							Else
								dr = r
								dc = c
								deltaLock = True
							End
						End
					End
					If (KeyDown(KEY_SHIFT) And Game.Cursor.Hit() And grab.item = Null And slots.Get(r).Get(c).item <> Null) And filter.canTouch(slots.Get(r).Get(c).item.type)
						If dr = r And dc = c
							deltaLock = False
							dr = -1
							dc = -1
						End
						grab.Pass(slots.Get(r).Get(c).item, pass)
					Else If (Game.Cursor.Hit() Or grab.isValidDrop())
						If (slots.Get(r).Get(c).item <> Null And grab.item = Null) And filter.canTouch(slots.Get(r).Get(c).item.type) 'grab an item yo
							If dr = r And dc = c
								deltaLock = False
								dr = -1
								dc = -1
							End
							grab.Grab(slots.Get(r).Get(c).item, pass)
							RemoveComponent(r, c)
							slots.Get(r).Get(c).item = Null
							kill = True
							change = True
							'Below is if its a valid equip
						Else If (grab.item <> Null and (slots.Get(r).Get(c).type = grab.item.type Or (slots.Get(r).Get(c).type = Component.COMPUTER And grab.item.type = Item.SYSTEM) Or (slots.Get(r).Get(c).type = Component.HARDPOINT And grab.item.type = Item.WEAPON))) And filter.canTouch(grab.item.type)
							If slots.Get(r).Get(c).type = Component.COMPUTER And grab.item.type = Item.SYSTEM
								If dr = r And dc = c
									deltaLock = False
									dr = -1
									dc = -1
								End
								For Local temp:Int = 0 Until ship.comp.Length()
									If slots.Get(r).Get(c).x = ship.comp.Get(temp).getX() And slots.Get(r).Get(c).y = ship.comp.Get(temp).getY()
										Local swap:Item = slots.Get(r).Get(c).item
										Computer(ship.comp.Get(temp)).system = grab.item
										slots.Get(r).Get(c).item = grab.item
										grab.item = swap
										change = True
									End
								Next
							Else If slots.Get(r).Get(c).type = Component.HARDPOINT And grab.item.type = Item.WEAPON
								If dr = r And dc = c
									deltaLock = False
									dr = -1
									dc = -1
								End
								For Local temp:Int = 0 Until ship.comp.Length()
									If slots.Get(r).Get(c).x = ship.comp.Get(temp).getX() And slots.Get(r).Get(c).y = ship.comp.Get(temp).getY()
										Local swap:Item = slots.Get(r).Get(c).item
										Hardpoint(ship.comp.Get(temp)).gun = grab.item.weapon
										slots.Get(r).Get(c).item = grab.item
										grab.item = swap
										change = True
									End
								Next
							Else If grab.item.type = Item.SYSTEM
								If dr = r And dc = c
									deltaLock = False
									dr = -1
									dc = -1
								End
								Local swap:Item = slots.Get(r).Get(c).item
								RemoveComponent(r, c)
								ship.systems[slots.Get(r).Get(c).x] = grab.item
								slots.Get(r).Get(c).item = grab.item
								grab.item = swap
								change = True
							Else
								If dr = r And dc = c
									deltaLock = False
									dr = -1
									dc = -1
								End
								Local swap:Item = slots.Get(r).Get(c).item
								RemoveComponent(r, c)
								ship.AddComponent(slots.Get(r).Get(c).x, slots.Get(r).Get(c).y, grab.item.comp)
								slots.Get(r).Get(c).item = grab.item
								grab.item = swap
								change = True
							End
							over = slots.Get(r).Get(c).item
						End
						
					End
				
					If Not kill
						found = True
						If slots.Get(r).Get(c).item <> Null
							overtime += 1
						End
						If overtime >= OVER_TIME_POPUP
							overtime = OVER_TIME_POPUP
							alpha += 0.1
							If alpha >= 1 Then alpha = 1
							If selected = False
								lastx = c
								lasty = r
								selected = True
								over = slots.Get(r).Get(c).item
							End
							If lastx = c And lasty = r
								overx = c
								overy = r
							Else
								overy = lasty
								overx = lastx
								kill = True
							End
						End
					End
				End
			Next
		Next
		#rem
		If grabItem <> Null And Not Game.Cursor.Down()
		items.slots[grabSlot] = grabItem
		grabItem = Null
		End
		#end
		
		If Not found
			overy = lasty
			overx = lastx
			kill = True
		End
		If kill
			alpha -= 0.1
			If alpha <= 0
				alpha = 0
				overtime = 0
				selected = False
				over = Null
				kill = False
			End
		End
		Return change
	End
	
	Method RemoveComponent:Void(r:Int, c:Int)
		If slots.Get(r).Get(c).type = Component.SYSTEM_SLOT
			ship.systems[slots.Get(r).Get(c).x] = Null
			Return
		End
		For Local temp:Int = 0 Until ship.comp.Length()
			If slots.Get(r).Get(c).x = ship.comp.Get(temp).getX() And slots.Get(r).Get(c).y = ship.comp.Get(temp).getY()
				If slots.Get(r).Get(c).type = Component.COMPUTER
					For Local xx:Int = 0 Until ship.comp.Length()
						If slots.Get(r).Get(c).x = ship.comp.Get(xx).getX() And slots.Get(r).Get(c).y = ship.comp.Get(xx).getY()
							Computer(ship.comp.Get(xx)).system = Null
						End
					Next
				Else If slots.Get(r).Get(c).type = Component.HARDPOINT
					For Local xx:Int = 0 Until ship.comp.Length()
						If slots.Get(r).Get(c).x = ship.comp.Get(xx).getX() And slots.Get(r).Get(c).y = ship.comp.Get(xx).getY()
							Hardpoint(ship.comp.Get(xx)).gun = Null
						End
					Next
				Else
					ship.comp.Remove(temp)
				End
			End
		Next
	End
	
	Method GridToX:Int(r:Int, c:Int)
		Return x + (c * GRID_SIZE) + ( (c + 1) * PADDING)
	End
	Method GridToY:Int(r:Int, c:Int)
		Return y + (r * GRID_SIZE) + ( (r + 1) * PADDING) + yOffset
	End
	Method CursorInScreen:Bool()
		If Game.Cursor.x() > x And Game.Cursor.x() < x + (cols * GRID_SIZE) + ( (cols + 1) * PADDING) And Game.Cursor.y() > y And Game.Cursor.y() < y + (rows * GRID_SIZE) + (rows + 1) * PADDING Then Return True
		Return False
	End
	Method isMouseOver:Bool(r:Int, c:Int)
		If (Game.Cursor.x() < GridToX(r, c) Or Game.Cursor.x() > GridToX(r, c) + GRID_SIZE) Return False
		If (Game.Cursor.y() < GridToY(r, c) Or Game.Cursor.y() > GridToY(r, c) + GRID_SIZE) Return False
		Return True
	End
End

Class EquipmentSlotMetaData
	Field slot:EquipmentSlot
	Field r:Int
	Field c:int
	Method New(slot:EquipmentSlot, r:Int, c:Int)
		Self.slot = slot
		Self.r = r
		Self.c = c
	End
End

Class EquipmentSlot Extends Slot
	Field item:Item
	Method New(x:Int, y:Int, type:Int, ship:Ship)
		Super.New(x, y, type)
		item = Null
		If type = Item.SYSTEM
			If ship.systems[x] <> Null
				item = New Item(ItemLoader.GetItem(ship.systems[x].id))
			End
		Else
			For Local c:ComponentInterface = EachIn ship.comp
				If c.getX() = x And c.getY() = y And c.getType() = type
					If type = Component.COMPUTER
						item = Computer(c).system
					Else If type = Component.HARDPOINT
						If Hardpoint(c).gun <> Null
							If ItemLoader.DoesItemExist(Hardpoint(c).gun.id)
								item = New Item(ItemLoader.GetItem(Hardpoint(c).gun.id))
							End
						End
					Else
						item = New Item(ItemLoader.GetItem(c.getId()), c.getPermDamage(), c.getQualityLevel())
						item.comp = c
						item.formatedName = c.getFormatedName()
					End
	
					Exit
				End
			Next
		End
	End
	
End