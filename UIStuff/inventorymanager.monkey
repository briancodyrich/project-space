Strict

Import space_game


Class InventoryManager
	Const GRID_SIZE:Int = 120
	Const OVER_TIME_POPUP:Int = 12
	Const PADDING:Int = 5
	Const POPUP_W:Int = 3
	Const POPUP_H:Int = 2
	Field items:Inventory
	Field over:Item
	Field x:Int
	Field y:Int
	Field rows:Int
	Field cols:Int
	Field overx:Int
	Field overy:Int
	Field lastx:Int
	Field lasty:Int
	Field overtime:Int = 0
	Field alpha:Float
	Field selected:bool
	Field kill:bool
	Field yoffset:Int
	
	Field vRows:int
	Field yOffset:Int
	Field nub:float = 0
	Field nubpercent:float
	Field nubBar:bool
	
	Field grab:GrabItem
	Field storeData:Economy
	
	Field pass:PassData
	Field filter:ItemFilter
	Field trash:TrashCan
	Field delta:DeltaItem
	
	Method New(x:int, y:Int, rows:Int, cols:Int, pass:PassData, items:Inventory, grab:GrabItem, storeData:Economy = Null, delta:DeltaItem = Null)
		filter = New ItemFilter()
		Self.pass = pass
		Self.storeData = storeData
		Self.x = x
		Self.y = y
		Self.rows = rows
		Self.cols = cols
		Self.items = items
		Self.grab = grab
		Self.delta = delta
		
		Local count:int = items.slots.Length() - (rows * cols)
		If (count > 0)
			If count Mod cols = 0
				vRows = count / cols
			Else
				vRows = (count / cols) + 1
			End
		End
	End
	
	Method AddTrash:Void(row:int, col:int)
		trash = New TrashCan(x, y, row, col, grab)
		If trash.c <= 0
			trash.cPad -= PADDING
		Else If trash.c >= cols
			trash.cPad += PADDING
		End
		If trash.r <= 0
			trash.rPad -= PADDING
		Else If trash.r >= rows
			trash.rPad += PADDING
		End
	End
	
	Method Render:Void()
	'	If grabItem <> Null then Print "type: " + grabItem.type
		SetColor(0, 0, 0)
		DrawRect(x, y, (cols * GRID_SIZE) + ( (cols + 1) * PADDING), (rows * GRID_SIZE) + ( (rows + 1) * PADDING))
		SetColor(255, 255, 255)
		DrawRectOutline(x, y, (cols * GRID_SIZE) + ( (cols + 1) * PADDING), (rows * GRID_SIZE) + ( (rows + 1) * PADDING))
		If vRows > 0
			DrawRectOutline(x + (cols * GRID_SIZE) + ( (cols + 1) * PADDING), y, 30, (rows * GRID_SIZE) + ( (rows + 1) * PADDING))
			DrawRectOutline(x + (cols * GRID_SIZE) + ( (cols + 1) * PADDING) + 1, y + nub + 1, 28, 28 * 3)
			ResetColorAlpha()
		End
		SetScaledScissor(x, y, 5 + (cols * GRID_SIZE) + ( (cols + 1) * PADDING), 5 + (rows * GRID_SIZE) + ( (rows + 1) * PADDING))
		Local count:Int = 0
		For Local i:Int = 0 Until rows + vRows
			For Local j:Int = 0 Until cols
				If count < items.slots.Length()
					DrawRectOutline(GridToX(i, j), GridToY(i, j), GRID_SIZE, GRID_SIZE)
					count += 1
				End
			Next
		Next
		Local r:Int = 0
		Local c:Int = 0
		
		For Local temp:Int = 0 Until items.slots.Length()
			If items.slots[temp] <> Null And items.slots[temp].count > 0
				DrawImage(items.slots[temp].icon, GridToX(r, c) + (GRID_SIZE / 2), GridToY(r, c) + (GRID_SIZE / 2))
				If items.slots[temp].comp <> Null
					Local color:int = items.slots[temp].comp.QualityLevelToColor(items.slots[temp].qualityLevel)
					SetColor(HexToRed(color), HexToGreen(color), HexToBlue(color))
					DrawRectOutline(GridToX(r, c), GridToY(r, c), GRID_SIZE, GRID_SIZE)
					SetColor(255, 0, 0)
					DrawRect(GridToX(r, c) + GRID_SIZE - 5, GridToY(r, c) + GRID_SIZE, 5, -Clamp(int(items.slots[temp].comp.getPermDamage() / 50.0 * GRID_SIZE), 0, GRID_SIZE - 1))
					ResetColorAlpha()
				End
				If items.slots[temp].max > 1
					Game.white_font.Draw(items.slots[temp].count + "/" + items.slots[temp].max, GridToX(r, c) + 5, GridToY(r, c) + 80)
				End
			End
		
			c += 1
			If c >= cols
				r += 1
				c = 0
			End
		Next
		
		If selected And over <> Null
			Local xPos:Int = GridToX(overy + 1, overx)
			Local yPos:Int = GridToY(overy + 1, overx)
			Local width:Int = GRID_SIZE * POPUP_W + ( (POPUP_W - 1) * PADDING)
			Local height:Int = GRID_SIZE * POPUP_H + ( (POPUP_H - 1) * PADDING)
			
			
			While (overx + POPUP_W > cols)
				overx -= 1
				xPos = GridToX(overy + 1, overx)
				yPos = GridToY(overy + 1, overx)
			Wend
			If yPos > ( (rows - POPUP_H) * GRID_SIZE) + y + ( (rows + 1) * PADDING)
				overy -= (1 + POPUP_H)
				xPos = GridToX(overy + 1, overx)
				yPos = GridToY(overy + 1, overx)
			End
			SetAlpha(alpha)
			DrawRectOutline(xPos, yPos, width, height)
			SetColor(0, 0, 0)
			DrawRect(xPos + 5, yPos + 5, width - 10, height - 10)
			SetColor(255, 255, 255)
			SetAlpha(alpha)
			Game.white_font.Draw(over.formatedName, xPos + width / 2, yPos + 10, 0.5)
			
			Local s:Stack<String>
			If storeData = Null
				s = over.GetDescr()
			Else
				s = over.GetDescr(, True, storeData)
			End
			For Local temp:Int = 0 Until s.Length()
				Game.white_font.Draw(s.Get(temp), xPos + 10, yPos + 50 + (temp * 30))
			Next
			ResetColorAlpha()
		End
		SetScaledScissor(0, 0, ScreenWidth, ScreenHeight)
		If trash <> Null
			trash.Render()
		End
	End
		
	Method Update:Void()
		If trash <> Null
			trash.Update()
		End
		If grab.hasPass And grab.isTarget(pass) And filter.canTouch(grab.item.type) 'get the pass
			Local num:Int = Min(grab.item.count, grab.item.max)
			For Local temp:Int = 0 Until items.slots.Length()
				If items.slots[temp] <> Null And items.slots[temp].id = grab.item.id
					Local room:Int = items.slots[temp].max - items.slots[temp].count
					Local move:Int = Min(room, num)
					items.slots[temp].count += move
					grab.item.count -= move
					num -= move
					If num = 0
						grab.passSuccess()
					End
				End
			Next
			If grab.success = False
				For Local temp:Int = 0 Until items.slots.Length()
					If items.slots[temp] = Null And grab.success = False
						If grab.storedata <> Null And grab.storedata.type = Storage.STORE
							items.slots[temp] = New Item(ItemLoader.GetItem(grab.item.id), grab.item.damage, grab.item.qualityLevel)
							items.slots[temp].count = num
							grab.item.count -= num
							grab.passSuccess()
						Else
							items.slots[temp] = grab.item
							grab.passSuccess()
						End
					End
				Next
				If grab.success = False And grab.storedata <> Null And grab.storedata.type = Storage.STORE
					grab.storedata.refund = num
				End
			End
		End
		If grab.hasPass And grab.success = False And grab.isHome(pass) 'handle a failure
			If grab.hasBackupTarget()
				grab.swapToBackup()
			Else
				grab.ClearGrab()
			End
		Else If grab.hasPass And grab.success And grab.isHome(pass) 'handle a success
			For Local temp:Int = 0 Until items.slots.Length()
				If grab.item <> Null And items.slots[temp] = grab.item
					items.slots[temp] = Null
					grab.ClearGrab()
				End
			Next
		End
		If CursorInScreen() And vRows > 0
			If Not Game.Keys.KeyDown(KEY_SHIFT)
				If Game.Cursor.WheelUp()
					Local num:Float = (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING) / float(vRows)
					nub -= num
					nub = Clamp(nub, 0.0, float(y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING))
					If nub = 0
						nubpercent = 0
					Else
						nubpercent = 1 + (float(nub - (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING))) / (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING)
					End
					yOffset = -nubpercent * ( (vRows) * GRID_SIZE + ( (vRows)) * PADDING)
				Else If Game.Cursor.WheelDown()
					Local num:Float = (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING) / float(vRows)
					nub += num
					nub = Clamp(nub, 0.0, float(y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING))
					If nub = 0
						nubpercent = 0
					Else
						nubpercent = 1 + (float(nub - (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING))) / (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING)
					End
					yOffset = -nubpercent * ( (vRows) * GRID_SIZE + ( (vRows)) * PADDING)
				End
			End
		End
		If Game.Cursor.Hit()
			If Game.Cursor.x() > x + (cols * GRID_SIZE) + ( (cols + 1) * PADDING) And Game.Cursor.x() < x + (cols * GRID_SIZE) + ( (cols + 1) * PADDING) + 30 And Game.Cursor.y() > y And Game.Cursor.y() < y + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING
				nubBar = True
			End
		End
		
		If Game.Cursor.Down And nubBar
			nub = Clamp(int(Game.Cursor.y()) -y - 42, 0, y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING)
			If nub = 0
				nubpercent = 0
			Else
				nubpercent = 1 + (float(nub - (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING))) / (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING)
			End
			yOffset = -nubpercent * ( (vRows) * GRID_SIZE + ( (vRows)) * PADDING)
		Else
			nubBar = False
		End
		If Game.Cursor.MiddleHit() And CursorInScreen() 'And grab.item = Null
			items.Sort()
			kill = True
			nub = 0
			yOffset = 0
		End
		If Not kill
			overx = -1
			overy = -1
		End
		Local found:Bool
		Local c:Int = 0
		Local r:Int = 0
		For Local temp:Int = 0 Until items.slots.Length()
			If isMouseOver(r, c) And CursorInScreen()
				If Game.Keys.KeyDown(KEY_SHIFT)
					If Game.Cursor.Hit() And grab.item = Null And items.slots[temp] <> Null
						If filter.canTouch(items.slots[temp].type)
							grab.Pass(items.slots[temp], pass)
						End
					Else If (Game.Cursor.RightHit() And grab.item = Null And items.slots[temp] <> Null) 'split pass
						'NOTHING HERE YET
					Else If Game.Cursor.WheelUp()
						If (items.slots[temp] <> Null And grab.item = Null) And filter.canTouch(items.slots[temp].type)
							If items.slots[temp].count = 1
								grab.Grab(items.slots[temp], pass)
								items.slots[temp] = Null
								kill = True
							Else
								grab.item = New Item(ItemLoader.GetItem(items.slots[temp].id))
								items.slots[temp].count -= 1
							End
						Else If items.slots[temp] <> Null And grab.item.count < grab.item.max And grab.item.id = items.slots[temp].id
							grab.item.count += 1
							items.slots[temp].count -= 1
							If items.slots[temp].count = 0
								items.slots[temp] = Null
								kill = True
							End
						End
					Else If Game.Cursor.WheelDown()
						If (items.slots[temp] = Null And grab.item <> Null) And filter.canTouch(grab.item.type)
							If grab.item.count = 1
								items.slots[temp] = grab.Get()
							Else
								items.slots[temp] = New Item(ItemLoader.GetItem(grab.item.id))
								grab.item.count -= 1
								If grab.item.count = 0
									grab.Get()
								End
							End
						Else If grab.item <> Null and items.slots[temp].count < items.slots[temp].max And grab.item.id = items.slots[temp].id
							grab.item.count -= 1
							items.slots[temp].count += 1
							If grab.item.count = 0
								grab.Get()
							End
						End
					End
				Else If (Game.Cursor.RightHit() And grab.item = Null And items.slots[temp] <> Null) And filter.canTouch(items.slots[temp].type)'split
					Local num:Int = Clamp(items.slots[temp].count, 1, Max(items.slots[temp].count / 2, 1))
					If num = items.slots[temp].count 'do normal stuff (Should only work when count = 1)
						grab.Grab(items.slots[temp], pass)
						items.slots[temp] = Null
						kill = True
					Else
						Local tItem:Item = New Item(ItemLoader.GetItem(items.slots[temp].id)) 'no damage level because only single items have damage
						tItem.count = num
						items.slots[temp].count -= num
						grab.Grab(tItem, pass)
					End
				Else If grab.item <> Null And Game.Cursor.RightHit() And filter.canTouch(grab.item.type)'Drop one
					If items.slots[temp] = Null
						If grab.item.count = 1
							items.slots[temp] = grab.Get()
						Else
							items.slots[temp] = New Item(ItemLoader.GetItem(grab.item.id)) 'no damage level because only single items have damage
							grab.item.count -= 1
						End
					Else If grab.item.id = items.slots[temp].id And items.slots[temp].count < items.slots[temp].max
						items.slots[temp].count += 1
						grab.item.count -= 1
						If grab.item.count = 0
							grab.Get() ' To clear it out
						End
					End
				Else If (Game.Cursor.Hit() Or grab.isValidDrop())
					If (items.slots[temp] <> Null And grab.item = Null) and filter.canTouch(items.slots[temp].type) 'grab an item yo
						grab.Grab(items.slots[temp], pass)
						items.slots[temp] = Null
						kill = True
					Else If (items.slots[temp] = Null And grab.item <> Null) and filter.canTouch(grab.item.type) 'drop an item
						items.slots[temp] = grab.Get()
					Else If (items.slots[temp] <> Null And grab.item <> Null And items.slots[temp].id = grab.item.id) And filter.canTouch(grab.item.type) 'give stuff
						If items.slots[temp].count < items.slots[temp].max
							Local room:Int = items.slots[temp].max - items.slots[temp].count
							Local move:Int = Min(room, grab.item.count)
							items.slots[temp].count += move
							grab.item.count -= move
							If grab.item.count = 0
								grab.Get() 'to clear it out
							End
						Else If grab.item <> Null And filter.canTouch(grab.item.type) And filter.canTouch(items.slots[temp].type)'swap those bad boys
							items.slots[temp] = grab.Swap(items.slots[temp])
						End
					Else If grab.item <> Null And filter.canTouch(grab.item.type) And filter.canTouch(items.slots[temp].type)'swap those bad boys
						items.slots[temp] = grab.Swap(items.slots[temp])
					End
				End
				If Not kill And grab.item = Null
					found = True
					If items.slots[temp] <> Null
						overtime += 1
					End
					If overtime >= OVER_TIME_POPUP
						overtime = OVER_TIME_POPUP
						alpha += 0.1
						If alpha >= 1 Then alpha = 1
						If selected = False
							lastx = c
							lasty = r
							selected = True
							over = items.slots[temp]
							If delta <> Null
								delta.new_item = over
								delta.grab_from = DeltaItem.INVENTORY
							End
						End
						If lastx = c And lasty = r
							overx = c
							overy = r
						Else
							overy = lasty
							overx = lastx
							kill = True
						End
					End
				End
			End
			c += 1
			If c >= cols
				r += 1
				c = 0
			End
		Next
		
		If Not found
			overy = lasty
			overx = lastx
			kill = True
		End
		If kill
			If delta <> Null And delta.grab_from = DeltaItem.INVENTORY
				delta.new_item = Null
				delta.grab_from = DeltaItem.NOTHING
			End
			alpha -= 0.1
			If alpha <= 0
				alpha = 0
				overtime = 0
				selected = False
				over = Null
				kill = False
			End
		End
		
	End
	
	Method GridToX:Int(r:Int, c:Int)
		Return x + (c * GRID_SIZE) + ( (c + 1) * PADDING)
	End
	Method GridToY:Int(r:Int, c:Int)
		Return y + (r * GRID_SIZE) + ( (r + 1) * PADDING) + yOffset
	End
	
	Method CursorInScreen:Bool()
		If Game.Cursor.x() > x And Game.Cursor.x() < x + (cols * GRID_SIZE) + ( (cols + 1) * PADDING) And Game.Cursor.y() > y And Game.Cursor.y() < y + (rows * GRID_SIZE) + (rows + 1) * PADDING Then Return True
		Return False
	End
	Method isMouseOver:Bool(r:Int, c:Int)
		If (Game.Cursor.x() < GridToX(r, c) Or Game.Cursor.x() > GridToX(r, c) + GRID_SIZE) Return False
		If (Game.Cursor.y() < GridToY(r, c) Or Game.Cursor.y() > GridToY(r, c) + GRID_SIZE) Return False
		Return True
	End
End

Class TrashCan
	Const DUMP_TIME:Int = 180
	Field timer:int
	Field rPad:Int
	Field cPad:Int
	Field x:Int
	Field y:int
	Field r:Int
	Field c:int
	Field pd:PassData
	Field grab:GrabItem
	Field item:Item
	Method New(x:Int, y:Int, r:Int, c:Int, grab:GrabItem)
		Self.x = x
		Self.y = y
		Self.r = r
		Self.c = c
		Self.grab = grab
		pd = New PassData(PassData.INVENTORY, PassData.NO_TARGET)
	End
	Method Render:Void()
		SetColor(0, 0, 0)
		DrawRect(GridToX(), GridToY(), InventoryManager.GRID_SIZE, InventoryManager.GRID_SIZE)
		SetColor(255, 255, 255)
		DrawRectOutline(GridToX(), GridToY(), InventoryManager.GRID_SIZE, InventoryManager.GRID_SIZE)
		If item <> Null
			DrawImage(item.icon, GridToX() +5 + (InventoryManager.GRID_SIZE / 2), GridToY() +5 + (InventoryManager.GRID_SIZE / 2))
			If item.comp <> Null
				Local color:int = item.comp.QualityLevelToColor(item.qualityLevel)
				SetColor(HexToRed(color), HexToGreen(color), HexToBlue(color))
				DrawRectOutline(GridToX(), GridToY(), InventoryManager.GRID_SIZE, InventoryManager.GRID_SIZE)
				SetColor(255, 0, 0)
				DrawRect(GridToX() +InventoryManager.GRID_SIZE - 5, GridToY() +InventoryManager.GRID_SIZE, 5, -Clamp(int(item.comp.getPermDamage() / 50.0 * InventoryManager.GRID_SIZE), 0, InventoryManager.GRID_SIZE - 1))
				ResetColorAlpha()
			End
			SetColor(255, 0, 0)
			DrawRect(GridToX() -5, GridToY() +InventoryManager.GRID_SIZE, 5, -Clamp(int(timer / float(DUMP_TIME) * InventoryManager.GRID_SIZE), 0, InventoryManager.GRID_SIZE - 1))
			ResetColorAlpha()
			If item.max > 1
				Game.white_font.Draw(item.count + "/" + item.max, GridToX() +5, GridToY() +80)
			End
		End
	End
	Method Update:Void()
		If item = Null
			timer = 0
		Else
			timer += 1
			If timer > DUMP_TIME
				item = Null
				timer = 0
			End
		End
		If isMouseOver()
			If Game.Cursor.Hit() Or Game.Cursor.RightHit()
				If grab.item <> Null
					item = grab.item
					grab.ClearGrab()
					timer = 0
				Else If item <> Null
					grab.Grab(item, pd)
					timer = 0
					item = Null
				End
			End
		End
	End
	Method isMouseOver:Bool()
		If (Game.Cursor.x() < GridToX() Or Game.Cursor.x() > GridToX() +InventoryManager.GRID_SIZE) Return False
		If (Game.Cursor.y() < GridToY() Or Game.Cursor.y() > GridToY() +InventoryManager.GRID_SIZE) Return False
		Return True
	End
	Method GridToX:Int()
		Return x + (c * InventoryManager.GRID_SIZE) + ( (c + 1) * InventoryManager.PADDING) + cPad
	End
	Method GridToY:Int()
		Return y + (r * InventoryManager.GRID_SIZE) + ( (r + 1) * InventoryManager.PADDING) + rPad
	End
End

Class GrabItem
	Field item:Item
	Field data:PassData
	Field storedata:Storage
	
	Field hasPass:bool
	Field success:bool
	
	Field holding:bool
	Field t:Int
	
	Method Update:Void()
		If Game.Cursor.Hit()
			holding = True
			t = Millisecs()
		End
		If holding And Not Game.Cursor.Down()
			holding = False
		End
	End
	
	Method isValidDrop:Bool()
		If holding = False Then Return False
		If Game.Cursor.Down() Then Return False
		If Millisecs() -t < 200 Then Return False
		Return True
	End
	
	Method Grab:Void(i:Item, passData:PassData)
		item = i
		data = New PassData(passData)
	End
	
	Method StoreGrab:Void(i:Item, passData:PassData, storage:Storage)
		Self.storedata = storage
		Grab(i, passData)
	End
	
	Method passSuccess:Void()
		success = True
	End
	
	Method hasBackupTarget:Bool()
		If data.backup <> PassData.NO_TARGET Then Return True
		Return False
	End
	
	Method swapToBackup:Void()
		data.target = data.backup
		data.backup = PassData.NO_TARGET
	End
	
	Method ClearGrab:Void()
		success = False
		hasPass = False
		Clear()
	End
	Method Pass:Void(i:Item, data:PassData)
		item = i
		Self.data = New PassData(data)
		hasPass = True
	End
	Method StorePass:Void(i:Item, passData:PassData, storage:Storage)
		Self.storedata = storage
		Pass(i, passData)
	End
	#rem
	Method Pass:Void(i:Item)
		Clear()
		item = i
		hasPass = True
	End
	#end
	Method Clear:Void()
		item = Null
		storedata = Null
		data = Null
	End
	
	Method isTarget:Bool(data:PassData)
		If Self.data = Null Then Return False
		If Self.data.target = data.home Then Return True
		Return False
	End
	
	Method isHome:Bool(data:PassData)
		If Self.data = Null Then Return False
		If Self.data.home = data.home Then Return True
		Return false
	End
	
	Method Get:Item()
		Local r:Item = item
		Clear()
		Return r
	End
	Method Swap:Item(i:Item)
		Local swap:Item
		swap = item
		item = Null
		item = i
		Return swap
	End
	Method Render:Void()
		If item <> Null And hasPass = False
			DrawImage(item.icon, Game.Cursor.x(), Game.Cursor.y())
			If item.max > 1
				Game.white_font.Draw(item.count + "/" + item.max, Game.Cursor.x() -60, Game.Cursor.y() +15)
			End
		End
	End
End

Class ItemFilter
	Field type:Int
	Field list:Int[]
	Field blackList:bool
	Method New()
		list =[]
		blackList = True
	End
	Method New(list:Int[], isWhiteList:Bool)
		If isWhiteList
			WhiteList(list)
		Else
			BlackList(list)
		End
	End
	Method WhiteList:Void(list:Int[])
		Self.list = list
		blackList = False
	End
	Method BlackList:Void(list:Int[])
		Self.list = list
		blackList = True
	End
	Method canTouch:Bool(code:Int)
		If list.Length() > 0
			For Local temp:Int = 0 Until list.Length()
				If list[temp] = code Then Return not blackList
			Next
		End
		Return blackList
	End
End

Class PassData
	Const NO_TARGET:Int = 0
	Const EQUIPMENT:Int = 1
	Const INVENTORY:Int = 2
	Const STORE:Int = 3
	Field home:Int
	Field target:Int
	Field backup:int
	Method New(home:Int, target:Int, backup:Int = 0)
		Self.home = home
		Self.target = target
		Self.backup = backup
	End
	Method New(data:PassData)
		home = data.home
		target = data.target
		backup = data.backup
	End
End