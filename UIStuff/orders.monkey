
Strict
Import space_game


Class Orders
	Const TYPE_PILOT:Int = 1
	Const TYPE_SHIELD:Int = 2
	Const TYPE_WEAPON:Int = 3
	Const DUMP_OXYGEN:Int = 4
	Const AUTO_PILOT:Int = 5
	Const WIDTH:int = 150
	Const HEIGHT:Int = 60
	Field type:Int
	Field x:Int
	Field y:int
	Field ship:Ship
	Method New(x:Int, y:Int, type:Int, ship:Ship)
		Self.x = x
		Self.y = y
		Self.type = type
		Self.ship = ship
	End
	Method Render:Void()
		Local status:bool
		Select type
			Case TYPE_PILOT
				status = ship.pilot_ai_on
			Case TYPE_SHIELD
				status = ship.shield_on
			Case TYPE_WEAPON
				status = ship.gun_ai_on
			Case DUMP_OXYGEN
				status = ship.dump_oxygen
			Case AUTO_PILOT
				status = ship.aiControlled
		End
		If status
			SetColor(0, 255, 0)
		Else
			If type = DUMP_OXYGEN Or type = AUTO_PILOT
				SetColor(255, 255, 0)
			Else
				SetColor(255, 0, 0)
			End
		End
		DrawRectOutline(x, y, WIDTH, HEIGHT)
		Select type
			Case TYPE_PILOT
				Game.white_font.Draw("Piloting", x + (WIDTH / 2), y + (HEIGHT / 6), 0.5)
			Case TYPE_SHIELD
				Game.white_font.Draw("Shields", x + (WIDTH / 2), y + (HEIGHT / 6), 0.5)
			Case TYPE_WEAPON
				Game.white_font.Draw("Weapons", x + (WIDTH / 2), y + (HEIGHT / 6), 0.5)
			Case DUMP_OXYGEN
				Game.white_font.Draw("Dump O2", x + (WIDTH / 2), y + (HEIGHT / 6), 0.5)
			Case AUTO_PILOT
				Game.white_font.Draw("Autopilot", x + (WIDTH / 2), y + (HEIGHT / 6), 0.5)
		End
		ResetColorAlpha()
	End
	Method Update:Void()
		If Game.Cursor.Hit() And isMouseOver()
			Select type
				Case TYPE_PILOT
					ship.pilot_ai_on = Not ship.pilot_ai_on
				Case TYPE_SHIELD
					ship.shield_on = Not ship.shield_on
				Case TYPE_WEAPON
					ship.gun_ai_on = Not ship.gun_ai_on
				Case DUMP_OXYGEN
					ship.dump_oxygen = Not ship.dump_oxygen
				Case AUTO_PILOT
					If ship.aiControlled
						ship.aiControlled = False
					Else
						ship.AiStart()
					End
			End
		End
	End
	Method isMouseOver:Bool()
		If Game.Cursor.x() > x And Game.Cursor.x() < x + WIDTH And Game.Cursor.y() > y And Game.Cursor.y() < y + HEIGHT Then Return True
		Return False
	End
End