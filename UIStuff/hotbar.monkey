Strict

Import space_game

Class HotBar
	Const OFF:Int = 0
	Const KEY:Int = 2
	Const AUTO:Int = 1
	
	Const STANDARD_W:Int = 150
	Const STANDARD_H:Int = 50
	Field x:int
	Field y:Int
	Field w:Int
	Field h:Int
	Field hotKey:int
	Field mode:int = OFF
	Field target_img:Image
	Field ship:Ship
	Field system:Hardpoint
	Field special:ModSpecial
	Field targetx:Int = -100
	Field targety:Int = -100
	
	
	Method New(x:Int, y:Int, w:Int, h:Int, target_img:Image, hotKEY:int, system:Hardpoint, ship:Ship)
		Self.x = x
		Self.y = y
		Self.w = w
		Self.h = h
		Self.target_img = target_img
		Self.hotKey = hotKEY
		Self.ship = ship
		Self.system = system
		system.setState(False)
	End
	Method New(x:Int, y:Int, w:Int, h:Int, hotKEY:int, special:ModSpecial, ship:Ship)
		Self.x = x
		Self.y = y
		Self.w = w
		Self.h = h
		Self.hotKey = hotKEY
		Self.special = special
		Self.ship = ship
	End
	Method isMouseOver:bool()
		If Game.Cursor.x() > x And Game.Cursor.x() < x + w And Game.Cursor.y() > y And Game.Cursor.y() < y + h Then Return True
		Return False
	End
	Method Update:Void()
		If ship.targetShip = Null Then Return
		If ship.targetShip.isDead()
			mode = 0
			If system <> Null
				system.setState(False)
			End
			targetx = -100
			targety = -100
		End
		
		'gun stuff
		If system <> Null And mode <> OFF
			If system.isReady()
				Local fudgeX:Int = targetx '+ Rnd(-15, 15)
				Local fudgeY:Int = targety '+ Rnd(-15, 15)
				For Local x:Int = 0 Until Ship.MAX_X
					For Local y:Int = 0 Until Ship.MAX_Y
						If ship.targetShip.pipes[x * Ship.MAX_X + y].isPointOver(x, y, ship.targetShip.x_offset, ship.targetShip.y_offset, ship.targetShip.mirrored, fudgeX, fudgeY) And ship.targetShip.pipes[x * Ship.MAX_X + y].isImpactPoint()
							Select system.gun.boltData.GetString("type")
								Case "MISSILE"
									SoundEffect.Play(SoundEffect.MISSILE)
								Case "BEAM"
									SoundEffect.Play(SoundEffect.BEAM)
								Case "CANNON"
									SoundEffect.Play(SoundEffect.CANNON)
								Case "THERMITE"
									SoundEffect.Play(SoundEffect.CANNON)
								Default
									SoundEffect.Play(SoundEffect.LASER)
							End
							If Rnd(system.gun.boltData.GetInt("accuracy", 100)) > ship.targetShip.GetDodge(True) - ship.GetAccuracyBonus(system.x, system.y)
								ship.targetShip.AddBolts(New Bolt(x, y, system.gun.boltData, ship.targetShip.areShieldsActive()))
								Local box:BoxShipData = New BoxShipData(x * 30, y * 30, ship.targetShip.x_offset, ship.targetShip.y_offset, ship.targetShip.mirrored, Bolt.idGlobal)
								Game.OverParticle.Add( ( (system.getX() +2) * 30) + 15, (system.getY() * 30) + 15, ship.x_offset, ship.y_offset, ship.mirrored, PhotonTypes.GetType(system.gun.boltData.GetString("type")), box)
							Else
								Local box:BoxShipData = New BoxShipData(x * 30, y * 30, ship.targetShip.x_offset, ship.targetShip.y_offset, ship.targetShip.mirrored)
								Game.OverParticle.Add( ( (system.getX() +2) * 30) + 15, (system.getY() * 30) + 15, ship.x_offset, ship.y_offset, ship.mirrored, PhotonTypes.GetType(system.gun.boltData.GetString("type")), box)
							End
							Select system.FireGun()
								Case Weapon.MISSILE
								ship.items.RemoveItem(Item.MISSILE, 1)
								Case Weapon.CANNON
								ship.items.RemoveItem(Item.CANNON_BALL, 1)
								Case Weapon.THERMITE
								ship.items.RemoveItem(Item.THERMITE, 1)
							End
						End
					Next
				Next
				If mode = KEY
					targetx = -100
					targety = -100
				End
			End
		End
		
		
		'combat effect stuff
		If special <> Null
			
		End
		If Game.Cursor.Hit()
			If isMouseOver()
				If system <> Null And system.gun <> Null
					mode += 1
					If mode = 3 Then mode = 0
					If mode = 0 Then
						system.setState(False)
						targetx = -100
						targety = -100
				    Else
						system.setState(True)
					End
				End
				If special <> Null
					Select special.Activate()
						Case ModSpecial.BURN_FUEL
						ship.items.RemoveItem("Fuel", 1)
					End
				End
			End
		End
		If isKey()
			If mode = OFF And system <> Null
				For Local x:Int = 0 Until Ship.MAX_X
					For Local y:Int = 0 Until Ship.MAX_Y
						If ship.targetShip.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, ship.targetShip.x_offset, ship.targetShip.y_offset, ship.targetShip.mirrored) And ship.targetShip.pipes[x * Ship.MAX_X + y].isImpactPoint()
							targetx = Game.Cursor.x()
							targety = Game.Cursor.y()
							mode = AUTO
							system.setState(True)
						End
					Next
				Next	
			Else If system <> Null 'gun code
				For Local x:Int = 0 Until Ship.MAX_X
					For Local y:Int = 0 Until Ship.MAX_Y
						If ship.targetShip.pipes[x * Ship.MAX_X + y].isMouseOver(x, y, ship.targetShip.x_offset, ship.targetShip.y_offset, ship.targetShip.mirrored) And ship.targetShip.pipes[x * Ship.MAX_X + y].isImpactPoint()
							targetx = Game.Cursor.x()
							targety = Game.Cursor.y()
						End
					Next
				Next
			Else If special <> Null
				Select special.Activate()
					Case ModSpecial.BURN_FUEL
						ship.items.RemoveItem("Fuel", 1)
				End
			End
		End
	End
	Method Render:Void()
		If ship.targetShip = Null or ship.targetShip.isDead() Then Return
		If system <> Null 'gun code
			If system.gun <> Null
				SetColor(0, 255, 0)
				Local percent:Float = float(system.gun.power) / system.gun.cycle_time
				DrawRect(x + 1, y + h - 5, (w - 1) * percent, 5)
				SetColor(255, 255, 255)
			End
			If system.isActive() = False Then SetColor(255, 0, 0)
			DrawRectOutline(x, y, w, h)
			ResetColorAlpha()
			If mode = OFF Then Game.white_font.Draw("X", x + w - 15, y + h / 6, 0.5)
			If mode = KEY Then Game.white_font.Draw("M", x + w - 15, y + h / 6, 0.5)
			If mode = AUTO Then Game.white_font.Draw("A", x + w - 15, y + h / 6, 0.5)
			If system <> Null And system.gun <> Null
				Game.white_font.Resize(0.3)
				Game.white_font.Draw(system.gun.hotbar_name, x + 10, y + h / 3)
				Game.white_font.Resize(0.5)
				If mode <> 0
					DrawImage(target_img, targetx, targety)
					ResetColorAlpha()
				End
			End
		Else If special <> Null
			SetColor(0, 255, 0)
			Local percent:Float = special.cooldownCounter / float(special.cooldown + (special.used * special.stress))
			DrawRect(x + 1, y + h - 5, (w - 1) * percent, 5)
			If special.canActivate()
				SetColor(255, 255, 255)
			Else
				SetColor(255, 0, 0)
			End
			If special.isActive
				Game.white_font.Draw("A", x + w - 15, y + h / 6, 0.5)
			Else If special.cooldownCounter = (special.cooldown + (special.used * special.stress))
				Game.white_font.Draw("R", x + w - 15, y + h / 6, 0.5)
			Else
				Game.white_font.Draw("X", x + w - 15, y + h / 6, 0.5)
			End
			Game.white_font.Resize(0.3)
			Game.white_font.Draw(special.hotbar_name, x + 10, y + h / 3)
			Game.white_font.Resize(0.5)
			
			DrawRectOutline(x, y, w, h)
			ResetColorAlpha()
		End
	End
	
	Method isKey:Bool()
		If Game.Keys.KeyHit(hotKey) Then Return True
		Return False
	End
End
