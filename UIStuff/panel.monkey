Strict

Import space_game


Class ShipPanel
	Const NORMAL_OFFSET:Int = 300
	Const COMBAT_OFFSET:Int = 500
	Const MAP_MODE:Int = 1
	Const COMBAT_MODE:Int = 2
	Const POWER_GEN:Int = 1
	Field WEAPONS:Int[]
	Field SPECIAL:Int[]
	Field enginePower:PowerBar
	Field reactorPower:PowerBar
	Field shieldPower:PowerBar
	Field pilotBtn:Orders
	Field shieldBtn:Orders
	Field gunsBtn:Orders
	Field topOxygenBtn:Orders
	Field topAutoPilotBtn:Orders
	Field hotBarStack:Stack<HotBar>
	Field asteroid:Asteroid
	Field ship:Ship
	Field important:ImportantData
	Field mapSettings:OrderState
	Field combatSettings:OrderState
	Field stats:ShipStatViewer
	Field mode:int
	Method New()
		ship = Player.ship
		WEAPONS =[KEY_A, KEY_S, KEY_D, KEY_F, KEY_I]
		SPECIAL =[KEY_1, KEY_2, KEY_3, KEY_4, KEY_5]
		enginePower = New PowerBar(NORMAL_OFFSET, 800, PowerBar.TYPE_ENGINE, ship.enginePower, ship)
		reactorPower = New PowerBar(NORMAL_OFFSET, 900, PowerBar.TYPE_REACTOR, ship.reactorPower, ship)
		shieldPower = New PowerBar(NORMAL_OFFSET, 1000, PowerBar.TYPE_SHIELD, ship.shieldPower, ship)
		pilotBtn = New Orders(50, 800, Orders.TYPE_PILOT, ship)
		shieldBtn = New Orders(50, 900, Orders.TYPE_SHIELD, ship)
		gunsBtn = New Orders(50, 1000, Orders.TYPE_WEAPON, ship)
		topOxygenBtn = New Orders(10, 10, Orders.DUMP_OXYGEN, ship)
		topAutoPilotBtn = New Orders(10 + 160, 10, Orders.AUTO_PILOT, ship)
		mapSettings = New OrderState(OrderState.DEFAULT_MAP)
		combatSettings = New OrderState(OrderState.DEFAULT_COMBAT)
		important = New ImportantData()
		stats = New ShipStatViewer(ship)
		mode = MAP_MODE
		mapSettings.SetState(ship)
		UpdateHotbar()
	End
	
	Method OnSave:JsonObject()
		Local data:JsonObject = New JsonObject()
		data.Set("map", mapSettings.OnSave())
		data.Set("combat", combatSettings.OnSave())
		Return data
	End
	
	Method OnLoad:Void(data:JsonObject)
		mapSettings.OnLoad(JsonObject(data.Get("map")))
		combatSettings.OnLoad(JsonObject(data.Get("combat")))
		mapSettings.SetState(ship)
	End
	
	Method SetMode:Void(mode:int)
		If Self.mode <> mode
			Self.mode = mode
			If mode = MAP_MODE
				combatSettings.GetState(ship)
				mapSettings.SetState(ship)
				enginePower.x = NORMAL_OFFSET
				reactorPower.x = NORMAL_OFFSET
				shieldPower.x = NORMAL_OFFSET
			Else If mode = COMBAT_MODE
				mapSettings.GetState(ship)
				combatSettings.SetState(ship)
				enginePower.x = COMBAT_OFFSET
				reactorPower.x = COMBAT_OFFSET
				shieldPower.x = COMBAT_OFFSET
			End
		End
	End 
	
	Method UpdateHotbar:Void()
		hotBarStack = New Stack<HotBar>
		Local key:Int = 0
		Local spacing:Int = 100
		For Local temp:Int = 0 Until Player.ship.comp.Length
			If ship.comp.Get(temp).getType() = Component.HARDPOINT
				hotBarStack.Push(New HotBar(spacing, 725, HotBar.STANDARD_W, HotBar.STANDARD_H, LoadImage("ui/target.png",, Image.MidHandle), WEAPONS[key], Hardpoint(ship.comp.Get(temp).getSelf()), ship))
				key += 1
				spacing += HotBar.STANDARD_W + 10
			End
		Next
		spacing = 100
		key = 0
		For Local temp:Int = 0 Until ship.modSpecial.Length()
			hotBarStack.Push(New HotBar(spacing, 670, HotBar.STANDARD_W, HotBar.STANDARD_H, SPECIAL[key], Player.ship.modSpecial.Get(temp), ship))
			key += 1
			spacing += HotBar.STANDARD_W + 10
		Next
	End
	
	Method BackgroundUpdate:Void()
		Game.OverParticle.Update()
		Game.UnderParticle.Update()
	End
	
	Method Update:bool(important_only:Bool = False)
		important.Update()
		If important_only Then Return True
		If Player.ship.isNull() Then Return True
		If Player.clock.pay_crew
			Player.clock.pay_crew = False
			Player.accounting.DailyPayout(1,1)
		End
		For Local temp:Int = 0 Until hotBarStack.Length()
			hotBarStack.Get(temp).Update()
		End
		If Player.paused = True Or Game.CurrentScreenNumber <> Screens.TestGame
			If enginePower.Update()
				ship.Update(True)
				If ship.targetShip <> Null
					ship.targetShip.Update(True)
				End
				Return True
			End
			
			If reactorPower.Update()
				ship.Update(True)
				If ship.targetShip <> Null
					ship.targetShip.Update(True)
				End
				Return True
			End
		
			If shieldPower.Update()
				ship.Update(True)
				If ship.targetShip <> Null
					ship.targetShip.Update(True)
				End
				Return True
			End
		Else
			enginePower.Update()
			reactorPower.Update()
			shieldPower.Update()
		End
		If mode = COMBAT_MODE
			pilotBtn.Update()
			shieldBtn.Update()
			gunsBtn.Update()
			topOxygenBtn.Update()
			topAutoPilotBtn.Update()
		End
		Return false
	End

	Method Render:Void(drawExtendedStats:Bool = False, important_only:Bool = False)
		important.Render(mode)
		If Not important_only
			If drawExtendedStats
				stats.Render()
			End
			
			If Player.ship.isNull() Then Return
			enginePower.Render(mode)
			reactorPower.Render(mode)
			shieldPower.Render(mode)
			If mode = COMBAT_MODE
				pilotBtn.Render()
				shieldBtn.Render()
				gunsBtn.Render()
				topOxygenBtn.Render()
				topAutoPilotBtn.Render()
			End
			For Local temp:Int = 0 Until hotBarStack.Length()
				hotBarStack.Get(temp).Render()
			Next
		End
	End
	
End

Class ImportantData
	Const FOOD_WARNING:Int = 20
	Const FUEL_WARNING:Int = 20
	Field credits:int
	Field showCredits:Int
	Field Fuel:int
	Field showFuel:Int
	Field Food:int
	Field showFood:int
	
	Method Update:Void()
		credits = Player.getCredits()
		Fuel = Player.ship.GetRange()
		Food = Player.ship.GetFood()
		showCredits = credits
		If showCredits <> credits
			If showCredits > credits - 50 And showCredits < credits + 50
				showCredits = credits
			Else If showCredits < credits
				showCredits += 51
			Else If showCredits > credits
				showCredits -= 51
			End
		End
		If Food <> - 1
			If showFood > Food - 3 And showFood < Food + 3
				showFood = Food
			Else If showFood < Food
				showFood += 3
			Else If showFood > Food
				showFood -= 3
			End
		End
		If showFuel < Fuel
			showFuel += 1
		Else If showFuel > Fuel
			showFuel -= 1
		End
	End
	
	Method Render:Void(mode:int)
		If mode = ShipPanel.COMBAT_MODE
			PrettyStats(1700, 1030, 100, "DOSH: ", FormatCredits(showCredits))
		Else If mode = ShipPanel.MAP_MODE
			If showFuel <= FUEL_WARNING
				PrettyStats(1700, 970, 100, "FUEL:", "#ff0000" + showFuel)
			Else
				PrettyStats(1700, 970, 100, "FUEL:", showFuel)
			End
			If Food = -1
				PrettyStats(1700, 1000, 100, "FOOD: ", "INF")
			Else
				If showFood <= FOOD_WARNING
					PrettyStats(1700, 1000, 100, "FOOD:", "#ff0000" + showFood)
				Else
					PrettyStats(1700, 1000, 100, "FOOD:", showFood)
				End
			End
			PrettyStats(1700, 1030, 100, "DOSH: ", FormatCredits(showCredits))
		End
		
	End
	
End

Class OrderState
	Const DEFAULT_COMBAT:Int = 1
	Const DEFAULT_MAP:Int = 2
	
	'bars
	Field ep:Int
	Field rp:Int
	Field sp:int
	
	Field pilot:Bool
	Field shield:Bool
	Field gun:Bool
	Field oxygen:bool
	Field autopilot:bool
	
	Method New(state:Int)
		If state = DEFAULT_COMBAT
			ep = Ship.NORMAL
			rp = Ship.NORMAL
			sp = Ship.LOW
			pilot = True
			shield = True
			gun = True
			oxygen = False
			autopilot = False
		
		Else If state = DEFAULT_MAP
			ep = Ship.LOW
			rp = Ship.OVERLOAD
			sp = Ship.LOW
			pilot = False
			shield = False
			gun = False
			oxygen = False
			autopilot = False
		End
	End
	
	Method OnSave:JsonObject()
		Local data:JsonObject = New JsonObject()
		data.SetInt("ep", ep)
		data.SetInt("rp", rp)
		data.SetInt("sp", sp)
		data.SetBool("p", pilot)
		data.SetBool("s", shield)
		data.SetBool("g", gun)
		data.SetBool("o2", oxygen)
		data.SetBool("auto", autopilot)
		Return data
	End
	
	Method OnLoad:Void(data:JsonObject)
		ep = data.GetInt("ep")
		rp = data.GetInt("rp")
		sp = data.GetInt("sp")
		pilot = data.GetBool("p")
		shield = data.GetBool("s")
		gun = data.GetBool("g")
		oxygen = data.GetBool("o2")
		autopilot = data.GetBool("auto")
	End
	
	Method SetState:Void(ship:Ship)
		ship.enginePower = ep
		ship.reactorPower = rp
		ship.shieldPower = sp
		ship.pilot_ai_on = pilot
		ship.shield_on = shield
		ship.gun_ai_on = gun
		ship.dump_oxygen = oxygen
		ship.aiControlled = autopilot
	End
	
	Method GetState:Void(ship:Ship)
		ep = ship.enginePower
		rp = ship.reactorPower
		sp = ship.shieldPower
		pilot = ship.pilot_ai_on
		shield = ship.shield_on
		gun = ship.gun_ai_on
		oxygen = ship.dump_oxygen
		autopilot = ship.aiControlled
	End
End


