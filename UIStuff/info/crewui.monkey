Strict

Import space_game

Class CrewStatusSmall
	Const PADDING:int = 10
	Const DEPLOY_TIME:Int = 25
	Field x:Int
	Field y:Int
	Field h:Int
	Field w:int
	Field crew:Stack<CrewIcon>
	Method New(x:Int, y:int, h:Int, w:int, ship_crew:Stack<Crew>)
		Self.x = x
		Self.y = y
		Self.h = h
		Self.w = w
		crew = New Stack<CrewIcon>()
		For Local temp:Int = 0 Until ship_crew.Length()
			crew.Push(New CrewIcon(x, y + h + CrewIcon.HEIGHT, w, getMin(temp), ship_crew.Get(temp), temp * DEPLOY_TIME))
		Next
	End
	Method Update:Void()
		For Local temp:Int = 0 Until crew.Length()
			crew.Get(temp).Update(getMin(temp))
		Next
		
		'
	End
	Method Render:Void()
		'DrawRectOutline(x, y, w, h)
		SetScaledScissor(x - 5, y - 5, w + 10, h + 10)
		Game.white_font.Resize(0.25)
		For Local temp:Int = 0 Until crew.Length()
			If crew.Get(temp).Render()
				crew.Remove(temp)
				temp -= 1
			End
		Next
		SetScaledScissor(0, 0, ScreenWidth, ScreenHeight)
		Game.white_font.Resize(0.5)
	End
	Method getMin:Int(index:Int)
		Return y + (index * CrewIcon.HEIGHT) + (PADDING * (index + 1))
	End
End

Class CrewIcon
	Const HEIGHT:Int = 50
	Const SPEED:Int = 80
	Const BAR_H:Int = 10
	Const BAR_PAD:Int = 4
	Const ALPHA_DECAY:Float = 0.005
	Field crew:Crew
	Field x:Int
	Field y:Int
	Field w:int
	Field time:int
	Field goal:Int
	Field start:int
	Field deploy:int
	Field last_health:int
	Field color:float
	Field alpha:Float = 1.0
	Method New(x:Int, y:Int, w:int, goal:int, crew:Crew, deploy:int)
		Self.x = x
		Self.y = y
		Self.w = w
		Self.goal = goal
		Self.start = y
		Self.crew = crew
		Self.deploy = deploy
		Self.color = 255.0
		last_health = crew.health
	End
	Method Update:Void(min:int)
		If deploy > 0
			deploy -= 1
			Return
		End
		If crew.health < last_health
			color = 0
		End
		last_health = crew.health
		If min <> goal
			start = y
			time = 0
			goal = min
		End
		If time <= SPEED
			y = easeOutQuad(time, start, goal - start, SPEED)
			time += 1
		End
	End
	Method Render:bool()
		Local aP:Float = crew.oxygen / 100.0
		Local hP:Float = crew.health / float(crew.GetMaxHp())
		If crew.isDead()
			aP = 0
			hP = 0
			alpha = Clamp(alpha - ALPHA_DECAY, 0.0, 1.0)
		End
		Game.white_font.Draw(crew.first_name, x + (w / 2), y + 5, 0.5)
		Local i:Incrementer = New Incrementer(y + HEIGHT - BAR_H, - (BAR_H + BAR_PAD))
		SetAlpha(alpha)
		SetColor(0, 0, 255)
		DrawRect(x, i.getNext(), w * aP, BAR_H)
		SetColor(255, 255, 255)
		DrawRectOutline(x, i.getCurrent(), w, BAR_H)
		
		SetColor(255, 0, 0)
		DrawRect(x, i.getNext(), w * hP, BAR_H)
		SetColor(255, 255, 255)
		DrawRectOutline(x, i.getCurrent(), w, BAR_H)
		
		SetColor(255, color, color)
		DrawRectOutline(x, y, w, HEIGHT)
		ResetColorAlpha()
		color = Clamp(color + 0.5, 0.0, 255.0)
		If alpha <= 0 Then Return True
		Return False
	End
End

Class CrewInteractionPanel
	
	Method Update:int(x:Int, y:Int)
		
	
		Return 0
	End
	Method Render:Void()
		
	End
End

Class CrewStatusLarge
	Const HIRE_CREW:Int = 1
	Const FIRE_CREW:Int = 2
	Const SEND_ON_LEAVE:Int = 3
	Const ICONS_LOCATION:String = "crew_icons/small/"
	Const WIDTH:Int = 260
	Const HEIGHT:Int = 460
	Field member:Crew
	Field x:Int
	Field y:Int
	Field health_icon:Image
	Field happy_icon:Image
	Field food_icon:Image
	Field medic_icon:Image
	Field pilot_icon:Image
	Field gunner_icon:Image
	
	Field repair_icon:Image
	Field toughness_icon:Image
	Field welding_icon:Image
	Field iPanel:CrewInteractionPanel
	
	Method New(x:Int, y:Int, member:Crew)
		Self.x = x
		Self.y = y
		Self.member = member
		health_icon = LoadImage(ICONS_LOCATION + "icon_crew_attribute_health.png")
		happy_icon = LoadImage(ICONS_LOCATION + "icon_crew_attribute_morale.png")
		repair_icon = LoadImage(ICONS_LOCATION + "icon_skill_repair.png")
		welding_icon = LoadImage(ICONS_LOCATION + "icon_skill_welding.png")
		gunner_icon = LoadImage(ICONS_LOCATION + "icon_skill_gunner.png")
		toughness_icon = LoadImage(ICONS_LOCATION + "icon_crew_attribute_toughness.png")
		medic_icon = LoadImage(ICONS_LOCATION + "icon_skill_medic.png")
		pilot_icon = LoadImage(ICONS_LOCATION + "icon_skill_pilot.png")
	End
	
	Method getCrew:Crew()
		Return member
	End
	
	Method Update:Void()
		If member = Null Then Return
		If iPanel <> Null
			iPanel.Update(x, y)
		End
		If KeyHit(KEY_ENTER)
			member.PrintMorale()
		End
	End
	
	Method Attach:Void(iPanel:CrewInteractionPanel)
		Self.iPanel = iPanel
	End
	
	Method isMouseOver:Bool()
		If Game.Cursor.x() > x And Game.Cursor.x() <= x + WIDTH And Game.Cursor.y() > y And Game.Cursor.y() < y + HEIGHT Then Return True
		Return False
	End
	Method Render:Void(selectedCrew:Crew = Null)
		If member = Null Then Return
		If iPanel <> Null
			iPanel.Render()
		End
		'health
		Local i:Incrementer = New Incrementer(y, 50)
		Local percent:Float = member.health / float(member.GetMaxHp())
		DrawImage(health_icon, x + 2, i.getNext() +2)
		DrawRectOutline(x, i.getCurrent(), 50, 50)
		SetColor(255, 0, 0)
		DrawRect(x + 50, 10 + i.getCurrent(), 200 * percent, 30)
		SetColor(255, 255, 255)
		DrawRectOutline(x + 50, 10 + i.getCurrent(), 200, 30)
		
		'oxygen
		percent = Clamp(member.GetMorale(), 0.0, 100.0) / float(100)
		DrawImage(happy_icon, x + 2, i.getNext() +2)
		DrawRectOutline(x, i.getCurrent(), 50, 50)
		SetColor(0, 255, 255)
		DrawRect(x + 50, 10 + i.getCurrent(), 200 * percent, 30)
		SetColor(255, 255, 255)
		DrawRectOutline(x + 50, 10 + i.getCurrent(), 200, 30)
		
	
		'repair
		i.getNext()
		DrawSkill(i.getNext(), SkillManager.SKILL_PILOT, pilot_icon)
		DrawSkill(i.getNext(), SkillManager.SKILL_MEDIC, repair_icon)
		DrawSkill(i.getNext(), SkillManager.SKILL_GUNNER, gunner_icon)
		DrawSkill(i.getNext(), SkillManager.SKILL_WELDING, welding_icon)
		DrawSkill(i.getNext(), SkillManager.SKILL_TOUGHNESS, toughness_icon)
		DrawSkill(i.getNext(), SkillManager.SKILL_MEDIC, medic_icon)
		
		'medic
		i.getNext()
		i = New Incrementer(i.getNext(), 30)
		Local s:Stack<String> = member.getStatus(True)
		For Local temp:Int = 0 Until s.Length()
			Game.white_font.Draw(s.Get(temp), x, i.getNext())
		Next
		
		'	SetColor(255, 255, 0)
		
		'	DrawRectOutline(x + 65, 10 + y_offset, 190, 30)
		If selectedCrew <> Null And member = selectedCrew
			SetColor(0, 255, 0)
		End
		DrawRectOutline(x - 5, y - 5, WIDTH, HEIGHT)
		ResetColorAlpha
	End
	
	Method DrawSkill:Void(y:Int, skill:Int, icon:Image)
		Local percent:float = member.skills.GetPercent(skill)
		DrawImage(icon, x + 2, y + 2)
		SetColor(255, 255, 0)
		DrawRect(x + 65, 10 + y, 185 * percent, 30)
		SetColor(255, 255, 255)
		DrawRectOutline(x + 65, 10 + y, 185, 30)
		SetColor(0, 0, 0)
		DrawCircle(x + 65, y + 25, 15)
		SetColor(255, 255, 255)
		DrawRectOutline(x, y, 50, 50)
		DrawArc(x + 65, y + 25, 15, 15, 0, 360, 16)
		Game.white_font.Draw(member.skills.types.Get(skill).level, x + 66, y + 6, 0.5)
	End
	
End
Class AiIcon
	Field img:Image
	Field val:Int
	Method New(icon:String, value:int)
		img = LoadImage(icon)
		val = value
	End
	Method New(img:Image, value:Int)
		Self.img = img
		Self.val = value
	End
	Method Render:Void(x:Int, y:int)
		If img <> Null
			DrawImage(img, x, y)
			DrawRectOutline(x, y, 120, 120)
		End
		
	End
End

Class CrewAiBox
	Const ICON_SIZE:Int = 120
	Field member:Crew
	Field x:Int
	Field y:Int
	Field grabBox:AiGrabBox
	Field aiSlot:AiIcon[7]
	Method New(x:Int, y:Int, grabBox:AiGrabBox)
		Self.x = x
		Self.y = y
		Self.grabBox = grabBox
	End
	Method GrabCrew:Void(member:Crew)
		If member = Null Then Return
		Self.member = member
		UpdateAi()
	End
	Method UpdateAi:Void()
		For Local temp:Int = 5 To 11
			aiSlot[temp - 5] = grabBox.GetIcon(member.aiCodes[temp])
		Next
	End
	Method Update:Void(grabAi:AiIcon, c:Crew)
		If c = Null Then Return
		If member <> c
			member = c
			UpdateAi()
		End
		If Game.Cursor.RightHit()
			Local test:AiIcon = grabBox.grabAi()
			If test <> Null
				For Local temp:Int = 0 Until aiSlot.Length()
					If member.aiCodes[temp + 5] = 0
						aiSlot[temp] = test
						member.aiCodes[temp + 5] = test.val
						Exit
					End
				Next
			End
		End
		For Local temp:Int = 0 Until aiSlot.Length()
			If Game.Cursor.x() > x And Game.Cursor.x() < x + ICON_SIZE
				If Game.Cursor.y() > y + (temp * ICON_SIZE) And Game.Cursor.y() < y + ( (temp + 1) * ICON_SIZE)
					If grabAi <> Null And Not Game.Cursor.Down()
						aiSlot[temp] = grabAi
						member.aiCodes[temp + 5] = grabAi.val
					End
					If Game.Cursor.RightHit()
						aiSlot[temp] = Null
						member.aiCodes[temp + 5] = 0
					End
				End
			End
		End
	End
	Method isMouseOver:Bool()
		If Game.Cursor.x() >= x And Game.Cursor.x() <= x + ICON_SIZE
			If Game.Cursor.y() >= y And Game.Cursor.y() <= y + ICON_SIZE * aiSlot.Length()
				Return True
			End
		End
		Return False
	End
	Method GrabAi:AiIcon()
		For Local temp:Int = 0 Until aiSlot.Length()
			If Game.Cursor.x() > x And Game.Cursor.x() < x + 120
				If Game.Cursor.y() > y + (temp * 120) And Game.Cursor.y() < y + ( (temp + 1) * 120)
					If aiSlot[temp] <> Null
						Local tai:AiIcon = New AiIcon(aiSlot[temp].img, aiSlot[temp].val)
						aiSlot[temp] = Null
						member.aiCodes[temp + 5] = 0
						Return tai
					End
				End
			End
		Next
		Return Null
	End
	Method Render:Void(c:Crew)
		If c = Null Then Return
		For Local temp:Int = 0 Until aiSlot.Length()
			If aiSlot[temp] <> Null And aiSlot[temp].val <> 0
				aiSlot[temp].Render(x, y + (temp * 120))
			End
			DrawRectOutline(x, y + (temp * 120), 120, 120)
		Next
	End
End
Class AiGrabBox
	Const AI_FILE:String = "Strings/Crew/ai.json"
	Const ICON_SIZE:Int = 120
	Field x:Int
	Field y:Int
	Field icons:Stack<AiIcon>
	Method New(x:Int, y:int)
		Self.x = x
		Self.y = y
		icons = New Stack<AiIcon>()
		Local arr:JsonArray = JsonArray(New JsonParser(app.LoadString(AI_FILE)).ParseValue())
		For Local temp:Int = 0 Until arr.Length()
			Local item:JsonObject = JsonObject(arr.Get(temp))
			icons.Push(New AiIcon(item.GetString("img"), item.GetInt("value")))
		Next
	End
	Method GetIcon:AiIcon(val:Int)
		For Local temp:Int = 0 Until icons.Length()
			If icons.Get(temp).val = val Then Return icons.Get(temp)
		Next
		Return Null
	End
	Method isMouseOver:Bool()
		If Game.Cursor.x() >= x And Game.Cursor.x() <= x + ICON_SIZE
			If Game.Cursor.y() >= y And Game.Cursor.y() <= y + ICON_SIZE * icons.Length()
				Return True
			End
		End
		Return False
	End
	Method grabAi:AiIcon()
		For Local temp:Int = 0 Until icons.Length()
			If Game.Cursor.x() > x And Game.Cursor.x() < x + 120
				If Game.Cursor.y() > y + (temp * 120) And Game.Cursor.y() < y + ( (temp + 1) * 120)
					Local holding:AiIcon = New AiIcon(icons.Get(temp).img, icons.Get(temp).val)
					Return holding
				End
			End
		Next
		Return Null
	End
	Method Render:Void()
		For Local temp:Int = 0 Until icons.Length()
			icons.Get(temp).Render(x, y + (temp * 120))
		Next
		
	End
End