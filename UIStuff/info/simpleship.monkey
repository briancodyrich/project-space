Strict

Import space_game

Class SimpleShip Extends Window
	Field stat1:ShipStat
	Field stat2:ShipStat
	Field show:ShipStat
	Field delta:DeltaItem
	Field normalize:bool
	Method New(w:Int, h:int, parent:InfoPane, normalize:bool)
		Super.New(w, h, parent)
		Self.normalize = normalize
	End
	Method Update:Void()
		If parent.ship1 <> Null
			If stat1 = Null or stat1.ship = Null Or stat1.ship <> parent.ship1
				stat1 = New ShipStat(parent.ship1, parent.delta)
			End
			stat1.Update(normalize)
		End
		If parent.ship2 <> Null
			If stat2 = Null Or stat2.ship = Null Or stat2.ship <> parent.ship2
				stat2 = New ShipStat(parent.ship2, parent.delta)
			End
			stat2.Update(normalize)
		End
		If stat1 = Null And stat2 <> Null
			show = stat2
		Else If stat1 <> Null And stat2 = Null
			show = stat1
		Else If stat1 <> Null And stat2 <> Null
			stat1.GetDelta(stat2)
			show = stat1
		End
		
	End
	
	Method Render:Void()
		
	End
End


Class SimpleShipA Extends SimpleShip
	Method New(w:Int, h:int, parent:InfoPane, normalize:bool)
		Super.New(w, h, parent, normalize)
	End
	Method Update:Void()
		Super.Update()
	End
	Method Render:Void()
		If show = Null Then Return
		Local spacing:Int = 160
		Local i:Incrementer = New Incrementer(0, 30)	
		PrettyStats(0, i.getNext(), spacing, "Power Load:", show.GetInt(ShipStat.POWER_PER) + "%" + ShipStat.FormatDelta(show.GetInt(ShipStat.POWER_PER, True), True, True))
		PrettyStats(0, i.getNext(), spacing, "Heat Load:", show.GetInt(ShipStat.HEAT_PER) + "%" + ShipStat.FormatDelta(show.GetInt(ShipStat.HEAT_PER, True), True, True))
		PrettyStats(0, i.getNext(), spacing, "Shield:", show.GetInt(ShipStat.SHIELD_TANK) + ShipStat.FormatDelta(show.GetInt(ShipStat.SHIELD_TANK, True), False))
		PrettyStats(0, i.getNext(), spacing, "Battery:", show.GetInt(ShipStat.BATT_TANK) + ShipStat.FormatDelta(show.GetInt(ShipStat.BATT_TANK, True), False))
		i.getNext()
		PrettyStats(0, i.getNext(), spacing, "Hull DPS:", SigDig(show.GetFloat(ShipStat.HULL_DPS), 2) + ShipStat.FormatDelta(show.GetFloat(ShipStat.HULL_DPS, True), False, 2))
		PrettyStats(0, i.getNext(), spacing, "Shield DPS:", SigDig(show.GetFloat(ShipStat.SHIELD_DPS), 2) + ShipStat.FormatDelta(show.GetFloat(ShipStat.SHIELD_DPS, True), False, 2))
		PrettyStats(0, i.getNext(), spacing, "Crew DPS:", SigDig(show.GetFloat(ShipStat.CREW_DPS), 2) + ShipStat.FormatDelta(show.GetFloat(ShipStat.CREW_DPS, True), False, 2))
		PrettyStats(0, i.getNext(), spacing, "Heat Gen:", SigDig(show.GetFloat(ShipStat.GUN_HEAT), 2) + ShipStat.FormatDelta(show.GetFloat(ShipStat.GUN_HEAT, True), True, 2))
		'PrettyStats(0, i.getNext(), 200, "Battery:", batt_output)
	End
End

Class SimpleShipB Extends SimpleShip
	Method New(w:Int, h:int, parent:InfoPane, normalise:bool)
		Super.New(w, h, parent, normalise)
	End
	Method Update:Void()
		Super.Update()
	End
	Method Render:Void()
		If show = Null Then Return
		Local spacing:Int = 200
		Local i:Incrementer = New Incrementer(0, 30)
		'PrettyStats(0, i.getNext(), spacing, "Max:", show.GetInt(ShipStat.POWER_PER + "%" + ShipStat.FormatDelta(show.power_cap_percent[1], False, True))
		i.getNext()
		PrettyStats(0, i.getNext(), spacing, "Coolant:", show.GetInt(ShipStat.COOLANT_TANK) + ShipStat.FormatDelta(show.GetInt(ShipStat.COOLANT_TANK, True), False))
		PrettyStats(0, i.getNext(), spacing, "Recharge:", SigDig(show.GetFloat(ShipStat.SHIELD_RECHARGE), 2) + ShipStat.FormatDelta(show.GetFloat(ShipStat.SHIELD_RECHARGE, True), False, 2))
		PrettyStats(0, i.getNext(), spacing, "TW:", SigDig(show.GetFloat(ShipStat.TW), 2) + ShipStat.FormatDelta(show.GetFloat(ShipStat.TW, True), False, 2))
		PrettyStats(0, i.getNext(), spacing, "Efficiency:", SigDig(show.GetFloat(ShipStat.EFFICIENCY), 2) + ShipStat.FormatDelta(show.GetFloat(ShipStat.EFFICIENCY, True), False, 2))
		i.getNext()
		
		'PrettyStats(0, i.getNext(), 200, "Battery:", batt_output)
	End
End
#rem
Class SimpleShipXEE Extends SimpleShip
	Method New(w:Int, h:int, parent:InfoPane, normalise:bool)
		Super.New(w, h, parent, normalise)
	End
	Method Update:Void()
		Super.Update()
	End
	Method Render:Void()
		If show = Null Then Return
		Local spacing:Int = 200
		Local i:Incrementer = New Incrementer(0, 30)
		PrettyStats(0, i.getNext(), spacing, "Shield:", show.GetInt(ShipStat.SHIELD_TANK) + ShipStat.FormatDelta(show.GetInt(ShipStat.SHIELD_TANK, True), False))
		PrettyStats(0, i.getNext(), spacing, "Recharge:", SigDig(show.GetFloat(ShipStat.SHIELD), 2) + ShipStat.FormatDelta(show.shield_recharge[1], False, 2))
		PrettyStats(0, i.getNext(), spacing, "Hull DPS:", SigDig(show.hull_dps[0], 2) + ShipStat.FormatDelta(show.hull_dps[1], False, 2))
		PrettyStats(0, i.getNext(), spacing, "Shield DPS:", SigDig(show.shield_dps[0], 2) + ShipStat.FormatDelta(show.shield_dps[1], False, 2))
		PrettyStats(0, i.getNext(), spacing, "Crew DPS:", SigDig(show.crew_dps[0], 2) + ShipStat.FormatDelta(show.crew_dps[1], False, 2))
		PrettyStats(0, i.getNext(), spacing, "Heat Gen:", SigDig(show.gun_hps[0], 2) + ShipStat.FormatDelta(show.gun_hps[1], True, 2))
		'PrettyStats(0, i.getNext(), 200, "Battery:", batt_output)
	End
End
#end


Class SimpleShipC Extends SimpleShip
	Method New(w:Int, h:int, parent:InfoPane, normalise:bool)
		Super.New(w, h, parent, normalise)
	End
	Method Update:Void()
		Super.Update()
	End
	Method Render:Void()
		If show = Null Then Return
		Local spacing:Int = 200
		Local i:Incrementer = New Incrementer(0, 30)
		PrettyStats(0, i.getNext(), spacing, "Hull:", show.GetInt(ShipStat.HULL) + ShipStat.FormatDelta(show.GetInt(ShipStat.HULL, True), False))
		PrettyStats(0, i.getNext(), spacing, "Armor:", show.GetInt(ShipStat.ARMOR) + ShipStat.FormatDelta(show.GetInt(ShipStat.ARMOR, True), False))
		PrettyStats(0, i.getNext(), spacing, "Vent:", show.GetInt(ShipStat.HEAT_SINK) + ShipStat.FormatDelta(show.GetInt(ShipStat.HEAT_SINK, True), False))
		'PrettyStats(0, i.getNext(), spacing, "Crew:", show.crew + "/" + show.crew_cap)
	End
End

Class SimpleShipStore Extends SimpleShip
	Method New(w:Int, h:int, parent:InfoPane, normalise:bool)
		Super.New(w, h, parent, normalise)
	End
	Method Update:Void()
		Super.Update()
	End
	Method Render:Void()
		If show = Null Then Return
		Local spacing:Int = 200
		Local i:Incrementer = New Incrementer(0, 30)
		PrettyStats(0, i.getNext(), spacing, "Cost:", FormatCredits(parent.ship2.GetValue(parent.area, False) - parent.ship1.GetValue(parent.area, False, True)))
		PrettyStats(0, i.getNext(), spacing, "Hull:", show.GetInt(ShipStat.HULL) + ShipStat.FormatDelta(show.GetInt(ShipStat.HULL, True), False))
		PrettyStats(0, i.getNext(), spacing, "Armor:", show.GetInt(ShipStat.ARMOR) + ShipStat.FormatDelta(show.GetInt(ShipStat.ARMOR, True), False))
		PrettyStats(0, i.getNext(), spacing, "Vent:", show.GetInt(ShipStat.HEAT_SINK) + ShipStat.FormatDelta(show.GetInt(ShipStat.HEAT_SINK, True), False))
		PrettyStats(0, i.getNext(), spacing, "Crew:", show.GetInt(ShipStat.CREW_CAP) + ShipStat.FormatDelta(show.GetInt(ShipStat.CREW_CAP, True), False))
		PrettyStats(0, i.getNext(), spacing, "Storage:", show.GetInt(ShipStat.HOLD_SIZE) + ShipStat.FormatDelta(show.GetInt(ShipStat.HOLD_SIZE, True), False))
		'PrettyStats(0, i.getNext(), spacing, "Crew:", show.crew + "/" + show.crew_cap)
	End
End