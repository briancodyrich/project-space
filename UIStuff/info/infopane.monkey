Strict
Import space_game

Class Info Abstract
	Field parent:InfoPane
	Method New(parent:InfoPane)
		Self.parent = parent
	End
	Method Update:Void()
		
	End
	Method Render:Void()
		
	End
End

Class Panel Extends Info
	Field x:Int
	Field y:int
	Field position:Int
	Field windows:Stack<Window>
	Method New(x:Int, y:Int, parent:InfoPane)
		Super.New(parent)
		windows = New Stack<Window>()
		Self.x = x
		Self.y = y
	End
	Method Update:Void()
		For Local temp:Int = 0 Until windows.Length()
			windows.Get(temp).Update()
		Next
	End
	Method Render:Void()
		position = x
		For Local temp:Int = 0 Until windows.Length()
			If InfoPane.DRAW_BOX
				DrawRectOutline(position, y, windows.Get(temp).w, windows.Get(temp).h)
			End
			PushMatrix()
			SetBounds(windows.Get(temp))
			windows.Get(temp).Render()
			SetScaledScissor(0, 0, ScreenWidth, ScreenHeight)
			PopMatrix()
		Next
	End
	
	Method SetBounds:Void(w:Window)
		Translate(position, y)
		SetScaledScissor(position, y, w.w, w.h)
		position += w.w
	End
	Method Attach:Void(w:Window)
		windows.Push(w)
	End
End

Class Window Extends Info
	Field w:Int
	Field h:int
	Method New(w:Int, h:Int, parent:InfoPane)
		Super.New(parent)
		Self.w = w
		Self.h = h
		Self.parent = parent
	End
	Method Update:Void()
		
	End
	Method Render:Void()
		
	End
	
	
End

Class InfoPane
	Const DRAW_BOX:bool = True
	Field windows:Stack<Info>
	
	Field ship1:Ship
	Field ship2:Ship
	Field area:Economy
	Field delta:DeltaItem
	
	Method New()
		windows = New Stack<Info>()
	End
	Method Attach:Void(p:Info)
		windows.Push(p)
	End
	Method Update:Void()
		For Local temp:Int = 0 Until windows.Length()
			windows.Get(temp).Update()
		Next
	End
	Method Render:Void()
		For Local temp:Int = 0 Until windows.Length()
			windows.Get(temp).Render()
		Next
	End
	
	
End