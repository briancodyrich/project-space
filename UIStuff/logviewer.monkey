Import space_game

Class LogViewer
	Const BAR_WIDTH:Int = 30
	Field x:Int
	Field y:Int
	Field w:Int
	Field h:Int
	Field start:Int
	Field stop:int
	Field pos:int
	
	
	Field line:int
	
	Field chat:Logger
	Field logs:Stack<TextEffect>
	Field cam:TCam
	Field current:int
	
	Method New(x:Int, y:Int, w:Int, h:Int, chat:Logger)
		Self.x = x
		Self.y = y
		Self.w = w
		Self.h = h
		start = x + 5
		stop = x + w - 10
		pos = y + h - 15
		Self.chat = chat
		cam = New TCam(x, y, w, h)
		logs = New Stack<TextEffect>()
		For Local temp:Int = 0 Until chat.logs.Length()
			Add(chat.logs.Get(temp), True)
			current += 1
		Next
	End
	
	Method Render:Void()
		
		SetScaledScissor(x, y, w + 5, h + 5)
		For Local temp:Int = 0 Until logs.Length()
			logs.Get(temp).Render()
		Next
		
		DrawRectOutline(x, y, w, h)
		DrawRectOutline(x + w, y, 30, 30)
'		DrawRectOutline(x + (cols * GRID_SIZE) + ( (cols + 1) * PADDING) + 1, y + nub + 1, 28, 28 * 3)
		SetScaledScissor(0, 0, ScreenWidth, ScreenHeight)
	End
	
	Method Update:Void()
		If current < Player.chat.logs.Length()
			Add(Player.chat.logs.Get(current))
			current += 1
		End
		cam.Update()
		For Local temp:Int = 0 Until logs.Length()
			If temp = 0 Or logs.Get(temp - 1).done
				logs.Get(temp).Update()
			End
		Next
	End
	
	Method Add:Void(l:LogItem, fast:bool = False)
		If fast
			pos += 30
			cam.Set(30)
		Else
			pos += 30
			cam.Move(30)
		End
		AddSafe(l.GetType(), fast)
		Add(l.message, fast)
	End
	
	Method AddSafe:Void(txt:String, fast:bool)
		'	pos += 20
		'	cam.Move(-20)
		If fast
			logs.Push(New TextEffect(start, pos, TextEffect.TYPE_INSTANT, txt, cam, 30))
		Else
			logs.Push(New TextEffect(start, pos, TextEffect.TYPE_TERMINAL, txt, cam, 30))
		End
		pos += 30
	End
	
	
	Method Add:Void(txt:String, fast:bool)
		If stringTooLong(txt)
			Local spaceDet:Int
			For Local temp:Int = 0 Until txt.Length()
				If txt[temp] = 32
					If Not stringTooLong(txt[0 .. temp])
						spaceDet = temp
					Else
						If fast
							logs.Push(New TextEffect(start, pos, TextEffect.TYPE_INSTANT, txt[0 .. spaceDet], cam, 30))
						Else
							logs.Push(New TextEffect(start, pos, TextEffect.TYPE_TERMINAL, txt[0 .. spaceDet], cam, 30))
						End
						pos += 30
						Add(txt[spaceDet + 1 .. txt.Length()], fast)
						Return
					End
				Else If txt[temp] = 10
					If fast
						logs.Push(New TextEffect(start, pos, TextEffect.TYPE_INSTANT, txt[0 .. temp], cam, 30))
					Else
						logs.Push(New TextEffect(start, pos, TextEffect.TYPE_TERMINAL, txt[0 .. temp], cam, 30))
					End
					pos += 30
					Add(txt[temp + 1 .. txt.Length()], fast)
					Return
				End
			Next
		Else
			If fast
				logs.Push(New TextEffect(start, pos, TextEffect.TYPE_INSTANT, txt, cam, 30))
			Else
				logs.Push(New TextEffect(start, pos, TextEffect.TYPE_TERMINAL, txt, cam, 30))
			End
			pos += 30
		End
	End
	
	Method stringTooLong:Bool(txt:String)
		If txt.Contains("~n") Then Return True
		If start + (Game.white_font.font.TextWidth(txt) * 0.5) > stop Then Return True
		Return False
	End
	
	
	Method isMouseOver:Bool()
		If Game.Cursor.x() > x And Game.Cursor.x() < x + w And Game.Cursor.y() > y And Game.Cursor.y() < y + h Then Return True
		Return False
	End
	
End