Strict
Import space_game


Class PowerBar
	Const TYPE_ENGINE:Int = 1
	Const TYPE_REACTOR:Int = 2
	Const TYPE_SHIELD:Int = 3
	Const WIDTH:int = 30
	Const HEIGHT:Int = 60
	Field type:Int
	Field power:Int
	Field x:Int
	Field y:int
	Field ship:Ship
	Field showPer:float
	Method New(x:Int, y:Int, type:Int, setting:Int, ship:Ship)
		Self.x = x
		Self.y = y
		Self.type = type
		Self.power = setting
		Self.ship = ship
	End
	
	Method Render:Void(mode:int)
		For Local temp:Int = 0 Until 4
			If temp <= power
				If type = TYPE_ENGINE And ship.canFireEngines() = False
					SetColor(255, 0, 0)
				Else
					SetColor(0, 255, 0)
				End
				DrawRect(x + (WIDTH * temp), y, WIDTH, HEIGHT)
				ResetColorAlpha()
				DrawRectOutline(x + (WIDTH * temp), y, WIDTH, HEIGHT)
			Else
				DrawRectOutline(x + (WIDTH * temp), y, WIDTH, HEIGHT)
			End
		Next
		Select type
			Case TYPE_ENGINE
				power = ship.enginePower
				Game.white_font.Draw("Engine", x + (WIDTH * 5), y)
				Local percent:Float = 0
				If ship.heatMax <> 0
					percent = (ship.heatUnits / float(ship.heatMax))
				End
				SetColor(255, 0, 0)
				If percent < showPer
					showPer -= 0.02
				Else If percent > showPer
					showPer += 0.02
				End
				If Abs(percent - showPer) < 0.02
					showPer = percent
				End
				If showPer > 1 Then showPer = 1
				If showPer < 0 Then showPer = 0
				DrawRect(x - (WIDTH * 8) - 20, y, WIDTH * 8 * showPer, HEIGHT)
				ResetColorAlpha()
				If CombatScreen.DISPLAY_VALUES
					Game.white_font.Draw(ship.heatUnits + "/" + ship.heatMax, x - (WIDTH * 5), y + 10)
				End
				
				
				DrawRectOutline(x - (WIDTH * 8) - 20, y, WIDTH * 8, HEIGHT)
			Case TYPE_REACTOR
				power = ship.reactorPower
				Game.white_font.Draw("Reactor", x + (WIDTH * 5), y)
				Local percent:Float = 0
				If ship.powerMax <> 0
					percent = (ship.powerUnits) / float(ship.powerMax)
				End
				SetColor(255, 255, 0)
				If percent < showPer
					showPer -= 0.02
				Else If percent > showPer
					showPer += 0.02
				End
				If Abs(percent - showPer) < 0.02
					showPer = percent
				End
				If showPer > 1 Then showPer = 1
				If showPer < 0 Then showPer = 0
				DrawRect(x - (WIDTH * 8) - 20, y, WIDTH * 8 * showPer, HEIGHT)
				ResetColorAlpha()
				DrawRectOutline(x - (WIDTH * 8) - 20, y, WIDTH * 8, HEIGHT)
				If CombatScreen.DISPLAY_VALUES
					Game.white_font.Draw(ship.powerUnits + "/" + ship.powerMax, x - (WIDTH * 5), y + 10)
				End				
				If mode = ShipPanel.COMBAT_MODE
					percent = 0
					If ship.batteryMax <> 0
						percent = ship.batteryUnits / float(ship.batteryMax)
					End
					
					If ship.insuficientPower
						SetColor(255, 0, 0)
					Else
						SetColor(0, 255, 0)
					End
					DrawRect(x - (WIDTH * 8) + 550, y, WIDTH * 8 * percent, HEIGHT)
					ResetColorAlpha()

					DrawRectOutline(x - (WIDTH * 8) + 550, y, WIDTH * 8, HEIGHT)
				End
			Case TYPE_SHIELD
				power = ship.shieldPower
				Game.white_font.Draw("Shield", x + (WIDTH * 5), y)
				Local percent:Float = 0
				If ship.shieldMax <> 0
					percent = ship.shieldUnits / float(ship.shieldMax)
				End
				SetColor(0, 0, 255)
				If percent < showPer
					showPer -= 0.008
				Else If percent > showPer
					showPer += 0.008
				End
				If Abs(percent - showPer) < 0.008
					showPer = percent
				End
				If showPer > 1 Then showPer = 1
				If showPer < 0 Then showPer = 0
				DrawRect(x - (WIDTH * 8) - 20, y, WIDTH * 8 * showPer, HEIGHT)
				ResetColorAlpha()
				DrawRectOutline(x - (WIDTH * 8) - 20, y, WIDTH * 8, HEIGHT)
				
				If CombatScreen.DISPLAY_VALUES
					Game.white_font.Draw(ship.shieldUnits + "/" + ship.shieldMax, x - (WIDTH * 5), y + 10)
				End				
				
				If mode = ShipPanel.COMBAT_MODE
					percent = ship.HP / float(ship.maxHP)
					If percent < 0 Then percent = 0
					If percent > 1
						SetColor(102, 0, 204)
						DrawRect(x - (WIDTH * 8) + 550, y, WIDTH * 8 * 1, HEIGHT)
						SetColor(0, 255, 255)
						DrawRect(x - (WIDTH * 8) + 550, y, WIDTH * 8 * (percent - 1), HEIGHT)
						ResetColorAlpha()
					Else
						SetColor(102, 0, 204)
						DrawRect(x - (WIDTH * 8) + 550, y, WIDTH * 8 * percent, HEIGHT)
						ResetColorAlpha()
					End
					
				
					DrawRectOutline(x - (WIDTH * 8) + 550, y, WIDTH * 8, HEIGHT)
				End

			Default
		End

	End
	Method Update:bool()
		If Game.Cursor.Hit
			For Local temp:Int = 0 Until 4
				If Game.Cursor.x() > x + (WIDTH * temp) And Game.Cursor.x() < x + (WIDTH * temp) + WIDTH And Game.Cursor.y() > y And Game.Cursor.y() < y + HEIGHT
					power = temp
					Select type
						Case TYPE_ENGINE
							If ship.canFireEngines() = True
								ship.enginePower = temp
							Else
								power = 0
							End
						Case TYPE_REACTOR
							ship.reactorPower = temp
						Case TYPE_SHIELD
							ship.shieldPower = temp
						Default
							Print "error"
					End
					Return True
				End
			Next
		End
		Return False
	End
End