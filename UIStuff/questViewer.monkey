Strict

Import space_game

Class QuestViewer
	Const BUTTON_W:Int = 200
	Const BUTTON_H:Int = 80
	Const BUTTON_PAD:Int = 10
	Const SPACING:Int = 20
	Field x:Int
	Field y:Int
	Field w:Int
	Field h:int
	Field buttons:Stack<QuestButton>
	Field accept:Button
	Field current:QuestButton
	Method New(x:Int, y:Int, w:Int, h:int, jobs:Stack<Quest>)
		buttons = New Stack<QuestButton>()
		Self.x = x
		Self.y = y
		Self.w = w
		Self.h = h
		Local i:Incrementer = New Incrementer(BUTTON_PAD + y, BUTTON_PAD + BUTTON_H)
		For Local temp:Int = 0 Until jobs.Length()
			buttons.Push(New QuestButton(x + BUTTON_PAD, i.getNext(), BUTTON_W, BUTTON_H, jobs.Get(temp)))
			If temp = 0
				accept = New Button(x + (BUTTON_PAD * 2) + BUTTON_W, i.getCurrent(), BUTTON_W, BUTTON_H, "Accept")
			End
		Next
	End
	Method Update:Void()
		If Game.Cursor.Hit()
			If current <> Null
				If accept.isMouseOver()
					Player.addQuest(current.qData)
				End
			End
			For Local temp:Int = 0 Until buttons.Length()
				If buttons.Get(temp).isMouseOver()
					current = buttons.Get(temp)
				End
			Next
		End
	End
	Method Render:Void()
		DrawRectOutline(x, y, w, h)
	'	SetScaledScissor(x, y, w + 5, h + 5)
		For Local temp:Int = 0 Until buttons.Length()
			buttons.Get(temp).Render()
		Next
		If current <> Null
			accept.Render()
			Local x_pos:Int = BUTTON_PAD * 2 + BUTTON_W + x
			Local i:Incrementer = New Incrementer(y + 5 + BUTTON_H + BUTTON_PAD, SPACING)
			For Local temp:Int = 0 Until current.descr.Length()
				Game.white_font.Draw(current.descr.Get(temp), x_pos, i.getNext())
			Next
			If current.qData.ship <> Null
				current.qData.ship.Render(0, False)
			End
		End
	'	SetScaledScissor(0, 0, ScreenWidth, ScreenHeight)
	End
End

Class QuestButton Extends Button
	Field qData:Quest
	Field descr:Stack<String>
	Method New(x:int, y:int, w:int, h:int, qData:Quest)
		Super.New(x, y, w, h, qData.buttonText)
		Self.qData = qData
		descr = qData.descr
	End
	Method Render:Void()
		Super.Render()
	End
	Method isMouseOver:bool()
		Return Super.isMouseOver()
	End
End

Class Button
	Field x:Int
	Field y:Int
	Field w:Int
	Field h:int
	Field text:String
	Method New(x:Int, y:Int, w:Int, h:Int, text:String)
		Self.x = x
		Self.y = y
		Self.w = w
		Self.h = h
		Self.text = text
	End
	Method Render:Void()
		If isMouseOver() Then SetColor(255, 0, 0)
		DrawRectOutline(x, y, w, h)
		Game.white_font.Draw(text, x + w / 2, y + h / 4, 0.5)
		ResetColorAlpha()
	End
	Method isMouseOver:bool()
		If Game.Cursor.x() >= x And Game.Cursor.x() <= x + w And Game.Cursor.y() >= y And Game.Cursor.y() <= y + h Then Return True
		Return False
	End
End