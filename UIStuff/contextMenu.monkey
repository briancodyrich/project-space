Strict

Import space_game

Class ContextMenu
	'Const ACTION_REPAIR_SHIP:Int = 101
	Const ACTION_UNDOCK:Int = 102
	Field topBar:Stack<ContextItem>
	Field sideBar:Stack<ContextItem>
	Field optionBtn:MenuButton
	Field distressBtn:MenuButton
	Field isLocked:bool
	Field disableContext:bool
	Field exitBehavior:int
	Field key:int
	Method New()
		topBar = New Stack<ContextItem>()
		sideBar = New Stack<ContextItem>()
		optionBtn = New MenuButton(0, 0, ContextItem.WIDTH, ContextItem.HEIGHT)
		optionBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		distressBtn = New MenuButton(0, 0, ContextItem.WIDTH, ContextItem.HEIGHT)
		distressBtn.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		
	End
	Method Update:Void()
		Local t:int
		For Local temp:Int = 0 Until topBar.Length()
			t = topBar.Get(temp).Update(isLocked)
			If t <> 0 Then key = t
		Next
		For Local temp:Int = 0 Until sideBar.Length()
			t = sideBar.Get(temp).Update(isLocked)
			If t <> 0 Then key = t
		Next
		If disableContext = True Then Return
		If Player.map.hexField.grabObject <> Null
			If Player.map.hexField.grabObject.option <> ""
				optionBtn.Poll()
				If optionBtn.hit
					If isLocked
						Select Player.map.hexField.grabObject.option
							Case HexObject.ACTION_DOCK
								Player.accounting.onDocking()
								For Local c:Crew = EachIn Player.ship.crew
									If GetSigmoid(c.GetMorale(), 0, 85) + c.loyalty < Rnd(1)
										Player.chat.Log(LogItem.BANK, c.first_name + " " + c.last_name + " Has quit")
										Player.map.quitFlag = True
										Player.ship.crew.RemoveEach(c)
									Else
										For Local temp:Int = 0 Until c.morale.Length()
											If c.morale.Get(temp).type = Morale.TIME_AWAY
												c.morale.Get(temp).Reset()
											End
										Next
									End
								Next
								Player.SaveGame()
								key = Screens.Dock
							Case HexObject.ACTION_MINE
								Player.SaveGame()
								key = Screens.Asteroid
							Case HexObject.ACTION_POI
								Player.map.hexField.ClearCurrent()
								Player.interaction.LoadScript("on_poi.ess")
							Case HexObject.ACTION_QUEST
								Player.quests.currentQuest = Player.map.hexField.grabObject.questid
								Local quest:Quest = Player.quests.getQuest(Player.map.hexField.grabObject.questid)
								
								Print "id: " + Player.map.hexField.grabObject.questid
								Player.map.hexField.ClearCurrent()
								If quest <> Null
									Player.interaction.LoadScript(quest.script)
								Else
									Player.quests.currentQuest = 0
								End
						End
					Else
						Select Player.map.hexField.grabObject.option
							Case HexObject.ACTION_DOCK
								Player.chat.Log(LogItem.COMMS, "Welcome to " + Player.map.hexField.grabObject.name)
								Player.accounting.onDocking()
								For Local c:Crew = EachIn Player.ship.crew
									If GetSigmoid(c.GetMorale(), 0, 85) + c.loyalty < Rnd(1)
										Player.chat.Log(LogItem.CREW, c.first_name + " " + c.last_name + " Has quit")
										Player.ship.crew.RemoveEach(c)
									Else
										For Local temp:Int = 0 Until c.morale.Length()
											If c.morale.Get(temp).type = Morale.TIME_AWAY
												c.morale.Get(temp).Reset()
											End
										Next
									End
								Next
								Player.SaveGame()
								
								Game.SwitchScreen(Screens.Dock)
							Case HexObject.ACTION_MINE
								Player.SaveGame()
								Game.SwitchScreen(Screens.Asteroid)
							Case HexObject.ACTION_POI
								Player.map.hexField.ClearCurrent()
								Player.interaction.LoadScript("on_poi.ess")
							Case HexObject.ACTION_QUEST
								Player.quests.currentQuest = Player.map.hexField.grabObject.questid
								Local quest:Quest = Player.quests.getQuest(Player.map.hexField.grabObject.questid)
								
								Print "id: " + Player.map.hexField.grabObject.questid
								Player.map.hexField.ClearCurrent()
								If quest <> Null
									Player.interaction.LoadScript(quest.script)
								Else
									Player.quests.currentQuest = 0
								End
						End
					End
				End
			End
		End
		If Player.map.hexField.onEdgeTile
			optionBtn.Poll()
			If optionBtn.hit
				If isLocked
					key = Screens.Hyperspace
				Else
					Game.SwitchScreen(Screens.Hyperspace)
				End
			End
		End
	End
	Method Render:Void()
		For Local temp:Int = 0 Until topBar.Length()
			topBar.Get(temp).Render()
		Next
		For Local temp:Int = 0 Until sideBar.Length()
			sideBar.Get(temp).Render()
		Next
		If disableContext = True Then Return
	'	Local optionCount:int = 0
		If Player.map.hexField.grabObject <> Null And Player.map.hexField.grabObject.option <> ""
			optionBtn.x = ContextItem.TOP_START + (ContextItem.X_SPACE * ContextItem.topNum)
		'	optionCount += 1
			optionBtn.RenderScaled(0, 0, 0.75)
			Game.white_font.Draw(Player.map.hexField.grabObject.option, optionBtn.x + optionBtn.w / 2, optionBtn.y + optionBtn.h / 6, 0.5)
		End
		If Player.map.hexField.onEdgeTile
			optionBtn.x = ContextItem.TOP_START + (ContextItem.X_SPACE * ContextItem.topNum)
			'	optionCount += 1
			optionBtn.RenderScaled(0, 0, 0.75)
			Game.white_font.Draw("Hyperspace", optionBtn.x + optionBtn.w / 2, optionBtn.y + optionBtn.h / 6, 0.5)
		End
	End
	Method LockMenu:Void()
		isLocked = True
	End
	Method SwitchKey:Void()
		Local k:Int = key
		UnlockMenu()
		Game.DoSwitch(k)
	End
	Method UnlockMenu:Void()
		key = 0
		isLocked = False
	End
	Method isLockTriggered:Bool()
		If key <> 0 Then Return True
		Return False
	End
	Method Clear:Void()
		sideBar.Clear()
		topBar.Clear()
		ContextItem.topNum = 0
		ContextItem.sideNum = 0
	End
	Method Build:Void(screenNum:Int)
		Clear()
		Select screenNum
			Case Screens.Hyperspace
			topBar.Push(New ContextItem(False, Screens.SystemMap, "System"))
			topBar.Push(New ContextItem(False, Screens.ShipStatusMenu, "Ship"))
			topBar.Push(New ContextItem(False, Screens.CrewStatusMenu, "Crew"))
			disableContext = True
		
			Case Screens.SystemMap
			topBar.Push(New ContextItem(False, Screens.SystemMap, "Map"))
			topBar.Push(New ContextItem(False, Screens.ShipStatusMenu, "Ship"))
			topBar.Push(New ContextItem(False, Screens.CrewStatusMenu, "Crew"))
			disableContext = False
			
			Case Screens.Dock
			topBar.Push(New ContextItem(False, ACTION_UNDOCK, "Undock"))
			topBar.Push(New ContextItem(False, Screens.ShipStatusDock, "Ship"))
			topBar.Push(New ContextItem(False, Screens.CrewStatusDock, "Crew"))
			topBar.Push(New ContextItem(False, Screens.GoodsStore, "Goods"))
			topBar.Push(New ContextItem(False, Screens.PartsStore, "Parts"))
			topBar.Push(New ContextItem(False, Screens.WeaponStore, "Weapons"))
			topBar.Push(New ContextItem(False, Screens.ModStore, "Mods"))
			topBar.Push(New ContextItem(False, Screens.ShipStore, "Shipyard"))
			
			sideBar.Push(New ContextItem(True, Screens.Dock, "Report"))
			sideBar.Push(New ContextItem(True, Screens.GalaxyMap, "Galaxy Map"))
			sideBar.Push(New ContextItem(True, Screens.HireCrew, "Hire Crew"))
			sideBar.Push(New ContextItem(True, Screens.JobBoard, "Job Board"))
			disableContext = True
		End
	End
End

Class ContextItem
	Const TOP_START:Int = 0
	Const SIDE_START:Int = 100
	Const WIDTH:Int = 160
	Const HEIGHT:Int = 56
	Const X_SPACE:Int = 170
	Const Y_SPACE:Int = 60
	Field screen:Int
	Field text:String
	Field button:MenuButton
	Global topNum:Int = 0
	Global sideNum:Int = 0
	Method New(isSide:Bool, screen:Int, text:String)
		If isSide
			button = New MenuButton(0, SIDE_START + (sideNum * Y_SPACE), WIDTH, HEIGHT)
			sideNum += 1
		Else
			button = New MenuButton(TOP_START + (X_SPACE * topNum), 0, WIDTH, HEIGHT)
			topNum += 1
		End
		Self.text = text
		Self.screen = screen
		button.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
	End
	Method Update:int(isLocked:bool)
		button.Poll()
		If button.hit
			If screen < 100
				If Game.CurrentScreenNumber <> screen
					If isLocked
						Return screen
					Else
						Game.DoSwitch(screen)
					End
				End
			Else
				#rem
				If screen = ContextMenu.ACTION_REPAIR_SHIP
					If Player.getCredits() >= Player.ship.GetRepairCost(Player.map.hexField.area)
						Player.accounting.MakePayment(Payment.REPAIRS, -Player.ship.GetRepairCost(Player.map.hexField.area), True)
						Player.ship.RepairAll()
					Else
						'no money goes here
					End
				End
				#end
				If screen = ContextMenu.ACTION_UNDOCK And Not Player.ship.isDead() And Not Player.ship.isNull()
					If isLocked
						Player.accounting.unDock()
						Player.SaveGame()
						Return Screens.SystemMap
					Else
						Player.accounting.unDock()
						Player.SaveGame()
						Game.DoSwitch(Screens.SystemMap)
					End
				End
			End
		End
		Return 0
	End
	
	Method Render:Void()
		button.RenderScaled(0, 0, 0.75)
		Game.white_font.Draw(text, button.x + button.w / 2, button.y + button.h / 6, 0.5)
	End
End