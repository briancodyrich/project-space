Strict

Import space_game

Class SystemData
	Field area:Economy
	Field values:Float[]
	Field average:Float[]
	Field names:String[]
	Field rArr:Bool[]
	
	Field x:Int
	Field y:Int
	Field bar_length:Int
	Field bar_height:Int
	Field padding:Int
	Method New(area:Economy, x:Int, y:Int, bar_length:Int, bar_height:int, padding:int)
		Self.area = area
		If area <> Null
			UpdateSystem(area)
		End
		Self.x = x
		Self.y = y
		Self.bar_length = bar_length
		Self.bar_height = bar_height
		Self.padding = padding
	End
	
	
	Method UpdateSystem:Void(area:Economy)
		Self.area = area
	End
	
	Method Render:Void()
		If area = Null Then Return
		Local bar:Incrementer = New Incrementer(y, bar_height)
		Local pad:Incrementer = New Incrementer(0, padding)
		For Local temp:Int = 0 Until values.Length()
			
			Local percent:Float = values[temp] / (average[temp] * 2)
			If rArr[temp] = True
				SetColor(int(255 * (percent)), 0, int(255 * (1.0 - percent)))
			Else
				SetColor(int(255 * (1.0 - percent)), 0, int(255 * (percent)))
			End
			DrawRect(x, y + bar.getNext() +pad.getNext(), bar_length * percent, bar_height)
			ResetColorAlpha()
			Game.white_font.Draw(names[temp], x + bar_length + 8, y + bar.getCurrent() +pad.getCurrent())
			DrawRectOutline(x, y + bar.getCurrent() +pad.getCurrent(), bar_length, bar_height)
			DrawLine(x + (bar_length / 2), y + bar.getCurrent() +pad.getCurrent(), x + (bar_length / 2), y + bar.getCurrent() +pad.getCurrent() +bar_height)
		Next
		Game.white_font.Draw(area.name, x + bar_length / 2, y - 30, 0.5)
		Game.white_font.Draw("Lv " + area.level, x + bar_length / 2, y, 0.5)
	End
End

#rem
Class BarData
	Field val:float
	Method New
End
#end