
#If TARGET = "html5" Or TARGET = "flash" Or TARGET = "glfw" Or TARGET = "xna"
Import "native/yimput.${TARGET}.${LANG}"
#End

Extern

#If TARGET="html5" Or TARGET="glfw" Or TARGET="flash"
Function yimput_mouseWheel:Float()
Function yimput_init()
#ElseIf TARGET="xna"
Function yimput_mouseWheel:Float() = "yimWork.mouseWheel"
Function yimput_init() = "yimWork.init"
#EndIf

