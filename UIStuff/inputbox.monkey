Import space_game

Class InputBox
	Field x:Int
	Field y:Int
	Field w:Int
	Field h:int
	Field label:string
	Field length:Int
	Field text:String
	
	Field isActive:bool
	Field hasText:bool
	Method New(x:Int, y:Int, w:Int, h:Int, text:String, label:string)
		Self.x = x
		Self.y = y
		Self.w = w
		Self.h = h
		Self.label = label
		Self.text = text
	End
	
	Method Render:Void(overide:int[] =[])
		If isActive
			SetColor(0, 255, 000)
		Else
			SetColor(0, 0, 255)
		End
		If overide.Length() = 3
			SetColor(overide[0], overide[1], overide[2])
		End
		DrawRectOutline(x, y, w, h)
		SetColor(255, 255, 255)
		
		If isActive
			Local carat:String = ""
			If Millisecs() Mod 200 < 100 Then carat = "_"
			Game.white_font.Draw(text, x + (w / 2), y + 5, 0.5)
		Else
			Game.white_font.Draw(text, x + (w / 2), y + 5, 0.5)
		End
		If text = "" And Not isActive Then Game.white_font.Draw(label, x + (w / 2), y + 5, 0.5)
		
		
	End
	Method Update:Void()
		If Game.Cursor.x() > x And Game.Cursor.x() < (x + w) And Game.Cursor.y() > y And Game.Cursor.y() < (y + h)
			If Game.Cursor.Hit
				isActive = True
			End
		End
		
		If isActive
			Repeat
				Local c:Int = GetChar()
				If c = 0 Or c = 96 Then
					Exit
				ElseIf c = CHAR_ENTER
					isActive = False
					text = text.Trim()
					hasText = True
				ElseIf c = CHAR_BACKSPACE
					text = text[0 .. - 1]
				ElseIf c >= 32 And c < 127
					text += String.FromChar(c)
				End If
			Forever
		End If
	End
	Method GetText:String()
		hasText = False
		Return text
	End
End