Strict

Import space_game

Class ShipScroller Extends Info
	Const NEXT_SHIP:Int = 1
	Const LAST_SHIP:Int = 2
	Const BUY_SHIP:Int = 3
	Const VIEW_EQUIPMENT:Int = 4
	
	Const BTN_W:Int = 160
	Const BTN_H:int = 56
	
	Field x:Int
	Field y:Int
	Field w:Int
	Field h:int
	Field mirrored:bool
	Field hold:Inventory
	Field pos:int
	Field name:String
	Field value:int
	Field btn:Stack<MenuButton>
	Field btn_effect:Stack<Int>
	Field btn_label:Stack<String>
	Field btn_label_swap:Stack<String>
	Field list:Stack<JsonObject>
	Field area:Economy
	Field loadout:EquipmentManager
	Field buy_flag:bool
	
	Field enginePower:PowerBar
	Field reactorPower:PowerBar
	Field shieldPower:PowerBar
	
	Field ex:Int
	Field ey:Int
	Field rows:Int
	Field cols:int
	
	Method New(x:Int, y:int, mirrored:bool, area:Economy, list:Stack<JsonObject>, parent:InfoPane)
		Super.New(parent)
		Self.x = x
		Self.y = y
		Self.mirrored = mirrored
		Self.area = area
		w = Ship.MAX_X * Ship.GRID_SIZE
		h = Ship.MAX_Y * Ship.GRID_SIZE
		Self.list = list
		If list = Null
			Print "NO LIST"
		Else
			LoadShip(pos)
		End
		btn = New Stack<MenuButton>()
		btn_label = New Stack<String>()
		btn_label_swap = New Stack<String>()
		btn_effect = New Stack<Int>()
	End
	
	Method SetCurentShip:Void(playerShip:Ship, credits:int)
		
	End
	
	Method SetEquipment:Void(ex:Int, ey:Int, rows:Int, cols:int)
		Self.ex = ex
		Self.ey = ey
		Self.rows = rows
		Self.cols = cols
	End

	Method AddButton:Void(x:Int, y:int, type:int, text:String)
		Local b:MenuButton = New MenuButton(x, y, BTN_W, BTN_H)
		b.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		btn.Push(b)
		btn_label.Push(text)
		btn_label_swap.Push(text)
		btn_effect.Push(type)
	End
	Method AddButton:Void(x:Int, y:int, type:int, text:String, swap_text:String)
		Local b:MenuButton = New MenuButton(x, y, BTN_W, BTN_H)
		b.SetImages(LoadImage("buttons/btn.png"), LoadImage("buttons/btn_over.png"), LoadImage("buttons/btn_down.png"))
		btn.Push(b)
		btn_label.Push(text)
		btn_label_swap.Push(swap_text)
		btn_effect.Push(type)
	End
	Method Update:Void()
		If list = Null Then Return
		If loadout <> Null
			loadout.Update()
		End
		
		If parent.ship2 <> Null
			parent.ship2.shield_on = False
			parent.ship2.Update()
		End
		For Local temp:Int = 0 Until btn.Length()
			btn.Get(temp).Poll()
			If btn.Get(temp).hit
				Do(btn_effect.Get(temp))
				Local swap:String = btn_label.Get(temp)
				btn_label.Set(temp, btn_label_swap.Get(temp))
				btn_label_swap.Set(temp, swap)
			End
		Next
	End
	
	Method LoadShip:Void(number:int)
		Local data:JsonObject = list.Get(number)
		name = data.GetString("name")
		parent.ship2.OnLoad(ShipLoader.GetShip(data.GetString("ship")))
		hold = parent.ship2.items
		parent.ship2.items = New Inventory(1)
		parent.ship2.items.GiveItem("Fuel", 8)
		value = parent.ship2.GetValue(area)
		Game.UnderParticle.Clear()
		Game.OverParticle.Clear()
		parent.ship2.Update(True)
	End
	
	Method GetShip:Ship()
		parent.ship2.items = hold
		Return parent.ship2
	End
	
	Method Do:Void(type:Int)
		Local last_pos:Int = pos
		If type = NEXT_SHIP Or type = LAST_SHIP
			If type = NEXT_SHIP
				pos += 1
			End
			If type = LAST_SHIP
				pos -= 1
			End
			If pos <= 0 Then pos = list.Length() -1
			If pos >= list.Length Then pos = 0
			If pos <> last_pos
				LoadShip(pos)
				If loadout <> Null
					Local p:PassData = New PassData(0, 0, 0)
					Local grab:GrabItem = New GrabItem()
					loadout = New EquipmentManager(ex, ey, rows, cols, p, parent.ship2, grab)
					loadout.filter.WhiteList([])
				End
			End
		Else If type = VIEW_EQUIPMENT
			If loadout = Null
				Local p:PassData = New PassData(0, 0, 0)
				Local grab:GrabItem = New GrabItem()
				loadout = New EquipmentManager(ex, ey, rows, cols, p, parent.ship2, grab)
				loadout.filter.WhiteList([])
			Else
				loadout = Null
			End
		Else If type = BUY_SHIP
			Player.ship.items.Sort()
			'ShipItems
			'ShipCrew
			If Player.getCredits() < parent.ship2.GetValue(parent.area) - parent.ship1.GetValue(parent.area, False, True)
				ThrowErrorMessage("", "Insufficient credits")
				Return
			End
			
			If Player.ship.items.CountSlots(True, False) > parent.ship2.storageSpace
				Local message:Stack<String>
				message = New Stack<String>
				message.Push("Insufficient storage")
				message.Push("Discard extra items?")
				Game.popup = New Popup("ShipItems", message, "Yes", Popup.OK,,, "Cancel", Popup.CANCEL)
				Return
			End
			
			If Player.ship.crew.Length() > parent.ship2.crewSlots
				Local message:Stack<String>
				message = New Stack<String>
				message.Push("Insufficient crew space")
				message.Push("Continue with purchase?")
				Game.popup = New Popup("ShipCrew", message, "Yes", Popup.OK,,, "Cancel", Popup.CANCEL)
				Return
			End
			buy_flag = True
		End
	End
	Method Render:Void()
		If list = Null Then Return
		If loadout <> Null Then loadout.Render()
		If parent.ship2 <> Null
			parent.ship2.Render(, False)
			parent.ship2.DrawOutline()
		End
		For Local temp:Int = 0 Until btn.Length()
			Local b:MenuButton = btn.Get(temp)
			b.RenderScaled(0, 0, 0.75)
			Game.white_font.Draw(btn_label.Get(temp), b.x + b.w / 2, b.y + b.h / 6, 0.5)
		Next
		If enginePower <> Null And parent.ship2 <> Null
			enginePower.Render(ShipPanel.MAP_MODE)
			reactorPower.Render(ShipPanel.MAP_MODE)
			shieldPower.Render(ShipPanel.MAP_MODE)
		End
	End
End

Class ShipStatViewer
	Field total_power_gen:Int
	Field total_power_use:Int = 0
	Field reactor_power:Int
	Field power_load:Int
	Field backup_load:Int
	Field heat_load:int
	Field backup_time:int
	Field reactor_heat:int
	Field engine_power:Int 'lower is better
	Field engine_heat:int
	Field gun_power:int 'lower is better
	Field gun_heat:float
	Field batt_tank:int
	Field batt_output:int
	Field batt_time:int
	Field heat_release:int
	Field heat_gen:Int
	Field shield_power:Int 'lower is better
	Field shield_tank:Int
	Field shield_time:Int
	Field coolant_tank:int
	Field total_heat:int
	Field total_power:int
	Field ship:Ship
	
	Method New(ship:Ship)
		Self.ship = ship
	End

	
	Method Update:Void(sim_mode:Bool = False)
		Reset()
		heat_release = ship.heatRelease
		heat_gen = ship.heatGen
		heat_load = (heat_gen / float(ship.heatRelease)) * 100
		If ship.powerMax = 0
			power_load = 0
		Else
			power_load = ( (ship.powerMax - ship.powerUnits) / float(ship.powerMax)) * 100
		End
		TestComponent(Component.BATTERY, sim_mode)
		TestComponent(Component.SHIELD, sim_mode)
		If ship.powerUnits >= 0
			backup_load = 100
		Else
			backup_load = 100 - ( (Abs(ship.powerUnits) / float(batt_output)) * 100)
		End
		If ship.powerUnits < 0
			backup_time = batt_tank / Abs(ship.powerUnits) / 3
		Else
			backup_time = 0
		End
		If (sim_mode)
			TestComponent(Component.COOLANT_TANK, sim_mode)
			TestComponent(Component.HARDPOINT, sim_mode)
		End
		
	End
	
	Method Reset:Void()
		batt_tank = 0
		reactor_power = 0
		reactor_heat = 0
		engine_power = 0
		engine_heat = 0
		shield_power = 0
		gun_power = 0
		gun_heat = 0
		batt_output = 0
		shield_power = 0
		shield_tank = 0
		shield_time = 0
		coolant_tank = 0
	End
	
	Method GetDelta:Void(o:ShipStatViewer)
		
	End
	
	Method TestComponent:Void(type:Int, simflag:bool)
		For Local temp:Int = 0 Until ship.comp.Length()
			If ship.comp.Get(temp).getType() <> type Then Continue
			If ship.comp.Get(temp).getType() = Component.REACTOR
				Local r:Reactor = Reactor(ship.comp.Get(temp).getSelf())
				reactor_power += (r.energyProduction * r.GetEfficiency(True,, False)) * GetMult(ship.reactorPower, 1.5)
				reactor_heat += (r.heatProduction * r.GetEfficiency(True,, False)) * GetMult(ship.reactorPower, 2.0)
			End
			
			If ship.comp.Get(temp).getType() = Component.ENGINE
				Local r:Engine = Engine(ship.comp.Get(temp).getSelf())
				engine_power += (r.energyProduction * r.GetEfficiency(True,, False)) * GetMult(ship.enginePower, 2.0)
				engine_heat += (r.heatProduction * r.GetEfficiency(True,, False)) * GetMult(ship.enginePower, 2.0)
			End
			If ship.comp.Get(temp).getType() = Component.BATTERY
				Local r:Battery = Battery(ship.comp.Get(temp).getSelf())
				If simflag Then r.simflag = True
				batt_tank += r.capacity * r.GetEfficiency()
				batt_output += r.dischargeRate * r.GetEfficiency()
			End
			If ship.comp.Get(temp).getType() = Component.SHIELD
				Local r:Shield = Shield(ship.comp.Get(temp).getSelf())
				If simflag Then r.simflag = True
				shield_tank += r.capacity * r.GetEfficiency()
				If ship.shieldPower <> 0 And r.isActive()
					shield_time += ( (r.capacity * r.GetEfficiency()) * r.rechargeRate) / float(ship.shieldPower) / 3
				End
			End
			If ship.comp.Get(temp).getType() = Component.COOLANT_TANK
				Local r:CoolantTank = CoolantTank(ship.comp.Get(temp).getSelf())
				If simflag Then r.simflag = True
				coolant_tank += r.capacity * r.GetEfficiency()
			End
			If ship.comp.Get(temp).getType() = Component.HARDPOINT
				Local r:Hardpoint = Hardpoint(ship.comp.Get(temp).getSelf())
				If simflag Then r.simflag = True
				If r.gun <> Null
					gun_power -= r.gun.power_needed
					gun_heat += r.gun.heat_gen / float(r.gun.cycle_time)
				End
			End
			
		Next
	End
	
	
	
	Method RenderStore:Void(area:SystemType = Null)
		Local i:Incrementer = New Incrementer(800, 30)
		Local x:Int = 600
		Local spacing:Int = 230
		If not ship.insuficientPower
			PrettyStats(x, i.getNext(), spacing, "Draw:", power_load + "%")
		Else
			PrettyStats(x, i.getNext(), spacing, "#ff0000Draw:", power_load + "%")
		End
		If backup_time = 0
			i.getNext()
			'PrettyStats(x, i.getNext(), spacing, "Backup Time:", "INF")
		Else
			PrettyStats(x, i.getNext(), spacing, "Backup:", GetPrettyTime(backup_time))
		End
		If heat_gen > heat_release
			PrettyStats(x, i.getNext(), spacing, "#ff0000Heat:", heat_load + "%")
		Else
			PrettyStats(x, i.getNext(), spacing, "Heat:", heat_load + "%")
		End
		If heat_gen <= heat_release
			i.getNext()
		'	PrettyStats(x, i.getNext(), spacing, "Burnout Time:", "INF")
		Else
			PrettyStats(x, i.getNext(), spacing, "Burnout:", GetPrettyTime( (coolant_tank) / (heat_gen - heat_release) / 3))
		End
		PrettyStats(x, i.getNext(), spacing, "Shield:", shield_tank)
		If shield_time = 0
			i.getNext()
		'	PrettyStats(x, i.getNext(), spacing, "Recharge Time:", "INF")
		Else
			PrettyStats(x, i.getNext(), spacing, "Recharge:", GetPrettyTime(shield_time))
		End
			
		i = New Incrementer(800, 30)
		x = 950
		PrettyStats(x, i.getNext(), 150, "T/W:", SigDig(ship.GetThrust(), 2))
		If ship.GetRange() = 0
			PrettyStats(x, i.getNext(), 150, "Efficency:", 0.0)
		Else
			PrettyStats(x, i.getNext(), 150, "Efficency:", SigDig(ship.GetRange() / float(ship.items.CountItem(Item.FUEL)), 2))
		End
			
		i = New Incrementer(800, 30)
		x = 1200
		If area <> Null
			PrettyStats(x, i.getNext(), 150, "Cost:", FormatCredits(ship.GetValue(area, False)))
		End
		
		PrettyStats(x, i.getNext(), 150, "Hull:", (ship.HP))
		PrettyStats(x, i.getNext(), 150, "Armor:", (ship.HP - ship.maxHP))
		PrettyStats(x, i.getNext(), 150, "Defence:", (ship.defence + "%"))
	End
	
	Method Render:Void()
		Local i:Incrementer = New Incrementer(800, 30)
		Local x:Int = 600
		
		PrettyStats(x, i.getNext(), 200, "Power:", reactor_power)
		PrettyStats(x, i.getNext(), 200, "Battery:", batt_output)
		PrettyStats(x, i.getNext(), 200, "Heat:", (total_heat) + "/" + heat_release)
		PrettyStats(x, i.getNext(), 200, "Engine Pow:", engine_power)
		
		PrettyStats(x, i.getNext(), 200, "Gun Pow:", gun_power)
		PrettyStats(x, i.getNext(), 200, "Gun Heat:", SigDig(gun_heat, 2))
		PrettyStats(x, i.getNext(), 200, "Shield Pow:", SigDig( (ship.GetMass(, True) / 1000.0), 1) + "t")
		PrettyStats(x, i.getNext(), 200, "Recharge:", SigDig( (ship.GetMass(, True) / 1000.0), 1) + "t")
			
		i = New Incrementer(800, 30)
		x = 900
		PrettyStats(x, i.getNext(), 150, "T/W:", SigDig(ship.GetThrust(), 2))
		If ship.GetRange() = 0
			PrettyStats(x, i.getNext(), 150, "Efficency:", 0.0)
		Else
			PrettyStats(x, i.getNext(), 150, "Efficency:", SigDig(ship.GetRange() / float(ship.items.CountItem(Item.FUEL)), 2))
		End
			
		i = New Incrementer(800, 30)
		x = 1200
		PrettyStats(x, i.getNext(), 150, "Dry Mass:", SigDig( (ship.GetMass(, True) / 1000.0), 1) + "t")
		PrettyStats(x, i.getNext(), 150, "Mass:", SigDig( (ship.GetMass() / 1000.0), 1) + "t")
		PrettyStats(x, i.getNext(), 150, "Hull:", (ship.maxHP))
		PrettyStats(x, i.getNext(), 150, "Armor:", (ship.HP - ship.maxHP))
		PrettyStats(x, i.getNext(), 150, "Defence:", (ship.defence + "%"))
	End
	

End
