Strict

Import space_game


Class Storage
	Const GRID_SIZE:Int = 120
	Const OVER_TIME_POPUP:Int = 12
	Const PADDING:Int = 5
	Const POPUP_W:Int = 3
	Const POPUP_H:Int = 2
	
	Const DEBUG_INFINITE:Int = 1
	Const STORAGE_CHEST:Int = 2
	Const STORE:Int = 3
	
	Field items:Inventory
	Field manager:InventoryManager
	Field type:Int
	Field over:Item
	Field x:Int
	Field y:Int
	Field rows:Int
	Field cols:Int
	Field overx:Int
	Field overy:Int
	Field lastx:Int
	Field lasty:Int
	Field overtime:Int = 0
	Field alpha:Float
	Field selected:bool
	Field kill:bool
	Field yoffset:Int
	
	Field refund:int
	
	Field vRows:int
	Field yOffset:Int
	Field nub:float = 0
	Field nubpercent:float
	Field nubBar:bool
	
	Field grab:GrabItem
	Field storeData:Economy
	Field pass:PassData
	Field filter:ItemFilter
	Field delta:DeltaItem
	
	Method New(x:int, y:Int, rows:Int, cols:Int, pass:PassData, items:Inventory, grab:GrabItem, type:int, storeData:Economy = Null, delta:DeltaItem = Null)
		filter = New ItemFilter()
		Self.pass = pass
		Self.x = x
		Self.y = y
		Self.rows = rows
		Self.cols = cols
		Self.items = items
		Self.grab = grab
		Self.type = type
		Self.storeData = storeData
		Self.delta = delta
		If type = STORE
			items.Sort(True, True)
		End
		Local count:int = items.slots.Length() - (rows * cols)
		If (count > 0)
			If count Mod cols = 0
				vRows = count / cols
			Else
				vRows = (count / cols) + 1
			End
		End
	End
	
	Method Render:Void()
		'	If grabItem <> Null then Print "type: " + grabItem.type
		DrawRectOutline(x, y, (cols * GRID_SIZE) + ( (cols + 1) * PADDING), (rows * GRID_SIZE) + ( (rows + 1) * PADDING))
		If vRows > 0
			DrawRectOutline(x + (cols * GRID_SIZE) + ( (cols + 1) * PADDING), y, 30, (rows * GRID_SIZE) + ( (rows + 1) * PADDING))
			DrawRectOutline(x + (cols * GRID_SIZE) + ( (cols + 1) * PADDING) + 1, y + nub + 1, 28, 28 * 3)
			ResetColorAlpha()
		End
		SetScaledScissor(x, y, 5 + (cols * GRID_SIZE) + ( (cols + 1) * PADDING), 5 + (rows * GRID_SIZE) + ( (rows + 1) * PADDING))
		Local count:Int = 0
		For Local i:Int = 0 Until rows + vRows
			For Local j:Int = 0 Until cols
				If count < items.slots.Length()
					DrawRectOutline(GridToX(i, j), GridToY(i, j), GRID_SIZE, GRID_SIZE)
					count += 1
				End
			Next
		Next
		Local r:Int = 0
		Local c:Int = 0
		
		For Local temp:Int = 0 Until items.slots.Length()
			If items.slots[temp] <> Null And items.slots[temp].count > 0
				DrawImage(items.slots[temp].icon, GridToX(r, c) + (GRID_SIZE / 2), GridToY(r, c) + (GRID_SIZE / 2))
				If items.slots[temp].comp <> Null
					Local color:int = items.slots[temp].comp.QualityLevelToColor(items.slots[temp].qualityLevel)
					SetColor(HexToRed(color), HexToGreen(color), HexToBlue(color))
					DrawRectOutline(GridToX(r, c), GridToY(r, c), GRID_SIZE, GRID_SIZE)
					SetColor(255, 0, 0)
					DrawRect(GridToX(r, c) + GRID_SIZE - 5, GridToY(r, c) + GRID_SIZE, 5, -Clamp(int(items.slots[temp].comp.getPermDamage() / 50.0 * GRID_SIZE), 0, GRID_SIZE - 1))
					ResetColorAlpha()
				End
				If items.slots[temp].count > items.slots[temp].max
					Game.white_font.Draw(items.slots[temp].count, GridToX(r, c) + 5, GridToY(r, c) + 80)
				Else If items.slots[temp].max > 1
					Game.white_font.Draw(items.slots[temp].count + "/" + items.slots[temp].max, GridToX(r, c) + 5, GridToY(r, c) + 80)
				End
			End
			c += 1
			If c >= cols
				r += 1
				c = 0
			End
		Next
		
		If selected And over <> Null
			Local xPos:Int = GridToX(overy + 1, overx)
			Local yPos:Int = GridToY(overy + 1, overx)
			Local width:Int = GRID_SIZE * POPUP_W + ( (POPUP_W - 1) * PADDING)
			Local height:Int = GRID_SIZE * POPUP_H + ( (POPUP_H - 1) * PADDING)
			
			
			While (overx + POPUP_W > cols)
				overx -= 1
				xPos = GridToX(overy + 1, overx)
				yPos = GridToY(overy + 1, overx)
			Wend
			If yPos > ( (rows - POPUP_H) * GRID_SIZE) + y + ( (rows + 1) * PADDING)
				overy -= (1 + POPUP_H)
				xPos = GridToX(overy + 1, overx)
				yPos = GridToY(overy + 1, overx)
			End
			SetAlpha(alpha)
			DrawRectOutline(xPos, yPos, width, height)
			SetColor(0, 0, 0)
			DrawRect(xPos + 5, yPos + 5, width - 10, height - 10)
			SetColor(255, 255, 255)
			SetAlpha(alpha)
			Game.white_font.Draw(over.formatedName, xPos + width / 2, yPos + 10, 0.5)
			Local s:Stack<String>
			If storeData = Null
				s = over.GetDescr()
			Else
				s = over.GetDescr(, True, storeData)
			End
			For Local temp:Int = 0 Until s.Length()
				Game.white_font.Draw(s.Get(temp), xPos + 10, yPos + 50 + (temp * 30))
			Next
			ResetColorAlpha()
		End
		SetScaledScissor(0, 0, ScreenWidth, ScreenHeight)
		#rem
		If grabItem <> Null
			DrawImage(grabItem.icon, Game.Cursor.x(), Game.Cursor.y())
			If grabItem.max > 1
				Game.white_font.Draw(grabItem.count + "/" + grabItem.max, Game.Cursor.x() -60, Game.Cursor.y() +15)
			End
		End
		#end
		
	End
	
	Method Update:Void()
		If grab.hasPass And grab.isTarget(pass) And filter.canTouch(grab.item.type) 'incoming pass, yo
			Print "store has pass"
			If type = DEBUG_INFINITE
				grab.passSuccess()
			Else If type = STORAGE_CHEST
				For Local temp:Int = 0 Until items.slots.Length() 'get the pass
					If items.slots[temp] <> Null And items.slots[temp].id = grab.item.id
						Local room:Int = items.slots[temp].max - items.slots[temp].count
						Local move:Int = Min(room, grab.item.count)
						items.slots[temp].count += move
						grab.item.count -= move
						If grab.item.count = 0
							grab.passSuccess()
						End
					End
				Next
				If grab.success = False
					For Local temp:Int = 0 Until items.slots.Length()
						If items.slots[temp] = Null And grab.success = False
							items.slots[temp] = grab.item
							grab.passSuccess()
						End
					Next
				End
			Else If type = STORE
				Player.addCredits(grab.item.GetValue(storeData))
				For Local temp:Int = 0 Until items.slots.Length() 'get the pass
					If items.slots[temp] <> Null And items.slots[temp].id = grab.item.id And items.slots[temp].damage = grab.item.damage and items.slots[temp].qualityLevel = grab.item.qualityLevel
						items.slots[temp].count += grab.item.count
						grab.passSuccess()
						Exit
					End
				Next
				If grab.success = False
					For Local temp:Int = 0 Until items.slots.Length()
						If items.slots[temp] = Null And grab.success = False
							items.slots[temp] = grab.item
							grab.passSuccess()
						End
					Next
				End
				If grab.success = False
					Player.addCredits(-grab.item.GetValue(storeData))
				End
			End
		End
		If grab.hasPass And grab.success = False And grab.isHome(pass) 'handle a failure
			If grab.hasBackupTarget()
				grab.swapToBackup()
			Else
				If type = DEBUG_INFINITE
					For Local temp:Int = 0 Until items.slots.Length()
						If items.slots[temp] = grab.item
							items.slots[temp] = New Item(ItemLoader.GetItem(items.slots[temp].id), grab.item.damage, grab.item.qualityLevel)
							items.slots[temp].count = items.slots[temp].max
						End
					Next
				Else If type = STORE
					Local ti:Item = New Item(ItemLoader.GetItem(grab.item.id), grab.item.damage, grab.item.qualityLevel)
					ti.count = refund
					Player.addCredits(-ti.GetValue(storeData))
					refund = 0
				End
				grab.ClearGrab()
			End
		Else If grab.hasPass And grab.success And grab.isHome(pass) 'handle a success
			If type = DEBUG_INFINITE
				For Local temp:Int = 0 Until items.slots.Length()
					If grab.item <> Null And items.slots[temp] = grab.item
						items.slots[temp] = New Item(ItemLoader.GetItem(items.slots[temp].id), grab.item.damage, grab.item.qualityLevel)
						items.slots[temp].count = items.slots[temp].max
						grab.ClearGrab()
					End
				Next
			Else If type = STORE
				For Local temp:Int = 0 Until items.slots.Length()
					If grab.item <> Null And items.slots[temp] = grab.item
						If items.slots[temp].count = 0
							items.slots[temp] = Null
						End
						grab.ClearGrab()
					End
				Next
			Else
				For Local temp:Int = 0 Until items.slots.Length()
					If grab.item <> Null And items.slots[temp] = grab.item
						items.slots[temp] = Null
						grab.ClearGrab()
					End
				Next
			End
		End
		If CursorInScreen() And vRows > 0
			If Not Game.Keys.KeyDown(KEY_SHIFT)
				If Game.Cursor.WheelUp()
					Local num:Float = (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING) / float(vRows)
					nub -= num
					nub = Clamp(nub, 0.0, float(y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING))
					If nub = 0
						nubpercent = 0
					Else
						nubpercent = 1 + (float(nub - (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING))) / (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING)
					End
					yOffset = -nubpercent * ( (vRows) * GRID_SIZE + ( (vRows)) * PADDING)
				Else If Game.Cursor.WheelDown()
					Local num:Float = (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING) / float(vRows)
					nub += num
					nub = Clamp(nub, 0.0, float(y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING))
					If nub = 0
						nubpercent = 0
					Else
						nubpercent = 1 + (float(nub - (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING))) / (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING)
					End
					yOffset = -nubpercent * ( (vRows) * GRID_SIZE + ( (vRows)) * PADDING)
				End
			End
		End
		If Game.Cursor.Hit()
			If Game.Cursor.x() > x + (cols * GRID_SIZE) + ( (cols + 1) * PADDING) And Game.Cursor.x() < x + (cols * GRID_SIZE) + ( (cols + 1) * PADDING) + 30 And Game.Cursor.y() > y And Game.Cursor.y() < y + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING
				nubBar = True
			End
		End
		
		
		If Game.Cursor.Down And nubBar
			nub = Clamp(int(Game.Cursor.y()) -y - 42, 0, y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING)
			If nub = 0
				nubpercent = 0
			Else
				nubpercent = 1 + (float(nub - (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING))) / (y - 186 + (rows * GRID_SIZE) + ( (rows + 1)) * PADDING)
			End
			yOffset = -nubpercent * ( (vRows) * GRID_SIZE + ( (vRows)) * PADDING)
		Else
			nubBar = False
		End
		If Game.Cursor.MiddleHit() And CursorInScreen()
			If type = STORE
				items.Sort(True)
			Else
				items.Sort()
			End
			
			kill = True
			nub = 0
			yOffset = 0
		End
		If Not kill
			overx = -1
			overy = -1
		End
		Local found:Bool
		Local c:Int = 0
		Local r:Int = 0
		
		
		
		For Local temp:Int = 0 Until items.slots.Length()
			If isMouseOver(r, c) And CursorInScreen()
				If (Game.Keys.KeyDown(KEY_SHIFT) And Game.Cursor.Hit() And grab.item = Null And items.slots[temp] <> Null) And filter.canTouch(items.slots[temp].type)
					If type = STORE
						If Player.getCredits() >= items.slots[temp].GetValue(storeData)
							Player.addCredits(-items.slots[temp].GetValue(storeData))
							grab.StorePass(items.slots[temp], pass, Self)
						Else
							'storeData.notEnoughMoney = True
						End
					Else
						grab.StorePass(items.slots[temp], pass, Self)
					End
				Else If Game.Cursor.RightHit() And grab.item = Null And items.slots[temp] <> Null And filter.canTouch(items.slots[temp].type)  'split buy this item
					Local max:Int = Min(items.slots[temp].count, items.slots[temp].max)
					Local num:Int = Clamp(items.slots[temp].count, 1, Max(max / 2, 1))
					If type = DEBUG_INFINITE
						items.slots[temp].count = num
						grab.StoreGrab(items.slots[temp], pass, Self)
						items.slots[temp] = New Item(ItemLoader.GetItem(items.slots[temp].id), grab.item.damage, grab.item.qualityLevel)
						items.slots[temp].count = items.slots[temp].max
						kill = true
					Else If type = STORAGE_CHEST
						If num = items.slots[temp].count 'do normal stuff (Should only work when count = 1)
							grab.StoreGrab(items.slots[temp], pass, Self)
							items.slots[temp] = Null
							kill = True
						Else
							Local tItem:Item = New Item(ItemLoader.GetItem(items.slots[temp].id)) 'no damage level because only single items have damage
							tItem.count = num
							items.slots[temp].count -= num
							grab.StoreGrab(tItem, pass, Self)
						End
					Else If type = STORE
						Local tItem:Item = New Item(ItemLoader.GetItem(items.slots[temp].id), items.slots[temp].damage, items.slots[temp].qualityLevel)
						tItem.count = num
						If Player.getCredits() >= tItem.GetValue(storeData)
							If items.slots[temp].count = num 'one only
								grab.StoreGrab(items.slots[temp], pass, Self)
								Player.addCredits(-items.slots[temp].GetValue(storeData))
								items.slots[temp] = Null
							Else'overstacked
								items.slots[temp].count -= num
								Player.addCredits(-tItem.GetValue(storeData))
								grab.StoreGrab(tItem, pass, Self)
							End
						Else
							'storeData.notEnoughMoney = True
						End
					End
					kill = True
				Else If grab.item <> Null And Game.Cursor.RightHit() And filter.canTouch(grab.item.type)'Drop one one
					If items.slots[temp] = Null
						If grab.item.count = 1 'sell it
							If type = STORE
								Player.addCredits(grab.item.GetValue(storeData))
								items.slots[temp] = grab.Get()
							Else If type = DEBUG_INFINITE 'this will give and delete the item
								grab.Get()
							Else
								items.slots[temp] = grab.Get()
							End
						Else
							grab.item.count -= 1
							If type = DEBUG_INFINITE 'this will give and delete the item
								' NO REFUNDS
							Else If type = STORAGE_CHEST
								items.slots[temp] = New Item(ItemLoader.GetItem(grab.item.id)) 'no damage level because only single items have damage
							Else If type = STORE
								items.slots[temp] = New Item(ItemLoader.GetItem(grab.item.id)) 'no damage level because only single items have damage
								Player.addCredits(items.slots[temp].GetValue(storeData))
							End
						End
					'matching items	(Don't worry about damage level because
					Else If items.slots[temp].id = grab.item.id
						If type = DEBUG_INFINITE 'this will give and delete the item
							grab.item.count -= 1
						Else If type = STORAGE_CHEST
							If items.slots[temp].count < items.slots[temp].max
								items.slots[temp].count += 1
								grab.item.count -= 1
								If grab.item.count = 0
									grab.Get() 'to clear it out
								End
							End
						Else If type = STORE
							Player.addCredits(grab.item.GetValue(storeData, True))
							items.slots[temp].count += 1
							grab.item.count -= 1
							If grab.item.count = 0
								grab.Get() 'to clear it out
							End
						End
					End
				Else If Game.Cursor.Hit() Or grab.isValidDrop()
					If items.slots[temp] <> Null And grab.item = Null and filter.canTouch(items.slots[temp].type) 'buy this item
						If type = DEBUG_INFINITE
							grab.StoreGrab(items.slots[temp], pass, Self)
							items.slots[temp] = New Item(ItemLoader.GetItem(items.slots[temp].id), grab.item.damage, grab.item.qualityLevel)
							items.slots[temp].count = items.slots[temp].max
							kill = true
						Else If type = STORAGE_CHEST
							grab.StoreGrab(items.slots[temp], pass, Self)
							items.slots[temp] = Null
						Else If type = STORE
							If Player.getCredits() >= items.slots[temp].GetValue(storeData)
								If items.slots[temp].count <= items.slots[temp].max
									grab.StoreGrab(items.slots[temp], pass, Self)
									Player.addCredits(-items.slots[temp].GetValue(storeData))
									items.slots[temp] = Null
								Else'overstacked
									items.slots[temp].count -= items.slots[temp].max
									Player.addCredits(-items.slots[temp].GetValue(storeData))
									Local i:Item = New Item(ItemLoader.GetItem(items.slots[temp].id), items.slots[temp].damage, items.slots[temp].qualityLevel)
									i.count = i.max
									grab.StoreGrab(i, pass, Self)
								End

							Else
								'storeData.notEnoughMoney = True
							End
						End
						kill = True
					Else If items.slots[temp] = Null And grab.item <> Null and filter.canTouch(grab.item.type)'sell an item
						If type = STORE
							Player.addCredits(grab.item.GetValue(storeData))
						End
						items.slots[temp] = grab.Get()
					Else If items.slots[temp] <> Null And grab.item <> Null And items.slots[temp].id = grab.item.id And items.slots[temp].damage = grab.item.damage And items.slots[temp].qualityLevel = grab.item.qualityLevel and filter.canTouch(items.slots[temp].type)'combine
						If type = DEBUG_INFINITE 'this will give and delete the item
							grab.Get() ' NO REFUNDS
						Else If type = STORAGE_CHEST
							If items.slots[temp].count < items.slots[temp].max
								Local room:Int = items.slots[temp].max - items.slots[temp].count
								Local move:Int = Min(room, grab.item.count)
								items.slots[temp].count += move
								grab.item.count -= move
								If grab.item.count = 0
									grab.Get() 'to clear it out
								End
							End
						Else If type = STORE
							Player.addCredits(grab.item.GetValue(storeData))
							items.slots[temp].count += grab.item.count
							grab.Get() 'to clear it out
						End
					Else If items.slots[temp] <> Null And grab.item <> Null and filter.canTouch(items.slots[temp].type)
						If type = DEBUG_INFINITE
							grab.Get() ' NO REFUNDS
						'	items.slots[temp] = New Item(ItemLoader.GetItem(items.slots[temp].id))
						'	items.slots[temp].count = items.slots[temp].max
						
						Else If type = STORE
							If items.slots[temp].count <= items.slots[temp].max
								If Player.getCredits() + grab.item.GetValue(storeData) - items.slots[temp].GetValue(storeData) > 0
									Player.addCredits( (grab.item.GetValue(storeData) - items.slots[temp].GetValue(storeData)))
									items.slots[temp] = grab.Swap(items.slots[temp]) 'chest
								Else
									'storeData.notEnoughMoney = True
								End
							End
						Else
							items.slots[temp] = grab.Swap(items.slots[temp]) 'chest
						End
								
					End
				End
				If Not kill And grab.item = Null
					found = True
					If items.slots[temp] <> Null
						overtime += 1
					End
					If overtime >= OVER_TIME_POPUP
						overtime = OVER_TIME_POPUP
						alpha += 0.1
						If alpha >= 1 Then alpha = 1
						If selected = False
							lastx = c
							lasty = r
							selected = True
							over = items.slots[temp]
							If delta <> Null
								delta.new_item = over
								delta.grab_from = DeltaItem.STORE
							End
						End
						If lastx = c And lasty = r
							overx = c
							overy = r
						Else
							overy = lasty
							overx = lastx
							kill = True
						End
					End
				End
			End
			c += 1
			If c >= cols
				r += 1
				c = 0
			End
		Next	
		If Not found
			overy = lasty
			overx = lastx
			kill = True
		End
		If kill
			If delta <> Null And delta.grab_from = DeltaItem.STORE
				delta.new_item = Null
				delta.grab_from = DeltaItem.NOTHING
			End
			alpha -= 0.1
			If alpha <= 0
				alpha = 0
				overtime = 0
				selected = False
				over = Null
				kill = False
			End
		End
		
	End
	
	Method GridToX:Int(r:Int, c:Int)
		Return x + (c * GRID_SIZE) + ( (c + 1) * PADDING)
	End
	Method GridToY:Int(r:Int, c:Int)
		Return y + (r * GRID_SIZE) + ( (r + 1) * PADDING) + yOffset
	End
	Method CursorInScreen:Bool()
		If Game.Cursor.x() > x And Game.Cursor.x() < x + (cols * GRID_SIZE) + ( (cols + 1) * PADDING) And Game.Cursor.y() > y And Game.Cursor.y() < y + (rows * GRID_SIZE) + (rows + 1) * PADDING Then Return True
		Return False
	End
	Method isMouseOver:Bool(r:Int, c:Int)
		If (Game.Cursor.x() < GridToX(r, c) Or Game.Cursor.x() > GridToX(r, c) + GRID_SIZE) Return False
		If (Game.Cursor.y() < GridToY(r, c) Or Game.Cursor.y() > GridToY(r, c) + GRID_SIZE) Return False
		Return True
	End
End