'Used to import all angelfont stuff and extend it.  -nobu

Import AngelFont.angelfont
Import AngelFont.simpletextbox


'Capsule story font object, wraps angelFont and simpleTextBox in our coding convention for better consistency
Class CSFont
  Private
	Field origScale:Float = 1  'Used when restoring original size
  Public
	Field TextArea:= New CSFontArea
	Field font:AngelFont
	Field scale:Float = 1
	Field invScale:Float

	Method New(pathWithoutExtension:String = "", width:Int = 0, height:Int = 0, scale:Float = 1)
		font = New AngelFont()
		If pathWithoutExtension <> ""
			'LoadConfig takes a long time on android.  Figure out the problem  -nobu
			'Local ms:Int = Millisecs()

			font.LoadPlain(pathWithoutExtension)
			font.italicSkew = 0.15
			origScale = scale; Resize(scale)

			'Print "Font loading load took " + ( (Millisecs() -ms) / 1000.0) + " seconds"
		End If
		
		TextArea = New CSFontArea(0, 0, width, height)
	End Method

	Method Resize:Void(scale:Float)
		Self.scale = scale; invScale = 1 / scale
	End Method
	Method RestoreSize:Void()
		Resize(origScale)
	End Method
			
	Method Draw:Void(text:String, x:Float, y:Float, xAlign:Float = 0)
			DoScale()
			If text.Contains("#")
				Local m:IntMap<Int> = New IntMap<Int>()
				While text.Contains("#")
					Local l:Int = text.Find("#", 0)
					m.Set(l, StringToHex(text[l + 1 .. l + 7]))
					text = text.Replace(text[l .. l + 7], "")
				Wend
				font.DrawText(text, (x * invScale) - (scale * invScale * (font.TextWidth(text) * xAlign)), y * invScale, m)
			Else
				font.DrawText(text, (x * invScale) - (scale * invScale * (font.TextWidth(text) * xAlign)), y * invScale)
			End
			DoScale(False)
	End Method
		
	Method DrawHTML:Void(text:String, x:Float, y:Float, xAlign:Float = 0)
			DoScale()
			font.DrawHTML(text, (x * invScale) - (scale * invScale * (font.TextWidth(text) * xAlign)), y * invScale)
			DoScale(False)
	End Method
	
	Method StringToHex:Int(str:String)
		If str.Contains("random") Then Return - 1
		str = str.ToLower()
		Local arr:Int[] = str.ToChars()
		Local r:Int = 0
		For Local x:Int = 0 Until arr.Length()
			Local temp:Int = arr[x]
			If temp >= 48 And temp <= 57
				temp -= 48
			Else If temp >= 97 And temp <= 102
				temp -= 87
			End
			r += (temp * Pow(16, 5 - x))
		Next
		Return r
	End

	'Summary:  Debug hack method
	Method DrawTextBox:Void(text:String, x:Float, y:Float, w:Float, tracking:Int = 0)
		DoScale()
		AngelFont.current = font
		SimpleTextBox.lineGap = tracking 
		SimpleTextBox.Draw(text, x * invScale, y * invScale, w * invScale)
		DoScale(False)
	End Method
	
	'TODO:  ADD SCALING CAPABILITY TO THE BELOW FUNCTIONS =============================================
			
	'Summary:  Draws text within this font's current TextArea.
	Method DrawArea:Void(text:String, xAlign:Float = 0, yAlign:Float = 0)
		AngelFont.current = font
		SimpleTextBox.Draw(text, TextArea.x - font.TextWidth(text) * xAlign, TextArea.y +
						  (TextArea.h * yAlign), TextArea.w)
	End Method	
	'Summary:  Draws text within this object's TextArea, overriding the X and Y offsets.
	Method DrawArea:Void(text:String, x:Int, y:Int, xAlign:Float = 0, yAlign:Float = 0)
		AngelFont.current = font
		SimpleTextBox.Draw(text, x - font.TextWidth(text) * xAlign, y + (TextArea.h * yAlign), TextArea.w)
	End Method
	'Summary:  Draws text within the specified rectangle.
	Method DrawArea:Void(text:String, x:Int, y:Int, w:Int, h:Int, xAlign:Float = 0, yAlign:Float = 0)
		AngelFont.current = font
		SimpleTextBox.Draw(text, x - font.TextWidth(text) * xAlign, y + (h * yAlign), w)
	End Method	
	'Summary:  Draws text within the specified CSFontArea.
	Method DrawArea:Void(text:String, area:CSFontArea, xAlign:Float = 0, yAlign:Float = 0)
		AngelFont.current = font
		SimpleTextBox.Draw(text, area.x - font.TextWidth(text) * xAlign, area.y +
						  (area.h * yAlign), area.w)
	End Method

	'Summary:  Draws text within this font's current TextArea.
	Method DrawHTMLArea:Void(text:String, xAlign:Float = 0, yAlign:Float = 0)
		AngelFont.current = font
		SimpleTextBox.DrawHTML(text, TextArea.x - font.TextWidth(text) * xAlign, TextArea.y +
						  (TextArea.h * yAlign), TextArea.w)
	End Method	
	'Summary:  Draws text within this object's TextArea, overriding the X and Y offsets.
	Method DrawHTMLArea:Void(text:String, x:Int, y:Int, xAlign:Float = 0, yAlign:Float = 0)
		AngelFont.current = font
		SimpleTextBox.DrawHTML(text, x - font.TextWidth(text) * xAlign, y + (h * yAlign), TextArea.w)
	End Method
	'Summary:  Draws text within the specified rectangle.
	Method DrawHTMLArea:Void(text:String, x:Int, y:Int, w:Int, h:Int, xAlign:Float = 0, yAlign:Float = 0)
		AngelFont.current = font
		SimpleTextBox.DrawHTML(text, x - font.TextWidth(text) * xAlign, y + (h * yAlign), w)
	End Method	
	'Summary:  Draws text within the specified CSFontArea.
	Method DrawHTMLArea:Void(text:String, area:CSFontArea, xAlign:Float = 0, yAlign:Float = 0)
		AngelFont.current = font
		SimpleTextBox.DrawHTML(text, area.x - font.TextWidth(text) * xAlign, area.y +
						  (area.h * yAlign), area.w)
	End Method

  Private
	'Summary: Pushes or pops Scale to the matrix.  A hack for angelfont.
	Method DoScale:Void(push:Bool = True)
		If scale = 1 Then Return
		If push
			PushMatrix()
			Transform(scale, 0,0,scale,  0, 0)
			'Scale(scale, scale)
			TextArea.w /= scale  'Need to pretend wordwrap width's the same size as before
		Else
			TextArea.w *= 2
			PopMatrix()
		End If
	End Method	
End Class

'Text area for word-wrapped blocks
Class CSFontArea
	Field x:Int, y:Int, w:Int, h:Int
	
	Method New()
		w = DeviceWidth()
		h = DeviceHeight()
	End Method

	Method New(x:Int, y:Int, w:Int, h:Int)
		Self.x = x; Self.y = y; Self.w = w; Self.h = h
	End Method
End Class