'nScreen 1.6.  nobu@subsoap.com 25th August 2013
'NOTE:  TODO:  For 2.0, remove Inits Entirely.  Keep only the ctor methods; the current convention is convoluted.

Class nsScreenManager
	Global PausedForTransition:Bool                      'Global paused for input state. Stops input during transitions.
	Global PersistentOverlays:= New Stack<nScreen>       'Global stack of overlays intended to be managed in the global state.
	Global CurrentScreen:nScreen                         'The currently active screen.
	Global Screens:nsStateMachine                        'A state machine used to control the active screen.

	'Summary:  Starts the first screen in the state machine.
	Function Bootstrap:Int(statemachine:nsStateMachine)
		Screens = statemachine
		Screens.Switch(0)
	End Function
		
	'Summary:  Updates all screens.
	Function Update:Int()
		CurrentScreen.TryUpdate()
		CurrentScreen.UpdateCriticalEvents()
		
		For Local o:nScreen = EachIn PersistentOverlays
			o.TryUpdate()
			o.UpdateCriticalEvents()
		Next
	End Function
		
	'Summary:  Renders all screens.
	Function Render:Int()
		CurrentScreen.TryRender()
	
		For Local o:nScreen = EachIn PersistentOverlays
			o.TryRender()
		Next		
	End Function
	
	'Summary:  Tells the state machine to switch screens.  Utilizes current transition in effect.
	Function SwitchScreen:Void(state:Int, args:nsDataMessage = Null)
		Screens.Switch(state, args)
	End Function
End Class

'Summary:  Set up your own state machine here, using this interface, and transitions are handled for you!
Class nsStateMachine Abstract
	'Summary:  Instantly switches to the state indicated using the arguments provided.
	Method Switch:Void(state:Int, args:nsDataMessage = Null) Abstract
End Class

'Summary: The base unit, nScreen provides a way to separate game states into objects with their own loading routines, etc.
Class nScreen Abstract
	Field Owner:nScreen            'Who called this screen/overlay?
	Field Overlay:nScreen          'Is there a child overlay?
	Field Modal:Bool               'Am I modal? (Do I hijack input from my owner?)
	Field ReturnValue:nsDataMessage  'Set this to a non-null value to tell the owner that you wish to finalize.

	'Summary: Default ctor. Only use this if you don't intend to use your screen as an overlay.
	Method New()
		Init()
	End Method
	
	'Summary: Ctor convention for overlays. Copy this to your nScreen if you're using it as an overlay
	Method New(Owner:nScreen, Modal:Bool = True, args:nsDataMessage = Null)
		Init(Owner, Modal, args)
	End Method
		
	'Summary: Your init code goes here. May be depreciated in the future, so ctor code is OK to use instead.
	Method Init:Int() Abstract

	Method Init:Int(Owner:nScreen, Modal:Bool = True, args:nsDataMessage = Null)
		Self.Owner = Owner; Self.Modal = Modal
	End Method
	
	Method getType:Int() Abstract
		
		
	'Summary: Your render code goes here.
	Method Render:Int() Abstract
	
	
	'Summary: Called by the screen manager.  Tries to render recursively to the top overlay.
	Method TryRender:Int()
		Render()
		If Overlay <> Null Then Overlay.TryRender()
	End Method
	
	'Summary: Attempts an update cycle.  Transitions and modal overlays can short-circuit a screen running this method.
	Method TryUpdate:Int()
		If nsScreenManager.PausedForTransition Then Return 0  'Transition is playing.  Stop input.
		
		If Overlay <> Null And Overlay.Modal Then  'Overlay's modal.  Stop input.
			Overlay.Update()		
			
			If Overlay.ReturnValue <> Null Then   'Overlay's ready to finalize.
				ValueReturned(Overlay.ReturnValue)
			End If
			
			Return 0			
		End If
		
		Update()
	End Method

	'Summary: Your update code goes here.
	Method Update:Int() Abstract
	
	'Summary:  This method executes regardless of the Overlay and transition paused states, for critical updates that must be run every frame.
	Method UpdateCriticalEvents:Int()
		
	End Method
		
	'Summary:  Allows a screen to send arbitrary data to another screen. This is basically a callback routine.
	Method SendData:Void(data:nsDataMessage)
		'Put processing routines in the overriding method of the receiver instance.
	End Method

	'Summary:  Executes if an Overlay returns a value before closing.  Override this to process the message.
	Method ValueReturned:Void(data:nsDataMessage)
		Overlay = Null  'If the user chooses not to kill the overlay, he need not call Super.ValueReturned().
	End Method

End Class


'Summary:  Provides a message from an nScreen to its parent.
Class nsDataMessage
	Field ID:String        'Which screen sent this return value?
	Field UserData:Object  'Extra data to process with the return value.
	Field Value:Int        'The actual return value.
	
	Method New(sender:String, value:Int, userdata:Object = Null)
		ID = sender; Value = value; UserData = userdata
	End Method
End Class