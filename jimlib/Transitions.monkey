' Transitions.monkey, version 2.  
' By Nobuyuki (nobu@subsoap.com), 24 April 2013.

' Unlike the previous version of this module, Transition is now an abstract class which others
' must extend to provide the core functionality. Transitions only go in one direction, now, so 
' they must be set up on both sides of a screen fade to appear properly.  This was done in order
' to allow linking between multiple transitions, and so that the update thread waits for the render
' thread to display the last frame before saying it is at its end.  This allows us to hide any
' unsightly lockups when a resource load is occurring.

Import mojo

'Summary:  Base transition class.
Class Transition Abstract
  Private
	Field endFlag:Bool  'Tell the render thread that the update thread's finished so we can draw a full frame.
  Public
  	Field Starting:Bool 'Waiting for fade to start
	Field Started:Bool, Finished:Bool
	Field startTime:Int        'Time transition started, in ms
	Field fadeTime:Float         'How long should this transition last?
	Field Percent:Float        'How far along are we in the transition?

	'Summary:  Starts this transition with the specified fade time.
	Method Start:Void(FadeTime:Int = 500)
		startTime = Millisecs()
		fadeTime = FadeTime
		Started = True
	End Method

	'Summary:  Updates the Transition's percentage and checks to see if the transition is finished. 
	Method Update:Void()
		If Started Then
			Percent = (Millisecs() -startTime) / Float(fadeTime)
			
			If Percent >= 1 'We're ready to finish.  Next Render() we will.
				endFlag = True
			End If
		End If
	End Method

	'Summary:  NOTE:  Child classes MUST call Super.Render() to allow Transition to finalize!
	Method Render:Void()	
		If endFlag 'Finished transition. Lock to end values.
			Percent = 1
			Started = False
			Finished = True
			endFlag = False
		End If	
	End Method

	'Summary:  In your app, call this after you've checked for Finished and are ready to re-use the transition. 
	Method Reset:Void()
		Percent = 0
		Started = False
		Finished = False
		endFlag = False
	End Method

End Class

Class StandardTransition Extends Transition
	Field r:Int, g:Int, b:Int  'Fade color
	Field fadeIn:Bool          'Am I fading in or out?
	
	Method New(r:Int, g:Int, b:Int, FadeIn:Bool = False)
		Self.r = r
		Self.g = g
		Self.b = b
		fadeIn = FadeIn
	End Method
	
	Method Render:Void()
		Super.Render()  'Required
	
		If Started = False And Finished = False Then Return
		
		SetColor(r, g, b)
		If fadeIn Then SetAlpha(1 - Self.Percent) Else SetAlpha(Self.Percent)
		DrawRect(0, 0, DeviceWidth(), DeviceHeight())
		SetColor(255, 255, 255)
		SetAlpha(1)
	End Method
			
End Class

'Summary:  Fade states enum, for handling global fade states
Class FadeStates
	Const NONE = 0
	Const OUT = 1
	Const LOADING = 2
	Const FINISHED_LOADING = 3
	Const IN = 4
End Class