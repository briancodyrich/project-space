' Some sort of rudimentary audio engine.  WIP.

' Best practices:  Make your own bank of Sounds, using StringMap<Sound> or similar.
' When creating multiple Cues, use a reference to a sound from your bank, instead of loading a new one.
' Use the base Cue constructor when you need to make multiple variations of the same Sound for a SoundArray.
' DON'T use circular references in a SoundArray.  All array values should eventually lead to a Cue-like.

'	-Nobuyuki (nobu@subsoap.com)  18 Feb 2013

Import mojo


' NOTE:  There are no static classes in Monkey, so instead of having one, we have some globals in the AudioSystem class
Class AudioSystem
	Global Volume:Float = 1
	Global BGMVolume:Float = 0.1
	Global Pan:Float[32]
	
	Global MuteSFX:Bool
	Global MuteBGM:Bool

	Global currentMusic:String  'The current music playing.
	Global SoundFormat:String  'The file format used by this target.
		
	Function Init:Void()
		SetMusicVolume(BGMVolume)
		SoundFormat = ".ogg"
		#If TARGET="android" Or TARGET="glfw"
			SoundFormat = ".ogg"
		#ElseIf TARGET="flash" Or TARGET="ios"
			SoundFormat = ".mp3"
		#End
	End Function

	'Summary:  Plays music. Politeness levels- 0: Plays immediately. 1: Plays only if new music. 2: Plays only if silent.
	Function PlayBGM:Void(path:String, flags:Int = 1, politeness:Int = 1, AddFileFormat:Bool = True)
		If MuteBGM Then Return  'Don't bother playing music if music's off.
		'Don't play new music if the politeness flag is enabled and other music is currently playing.
		If politeness = 2 And MusicState() = 1 Then Return
		
		If (politeness > 0 And path <> currentMusic) or politeness = 0
			Print "AudioSystem:  Playing " + path
			If AddFileFormat Then PlayMusic(path + SoundFormat, flags) Else PlayMusic(path, flags)
			currentMusic = path
		End If
	End Function
End Class


'Summary: Cues and cue-like objects that can generate a sound should implement this
Interface PlayableSnd
	Method Play:Void(channel:Int)
	Method Stop:Void(channel:Int)
	Method Unload:Void()
End Interface

'Summary: A Cue is the basic playable object in nAudio.  It contains a Sound and associated metadata used to play one.
Class Cue Implements PlayableSnd
	Field snd:Sound
	Field lastChannelPlayedIn:Int  'Keeps track of the last channel this cue was played in, to stop it if there's only one playing.

	Field basepanning:Float
	Field basevolume:Float = 1
	Field volumeVariation:Float  'Amount the volume varies from the base.  Set to 1 to have full range.
	Field loopFlag:Int
	
	Method New(snd:Sound, loops:Bool = False, basevolume:Float = 1, volumeVariation:Float = 0, basepanning:Float = 0)
		Self.snd = snd; Self.basevolume = basevolume; Self.basepanning = basepanning; Self.volumeVariation = volumeVariation
		If loops Then loopFlag = 1
	End Method
	
	'Summary: Creates a new cue based on an old cue's sound.  -2 denotes "Use the old cue's value".
	Method New(baseCue:Cue, newvolume:Float, basepanning:Float = -2, volumeVariation:Float = -2)
		Self.snd = baseCue.snd
		loopFlag = baseCue.loopFlag
		Self.basevolume = newvolume
		If basepanning = -2 Then Self.basepanning = baseCue.basepanning Else Self.basepanning = basepanning
		If volumeVariation = -2 Then Self.volumeVariation = baseCue.volumeVariation Else Self.volumeVariation = volumeVariation
	End Method

	
	Method Play:Void(Channel:Int = 0)
		If AudioSystem.MuteSFX = True Then Return
				
		If volumeVariation = 0 Then
			SetChannelVolume(Channel, AudioSystem.Volume * basevolume)
		Else
			SetChannelVolume(Channel, AudioSystem.Volume * Clamp(basevolume - Rnd(volumeVariation), 0.0, 1.0))
		End If
			
		SetChannelPan(Channel, Clamp(AudioSystem.Pan[Channel] + basepanning, -1.0, 1.0))
		PlaySound(snd, Channel, loopFlag)
		
		lastChannelPlayedIn = Channel
	End Method

	'Summary:  Stops all cues on the specified channel. Specifying -1 stops the channel this cue was last played on.
	Method Stop:Void(Channel:Int = -1)
		If Channel = -1 Then StopChannel(lastChannelPlayedIn) Else StopChannel(Channel)
	End Method
	
	'Summary:  Unloads the sound samples inside this Cue.
	Method Unload:Void()
		snd.Discard()
	End Method
End Class

'Summary: Stores a number of cues and returns one of them
Class SoundArray Implements PlayableSnd
	Field range:ChannelRange
	Field sounds:= New Stack<PlayableSnd>  'Can be a cue or another soundbank.  Please watch out for circular references.
	
	Method New(range:ChannelRange)
		Self.range = range
	End Method

	Method New(range:ChannelRange, sounds:Cue[])
		Self.range = range

		For Local c:Cue = EachIn sounds
			Self.sounds.Push(c)
		Next
	End Method
		
	'Summary: If SoundNumber = -1 then pick a random sound in this bank. Satisfies PlayableSnd method requirement.
	Method Play:Void(SoundNumber:Int = -1)
		Play(SoundNumber, -1)
	End Method
	
	'Summary: If SoundNumber = -1 then pick a random sound in this bank. If Channel= -1, use the next channel in range.
	Method Play:Void(SoundNumber:Int, Channel:Int)
		If Channel = -1 Then Channel = range.GetNext()

		If SoundNumber = -1 Or SoundNumber >= sounds.Length() Then
			SoundNumber = int(Rnd(0, sounds.Length()))
			sounds.Get(SoundNumber).Play(Channel)
		Else
			sounds.Get(SoundNumber).Play(Channel)
		End If
	End Method

	'Summary:  Stops all channels in the range, regardless of channel number.
	Method Stop:Void(Channel:Int = 0)
		For Local i:Int = range.first To range.last
			StopChannel(i)
		Next
	End Method
	
	Method Unload:Void()
		For Local o:PlayableSnd = EachIn sounds
			o.Unload()
		Next
		
		sounds = Null
	End Method
End Class

'This class provides an iterator to iterate between a range of channels, for a multi-channel sound value to pass to things requiring it.
Class ChannelRange
	Field current:Int
	Field first:Int
	Field last:Int
	
	'Summary:  Creates a new channel range to iterate through between first and last. If last = -1, then last=first.
	Method New(first:Int = 0, last:Int = -1)
		If last = -1 Then last = first
		Self.first = first; Self.last = last
		Self.current = first
	End Method
	
	Method GetNext:Int()
		If current > first and current < last Then
			current += 1
			Return current - 1
		ElseIf current = first Then
			current += 1
			Return first
		ElseIf current = last Then
			current = first
			Return last
		Else  'If current > last then user specified first=last or a negative range.  Return first.
			current = first
			Return first
		End If
	End Method
End Class