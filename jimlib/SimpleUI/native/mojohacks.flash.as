
class MojoHacks
{
	public static function GetMouseWheel():int
	{
		return s_mouseWheelVal;
	}
	
	public static function MouseWheelScrolled():int
	{
		return int(s_mouseWheelVal != s_lastMouseWheelVal);
	}
	
	public static function MouseWheelChange():int
	{
		return s_mouseWheelVal - s_lastMouseWheelVal;
	}
	
	public static function BeginMouseWheelUpdate():void
	{
		BBFlashGame.FlashGame().GetDisplayObjectContainer().stage.addEventListener(MouseEvent.MOUSE_WHEEL, MouseWheelEvent);
	}
	
	public static function FinishMouseWheelUpdate():void
	{
		s_lastMouseWheelVal = s_mouseWheelVal;
	}
	
	public static var s_mouseWheelVal:int;
	public static var s_lastMouseWheelVal:int;
	
	public static function MouseWheelEvent( e:MouseEvent ):void
	{
		MojoHacks.s_mouseWheelVal += e.delta / 3;
	}
}