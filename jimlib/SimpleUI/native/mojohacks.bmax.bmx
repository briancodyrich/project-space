
Type MojoHacks
	
	Function GetMouseWheel:Int()
		Return s_mouseWheelVal
	End Function
	
	Function MouseWheelScrolled:Int()
		Return s_mouseWheelVal <> s_lastMouseWheelVal
	End Function
	
	Function MouseWheelChange:Int()
		Return s_mouseWheelVal - s_lastMouseWheelVal
	End Function
	
	Function BeginMouseWheelUpdate()
		s_mouseWheelVal = MouseZ()
	End Function
	
	Function FinishMouseWheelUpdate()
		s_lastMouseWheelVal = s_mouseWheelVal
	End Function
	
	Global s_mouseWheelVal:Int
	Global s_lastMouseWheelVal:Int
	
End Type
