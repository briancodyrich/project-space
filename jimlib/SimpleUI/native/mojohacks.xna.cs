
class MojoHacks
{
	public static int GetMouseWheel()
	{
		return s_mouseWheelVal;
	}
	
	public static int MouseWheelScrolled()
	{
		return Convert.ToInt32(s_mouseWheelVal != s_lastMouseWheelVal);
	}
	
	public static int MouseWheelChange()
	{
		return s_mouseWheelVal - s_lastMouseWheelVal;
	}
	
	public static void BeginMouseWheelUpdate()
	{
		s_mouseWheelVal = Mouse.GetState().ScrollWheelValue / 120;
	}
	
	public static void FinishMouseWheelUpdate()
	{
		s_lastMouseWheelVal = s_mouseWheelVal;
	}
	
	public static int s_mouseWheelVal;
	public static int s_lastMouseWheelVal;
}