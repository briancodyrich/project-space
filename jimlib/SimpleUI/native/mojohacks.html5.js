
var MojoHacks =
	{
GetMouseWheel : function()
{
return this.s_mouseWheelVal;
},
	
	MouseWheelScrolled : function()
{
return (this.s_mouseWheelVal != this.s_lastMouseWheelVal) & 1;
},
	
	MouseWheelChange : function()
{
return this.s_mouseWheelVal - this.s_lastMouseWheelVal;
},
	
	BeginMouseWheelUpdate : function()
{
var canvas = document.getElementById('GameCanvas');
	canvas.addEventListener('DOMMouseScroll', MojoHacks.MouseWheelEvent, false);
	canvas.addEventListener('mousewheel', MojoHacks.MouseWheelEvent, false);
	},
	
	FinishMouseWheelUpdate : function()
{
this.s_lastMouseWheelVal = this.s_mouseWheelVal;
},
	
	s_mouseWheelVal : 0,
	s_lastMouseWheelVal : 0,
	
	MouseWheelEvent : function( e )
{
MojoHacks.s_mouseWheelVal += e.wheelDelta ? e.wheelDelta / 120 : 0;
}
}
