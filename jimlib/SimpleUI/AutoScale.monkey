Import mojo
 Private
 
Global lastDeviceWidth:Float, lastDeviceHeight:Float

 Public

Global PreserveAspectRatio:Bool = True 'Aspect ratio preserving
Global ScreenWidth:Float = 800 'Native size of your app
Global ScreenHeight:Float = 480
Global ScaleFactor:Float  'Amount to scale window by preserving aspect ratio;  calculated at runtime

Global SlideAxis:Bool '0:  X-Axis, 1:  Y-Axis
Global SlideAmount:Int  'amount to slide the window to center the screen

Global ScaleX:Float, ScaleY:Float  'Individual scale amounts for sloppy stretch to window

Global UseGamePad:Bool = True

Function initAutoScale:Void(w:Float = 800, h:Float = 480, preserveAspectRatio:Bool = True)
	ScreenWidth = w; ScreenHeight = h; PreserveAspectRatio = preserveAspectRatio
	lastDeviceWidth = DeviceWidth(); lastDeviceHeight = DeviceHeight()

	ScaleX = DeviceWidth() / ScreenWidth
	ScaleY = DeviceHeight() / ScreenHeight
		
	'shorter distance determines scale factor
	If ScaleX < ScaleY then ScaleFactor = ScaleX else ScaleFactor = ScaleY
	
	'calculate slide distance based on derived size
	Local DiffX:Int= DeviceWidth() - ScreenWidth * ScaleFactor
	Local DiffY:Int= DeviceHeight() - ScreenHeight * ScaleFactor

	If ScaleX < ScaleY then 
		SlideAxis = True 'Set axis to Y
		SlideAmount = DiffY / 2
	else 
		SlideAxis = False 'Set axis to X
		SlideAmount = DiffX / 2
	End if   

	'Scale the slide amount by the scale factor to get the real slide amount after scaling
	SlideAmount *= (1/ScaleFactor)
End Function

Function CheckScreenDimensions:Void()
	If lastDeviceWidth <> DeviceWidth() Or lastDeviceHeight <> DeviceHeight() Then 
		initAutoScale(ScreenWidth, ScreenHeight, PreserveAspectRatio)
		
		'Print "Screen dimensions changed. " + lastDeviceWidth + "=" + DeviceWidth() + ", " +
		'	lastDeviceHeight + "=" + DeviceHeight()
	End If
End Function

'Summary: Scales the screen.  It is recommended that you call this at beginning of OnRender.
Function ScaleScreen:Void(checkForResize:Bool = True)
	If checkForResize = True Then CheckScreenDimensions()

	'SetMatrix (1,0,0,1,0,0) 'Identity Matrix
	If PreserveAspectRatio = True Then
		Scale(ScaleFactor, ScaleFactor)
		If SlideAxis = False Then Translate(SlideAmount, 0) Else Translate(0, SlideAmount)
	Else
		Scale(ScaleX, ScaleY)
	End If
End Function

'Derived mouse positions
Function dMouseX:Int()
	If PreserveAspectRatio
		If SlideAxis = False Then Return MouseX() / ScaleFactor - SlideAmount Else Return MouseX() / ScaleFactor
	Else
		Return MouseX() / ScaleX
	End If
End Function

Function dMouseY:Int()
	If PreserveAspectRatio
		If SlideAxis = True Then Return MouseY() / ScaleFactor - SlideAmount Else Return MouseY() / ScaleFactor
	Else
		Return MouseY() / ScaleY
	End If
End Function

'Derived multitouch positions
Function dTouchX:Int(index:Int = 0)
	If PreserveAspectRatio
		If SlideAxis = False Then Return TouchX(index) / ScaleFactor - SlideAmount Else Return TouchX(index) / ScaleFactor
	Else
		Return TouchX(index) / ScaleX
	End If
End Function

Function dTouchY:Int(index:Int = 0)
	If PreserveAspectRatio
		If SlideAxis = True Then Return TouchY(index) / ScaleFactor - SlideAmount Else Return TouchY(index) / ScaleFactor
	Else
		Return TouchY(index) / ScaleY
	End If
End Function

Function gamepadX:Int(index:Int = 0)
	If PreserveAspectRatio
		If SlideAxis = False Then Return TouchX(index) / ScaleFactor - SlideAmount Else Return TouchX(index) / ScaleFactor
	Else
		Return TouchX(index) / ScaleX
	End If
End Function

Function gamepadY:Int(index:Int = 0)
	If PreserveAspectRatio
		If SlideAxis = True Then Return TouchY(index) / ScaleFactor - SlideAmount Else Return TouchY(index) / ScaleFactor
	Else
		Return TouchY(index) / ScaleY
	End If
End Function



'Derived device screen positions.  Returns position based on percent from 0-1.
Function DeviceX:Float(percent:Float=1)
	If ScaleX = 1 Then Return DeviceWidth() * percent 
	If SlideAxis = False Then Return (DeviceWidth() / ScaleFactor) * percent - SlideAmount Else Return (DeviceWidth()/ScaleFactor) * percent 
End Function

Function DeviceY:Float(percent:Float=1)
	If ScaleY = 1 Then Return DeviceHeight() * percent 
	If SlideAxis = True Then Return (DeviceHeight() / ScaleFactor) * percent - SlideAmount Else Return (DeviceHeight()/ScaleFactor) * percent 
End Function

'Derived Scissor
Function SetScaledScissor:Void(x:Float, y:Float, width:Float, height:Float)
	Local sx:Float, sy:Float

	If PreserveAspectRatio = True Then
		If SlideAxis = False Then sx = (x + SlideAmount) * ScaleFactor Else sx = x * ScaleFactor
		If SlideAxis = True Then sy = (y + SlideAmount) * ScaleFactor Else sy = y * ScaleFactor

		SetScissor(sx, sy, ScaleFactor * width, ScaleFactor * height)
	Else
		sx = x * ScaleX
		sy = y * ScaleY
		SetScissor(sx, sy, ScaleX * width, ScaleY * height)
	End If
End Function

'NOTE:  WIP
Interface LetterBoxer
	Method Render:Void(x:Float, y:Float, w:Float, h:Float, TopLeft:Bool)
End Interface