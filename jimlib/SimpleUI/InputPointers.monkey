' This file is for dealing with the types of input devices available across platforms.
' Some types of interfaces may need to be custom-coded for a solution, for example,
' Using multitouch, or adjusting for surface scaling.  In those cases, SimpleUI
' should not attempt to poll directly from a single input source.
' Therefore, the InputPointer interface is used to poll for input.  One example
' class implementing InputPointer is provided, but you should write your own
' for your game to deal with how your game handles input.
'    -Nobuyuki (nobu@subsoap.com)  25 Jun 2012

Import mojo
Import AutoScale
Import mouseWheel
Import space_game

Interface InputPointer  'This is a generic wrapper for our human interface device.
	Method x:Float() Property
	Method y:Float() Property

	Method Poll:Void()  'Polls the input state
	Method Hit:Bool() Property '1st time hit 
	Method Down:Bool() Property 'Holding down
	Method RightHit:Bool() Property
	Method RightDown:Bool() Property
	Method Up:Bool() Property 'Unclicked / Lifted up
	Method IsOver:Void()
End Interface


' This class is provided as an example.  You may extend it to override individual functions,
' or simply write your own from scratch.
Class MousePointer implements InputPointer
Private
	Field Holding:Bool 'Used to implement MouseUp event
	Field _hit:Bool, _down:Bool, _up:Bool
	
	Field HoldingR:Bool 'Used to implement MouseUp event
	Field _hitR:Bool, _downR:Bool, _upR:Bool
	Field isover:bool

	Field HoldingM:Bool 'Used to implement MouseUp event
	Field _hitM:Bool, _downM:Bool, _upM:Bool
	Field wheel:ScrollWheel
	Field wheelUp:Bool
	Field wheelDown:Bool
Public

	Method Init:Void()
		wheel = GetScrollWheel()
	End
	Method x:Float() Property
		Return MouseX()
	End Method
	Method y:Float() Property
		Return MouseY()
	End Method
	Method IsOver:Void()
		isover = True
	End
	Method WheelUp:Bool()
		Return wheelUp
	End
	Method WheelDown:Bool()
		Return wheelDown
	End

	Method Poll:Void()  'This method sets all of the properties to their internal values.
		'The reason all of our input states are properties is so they can be defined in the abstract interface.
		'When we call this method, we're telling all of its internal values to be set correctly.
		'This method should be called each frame before checking any value.
		wheelUp = False
		wheelDown = False
		If wheel.YOffset > 0
			wheelUp = True
		ElseIf wheel.YOffset < 0
			wheelDown = True
		End
		wheel.YOffset = 0
		If MouseHit(MOUSE_LEFT) > 0 Then 
			_hit = True  
		Else
			_hit = False 
		End If 		
		
		If MouseDown(MOUSE_LEFT) Then 
			Self.Holding = True
			_down = True 
		Else 'Not holding MouseDown
			_down = False 
			If Self.Holding = True Then  'Was holding last frame.  Do MouseUp 
				Self.Holding = False
				_up = True
			Else ; _up = False 
			End If 
		End If
		
		If MouseHit(MOUSE_RIGHT) > 0 Then
			_hitR = True
		Else
			_hitR = False
		End If
		
		If MouseDown(MOUSE_RIGHT) Then
			Self.HoldingR = True
			_downR = True
		Else 'Not holding MouseDown
			_downR = False
			If Self.HoldingR = True Then  'Was holding last frame.  Do MouseUp
				Self.HoldingR = False
				_upR = True
			Else; _upR = False
			End If
		End If
		
		If MouseHit(MOUSE_MIDDLE) > 0 Then
			_hitM = True
		Else
			_hitM = False
		End If
		
		If MouseDown(MOUSE_RIGHT) Then
			Self.HoldingM = True
			_downM = True
		Else 'Not holding MouseDown
			_downM = False
			If Self.HoldingM = True Then  'Was holding last frame.  Do MouseUp
				Self.HoldingM = False
				_upM = True
			Else; _upM = False
			End If
		End If
			
	End Method
	
	'No Set methods;  These properties are read-only
	Method Hit:Bool() Property
'		If MouseHit(MOUSE_LEFT) > 0 then 
'			Self.Holding = True 
'			Return True 
'		End If 
'		Self.Holding = False 
'		Return False 
		Return _hit 
	End Method
	Method RightHit:Bool() Property
		Return _hitR
	End
	
	Method RightDown:Bool() Property
		Return _downR
	End
	Method MiddleHit:Bool() Property
		Return _hitM
	End
	
	Method MiddleDown:Bool() Property
		Return _downM
	End
	Method Down:Bool() Property
'		If MouseDown(MOUSE_LEFT)
'			Self.Holding = True 	
'			Return True 
'		End If
'		Self.Holding = False 
'		Return False 
		Return _down 
	End Method
	Method Up:Bool() Property
'		If Self.Holding And Not MouseDown(MOUSE_LEFT) Then 'The mouse was released
'			Self.Holding = False 
'			Print "Up"
'			Return True 
'		End If
'		Return False 
		Return _up 
	End Method
	
End Class

Class ScaledTouchPointer Extends MousePointer
	Method x:Float()
		Return dTouchX(0)
	End Method
	Method y:Float()
		Return dTouchY(0)
	End Method
End Class

Class GamepadPointer Extends MousePointer
	Field joy_threshold:Float = 0.30
	
	
	Field inUse:Bool
	Field popup:DataBox
	
	'stuff for rebinding controls
	Field disableJoy:Bool = False
	Field speed:Float = 8.0
	
	Field halo_on:Image
	Field halo_off:Image
	
	'for later maybe
	
	Field keybinds:Int[]

	
	Field lastTouchX:Int
	Field lastTouchY:Int
	Field xOffset:float
	Field yOffset:float
	
	
	Method Init:Void()
		Super.Init()
		popup = New DataBox()
		halo_on = LoadImage("cursor_halo_on.png",, Image.MidHandle)
		halo_off = LoadImage("cursor_halo_off.png",, Image.MidHandle)
		keybinds = New int[14]
		For Local temp:Int = 0 Until 14
			keybinds[temp] = temp
		Next
	End
	
	Method RebindKey:Void(joykey:Int, remapedkey:Int)
		If joykey < 0 or joykey > 14 Then Return
		If remapedkey < 0 or remapedkey > 14 Then Return
		keybinds[joykey] = remapedkey
	End
	
	Method SetKeyBindList:Void(keyarr:Int[])
		If keyarr.Length() = keybinds.Length()
			For Local temp:Int = 0 Until keyarr.Length()
				keybinds[temp] = keyarr[temp]
			Next
		End
	End
	
	Method GetKeyBindList:Int[] ()
		Local retarr:Int[] = New Int[keybinds.Length()]
		For Local temp:Int = 0 Until keybinds.Length()
			retarr[temp] = keybinds[temp]
		Next
		Return retarr
	End
	
	Method ResetDefaultKeyBinds:Void()
		For Local temp:Int = 0 Until 14
			keybinds[temp] = temp
		Next
	End
	
	Method Poll:Void()
		isover = False
		If disableJoy = True
			inUse = False
			Super.Poll
			Return
		End
		If inUse = False
			Super.Poll()
		Else
			If JoyButtonHit(JOY_A) = True
				_hit = True  
			Else
				_hit = False 
			End If 		
			
			If JoyButtonDown(JOY_A) Then
				Self.Holding = True
				_down = True 
			Else 'Not holding MouseDown
				_down = False 
				If Self.Holding = True Then  'Was holding last frame.  Do MouseUp 
					Self.Holding = False
					_up = True
				Else ; _up = False 
				End If 
			End If
		End
		
		If lastTouchX <> dTouchX(0) and lastTouchY <> dTouchY(0)
			inUse = False
			xOffset = 0
			yOffset = 0
			lastTouchX = dTouchX(0)
			lastTouchY = dTouchY(0)
		End
		If Abs(JoyX) > joy_threshold
			xOffset += (JoyX * speed)
			inUse = True
		End
		If Abs(JoyY) > joy_threshold
			yOffset -= (JoyY * speed)
			inUse = True
		End		
	
	End
	
	Method JoyButtonHit:Bool(button:Int)
		If button < 0 or button > 14 Then Return False
		If JoyHit(keybinds[button]) Then Return True
		Return False
	End
	
	Method JoyButtonDown:Bool(button:Int)
		If button < 0 or button > 14 Then Return False
		If JoyDown(keybinds[button]) Then Return True
		Return False
	End
	
	
	
	Method Draw:Void()
		If isover = False
			popup = Null
		End
		If inUse
			If isover = True
				If halo_on <> Null Then DrawImage(halo_on, x, y)
			Else
				If halo_off <> Null Then DrawImage(halo_off, x, y)
			End
		End
		If popup <> Null
			Local spaceing:Int = 100
			For Local x:Int = 0 Until popup.data.Length()
				Game.white_font.Resize(0.6)
				Game.white_font.Draw(popup.data.Get(x), 0, spaceing)
				Game.white_font.RestoreSize()
				spaceing += 100
			Next
		End
	End
	
	Method x:Float()
		If inUse
			Return dTouchX(0) + xOffset
		Else
			Return dTouchX(0)
		End
		
	End Method
	Method y:Float()
		If inUse
			Return dTouchY(0) + yOffset
		Else
			Return dTouchY(0)
		End
	End Method
End Class


Class DataBox
	Field data:Stack<String>
	Method New()
		data = New Stack<String>
	End
	Method AddString:Void(str:String)
		data.Push(str)
	End
End

