

#If TARGET="glfw" and GLFW_VERSION=3
Import "native/extern3.${TARGET}.mac.${LANG}"
Import brl.gametarget
#End

#if TARGET="glfw" and GLFW_VERSION=3

	Extern
		
	' Classes:
	Class ScrollWheel Extends Null = "external_scrollwheel::scrollInfo"
		' Fields:
		Field XOffset:Float="xoffset"
		Field YOffset:Float="yoffset"
	End
		
	' Functions:
	#If LANG = "cpp"
		Function GetScrollWheel:ScrollWheel() = "external_scrollwheel::initWheel"
				
		#If BRL_GAMETARGET_IMPLEMENTED
			Function GetScrollWheel:ScrollWheel(Game:BBGame) = "external_scrollwheel::initWheel"
		#End
		#Else
		Function GetScrollWheel:ScrollWheel() = "external_scrollwheel.initWheel"
				
		#If BRL_GAMETARGET_IMPLEMENTED
			Function GetScrollWheel:ScrollWheel(Game:BBGame) = "external_scrollwheel.initWheel"
		#End
	#End
		
	Public
	
#EndIf
