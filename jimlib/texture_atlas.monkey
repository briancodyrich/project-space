'Texture atlas routine for that weird Texture Packer thing we're using.
'Each TextureAtlas holds a map of TAtlasRects which can be used with the
'GetImage routine to return an Image in the atlas.  Originally, only LoadStraightImages
'was used, but to provide functionality for multiple frames (and for maximum efficiency),
'An atlas should be constructed once, and then all images grabbed from GetImage at init.
'Therefore, the Map lookups are only done once.
'		-Nobu (11 Feb 2013)

Import mojo

Class TextureAtlas
	Field items:= New StringMap<TAtlasRect>
	Field img:Image
	Field defaultImage:Image  'the default image to load if there's an error.

	'Replace the ctor if using this in other projects, it's a good idea!	
'	Method New(pathWithoutExtension:String, defaultImage:Image = Null)
	Method New(pathWithoutExtension:String, defaultImage:Image = Game.default_img)
		Self.defaultImage = defaultImage
	
		Try
			Local pathdata:String = app.LoadString(pathWithoutExtension + ".txt")
			Local ln:String[] = pathdata.Split("~n")  'line number
		
			img = LoadImage(pathWithoutExtension + ".png")
				
			For Local i:Int = 1 To(Int(ln[0]) * 5) Step 5
				Local r:= New TAtlasRect(int(ln[i + 1]), int(ln[i + 2]), int(ln[i + 3]), int(ln[i + 4]))
				items.Add(ln[i].Trim, r)
			Next
			
		Catch err:Throwable
			Error("There was a problem loading the atlas " + pathWithoutExtension + ".")
		End Try
	End Method

	Method GetImage:Image(imageName:String, frames:Int = 1, flags:Int = Image.DefaultFlags)
		If items.Contains(imageName) = False Then
			Print("Warning: subtexture '" + imageName + "' doesn't exist; Replacing with default")
			Return defaultImage
		End If
		Local r:TAtlasRect = items.Get(imageName)
		Return img.GrabImage(r.x, r.y, r.w, r.h, frames, flags)
	End Method
	
	
	'Loads an atlas of type StringMap<Image> from a Texture Packer metadata file.
	Function LoadStraightImages:StringMap<Image>(pathWithoutExtension:String)
		Local p:= New StringMap<Image>
		Local pathdata:String = app.LoadString(pathWithoutExtension + ".txt")
		Local ln:String[] = pathdata.Split("~n")  'line number
	
		Local img:Image = LoadImage(pathWithoutExtension + ".png")
			
		For Local i:Int = 1 To(Int(ln[0]) * 5) Step 5
			p.Add(ln[i].Trim, img.GrabImage(int(ln[i + 1]), int(ln[i + 2]), int(ln[i + 3]), int(ln[i + 4])))
		Next
		
		Return p
	End Function
End Class

'Used internally to get reference values from a texture atlas.

Class TAtlasRect
	Field x:Float
	Field y:Float
	Field w:Float
	Field h:Float
	
	Method New(X:Float, Y:Float, Width:Float, Height:Float)
		x = X; y = Y; w = Width; h = Height
	End Method
End Class