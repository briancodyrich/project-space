﻿Import mojo
'Summary: Returns distance between two points.
Function Distance:Float(x1:Float, y1:Float, x2:Float, y2:Float)
	Return Sqrt(Pow(x2 - x1, 2) + Pow(y2 - y1, 2))
End Function

'Summary:  Returns a value in radians when given a value in degrees.
Function DegToRad:Float(theta:Float)
	Return (theta * PI) / 180
End Function

'Summary:  Returns a 2-length array containing cartesian coordinates of a given polar coordinate.
Function PolarToRect:Float[] (r:Float, angle:Float)
	Local out:Float[] =[r * Cos(angle), r * Sin(angle)]
	Return out
End Function

'Summary: Rounds a number to the nearest integer value.
Function Round:Int(value:Float)
   Return value + 0.5 * Sgn(value)
End

Function easeOutQuad:float(t:float, b:float, c:float, d:float)
	t /= d
	Return -c * t * (t - 2) + b
End

Function easeInOutSine:float(t:float, b:float, c:float, d:float)
	Return -c / 2 * (Cosr(PI * t / d) - 1) + b
End

'Summary: Alters a value ± a set amount.
Function Tweak:Float(value:Float, amount:Float)
	Return value + Rnd(-amount, amount)
End Function

'Summary:  Returns a positive or negative value between the first and last values specified.
Function RndSpread:Float(first:Float, last:Float)
	Local v:Float = Rnd(first, last)
	Local inverter:Int = Sgn(Rnd(-100, 100))
	If inverter = 0 Then inverter = 1
	v *= inverter
	Return v
End Function

'Summary: Provides a class to generate a random value in an array of distributed values.
Class WeightedRnd<T>
	Field ValueTable:T[]
	
	'Summary: Provide a table containing the elements you wish to return at random.  eg: [0,0,1,1,2]
	Method New(valueTable:T[])
		Self.ValueTable = valueTable
	End Method

	'Summary:  Picks a random element from this WeightedRnd's value table.	
	Method Pick:T()
		Return ValueTable[Rnd(0, ValueTable.Length)]
	End Method
	
	'Summary:  Provides a distribution of values based on the percentages provided. Arrays must be equal size.
	'Example:  WeightedRnd.MakeDistribution([50,30,20], ["50% Item", "30% Item", "20% Item"])
	Function MakeDistribution:T[] (percentages:Int[], values:T[])
		Local output:= New Stack<T>
		For Local i:Int = 0 Until percentages.Length
			For Local j:Int = 0 Until percentages[i]
				output.Push(values[i])
			Next
		Next
		
		Return output.ToArray		
	End Function

End Class
'Summary:  Creates a distributed value array for use with WeightedRnd. Provide an array with percentages for each value.
Function MakeIntDistribution:Int[] (input:Int[], startvalue:Int = 1)
	Local output:= New Stack<Int>
	For Local i:Int = 0 Until input.Length
		For Local j:Int = 0 Until input[i]
			output.Push(i + startvalue)
		Next
	Next
	
	Return output.ToArray
End Function


'Summary:  Shortcuts to deal with date and time.  Monkey v69+.
Class DateTime
	Field time:Int[]
	Global months:=["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
	
	Method New()
		time = GetDate()
	End Method
	
	Method New(date:Int[])
		time = date
	End Method
	
	Method Now:String()	
		Local day:= ("0" + time[2])[ - 2 ..]
		Local month:= months[time[1] - 1]
		Local year:=date[0]
		Local hour:= ("0" + time[3])[ - 2 ..]
		Local min:= ("0" + time[4])[ - 2 ..]
		Local sec:= ("0" + time[5])[ - 2 ..] + "." + ("00" + time[6])[ - 3 ..]
		
		Return hour + ":" + min + ":" + sec + "  " + day + " " + month + " " + year
	End Method
	
	'Summary: Updates the date of this DateTime. If no value specified, defaults to Now.
	Method Update:Void(date:Int[] = Null)
		If date = Null Then time = GetDate() Else time = date
	End Method
		
	Method Year:Int()
		Return time[0]
	End Method
	Method Month:Int()
		Return time[1]
	End Method
	Method Day:Int()
		Return time[2]
	End Method
	Method Hour:Int()
		Return time[3]
	End Method
	Method Minute:Int()
		Return time[4]
	End Method
	Method Second:Int()
		Return time[5]
	End Method
	Method Ms:Int()
		Return time[6]
	End Method
	Method Millisecond:Int()
		Return time[6]
	End Method
	
	'Summary:  Returns a value relative to this class's epoch, 1 Jan 2013.  Can be used to measure time elapsed.
	'Note:     Each month is considered 31 days, and each year is considered 372 days long.  Hacky.
	Method MinutesSinceEpoch:Int()
		Return time[4] + (time[3] * 60) + (time[2] * 60 * 24) + (time[1] * 60 * 24 * 31) + (time[0] - 2013) * 535680
	End Method
End Class



'Summary:  Pads a number to the specified number of digits with zeroes, if the number has less digits.
Function PadNumber:String(number:Int, digits:Int = 3)
	Local n:String = String(number)
	Local len:int = Max(0, digits - n.Length)

		For Local i:Int = 0 Until len
			n = "0" + n
		Next
	
	Return n
End Function

'Summary:  Returns a value between startValue and endValue by precentage 0-1.
Function Lerp:Float(startValue:Float, endValue:Float, percent:Float)
	Return startValue + (endValue - startValue) * percent
End

'Summary: Transforms a string to fit inside a URL.
Function UriEscape:String(input:String, spacesToo:Bool = False)
	If spacesToo Then input = input.Replace(" ", "%20")
	input = input.Replace("<", "%3C")
	input = input.Replace(">", "%3E")
	input = input.Replace("#","%23")
	input = input.Replace("%","%25")
	input = input.Replace("{","%7B")
	input = input.Replace("}","%7D")
	input = input.Replace("|","%7C")
	input = input.Replace("\","%5C")
	input = input.Replace("^","%5E")
	input = input.Replace("~~", "%7E")  'Escape char is unescaped then escaped again
	input = input.Replace("[","%5B")
	input = input.Replace("]","%5D")
	input = input.Replace("`","%60")
	input = input.Replace(";","%3B")
	input = input.Replace("/","%2F")
	input = input.Replace("?","%3F")
	input = input.Replace(":","%3A")
	input = input.Replace("@","%40")
	input = input.Replace("=", "%3D")
	input = input.Replace("&","%26")
	input = input.Replace("$","%24")
	Return input
End Function

'Summary: Cycles a value until it's within the range specified, from first(inclusive) to last(exclusive).
Function Cycle:Float(value:Float, first:Float, last:Float)
	Local amt:Float = last - first  'The amount to cycle values outside the range
	
	While value >= last
		value -= amt
	Wend
	
	While value < first
		value += amt
	Wend
	
	Return value
End Function

Function FormatCredits:String(credits:int)
	Local neg_flag:Bool
	If credits < 0
		neg_flag = True
		credits = Abs(credits)
	End
	Local str:String = credits
	For Local temp:Int = str.Length() Until 0 Step - 3
		If temp - 3 > 0
			str = str[0 .. (temp - 3)] + "," + str[ (temp - 3) .. str.Length()]
		End
	Next
	If neg_flag
		Return "-¤" + str
	Else
		Return "¤" + str
	End
End

'Summary:  Reduces a float to n decimal places and returns a formatted string.
Function SigDig:String(input:Float, decimalPlaces:Int)
	Local s:String = string(input)
	Local cutoff:Int = Min(s.FindLast(".") + decimalPlaces + 1, s.Length)
	Return s[0 .. cutoff]
End Function

Function PrettyDps:String(num:Float)
	Local rs:String = int(num)
	num = int( (num - int(num)) * 10)
	If num = 0 Then Return rs
	Return rs + "." + int(num)
End

'returns (0,1) for all values of x
'greatest variance is between minRange and maxRange
Function GetSigmoid:Float(x:Float, minRange:float, maxRange:Float)
	Local range:Float = 8.0 / Abs(minRange - maxRange)
	Local shift:Float = (minRange + maxRange) / 2
	Local e:Float = 2.718281828
	Return 1.0 / (1.0 + Pow(e, -range * (x - shift)))
End

Function GetPhonetic:String(letters:String)
	Local charArr:Int[] = letters.ToUpper().ToChars()
	Local rString:String = ""
	For Local temp:Int = 0 Until letters.Length()
		Select charArr[temp]
		Case 65
			rString += "Alpha "
		Case 66
			rString += "Bravo "
		Case 67
			rString += "Charlie "
		Case 68
			rString += "Delta "
		Case 69
			rString += "Echo "
		Case 70
			rString += "Foxtrot "
		Case 71
			rString += "Golf "
		Case 72
			rString += "Hotel "
		Case 73
			rString += "India "
		Case 74
			rString += "Juliet "
		Case 75
			rString += "Kilo "
		Case 76
			rString += "Lima "
		Case 77
			rString += "Mike "
		Case 78
			rString += "November "
		Case 79
			rString += "Oscar "
		Case 80
			rString += "Papa "
		Case 81
			rString += "Quebec "
		Case 82
			rString += "Romeo "
		Case 83
			rString += "Sierra "
		Case 84
			rString += "Tango "
		Case 85
			rString += "Uniform "
		Case 86
			rString += "Victor "
		Case 87
			rString += "Whiskey "
		Case 88
			rString += "X-ray "
		Case 89
			rString += "Yankee "
		Case 90
			rString += "Zulu "
		End
	Next

	Return rString
End

Class Encoder Abstract
	Const MIN_BIT_RUN:Int = 10
	Function GetBitsRequired:Int(num:Int)
		If num > 65535 Then Return 32
		If num > 255 Then Return 16
		If num > 15 Then Return 8
		If num > 3 Then Return 4
		If num > 1 Then Return 2
		Return 1
	End
End

Function StripNums:Void(str:String)
	Local charArr:Int[] = str.ToChars()
End


Function Encode:string(code:Int[])
	Local rString:string = ""
	Local bitDepth:Int = 1
	For Local x:Int = 0 Until code.Length
		If code[x] > 1 Then bitDepth = Max(2, bitDepth)
		If code[x] > 3 Then bitDepth = Max(4, bitDepth)
		If code[x] > 15 Then bitDepth = Max(8, bitDepth)
		If code[x] > 255 Then bitDepth = Max(16, bitDepth)
		If code[x] > 65535 Then bitDepth = Max(32, bitDepth)
	Next
	rString += String.FromChar(bitDepth)
	rString += String.FromChar(code.Length)
	Local value:Int = 0
	Local pow:Int = 15
	For Local x:Int = 0 Until code.Length
		Local arr:Int[] = ToBinaryArray(code[x], bitDepth)
		For Local y:Int = 0 Until arr.Length
			value += Pow(2, pow) * arr[y]
			pow -= 1
			If pow < 0
				pow = 15
				rString += String.FromChar(value)
				value = 0
			End
		Next
	Next
	If pow <> 15
		rString += String.FromChar(value)
	End
	Return rString
End

Function Decode:Int[] (code:string)
	Local bitDepth:Int = int(code[0])
	Local size:Int = int(code[1])
	Local rArr:Int[size]
	Local current:Int = 0
	Print "Depth: " + bitDepth
	Print "Length: " + code.Length()
	Print "Size: " + size
	For Local x:Int = 2 Until code.Length()
		Local arr:Int[]
		If bitDepth = 32
			arr = ToBinaryArray(int(code[x] * 65536) + int(code[x + 1]), 32)
			x += 1
		Else
			arr = ToBinaryArray(int(code[x]), 16)
		End
		Local pow:Int = bitDepth - 1
		Local value:Int = 0
		For Local y:Int = 0 Until arr.Length
			value += Pow(2, pow) * arr[y]
			pow -= 1
			If pow < 0 And current < rArr.Length
				rArr[current] = value
				current += 1
				value = 0
				pow = bitDepth - 1
			End
		Next
	Next
	Return rArr
End

Function ToBinaryArray:Int[] (code:Int, bitdepth:Int)
	Local arr:Int[bitdepth]
	For Local x:Int = arr.Length - 1 To 0 Step - 1
		arr[x] = code Mod 2
		code /= 2
	Next
	Return arr
End



