Import mojo

'Summary:  Draws a rectangular outline.
Function DrawRectOutline:Void(x:Float, y:Float, w:Float, h:Float)
	DrawLine(x,y,x+w,y)
	DrawLine(x,y,x,y+h)
	DrawLine(x,y+h,x+w,y+h)
	DrawLine(x+w,y,x+w,y+h)
End Function

Function DrawGradient:Void(x:Float, y:Float, w:Float, h:Float, r1:Int, g1:Int, b1:Int, r2:Int, g2:Int, b2:Int, Vertical:Bool = True)
	Local r:Float, g:Float, b:Float
	If Vertical
		For Local i:Int = 0 Until h
			r = r1 + (r2 - r1) * (i / float(h))
			g = g1 + (g2 - g1) * (i / float(h))
			b = b1 + (b2 - b1) * (i / float(h))
			SetColor(r, g, b)
			DrawRect(x, y + i, w, 2)
			SetColor(255, 255, 255)
		Next
			SetColor(r2, g2, b2)
			DrawRect(x, h, w, 1)
			SetColor(255, 255, 255)		
		
	Else
		For Local i:Int = 0 Until w
			r = r1 + (r2 - r1) * (i / float(w))
			g = g1 + (g2 - g1) * (i / float(w))
			b = b1 + (b2 - b1) * (i / float(w))
			SetColor(r, g, b)
			DrawRect(x + i, y, 2, h)
			SetColor(255, 255, 255)
		Next
			SetColor(r2, g2, b2)
			DrawRect(w, y, 1, h)
			SetColor(255, 255, 255)		
	
	End If
End Function

Function DrawRegularPoly:Void(x:Float, y:Float, r:Float, nSides:Int = 3, angle:Float = 0)
	If nSides < 3 Then Return
	PushMatrix()
	Translate(x, y)
	
	Local pts:Float[nSides * 2]
	Local sweepAngle:Float = 360.0 / Float(nSides)
	For Local i:Int = 0 Until nSides
		pts[i * 2] = Cos(i * sweepAngle + angle) * r
		pts[i * 2 + 1] = Sin(i * sweepAngle + angle) * r
	Next
	
	DrawPoly(pts)
	
	PopMatrix()
End Function

'Summary:  Draws an arc.  With the default arguments, this function can also draw elipse outlines.
Function DrawArc:Void(x:Float, y:Float, xRad:Float, yRad:Float, aStart:Float = 0.0, aEnd:Float = 360.0, segments:Int = 8)
	Local x1:Float = x + ( Cos( aStart ) * xRad )
	Local y1:Float = y - ( Sin( aStart ) * yRad )
	Local x2:Float
	Local y2:Float
	Local div:Float  = ( aEnd - aStart ) / segments
	For Local i:Int = 1 To segments
		x2 = x + ( Cos( aStart + ( i * div )) * xRad )
		y2 = y - ( Sin( aStart + ( i * div )) * yRad )
			DrawLine(x1, y1, x2, y2)
		x1 = x2
		y1 = y2
	Next
End Function

'Summary: Draws a circle with sides or pie segment.
Function DrawPie:Void(x:Float, y:Float, xRadius:Float, yRadius:Float, aStart:Float = 0.0, aEnd:Float = 360.0, segments:Int = 8)
	Local points:Float[( segments * 2 ) + 4 ]
	points[0] = x
	points[1] = y
	points[2] = x + Cos(aStart) * xRadius
	points[3] = y - Sin(aStart) * yRadius
	Local div:Float = (aEnd - aStart) / segments
	For Local i:Int = 1 To segments
		points[( i + 1 ) * 2 ] = x + Cos( aStart + ( i * div )) * xRadius
		points[(( i + 1 ) * 2 ) + 1 ] = y - Sin( aStart + ( i * div )) * yRadius
	Next
	DrawPoly( points )
End Function

'Summary: Draws the outline of a rounded rectangle.
Function DrawRoundedRect:Void (x:Float, y:Float, w:Float, h:Float, r:Float=8, segments:Int=8)
	If h < w
		If r > Abs( h / 2.0 ) Then r = ( h / 2.0 )
	Else
		If r > Abs( w / 2.0 ) Then r = ( w / 2.0 )
	End If

	DrawLine(x,y+r,x,y+h-r) 'Left
	DrawLine(x+r,y,x+w-r,y) 'Top
	DrawLine(x+r,y+h,x+w-r,y+h) 'Bottom
	DrawLine(x+w,y+r,x+w,y+h-r) 'Right
	
	DrawArc(x+r,y+r,r,r,90,180,segments) 'UL
	DrawArc(x+w-r,y+r,r,r,0,90,segments) 'UR
	DrawArc(x+r,y+h-r,r,r,180,270,segments) 'LL
	DrawArc(x+w-r,y+h-r,r,r,270,360,segments) 'LR
End Function

'Summary: Draws a filled rounded rectangle.
Function FillRoundedRect:Void(x:Float, y:Float, w:Float, h:Float, r:Float=8, segments:Int=8)
	If h < w
		If r > Abs( h / 2.0 ) Then r = ( h / 2.0 )
	Else
		If r > Abs( w / 2.0 ) Then r = ( w / 2.0 )
	End If

	DrawPie(x+r,y+r,r,r,90,180,segments) 'UL
	DrawPie(x+w-r,y+r,r,r,0,90,segments) 'UR
	DrawPie(x+r,y+h-r,r,r,180,270,segments) 'LL
	DrawPie(x+w-r,y+h-r,r,r,270,360,segments) 'LR
	
	'We could do this with just 2 calls, but the rects would overlap and create a dark box in the center if alpha were to be <1	
	DrawRect(x+r,y,w-r-r,r) 'Top
	DrawRect(x,y+r,w,h-r-r)  ' Left, Center, Right
	DrawRect(x+r,y+h-r,w-r-r,r) 'Bottom

	'DrawRect(x,y+r,r,h-r-r) 'Left
	'DrawRect(x+w-r,y+r,r,h-r-r) 'Right
	'DrawRect(x+r,y+r,w-r-r,h-r-r) 'Center
End Function

'Summary: Draws an RPG-style rounded rectangle for use in dialogs.
Function DrawDialogWindow:Void(x:Float, y:Float, w:Float, h:Float, r:Float = 0, g:Float = 0, b:Float = 0, a:Float = 0.75)
	SetAlpha(a); SetColor(r, g, b)
	FillRoundedRect(x, y, w, h)
	SetColor(255, 255, 255)
	DrawRoundedRect(x + 2, y + 2, w - 4, h - 4)
End Function

'Summary: Draws multiple lines by looking for the line split escape character.
Function DrawParagraph:Void(input:String, x:Float, y:Float, xAlign:Float = 0, yAlign:Float = 0, yPad:Float = 0)
		Local piece:String[] = input.Split("~n")
		Local h:Int = GetFont().Height +yPad
		Local offset:Int = (h * piece.Length * yAlign) - (yPad * yAlign)
		
		For Local i:Int = 0 Until piece.Length
			
			DrawText(piece[i], x, y + (i * h) - offset, xAlign)
		Next 
End Function

'Summary: Converts a string into one with characters acceptable for a static-width font with half-width ligatures.	
Function MakeLigatures:String(text:String)
		Local output:String = text	
		output = output.Replace("'s", "`")
		output = output.Replace("'l", "^")
		output = output.Replace("'r", "{")
		output = output.Replace("'v", "}")
		output = output.Replace("'t", "~~")

		output = output.Replace("il", "|")
		output = output.Replace("ll", "#")
		output = output.Replace("li", "\")

		Return output 
End Function